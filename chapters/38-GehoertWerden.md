Gehört werden
=============

*CN: Ableismus, Religion mit Tradition und Kleiderordnung, vermutlich
am ehesten auf katholische Rituale anspielend.*

Sie sahen die Hafenstadt Bellim, sobald sie den Wald verlassen hatten,
unter dem die Höhle gelegen hatte. Ein Wald aus hohen, harzigen Nadelbäumen,
die sich in dem felsigen Boden weitwurzelig festkrallten. Irgendwo
unterhalb des Felsgesteins musste Erde sein, in die die Wurzeln durch
Felsritzen hinabreichten. Die Bäume hatten hohe Stämme. Die Rinde fühlte
sich bröselig an. Die Äste saßen so hoch, dass der Wald leicht zu
durchdringen war. Und trotzdem ergab sich das
Stadtbild für Lilið erst, als sie nur noch wenige Reihen Stämme vom
Waldrand trennten.

Bellim war eine Hafenstadt, wie Lilið noch nie zuvor eine gesehen hatte. Sie
war auf einen flachen Hügel erbaut worden und lag insgesamt tiefer als
der Wald, sodass sie die weißen und gelben Häuschen von oben
sehen konnte. Die flachen Dächer bedeckten eine riesige Fläche bis an den
Horizont. Lilið brauchte nicht zu fragen, wo die Zivil-Sakrale und wo die
Zentral-Sakrale waren. Erstere war ein großes, weißes Gebäude im Zentrum der
Stadt, höher gelegen als die Häuser darum herum, nicht zu
übersehen. Es war, wie für Sakralen üblich, ein Karree: ein
Gebäude, das einen großen, quadratischen Innenhof einschloss, in dem Blumen, Kräuter,
Gewürze und Gemüse angebaut wurde. Es gab Feierlichkeiten, zu denen jeweils geerntet
und genossen wurde. Abgesehen von dieser klassischen Grundform unterschied sich
diese Sakrale gewaltig von allen, die Lilið kannte. Das Gebäude hier war
mindestens vierstöckig. Wieviele Leute mochten hier zu den Sakrals-Dienenden
gehören? Die unteren zwei Stockwerke der vier Seiten des Gebäudes waren außerdem jeweils für einen
mächtigen Torbogen durchtrennt. Das Dach war begehbar, mit
Zinnen versehen, die wiederum mit Fahnen geschmückt waren. Sie flatterten lila und blau
gemustert im Wind und gehörten wohl zu irgendeinem Jahrestag, den Lilið nicht kannte.

Die Zentral-Sakrale in der Nähe des Hafens mochte der Stolz des
ganzen Königreichs Sper sein: Das Gebäude thronte monströs auf der Grenze
zwischen Land und Wasser. Die hellere Farbe erstrahlte gerade besonders gegen den gräulichen
Himmel dahinter, während die Sonne von oben schien. Lilið hatte vielleicht
mal einen Leuchtturm gesehen, der im Wasser stand -- so einen gab es hier
auch -- aber nie ein so riesiges Gebäude, das zur Hälfte auf mächtige Stützpfeiler
und Mauern ins Wasser gebaut worden war. Auch die Zentral-Sakrale hatte den Grundriss
eines Quadrats mit einem ausgesparten kleineren Quadrat im Inneren. Eine
Ecke des Gebäudequadrates bot einen Landzugang, die diagonal
gegenüberliegende mochte einen halben Kilometer weit ins Wasser ragen. Auch im
Wasser gab es einen Zugang, glaubte Lilið zu erkennen. Die Ecke des
Quadrats war symmetrisch zum Landeingang eingedellt, aber Lilið
konnte keine Details ausmachen. Sie entdeckte ein kleines Segelboot, das von
besagter entfernten Ecke aus gen Leuchtturm segelte. Irrte sie sich, oder sah
sie Wasser aus der Sakrale fließen?

Die Fenster deuteten an, dass die Sakrale auch vier Stockwerke hatte, diese
aber um einiges höher waren als die der Zivil-Sakrale. Sie war aus hellem
Material gebaut, das in der Sonne schimmerte und Lilið erstaunlich wenig
an Stein erinnerte. Rostrotbraune Linien maserten das sonst perlweiße
Gemäuer. "Wow.", entfuhr es Lilið leise. "Und in dem Gebäude wollen wir", Lilið
zögerte und vollendete den Satz grinsend, "ein Ding drehen?"

Drude wiegte den Kopf hin und her. "Sieht so aus.", sagte dey. "Die zwei
Zugänge, die du siehst, sind die einzigen."

Das bestätigte Liliðs Überlegungen, dass es tatsächlich zwei
gab. "Von der Landseite, und von der Seeseite also."

"Beide Eingänge sind auf ihre Art eine Herausforderung.", führte Drude
fort. "Ich würde die Wahl davon abhängig machen, ob wir herausfinden können,
auf welcher Seite der Sakrale Lajana gefangen gehalten wird."

"Bräuchten wir noch ein Boot, sollten es auf die Seeseite
hinauslaufen, über die wir eindringen?", fragte Lilið.

"Ein bestimmtes.", bestätigte Drude. "Es muss ein sakraliertes sein."

"Ein sakraliertes?" Lilið hob skeptisch die Augenbrauen. "Und? Bist du so
eines mal gesegelt? Ist es schwer, daranzukommen?"

"Ich bin nie mit so einem gesegelt, aber ich glaube, die eigentliche
Hürde liegt woanders.", antwortete dey. "Da ist nicht einfach ein
Steg hinten am Gebäude. Ich habe es von der Seeseite gesehen. Ich
habe keine Ahnung, wie die Boote oder die Menschen aus den Booten die zwei
Stockwerke zum Eingang hinaufgelangen. Aber lass uns mal hinne
machen und bei der Zivil-Sakrale anheuern. Nicht anheuern,
du weißt. Zügig, sonst wird das nix. Gar nix!"

Lilið warf noch einen Blick auf den hinteren Teil des Gebäudes. Sie
hätte gern ihr Fernglas hervorgeholt, aber Drude hetzte. Es ergab
schon Sinn, was Drude sagte: Das Tor oder was es war, das symmetrisch
beide Ecken einschnitt, lag auf Landseite fast ebenerdig, aber auf Seeseite
wohl etwa zwei Stockwerke oberhalb des Wasserspiegels. Vor den Türen befand
sich jeweils eine Terrasse, äußere Galerie oder ein Laufgang, -- Lilið
wusste die Bezeichnung für so etwas nicht --, breit, aber nicht das ganze
Gebäude umschließend. Der Boden bot eine Möglichkeit, vor dem Gebäude
auf Höhe des Eingangs zu stehen oder eingeschränkt zu flanieren. Aber auch dieser
Boden lag auf Seeseite weit oberhalb des Wasserspiegels. Lilið konnte
gerade so ausmachen, dass die Mauern, die die Terrasse stützten, nicht
aus Stufen bestehen konnten, sondern steil ins Wasser hinabreichten.

---

Kurz bevor sie einen Pfad erreichten, der Richtung Stadt führte,
bat Drude sie, sie beide unauffälliger zu falten. Nur, bis sie
Sakralutten gestohlen hätten. Lilið tat es und fühlte sich
dabei überraschend routiniert.

Es war ein interessantes Gefühl,
den ersten Eindruck der Stadt hinter Drude herlaufend zu
bekommen. Dey kannte sich aus. Dey wählte einen
Zickzackweg durch schmale Gassen zwischen den Häusern die sachte
ansteigenden Wege hinauf. Die Luft war warm und trocken und schmeckte
nach feinem Sand. Kleine, blassgraue und zartblaue Drachen gurrten von den
Dächern oder spazierten über die Gassen, wichen ihnen aber
rasch aus. Auf den meisten Straßen war wenig Betrieb, aber zweimal
kreuzten sie eine Hauptstraße, wo das anders war. In der Mitte jener
waren in regelmäßigen Abständen Bäume angepflanzt worden, die
inzwischen die Häuser überragten und mit ihren großen, breiten Blättern
Schatten boten. Auch von den Bäumen hingen Wimpel in den Farben
lila und blau. Lilið wagte nicht zu fragen, was es bedeutete, weil
Drude zügig voranschritt und schwieg. Unter den Bäumen spielten
Kinder und Jugendliche Ball oder kletterten an Seilen oder
schaukelten. An den Straßenrändern saßen Erwachsene mit Getränken
und unterhielten sich. Gab es hier so etwas wie eine
Mittagsruhe, die gerade war? Oder war es einer der Feiertage,
an denen alle frei hatten? In Nederoge gab es zwei solcher Feiertage, aber
Lilið hatte gehört, dass es anderswo mehr und andere Feiertage gab.

Sie entschied sich, doch zu fragen, falls sie dadurch mehr Ritualdinge
hätte wissen müssen, und schloss zu Drude auf. "Ist heute ein
Feiertag?", flüsterte sie, als sie sich ausreichend weit weg von
allen Menschen befanden, dass nicht auffiele, dass sie Baeðisch
sprach.

Drude nickte. "Das spielt uns in die Hände. Beeil dich!"

Drude legte noch einen Zahn zu, sodass Lilið aus der Puste war,
als sie die Sakrale endlich erreichten. Gerade rechtzeitig für
den Gong, der das Ende einer Verkündung einläutete. Drude führte
sie durch einen Eingang in einen hohen Flur, der kurz darauf von
Menschen geflutet wurde. Sie strömten durch eine Flügeltür aus
dem Saal der Verkündung an ihnen vorbei die Vorhalle entlang
ins Freie. In dem Gewusel fielen Lilið und Drude kaum
auf. Gleichzeitig waren die meisten Sakrals-Dienenden wahrscheinlich noch
im Saal beschäftigt oder an den Glocken oder an den Musikinstrumenten,
die das Ende der Verkündung begleiteten, sodass ihnen weniger
als sonst begegnen würden.

Drude führte Lilið eine Treppe hinab an den Toiletten vorbei in
einen Kellerflur. Es war sofort ruhig, kühl und leer. Am Ende
des Flurs führte Drude sie ein weiteres Stockwerk tiefer, hielt
Lilið aber kurz am Ärmel fest, um eine Gestalt am Fuß der Treppe
vorbeigehen zu lassen. Sie wurden nicht bemerkt. Dann blickte
Drude im Keller nach links und rechts und führte Lilið einen
schmalen, schwach erleuchteten Gang entlang, bis dey vor einer
verschlossenen Tür stehen blieb. Dey nickte Lilið zu und deute
auf das Schloss.

Der Flur machte auf Lilið den Eindruck, als wäre er vor allem
für Dienstpersonal gedacht. Das ergab in einer Sakrale vielleicht
weniger Sinn. Hier dienten alle. Lilið wusste, dass es zwar
Karrieren mit Aufstiegsmöglichkeiten gab, aber jede Drecksarbeit
wurde weiterhin auch jeder Person zugeteilt. Trotzdem. Hier roch
es nach Reinigungsmitteln. Der Boden war nicht edel. Die Türen
sehr zweckmäßig. Dieser Flur war nicht für Besuchende gedacht,
sondern für Leute, die zur Sakrale gehörten.

Lilið haderte nicht lange. Sie ließ sich auf ein Knie nieder
und suchte ihr Werkzeug aus der Tasche, den Blick dabei auf das
Schloss gerichtet. Es war kein solches Kellerschloss, bei dem
ein einfacher Drahtbügel zum Öffnen gereicht hätte, aber sehr komplex
war es nicht. Lilið musste drei Stifte setzen und es sprang
innerhalb von kaum einer halben Minute auf. Drude nickte
anerkennend und betrat mit ihr den Raum.

---

Die Sakralutte fühlte sich steif an und machte sie unbeweglicher.
Lilið konnte verstehen, warum Drude so etwas nicht mochte. Allerdings
schränkte es sie nicht in dem ein, was sie vorhatten. In Sakralutten
wurde höchstens vorsichtig geeilt. Ein Schreiten mit geradem Rücken
und gesenktem Kopf war die übliche Fortbewegungsform. Lilið mochte
allerdings an der Kleidung, dass sie sie in eine geradere Haltung
zwang oder eine andere zumindest ungemütlicher machte.

Die Kapuze hing weit über Liliðs Stirn hinweg. Sie hätte gern andere
Personen in diesen Sakralutten beobachtet, um sie nachzuahmen, aber ihr
Sichtfeld war durch die Kapuze beschränkt. Erst, als Drude sich mit ihr
neben die Tür des Haupt-Sakraleten stellte, nachdem dey geklopft
hatte, ergab sich die Möglichkeit. Meist in Paaren schreiteten
verschiedene Sakrals-Dienende den Gang entlang. Die meisten
nahmen sie nicht zur Kenntnis, -- sie unterhielten sich höchstens
leise miteinander --, aber einige wenige floskelten ihnen ein
"Beðem Ajad?" zu. Lilið hatte es in der Höhle so unzählige Male
beantwortet, kannte den zeitlichen Abstand, sodass sie ohne Zögern
mit Drude zugleich mit "Hatinan" antwortete. Trotzdem fühlte sie
dabei ihren Adrenalin-Pegel unbehaglich ansteigen. Hoffentlich
würde er sie nicht zu frühzeitig erschöpfen.

Die Tür, neben der sie standen, öffnete sich, und ein kleiner Mensch
ließ sie ein. Seine Sakralutte hatte im Gegensatz zu ihren blasseren
ein strahlendes Rot. Er ließ sie ein und schloss die Tür
hinter ihnen. Drude blieb mit Lilið hinter
den Stühlen vor einem halbmondförmigen Tisch stehen. Aus einem hohen
Fenster viel Sonnenlicht auf die halbe Tischplatte und zeichnete
ein abgeschnittenes Rechteck auf den Boden davor. Es wärmte
Liliðs einen Fuß, der andere stand im Schatten. Lilið wusste nicht,
wie sie sich hier verhalten musste, also ahmte sie einfach Drude
nach.

Der Sakralet schritt gemächlich um sie herum und sprach dieselben Worte
aus, die Lilið nun schon so oft beantwortet hatte. Sie antwortete
wieder mit Drude, erschreckte sich aber fast, weil Drude die Antwort
nicht ganz so gleichgültig aussprach wie sonst. Liliðs Eintönigkeit
ging hoffentlich in Drudes Antwort unter.

Der Sakarlet schlug seine Kapuze zurück und Lilið hielt sich gerade so
davon ab, es ihm nachzutun. Drude verharrte unbewegt. Auf ein einladendes
Handzeichen des Sakraleten hin setzten sie sich. Der Sakarlet stellte
eine Frage auf Alevisch, die Lilið nicht verstand, worauf Drude zu einer längeren
Antwort in der selben Sprache ansetzte. Lilið lauschte auf die Worte und versuchte, irgendetwas
zu verstehen. Sie überlegte, dass Drude vielleicht erzählen könnte, dass
Lilið die Sprache nicht sprach, und versuchte, Ortsnamen wie Nederoge
oder Angelsoge oder ähnliche herauszuhören. Allerdings erinnerte sie sich,
dass die Endung -oge, -- ein altes Wort für Insel in ihrer Sprache Baeðisch --,
im Alevischen nicht dieselbe war, sie also auf so etwas wie Nede- oder
Angels- mit einer anderen Endung achten müsste. Wie war die Endung für Inseln
noch im Alevischen?

Stattdessen hörte sie plötzlich ein Wort heraus, das dem alevischen
Wort für stimmlos ähnlich war, das sie kannte. Ob Drude behauptete, Lilið könne
gar nicht sprechen? Aber sie hatte doch schon den Gruß erwidert! Lilið hoffte, dass
die kurze Panik, die sie durchströmt hatte, unter der Sakralutte unsichtbar
blieb.

Sie hatte sich gerade wieder beruhigt, als Drude dere Hand ausstreckte und
sich vom Sakraleten mit den Fingern über das Handgelenk streichen ließ. Ein
Identifikationsverfahren? Würden sie Drude nun nicht doch wiederekennen? Und
was, wenn sie auch getestet würde?

Der Sakralet sprach nun auch zu Lilið. Drude stuppste sie unter dem Tisch mit
dem Fuß an und Lilið streckte unsicher den Arm aus. Die Berührung an ihrem
Handgelenk war ruhig und kühl. Sie zitterte nicht. Auch wenn sie fürchtete,
dass gleich eine Horde Wachen in den Raum strömen würde. Oder sie später
auf welche treffen würden, sobald dem Sakralet klar wurde, dass er es
mit Kriminellen zu tun hätte.

Der Sakralet leutete eine Glocke, holte einige Marken aus einer Schublade
hervor, eichte sie und reichte sie an Drude und Lilið. Lilið beobachtete, wo
Drude sie in der Sakralutte unterbrachte, und ahmte demm wieder nach. Das war
alles etwas arg aufregend. Kaum saßen sie wieder still, klopfte es an der Tür.
Der Sakralet stand auf, führte sie zur Tür und übergab sie an eine weitere
Sakrals-dienende Person in einer schlichteren Sakralutte, -- rosa, wie Liliðs und
Drudes. Sie wurden vom Sakraleten mit einem komplizierteren Sprechritual
verabschiedet, dass Lilið den letzten Nerv kostete, weil die Antwort komplexer
auszusprechen war. Aber immerhin tat sie es mit Drude zugleich, sodass ihr
Akzent vielleicht nicht so sehr auffiele.

Die Person, die sie abholte, sprach kein Wort mit ihnen. Sie führte sie zwei
Stockwerke hinauf durch einen Flur an einer Reihe nummerierter Türen vorbei
zu der mit der Nummer 217. Drude drehte sich der Person noch einmal zu, bevor
jene wieder ging, und floskelte dieses Mal dererseits ein 'Beðem Ajad?'. Lilið
wusste nicht, ob sie auch antworten sollte, aber da sich Drude an die andere Person
richtete, blieb sie still, als diese antwortete. Dann waren sie wieder allein.
Drude stieß die Tür auf, ließ Lilið ein, und schlug die Kapuze zurück, als die
Tür wieder geschlossen war. "Das war ein guter Anfang, würde ich sagen."

"Wir wurden berührt!", kommentierte Lilið. "Ich weiß nicht, ob ich das gut
finde."

"Sakralet Henre hat mich noch nie zuvor gesehen und hat auch nicht
Eichungen von Strafregistern auswendig gelernt. Für ihn sind wir
neu.", beruhigte Drude. "Das diente nur dazu, uns mit ein paar Marken
auszustatten, damit wir unterwegs an Lebensmittel und Getränke kommen, wenn
ich dich herumführe."

"Hast du behauptet, ich wäre stimmlos?", fragte Lilið.

Drudes Mimik zuckte. "Fast.", sagte dey. "Ich habe behauptet, dass du Schwierigkeiten
mit dem Sprechen hast, wenn du sehr nervös bist, oder vor Fremden, oder wenn
dir alles zu viel ist."

Lilið brummte als Zeichen, dass sie verstanden hatte. Also drehte sie wieder
ein Ding, in dem einer beteiligten Person eine Behinderung angedichtet
wurde. Interessanterweise fühlte es sich nicht so schlimm an wie beim letzten
Mal. Vielleicht, weil die Sprache nicht sprechen zu können eigentlich
eine noch größere Einschränkung war. Oder eine irgendwie ähnliche
zumindest. Und vielleicht, weil es sich für sie vertraut anfühlte, als
hätte, was Drude ihr zugeschrieben hatte, irgendwann einmal auf sie
zugetroffen. Allerdings erinnerte Lilið sich an kein Erlebnis, bei
dem das so gewesen sein sollte.

"Kannst du eigentlich Eichungen prüfen?", fragte Drude.

Lilið schüttelte den Kopf. "Ich stelle es mir nicht so schwierig vor, aber
ich habe es nie gelernt."

"Hm.", machte Drude. "Ich hätte damit gerechnet, dass du vielleicht sogar welche
erstellen könntest. Du hast unserem Kapitän ein Papier gegeben, dass dich
als Nautika Aurin ausgewiesen hat. Gerade, nachdem du noch einmal
versichert hast, Lilið von Lord Lurch zu sein, hätte ich damit
gerechnet, dass du es dir selbst erstellt hast. Wie hast du ihn dann getäuscht?"

"Das Zertifikat ist meins. Ich habe nur den Namen durch eine, hm,
ich sage mal papierinterne Faltung verändert.", erläuterte Lilið.

"Oh, das ist beeindruckend." Drude ließ sich auf eins der Betten
nieder. Es waren zwei. Lilið nahm das andere. "Ich wusste eigentlich,
dass du Lilið von Lord Lurch bist. Eigentlich war ich mir
sicher, dass du mich bei unserer ersten Unterredung nicht angelogen
hast. Aber als du dann ein Zertifikat mit Namen Aurin hattest, dass dich
ausweist, war ich dann doch nicht mehr überzeugt davon. Du bist so voller
Geheimnisse und Überraschungen." Dey machte ein einladendes Zeichen
mit der Hand neben sich. "Magst du rüberkommen?"

Lilið runzelte die Stirn, erhob sich aber und setzte sich neben
Drude.

Drude holte eine der Marken aus derer Sakralutte und
reichte sie Lilið. "Leg einen Finger sehr sachte auf das Papier,
sodass du nur die Oberfläche fühlst."

Lilið tat es. Natürlich fühlte sie nicht nur die Oberfläche, es
war Papier, das erfasste sie vollumfänglich, aber es fiel ihr
nicht schwer, die Eindrücke in ihrem Kopf zu trennen. Dann
würde sie also nun Eichungen erfühlen lernen. Als
Drude ihr auch deren Arm reichte und Lilið ihre Finger auf dere Haut
legte, stellte sie fest, dass es wirklich nicht schwierig war. Also,
es wäre vielleicht schwierig für sie gewesen, bevor sie ein Buch kopiert
hatte. Sie fühlte, wie eine bestimmte, einmalige Struktur der oberen
Hautschicht in die Härchen des Papiers eingeflochten war. Sie nickte
nicht sofort, fühlte es genau nach, ließ sich Zeit.

Sie entnahm ihrer Sakralutte einige der eigenen Marken. "Ich verzichte
im Zweifel auf eine Mahlzeit unterwegs und versuche eine auf dich
umzueichen, ja?"

Über Drudes Stirn huschte ein Runzeln, aber kurz darauf wirkte dey
wieder entspannt. "Ich kann dir keinen Mangel an Überraschungen
unterstellen. Dass du es noch nie gemacht und nicht gelernt
hast, aber dir direkt zutraust."

"Das mit den Überraschungen sagtest du eben schon einmal, das war
mir gar nicht bewusst.", hielt Lilið fest. Sie berührte eine der Marken
und verlor zunächst kurz an Selbstischerheit. Die Papierhaare dieses
Papiers fühlten sich sehr anders an. Aber das war ja auch kein
Wunder, es war auf sie geeicht. Sie erfühlte vorsichtshalber ihr
eigenes Handgelenk mit den Fingern. Es fühlte sich fremd an,
wie, wenn sie sich im Spiegel sah und nicht selbst erkannte. Aber
es passte zum Papier.

Abermals griff sie nach Drudes Handgelenk, schloss die Augen und
veränderte die Haarstruktur des Papiers entsprechend. Sie reichte
Drude die Marke. "Würdest du darauf reinfallen?"

Drude lachte lautlos. "Ich wünschte, ich wüsste nicht, was
Sache ist. Darf ich dir noch zwei Marken von mir geben, die du nicht
veränderst, und du reichst mir alle drei, wenn ich die Augen
schließe, sodass ich nicht weiß, welche welche ist?"

Lilið nahm Drude die Marke wieder ab, zu denen dey zwei weitere aus derer
Sakralutte hinzufügte. Während Drude die Augen schloss, überlegte sie, ob sie auch
eine der unveränderten eigenen Marken zur Verwirrung reichen sollte. Aber
das ergab keinen Sinn. Drude würde im schlimmsten Fall Liliðs Fähigkeiten,
zu eichen, völlig unterschätzen und glauben, sie hätte nichts verändert, und
im besten Fall die veränderte Marke nur aus zweien herausfinden.

Sie entschied sich anders und reichte Drude drei der
Marken. Drude fühlte darüber, tat
es ein zweites Mal und fischte dann eine Marke hervor. "Die ist anders."

"Das ist die, die Original geblieben ist.", kommentierte Lilið.

"Du hast eben auf die Schnelle noch eine von dir umgeeicht?", fragte
Drude.

"Ich wollte meinen Ruf mit den Überraschungen untermauern." Lilið
grinste.

"Das hast du!" Drude grinste ebenfalls -- nur für einen Moment, wie
immer. "Ich denke, für die meisten Anwendungszwecke sollte das
taugen. Eichungen sind nie genau gleich. Aber es lässt sich vielleicht,
sollte irgendwer genaue Nachforschungen anstellen, herausfinden, dass
nicht Sakralet Henre sie geeicht hat. Was für ein Naturtalent du bist!"

"Falten.", korrigierte Lilið. "Ich kann falten. Ich kann so klassische
Magie-Dinge nicht so gut. Wie Kessel erhitzen zum Beispiel."

"Gut zu wissen." Drude fühlte ein weiteres Mal nachdenklich über
die Marken. "Eichen ist jedenfalls eine Fähigkeit, die Personen
erst lernen, wenn sie bestimmte Rechtsprüfungen abgelegt haben
und vereidigt worden sind. Ich bin daran vorbeigeschrammt und
kann es nicht. Mal schauen, ob es nützlich für uns wird, dass
du es nun brauchbar kannst."

"Was ist ab jetzt der Plan?", fragte Lilið.

"Ich führe dich in der Sakrale herum und zeige dir ortseigene
Abläufe. Das mache ich langsam und gründlich. Und eigentlich
nur zur Tarnung. Ich werde dabei belauschen.", legte Drude
dar. "Ich hatte deine Anspannung wahrgenommen und dachte, es
wäre ein guter Zeitpunkt für eine Pause. Du fühlst dich nun auch
wieder entspannter an. Ich fühle mich sicherer, immer eine
Pause einzulegen, wenn bei dir die Panik so sehr ansteigt. Ist
das recht?"

Lilið verweilte einige Momente still, den Blick auf Drudes
unbewegte Mine geheftet. Dann nickte sie. "Es ist ein bisschen
unheimlich, dass du mir Stimmungen so genau anmerken kannst."

"Du kannst mich falten.", konterte Drude.

"Stimmt." Lilið seufzte. "Auf?"

---

An das alevische Wort für Prinzessin erinnerte Lilið sich. Sie hatten
in der Schule ein Märchen auf Alevisch gelesen. Lilið erinnerte sich
nicht an Details, aber eine Prinzessin war Hauptperson gewesen.

So sehr Lilið in den Gesprächen, die sie belauschten, überwiegend nichts
verstand, bekam sie doch mit, dass mindestens vier der Sakrals-Dienenden,
in deren Nähe sie sich oft aufhielten, das alevische Wort für Prinzessin
immer wieder benutzten. Oder war es ein Teekesselchen und bedeutete
noch etwas anderes? Irgendetwas in einer Sakrale Alltägliches? Immerhin
steckte im Baeðischen zum Beispiel das Wort Krone in Kronleuchter, während
ein Kronleuchter meistens nichts mit Monarchie zu tun hatte. Aber
vielleicht wählte Drude ihre Aufenthaltsorte auch gerade danach
aus, dass sie jeweils abwechselnd bei den einen zwei Sakrals-Dienenden
und bald bei den anderen zweien waren, bei denen eine Prinzessin immer
wieder Thema war. Wenn Lilið doch nur Details verstanden hätte.

Das erste Paar Sakrals-Dienende mit diesem Thema hatten sie im
Saal der Verkündung gefunden. Drude erklärte Lilið auf Alevisch (also
so, dass sie kaum ein Wort verstand), was sie wo finden würde, über
Alter und Geschichte der Sakrale, Besonderheiten dieser Sakrale. Dabei
schritten sie durch die Räumlichkeiten und außen herum. Lilið mochte die
kühlen Räume und alten, maroden Gerüche darin. Im Saal der Verkündung war, als sie
hindurchgeschritten waren, ein Paar Sakrals-Dienende mit Aufräumarbeiten
beschäftigt gewesen, das sich leise unterhalten hatte. Lilið wäre
fast zusammengezuckt, als sie bei dem Gespräch das alevische Wort für
Prinzessin das erste Mal gehört hatte.

Das andere Paar war mit Gartenarbeit beschäftigt: Fegen, Rasen schneiden,
Blumen gießen oder an ihnen herumschnibbeln. Sie hörten immer zu reden
auf, wenn Drude mit Lilið an derer Seite an ihnen vorbeischritt, aber
Lilið hatte auch bei ihnen das Wort fallen gehört.

Sie war so neugierig, worum es wohl ging, dass sie am liebsten Drude
gefragt hätte. Aber das wäre auffällig gewesen. Und Drude leiherte
keine weitere Pause an. Vermutlich weil dey zum einen keine Gelegenheit
verstreichen lassen wollte, Gespräche zu belauschen, und zum anderen
Liliðs Stimmung für stabil genug befand. Beides war richtig. Und doch
konnte Lilið die Spannung nur schwer ertragen, nicht zu wissen, was Drude
wusste.

Besonders spannend wurde es, als Drude ihr ein in das Gemäuer
eingearbeitetes Relief zeigte, es
leise (und für Lilið unverständlich) erklärte und sie dann unvermittelt
in eine Nische zerrte. Lilið fügte sich und verharrte still und starr. Schritte
näherten sich, und wieder hörte sie leises Murmeln. Sie identifizierte
die Stimmen als die der beiden Gartenarbeitenden. Sie hatte keine Ahnung,
worüber sie sprachen, außer, dass wieder das Wort für Prinzessin
fiel, aber die Stimmung konnte sie vielleicht schon deuten: Die Worte waren
teils artikuliert gesprochen, die Wortgefechte waren erhitzt aber nicht
überhitzt. Wohl wütend, aber vielleicht, als würden sie es mit Humor
nehmen oder so.

"Ich weiß es doch auch nicht!", sagte die eine der Personen. Auf Baeðisch mit
einem alevischen Akzent.

Lilið war froh, dass sie versteckt war, weil es sie so überraschte, dann
doch plötzlich die eigene Sprache zu hören.

"Hey, nicht Baeðisch sprechen hier!" Die Antwort auch auf Baeðisch, ohne
jeglichen Akzent, gleichzeitig belustigt und etwas drohend.

"Ich finde 'Ich weiß es doch auch nicht!' so schön auf
Baeðisch, und dachte, wenn die auf Regeln misten, dürfen wir das
auch." Auch bei der ersten Person klang wieder der verärgerte und gleichzeitig humorvolle
Unterton durch.

"Ich glaube, 'scheißen' gehört in die Redewendung.", korrigierte
die andere Person und fuhr dann mit langgezogenen Worten fort: "Wenn wir schon
zum Spaß Regeln brechen, dann gleich mit dem unflätigen Geflüch."

Ein Kichern der Person mit Akzent. "Du meinst, den Hammer gleich mit Schrank?"

Lilið musste ein Kichern unterdrücken, weil sie dem Gespräch spätestens
jetzt auch nicht mehr folgen konnte, obwohl es auf Baeðisch war.

"Alawin!" Die akzentfreie Person betonte den Namen, als hätte sie ihn
empört geschrien, aber hielt dabei die Stimme gesenkt.

Alawin kicherte abermals. "Zum Repertoir der schlimmen Dinge, die
wir tun, sollte auch das schlechte Übersetzen von Redewendungen
gehören. Nicht?"

Die andere Person seufzte. Die Antwort war wieder auf Alevisch, aber
Lilið verstand sie trotzdem ungefähr: "Ich hole neues Wasser."

Lilið roch die kühler werdende Luft des Abends, die Blütenschwere
darin. Wieder verstand sie Drude. Sie hätte die Luft gern direkt auf
der Haut gespürt. Eine Weile sprach niemand ein Wort, aber Drude
verharrte mit ihr in der Nische. Vielleicht konnten sie hier ohnehin
nicht unauffällig weg, bis die beiden anderen mit gießen und schnibbeln
fertig wären. Es sei denn, Lilið faltete sie. Aber Drude hatte
gewarnt, dass es nicht immer eine gute Idee war, in einer Sakrale Magie
auszuführen. Heute, hatte dey gemeint, könnten sie auch Glück haben, dass
es nicht auffiele, weil allerlei Besuchende da waren, aber
wenn sie hier mit Sakrals-Dienenden unter sich waren, dann unterstand
Magie gewissen Gesetzgebungen, die Drude für nicht einfach genug
eingeschätzt hatte, um Lilið damit im Vorfeld vertraut zu machen. Leider
konnte Lilið auch nicht fragen.

"Ich wollte schon länger mal Baeðisch mit dir sprechen.", durchbrach
die akzentfreie Stimme schließlich die Stille.

"Wieso das?", fragte die mit Akzent. "Damit ich mich mal, wie heißt
das, blammere?"

"Blamiere, wäre richtig. Nein, sicher nicht deshalb." Die akzentfreie
Stimme hatte einen melancholischen Beiklang bekommen. Kein Humor schien
mehr darin versteckt.

Alawin ließ sich Zeit mit der Reaktion. Ein paar Schnippschnappgeräusche
drangen an Liliðs Ohren. Schließlich fragte Alawin: "Worüber?"

Das Wasser aus den Gießkannen plätscherte auf die Pflanzen und tropfte auch
dort, wo sie schon gewesen waren, noch von den Blättern.

"Ich liebe dich asexuell." Die Stimme der akzentfreien Person wenig
mehr als ein Flüstern, das Lilið nur hörte, weil sie ihnen recht
nah waren.

Das war kein Gespräch, das sie belauschen wollte. Aber es gab auch
keine Möglichkeit zu verschwinden oder wegzuhören.

"Was?" Alawin antwortete auf Alevisch, aber die Vokabel kannte Lilið
gut. Halb nuschelnd fügte Alawin auf Baeðisch hinzu: "Was heißt
asexuell?"

Die akzentfreie Stimme nannte ein Wort auf alevisch, das Lilið vielleicht von
der ihr bekannten Vokabel für Liebe als unbeliebt oder ungeliebt abgeleitet
hätte. "Wörtlich übersetzt ins Baeðische hieße es 'lieblos' oder 'ohne
Liebe'. Ich fand den Klang von 'Ich liebe dich lieblos' immer so furchtbar
und albern, dass ich es bisher nie gewagt habe, es dir zu sagen."

"Ach, Lenja." Alawins Worte fühlten sich für Lilið wie ein Streicheln
an. "Heißt das, du liebst mich sehr, auf eine Art, die für dich
mehr als Freundschaft ist, aber du willst keinen Sex mit mir?"

Lenja schnaubte. "Und jetzt reden wir also auf einer Sprache, die wir
nicht sprechen sollen, über Sex. Wir nageln in der Tat den Hammer gleich
mit an die Wand."

Ein Rascheln über den Boden, eine Veränderung des Klangs des Wassers,
der aus der Gießkanne schwappte und ein verspieltes Kreischen beider
Beteiligten. Lilið interpretierte, dass sie sich gekabbelt und
nassgespritzt hatten. Dann Alawins Stimme: "Und? Heißt es das?"

"Ja." Lenja klang kleinlaut. "Wie fühlt es sich für dich an."

"Schön!" Lilið interpretierte Alawins Stimme als aufrichtig und
ehrlich. "Wirklich schön." Alawin wiederholte die Worte auch
auf Alevisch.

"Das beruhigt mich." Ein Lächeln klang aus Lenjas Stimme.

"Überhaupt nicht.", widersprach Alawin neckisch. "Aber ich, wie
sagt man das, ich will dir nichts Böses? Ich möchte dir nicht
weh tun? Etwas Weicheres."

"Ich will mal nicht so sein?", schlug Alawin vor. "Ich weiß
noch nicht, wodrauf du hinaus willst."

"Dass du von mir wissen willst, ob ich dich auch liebe.", erwiderte
Lenja. "Ich kenne dich doch, ich weiß, was für Romanzen du liest."

Einen Moment blieb es ruhig. Daraus, dass Alawin wieder sprach,
schloss Lilið, dass Lenja wohl genickt hatte. "Ich liebe dich
auch." Die Stimme war so weich, dass es Lilið den Rücken
runterrann. Sie fühlte sich wirklich falsch, ein solches Gespräch
zu belauschen, und doch genoss sie es. "Asexuell. Oder, ich weiß
nicht. Vielleicht bin ich nicht asexuell, aber ich bin im Moment
glücklich, wie es ist."

"Schön." Lenja klang geradezu verträumt.

Einen Moment hörten sie wieder nur das Plätschern. Und schließlich,
dass dieses Mal Alawin neues Wasser holte. Als sie wieder
nebeneinander standen und gossen, fragte Alawin: "Noch etwas auf
Baeðisch? Es strengt etwas an, aber üben ist auch gut. Also
sag gern, was dir lieber ist."

"Magst du mir das mit den Schleusen noch einmal auf Baeðisch
erklären?", bat Lenja.

"Du willst doch, dass ich mich", ein kurzes Zögern, "blamiere. Blamiere?"

"Blamieren ist richtig.", bestätigte Lenja. "Nein. Mein Alevisch ist
nicht gut genug gewesen, alles zu verstehen. Du weißt, Fachwörter sind
manchmal ein Problem."

"Ja, das Problem habe ich umgekehrt!", mokierte sich Alawin.

"Ich verstehe." Die Enttäuschung in Lenjas Stimme blieb Lilið
nicht verborgen.

"Ich versuche es ja.", versprach Alawin. "Was willst du wissen? Was
hast du verstanden?"

"Wir bekommen im Sakrals-Bootshaus ein Boot und fahren damit zur
Schleuse. Dort gibt es ein Pumpsystem, das uns hochfährt.", erklärte
Lenja. "Aber das funktioniert nur, wenn, ja, das habe ich nicht
verstanden."

Alawin seufzte. "Vorweg, wir machen das nicht, oder? So sehr willst
du den Hammer nicht gleich beschränken, oder?"

Lenja schnaubte. "Wenn du die Übersetzung der Redewendung, die
ohnehin nicht funktioniert, weil es sie im Baeðischen nicht gibt, weiter
so übertrieben vermurxt, hänge ich dir den Hammer gleich sonst wo hin!"

"Ich dachte, du willst keinen Sex!", kam es prompt von Alawin.

"Du bist unmöglich!", schimpfte Lenja, spielerisch. "Aber du hast
schon recht, dass die Redewendung exzellent passt. Den Hammer gleich
mit an die Wand nageln. Sowohl zu uns, wenn wir das Ritual doch
durchführen würden, obwohl sie uns gesagt haben,
dass wir das lassen sollen, als auch zu diesem Monarchie-Pack, dass
wir das lassen sollen. Als ob zwei Sakrals-Dienende, die über
ein Boot in eine Magie abschirmende Hochsicherheits-Sakrale
geschleust werden, um das Reinheits-Ritual auszuführen, viel
ausrichten könnten. Dass das die Ruhe der Kronprinzessin stören
würde, kann ich mir auch echt nicht vorstellen. Alles übertrieben."

"Das hatten wir alles schon." Alawin wirkte nicht ungeduldig.

"Ich glaube, ich würde es nicht drauf anlegen.", sagte Lenja. "Du
hast schon recht, so sehr sollten wir nicht den Hammer, du weißt schon. Wir
sollten nicht übertreiben. Ich fand die Idee witzig, das abgesagte
Ritual doch auszuführen, weil es hier so viel Durcheinander gab
und ich immer ganz gern fassungslose Gesichter der Sakrallosen
sehe, vor allem, wenn sie zum Adel oder zur Monarchie gehören und
realisieren müssen, dass ihnen doch nicht die Welt gehört. Aber
das Risiko für allerlei Strafarbeiten ist ein bisschen arg hoch."

"Ja, das sehe ich genau so. Das waren erheiternde Gedanken, aber
wir sollten das nicht riskieren. Sakralet Henre ist noch
neu. Die Neuen neigen oft dazu, strenger zu sein.", pflichtete
Alawin bei. "Sollen wir trotzdem über Schleusen reden?"
