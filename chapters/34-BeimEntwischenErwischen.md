Beim Entwischen erwischen
=========================

*CN: Luftnot, tiefes Meer, Selbstabscheu, Blut - erwähnt, Nacktheit,
Fantasy-Religion - erwähnt, ab hier rückt Religion stärker in
den Fokus.*

Die Nacht brach herein und fühlte sich dabei viel mehr nach Nacht an als die
vorangegangenen Nächte. Vielleicht, weil sie voraussichtlich bei Dunkelheit
im Meer schwimmen würden, wenn alles gut ginge. Den Tag über hatte Lilið befürchtet,
irgendetwas könnte sich als schwieriger herausstellen als geplant. Zum
Beispiel, dass die Wache, die Lajana immer in den Raum ließ, doch nicht
Dienst hätte oder weniger dösen könnte. Oder dass Drude aussteigen könnte,
weil die Abe nicht wieder auftauchte.

Aber als Lilið gerade eines der Navigationsinstrumente erhob, um die
ersten Abendsterne anzupeilen, entdeckte sie Lil am Himmel. Die Instrumente würde Lilið auch
vermissen. Aber auf der anderen Seite: Sie war nun Nautika. Mit
einem gewissen Umfang an Erfahrung. Sie hätte durchaus eine
Chance, sich einen Ruf aufzubauen, und wenn es soweit wäre, würde sie
wieder ähnliche Instrumente in die Hand bekommen. Oder auch, wenn sie
auf weniger offiziellem Wege irgendwo angeheuert würde als jetzt.

Die Abe jedenfalls legte den letzten Abstand zwischen sich und
der Kagutte Abschnitteweise schwebend zurück: Immer wieder
flatterte sie ein wenig, bis sie eine gute Stelle gegen den
Wind fand, um wieder zu gleiten. Lilið vermutete, dass dey das
zur Entspannung tat. Denn hätte die Abe das den ganzen Flug hindurch
so gehalten, wäre dey viel zu langsam für die Gesamtstrecke
gewesen.

Lilið wandte sich um, um nach Drude Ausschau zu halten und demm im
Zweifel zu holen, aber das war nicht notwendig. Drude schritt in jenem
dünnen Mantel, in dem Lilið demm zum ersten Mal gesehen hatte,
den Niedergang hinauf. Der Wind griff in den Mantel und bauschte
ihn. Drudes Schritte waren schwer und mächtig. Dey brauchte den stabilen
Stand auch, weil die Abe sich nicht die Mühe gab, abzubremsen,
bevor dey gegen Drudes großen Körper prallte. Drude packte der Abe
in den Nacken, legte den anderen Arm sanft um demm und verschwand
mit dem Drachen irgendwo im Unterdeck. Lilið hatte Drude nicht lächeln
sehen. Hätte sie demm nicht schon ein wenig gekannt, hätte sie sich vielleicht
Sorgen gemacht. Nun, ein wenig Sorgen machte sie sich doch: Bis
zur Schlafenzeit bekam sie beide nicht wieder zu Gesicht.

Lilið hinterließ den Kartenraum, wie sie es zu jedem anderen Tag
auch getan hatte. Sie widerstand dem Drang, irgendetwas zu
stehlen, und ging einfach zu Bett, wie jeden Abend um diese Zeit. Und
als sie sicher war, dass alle anderen im Raum schliefen, oder
zumindest eher auf die Idee kommen würden, dass sie sich noch einmal
erleichterte, als von längerer Abwesehenheit auszugehen, stand
sie wieder auf, stopfte die Decken rasch so, dass sie eine
Silhouette von ihr darstellen könnten und schlich sich zu Drudes
Unterwassertür, wo sie sich treffen wollten.

Drude lehnte in der Dunkelheit an der Wand, das Gesicht im Schatten
und auf der Schulter die Abe. Es war ein unheimliches Bild. Lilið
wollte vertrauen, aber irgendwo in ihr fragte sie sich schon, ob
sie nicht doch irgendwie einen Plan B hätte finden sollen. Das hatte
sie nicht geschafft. Sie lehnte sich Drude gegenüber an die Wand, sodass
zwischen ihnen vielleicht noch eine Person Platz gefunden hätte, wenn
sie sich gequetscht hätte.

"Fühlst du dich bereit?", fragte Drude leise.

"Ich habe schon Angst. Wann fühlt man sich denn schon bereit dazu,
eine lebensgefährliche Sache durchzuziehen?", erwiderte Lilið. "Egal.
Wie geht es Lil?" Ein Teil von ihr hoffte, dass die Abe irgendeine
Nachricht von Heelem zurückgebracht hätte. Aber die Abe war natürlich bei
König Sper untersucht worden. Heelem konnte nichts unbemerkt mitgegeben haben.

"Erschöpft.", antwortete Drude. "Die Zustellungen waren erfolgreich. Soweit
ich demm verstanden habe, haben Leute an der ersten Adresse so etwas
gesagt, wie, dass Heelem am Abend wiederkäme und sie ihm den Brief dann
überreichen würden. Das macht Hoffnungen."

Lilið nickte. Sie fühlte besagte Hoffnung einen Augenblick wild
in sich aufsteigen und ihr die Atemwege blockieren. Es konnte immer noch
so viel schief gehen. Aber es klang, als wäre Heelem zumindest nicht
in der Weltgeschichte unterwegs und unerreichbar gewesen. "Sehr.", murmelte
sie.

"Lil ist nun wieder gut genährt und hat sich ausgeschlafen. Wir treffen
demm auf Lettloge.", erklärte Drude. "Ich schicke demm los, sobald du
sagst, dass wir uns zu Lajana aufmachen."

"Gibt es irgendeinen Grund, warum wir zögern sollten?", fragte Lilið.

Drude schüttelte den Kopf.

Lilið blickte demm noch ein paar Momente an. Es war ein merkwürdiges
Gefühl, ein Kommando dafür zu geben, dass es losgehen sollte. Es
fühlte sich albern an. Als wären sie nicht schon längst dabei. Auf
der anderen Seite realisierte sie dadurch erst so richtig, dass sie den größten
Teil der Idee und Organisation des Plans übernommen hatte. Das stimmte
sie nicht gerade zuversichtlich. Hatte sie genug Erfahrung für so
etwas?

Es spielte keine Rolle. Sie musste es halt so gut machen, wie es
ginge. Sie nickte. "Lasst uns aufbrechen.", flüsterte sie.

---

Lilið hatte das Gefühl, irgendetwas Wichtiges nicht zu beachten, als
sie sich zu Lajana in den sichersten Raum der Kagutte begab. Es
war inzwischen Routine für sie, sich zur Ratte gefaltet
um die Ecke zu schieben, der dösenden Wache ein wenig beim Atmen
zu lauschen und sich dann möglichst rattentypisch zur Tür zu
bewegen. Nichts passierte.

Lajana war spürbar nervös. Lilið hatte das Gefühl, dass die Nervosität
auffällig für Wachen sein musste, die gründlich waren. "Hast du
deinen Ritualen folgen können und nichts darüber hinaus erzählt?", fragte
sie.

Lajana zögerte und nickte dann. "Ich habe mich so wie immer
verhalten. Aber ich glaube, Marusch hätte spüren können, dass
etwas anders ist. Es tut mir leid."

Wenn Lilið nicht alles täuschte, hatte Lajana ein schlechtes
Gewissen, so zu sein, wie sie war. Und Angst, dass an ihr Pläne
scheitern könnten. "Ich frage gerade vor allem, um darein, wie
ich mich jetzt verhalte, einzuberechnen, was schief gelaufen
sein könnte." Lilið hoffte, dass sie dadurch wenigstens mögliche
Schuldgefühle beruhigen könnte, dass sie nicht wertete. "Du
machst Dinge so gut, wie du kannst. Das muss reichen."

"So etwas sagt Marusch auch immer.", murmelte Lajana so leise,
dass Lilið sie fast nicht verstand. "Ich habe trotzdem Angst,
dass alles meinetwegen nicht klappt."

Ob Drude ihr besser gut zureden könnte? "Ich verstehe die
Angst.", sagte Lilið. "Ich weiß nicht, was ich dir Hilfreiches
sagen kann. Außer, dass du nicht schuld bist und wir das beste
daraus machen werden, was wir jeweils können. Magst du dich falten
lassen?"

"Ich habe noch eine Frage, die ich mich gestern nicht getraut
habe.", sagte Lajana. "Kannst du auch das Igeldings falten?"

Das hätte sie gestern üben müssen, war Liliðs erster Gedanke. "Ich
probiere es. Aber wir haben nicht viel Zeit. Ich probiere es einige
Male und wenn es nicht klappt, geht es nicht."

Lilið fühlte sich unhöflich dabei, nicht zu fragen, bevor sie das
Igeldings wieder berührte. Sie wunderte, dass es dieses Mal nicht
zurückzuckte. Es bitzelte, wie beim ersten Mal. Wieder spürte
Lilið anschließend das Pulsieren. Sie fragte sich, ob es besser wäre, es
auszublenden, wenn sie in das Igeldings hineinfühlte, um es zu
falten, oder nicht, weil es eben zu dem Igeldings gehörte. Außerdem
wusste sie, dass sie nicht viel Zeit hatte.

Und dann bohrten da noch diese Fragen: Brauchte das Igeldings
vielleicht Luft zum Existieren? Wäre es ungünstig, wenn es mit
Wasser in Berührung käme? Oder reagierte es gar jetzt bereits
weniger, weil es eigentlich Nahrung bräuchte?

Lilið schloss die Augen und versuchte, sich in die Eingeweide
dieses Igeldings hineinzufühlen.

Eine Lunge hatte es nicht. Das Pulsieren hatte etwas mit Energie
zu tun, aber basierte nicht auf einem Stoffwechsel, der Lilið
vertraut gewesen wäre. Es war nicht aus Material, das sich auch
nur entfernt nach Igel angefühlt hätte. Die Stacheln hatten eher
etwas von Kompassnadeln. Lilið fühlte, wie auf die Stacheln eine ähnliche
Kraft wirkte wie auf Eisennadeln, und auch, dass sich mit dem Pulsieren
vielleicht sogar der Einfluss des Magnetfelds darauf änderte.

Lilið hätte sich darin verloren, für Stunden, wenn ihr nicht
doch wieder eingefallen wäre, dass sie es eilig hatten. Die Frage war,
ob sie dieses Igeldings falten könnte. Eigentlich hätte Lilið
damit gerechnet, dass es ihr schwer fallen sollte, weil sie nie
einen Kompass gefaltet hatte und das Material nicht so oft in
der Natur oder in ihrer Umgebung vorkam. Aber aus Gründen, die
sich ihr in diesem Moment nicht erschlossen, fühlte sich das
Igeldings vertraut an, als hätte sie sich über Wochen intensiv
mit fast nichts anderem auseinandergesetzt.

Sie atmete tief ein und versetzte sich beim Ausatmen in einen Zustand,
tiefer Entspannung. Sie versuchte, das Pulsieren des Igeldings
nachzuahmen, in der Hoffnung, es zu beruhigen, bevor sie es
faltete. Sie fühlte sich innerlich angenehm leergefegt, als sie
die viele Stacheln in einer Kreisbewegung zu einer flachen, kleinen
Münze zusammenflocht. Es war, zumindest, wenn das Igeldings nicht
ungewöhnlich viel zappelte, eine sehr stabile Faltung. Sie würde
es als Ratte im Maul einklemmen können, oder konnte es
direkt im Körper einfalten. Sie entschied sich für ersteres, damit
Drude eine Ahnung bekommen könnte, was los wäre.

"Gehen wir?", fragte sie Lajana.

Lajana nickte. Ein Lächeln lag in ihrem ansonsten traurigen
Gesichtsausdruck. Sie legte sich hin, damit Lilið sich von vorn
mit dem Rücken gegen sie lehnen könnte. Das machte eine praktische
Faltung möglich, bei der Lilið hinterher Lajanas Gewicht überwiegend
auf dem Rücken tragen, aber Teile des Gewichts auch über
ihre Gliedmaßen gut in den Boden abgeben konnte.

Die Faltung lief gut, dachte Lilið, als sie Lajanas Gewicht
überraschend wenig auf ihrem eigenen Körper lasten spürte. Vielleicht
wogen sie zusammen nun nur etwa 100 Kilogramm. Oder noch weniger?

Lilið faltete den Kopf flach, um ihn unter der Tür
hindurchzubekommen, und sah nicht mehr genau, was sie tat, als sie
das zur Münze geformte Igeldings unter der Tür vor sich herschob. Nachdem
sie den Kopf wieder zu einem dreidimensionalen Rattenkopf gefaltet
hatte, blickte sie sich sofort nach der Wache um. Diese döste, wie
immer. Lilið atmete flach erleichtert ein und aus, fühlte dabei
Lajanas gegen ihren gefalteten Körper besonders intensiv. Er war
heiß. Das war nicht so gut. Wahrscheinlich hatte Lajana große
Angst.

Lilið nahm die Münze zwischen die Kiefer und krabbelte möglichst
leise und rattenartig zur Treppe. Zwei Stufen schleppte sie sich
hinauf, damit sie dem Blickfeld der Wache entkamen, dann
übergab sie an Drude. Drude hatte an die Wand gelehnt gewartet. Die
Abe war nicht mehr bei demm. Nun ging dey breitbeinig in die Hocke,
um die viel zu schwere Ratte rückenfreundlich zu heben. Drude hatte
es gestern mit ihnen geübt. An deren Bewegungen heute konnte Lilið
ablesen, dass sie es tatsächlich geschafft hatte, ihre Masse zu
reduzieren: Dey hob sie mit mehr Kraft an, als für sie notwendig
gewesen wäre. Lilið hatte nach wievor keine Ahnung, wie sie Masse
veränderte, und konnte es nicht zuverlässig wieder tun.

Drude trug die Ratte auf dem Arm, wie dey es vielleicht mit der
Abe getan hätte, und ging zügigen Schrittes zur Tür, die direkt
ins Wasser führte. Dey setzte sie dort ab und zog sich
wieder einfach nackt aus. Lilið
machte sich Gedanken, ob sich hier vielleicht deshalb fast nie
eine andere Person aufhielt als Drude, weil Drude so
freizügig mit Nacktheit umging. Auf der anderen Seite
war ein Flur mit nichts als einer Tür ins Meer am Ende
für die meisten Menschen auch einfach kein so praktischer
Aufenthaltsort.

Drude schnallte sich dieses Mal zwei Taschen
um. Eine für die Münze und die Ratte und eine für dere
Kleidung. Dey verschloss die Tasche mit der Ratte darin, aber
Lilið fühlte, wie dey sie weiterhin trug, damit der Stoff durch das
Gewicht nicht reißen würde. Und wie Lajanas Körper zu zittern
anfing. Lilið hatte alle Mühe damit, die Faltung aufrecht zu
erhalten, und bekam erst mit, dass sie draußen waren, als angenehme
Kühle ihren Körper umfing.

Sie hatten die Kagutte also erfolgreich zu dritt verlassen. Zu
viert, wenn das Igeldings mitgezählt werden konnte. Lilið kam
das alles zu einfach vor. Eine Kronprinzessin unter der Nase von
Wachen von König Sper wegzustehlen, sollte schwieriger sein, oder
nicht? Aber immerhin war eine der Wachen, wenn auch nicht direkt
König Sper untergeordnet, mit von der Partie. Drude war an Deck
zuständig dafür, Magie zu erfühlen, die nicht im Sinne der Crew
angewandt wurde, und hätte sie ihre Arbeit
getan, wäre Lilið nie so weit gekommen.

Lajana wurde immer unruhiger. Wahrscheinlich atmete sie nicht richtig. Es
kostete Lilið alles an Konzentration, sie gefaltet zu belassen. Sie
spürte außer Lajanas um sie gewickelten Körper noch, wie
die Wassermassen über die Tasche hinwegströmten,
in der sie waren, aber mehr an äußeren Reizen auch nicht.

Lilið fühlte die ganze Zeit eine dumpfe Angst in sich, aber
als sie schließlich merkte, dass ihr schwindelig wurde, weil ihr
selbst die Luft ausging, schlug sie plötzlich und unvermittelt
in Panik um. Im Normalfall hätte sie, um sich zu beruhigen, langsam
und bewusst geatmet, aber stattdessen atmete sie schnell und
flach die sauerstoffarmen Luftüberreste in der Tasche ein und hatte dabei
Angst, sie Lajana wegzuatmen. Sie versuchte mit den Pfoten
durch die Tasche gegen Drudes Körper zu drücken, aber war zu
orientierungslos, um zu wissen, ob sie wirklich die richtige
Richtung erwischte.

Lilið wusste später nicht mehr, wie sie es geschafft hatte, die
Faltung aufrecht zu erhalten, bis Drude die Tasche öffnete. Wasser
strömte in die Tasche und Lilið hätte fast damit gerechnet, dieses
einatmen zu müssen, aber Drude war schnell genug mit einer Flossenhand
darin, sie über Wasser zu heben. Das war eine Leistung: Immerhin
waren sie eine ziemlich schwere Ratte mit hoher Dichte, die entsprechend
keinen Auftrieb hatte.

Lilið entfaltete Lajana und sich. Das Igeldings verblieb gefaltet in der
Tasche, hoffte Lilið. Drude blickte sie abwechselnd an
und entschied sich dann, Lajana locker in einem Griff festzuhalten,
sodass sie zu Atem kommen und sich nicht selbst ums Schwimmen
kümmern musste. Lilið machte rasche Schwimmbewegungen, um sich
über Wasser zu halten. Es war eine ungemütlich Nacht. Am Abend
war es noch sternenklar aber schon windig gewesen. Nun war
der Himmel zugezogen. Es war
stockfinster. Nicht einmal der Mond leuchtete hell
genug, dass Lilið mehr als Umrisse ausmachen
konnte. Die Wellen waren nicht furchtbar hoch, aber garstig. Sie spülten Lilið immer
wieder salziges Wasser in den Mund, durch den sie zeitgleich
sehr rasch zu atmen versuchte. Es fühlte sich nach sehr viel
Zeit an, bis sie allmählich ruhiger wurde, und in all der Zeit
hatte sie Angst, von Drude wegzutreiben. Unbegründete Angst, denn
Drude blieb ruhig in ihrer Nähe und raunte Lajana beruhigende
Dinge zu, die Lilið nicht verstand.

Irgendwo in der Ferne entdeckte Lilið die Nachtlichter der Kagutte. Selbst
wenn sie von ihrer Abwesenheit schon mitbekommen haben sollten, würden
sie sie hier nicht sehen. Die Distanz wäre zu groß, selbst mit einem
guten Fernglas, irgendwo in der Ferne drei unbeleuchtete Köpfe
zwischen den Wellen auszumachen. Auf der anderen Seite in ähnlicher
Entfernung entdeckte Lilið die gelblich beleuchtete Hafenstadt
auf Lettloge. Lilið wusste irgendwo in ihrem Inneren, dass es
machbar war, dort zu dritt anzukommen. Aber sie konnte sich
beim besten Willen nicht vorstellen, sich
noch einmal mit Lajana in eine Ratte zu falten, das auch noch beim
Schwimmen, und die Distanz in die Tasche gepfercht zu
überstehen.

"Du kannst dich in beliebige Dinge falten, oder?", fragte Drude
sie. "Nicht nur in welche, die du schon einmal gesehen hast."

Lilið fühlte sich nicht in der Lage, aus der Puste, wie sie war,
die Frage vollständig oder präzise zu beantworten, aber
vielleicht war das auch gar nicht notwendig. Vielleicht reichten
Teilantworten. "Hattest du eine bestimmte Vorstellung?"

"Keine allzu konkrete.", widersprach Drude. "Ich dachte daran, dass
du so eine Art Gurt sein könntest, der auf meinem Rücken befestigt
ist, in dem Lajana sitzen könnte. Ich glaube, es könnte für sie
zu viel sein, wieder in eine Ratte gefaltet zu werden."

Lajana gab ein zustimmendes, schluchzendes Geräusch von sich. "Ich
will keine Last sein.", flüsterte sie außerdem laut.

"Wenn Drude sagt, dass wir keine Ratte sein müssen, dann müssen
wir das nicht.", sagte Lilið. "Ich bin ehrlich gesagt selbst
erleichtert darüber. Wir kriegen das hin."

Sie gab vor, zuversichtlicher zu sein, als sie war. Auf der
anderen Seite hatte sich gerade eine Angst von ihr aufgelöst: Sie
vertraute Drude. Dere Möglichkeiten, eine Falle zu stellen, wurden
einfach immer weniger und dey benahm sich sehr klar für sie
vorteilhaft.

Sie zitterte trotzdem vor Anspannung, als sie sich also nun im
Wasser das erste Mal nicht bloß für einen Gefahrenmoment
sondern für einen gewissen, längeren Zeitraum in eine Form faltete, die nicht
als Tarnung gedacht war. Es klappte auch nur so mäßig gut. Sie
mussten die Faltung auf dem Weg zum Land noch zweimal erneuern. Trotzdem
war es sehr viel besser als zuvor in der Tasche: Drude schwamm zwar den
größten Teil der Strecke unterwasser, aber in regelmäßigen, bald
vorhersehbaren Abständen, die zum Atmen für Lilið und
Lajana ausreichten, schoss sie durch die Wasseroberfläche, und
sprang wie ein Delphin über die Wellen. Was für eine enorme
Kraft dieser Körper hatte!

Obwohl sie alle sehr aus der Puste waren, als sie an Land ankamen, war
der erste Wunsch Lajanas, dass Lilið nach dem Igeldings schauen
und es entfalten sollte. Lilið wusste nicht, wie sie sich hätte
fühlen sollen, wenn es unterwegs aus der Tasche gefallen wäre, aber
das Igeldings hatte einen Stachel entfaltet und sich in der
Tasche festgepiekst. Ob das Absicht war, wusste Lilið nicht.

Sie waren also alle unbeschadet, bis auf ein Loch in besagter
Tasche, auf Lettloge angekommen.

Lilið sank erschöpft auf den Kiesstrand. Ihr war völlig gleich, dass der
nicht gerade viele Kriterien von Gemütlichkeit erfüllte. Die
Hafenstadt lag vielleicht eine gute halbe Stunde Fußweg
rechts von ihnen.

"Ich weiß nicht, ob der Plan so gut ist, sich nun auszuruhen.", gab
Drude zu bedenken. Entgegen derer Worte saß dey nackt, noch halb in
Fischform auf dem Kies und kam selbst erst langsam zu Atem.

"Ich habe den Eindruck, egal was wir tun, kann ich mich schon ausruhen,
bis du angezogen bist.", entgegnete Lilið.

Drude nickte zustimmend und fing auch gleich damit an, sich
anzuziehen. "Mir ist zu warm vom Schwimmen mit zu viel Gewicht. Aber
es muss wohl sein, wenn wir unter Leute gehen, da hast du wahrscheinlich
Recht."

"Ich habe keinen allzu genauen Plan ab jetzt.", sagte Lilið. "Ich
vermute, dass wir morgen Abend frühestens abgeholt werden. Und
bis dahin sollten wir irgendwie untertauchen, dachte ich. Wir
sollten uns gut verstecken."

"Zum Untertauchen brauchen wir ausreichend Lebensmittel.", ergänzte
Drude. "Ich habe so viele Marken eingepackt, wie ich unauffällig
mitnehmen konnte. Wir sollten sie nicht in der Hafenstadt, sondern in
der landesinneren Stadt eintauschen, weil in der Hafenstadt eher nach
uns gesucht wird. Wir sollten die Besorgungen spätestens
zum frühen Vormittag erledigt haben, weil wir auch dort
nicht lange haben werden, bis dort Ausschau nach Lajana gehalten
wird. Sobald auffliegt, dass sie weg ist, wird Lettloge als Fluchtort
naheliegen."

Lilið wurde mit einem Mal wieder sehr heiß, obwohl sie gerade noch
gefroren hatte. Drude hatte recht. Sie waren hier nicht sicher. "Wahrscheinlich
ist es auch sinnvoll, sich nicht vollkommen darauf zu verlassen, dass wir
abgeholt werden, und uns nach einer Möglichkeit umzusehen, ein Schiff zu
stehlen."

Drude blickte sie mit einem so bösen Gesichtsausdruck an, dass Lilið
es selbst im Dunkeln nicht anders deuten konnte. "Du hättest Mal ein
Sterbenswörtchen darüber verlieren können, dass dein Kontakt eventuell
nicht zuverlässig ist." Auch in derer Stimme schwang leise Wut
mit. "Ich stehe hinter dem Plan, aber ich wäre ab nun gern immer
in alles eingeweiht. Ist das klar?"

Lilið nickte. Sie schämte sich sofort. Sie merkte, wie sie eine Ausrede
suchte, weil sie sich selbst verstand, während sie sich zugleich
verabscheute. "Ich habe dir nicht vertraut.", gab sie schließlich
zu. "Es tut mir leid."

Drude betrachtete sie lange. Lilið wusste nicht, wieviel Drude
eigentlich in ihrem Gesicht erkennen konnte. Schließlich senkte
dey den Blick. "Es tut mir leid, dass ich sauer war. Dass du
mir in anderen Punkten vertraut hast, hat
verständlicher Weise nichts mit unserer jetzigen Lage zu tun.", sagte
dey und überraschte Lilið sehr damit. "Wegen des Machtgefälles
hattest du gar nicht die Möglichkeit, ein Vertrauen in mich aufzubauen. Das
kann ich nicht von dir erwarten. Ich hasse die Auswirkungen, aber ich
verstehe deine Entscheidung, mir nichts davon gesagt zu haben, dass
wir hier möglicherweise einen Plan B entwickeln müssen. Und ich
hasse stehlen."

"Wie praktisch, dass du zufällig mit einer Diebesperson unterwegs
bist.", murmelte Lilið.

"Äh!", mischte sich Lajana ins Gespräch ein. "Drude, du hast mich
gestohlen. Du warst immer nett zu mir und hast mich dann auch
befreit, aber du hast mich gestohlen. Ich finde, du solltest
Lilið nicht böse sein, wenn sie auch mal gestohlen hat."

Drude nickte. "Du hast schon recht.", murmelte dey. "Aber es geht
hier ja nicht darum, was Lilið getan hat, sondern was dey tun wird. Oh
Moment. Ist 'dey' für dich in Ordnung, Lilið?"

Lilið blickte mit gerunzelter Stirn in Drudes Richtung. Es hatte sich
gerade nicht falsch angefühlt. Aber das lag an Drude, glaubte sie. "Von
dir schon, denke ich.", sagte sie. "Weil du es neutral meinst und für
dich und Lil auch nimmst. Ich sage Bescheid, wenn ich das nicht mehr
mag."

"Ich kann das nicht mit dem 'dey', glaube ich.", sagte Lajana.

Drude zuckte mit den Schultern. "Dann nimmst du einfach weiter meinen
Namen und probierst es erst dann mit dem 'dey' aus, wenn du Lust hast
und dich mutig fühlst."

Lajana nickte. "Ich habe das im Gespräch mit dem Igeldings probiert. Ich
stolpere immer und fühle mich dann mies. Aber ich übe weiter."

"Kein Ding.", meinte Drude. "Jedenfalls geht es gerade um die Frage,
ob wir von irgendwem ein Boot stehlen. Ein ganzes Boot. Und wenn es
nicht mit einer Meute Wachen gesichert ist, ist es ein Boot von
Leuten, die in große Schwierigkeiten kommen, wenn es weg ist, und
die nichts Böses getan haben. Das gefällt mir nicht."

"Das gefällt mir auch nicht." Lajana wandte ihr Gesicht Lilið zu. "Würdest
du so etwas tun?"

Lilið seufzte. "Äußerst ungern. Aber wenn meine Wahl ist, dass zu tun, oder
du wirst zurückgestohlen oder Drude oder ich getötet, dann eben
eher ein Boot stehlen."

"Kannst du eigentlich Wasser aus Kleidung irgendwie rausfalten?", fragte
Drude und wechselte auf diese Art das Thema.

---

Ihre Kleidung war immer noch klamm, als sie sich zu dritt erst durch einen
Wald auf einen Wanderweg schlugen und dann dem Weg hinauf auf einen Berg
zur kleinen Stadt folgten. Das Igeldings saß auf Lajanas Schulter. Die
Abe war noch nicht aufgetaucht. Es war kein kurzer Weg und die Sonne war bereits
aufgegangen, als das Sadtmäuerchen in Sicht kam. Es war eher Zierde als
Wehr. Die Mauern waren gerade hoch genug, dass Lilið nicht einfach hinübergucken
konnte, und begrenzten vermutlich Hinterhöfe und Gärten oder bildeten
einen Sichtschutz. Es führten viele Tore hinein. Irgendwo in der Mitte
der kleinen Stadt ragten die Mauern einer Sakrale über die anderen Gebäude
hinweg. Lilið erinnerte sich daran, dass Religion im Königreich
Sper einen zentraleren und sichtbareren Stellenwert hatte als im
Königreich Stern, und sie nah an der Grenze waren.

Sie hielten auf einen der Torbögen zu, der auf ihrem Weg lag und nicht
wie ein kleiner Nebeneingang wirkte, aber unvermittelt hielt Drude
sie auf. Dey blieb einfach stehen und studierte Aushänge mit gerunzelter
Stirn, die an der Innenseite des Torbogens aufgehängt waren. Drude
musste wirklich gute Augen haben. Lilið erkannte von hier aus nicht
viel. Es gab ein Gesuch mit abgebildetem Gesicht und eine Menge
Buchstaben, die für sie zu weit weg waren.

"Der Blutige Master M.", sagte Drude. "Bist du das? Das Gesicht
sieht aus wie deines."

Lilið schluckte. "Ja, irgendwie schon.", gab sie zu. "Lange
Geschichte." Sie fühlte sich mit einem Mal wie gelähmt. Nun war also doch
ein Phantombild von ihr entstanden, und sie wurde bis an den Rand
des Königreichs gesucht. Wie war es dazu gekommen?

Drude holte ein kleines Fernglas aus derem Mantel und
betrachtete das Plakat mit zusammengekniffenen Augen. "Du sollst die
Kronprinzessin entführt und im Alleingang eine
Kriegskaterane versenkt haben.", gab Drude wieder, was dey gelesen
hatte, und kicherte lautlos. "Ich glaube dir aus guten Gründen, dass du
das nicht getan hast. Aber ich schließe, dass es nach unserer
Begegnung mit der Kriegskaterane außer dir doch noch andere Überlebende
gegeben hat, die von deiner Identität als Blutiger Master
M wussten. Klingt das für dich realistisch?"

Lilið nickte. "Ich wurde dort identifiziert. Damals hat noch kein
Bild von mir existiert, aber sie konnten es über eine Blutprobe
tun.", erinnerte sie sich. "Das
ist der Grund, warum mir nicht geglaubt wurde, als ich sie vor euch
gewarnt hatte."

"Ungünstig.", meinte Drude und fügte eindringlich hinzu: "Du bist
trotzdem nicht schuld." Dann wirkte sie wieder nachdenklich. "Du
sollst außerdem einen Teil des Schatzes der Monarchie gestohlen
haben. Insgesamt hast du also einen sehr beachtlichen und
ziemlich üblen Ruf."

"Den Anteil des Schatzes habe ich versucht, zurückzugeben.", murmelte Lilið. "Ich
weiß nicht, ob das geklappt hat. Das war kurz bevor ich zum ersten
Mal bei euch an Bord gekommen bin." Immerhin ergab nun für Lilið
Sinn, woher sie ein Phantombild hatten: Je nachdem, wer überlebt
hatte, hatten sie etwas frischeres Blut von ihr gehabt, oder sie war einfach
gesehen worden. Letzteres konnte für ein Phantombild ausreichen. "Aber
warum glauben sie, ich hätte die Katerane versenkt? Ich war dort
eingekerkert!"

"Wurdest du von einer Wache bewacht, die nichts vom Geschehen
mitbekommen hat", fragte Drude.

Lilið nickte zögerlich. "Ja, schon. Sie ist zumindest erst spät
an Deck gelaufen. Aber die Wache hat mich
auch bewacht und weiß, dass ich zu dem Zeitpunkt nicht an Deck war."

"Was bei berüchtigten Kriminellen nichts heißt. Gute Überfälle
zeichnen sich dadurch aus, dass den Verbrechenden nichts
nachgewiesen werden kann." Drude blickte sich zu ihr um
und kicherte noch einmal. "Eigentlich ist das
überhaupt nicht lustig für uns.", sagte dey. "Du solltest dich
dringend tarnen. Falten oder so, wenn du kannst. Auch den Mantel
des Nautikas. Vielleicht faltest du ihn in etwas, was Lajana tragen
kann, damit sie nicht in so einem edlen Kleid gesehen wird. Aber
ich finde schon ganz schön absurd, ausversehen mit der meistgesuchten und
angeblich gefährlichsten Diebesperson der Monarchie unterwegs
zu sein, und hätte auch dafür gern eine Vorwarnung gehabt. Gibt es
noch etwas, was ich wissen sollte?"

Dieses Mal klang Drude nicht sauer. Lilið dachte darüber nach, was
für Drude noch wichtiges Wissen sein könnte und zog dabei den Mantel
aus. Dass ihr Umfeld insgesamt eher kriminell war? Aber an sich wusste
Drude das schon. Über das Buch? Aber darüber musste Drude nicht mehr
wissen, als dass es ein Teil des Schatzes der Monarchie war. Oder?

Lilið war gerade damit beschäftigt, den Mantel zu falten, als Drude
sie am Arm packte. "Entfalte wieder und zieh ihn an.", sagte
dey. Dere Stimme hatte etwas Unnachgiebiges, obwohl dey leise
gesprochen hatte. "Wir müssen vom Stadttor weg, sonst stirbst
du. Wir gehen in Richtung Hafenstadt."

"Was?", fragte Lilið.

"Wir können nicht entkommen.", raunte Drude, gerade so laut, dass
Lajana und sie sie hören konnten. "Uns kommt die Wache Schäler
entgegen. Das ist die, die auf der Kriegskaterane für das meiste
Blut gesorgt hat und sie schreckt vor nichts zurück. Und leider
wird sie begleitet von einer Person, die ich nicht kenne, aber von
der ich wahrnehme, dass sie Magie nicht nur spüren, sondern auch
auflösen kann. Wenn du jetzt etwas faltest, dann wird das enttarnt
werden. Wenn sie nicht bis zur Stadt kommen, gibt es eine gewisse
Chance, dass sie noch nicht wissen, dass du der Blutige Master M
bist und uns einfach wieder an Bord bringen. Und zum Weglaufen sind
wir nicht schnell genug. Wache Schäler ist schnell."

Lajana fing an zu weinen und Lilið konnte das gut nachfühlen. Sie
fühlte sich, als hätten ihre Adern beschlossen, ihr
Blut in Lawa umzuwandeln. Es konnte von Drudes Seite
keine Falle sein, überlegte sie. Das ergab keinen Sinn. Aber
hatten sie wirklich keinen Ausweg?

"Es hat keinen Sinn.", sagte Drude sachlich, als hätte dey
Liliðs Gedanken gelesen. "Ich kenne Wache Schäler, ich habe
viel zu viel Zeit mit demm verbraucht. Ich weiß, warum
dey ein Antimaga dabei hat und was der Plan ist und wie viel
zu dicht wir sind, um zu entkommen."

"Ich vertraue dir.", flüsterte Lilið. "Ich will nicht, aber ich
tue es."

"Gut.", sagte Drude. "Lajana, ich tue so, als hätte ich dich wieder
eingefangen. Du darfst weinen und schreien, aber am besten du verweigerst
jegliche Antwort auf Fragen."

Lajana nickte. "Das habe ich ja auch gemacht, als ihr wolltet, dass ich
Verträge unterschreibe.", wimmerte sie.

"Genau! Das hast du damals sehr gut gemacht. Genau so!", forderte
Drude. "Und du, Lilið, dich habe ich mitgenommen,
weil du gerade greifbar warst, um mich zur Kagutte zurückzubringen."

Lilið nickte. "Muss ich irgendetwas behaupten?"

Drude schüttelte den Kopf. "Nur bestätigen, was ich sage. Und nun, weg
von der Stadt."

Drude klemmte unsanft Lajanas Arm ein und zog sie mit sich. Bei
genauerem Hinsehen bemerkte Lilið, dass es vielleicht brutaler wirkte,
als es war. Oder sie hoffte es einfach. Sie trottete den beiden hinterher. Sie
ließen die Stadtmauer hinter sich. Bald verdeckte eine Landschaft
aus schwer durchdringlichen Dornbüschen die Sicht darauf. Ja,
wenn sie da hindurchgeflohen wären, hätten sie Spuren hinterlassen und
wären zudem sehr langsam gewesen. Trotzdem
dachte Lilið noch über mögliche Fluchtwege nach, bis der Weg einen
Bogen nahm und sie einem Paar aus zwei Menschen gegenüberstanden, beide eigentlich
relativ schmal, klein und unscheinbar. Drude machte da eine viel beeindruckendere
Figur.

"Wache Schäler!", rief Drude zum Gruß.

"Wache Drude!", grüßte eine der Personen zurück. "Kannst du uns erzählen,
was los ist?"

Drude nickte einmal. "Nicht genau.", sagte dey. "Ich bin nachts, nicht
lange vor Sonnenaufgang, aufgewacht und habe gemerkt, dass ich die Anwesenheit
der Kronprinzessin nicht mehr dort wahrnehme, wo sie hingehört. Ich wollte es
dem Kapitän melden, aber auf halbem Weg habe ich sie immer schwächer wahrgenommen
und gemerkt, wie sie mir entgleitet. Ich habe mir das Nautika geschnappt, weil
er gerade in der Nähe war, und bin dem Gespür hinterher. Wie sie abgehauen
ist, weiß ich nicht, aber ich habe sie hier gefunden und wollte mich gerade
wieder auf den Weg zurück zur Kagutte machen."

"Und das Nautika brauchtest du wofür?" Wache Schäler warf einen skeptischen
Blick auf Lilið.

Lilið blickte zu Boden und blieb unbewegt stehen.

"Ich habe viel mit ihm zu tun.", erklärte Drude. "Ich wusste von ihm, wie
schnell sich die Kagutte von hier fortbewegen würde und mir war wohler,
eine Person dabei zu haben, die mir dann den Weg zurück zeigen kann, weil
sie weiß, wo nach Plan die Kagutte zu welchem Zeitpunkt sein wird. Wäre er nicht
in der Nähe gewesen, hätte ich es nicht getan, aber so war es sicherer."

"Schlau!", kommentierte Wache Schäler.

Lilið konnte nicht gut einschätzen, ob Wache Schäler Drude glauben schenkte oder
nicht. Lilið fühlte sich miserabel. Wieso ging ein Plan auf diese Art
schief? Es fühlte sich surreal an, absichtlich den ihnen schaden
wollenden Personen in die Arme zu laufen. Aber doch traute sie Drude.

Würde es eine weitere Fluchtmöglichkeit geben?

"Es ist gut, dass du ihnen hinterher bist." Wache Schäler
klang freundlich, wie zu einem Pläuschchen aufgelegt. "Der Kapitän hat
sich Sorgen gemacht, dass du überlastet wärest und deshalb deinen
Aufgaben nicht mehr so gut nachkommst. Da wird er froh sein,
dass du das Maleur unterbinden konntest. Fraglich ist natürlich, wer
Ursache dafür war. Wir haben eine Wache in Verdacht, die zugegeben
hat, die Kronprinzessin aus der Zelle gelassen zu haben, aber
sie gibt weiter nichts zu. Ich hätte ja außerdem das
Nautika in Verdacht, aber das hast
du übergründlich bewacht." Wache Schäler warf Lilið einen weiteren
Blick mit gerunzelter Stirn zu. Dann blickte sie zurück zu Drude
und deutete auf die Person neben sich. "Jedenfalls ist gut, dass wir nun ein
Antimaga in der Crew haben. Das ist Wache Luanda."

Wache Luanda lüpfte eine Schirmmütze, wie sie typisch für Matrosen war, außer
dass auf diese ein Sakralzeichen gestickt war.
