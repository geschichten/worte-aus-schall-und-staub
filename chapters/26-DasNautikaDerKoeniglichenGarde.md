Das Nautika der königlichen Garde
=================================

*CN: Apathie, Gore/Splatter - erwähnt, Fesseln, Morde erwähnt,
Selbstzerstörungswunsch, Grenzkontrollen, Schuldgefühle.*

"Zertifikat?"

Das war die Stimme des Kapitäns, die sie schon bei ihrer ersten
Begegnung im nederoger Handelshafen kennen gelernt hatte. Er war
eine ruhige, durchdachte Person, hatte Lilið den Eindruck. Sie
fühlte sich immer noch losgelöst vom Weltlichen, dumpf und irgendwo
darunter befand sich eine Art Wut, die sie nie zuvor gefühlt
hatte, für die sie keine Worte hatte, nicht einmal Taten gehabt
hätte. Mit der konnte sie ohnehin nicht arbeiten. Also war ihr
Sein in eine Art Funktionsmodus übergeglitten, das wie ein mechanisches
Gerüst die Wut unter sich einklemmte und nicht weiter beachtete.

Sie griff sehr langsam in die Tasche des Mantels, in der das
Zertifikat steckte. Sie tat es deshalb langsam, weil Langsamkeit für
eine Person in ihrer Situation nachvollziehbar wirken musste und
sie auf diese Art Zeit hätte, es lange zu berührern.

Die Leute hier an Bord hatten bisher nicht den Verdacht geäußert,
dass sie Lilið von Lord Lurch sein könnte. Lilið hoffte, dass sich
niemand während der kurzen Begegnung im Halbdunkel beim Treffpunkt im
nederoger Handelshafen so schnell ihr Gesicht hatte merken können. Außer
Drude. Dort war aber ihr Name gefallen. Wenn sie herausfinden würden,
dass das Nautika, das sie aus den schwimmenden Überresten der
Kriegskaterane gefischt hatten, das gleiche war wie jenes, das sie
belauscht hatte, fürchtete Lilið größere Probleme.

Sie berührte das Material des Papiers mit den Fingerspitzen und
fühlte hinein. Sie war wach genug, um die eingesogene Tinte
darin mit ihren Sinnen zu ertasten. Sie fühlte ihren Namen und
verschob in einer komplexen Zwischenpapierfaltung die Tintenpartikel so, dass
sie einen neuen ergaben, zog das Zertifikat noch während des Prozesses
langsam heraus und legte es vor dem Kapitän ab. Es waren interessante
Vorteile, die ein Buch mit den Fingern zu kopieren so mit sich
gebracht hatten. Übung im präzisen Erfühlen von Partikeln in Papier.

"Aurin.", sagte der Kapitän, den neuen Namen korrekt ablesend.

Lilið ließ zwei Finger auf dem Zertifikat liegen, um im Zweifel, wenn
es doch nicht stabil genug wäre, die komplexe Faltung zu
halten, als der Kapitän selbiges und ihr Handgelenk
berührte, um die Eichung zu überprüfen. Dann packte sie es
genauso langsam wieder ein.

"Aurin aus?", fragte der Kapitän.

"Nederoge." Dort kannte sie sich einfach am besten aus. Wenn sie über den Ort
ihres Großwerdens erzählen sollte, über die Segelschulen und so weiter, dann
wäre es am besten, wenn sie mit jenen auch wirklich vertraut war.

"Adel?", fragte der Kapitän.

Lilið schüttelte den Kopf.

"Wieso stellt die königliche Garde ein Nautika aus Nederoge ein, das
nicht einmal von Adel ist? Ist an dir etwas Besonderes?", fragte der Kapitän.

Lilið war erst jetzt fertig mit dem Prozess, die Tasche, in die sie ihr
Zertifikat sortiert und dabei wieder entfaltet hatte, zu verschließen. Sie
mochte die langsamen, konzentrierten Bewegungen. Den Blick auf den
Kapitän gewandt schüttelte sie den Kopf. "Die
Garde hatte ursprünglich ein anderes Nautika.", berichtete Lilið. "Ich
bin nicht über Details aufgeklärt worden, sondern
habe nur mitbekommen, dass jenes Nautika
sich unter Vorgabe einer falschen Identität woanders anheuern hat lassen, was wohl
im Sinne der Garde war, und woraus sich die Notwendigkeit für letztere ergab, dass
sie ein neues brauchten." Sie ließ die Geschichte absichtlich so vage,
dass der Kapitän Schlüsse ziehen konnte, wenn sie passten, aber
auch andere zulässig wären, wenn sie nicht passten. "Die Nautikae vor Ort in Nederoge
haben mich empfohlen, weil ich mich in den nederoger Gewässern
exzellent auskenne und gerade für einen längeren, auch gern unkalkulierbaren
Zeitraum entbehrlich war. Dass Nautikae meist auf Adel pfeifen, weißt du
sicher."

Der Kapitän nickte und lächelte dabei. "Das ist mir nicht unbekannt.", sagte
er. "Nun, was die Geschichte mit der falschen Identität deines Vorgängers
angeht, kann ich wohl über mehr Hintergründe aufklären. Wir
haben ihn hier kennen lernen dürfen, denke ich. Und als aufflog, dass
er uns nicht an unser Ziel sondern in eine Falle navigieren wollte, sind wir mit
ihm nur unwesentlich glimpflicher umgegangen als mit dem Rest der Crew der
Kriegskaterane, abgesehen von dir. Das heißt, er lebt höchstwahrscheinlich
nicht mehr, aber war zum Todeszeitpunkt noch am Stück. Wenn du
ein Interesse hast, ganz und am Leben zu bleiben, navigier uns also
besser sinnvoll."

Lilið nickte. Sie hatte bereits mitbekommen, dass sie wohl die einzige überlebende
Person der Kriegskaterane war. Sie wusste nicht, ob die beiden Menschen
im Ruderboot, bevor sie sie eingesammelt hatten, noch andere Menschen gefunden
und getötet hatten. Wenn, dann war es jeweils sehr schnell gegangen. Oder
ob, nachdem sie sie eingesammelt hatten, noch einmal gesucht
worden war. Es konnte auch sein, weil sie ja schließlich aus dem sinkenden
Kriegsschiff in die fast abgeschlossene Zerstörung
hinaufgestiegen war, dass zu dem Zeitpunkt bereits alle anderen
tot gewesen waren. Sie wusste es eben nicht. Sie hatte die Information,
dass sie als einziges überlebt hatte, aus einem Gespräch, das sie überhört
hatte, während sie an den Mast gefesselt darauf gewartet hatte, dass
das Chaos abgearbeitet gewesen war und der Kapitän Zeit für sie
gefunden hatte. Nun saß sie ihm in seiner Kajüte ungefesselt gegenüber.

Sie dachte darüber nach, ob sie erklären sollte, dass ihr egal wäre, für wen sie
arbeitete, oder dass sie gar eigentlich lieber für diese
Staatsmeuterei arbeite als für die Monarchie, und für letztere
nur gearbeitet hätte, weil sie eben nicht groß eine Wahl gehabt hätte, wenn
Stellvertretende der Königin sie dazu aufforderten. Aber sie hielt den Zeitpunkt
für zu früh, so etwas zu behaupten. Nun galt es erst einmal, ein
bis zwei Tage eine Meisterleistung an Navigation hinzulegen, die
ein vermeintliches Vertrauen aufbaute, währenddessen Erkenntnisse zu
sammeln, Drude davon zu überzeugen, sie nicht zu verpfeifen, und
dann weiterzusehen. Und ja, wenn sie zwei Tage lang zielführend
navigierte, wären sie sicher aus dem Bereich der Reiseinseln raus,
in dem Marusch sie erwarten würde. Allerdings würde Marusch vielleicht
derzeit erwarten, dass die Kronprinzessin gerettet und auf dem Weg
zurück nach Nederoge wäre. Es sei denn, Marusch konnte überhaupt
nichts mehr erwarten, weil sie zum Zeitpunkt des Massakers
an Bord der Kriegskaterane gewesen wäre. Oder doch
das fremde Nautika gewesen wäre und in dem Zuge bereits gestorben. Folglich,
selbst wenn es eine Möglichkeit gegeben hätte, im verabredeten
Bereich zu bleiben, war fraglich, ob es überhaupt nützen würde.

Wenn sie aus dem Bereich raus wären, würde es schwer werden, sie
zu finden. Lilið könnte sie dann auf so viel Gewässer wahllos
herumirren lassen, -- zumindest könnte sie es dann versuchen --, dass
selbst die gesamte königliche Flotte beim systematischen Durchkämmen
des Ozeans Probleme bekommen würde, sie zu finden. Und selbst wenn
sie es täten, würde dann wieder das Problem auftreten, dass sie
auf ein einzelnes Kriegsschiff treffen würden, weil sich die Flotte
zum Durchkämmen ja aufteilen müsste.

Es nützte nichts. Sie nützte tot gar nichts. Wenn sie jetzt
schlecht navigierte, würde sie vermutlich noch maximal einen halben
Tag leben. Es müssten bereits weitere Schiffe der königlichen
Flotte unterwegs sein, damit dieser halbe Tag nicht völlig
verschwendet wäre, wenn sie sie sofort falsch
navigierte. Es war besser zu versuchen, Vertrauen zu
gewinnen und an einem anderen Plan zu arbeiten. Immerhin gab es
einen Postdrachen an Bord. Und die Kronprinzessin selbst. Wenn
sie sich Vertrauen erarbeitete, indem sie erst einmal tat, was
von ihr verlangt wäre, war das zwar immer noch keine gute
Ausgangsposition, aber vielleicht die beste, die sie kriegen
konnte.

Sie musste wieder an Marusch denken, an das Bild mit den
Pfaden. Sie musste außerdem an Marusch denken, weil in ihr drin
alles dumpf war, bis auf diese sehr merkwürdige Wut, die
sie aber behütet in sich drin ließ. Sie musste an Marusch
denken, weil sie sich vor allem
funktional fühlte. Und so, als würde sie sich nie wieder
wirklich freuen können. Fühlte sich Marusch so? Hatte Marusch
so etwas erlebt? Lilið erinnerte sich daran, dass Marusch davon
geredet hatte, selbst gemordet zu haben. Das hatte sie eigentlich
nicht, aber auf der anderen Seite vermutete sie, dass es nur
eine Frage der Zeit war, bis ihr Gewissen ihr das einreden
würde, weil sie eigentlich die Aufgabe gehabt hätte, das
Gemetzel zu vermeiden, und an jener gescheitert war.

Der Kapitän stellte ihr noch ein paar Fragen, die auf Lilið
wie Fangfragen wirkten, um zu testen, ob sie die Wahrheit
spräche. Lilið hatte keine Schwierigkeiten, sie zu beantworten. Dazu
hatte sie die Grundsteine zu gut gelegt. Es fiel ihr nicht
schwer, diese Rolle zu spielen. Oder vielleicht half es ihr
sogar in ihrer Situation, eine Rolle zu spielen. Sie war jetzt eine andere
Person.

Trotzdem würde sie den Namen Lilið in sich hüten.

Nach dem Gespräch schickte der Kapitän sie in den Speisesaal zum
Essen. Es war inzwischen früher Nachmittag. Lilið wunderte ein wenig,
dass sie nicht sofort ans Navigieren gesetzt wurde, aber der Kapitän
argumentierte, dass sie ohnehin noch für ein oder zwei Stündchen
vor Anker liegen würden. Ihre Segel waren zerrissen worden. Es
gab Reparaturarbeiten, bevor es weitergehen würde. Und auch der
Rest der Crew hatte nach dem Angriff Hunger. Sie würden gemeinsam
speisen und dann erst würde Lilið unter Aufsicht des Kapitäns und
eines Matrosens, der Grundkenntnisse der Nautik hatte, navigieren.

Lilið hätte nicht damit gerechnet, dass sie in dieser Stimmung
irgendetwas beunruhigen könnte. Sie hätte vielleicht einen
zweiten vergeblichen Angriff der königlichen Flotte einfach
hingenommen, ohne zu fühlen. Es war fast albern, dass es dagegen
reichte, an ihrem toten Gefühlsgefüge zu rütteln, dass Drude
sie während des Essens unentwegt anstarrte, mit einem Gesichtsausdruck,
den Lilið als anklagend oder wütend deutete. Deuten war bei Drude
schwer, das wusste sie. Aber sie merkte auch, dass sie nicht
anders konnte, weil ihr Unterbewusstsein verstand, dass Drude
sie nun hassen musste. Oder mehr als das. Dass Drude auch so
eine merkwürdige, alles zerstören wollende Wut in ihrem Inneren
hatte, die sich aber vollständig und zurecht gegen Lilið
richtete. Lilið erinnerte sich an den Blick, den sie den beiden
Menschen zugeworfen hatte, die sie aus dem Wasser gefischt
hatten. Wenn Drudes Blick genau so einer war, dann wollte Lilið
diesen Schmerz fühlen. Und vielleicht darunter zu Asche zerfallen, sich
vollständig auflösen.

Es war interessant, wie das kein negativer Gedanke war. Nicht
einmal, weil sie glaubte, so etwas verdient zu haben. Sondern weil
sie dadurch für die Linderung von Schmerz bei Drude dienen könnte,
während sie sonst keinen Sinn hatte. Und weil es zwar merkwürdiges,
aber immerhin vorhandenes Mitgefühl war, was sie da wahrnahm, und
es sie erleichterte, doch etwas zu fühlen.

Trotzdem war sie froh, als sie sich nach dem Essen in den Raum
mit den Karten begab. Das Essen war wieder ausgezeichnet gewesen. Lilið
fragte sich, ob die Prinzessin auch so gutes Essen bekam, und wann und
wo.

Es waren auch ausgezeichnete Karten, die sie vorfand. Es war ein angenehmer Raum zum
Navigieren. Er lag hinten im Heck, weit oben in der Kagutte. Es gab
ein Heckfenster, durch das Tageslicht hereinflutete. Die Wolken, die
über den Himmel zogen, ließen ihn heller und dunkler werden, aber
das störte Lilið nicht.

Sie schaffte es, den Kapitän zu ignorieren, der mit verschränkten
Armen neben ihr stand. Den Matrosen band sie ein. Sie versuchte,
unauffällig herauszufinden, welchen Kenntnisstand er hatte. Sie
stellte ihm Fragen in einem Stil, der vielleicht zu Heelem passte. Auf
eine Weise, dass klar war, dass sie die Antwort wusste, um ihm
die Wahl zu geben, seine Gedanken durch sie kontrollieren zu
lassen. Sie durfte die Taktik nicht zu viel nutzen, aber in einem
gewissen Rahmen war es wahrscheinlich für fortgeschrittene Nautika
üblich, die sich eben auch mit Ausbildung befassten.

Sie navigierte, bis die Sonne allmählich unterging, und hatte gerade rechtzeitig zum
Ankerlichten herausgefunden, welcher Kurs ab jetzt zunächst am meisten
Sinn ergab. Sie hatte allerlei Alternativrouten in petto. Die Reiseplanung
für die Kagutte unterschied sich von der für die Ormorane: Auf der
einen Seite fuhr die Kagutte die Nächte durch, auf der anderen
Seite hatte sie mehr Tiefgang und konnte zwischen manchen Reiseinseln
nicht hindurchsegeln, zwischen denen der Grund zu flach war. Es
war ungewohnt für Lilið, aber sie glaubte, durch ihre Fragen an
den Matrosen und ihre immer noch mitschwingende Lethargie genügend
kaschiert zu haben, dass sie nicht völlig routiniert in solchen
Aufgaben war.

Sie wusste am Ende immer noch nicht, wie der Matrose oder der Kapitän
hießen, sie hatte es nach der Vorstellung einfach direkt wieder
vergessen. Aber als sie den Raum wieder verließ, fühlte sie ein
völlig unerwartetes zweites Gefühl aufflammen: Eine gewisse Freude,
weil sie gemerkt hatte, dass der Matrose sich bei ihrer Lehre
wohlgefühlt hatte. Dass sie es hinbekommen hatte, eine Lernsituation
zu ermöglichen, in der er mutig genug gewesen war, selbst Fragen
zu stellen und gewagte Gedanken zu äußern, um darüber zu
reflektieren.

Natürlich war es nicht damit getan, dass sie über ein paar
Stunden hinweg Routen geplant hatte. Sie musste auch regelmäßig
mit Messgeräten kontrollieren, ob sie den Kurs auch fuhren. Solche
hatte sie zuvor noch nie länger in der Hand gehabt und kannte deren
Funktionsweise bisher nur aus Büchern. Sie befasste sich mit jenen
nun in der Abenddämmerung, während noch Chaos herrschte, obwohl sie
allmählich sehr müde war.

Und schließlich gehörte natürlich zum Navigieren, dass sie die
Position des Kartensteinchens immer wieder korrigierte, sowie
die Karte an die sich zeitlich ändernde Realität anpasste. Ihr wurde, weil sie
die Nächte durchfuhren, zweimal drei Stunden Schlaf zugestanden, in
denen der Matrose Kontrolle des Kurses und Korrektur des Steinchens
und der Karte übernahm. Er durfte sie nur wecken, wenn er mit
etwas unsicher wäre. Die Koje, die ihr gezeigt worden war, lag
nicht in einem Einzelraum und roch nach zu vielen Menschen. Aber
nach diesem Tag war sie einfach so müde, dass sie die anderen
Kojen nicht einmal zählte, als sie sich zum Schlafen einrollte.

---

Sie hatte den Eindruck, viel zu wenig geschlafen zu haben, als der Matrose
sie das erste Mal weckte, und dachte daher, er täte es wegen
so einer Unsicherheit seinerseits. Aber es waren einfach ihre
ersten drei Stunden rum. Sie hatte tief geschlafen. Die Matapher,
wie ein Stein, ergab für sie dabei eine neue Bedeutung, weil
ein Stein vielleicht auch nicht so viel fühlte. Es war gut
gewesen und eigentlich wollte sie nie wieder aufwachen. Aber
der Matrose schüttelte sie unbarmherzig. Sie lag auf dem Bauch
und realisierte gerade so rechtzeitig vorm Umdrehen, dass sie
ihre Brüste neu wegfalten musste, die noch von gestern
schmerzten, weil sie sich nicht getraut hatte, zwischendurch
loszulassen.

Sie hatte mit fünf anderen in einem kleinen Raum geschlafen,
von denen mindestens die Hälfte laut schnarchte. Ihr
Schlafplatz war eine Kiste mit einer Decke, die
der Matrose nun einfach übernahm, bis sie ihn in einer Stunde
wieder wecken durfte.

Lilið schritt an Deck in die kühle Nachtluft. Um ihn besser auf
der Haut zu fühlen, zog sie ihren Mantel aus, in dem sie sogar
geschlafen hatte, und hängte ihn sich über den Arm. Sie mochte
die Gänsehaut, die dabei entstand. Sie war nicht allein an
Deck. Sonst hätte sie den Moment vielleicht länger genossen. Diese
friedliche Ruhe eines gleichmäßigen Windes, der in die Segel
strich und diese mächtige, schwere Holzkonstruktion
vorantrieb, die die Kagutte war. Das dunkle Meer um sie herum mit seinen
Wellen, auf die der Mond eine schillernde Lichtspur warf. Der
Sternenhimmel, der ihr das Gefühl gab, dass diese Momentaufnahme
des Kosmos, die sie gerade belastete, vergänglich war und
vielleicht in den Weiten gar nicht so wichtig.

Lilið peilte mit Werkzeugen bestimmte Sterne an, machte
sich Notizen in ein Büchlein und ging mit den neuen Erkenntnissen
in den Kartenraum. Sie zündete einige Lampen an und besah
sich, was der Matrose ihr hinterlassen hatte. Ein Chaos, wie sie
feststellte. Entweder, er hatte eine Menge vergessen, was an der
Karte hätte angepasst werden müssen, oder er testete sie. Lilið
vermutete eine Mischung aus beidem. Denn wenn er gut genug für
diese Route mit der Kagutte hätte navigieren können, dann hätten
sie ja nicht so verzweifelt nach einem Nautika gesucht, dass sie
bereit wären, das vermeintliche Nautika der königlichen Garde
am Leben zu lassen. Aber es wäre auch eine verpasste Chance, sie
zu prüfen, wenn er nicht zusätzlich auch noch Fehler einbaute,
an denen er kontrollieren könnte, ob sie sie absichtlich übersehen
würde oder nicht.

Lilið hatte nicht vor, etwas absichtlich zu übersehen. Aber sie
hatte vor, genau zu beobachten und zu versuchen, seine Muster
zu erlernen. Sie traute sich zu, über ein paar Tage hinweg herauszufinden,
welche Fehler ihm warum passierten, welche Absicht wären und
welche eben nicht. Sie lächelte ein wenig, weil es ein kleiner
Hoffnungsschimmer war.

Das Navigieren beruhigte. Sie mochte die konzentrierte Arbeit, die
ein anderes Gedankenuniversum in ihr wachrief, in dem egal war, wer
warum gestorben wäre. In der sie ihre Angst um Marusch vergessen
konnte. Aber als sie fertig war, war ihre Stunde noch nicht rum. Und
ein weiteres Mal zu kontrollieren lohnte sich jetzt noch nicht. Sie sollte aber
auch nicht einschlafen, weil sie sonst vermutlich einfach
ausversehen die Nacht durchgeschlafen hätte.

Nachdenklich blickte sie Richtung Fensterfront, aber scheiterte
bei dem Versuch, in die Ferne zu sehen. Sie sah durch die Fenster
natürlich nichts, weil drinnen Licht leuchtete. Sie hatte gerade überlegt,
das Licht zu löschen, um ihre Augen mehr zu
entspannen, als ihr Blick in eine schattige Ecke des Raums
fiel, in der sie die Silhouette einer Person entdeckte. Trocken
registrierte Lilið an ihrer Körperreaktion, dass sie durchaus noch
im Stande war, sich zu erschrecken.

"Magst du noch mit mir reden?" Das war Drudes Stimme.

Lilið nickte. "Und du mit mir?"

Drude trat aus dem Schatten und an den Tisch heran. Die Abe trug
sie im Arm, aber die bekam davon wahrscheinlich nicht viel mit, weil
sie schlief. Drude legte sie auf dem Tisch ab, wo der Körper mit der
unwillkührlichen Weiche des Schlafs rasch eine gemütliche Position fand. "Ich würde
dir gern so etwas sagen wie, es ist nicht deine Schuld.", sagte sie. "Ich
weiß nicht einmal, ob du das brauchst. Ich hätte es gebraucht, als ich
das erste Mal versagt habe."

Lilið schluckte. Das war nicht, was sie erwartet hätte. Einen
Moment hatte sie das Bedürfnis, Drude in den Arm zu nehmen, weil
sie vom ersten Mal sprach, und davon, mit einem Gefühl allein
gewesen zu sein. Aber stattdessen fokussierte sie sich wieder
auf die erst kürzlich vergangenen Ereignisse. "Du weißt doch
nicht einmal, ob ich ihnen wirklich davon abgeraten habe, euch
anzugreifen, oder doch?", fragte sie.

"Das spielt keine Rolle bei der Frage nach der Schuld.", antwortete
Drude. "Ich kann mir eigentlich nicht vorstellen, dass du nicht alles
versucht hast, um Schlimmeres zu vermeiden, aber die Lage war folgende:
Die königliche Crew hatte das Ansinnen, uns anzugreifen und durchaus auch
zu töten. Die Crew hier, zu der ich gehöre, wehrt sich dagegen ebenfalls
mit entsprechenden Tötungsabsichten, die sicher auch über
Selbstverteidigung hinausgehen. Du kannst nichts für die
Tötungsabsichten. Solange du nicht gerade die eine Gruppe dazu anstachelst,
die andere zu töten, ist es nicht deine Schuld, wenn sie es tun. Wir beide
haben versucht, sie auszutricksen, sodass sie es nicht tun, aber
es ist immer noch deren Verantwortung, was sie tun, und nicht deine."

"Das ist eine interessante Sichtweise.", sagte Lilið. Sie überlegte,
ob Drudes Worte vielleicht tatsächlich helfen würden, sobald Schuldgefühle
drohten, einzusetzen. "Vor allem der Aspekt mit dem Austricksen. Damit
nehme ich Menschen gegebenenfalls sogar Verantwortung oder freie
Entscheidungsmöglichkeit aus der Hand." Lilið erinnerte sich, dass sie
das gar nicht getan hatte: Ihr Weg zwischen Wahrheit und Lüge war
so ausgelegt gewesen, dass die wesentlichen Informationen, auf deren Basis
die neuen Entscheidungen gefällt werden würden, der Realität
entsprochen hatten. Aber das wusste Drude ja nicht.

Drude nickte. "Hast du Schuldgefühle?"

"Noch nicht.", antwortete Lilið. "Ich fühle insgesamt sehr dumpf."

"Ich auch.", gab Drude zu. "Ich bin für das Sinken der Schiffe
verantwortlich. Eigentlich hielte ich für kein Problem, dass Überlebende
in einem Ruderboot zurückgelassen werden. Die brauchen dann immer noch
lange genug, bis ihnen vielleicht geholfen werden kann. Aber die Wachen des Königs
haben andere Verpflichtungen und nicht den Mut, sich darüber
hinwegzusetzen."

"Nicht den Mut, sich darüber hinwegzusetzen.", wiederholte Lilið leise.
Sie wusste nicht warum, vielleicht, weil es sie fassungslos machte,
-- und sie sprach es laut aus: "Dass es Mut kostet, Menschen nicht zu
töten. In welcher Welt leben wir?"

Drude warf ihr einen erneuten Blick der Sorte zu, die Lilið als
Schmerzen auslösen wollend las. "Ich verstehe es auch nicht, aber
ich glaube, du machst es dir zu einfach.", sagte sie. "Es stecken
so viele Konflikte da drin. Die Wut auf eure königliche Armee zum
Beispiel, die seit zwei Jahren Handel massiv erschwert, weil sie
alle, die durch euer Land reisen wollen, schlimm filzt, und das
alles nur, weil eure Königin Stern an der Macht bleibt, obwohl
sie nicht mehr darf, weil sie glaubt, etwas retten zu können, was
nicht zu retten ist."

Lilið runzelte die Stirn. "Ist die Entführung der Kronprinzessin nicht
genau das Ansinnen, etwas zu retten, was die Königin an sich auch
zu retten versucht, nur mit zwar vielleicht effektiveren aber
eben auch unmenschlicheren Mitteln?"

"Hm.", machte Drude und wirkte nachdenklich. "Ja, da war ich voreilig
in der Formulierung. Es ist etwas zu retten und sie versucht es, aber
ich persönlich finde ihre Wege unmenschlicher als unsere. Vor allem
eben, weil es absolut nicht absehbar ist, dass sie innerhalb der nächsten
Jahre zu einem Ziel führen würden. Und bis dahin wird halt auch gern
mal ein Bote samt Eskorte gemeuchelt, der für den König verhandeln
soll, weil sich dadurch Verhandlungen länger hinziehen und Königin
Stern so länger ausharren kann."

Lilið nickte. Davon bekam sie mit, auch wenn das in der Berichterstattung
immer verteidigt wurde. Damit, dass diese Eskorten unverschämt
wären, eine Bedrohung darstellten, oder sich an irgendwelche
Protokolle nicht hielten, was einer Beleidigung schlimmsten
Grades gleichkäme und so etwas. "Dann wiederum ist es eine vertrackte
Situation.", sagte sie. "Ich jedenfalls habe keine sonderliche
Lust auf eine Herrschaft durch König Sper."

"Es würde gerade für euer Königreich Rückschrittlichkeit in
Sachen Frauenrechte bedeuten, das sehe ich ein.", antwortete
Drude. "Ich habe den Unterschied erlebt. Er ist spürbar. Aber
definitiv nicht schlimmer als die Situation jetzt für uns, oder
eine Situation, die da kommen möge, würde die Kronprinzessin
einen lächerlichen Regierungsversuch starten, oder würde
der Kronprinz sich doch dazu herablassen. Den schätze ich nicht
unbedingt als weniger sexistisch ein. Ich denke, im Gegenteil:
es würde sich die Gesamtsituation verbessern, wenn König Sper
eure Monarchie übernimmt, auch wenn ich ihn hasse. In
dem Moment, wo er eure Monarchie übernimmt, kann er nämlich
durchaus durch Verträge dazu gezwungen werden, seinen
Frauenhass weniger politisch umsetzen zu können."

"Ich bin übrigens keine Frau.", informierte Lilið. "Auch wenn
mich das dadurch nicht weniger betreffen wird."

Drude zog sich einen Stuhl heran und nahm am Kartentisch platz. "Mir
ist aufgefallen, dass du deine Brüste wegfaltest.", sagte sie. "Ich
wusste nicht, ob das nur damit zu tun hat, dass dann der Mantel
besser passt, oder ob du semierfolgreich versucht hast, dich
als Mann auszugeben, aber für letzteres wirkte die Veränderung eigentlich
zu halbherzig. Also hatte ich mich bereits gefragt, ob es was mit
deinem Geschlecht zu tun haben könnte. Ich habe keine Ahnung, wie
du das ausgedrückt haben willst."

"Oh, ich werde hier nicht als Mann wahrgenommen?", fragte Lilið überrascht
und ein wenig enttäuscht. "Ich dachte, sie reden hier mit 'er' über mich."

"Das tun sie, weil sie deine Verkleidung akzeptieren. Die
meisten.", antwortete Drude. "Ich hoffe, es ist nicht zu schlimm für dich,
dass ich dir das sage. Jedenfalls wissen wir hier doch alle, dass es
so sexistische Katastrophen wie den Hut gibt und dass man da als
Nautika besser wegkommt, wenn man männlich wahrgenommen wird. Einige
an Bord lästern deshalb über dich, soweit ich das mitbekommen
habe, aber die meisten nehmen es halt als Versuch hin, weniger Gewalt
abzubekommen, und akzeptieren dich als Mann, glauben aber zu
wissen, dass du es nicht wärst." Drude holte tief Luft und
seufzte die Frage, die sie dann stellte, fast: "Bist du denn
ein Mann?"

Lilið schüttelte den Kopf. "Auch das nicht."

"Ist dir das Pronomen 'er' denn recht?", fragte Drude. "Das
heißt doch Pronomen, oder?"

"Ich würde in meiner Rolle hier gern 'er' behalten, aber sonst
ist mir eigentlich aus Gewohnheit 'sie' lieber.", informierte
Lilið. Hoffentlich machte sie es für Drude nicht zu kompliziert. Sie
fand das Gespräch interessant, weil es sich anfühlte, als würde
Drude überhaupt nicht werten und vorbereitet auf so ein Gespräch sein. Lilið
setzte sich auf der anderen Seite an den Kartentisch und hoffte, dass
es sich noch lohnend anfühlen würde. Bald müsste sie noch einmal
den Kurs kontrollieren und dann ihre zweiten drei Stunden schlafen.

"Das kriege ich hin." Drude seufzte abermals. "Ich bin eine Frau. Aber
ich finde so anstrengend, dass das in Gesprächen irgendwie automatisch
immer sofort mindestens implizit Thema ist. Durch
Pronomen, Anreden und Bezeichnungen. Ich habe
mich oft gefragt, ob ich nicht neutrale Begriffe für mich erfinden
möchte, die dann auch alle anderen natürlich benutzen dürfen, sodass
Leute mit mir und über mich ohne Geschlechtsbezug reden können."

Lilið lächelte unwillkührlich. "Das hört sich schön an!", sagte
sie. "Und? Hast du dir welche ausgedacht?"

"Ich glaube, von den Begriffen wie Transmagika hätte ich gern
einfach das 'das' davor statt das 'die'. Und statt Beobachterin
gern Beobachteran oder Beobachtungsfisch oder sowas.", sagte
sie.

"Oh, das trifft auf mich auch zu, ich möchte neutrale Begriffe
für mich.", fiel Lilið ein. "Wobei, Fisch bin ich ja nicht. Und
über so eine Form wie Beobachteran muss ich erst noch nachdenken,
das Konzept ist mir neu. Meine", Lilið zögerte. Waren sie befreundet?
Was war es? "Meine Beziehungsperson nutzt für mich dann zum Beispiel
etwas wie 'beobachtende Person'."

"Beziehungsperson ist auch ein schönes Wort." Drude
lächelte. "Beobachtungsperson wäre mir auch
recht." Dann war das Lächeln auch schon wieder vorbei. "Als
größte Schwierigkeit stellt sich für mich das Pronomen heraus. Ich
habe in dieser Sprache immer Mal wieder welche zu basteln versucht,
die sich natürlich anhören und nicht wie eine Mischung aus
beidem. In meiner Erstsprache gibt es ein Pronomen für unbekannte
Menschen, dass ich mal versucht habe, an diese Sprache anzugleichen. Das
wäre dann dey, dere, demm, demm. Also sowas wie: Dey ist an
Deck, um dere Aufgaben zu erfüllen, damit niemand demm ausschimpft
oder demm auf die Nerven geht. Aber ein ey fühlt sich unatürlich
in dieser Sprache an, und auch der dritte und vierte Fall mit
jeweils vier statt drei Buchstaben." Es huschte doch noch ein
weiteres Lächeln über Drudes Gesicht. "Ich bin Grammatik- und
Sprachbastelstreberan. Und texte Leute anscheinend zu, sobald
sie mich über so etwas reden lassen."

"Ich finde das sehr interessant! Mach das ruhig.", bat Lilið. "Ich
glaube, du könntest mir gerade keinen größeren Gefallen tun, als
mich über so etwas zuzutexten. Es fühlt sich nach dem Sinnvollsten
an, was ich gerade habe. Ich habe nur leider nicht mehr viel Zeit."

"Ich weiß.", sagte Drude. "Ich kenne deinen Zeitplan. Und eigentlich
soll ich dich beobachten und mich nicht mit dir unterhalten. Und
ich bin unbeschreiblich froh, dass du es zulässt, dass ich das
heimlich tue. Ich vertraue dir, dass du mich nicht verrätst."

"Wo wir beim Thema sind: Du scheinst mich nicht mehr verraten
zu wollen. Ist das richtig?", fragte Lilið.

"Nicht innerhalb der nächsten zwei Tage.", antwortete
Drude. "Es werden zwei Tage sein, in denen wir von deinen Nautikkünsten
profitieren können, ohne dass du uns Schaden kannst. Die Lage
ist für mich auch nicht mehr heikel, weil ich dich nicht
vor anderen verstecken muss. Du bist ja nun das Nautika der
königlichen Garde und nicht aus einem Würfel geschlüpft, den
ich hätte entdecken müssen."

Lilið kicherte, aber wurde direkt wieder ernst. "Was ist nach den zwei Tagen?"

"In zwei Tagen könntest du beim Kapitän ausreichendes Vertrauen in dich aufgebaut
haben, dass er glaubt, dass du tust, was er will.", antwortete Drude. "Aber
ich weiß, was ursprünglich dein Plan war, und gehe davon aus, dass
du wieder dahin zurückkehren wirst, sobald du kannst. Ich werde
mit dir also Gespräche führen, und es gibt drei Möglichkeiten. Entweder,
du überzeugst mich von deinen Ideen. Oder ich dich von meinen auf eine
Art, dass ich daran glaube, dass du wirklich die Seite wechselst. Oder
ich verrate dich, weil wir gegeneinander arbeiten."

Lilið spürte Hitze, die ihr durch den Körper wallte. Eine Panikreaktion?
Nun? Nach drei Tagen an Bord, wo in vielen Situationen eher eine
angebracht gewesen wäre. Sie nickte und stand auf. "Ich freue
mich trotzdem auf die Gespräche. Sie sind mir mit dir eine ehrliche
Freude.", versicherte sie. Und sie fühlte auch so. "Soll ich, bis
du ein besseres Pronomen gefunden hast, versuchen von dir als 'dey'
zu denken?"

Da war es, das dritte Lächeln, und dieses mal war es breit. "Gerne.", sagte
Drude. "'Sie' ist wirklich nicht schlimm, aber ich würde mich freuen,
wenn du 'dey' ausprobierst. Ich wüsste gern, wie es sich anfühlt."
