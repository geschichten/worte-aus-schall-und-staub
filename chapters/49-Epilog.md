Epilog
======

Lilið -- das war der Name einer dunklen Kreatur. War sie
schön und intelligent, skorsch und zielstrebig, und voll
Liebe? Vielleicht alles nicht. Vielleicht alles davon. Vielleicht
war es eine Frage der Perspektive. Und vielleicht sollte
die Welt daran arbeiten, diesen Eigenschaften weniger
Bedeutung beizumessen. Allen davon. Irgendwann. Wenn wir
lernen, dass Menschen für sich wertvoll sind und keinen
Ausgleich auf einem anderen Feld brauchen, wenn sie auf
dem einen nicht taugen. Nicht zu taugen, ist ein
shit Konzept to begin with.

Lilið brachte durch ihre bloße Existenz das Geschlechtsgefüge
durcheinander, doch das fühlte sich nicht nach aktiver
Handlung an und es war ihr ohnehin nicht genug. Sie hatte
in den Abgrund gesehen, der durchs nach unten treten
entstanden war, und war ohne Zögern sanft hinabgeglitten. Doch
was hatte sie dann getan? War sie ihrem Namen gerecht
geworden? War sie jene Kreatur, die so stark war, dass sie sich
dem Willen der Bestimmenden entgegenlehnte? Schwang sie nicht
bloß schallende Reden, aber fügte sich diesen nicht folgend in ihr
Schicksal, weil ihr die Macht fehlte, überließ das Stören anderen?
Wandelt Lilith wirklich unbemerkt, in vielen unerwarteten Verkleidungen
im irdischen Reich und bringt das Machtgefüge durcheinander? Oder
verschwindet sie nicht viel eher in der Versenkung der
Unterwelt wie ein Tropfen im Ozean?

Alles davon ist zugleich wahr. Die Unterwelt, unbemerkt, ist längst ein
Teil der irdischen Welt. Schon immer. Die Getretenen
sind unter uns. Unter, nicht mit uns. Wenn wir hinschauten,
könnten wir sie sehen. Viele von uns sind Teil
dieser Unterwelt und sehen sie doch nie ganz. Das
ist so nicht richtig, aber was ändert sich schon durch
halbe Sachen? Wir können nicht ein Problem herauspicken
und es zuerst lösen, weil es mit den anderen verwebt
ist. Wenn wir die Weben zerschneiden, verletzen wir
nicht nur die anderen sondern auch uns. Wir lassen die
anderen tiefer in den Staub fallen. Stattdessen müssten wir
das Gewebe selbst verstehen. Das Wissen dazu ist alt
und einfach, in vielen von uns verborgen. Die helfenden Worte
unter einer Staubschicht vergraben.

Lilið fühlt sich nicht, als wäre sie ihrem Namen gerecht
geworden. Es ist ein Gefühl des Versagens, des nicht
Genugseins. Ein Gefühl, das in der Unterwelt verbreitet
ist. Kennt ihr das auch? Dabei hat sie so viel getan: Menschen
das Gefühl gegeben, richtig zu sein in ihrer eigenen kleinen
Welt. Die Unterwelt ist nun lokal vorübergehend ein wenig
gemütlicher. Was machen wir jetzt?
