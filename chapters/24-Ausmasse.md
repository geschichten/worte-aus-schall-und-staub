Ausmaße
=======

*CN: Übelkeit, Lebensbedrohung, Messer, Mord, Schmerzen, Body
Horror, Blut, Ekel?, Sanism, Entführung, Damsel in Distress*

Lilið war auch nach Stunden noch speiübel davon, wehrlos in
einem Becher herumgeschüttelt worden zu sein. Ihr Gleichgewichtssinn
hatte ohnehin in dieser Form ein paar Probleme mit dem Oben und
Unten gehabt, aber alles war noch viel schlimmer geworden, als
sich auch dauernd änderte, wo oben und unten jeweils tatsächlich war, teils
in Parabelflügen oder so etwas, teils abrupt, weil ihr würfelförmiger
Körper gegen eine Becherwand gestoßen oder auf dem Tisch
aufgekommen war. Wahrscheinlich nicht so viel später, auch wenn es
ihr wie Ewigkeiten vorgekommen war, war sie zusammen mit den anderen
Würfen in einen kleinen Spielkoffer verräumt worden. Ehe er
zugeklappt worden war, hatte sie noch in das riesige Gesicht
einer Königin auf einer Spielkarte gesehen. Dann war es dunkel
geworden.

Liliðs Körper schmerzte im Inneren, sowie an jeder Kante, jeder
Ecke und jeder Fläche. Vor allem brannten ihre einundzwanzig Augen, weil
sie für die Faltung ihre realen zwei in die entsprechenden
Positionen hatte auseinanderfalten müssen. Es hatte an ein Wunder gegrenzt,
dass sie mit ihnen hatte sehen können. Farbverschoben zwar, weil
sie vermutlich jeweils die Sehzäpfchen und -stäbchen, oder
wie die Dinger hießen, nicht so zusammengewürfelt hatte, dass
ihr Hirn aus den Daten ausreichend herleiten hätte können, was
für Informationen zu ergänzen wären. Aber gut genug, um
einen überraschend guten Überblick zu gewinnen. Zusammengewürfelt. Lilið
musste lachen, aber die Lunge, die in ihrer beengten
Form nun gegen ihre gefalteten und teils spitzen Rippen
flatterte, ließ sie sofort damit aufhören. Luft anhalten, um
Schmerz zu verdrängen.

Lilið hatte versucht, zu zählen, um Zeit abzuschätzen. Aber
in so etwas war sie nie gut gewesen. Ihre Möglichkeiten waren sehr
beschränkt. Sie wusste, dass sie sich nun auf der Kagutte
befand. Und sie ahnte, dass die Kagutte abgelegt hatte. Sie
ahnte es aus den Kommandos, die sie zwischendurch gehört
hatte, und aus der leichten Schaukelbewegung. Dann
war sie eingeschlafen. Sie hatte
keine Ahnung, wie ihr das bei den Schmerzen gelungen war. Sie
hatte ebenso wenig Ahnung, wie sie es gerade schaffte, ihren
verkrampften Nacken aus ihrem Knäuel herauszufühlen.

Als sie wieder aufgewacht war, war ein Streifen Licht in den
Koffer gefallen. Nicht so viel, dass sie irgendetwas darin
hätte ausmachen können. Aber Licht bedeutete, dass dort, wo
sie war, wahrscheinlich Leute in der Nähe waren. Sonst würde
ja niemand Licht anmachen.

Ihre Gedanken klebten. Irgendwann, dieses Mal wirklich
nach einer langen Zeit, verschwand das Licht wieder. Lilið
konnte sich nicht erinnern, ob es plötzlich passiert
oder verblichen war. Es war auch egal. Sie spürte, dass sie
nicht mehr in der Lage war, diese Form zu halten. Es war
still um sie herum, sofern sie das durch das Rauschen ihrer
nach innen gefalteten Ohren beurteilen konnte. Also hieß
es wohl: Jetzt oder Nie.

Sie versuchte, langsam atmend, die Entfaltung kontrolliert
hinzubekommen. Aber es war alles von ihr zu perfekt
durcheinander gesteckt, es war alles andere als einfach. Sie
schaffte es lediglich, einen Finger zu befreien und drückte
damit von unten gegen den Deckel. Nichts passierte. Sie
hatte mit dem Finger nicht die Kraft, den Deckel des Koffers
zu öffnen. Vermutlich hatte er auch Schnallen.

Als sie versuchte, einen zweiten Finger herauszulösen, sprang
ihr ganzer Arm aus der Faltung und gab dem Koffer einen Ruck,
mit dem er woanders landete. Sie durchfuhr ein solcher Schmerz,
dass sie ihm, ohne weiter darüber nachzudenken, nachgab, und sich
ohne Rücksicht auf Lärm endlich zumindest obenrum
ganz entfaltete. Sie sprengte den Koffer durch ihre
neuen Ausmaße. Eine Schnalle flog
durch den dunklen Raum und schepperte. Das musste doch jemand
hören! Egal. Es war Lilið einfach nicht möglich, sich um
irgendetwas anderes zu kümmern als um ihren flauen Magen, den
entfalteten Brustkorb, in den sie nun endlich wieder
Luft in üblichen Mengen ziehen konnte, und den pochenden, brennenden
Schmerz in ihrer einen Schläfe und ihren Augenbällen. Sie
schnaufte. Nur ihre Beine bildeten noch ein Knäuel. Sie hörte ihren
Magen knurren. Das war eine äußerst miese Lage, in der sie hier
steckte. Sie dachte an Marusch, aber eigentlich hatte sie dafür
gerade keinen Raum in ihrem Kopf.

Der Raum lag immer noch dunkel und still vor ihr. Oder ihre
Augen waren beschädigt. Ihr Magen knurrte abermals und das Geräusch
verklang einsam in der Stille. Niemand sagte etwas. Vielleicht war
sie wirklich allein. Also entfaltete sie mit einem Ruck auch noch
ihre Beine und legte sich erschöpft auf den Rücken. Sie bemerkte,
dass sie nicht nur Hunger sondern noch viel mehr Durst hatte. Und
dass ihr Gesicht auslief, Tränen darüberflossen. Sie atmete und
konzentrierte sich auf nichts anderes als das Heben und Senken
ihres Brustkorbs und ihres Bauchs. Sie wollte eine Hand auf den Bauch
legen, aber sie streifte auf dem Weg etwas, was sich bewegte. Und
warm war.

Sie war hier doch nicht allein.

Lilið drehte sich vorsichtig auf die Seite. Vielleicht war es ein
Schlafraum? Ihre Augen, durch die inzwischen schwache Blitze
zuckten, -- sie würden wohl lange brauchen, um sich zu erholen --, trafen
ein anderes Augenpaar, aber kein menschliches. Es waren schwarze Augen,
die in dem sehr schwachen Licht glänzten, das von irgendwoher in
den Raum fiel. Sie betrachteten Lilið neugierig. Die Nase der Abe
kam ihr näher und berührte die ihre, zuckte aber sofort wieder
zurück. Eine Abe! Aben gehörten wie Auben zu den Postdrachen. Sie
waren schwerer davon zu überzeugen, Bindungen mit Menschen einzugehen,
als Auben, aber im Gegensatz zu Auben brachten sie Post nicht nur
an ein immer gleiches Ziel. Aben waren sogar in der
Lage, Menschen wiederzuerkennen und ihre Sprache zu verstehen, auch
wenn sie es nicht immer taten oder zugaben. Weil sie sehr eigensinnig waren,
wurden sie heutzutage kaum noch als Postdrachen benutzt. Irgendwie
wollte Lilið eigentlich nicht so denken, das Wort 'benutzen' störte
sie. Diese Abe war immerhin das erste Lebewesen, dem sie nach
dieser Tortour begegnete und bisher freute sie sich über die Begegnung.

Aber was machte eine Abe in diesem dunklen Raum?

Die Abe leckte ihr zärtlich durchs Gesicht. Ohne zu fragen,
natürlich. Lilið mochte es trotzdem. Nun hatte sie also Drachenspucke
statt Tränen im Gesicht, was soll's. Als die Abe genug geleckt
hatte, tappste sie auf Liliðs Körper, stieß sich dort weich
ab und flatterte in die Luft. Lilið drehte sich wieder auf den
Rücken, um ihr mit dem Blick zu folgen, und beobachtete, wie
sie mit einem Flammenstoß einen Docht einer Deckenleuchte
entzündete. Diese brauchte einige Augenblicke zum Warmwerden,
dann tauchte sie den kleinen Abstellraum in warmes, schummriges
Licht. Die Abe flog einen Kreis um die schaukelnde Lampe und dann
einen Bogen in eine Ecke der Kammer. Lilið tat das schaukelnde
Licht in den Augen weh, aber trotzdem richtete sie sich halb
auf, -- ächzte ausversehen dabei leise vor Schmerz --, um zu
sehen, wo die Abe nun hinflog. Lilið verkrampfte sich irgendetwas
in Herzgegend, als sie sah, wie der kleine, schwarze Drache auf dem
ausgestreckten Unterarm einer Person landete, und von dort auf
deren Schulter kletterte. Sie erkannte die Person am Mantel
wieder, sie war Drude genannt worden. Und im Gegensatz zur
verspielten Abe waren ihre Gesichtszüge steinern.

Sie blickten sich eine Weile gegenseitig einfach an, ohne sich
zu regen. Drude hatte sie also die ganze Zeit über beobachtet, schloss Lilið. Sie
schluckte. Dieses Mal funktionierte es wenigstens, auch wenn sich alles rau
und falsch anfühlte und weh tat.

Drude schritt mit der Abe auf der
Schulter zu ihr hinüber und ließ sich zügig elegant in die Hocke neben
ihr nieder. Der Mantel, viel leichter als der eines Nautikas, fächerte
sich dabei schön wie die Flossen eines Fisches beim Bremsen. Die Abe sprang
wieder von Drudes Schulter herab und hopste, die Flügel dafür nur ausgestreckt, um
weiterzukommen, nicht um zu fliegen, über Liliðs Körper auf ihre andere
Seite.

"Drude?", erkundigte sich Lilið.

Drude nickte. "Die bin ich wohl.", sagte sie, die Stimme so steinern
und ausdruckslos wie ihr Gesicht. "Mach dir keine Mühe, zu schreien."

Und ohne, dass Lilið es auch nur vorhergeahnt hatte, war Drudes
Körper plötzlich über ihrem. Sie nahm auf Liliðs Becken Platz und
presste die Knie seitlich mit einer Kraft gegen Liliðs Flanken, mit
der Lilið nicht gerechnet hätte. Drude schob mit
der einen Hand den Ärmel des anderen Arms nach oben, der sich vor
Liliðs Augen in etwas Flossiges und zugleich Scharfkantiges
verformte und sich in einer gezielten Bewegung an ihren Hals legte.

"Mach dir auch nicht die Mühe, zu lügen.", fügte Drude hinzu. "Ich
werde dich nun ein wenig ausfragen und dann entscheiden, ob ich dir
helfe oder dich töte. Ich bin gut darin, mitzubekommen, wenn jemand
Geschichten erfindet oder zu routiniert eine Geschichte runterbetet. Wenn
ich es bei dir merke, bist du tot. Verstanden?"

Lilið wollte nicken, kam aber sofort darauf, dass das mit einer
scharfkantigen Unterarmflosse an der Kehle keine gute Idee
wäre. "Ja.", flüsterte sie also.

Drudes freie Hand landete neben ihrem Kopf. Das beruhigte Lilið auf
der einen Seite, weil Drude dadurch in einer abgestützteren Körperhaltung
war, in der sie mit der scharfen Flosse weniger leicht ausrutschen
könnte, aber auf der anderen Seite kam dadurch Drudes Gesicht ihrem sehr
nahe. Drude roch herb. Ein Geruch, den Lilið weder positiv noch negativ
empfand, aber sie fand, er passte zu Drude. Was waren das für alberne
Gedanken?

"Bist du Nautika?", fragte Drude.

"Ja.", flüsterte Lilið.

Die Flosse drückte sich etwas mehr gegen ihren Hals und Lilið jappste. Hielt
Drude das für eine Lüge? "Du bist also nicht Lilið von Lord Lurch?", schlussfolgerte
Drude falsch.

"Doch!" Liliðs Stimme war nur noch ein fiepsiges Wimmern. Nicht, weil
sie Angst hatte. Sie war sich nicht einmal sicher, ob sie Angst hatte. Sie
glaubte schon, aber eigentlich bestand sie aus Schmerz und dem
erleichternden Wissen, zumindest den Würfel überlebt zu haben. Sondern weil
sie den Eindruck hatte, die Stimme passte nicht mehr in ihren durch diese
Klingenflosse zusammengedrückten Hals. Ob sie schon blutete?

Die Flosse ließ sich Zeit damit, aber löste sich dann doch wieder ein
wenig von ihr. "Das will ich meinen. Ich fühle nichts mehr an Magie
von deiner Seite im Raum.", meinte Drude. "Und wenn der Hut dich als
Lilið erkannt hat, dann musst du also Lilið von Lord Lurch sein."

Das war keine Frage mehr, aber trotzdem bestätigte Lilið mit einem
zustimmenden Geräusch.

"Der Hut hat sehr viel geredet. Sein Stil sozusagen. Er meinte, du
wärest totensicher auf Frankeroge.", fuhr Drude fort. "Wieso bist
du da nicht?"

Lilið beeilte sich, eine Antwort zu finden. Das war keine so
leichte Frage, weil dieses 'Wieso' sich auf verschiedene Weisen
beantworten ließ. "Ich wollte da nie hin.", gab sie zu verstehen. "Ich
habe mit einer Person getauscht, die sich als mich ausgibt."

Drude lachte tonlos. Ihr Flossenarm drückte dabei einige Male
leicht gegen Liliðs Hals, aber nicht so sehr wie vorhin. "Der
Hut.", sagte sie. "Ihm ist sehr zuzutrauen, auf so etwas reinzufallen. Ich
kann mir auch gut vorstellen, dass deine Doppelgängerperson sich
bewusst unskorsch anstellt, wenn er zum Prüfen vorbeikommt, und
er auch darauf reinfällt. Jedenfalls erzählt er, dass sie nicht
gerade mit Leistung bestäche. Er ist so ein Ekel."

Lilið gab abermals ein zustimmendes Geräusch von sich. "Hast
du mich deshalb nicht verraten?", fragte sie.

"Siehst du?", sagte Drude. "Du stellst nicht einmal in Frage, dass
ich dich von Anfang an gespürt habe. Das ist dir einfach klar. Er würde
es immer abstreiten. Zum Glück ist er nicht an Bord."

Erleichterung durchströmte Lilið. "Puh, das ist gut.", sagte
sie. Aber eine Antwort war das eigentlich nicht gewesen.

"Das hilft dir wahrscheinlich wenig.", informierte Drude.

"Warum?", fragte Lilið. "Ich könnte mein Gesicht falten und
das tun, was ich ursprünglich wollte: Euer Nautika werden." Sie
merkte, dass sie redete, ohne viel nachgedacht zu haben, und
dass das angesichts ihrer Lage eigentlich keine gute Idee
war. Weder war das, was sie gesagt hatte, schlüssig, noch
war es Information, die sie hätte weitergeben wollen, wenn sie
nicht unbedingt gemusst hätte.

"Du wolltest hier also Nautika werden.", wiederholte
Drude. "Was prinzipiell gut ist, denn wir haben keins mehr. Aber
wie willst du bitte dein Erscheinen an Bord erklären?"

Lilið schluckte. "Keins mehr? Ihr hattet eins?", fragte sie. Ja, sie
hatten eins anheuern wollen, fiel ihr ein. Sie dachte an Marusch. Endlich
dachte sie an Marusch.

Drude verzog das erste Mal den Blick zu einem Grinsen, kein
glückliches. "Es hat fertig gebracht, uns so schlecht zu navigieren,
dass wir es über Bord geworfen haben, als Nederoge am zweiten Abend wieder in
Sicht kam. Nun versuchen wir es ohne. Du kannst dir denken, dass das
nicht unbedingt so gut klappt."

Endlich fühlte Lilið Angst. "Über Bord geworfen?", fragte sie. War
es Marusch gewesen? "Nah genug vor Ufer, dass es theoretisch an
Land hätte gelangen können?"

Drude schüttelte den Kopf. "Im Prinzip vielleicht schon, aber ich
habe bei meiner Erzählung an Ausführlichkeit gespart. Deinem
Magen zuliebe.", sagte sie. "Wir haben es vorher Kiel geholt, das
hat es schon kaum überlebt. Es wird Fischfutter geworden sein." Bei
den Worten verformten Drudes Mund und Kiefer sich für einen kurzen
Moment, es zeigten sich spitze Zähnchen, aber verwandelten
sich sofort wieder zurück.

"Du hast eine Fischform, in die du wechseln kannst?", erkundigte
sich Lilið.

"Äußerst aufmerksam von dir.", antwortete Drude in einem unverkennbar
sarkastischen Tonfall.

"Entschuldige." Ja, das war einigermaßen offensichtlich, dachte Lilið.

Sie kannte einige Menschen, die eine feste andere tierischere Form hatten, in
die sie leicht wechseln konnten. Zum Beispiel Elmar von ihrem Hof, dessen
andere Form Schwingen hatte. Häufig waren diese Formen gar nicht klar
an einem bestimmten Tier orientiert. Lilið kannte kein Fischwesen, aber
hatte davon gehört, dass diese in ihrer Fischform meistens
sehr viel flossigere Arme und Beine hatten und einen stromlinienförmigeren
Körper. Von Kopfveränderungen hatte sie bisher nichts gehört.

"Deine Gehirnwindungen waren über eine volle Nacht und einen weiteren
Tag hinweg in eine Würfelform gefaltet.", erinnerte Drude sie. "Ich glaube, ich sollte
Verständnis dafür haben, wenn sich deine Gedanken erst einmal wieder an ihren
Platz und die Richtung, in die sie fließen sollen, gewöhnen müssen."

Lilið wusste nicht, wie sie darauf antworten sollte. Sie fand, egal
warum sie nicht so klar denken könnte, sollte Drude dafür
Verständnis aufbringen, aber sie wollte keinen Streit mit einer
Person anfangen, die mit einer scharfen Klingenflosse an ihrem
Hals dicht über sie gebeugt war. Also ging sie
auf eine Frage von zuvor ein. "Du hast recht, dass ich eine gute Begründung
bräuchte, warum ich hier einfach aufgetaucht bin, wenn ich mich
als Nautika ausgeben möchte. Ich müsste erklären können, wie mir das
geglückt ist und was das soll.", leitete sie ein. "Wenn du zum Schluss
kommen solltest, dass du mich nicht töten willst, würdest du mir
vielleicht helfen?"

"Indem ich meine Crew belüge?", fragte Drude. "Niemals. Das maximale, was du aus
mir rausbekommen kannst, wenn ich dich am Leben lasse,", Drude betonte
das 'wenn' ausschweifend, "ist, dass ich dich vielleicht nicht
verrate, wenn du dich versteckt hältst, und ich dir Nahrung und Trinken
zuschmuggeln würde. Sobald du aber gesehen würdest, wäre mir lieb, wenn
wir den Deal schließen, ich habe dich nie gesehen und du mich nicht. Ist
das klar?"

Lilið bestätigte. "Ich akzeptiere natürlich."

"Wenn du dich nicht dran hältst, na ja, du weißt schon." Drude drückte
abermals ihre Flosse für einen Moment gegen Liliðs Hals.

Lilið war es schon so gewohnt, dass sie nicht einmal mehr jappste, aber
sie fühlte trotzdem was es bedeutete. "Was ist dein
Interesse an mir, wenn du mich nicht tötest?", fragte
sie mutig.

"Ich stelle hier die Fragen!", stellte Drude klar. Ihr Gesicht und ihre
Stimme waren nun wieder steinern.

"Dann stell sie!", bat Lilið.

"Eilig hast du es also.", sagte Drude. "Nun gut. Warum wolltest du
bei uns Nautika werden?"

Sofort bereute Lilið, um Fragen gebeten zu haben. Sie schluckte
und dachte nach, wie sie darauf antworten sollte.

Langsam drückte Drude die Kante weiter gegen ihren Hals, bis
es brannte und Lilið sich dieses Mal sicher war, das dornige,
kleine Vorsprünge der Flossenkante in ihre Haut
stachen. "Ich werde dir dieses Mal keine Ideen vorlegen, die
ich schon habe, die du nur zu bestätigen bräuchtest.", sagte
sie. "Und lass nicht zu viel Zeit vergehen!"

Lilið presste ein "Verstanden!" hervor und jappste doch wieder,
als Drude ihrem Hals wieder Raum gab. "Ja, ich bin hier von
feindlicher Seite, schätze ich.", sagte sie. Sie hatte keine
Wahl, vermutete sie. "Ich weiß, dass hier die Kronprinzessin
an Bord ist und ich möchte nicht, dass sie Spielball
der Mächte wird, wie das gerade passiert."

"Du möchtest sie befreien.", schloss Drude. Sie wirkte plötzlich
nachdenklich.

"Ja.", antwortete Lilið. Sie hätte damit gerechnet, nun wieder
mehr von der Flosse zu spüren zu bekommen. Sie hatte sich
überlegt, sich in dem Moment sehr flach zu falten. Oder, wenn
das aus irgendwelchen Gründen nicht klappen sollte, ihre
freien Arme zu benutzen, um zu kämpfen. Aber stattdessen
drückte der Flossenarm noch weniger zu.

"Mich sollte das eigentlich nicht überraschen. Aber
das tut es.", sagte Drude. "Das erste Nautika hat von der Kronprinzessin
nichts gewusst. Es hat herausgefunden, dass wir illegal unterwegs
sind, was nicht so schwer herauszufinden ist, und wollte uns
deshalb verpfeifen. Ich hatte noch die Idee, ihm die Sache mit
der Prinzessin zu erklären, und hätte dann damit gerechnet, dass
er vielleicht verstehen würde, was unsere Absichten sind, und
sich umentscheiden würde. Dann hätten wir noch ein Nautika gehabt,
hätten ihm eine zweite Chance geben können. Aber du fängst gleich
damit an, dass unser Vorhaben dir nicht passt. Es geht dir nicht
um Illegalität. Das ist dir egal."

Sie hatte verloren, dachte Lilið. Sie antwortete nicht. Die Erleichterung,
die sie kurz gefühlt hatte, weil die Beschreibung des anderen Nautikas
nicht auf Marusch passte, drang nicht einmal ansatzweise tiefer
zu ihr durch. Zumal Marusch auch eine Rolle hätte spielen können, zu
der das gehörte.

"Illegalität ist dir egal, oder?", bohrte Drude nach und drückte
noch einmal ihren Arm an Liliðs Kehle. Fast sanft.

"Ja.", flüsterte Lilið matt. "Ich bin selbst derbst kriminell. Das
ist mir herzlich egal."

Drude nahm den Arm von ihrem Hals und richtete sich auf. Liliðs
Oberkörper war frei. Sie runzelte irritiert die
Stirn, aber wurde dann davon abgelenkt, dass die kleine Abe auf ihre
Brust sprang und über ihren Hals
leckte. Drude fädelte ihre Hand, die nun wieder eine menschliche war, unter
den Körper der Abe. "Lilið, lass das.", raunte sie. "Ich glaube,
Abenspucke in Halswunden ist nicht sehr hygienisch." Drude nahm
den Drachen in den Arm, der sich das gern gefallen ließ, und blickte
in Liliðs verwirrtes Gesicht. "Die Abe heißt auch Lilið.", informierte
sie. "Vielleicht sollte ich sie Lil abkürzen, solange du an Bord
bist."

"Du tötest mich nicht?", fragte Lilið.

Drude schüttelte den Kopf. "Dir sollte klar sein, dass du dich nicht
einfach als Nautika offenbaren kannst. Und selbst wenn du es schaffen
solltest, dass du dann ein verlässliches, sehr gutes Nautika sein
musst und nicht tricksen darfst, weil du, sobald
der Verdacht aufkommt, dass du nicht dahin navigierst, wo wir hinwollen,
kielgeholt wirst oder Schlimmeres. Du würdest sehr gründlich
überwacht werden.", erklärte Drude. "Ich möchte gern herausfinden, wie du
auf die Idee kommst, dass, die Prinzessin zu retten, sinnvoll wäre. Und das
als Person, die darauf pfeift, ob Handeln Gesetzen folgt oder nicht. Außerdem
als Person, die nicht so wirkt, als würde sie von Monarchie
viel halten. Also, das wäre untypisch, wenn du von Monarchie viel
hältst, aber sehr gesetztesuntreu bist. Und ich fühle mich mit
dir in Sachen Sexismus verbunden. Ich hasse, wie
der Hut über dich geredet hat. Fazit: Ich sehe dich ausgeliefert genug,
dass du keine Gefahr für mich darstellst, während ich meine Neugierde
stillen kann, wieso ein Mensch wie du, hm, ist."
