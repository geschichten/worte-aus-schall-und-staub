Treffsicherheit
===============

*CN: Blut, Transfeindlichkeit von der übelsten Sorte,
misgendern, Sex - erwähnt, reden über ein
Massaker, Gaslighting ?, Vor Leute spucken - erwogen,
Fäkalien, Ekel.*

Mazedoge war eine schon vom Weiten beeindruckende Insel. Sie war
wahrscheinlich ähnlich groß, wenn nicht größer als Angelsoge. Sie
war außerdem gebirgig. Von weitem erkennbare Prunkbauten wie
Torbögen ragten aus den bewaldeteten Hängen. Die Hauptstadt
zog sich um eine Bucht, von dort ins Landesinnere hinein
bis tief in die Berge darum herum hinauf.

Sie steuerten den Sitz der Monarchie an und segelten dazu an der
Hauptstadt vorbei. Sie hatten beschlossen, die Jagd nach dem Igeldings
erst nach der Sitzung wieder aufzunehmen. Unter anderem auch, weil
es ja möglich wäre, dass sich beides in einem machen ließe. Schließlich
gehörte das Igeldings quasi auch zum Schatz der Monarchie und könnte
an Königin Stern oder andere königliche Leute bei so einer passenden
Gelegenheit wie dieser Versammlung abgegeben worden sein. Oder bald
abgegeben werden.

Es mangelte auch dem Hof der Monarchie, ehemals Sitz von König Sper, nicht
an Größe und Prunk. Er mochte mit der Ausdehnung einer kleinen
Hafenstadt mithalten, wenn auch die Gebäude größere Abstände
zueinander hatten und weniger auf Zweckdienlichkeit ausgelegt
waren als sie es vielleicht in einer Stadt sein mochten. Die Häuser
auf dem mit bunten Pflanzen übersähten Gelände wirkten allesamt so
edel -- höchstens etwas kleiner -- wie das Gebäude, in dem
der Angelsoger Adelsball stattgefunden hatte. Der Komplex
war von einer gelben Mauer eingefasst, die sich
zum Hafen hin öffnete. Der Hafen bestand aus einem größeren
Teil, in dem schon ein paar Kagutten lagen, und einem kleineren,
der für Yachten und Jollen gedacht war. Beide Hafenteile waren
bewacht. Drude informierte sie, dass die Anzahl der Wachen im Vergleich
zu sonst bei ähnlichen Veranstaltungen ganz schön dezimiert
war. Den Uniformen nach zu urteilen gehörten nicht alle zum Königreich
Sper. Marusch, Heelem und Lajana erkannten einige, die zu ihrer Königsfamilie
gehörten, und Drude meinte einige Uniformen Königreich Dornrose
zuordnen zu können. Jene anders uniformierten Wachen standen
allerdings alle an der Einfahrt zum größeren Hafen, aufgereiht
nebeneinander mit breitem Stand und die Hände in den
Hosen- und Rocktaschen, wie es ein selbstbewusstes Auftreten
nach aktueller Etikette verlangte.

Sie wurden an der Einfahrt zum kleinen Hafen von zwei Wachen
in mazedoger Uniform aufgehalten. Lilið fühlte eine leichte
Taubheit in sich, die sie von ihrer Gefangenschaft in der
Zentral-Sakrale ausgelöst durch Wache Luanda kannte. Sie war
nicht gefaltet. Niemand von ihnen wandte Magie an, aber eine
der Wachen, das spürte Lilið, wäre im Stande gewesen, dagegen
anzugehen, und testete gerade, ob es nötig wäre.

"Grund der Anreise?", fragte die kleinere der beiden Personen.

"Versammlung der Orakel und der Monarchie.", sprach Heelem. Er
war der Kapitän, also war es seine Aufgabe. "Und
Vorabtreffen mit Königin Stern."

Die Wache nickte. "Wir müssen eure Identitäten prüfen. Wer
seid ihr? Sind alle von euch an Deck oder befinden sich
noch Personen unter Deck?"

Wenn die Wache so gut war wie Drude, wusste sie, dass sie alle
an Deck waren, dachte Lilið.

"Wir sind alle an Deck.", teilte Heelem mit.

Eine Windböe griff
in das noch nicht geborgene Segeltuch, sodass es schlug. Drude
stabilisierte die Teeseufel im Wasser, indem dey die Wellen
darum herum sortierte. Vielleicht tat dey es unauffällig genug,
dass es den Wachen nicht auffiel. Sie taten jedenfalls
nichts dagegen.

Heelem sprach weiter, als es wieder leiser war. "Die
Prinzessinnen Stern, namentlich Prinzessin Lajana
und Prinzessin Marusch. Lilið von Lord Lurch, Nautika, auch
bekannt als der Blutige Master M. Drude aus Belloge, freie Wache,
Informantika und Transmagika. Und meine Wenigkeit: Heelem,
Nautika, ehemals im Dienst der Garde von Königin Stern."

Heelem stellte sie eigentlich relativ gelassen vor, aber es
war für Lilið trotzdem ein unbeschreiblich mächtiges Gefühl,
hier in dieser Konstellation vorgestellt zu werden. Besonders,
dass sie auch als Nautika vorgestellt worden war, fühlte
sich ermächtigend an.

Die Wache stutzte. Ihr Blick wanderte über die Besatzung
und blieb an Drude hängen. "Prinzessin Marusch?", fragte
sie.

Heelem seufzte und deutete mit nach oben geöffneter Hand
auf Marusch. "Das ist sie. Du kennst sie vielleicht unter
einem anderen Namen. Den alten Namen solltest du tunlichst
vergessen."

Die Wache nickte und musterte nun Marusch. Hatte sie vorher für
am wahrscheinlichsten gehalten, dass Drude Marusch wäre, weil
dey so groß und stabil gebaut war? Lilið fiel ein, dass der
Wache vermutlich nur Drudes und Maruschs Gesicht unbekannt
waren. Sie wirkte hilflos. Wieder schlug das Segel einige
Male hin und her.

"Ihr seid nicht darauf vorbereitet, dass das passiert?", fragte
Heelem, als es wieder leiser wurde.

"Doch natürlich.", behauptete die Wache, aber die andere schüttelte
den Kopf. Sie blickten sich kurz an, dann sprach die andere: "Wir
haben den Auftrag, eure Identitäten zu überprüfen, weil viele Gerüchte
über Betrügende im Umlauf sind, die die Identitäten von Personen
übernehmen, um in deren Namen Verbrechen zu begehen. Aber wenn
ihr seid, wer ihr behauptet, wäret ihr in der Übermacht und
könntet die verlangte Überprüfung ausschlagen. Das macht
die Lage für uns schwierig."

Marusch nickte. "Es gehört sich auch nicht, wenn keine
komplexere Situation vorliegt, Königliche zu kontrollieren."

Die zweite Wache nickte. "Genau. Das bringt uns in eine
Konfliktsituation."

"Warum erzählst du das alles?", raunte die erste Wache der zweiten
zu.

"Ich habe den Eindruck, Offenheit führt uns in dieser Situation
am weitesten.", murmelte die andere zurück.

Marusch lächelte. "Führt gern die Identitätsprüfung durch. Wir
haben uns im Vorfeld darüber unterhalten, dass der Fall eintreten
kann, und gemeinsam entschieden, euch die Situation nicht zu
erschweren."

Auf einen einladenden Wink Heelems hin schwang sich die zweite
Wache an Bord, um ihr Blut gegen Seiten in einem Buch gegenzuprüfen,
dass sie mit sich führte. Die Einschnitte waren so papierdünn, dass
sie sich wieder schlossen, noch bevor die Wache Heilmagie ausführte.

Es ging relativ zügig. Trotzdem bemerkte Lilið, wie Lajana
auf ihrem Platz zusammensank, sich dann aber, als die Reihe
an ihr war, zusammenriss und sich besonders aufrichtete. Der
lange Zopf der Wache flog um sie herum, als sie wieder von
Bord sprang. "Willkommen auf Mazedoge. Ihr könnt einfahren. Der
erste Liegeplatz am Hauptsteg ist euer."

Heelem verlor keine Zeit, das Zeichen zu geben, dass Lilið das
Segel dicht holen möge. Mit einem Geräusch wie Papier, das in eine
Form schlägt, nur lauter, füllte es sich mit Wind und sie
segelten in den Hafen. Es war nicht viel Segelfläche und
trotzdem waren sie für ein Anlegemanöver ziemlich schnell. Lilið
benötigte alle Konzentration, um im rechten Moment die vorbereiteten
Palsteks über die Pfeiler zu werfen. Der erste Liegeplatz
war luxuriös am Rand des Hafenbeckens gelegen. Sie knoteten
Fender aus weichem Material außen an die Reling, damit der
Rumpf nicht an der Kaimauer scheuern würde. Anschließend konnten sie
einfach zur Seite aussteigen. Ein gepflasterter Weg aus gelben,
abgerundeteten Steinen führte vom Hafen einen Wiesenhang hinauf
zu einem großen Besuchshaus für Höherranginge, das sie
anstrebten. Lajana schritt voraus, Marusch dicht hinter
ihr. Sie alle, außer Lajana, trugen Matrosinnenkleider, die
sie ab nun Seefahrtskleider nennen wollten. Lilið erinnerte
sich mit einem Schmunzeln daran zurück, wie die Einigung auf
ihre Kleidung verlaufen war. Marusch hatte beschlossen, ein
solches Kleid zu tragen, weil sie es antiautoritär fand, als
Prinzessin im Seefahrtskleid zu kommen. Drude hatte mitgezogen,
und Lilið und Heelem hatten etwa zeitgleich mit den Schultern
gezuckt und gesagt 'Okay, ich mache mit'. Nur Lajana trug
ein langes, lavendelfarbenes Kleid. Marusch und Heelem hatten
weitsichtig für den Fall, dass sich öffentliche Auftritte
ergäben, eine kleine Garderobe für sie in der Teeseufel
untergebracht.

Wieder fühlte Lilið diese Mächtigkeit, diese epische
Unbeugsamkeit ihrer Gruppe, wie sie gemeinsam dem
Besuchshaus entgegen schritten und davor stehen blieben, als
aus dem Eingang einige hochrangige Personen hervortraten. Niemand von ihnen
beugte das Haupt.

Eine bedienstete Person trat vor und stellte sie alle trocken
einander vor. Sie fing mit ihrer Fünfergruppe an, verwendete
dafür Heelems Worte und benutzte für Marusch sogar ihren
Namen. Lilið entging dabei das Stirnrunzeln auf Königin Sterns
Gesicht nicht. Sie kannte ihr Gesicht von Zeichnungen aus Zeitungen
und Aushängen, aber in Echt wirkte es gestresster. Damit hatte
Lilið gerechnet. Wer sich freiwillig in Zeitungen abbilden
ließ, wirkte auf entsprechenden Bildern immer entspannter als
in Echt.

Die Person neben Königin Stern wurde ihnen als
Königin Dornrose vorgestellt. Hinter ihnen stand der
ehemalige König Stern und die Verlobte von Königin
Dornrose. Etwas abseits stand
eine Sakralet. Sie bekamen in diesem Rahmen alle
mit Namen vorgestellt, aber Lilið konnte sie sich nicht so schnell
merken. Anschließend wurden sie durch einen mit Ranken
verhangenen Torbogen in einen Garten hinter dem Haus
geführt, wo sie an einer großen Tafel
platznahmen. Es gab Obst zum Essen. Eine vollständige Mahlzeit
würde es in ein paar Stunden geben. Königin Stern informierte
sie darüber, was es für Obst gab, woher es kam und welche
Qualität es hatte. Es strengte Lilið jetzt schon an, ihr
zuzuhören, weil ihr die meisten Informationen reichlich
redundant vorkamen. Sie konnte selbst sehen, welches Obst
es gab. Sie versuchte, nicht so hart zu sein. Sie wusste,
dass sie Königin Stern gegenüber inzwischen voreingenommen
war. Es war auch nichts Schlimmes, über unwichtige
Dinge zu reden.

Königin Dornrose wirkte, als würde sie dieses Gespräch eine
Weile aus Höflichkeit mitführen, aber als sich die erst beste
Gelegenheit ergab, das Thema zu wechseln, tat sie es. "Ich
finde es eine spannende Entwicklung, euch als Gruppe zusammen
kommen zu sehen. Wie kam es dazu? Woher kennt ihr euch?"

Lilið lächelte, goss sich Saft ein und verdünnte ihn mit
Wasser. Sie fand die Frage sehr gut und berechtigt und beabsichtigte
das Antworten andere übernehmen zu lassen.

"König Sper hat mich entführt, und die sind alle gekommen, um mich
zu retten.", berichtete Lajana. "Dabei haben sie sich kennen
gelernt. Manche schon vorher."

"Meine Kleine, es ist lieb, dass du antwortest, aber Ðinda
möchte sicher vor allem etwas über dieses
Vorher erfahren.", erklärte Königin Stern. "Ich hoffe, es ist in
Ordnung, dass ich dich immer noch 'Kleine' nenne."

"Nein." Lajanas Gesicht versteinerte.

"Ich meine das nicht im Altersinne. Marusch ist einfach größer
als du.", verteidigte sich Königin Stern. Es sollte vielleicht beruhigend
klingen. Aber Lilið machte es eher wütend.

"Sie hat jedenfalls recht." Königin Dornrose, die Königin Stern gerade
Ðinda genannt hatte, lächelte Lajana freundlich an. "Wer von euch
kannte sich schon zuvor?"

"Heelem ist mit Marusch und mir großgeworden.", sagte Lajana.

Das, vermutete Lilið, war auch nicht die richtige Antwort.

"Lilið und ich haben uns bei ihr im Haus kennen gelernt, als ich
dort aus Gründen, die ich nicht weiter ausführen werde, eingebrochen
bin.", berichtete Marusch. "Wir sind wenig später im Rahmen ihrer
Ausbildung zum Nautika zwei Wochen gemeinsam auf einer Jolle
gereist. Die zwei Wochen endeten damit, dass wir von Lajanas
Entführung erfahren haben. Beim ersten Versuch, Lajana zu retten,
geriet leider nur Lilið auf die Kagutte der Entführenden."

"Zum Glück!", korrigierte Königin Stern. "Dir hätte sonst etwas
passieren können!" Leiser fügte sie hinzu: "Es wäre von
Vorteil, wenn du nicht von deinen moralischen Eskapaden wie
Einbrüchen bei bekannten Adelsfamilien oder Sex in der Öffentlichkeit
erzählst. Auch ein ordentlicher Aufzug wäre eine gute Idee
gewesen, angesichts der Tatsache, dass immer noch eine Möglichkeit
besteht, dass du zusammen mit Lajana eine Regierung des Königreichs
Stern bilden kannst. Ich weiß, du möchtest nur nichts unter den
Tisch kehren, aber deine Darlegungen über dein unmoralisches
Verhalten können von Menschen, die dich nicht gut kennen, in einer
Weise missinterpretiert werden, dass du immer noch stolz
auf dein Verhalten von damals wärest."

Richtig: Lilið erinnerte sich daran, dass Marusch zu ihrer Krönung
beim Sex in der Öffentlichkeit erwischt worden war. Sie grinste,
weil sie es heute ganz anders einrordnete. Vielleicht, wenn Marusch
sie heute fragen würde, ob sie am Tag ihrer Krönung als Akt
eines größtmöglichen Statements von Ungehorsam mit ihr Sex
in der Öffentlichkeit haben würde, würde sie nicht nein sagen.

Marusch hatte die Finger, mit denen sie gerade ein klebriges
Stück Obst gegessen hatte, zu ihrem Kragen bewegt, vielleicht,
um sie dort abzuwischen, aber senkte die Hände unverrichteter
Dinge wieder. "Mit Lajana regieren.", wiederholte sie. "Das ist neu."

"Du liebst deine Schwester, nicht wahr?", fragte Königin
Dornrose.

Marusch hob eine Augenbraue. "Offenkundig.", sagte sie. "Warum
hört sich das wie eine Drohung an, oder zumindest wie eine
Vorbereitung auf eine?"

"Das muss falsch rübergekommen sein.", versicherte Königin
Dornrose rasch. Sie sprach sehr sanft und deutlich. "Wir
kennen uns kaum, und schon stehen Verständnisschwierigkeiten
zwischen uns. Ich habe, zugegeben, nicht das beste Bild von
dir und du vielleicht auch nicht von mir. Aber ich gehöre zu den
Leuten, bei denen der erste Eindruck überschrieben werden kann. Ich
würde dich gern besser kennen lernen. Die Öffentlichkeit zeichnet
das Bild eines Jungspundes und Luftikusses von dir, der seine
Pflichten nicht ernst nimmt und sein königliches Prestige dazu
ausnutzt, Frauen aufzureißen und unbehelligt kriminelle Akte zum
Spaß auszuüben. Wettrennen, Lustvergnügen, bloß nicht
Verantwortung fürs eigene Handeln übernehmen. Und doch bist
du hier. Das bereits widerspricht diesem Bild der
Öffentlichkeit. Sobald es um deine Schwester geht, nimmst du eine
geradere Haltung ein. Dein Blick wird offener. Kann
es bedeuten, dass dein ganzes Verhalten bisher ein seltsamer
und undurchschaubarer Versuch war, deiner Schwester den
Rücken zu stärken?"

"Das habe ich dir doch die vergangenen zwei Tage versucht
zu erklären!" Königin Stern wirkte ungeduldig.

"Das hast du, und ich möchte nun von deinem Kind hören, ob
du ihm Worte in den Mund gelegt hast, oder ob sie dahinter
steht." Königin Dornrose hatte kurz gezögert, bevor sie
das Pronomen 'sie' betont ausgesprochen hatte.

"'Sie' ist richtig.", sagte Marusch.

Maruschs Einwand brachte Lilið dazu, ihre erste Interpretation, dass
es sich um eine Form von Abwertung gehandelt hätte, neu zu
überdenken. Vielleicht war die Betonung ein Zeichen von
Königin Dornroses Unsicherheit gewesen.

"Kind!", ermahnte Königin Stern. "Wir können das in unserer
Familie machen. Aber du kannst die Welt nicht verändern, nur
weil du dich weiblich fühlst oder so etwas. Es verunsichert
Menschen und fühlt sich für sie unbehaglich an. Du kannst nicht
erwarten, dass eine Königin eines anderen Landes bei der
Sache mitmacht. Ich stehe hinter dir, das weißt du. Aber hier
geht es um Politik und die Zukunft unserer Inseln. Das ist
kein Raum, wo du deine Sexualität an die große Glocke hängen
kannst, das hat mit Regierung nichts zu tun."

"Doch, sie kann das erwarten.", widersprach Königin Dornroses
Verlobte. "Wir benutzen 'sie'. Das ist eine Selbstverständlichkeit."

Königin Dornrose machte eine Geste zu ihrer Verlobten und
nickte. "Mach dir keine Sorge, Jelenika. Das macht uns
keine Umstände."

Während Liliðs Inneres gerade noch vor Wut gebrannt hatte (von
wegen 'Ich stehe hinter dir, das weißt du.'), fühlte sie
nun jähe Hoffnung, dass Allil mit ihrer Einschätzung unrecht
gehabt haben könnte und Königin Dornrose Marusch anerkannte
als was sie war.

"Ich mache mir keine Sorgen.", versicherte Königin Stern. "Ich
wollte in dieser Runde so Persönliches außenvor lassen. Sexualität
geht niemanden etwas an."

Je eine Hand von Königin Dornrose und ihrer Verlobten wanderten
wie beiläufig auf den Tisch, wo sich ihre Finger ineinander
verschlangen. "Niemand muss, alle dürfen.", murmelte die
Verlobte. Und noch leiser raunte sie: "Sexualität ist
was anderes."

"Jedenfalls!", sagte Königin Dornrose und es war deutlich,
dass sie damit einem weiteren Verteidigungsversuch Königin
Sterns zuvorkam. "Ich wüsste gern von Marusch, was sie
von einer Doppelregentschaft hält, bei der ihre Schwester
Königin ist und Marusch sie berät, sowie die Garde
koordiniert."

"Die Garde?", fragte Marusch.

"Ja." Es war das erste Mal, dass sich die Sakralet zu Wort
meldete. Ihre Stimme war dünn. "Die Orakel haben damals über Marusch
bestimmt, dass ihr als Königsperson ein großes Inselreich
zugeordnet werden würde. Ich lasse das gerade so vage, weil
die Prognosen darüber, wie viel sinnig wäre, derzeit
auseinandergehen und von bestimmten Informationen abhängen, die
wir derzeit noch nicht haben. Die Zuordnung baut, wie stets,
auf Hochrechnungen der Gewinnchancen in Kriegen auf. Wenn
wir also von dem Prinzip abweichen, dass die Person, die selbst
die größte Schlagkraft hat, das Land regiert und ein Zweiergespann
in unseren Hochrechnungen zulassen, dann wäre es nur allzu
sinnvoll, die Person mit dem hohen Skorem im Team auch die
Garde organisieren und befehligen zu lassen. Das Bundesorakel
bespricht diese neue Möglichkeit derzeit."

"Und wenn all deine Eskapaden eigentlich nur eine verschlüsselte
Botschaft waren, dass du deine Schwester liebst und unterstützen
möchtest, kann das eine Regierungsform werden, auf die ich
mich einlassen würde.", fügte Königin Dornrose hinzu. Sie
klang wohlwollend.

"Du wolltest ein ehrliches Gespräch, Ðinda, und mir liegt
daran, einmal die unausgesprochenen Gedanken offen
auf den Tisch zu legen, die eine Rolle für dein Zugeständnis
spielen.", mischte sich Heelem ins Gespräch ein.

Königin Dornrose lächelte. "Von dir hätte ich nichts anderes
erwartet.", sagte sie. "Und du hast recht, dass ich mich
in alter Manier diplomatisch ausgedrückt habe. Natürlich
weiß ich, was es für mich bedeutet hätte, wäre Marusch
vor zwei Jahren gekrönt worden. Und natürlich weiß ich, dass
meine Darstellung, dass es meinerseits ein großzügigegs
Zugeständnis an Marusch und ihre Schwester wäre, sie als
Team regieren zu lassen, geschönt rüberkommen mag."

Lilið fragte sich, ob sie verstand, worum es ging, und
runzelte die Stirn. Aber etwas anderes fing ihr zunehmend
mehr an, unangenehm zu sein: Von Lajana wurde geredet, als
wäre sie gar nicht dabei.

"Ich möchte es gern der Klarheit halber aussprechen.", sagte
Heelem. "Wenn Marusch vor zwei Jahren einer Krönung zugestimmt
hätte, wäre dein Inselreich nun viel kleiner, weil Marusch davon
mindestens die Randinseln zugesprochen worden wären, wenn nicht
mehr. Wenn das Bundesorakel für Marusch und Lajana vorherbestimmt,
dass Marusch quasi die Garde regieren würde, dann wäre ein
Nichtzustimmen eurerseits ein möglicher Kriegsgrund eines Krieges,
bei dem ihr schlechte Karten hättet. Deine Entscheidung,
Marusch und Lajana irgendetwas zuzugestehen, fußt nicht
auf bloßem Wohlwollen, sondern auch auf dem Wissen, dass ihr
mehr auf Augenhöhe regieren würdet, als du es gerade darzulegen
versuchst."

"Natürlich. Das tut mir leid.", bestätigte Königin Dornrose. "Ich
würde sehr gern auf Augenhöhe mit Marusch und ihrer Schwester
regieren, sofern ich bei diesem Treffen herausfinde, dass
Marusch durchaus eine Person ist, die politische Verantwortung
übernehmen kann und die zukünftige sternsche Regierung einen
feministischen Einschlag erleben wird. Mir ist sehr wohl
bewusst, wo auch sonst mein Platz im Machtgefälle wäre, aber
mir fällt andernfalls schwer, euch zu respektieren."

Heelem lächelte. "Das ist eine Direktheit, die mir zusagt."

Ein kurzweiliges Schweigen trat ein, in dem Lilið das Spektakel der Spantunken
besonders wahrnahm, die in einem flachen Wasserbecken im Blumenbeet neben ihnen
herumplanschten und aufgeregt durcheinanderfaselten, -- in ihrer zwitschernden
Sprache. Sie hatten sich zunächst von der Abe vertreiben lassen, die sich zur
Abkühlung zu ihnen ins Becken gelegt hatte, aber nun, als Lil ruhig dalag, als
würde sie zuhören, waren die kleinen, braungrauen Drachen zurückgekehrt. Lilið
hatte keine Ahnung, wie sie es schaffte, ihren hibbeligen Lärm während des
Gesprächs so restlos auszublenden.

"Ich soll also Befehlshaberin der Garde werden.", fasste
Marusch zusammen. Sie runzelte die Stirn ziemlich arg.

"Ja, das würde ermöglichen, was du immer wolltest: dass
deine Schwester Königin wird.", erklärte Königin Stern.

Marusch wandte sich Lajana zu und diese blickte skeptisch
zurück. Sie wirkte, als würde sie nicht viel verstehen.

Marusch seufzte und richtete sich an die Versammlung. "Erstens,
nein, das würde nicht ermöglichen, dass meine Schwester
regiert. Es wäre nicht ihre Regierung. Darum geht es dir gar
nicht, Muddern. Und dem Rest hier auch nicht. Sonst würde
irgendwer hier mal wenigstens versuchen, mit ihr
auch nur zu reden.", sagte sie. Ihre Mutter wollte
etwas einwenden, aber Marusch ließ sie nicht zu Wort
kommen. "Und zweitens: Was an 'Ich bin antiautoritär' hast
du nicht verstanden? Ich, eine Institution befehligen, die
dazu da ist, eine Obrigkeit mit Gewalt zu verteidigen. Kennst
du mich auch nur ein bisschen?"

"Nein, Kind, ich habe eigentlich schon lange das Gefühl, dass
du dich total verändert hast und ich dich gar nicht mehr kenne. Aber
bitte hör wenigstens zu Ende zu." Königin Stern hob beschwichtigend
eine Hand.

Die Worte, dass sie ihr Kind nicht kenne, taten Lilið
überraschend weh. Sie wusste nicht warum. Sie entsprachen so
offensichtlich der Wahrheit. Oder vielleicht auch nur fast: Vielleicht
hatte Marusch sich nicht verändert, sondern war nun bloß
offener darüber, wie sie wirklich war.

Marusch nahm einen Fuß mit auf die Sitzfläche und machte es sich
auf dem Stuhl legerer gemütlich. "Gut."

Vielleicht, überlegte Lilið, tat es so weh, weil es eigentlich
nicht um die Frage ging, wie sehr Königin Stern Marusch
wirklich kannte, sondern wie sehr sie sie kennen wollte, wie
sehr sie Marusch in ihrer Vorstellungswelt zulassen würde, wie sie war.

"Wir haben nicht mehr viele Möglichkeiten und doch haben
wir eine Chance, die es zuvor nie gegeben hat.", erklärte Königin
Stern. "Es war schon immer klar, dass Lajana nicht im Stande
sein wird, zu regieren. So sehr es euer Wunsch war, hätte
ihre Krönung bedeutet, Königreich Stern aufzugeben."

Heelem hob eine Hand und unterbrach sie knallhart. "Wir haben
in Wirklichkeit keine Ahnung, was möglich gewesen wäre, wenn
wir es gewagt hätten, Änderung zuzulassen.", sagte er. "Das,
was jetzt als Möglichkeit in Aussicht steht, hättest du
doch nie selbst erwogen, auch nur in den Raum zu stellen."

"Nicht ohne Not, nein.", bestätigte Königin Stern. "Politik
und Diplomatie ist schwieriger, als ihr es euch vorstellt. Ich
habe stets versucht, es allen Recht zu machen und möglichst
so zu regieren, dass es keinen Anlass für Kriege gibt. Oder
wollt ihr Krieg?"

Marusch runzelte die Stirn abermals. "Und nun wäre aus deiner Sicht
irgendwie nicht mehr die beste Strategie gegen Krieg, dass
ich gekrönt würde, sondern stattdessen, dass ich die Garde anführte?"

"Dazu wollte ich gerade kommen.", setzte Königin Stern
wieder ein. "Ich weiß nicht, was in der Zentral-Sakrale passiert
ist und wer dabei war. Es ist bekannt, dass die Blutige Mistress
M, also du,", sie deutete auf Lilið, "zum Zeitpunkt des
Massakers in der Zentral-Sakrale gefangen gehalten worden
ist. Ihr traut die Bevölkerung eine solche Tat zu."

Lilið schnaubte grinsend. Das tat die Bevölkerung auch nur
wegen zuvor passend gestreuter Falschinformationen über
sie. Es war so absurd, dass sie sich nicht einmal über das
zugewiesene 'Mistress' aufregte.

"Es gibt Gerüchte, dass du, mein Kind, in der Zentral-Sakrale
erwartet worden bist.", fuhr Königin Stern fort, ohne
auf Lilið einzugehen. "Und allein, dass das ehemals spersche Volk
für möglich hält, dass du dahinter stecken könntest, führt dazu, dass
es ein Frieden sicherndes Zugeständnis wäre, nicht dich
zu krönen. Damit du in einer so hohen Position wie als Führung
der Garde akzeptiert wirst, muss natürlich trotzdem offiziell
widerlegt sein, dass du das Massaker verübt hättest."

"Also möchtest du hier entscheiden, dass ich das Massaker
verübt hätte?", fragte Lilið.

"Das hätte politisch für die Stabilität der Monarchie
Vorteile.", hielt Königin Stern fest. "Und das sage ich nicht,
weil ich möchte, dass ich meine Familie nicht entmachtet sehen
möchte. Das ist natürlich auch der Fall. Aber stellt euch die
Alternative mal vor: Nur noch zwei Königsfamilien, die regieren,
und die über Vasallschaften die viele Arbeit koordinieren. Das
bedeutet auf der einen Seite mehr Machtmonopol und auf der
anderen eine destabilisierende Systemumstellung, weil völlig unerfahrenen
Personen für sie zunächst zu große Aufgaben zugeordnet
werden. Die Instabilität ist keine, die unseren Inseln gut
tun wird."

So sehr sich Liliðs Inneres gegen ihre Worte wehrte, sie konnte
Königin Sterns Argumente nicht völlig von der Hand weisen. "Ihr
wollt mir den Hammer also gleich mit in den Schuh schieben.", murmelte
sie.

Marusch lachte schallend auf. "Ich liebe dich für diesen Spruch!"

Drude schnaubte und selbst Königin Dornrose und ihre Verlobte
lächelten milde amüsiert.

"Übrigens wäre diese Vertuschung ein Spielzug, den ich nicht
schätze, aber akzeptieren würde.", hielt Königin Dornrose fest. "Ich
bin allerdings persönlich nicht davon überzeugt, dass die Systemumstellung
so ungute Folgen hätte. Natürlich hat Königin Stern Recht damit, dass ihr
alle unerfahren seid." Königin Dornrose deutete mit elegant ausgestreckter
Hand auf Lilið und Drude. "Aber auf der anderen Seite seid ihr Frauen
und habt Erfahrungen, die wir dringend in Führungspositionen
brauchen. Kurz runtergebrochen. Ich halte darüber gern einen
längeren Vortrag."

"Ich bin keine Frau.", informierte Lilið. "Ich fände auch gut, nicht
blutige Mistress M genannt zu werden. Master oder Mastress oder so
etwas wäre mir lieber."

"Du kannst natürlich auch als Nicht-Frau die Regierungsaufgabe bekommen,
aber der einfachheithalber würde ich trotzdem von Frauen in
Führungspositionen reden und dich mitmeinen." Königin Dornrose
lächelte freundlich.

Lilið schüttelte den Kopf. "Das halte ich nicht für zu Ende gedachten
Feminismus."

"Ich verstehe.", sagte Königin Dornrose. "Aber das Volk wird das
nicht verstehen. Wir werden einen Schritt nach dem anderen gehen
müssen. Und am Ende ist es ja auch einfach so, dass du wegen dem,
was dir bei Geburt zugewiesen worden ist, als Nicht-Mann benachteiligt
bist. Und wenn du ein Mann wärest, wärest du wieder bevorteilt. Wenn
du dir Marusch zum Beispiel ansiehst, hätte sie gekrönt werden
können, obwohl sie jünger ist als Lajana, und ist Frauen gegenüber
also im Vorteil, die von Anfang an für welche gehalten wurden. Und
wärest du ein Mann, so würdest du die Schwierigkeiten, die Frauen
im Leben haben, nicht richtig verstehen können, selbst wenn du
für eine gehalten würdest. Das ist einfach so."

"Wir werden nicht übereinkommen.", sagte Lilið. "Ich bekomme gerade
den Eindruck, du hast keine Ahnung, wie unser Leben aussieht, weil
wir nicht einfach sind, was Leute glauben, was wir sind. Ich habe
das Gefühl, du denkst dir mal eben Argumente möglichst zu deinen
Gunsten aus, ohne den Sachverhalt wirklich untersuchen zu wollen." Lilið
hätte damit gerechnet, sich ängstlich zu fühlen, einer
Königin etwas so streng entgegenzusetzen, aber das war nicht
der Fall. Sie fühlte sich überraschend selbstsicher.

"Vielleicht." Königin Dornrose wirkte einen Moment aufgebracht,
aber kurz darauf wieder gelassen und nachdenklich. "Ehrlich
gesagt habe ich auch nicht die Kraft, mir darüber Gedanken
zu machen und ich sehe es auch nicht ein. Es gibt nicht so
viele Menschen wie euch. Wenn du nicht im Interesse von Frauen
kämpfen und die Position von vornherein ausschlagen möchtest, die
ich dir vielleicht anbiete, je nachdem, ob wir irgendwie
zusammenkämen, ist das schade und zeigt direkt
deine Prioritäten. Warum sollte ich Interesse an deinen
besonderen Problemen zeigen, wenn du dich nicht mit dem
grundlegenden Sexismus-Problem befassen möchtest?"

Lilið riss sich zusammen, weil sich ihr der Gedanke ans Spucken
wieder aufdrängte. Allil hatte doch recht gehabt. "Bevor ich
hier streite, -- und es gäbe eine Menge zu verstreiten --, würde ich
noch gern wissen, was eigentlich mit mir passieren würde, würde
mir der Hammer gleich mit in den Schuh geschoben, sprich, würde
ich verantwortlich für das Massaker gemacht werden."

"Da wir vermitteln können, dass dein Anliegen gewesen wäre, meine
Tochter zu retten, ließe sich aushandeln, dass du
lediglich entmachtet wirst.", antwortete
Königin Stern. "Aber meines Wissens möchtest du ohnehin Nautika
sein und kämst ohne Grund und Adelstitel gut aus."

Marusch schnaubte. "Ich finde nicht, dass Lilið für irgendetwas
aus politischen Gründen herhalten müssen sollte.", sagte sie. "Aus
diesem Grund: Ich bekenne mich hiermit dazu, für das Massaker
verantwortlich zu sein."

Königin Dornrose nickte. "Ehrlich gesagt hatte ich damit
gerechnet. Du hast eine unbeschreibliche Macht."

"Niemand sollte eine solche Macht haben.", wiederholte Marusch,
was sie kürzlich schon einmal gesagt hatte. "Ich
denke nicht, dass ich Teil einer Regierung sein sollte. Ich bin
nicht wegen Gechlechterdingen privilegiert, sondern wegen meines
Skorems. Und ein System, das darauf aufbaut, ist von vornherein
ein Unterdrückungssystem. Wenn wir hier über systematische
Machtungleichgewichte wie Sexismus reden wollen, dann
sollten wir vielleicht auch über Behinderungen reden und
uns die Frage stellen, warum, nachdem ich meine Krönung
abgelehnt habe, wir nicht Lajana ermöglichen konnten,
Königin zu sein. Königin und nicht nur ein Werkzeug, das
nur auf dem Papier Königin heißt, wofür sie dann auch
noch dankbar sein soll."

"Aber mit deinem Bekenntnis verbaust du auch
deiner Schwester den Weg." Königin Stern schüttelte resigniert
den Kopf. "Du bist manchmal so kurzsichtig, mein Junge."

"Mein Kind. Nicht Junge.", korrigierte Marusch.

"Es tut mir leid.", entschuldigte sich Königin Stern. "Diesbezüglich
komme ich aus einer anderen Generation als ihr alle. Ich
bin alt. Ich strenge mich an, aber es fällt mir so schwer. Das
sind alles Gewohnheiten, weißt du? Fast mein ganzes Leben warst du mein
Junge. Das kriege ich nicht so einfach raus."

Es reichte. Lilið sprang auf und warf dabei ihren Stuhl nach hinten
um. Sie trat nach, als er sich doch wieder aufrichten
wollte. Die Spantunken im Beet stoben erschreckt auseinander und flohen
in eine Hecke. Ihr Schweigen unterstrich Liliðs donnernde
Stimme nur noch. "Du sollst die Klappe halten, habe ich gesagt! Oh, das habe ich noch
nicht gesagt. Zentrier dich hier nicht selbst und halt's Maul!"

"Ich mag deine Freundin nicht.", richtete sich Königin Stern leise
an Marusch. "Dein Umfeld hat sich auch so sehr geändert. Deine Freundin
hat keinen guten Einfluss auf dich."

"Du meinst Lajana mit Freundin?", fragte Lilið. "Denn mich
meinst du sicher nicht. Das würde der Behauptung, du strengtest
dich an, ziemlich widersprechen, wenn du direkt, nachdem
es ums Thema ging, nicht einen Moment zusammenzuckst, bevor
du mir ein Femininum verpasst."

Nun stand auch Königin Stern auf, auf viel edlere
Weise. "Für meinen Jungen, ich
meine, mein Kind würde ich das tun. Ich sehe nicht ein, warum
ich mich für dich anstrengen soll. Du schiebst dich in unsere
Familienangelegenheiten, die dich nichts angehen, und bringst
durch deine Existenz und dein Leben das ganze System ins Wanken."

Lilið zog eine Schnute und fühlte sich überraschend selbstbewusst. Es
war eigentlich immer beschissen, wenn die eigene Existenz als etwas
Schlechtes dargestellt wurde. Aber dass ihr persönlich die Macht
zugeschrieben wurde, das System durch ihre Existenz ins Wanken
zu bringen, gefiel ihr. "Ich hätte gut Lust, euch hier auf den Tisch
zu kacken. Zum Beispiel auf deinen Teller." Sie lächelte Königin Stern
unfreundlich an. "Ich weiß, dass das nicht so der Höflichkeitsstandard
ist, aber ich habe nicht so sehr Lust, für dich höflich zu sein. Ich weiß
nicht, warum ich mich für dich anstrengen soll."

Lajana kicherte und es klang zugleich verheult. Wahrscheinlich
hatte sie angefangen zu weinen, als es lauter geworden war.

"Raus." Königin Stern sprach nicht laut, aber sofort setzte sich eine
Wache in Bewegung, um sich Lilið zu nähern.

Lajana schüttelte den Kopf. "Ich möchte, dass sie bleibt.", sagte
sie. "Ich verstehe das alles hier nicht. Aber du kannst sie nur
rausschmeißen, wenn ich das zulasse. Das hast du mal gesagt."

Die Wache stellte sich wieder in den Hintergrund zu den anderen.

"Dass, was diese Person sich hier leistet, reicht für eine
Hinrichtung.", stellte Königin Stern klar. Sie setzte sich wieder. "Ich
fühle mich durch sie bedroht."

"Du solltest dich lieber durch mich bedroht fühlen." Marusch sprach
mit einem Lächeln im Gesicht, als redete sie über gutes Essen. Passend
dazu steckte sie sich noch eine Traube in den Mund.

"Du bedrohst mich?", fragte Königin Stern entgeistert.

"Wenn du Menschen drohst, die mir die Welt bedeuten, dann werde
ich sie verteidigen.", antwortete Marusch um die Traube
herum. "Auch gegen dich, wenn es sein muss."

"Ich will das alles nicht.", flüsterte Lajana. "Ich habe Angst! Ich
will das alles nicht."

"Du machst deiner Schwester Angst. Und du verletzt auch meine
Gefühle, aber das ist dir wahrscheinlich egal." Zu
allem Überdruss kamen nun auch Königin Stern die Tränen. "Mach
dir doch mal Gedanken, bevor du etwas sagst, wie sich andere
dabei fühlen müssen!"

Einen Moment blieb es sehr still. Wut stand im Raum. Lilið wusste nicht,
ob sie sich wünschen sollte, dass Marusch es wieder täte. Alles, was
störte, zu Staub zerfallen zu lassen. Aber
Marusch würde es nicht tun, wenn Lajana dabei wäre. Sollte Lilið mit
Lajana gehen? Und... war sie, Lilið, wirklich so weit, dass sie
diesen Menschen hier den Tod wünschte? Eigentlich nicht. Es war ihr
bloß alles egal. Sie wollte existieren dürfen. Sie bekam keine Luft. Sie
bekam keinen Halt zu sich selbst, sie hatte keinen Raum, wurde psychisch
zerquetscht. Es gab auf der Welt keinen Ort, wo sie sein durfte. Noch
weniger einen, wo Lajana sein durfte. Selbst die Abe, die unauffällig
zu ihnen auf den Tisch gehopst war, sich nun
zurückhaltend weiches Obst aus den Schalen aussuchte und
mit mehr Manieren verspeiste als Marusch, wurde von Königin
Stern herabwertend beäugt, weil sie hier in ihren Augen nicht
hingehörte. Lilið hatte emotional keine Bindung zu Menschen, die
eine Welt etablierten und konzipierten, in der
sie einfach nicht mitgedacht wurde. Vielleicht war sie böse. Vielleicht
war sie innerlich diese Furie, von der Marusch geschrieben hatte.
Interessanterweise war der erste Gedanke, der dazu führte, sich
zu wünschen, dass Marusch es nicht täte, dass Marusch dann auch
damit noch leben müsste. Wenn sie sich diese Zerstörung wünschte,
dann sollte sie sie selbst verursachen.

Unvermittelt sprang Lajana auf. "Es reicht!", schrie sie. "Ich hätte
gern regiert. Ich wollte Gutes tun. Aber auch wenn ich nicht kapiere,
wo ihr drüber redet, sehe ich, dass ihr einfach so meine Schwester
verletzt und Lilið und mich. Und auch Drude, weil Drude nicht adelig
ist und ihr nur Adel mögt. Und dann tut ihr so, als wären wir
selbst schuld. Und als hätten wir irgendeine Chance gegen euch." Sie
holte Luft. "Wisst ihr was? Ich hatte nie eine! Nie! Ich wurde bis
jetzt immer ignoriert und nun würdet ihr mich vielleicht als Werkzeug
benutzen und ich soll mich dafür dankbar und respektiert
fühlen. Ich mag wenig verstehen, aber das verstehe ich. Und mir
reicht's. Ich möchte Königin sein, aber ich glaube, ich habe keine
Chance und beim Versuch werdet ihr auf Dauer die umbringen, die
ich lieb habe. Weil ich mich bei ihnen nicht so wertlos fühle. Der
Versuch ist es nicht mehr wert. Macht euch ohne mich fertig. Es
reicht mir!"

"Meine Kleine, das Problem ist, dass in der Politik Kompromisse
geschlossen werden müssen.", sagte Königin Stern. Ihre Stimmung
klang nach erzwungener Geduld. "Ich finde schade, dass du
nun auch den Weg der Kompromisslosigkeit
einschlägst, den dein Bruder dir vorgibt."

"Meine Schwester!" Lajana stampfte mit dem Fuß auf. "Das, was ihr
versucht, sind keine Kompromisse, sondern Unterdrückung mit
Schleifen dran." Sie blickte Marusch an. "Willst du hier noch
was?"

Marusch schüttelte den Kopf und stand auf. "Wir können gern
gehen."

Heelem und Drude standen ebenfalls auf. Heelem rückte die Stühle
an den Tisch, auch den, den Lilið umgetreten hatte. "Für das
Kacken auf Teller haben wir leider keine
Zeit mehr. Aber vielleicht ergibt sich in Zukunft dafür noch
eine Gelegenheit.", sagte er trocken.

Lajana kicherte. Sie hielt für alle anderen den Vorhang aus
Ranken am Torbogen zurück, und bevor sie zuletzt den Garten
verließ, hielt sie die freie Hand mit erhobenen Mittelfinger
der verbliebenden Monarchei als Abschied entgegen.
