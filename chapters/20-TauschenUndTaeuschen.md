Tauschen und Täuschen
=====================

*CN: Gedanken über Gewalt, rauchen, Ungerechtigkeit, Kidnapping
oder sowas, Vegetarismus, Ableismus, Sanism, Body Horror.*

Danmoge hieß die Insel, die sie erreicht hatten, und sie lag
nicht allzu weit von Nederoge entfernt. Lilið hatte an manchen
Tagen ihrer Segelausbildung Ausflüge hier her und zurück
gemacht. Eigentlich musste Lilið sich nicht mehr sorgen. Dass
sie am Folgetag diese Etappe schaffen würden, stand außer Frage. Es
stimmte sie dennoch unruhig, weil sie keinen Spielraum für ihr
Zertifikat hätte.

Sie besprachen kurz, ob sie doch noch in der Nacht hinübersegeln
wollten. Aber es wäre Unfug. Sie waren müde und erschöpft. Ihre
Muskeln zitterten. Es war Nacht und Lilið kannte die Gewässer
bei tageslicht gut, bei Dunkelheit weniger. Alles sprach dafür,
dass sie eher ein größeres Risiko eingingen, wenn sie jetzt
schon aufbrächen, dass auf der Fahrt etwas passierte, als wenn sie
es morgen früh täten.

Danmoge war keine einsame Insel. Sie hatte einen kleinen Hafen
für größere Schiffe wie Kagutten. Für die Jollen gab es in der
Hafenbucht einen kleinen Strand, auf den sie hochgezogen werden
konnten, der regelmäßig von Steinen befreit wurde. Die Häuser am
Hafen bildeten eher ein Dorf als eine Stadt. Es waren mit Stroh
bedeckte Klinkerbauten. Auf einem Schornstein nisteten majestätische
Orcheen. Drachen nit sehr langen Gliedmaßen, die es trotzdem schafften,
damit wuchtig und schön auszusehen. Lilið mochte sie, vor allem, wenn
sie elegant auf den breiten Nestern standen, ihre Hälse reckten
und blaues Feuer nach oben spien. Sie mochte das dumpfe, heiße
Geräusch, wenn sie es taten. Gerade aber schliefen sie und ihre langen Schwänze
ringelten sich durch das angefeuchtete Astgestrüpp des Nestes hindurch
über das Dach eines Imbisses. Einer der Schwänze zuckte, der längste,
wahrscheinlich die Eltern-Orchee. Die anderen sahen noch weniger
stabil beschedert aus.

"Es ist ein Nautikae-Imbiss.", informierte Lilið. "Dort werden
besonders Nautikae willkommen geheißen, aber auch Leute, die segeln lernen. Jene
bekommen dort Essen ohne Marken! Das war einer der ersten Gründe, warum
ich Nautika werden wollte."

"Du wolltest dir unabhängig von irgendwelchen Regeln Essen
einheimsen?" Marusch grinste und sprach deutlich in einer nicht
ernstmeinenden Art.

"Unabhängig ist das Stichwort.", antwortete Lilið, schon eher
ernsthaft. "Es ging mir dabei nicht unbedingt um Essen. Sondern es
war das erste Mal, dass ich mitbekommen habe, dass es in diesem Reich
noch eine weitere Möglichkeit gibt, außer schutzbefohlen zu sein,
eine Lordschaft zu sein, -- Ladyschaft? Gibt es dafür ein sinnvolles
neutrales Wort? --, oder kriminell. Nautikae sind auch manchmal
schutzbefohlen, aber sie sind es auch oft nicht. Manche von ihnen
reisen ohne Hab und Gut, für das sie Schutz bräuchten, auf fremden
Schiffen gegen Kost und Koje. Das fühlte sich so unbeschreiblich
frei an. Und in diesem Imbiss habe ich angefangen, das Konzept zu
verstehen."

Marusch nickte. "Das kann ich nachfühlen.", sagte sie. "Ich glaube,
es passiert aber auch leicht, dass der Beruf romantisiert wird. Denn
eine gewisse Art, schutzbefohlen zu sein, ist es schon, was mit
der Kost und Koje gegen die Dienste kommt. Nur eben wechselnd."

"Schon!", gab Lilið zu. "Aber guck dir diesen Imbiss an! Natürlich
dürfen alle rein und Essen bekommen. Niemandem darf verwehrt
werden, gegen Marke an einem öffentlichen Imbiss Essen zu bekommen. Aber du
musst nur ein bisschen nach Segeln aussehen und du bist auch ohne willkommen. Solche
Orte habe ich noch nie für andere Berufsgruppen als für Nautikae gefunden."

"Willst du einkehren?", fragte Marusch.

"Ich bin noch unentschieden.", murmelte Lilið. "Eigentlich möchte
ich mal wieder. Aber unser letztes Einkehren in so etwas war ein Desaster. Und
mich könnten Leute wiedererkennen. Ich weiß nicht, ob das nicht
ungute Folgen haben kann."

"Falte dich doch ein wenig.", erwiderte Marusch. "Ich glaube, es
wäre sinnvoll einzukehren und ein wenig zuzuhören, was so geredet
wird. Wir haben sehr lange nichts mehr über den Stand der Suche nach
dem Blutigen Master M gehört. Oder vielleicht erfahren wir etwas Wichtiges
über Lord Lurch."

Lilið nickte. "Soll ich auch dich ein wenig zu falten versuchen? Ist
wahrscheinlicher, dass es nicht hält, oder dass Leute sich hier
dein Gesicht merken könnten und wir bei der Buchrückgabe
oder später im Leben Schwierigkeiten bekämen?"

Lilið konnte Marusch und sich eigentlich ganz gut falten, und auch
inzwischen so, dass sich
die Faltung, wenn sie nicht zu kompliziert war, einigermaßen hielt. Aber
Menschen lebten und bewegten sich. Wenn ein gefaltetes Papier oft
und viel bewegt wurde, lösten sich Falten. (Natürlich kam es auf
die Faltungen an.) So war es letztendlich bei Menschen auch: Nach
und nach lösten sich Faltungen auf. Und wenn sie nicht angeleitet
gelöst wurden, konnte das zwischendurch sehr auffällig sein, wenn
eine Nase etwa plötzlich auf der Stirn saß, oder das Konzept Nase
gegebenenfalls nicht einmal mehr erkennbar wäre.

Faltungen, die weiter weg vom Original waren, hielten gegebenenfalls
fester, aber taten auch nach einiger Zeit weh, weil der Körper in
der ungewohnten Haltung verkrampfte. Als Lilið das herausgefunden
hatte, hatte sie sich zunächst gefragt, ob sich das Problem
ähnlich wie das Atemproblem lösen ließe, aber dazu hatte Marusch
die enttäuschende Antwort gewusst, dass es leider war, was Falten
mit sich brachte.

Marusch entschied sich trotz der Risiken dafür, dass Lilið sie ein wenig falten
sollte. Sie wickelte sich einen Schal um den Hals. Wenn ihnen
auffallen würde, dass die Faltung sich löste, würde Marusch ihn
sich um das Gesicht wickeln, einen Nießanfall vortäuschen und
den Imbiss verlassen. Lilið würde nachkommen und das Gesicht wieder
richten. So der Plan.

---

Als sie den Imbiss betraten und sich einen Tisch am Rand suchten, ging
Lilið auf, wieviele Menschen ihr vielleicht im Laufe
des Lebens begegnet sein könnten, die dabei nicht ausgesehen
hatten wie sie selbst. Auf der anderen Seite wiederum versicherte
Marusch ihr, dass ihre Faltungen schon sehr fortgeschrittene Magie wären, die
sie eben gelernt hatte, weil sie sich seit ihrer Kindheit genau damit
befasste. Allil beherrschte ein paar Illusionspraktiken, aber konnte
sie nicht so lange aufrecht erhalten. Licht war zwar einfacher zu
beeinflussen, aber auch flüchtiger.

Lilið dachte darüber nach, ob sie der Gedanke erschrecken sollte,
dass ihr Leute etwas vorgemacht haben könnten. Aber es
änderte sich angesichts dessen, dass sie sich Gesichter
nicht merken konnte, für sie vielleicht ohnehin wenig.

Es war nicht das erste Mal, dass sie sich über so eine Dimension von Dingen, die
ihr einfach passieren konnten, weil Leute Magie beherrschten, Gedanken
machte. Von den Wachen auf ihrem Hof, wo sie groß geworden war, hätte
sie einfach im Alltag entzwei gerupft werden können. Von dem Moment der Erkenntnis
bis sie sich daran gewöhnt hatte, hatte es ein paar Tage gebraucht. Dass
sie es konnten, hieß ja nicht, dass sie es täten. Es war trotzdem
etwas gruselig, aber wo fing gruselig an? In der Schule hätte
ihr viel eher passieren können, dass eine Gruppe Kinder sich zusammenschloss
und ihr Gewalt zufügte. Prinzipiell war das geschehen, aber es gab
viel Spielraum nach oben, der realistisch gewesen wäre. Jene Gewalt
oblag keiner besonderen magischen
Fähigkeit und war viel bedrohlicher. Für sie zumindest. Weil sie nicht
Zielgruppe der Gewalt der Wachen war.

Im Imbiss war nicht viel los. Irgendwo in einer anderen Ecke saß ein älterer
Mensch in einem schicken Anzug und stopfte sich eine Pfeife. Lilið
befürchtete, er würde sie drinnen rauchen, aber er legte sie
bloß giffbereit beiseite. Lilið schloss, dass er bald gehen wollte.

Mittig saß eine Gruppe aus fünf Personen, die eine interessante
Stimmungsmischung aus Ausgelassenheit und Schwermütigkeit verströmte. Es
handelte sich um eine Person aus der benachbarten Monarchie des
Königs Sper, ein Nautika, zwei Personen, die Nautika werden
wollten und einen Schiffskoch. Das wusste Lilið, weil sie sich
alle noch einmal kurz vorgestellt hatten, als die zweite Person,
die Nautika werden wollte, hinzugestoßen war.

Bevor ein weiteres Gespräch hätte in Gang kommen können, nahm
eine bärtige Person Bestellungen auf. Erst bei der Gruppe,
die alle etwas essen wollten, und dann kam sie zur Marusch
und Lilið.

"Ich bin Smutje Andert. Ich bin der Mann fürs Kochen, und
heute auch fürs Bedienen.", stellte er sich vor. Er
klang herzlich und warm, wie Lilið das in diesem Imbiss
gewohnt war. Aber mangels Fähigkeit, sich an Gesichter
zu erinnern, wusste sie nicht, ob sie ihn kannte. Jedenfalls nicht
gut genug, um die Stimme wiederzuerkennen. Besagte Stimme, die
nach noch nicht ausgebrochenem, fröhlichem Lachen klang, fügte
hinzu: "Wer seid ihr? Und kann ich euch mit Fischsuppe
erfreuen? Das ist das Tagesangebot."

"Ich habe meine Ausbildung zum Leicht-Nautika fast
abgeschlossen.", sagte Lilið, nicht ohne Stolz durchklingen
zu lassen.

Sie erntete ein besonders breites Grinsen, ein "Ha!" und
ein erfreutes, ungeniert verhibbeltes Händeklatschen. "Wenn
du ein Feier-Typ bist, feier gern hier, wenn es soweit ist!"

Lilið grinste. "Ich denke darüber nach.", versprach sie und
deutete anschließend auf Marusch. "Und das ist meine
Reisebegleitung."

"Marusch.", sagte diese und berührte sich zur Begrüßung an
der Schläfe.

"Aurin. Ganz vergessen in der Aufregung!", holte Lilið die
eigene Vorstellung nach und spiegelte Maruschs Geste.

Smutje Andert machte eine riesige, leicht unkoordiniert
wirkende Geste mit Armen und Kopf, die überschwängliche
Freunde ausdrückte. "Willkommen!" Weniger überschwänglich
fügte er hinzu: "Wie sieht es aus? Fischsuppe?"

Lilið blickte zu Marusch hinüber. Sie nahm nicht an, dass
Marusch zwischen verschiedenen Fleischarten unterschied, aber
wollte sich, wenn Marusch doch anders als sonst entscheiden sollte, die
Fischsuppe doch nicht entgehen lassen. Bei der Gelegenheit
fiel ihr auf, dass sie über das Thema noch nicht gesprochen
hatten.

Marusch schüttelte freundlich lächelnd den Kopf. "Lieber
nur etwas zu trinken, bitte."

"Keinen Hunger oder ist Fischsuppe nicht das Richtige?", bohrte
Andert nach.

Maruschs Haltung fiel eine Spur in sich ein, vielleicht, um
zu kommunizieren, dass sie nichts kritisieren
wollte. "Letzteres. Aber ich möchte keine Umstände machen.", sagte
sie. "Meine Küche ist etwas speziell. Und ich habe Proviant
dabei."

Smutje Andert lachte. "Unser Angebot ist nicht riesig, aber
wenn ich die Dame glücklich machen kann, wäre mir das ein
Vergnügen.", versprach er. "Ich kann sehr gut verstehen,
dass Fischsuppe nicht immer so das Wahre ist. Was würde
deinem speziellen Gaumen denn munden?"

Lilið registrierte, dass Smutje Andert Marusch als Dame las, trotz
Maruschs Stimme. Es erfreute sie irgendwie. Auch wenn sie damit
rechnete, selbst auch so einsortiert zu werden, die ungefragte
Einsortierung also eigentlich an sich nicht so gut fand.

"Brot mit Kräutergewürzaufstrich ist meistens
das einfachste, wenn ich auswärts bin.", informierte Marusch.

Smutje Andert runzelte kurz die Stirn, hörte dabei aber
nicht auf, fröhlich zu lächeln. "Ohne totes Tier?", fragte
er.

Lilið sah den Schweißfilm, der sich auf Maruschs Stirn
bildete, als diese nickte.

"Nur keine Scham!", motivierte der Smutje. "Das mit dem Brot
und Kräutergewürzaufstrich ist kein Problem. Ich hätte außerdem
noch ein paar in Ambusöl eingelegte Antomatinen und angedickte
Haselmilch mit Minzinienblättern."

"Das klingt sehr edel.", murmelte Lilið.

Smutje Andert warf einen kurzen, vielsagenden Blick auf die
Person im Anzug, die gerade aufstand und ihre Sachen
zusammensammelte. Lilið hatte also recht gehabt: Diese Person war
im Aufbruch gewesen.

"Ich möchte wirklich nicht zur Last fallen.", wiederholte
Marusch.

Andert winkte ab. "Iwo, ich liefere das euch viel lieber, als
so Anzug-Fuzzis." Er beugte sich zu ihnen hinunter, als er dies
fast flüsterte, aber Lilið war sich recht sicher, dass der Mensch, der
nun Richtung Ausgang trat, davon mitbekam. Andert wendete
sich um und verabschiedete sich erkennbar aufgesetzt höflich.

Aus Lilið nicht ganz klaren Gründen feierte sie Anderts Verhalten
innerlich sehr. Sie verstand, dass sie es mochte, dass in diesem
Imbiss Adel schlechter wegkam, -- wenigstens etwas, denn
Adel hatte einfach Möglichkeiten, die anderes Volk nicht
hatte. Ihr waren die Gründe deshalb nicht ganz klar, weil sie
sich im Prinzip selbst zu Adel zählen konnte. Sie
hatte sich nie zugehörig gefühlt, aber das änderte
nichts daran, dass sie gewisse Privilegien hatte. Gehabt hatte
zumindest, bis sie der Blutige Master M geworden war und nicht
mehr so leicht zurückkonnte.

Smutje Andert entschied schließlich einfach für Marusch und
diese wehrte sich nicht doll genug. Lilið bestellte zögerlich
das Gleiche. Andert freute das sichtlich, und als
nun endlich alles geklärt war, überließ er sie wieder sich selbst.

"Ein netter Imbiss, in der Tat.", raunte Marusch Lilið zu.

Lilið grinste als Antwort nur.

Sie lauschten wieder möglichst unauffällg auf die Gespräche
des benachbarten Tischs und mussten feststellen, dass diese
auch ihrem Gespräch zugehört hatten.

"Wenn ich gewusst hätte, dass ich was anderes als Fischsuppe
kriegen kann.", murmelte die Person aus dem benachbarten
Königreich bedauernd. Lilið erkannte die Stimme am
alevischen Akzent wieder, den sie mochte.

"Ich hätte mich nicht einmal getraut, zu fragen.", murmelte eine
andere Person zurückhaltend.

"Du bist doch Schiffskoch!", meinte eine weitere Stimme, die Lilið
einem der beiden werdenden Nautikae zuordnete. "Also, musst du da nicht
zwangsläufig mit allem, also auch mit totem Tier arbeiten?"

"Hey!", griff die Stimme mit dem Akzent ein. "Er hat gerade
gesagt, dass er sich kaum getraut hätte, zu fragen. Da ist das
unsensibel, die Konsistenz in Frage zu stellen."

"Konsistenz?", fragte die Stimme vom ausgebildeten Nautika, die
sich Lilið besonders einzuprägen versucht hatte. Mit Erfolg, wie
sie glücklich feststellte.

"Ob es konsistent ist.", antwortete die Stimme mit Akzent. "Heißt
das nicht Konsistenz? Wenn sich etwas ergibt? Zusammen
passt?"

"Ah!" Das war wieder die Stimme des Nautikas, dann erklang ein Stühlerücken für eine geänderte
Sitzposition. "Nein. Es wäre richtig zu sagen, die Handlung ist konsistent. Aber
Konsistenz hat eine andere Bedeutung. Da geht es um die Beschaffenheit
von Dingen. Um Festigkeit. Vor allem von so etwas wie Brei oder Teig. Also, wie zähflüssig, wie
fein, wie gut durchgeknetet oder so etwas ist."

"Aha!", verstand die Person von vorher. "Ich will nur meinen, dass
Koch zu sein nicht heißen muss, dass man alles essen mag. Und auch nicht,
dass man alles zubereiten mag."

"Danke!", murmelte der Schiffskoch, klang aber immer noch sehr, als würde
er sich schämen. "Mein Traum war immer, eine Crew zu versorgen, die
kein Tier isst. Aber das bleibt wohl ein Traum. Ich traue mich, wie gesagt,
kaum darüber zu reden, weil sobald Leute davon Wind kriegen, sie mich
nicht einmal mehr dann haben wollten, wenn ich Kompromisse eingehe."

"Ich bin wegen so etwas kein Fan von diesem Reich.", sagte eins der werdenden
Nautikae. "Ist es im Königreich Sper besser?"

"In dem Punkt vielleicht schon.", antwortete die Stimme mit Akzent. "Obwohl?
Ich habe fast eher den Eindruck, es ist nur in anderer Weise schlimm bei uns. König
Sper isst kein Fleisch. Viele eifern ihm nach und halten sich deshalb
für was Besseres." Die Person unterbrach sich einen kurzen
Moment nachdenklich. "Es ist wegen König Spers Vorliebe kein Problem, ohne Fleisch
essen zu gehen. Es gibt immer fleischlose Optionen. Oft genug
gibt es sogar keine Option mit Fleisch. Und das ist gut für mich, denn ich esse
selbst normalerweise kein Tier, wie ich bereits zugab, aber nicht wegen
König Sper. Im Ausland ist mir das zu schwierig. Ich würde mich mit
einem anderen Standard wohler fühlen, möchte aber auch nicht unhöflich
sein. Und doch fühle ich mich eigentlich zu Hause fast unwohler, weil diese
Lebensweise daheim mit Patriotismus verknüpft ist. Bah."

Das Gespräch verstummte, oder wurde für eine Weile so unkoordiniert, dass Lilið
nichts verstand, weil Smutje Andert mit den ersten zwei Tellern kam. Vom Nachbartisch
standen einige auf, ihm anbietend, beim Tragen behilflich zu sein, was Andert
dankend annahm. Auch das mochte Lilið: Dass so etwas hier nicht als unhöflich galt, sondern
einfach umgesetzt wurde, was praktisch war. Sie stand nur deshalb nicht mit
auf, weil die Tür zur Kombüse, wie die Küche hier hieß, ein Nadelöhr bildete und
sie nicht wusste, ob Maruschs und ihr Abendessen nicht erst später fertig
sein mochte.

"Wir brauchen offenere Grenzen und Bündnisse.", hörte Lilið die Stimme mit Akzent
wieder, als sich die Lage beruhigte. "Das war ein Papierakt, hierher zu kommen."

"Aber dazu müsste jemand die Kronprinzessin entweder davon überzeugen, nicht
zu regieren, oder Bündnisse einzugehen.", murmelte eines der werdenden Nautikae. "Und
letzteres würde für sie wohl bedeuten, König Sper zu heiraten. Ich habe da wenig Hoffnung."

"Es gibt Hoffnungsschimmer.", erwiderte das Nautika. Etwas in der Stimme machte
Lilið klar, dass es sich hierbei nicht mehr um Geplauder handelte, sondern
das Nautika etwas andeutete, wofür es in Schwierigkeiten kommen
könnte, wenn es zu viel darüber preisgab. "Ersteres
muss ja nicht zwangsläufig freiwillig geschehen."

Lilið bemerkte die leichte Veränderung in Maruschs Gesichtsausdruck. War
es wieder die Wut, die sie beim letzten Mal in einer Gesellschaft mit ihr
kennen gelernt hatte? Aber um das zu erkennen, war es zu schnell vorbei.

"Ich wünsche der Kronprinzessin keine Gewalt an den Hals.", murmelte der
Schiffskoch. "Auch wenn ich sie nicht sonderlich mag. Aber auch dumme
Leute sollten geschützt werden."

"Vielleicht sollte jemand sie mal vor sich selber schützen, wenn sie es
nicht kann.", sagte das werdende Nautika, das seltener sprach. Lilið
registrierte es, merkte aber trotzdem, dass sie beide werdenden Nautikae
noch durcheinander warf.

"Ich mag die arrogante Denkweise nicht.", mischte sich nun das fertige
Nautika wieder ein. (Wobei Nautikae nie mit ihrer Ausbildung fertig
waren. Lilið wusste nicht, welchen Grad dieses erreicht hatte.) "Ich
mag auch Gewalt nicht, aber ohne Gewalt werden die nächsten Jahre nicht
ablaufen. Es gibt Krieg, wenn die Königin weiterregiert. Einen anderen Krieg, wenn
die Kronprinzessin übernimmt, oder Attentate oder sonst etwas. Wenn ich da
pragmatisch drangehe, denke ich, ist es der beste Weg, die Kronprinzessin
unter Druck zu setzen, ihren Thronanspruch aufzugeben. Danach kann sie gern
ihren Lebensabend genießen, aber alles was dann käme, wäre besser, als wenn
sie nicht auf den Thron verzichtete. Es wäre also die geringste Gewalt."

"Ich verstehe.", meinte das werdende Nautika von eben. "Pragmatisch gesehen hast
du wohl recht. Aber kennst du Pläne?"

"Ich verstehe noch nicht.", warf das andere werdende Nautika ein. "Wäre
das nicht ungefähr so eine Situation wie jetzt? Die Königin versucht,
derzeit zu erklären, dass sie den Kronprinzen schon in einem Jahr oder
höchstens zweien dazu bekommen würde, die Sache ernst zu nehmen, und
regiert so lange selbst, während die Lage zunehmend instabil
wird. Würde sie das nicht weiterhin auch tun, wenn die Kronprinzessin
abdankte oder auf ihren Thronanspruch verzichtete?"

Ein zustimmendes Geräusch des Nautikas erklang. "Schon. Aber es heißt,
der Kronprinz argumentiert, dass er seiner Schwester den Vortritt lassen
würde, wo sie doch regieren mag. Das könnte er dann nicht mehr. Auch von
ihm müsste dann eine klarere und neue Stellungnahme kommen." Leiser, aber für Lilið
trotzdem hörbar, fügte das Nautika hinzu: "Und vielleicht übt es auch einen
gewissen Druck auf den Kronprinzen aus, wenn seine Schwester als Geisel
gehalten wird. Ich kann ihn nicht einschätzen, aber gut möglich wäre, dass
er aus Angst oder Frust auch endgültig abdankt, oder aus Wut oder Machtgegenpol dann doch
die Macht übernimmt. Beides würde wahrscheinlich einen Krieg vermeiden."

"Die Fischsuppe ist übrigens köstlich!", hörte Lilið nun wieder die Stimme mit
Akzent.

"Oh richtig, ich habe bei dem spannenden Tischgespräch vergessen, zu
probieren.", fiel dem Nautika ein.

Nicht nur das folgende Geschirrgeklapper, sondern auch Smutje Andert,
der Maruschs und Liliðs Essen zu ihrem Tisch balancierte,
unterbrachen die Möglichkeit, weiter zu lauschen. Es sah hervorragend
aus, fand Lilið. Sie bereute nun doch nicht, dass sie heute nichts
Warmes essen würde.

Marusch hatte die Ablenkung durch die Bewegung im Raum ausgenutzt,
um beiläufig einen kleinen Zettel hervorzuholen. Mit einem
feinen Kohlestift schrieb sie etwas darauf,
als Smutje Andert sie wieder Richtung Kombüse verließ, um Getränke zu
holen.

"Ich möchte den Mantel des Nautikas haben. Ich spiele, ich hätte
Schwächeanfälle. Du kennst dich damit aus und weißt, im Zweifel brauche
ich frische Luft, aber es geht schon. Ich gebe dir später eine
Ausstiegmöglichkeit aus dem Spiel, bevor es sehr gefährlich wird.", stand
darauf. Lilið hatte etwas Mühe, es zu lesen, weil Marusch die Zeichen
benutzte, die sie im Buch gefunden hatten, bevor sie herausgefunden hatten,
dass sie sich feiner unterschieden. Sie hatten ihnen ja Namen gegeben, Namen,
die einfach Buchstaben aus ihrem Alphabet entsprachen. Auf diese
Weise war es eine für sie benutzbare Substitutionsgeheimschrift, unabhängig
davon, dass Marusch das Buch noch nicht entschlüsselt bekommen hatte.

Lilið atmete möglichst unauffällig tief durch und nickte schließlich. Im
Nachhinein wurde ihr aus dem Gespräch nun klarer, dass nahelag, dass das
Nautika und die Person aus dem Königreich Sper nicht nur eventuell
Gerüchte gehört hatten oder Möglichkeiten ersonnen, sondern auch in
etwas Konkretes verwickelt waren. Das Loben der Fischsuppe war
vielleicht eine Strategie gewesen, das Nautika am Weiterreden zu hindern. Es
war zu bereitwillig darauf eingegangen. Und ob eine Person, die kein
Fleisch aß, sich sonst ungefragt über eine Fischsuppe positiv äußern würde,
hielt Lilið auch zwar für möglich, aber nicht für wahrscheinlich.

Der kleine Zettel auf ihrem Besteckteller verkohlte unter ihren
Augen und zerrieselte zu Asche. Maruschs Feuermagie also. Aber so
etwas hatte sie zu Schulzeiten immerhin auch schon ein paar Mal
hinbekommen. Sie lächelte. Noch einmal mehr, als sie wahrnahm,
dass der typische Geruch ausblieb, den sie von verbranntem Papier
erwartet hätte.

Als Smutje Andert die Getränke brachte, erkundigte sich Marusch
nach dem Abort. Andert erklärte ihr wortreich den Weg, der eigentlich
nicht so kompliziert war, und schlug schließlich vor, sie dort
hinzuführen. Marusch tat auf dem Weg schon, als wäre sie etwas
wackelig auf den Beinen, vielleicht wie als würde sie zum
ersten Mal auf hohen Schuhen laufen. (Lilið konnte sich vorstellen,
dass Marusch es in Wirklichkeit drauf hätte.) Während ihrer Abwesenheit versuchte
Lilið wieder, beim Gespräch zu lauschen, aber es war belanglos. Wie
gedankenverloren tauchte sie einen Finger in die Asche, um
daran zu riechen. Sie war so fein, dass sie bei der Bewegung
ihrer Hand zum Teller fast völlig
davonflog. Lilið roch auch nichts, als sie das bisschen, was an
ihrem Finger haften geblieben war, vor ihre Nase führte.

Auf ihrem Rückweg schwankte Marusch noch mehr als auf dem
Hinweg, und auf halber Strecke, also ausgerechnet am Tisch der
anderen, hielt sie sich an dem einen unbesetzten Stuhl an deren
Tisch fest und sank überzeugend zu Boden. Sie riss niemanden
dabei um und berührte auch niemanden dabei. Das irritierte
Lilið in sofern, als dass sie den Akt als Ablenkungsmanöver
für einen Diebstahl eines Mantels gehalten hatte.

Ihr fiel bei dem Gedanken wieder ein, dass sie es nicht mochte,
Krankheit oder Behinderungen in ein Spiel einzubinden, bei dem
andere durch sie zu Schaden kämen. Sie war ziemlich überzeugt
davon, dass es das Leben von kranken oder behinderten Menschen
beeinträchtigte, wenn Leute ihren Anblick mit Verbrechen
verknüpften. Darüber sollte sie dringend mit Marusch
reden. Später.

Jetzt erst einmal stand sie auf, um zu Marusch herüberzueilen. Das
war, was eine gute Beziehungsperson tun würde. Aber sie war nicht allein
damit. Das Nautika und der Koch vom benachbarten Tisch halfen
Marusch dabei, sich aufzurappeln und sich auf den Stuhl zu
setzen, an dem sie sich festgehalten hatte. "Danke! Danke euch!", sagte
Marusch dünn. "Ich hoffe, ich störe nicht allzu sehr, wenn ich einen
Moment hier sitzen bleibe?" Sie schnappte nach Luft, aber wirkte,
als ginge es ihr schon etwas besser.

Das war ein enorm überzeugendes Spiel, fand Lilið. Marusch machte
immer noch keine Anstalten, etwas zu stehlen. Sie machte auch kein
übermäßiges Chaos. Sie hielt Abstand und drängte sich nicht auf.

"Bleib ruhig sitzen.", lud das Nautika ein.

"Brauchst du ein Medika?", fragte der Schiffskoch.

"Ist jemand von euch zufällig Medika?", fragte Marusch. Sie
brachte ein Lächeln auf ihrem Gesicht an, das gleichzeitig
höflich wirkte und als würde es sie anstrengen.

Der Schiffskoch schüttelte den Kopf und blickte sich um. Lilið
war inzwischen hinter Marusch getreten und konnte gut sehen,
dass alle anderen ebenfalls verneinende Gesten taten.

"Aber es gibt hier sicher eines im Hafen. Ich frage den
Smutje." Das Nautika hatte sich kaum hingesetzt und stand
nun wieder auf.

Das wäre wohl die Gelegenheit gewesen, dachte Lilið. Der Mantel hing
über der Lehne des Stuhls, auf dem das Nautika gesessen hatte. Auf
der anderen Seite, wie sollte irgendwer das Kunststück vollbringen,
einen großen Mantel unter den Augen aller anderen wegzustehlen?

"Nein, nein!", widersprach Marusch freundlich. "Ich habe,
ehrlich gesagt, Angst vor Medikae. Mein Mann und ich wissen,
wie wir das händeln. Es ist nur ein Schwächeanfall." Als
Marusch Lilið als ihren Mann bezeichnet hatte, hatte sie
sich kurz zu Lilið umgedreht und nach ihrem Arm gegriffen, den
Lilið ihr bereitwillig hingereicht hatte. Marusch spielte
überzeugend, dass die Drehbewegung mit ihrem Körper einen weiteren
Schwindel auslöste.

"Ihr müsst verstehen.", sagte Lilið. Sie versuchte, nicht allzu
gekünstelt mit recht tiefer Stimme zu sprechen. Nicht, indem
sie sie nach unten verbog, sondern indem sie mit der tiefsten
Frequenz einsetzte, die zu ihrer normalen Stimme gehörte. "Sie
hat Skeletrophie." Wenn kein Medika da wäre, das sich nicht
mit wirklich allem auskannte, würde niemand wissen, dass es ein
erfundenes Wort war. "Wir haben weit über die Inseln reisen
müssen, bis wir das eine Medika gefunden haben, das uns
darüber hat aufklären können."

"Kommt die Stimme und die Diät daher?", fragte eines der
werdenden Nautikae, erhielt aber direkt einen tadelnden
Blick mehrerer Anwesender, unter anderem vom Schiffskoch
und vom Nautika, das sich gerade ein weiteres Mal hingesetzt hatte.

Marusch nickte allerdings einfach. "Ich soll kein Fleisch
essen, hat das Medika empfohlen.", sagte sie. "Es ist mir
peinlich, aber ich habe vorhin ausversehen
mitbekommen, dass es hier kurz Thema war. Ihr seid gute Leute!"

"Aus medizinischen Gründen ist das ja auch nochmal was
anderes.", meinte der Schiffskoch. "Das akzeptieren die
Leute eher. Aber ich verstehe es trotzdem. Du hast dann
halt nicht einmal eine Wahl. Du kannst nicht mal eben an einem
Tag, an dem du den gesellschaftlichen Druck nicht aushältst,
anders entscheiden."

"Es ist nicht so schlimm." Marusch klang nicht überzeugt. "Aber
ich betone nochmal: Ihr seid gute Menschen. Ich möchte euch
wirklich nicht bei etwas Privatem stören. Aurin, hilfst du mir
auf?"

"Unfug!", mischte das Nautika sich wieder ein. "Ruh dich hier
aus, sei willkommen. Wir sind hier zwei Gruppen, mit Überschneidungen,
die sich im Vorfeld auch nicht alle untereinander kannten. Wir
haben nichts gegen zwei mehr." An die Runde gewandt fügte es
hinzu: "Oder?"

Marusch blickte einige Momente verdattert in die zustimmend
murmelnde Runde und bedankte sich anschließend. "So
zuvorkommend! Aurin, magst du dir
einen Stuhl, und mir die graue Jacke holen? Die mit der
kleinen Tasche, wo das Taldin gegen den Schwindel drin ist,
wenn ich es doch brauchen sollte? Müsste nicht tief im Gepäck
sein." Für die anderen fügte sie hinzu: "Ich soll es nur nehmen,
wenn es nicht anders geht."

Lilið wusste sofort, welche Jacke sie meinte, und dass kein
Medikament mit dem Namen in der Tasche war, sondern ein kleiner
Stein, den Marusch hübsch gefunden hatte. Sie hoffte, Marusch
wollte den Stein nicht schlucken. Aber sie hatte ja bereits angedeutet, dass
es nur für den Fall der Fälle wäre.

Sie stand auf, nickte und kramte nach der Jacke. Um den Stuhl
musste sie sich allerdings nicht kümmern. Einige der anderen
halfen ihr und holten ihn, sowie ihr Essen an den benachbarten
Tisch. Sie beobachtete, wie Marusch zwischen sich und dem
Nautika Platz machte. Auch das erschien ihr zunächst
counterintuitiv. Bis sie begriff, dass sie dann zwischen
dem Stuhl mit dem Mantel und Maruschs Jacke sitzen würde.

Es war so beengt, als sie schließlich saß, dass sie beide Kleidungsstücke auf
der Haut ihrer Arme fühlte. Marusch hatte sich die mitgebrachte
Jacke übergehängt und der Stoff war zwischen ihren Schultern
und Armen eingeklemmt. Die Jacke kannte sie an sich schon ganz gut. Marusch
hing nicht daran, glaubte sie, aber an manchen kühlen Abenden
hatte sie sie sich angezogen und mit Lilið gekuschelt.

Der Mantel des Nautikas war ein beeindruckendes Gerät. Er
war schwer, lang und wetterfest. Er fühlte sich lebendig
an, als hätte er schon viel erlebt. Er hatte eine Geschichte,
die Lilið erfühlen konnte. Also, nicht wie sie genau abgelaufen
war, sondern dass es sie gab. Wie bei Ruinen von Burgen oder
so etwas, die sich die Natur zurückeroberte.

Lilið fühlte in das Material des Mantels hinein, fühlte die
verschiedenen darin verarbeiteten Materialien. Manche nahm
sie lauter wahr, manche leiser. Sie fühlte die Flicken, die
nicht elegant, aber sehr robust und haltbar eingearbeitet
waren, und in welche Richtung der Stoff durch jene häufiger
verzogen wurde. Sie spürte die Verschlüsse zunächst
dumpfer als den Rest des Mantels, bis sie ein Bild von ihnen
ausmachen konnte und sie sich ihr klar erschlossen. Der Mantel
hatte viele Innentaschen mit Karten und anderen Papieren.

Als Lilið aus ihrer Faltwelt wieder in die Realität zurückkehrte,
während der sie lediglich im Stande gewesen war, zu essen, hatte
sich das Thema wieder der Kronprinzessin zugewandt. Lilið vermutete
Maruschs Werk dahinter, aber sie hatte wirklich keine Ahnung, wie
Marusch das fertig gebracht haben könnte. Sie hatte allerdings
auch nicht bemerkt, wie sie ihren Teller fast leergegessen hatte. Was
schade war, denn vom Geschmack bekam sie nun erst im Nachhinein
etwas mit, und der hätte sich sehr gelohnt.

"Die Kagutte mit der entführten Kronprinzessin an Bord legt morgen
ab.", sagte das Nautika. Es klang dabei ziemlich geschäftlich. "So
viel verrate ich noch, aber mehr Details könnten mich wirklich
in Schwierigkeiten bringen."

Lilið war noch zu sehr in dem Gedankenzustand, in dem sie versuchte,
nicht allzu auffällig aus einer anderen Welt zurückzukehren. Daher
drang das Gefühl von Überraschung und vielleicht Schock über die
konkrete Information nicht direkt in ihr Inneres durch. Und als
es das tat, war es bereits gedämpft. Sie fragte sich, was sie
davon halten sollte, aber für solche Fragen war wohl erst später Zeit.

"Das verstehe ich.", sagte Marusch. "Ich wollte auch wirklich nicht
drängen. Aber das Angebot steht, wenn ich irgendwie unterstützen kann,
wäre ich dabei. Ich halte das für einen grässlichen, aber durchaus
sinnvollen Lösungsansatz des derzeitigen Konflikts."

Das Nautika schüttelte den Kopf. "Sieh du mal lieber zu, dass du
dich schonst.", riet es. "Die Crew ist komplett, bis auf ein gutes
Nautika. Über einige Verbindungen hat eine Person an Bord eine
Empfehlung für mich ausgesprochen. Die Crew ist ansonsten
hervorragend ausgestattet." Das Nautika klang unter der sachlichen
Unsicherheit nicht wenig stolz auf sich, aber verstummte auf einen
Blick der Person aus dem Königreich Sper.

Erneut brachte letztere das Gespräch wieder in sicherere Gewässer. Sicherer
für das Nautika. Sie redeten nun über Wetter und Kartenspiele.

Als Smutje Andert eine neue Runde Getränke brachte, verabschiedete sich Marusch
noch einmal zum Abort. Ihre Jacke ließ sie dabei über den Stuhl gehängt
zurück und rieb sich beim Gehen über die Arme. Wieder schwankte sie dabei. Lilið fragte sich, ob
sie sie begleiten sollte, aber blickte sich zunächst lediglich immer wieder um. Das
machte die anderen nervös, aber das war ihr nur recht. Als besorgter Ehemann
war das zu erwartendes Verhalten bei ihr. Ihr Blick fiel dabei einige
Male auf Smutje Andert, der in der Nähe mehrere Tische wischte und
keineswegs so glücklich wirkte wie zuvor. Ob er von dem Gespräch
mitbekommen hatte und weniger von der Entführung der Kronprinzessin
hielt als das Nautika?

Für Lilið nicht unerwartet, klappte Marusch auf dem Rückweg zusammen, wieder
auf halbem Weg, also dieses Mal mitten im Raum.

Lilið sprang sofort auf, ihren Stuhl
dabei umschmeißend und eilte auf Marusch zu. Sie verbarg ihre innere Freude darüber,
dass es ihr gelungen war, Maruschs Stuhl gleich mit umzuschmeißen.

Ebenso nicht unerwartet für Lilið sprang auch das Nautika, das sich immer
gekümmert hatte, auf, sowie der Koch und eines der werdenenden Nautikae. Sie
beugten sich alle zu Marusch hinunter, die verschiedene Anweisungen gab. Das
Nautika bat sie, ein Glas Wasser zu organisieren, aber welches ohne
Geschmack, wofür das Nautika die Kombüse aufsuchen musste. Marusch bat Lilið,
ihre Jacke zu holen. Und das war ebenso nicht unerwartet für Lilið.

Sie ging zurück zum Tisch, blickte sich unauffällig um, als Marusch ein
ächzendes Geräusch von sich gab, ob es auch wirklich alle Aufmerksamkeit
auf diese zog, und faltete rasch zwei Kleidungsstücke. Den Mantel zur
grauen Jacke, und die Jacke zum Mantel. Es war herausforderd, weil
Farbpartikel passend gefaltet werden mussten. Formen waren einfacher,
wenn nicht auf Farbe geachtet werden musste. Aber immerhin war auch
der Mantel grau, nur etwas dunkler.

Sie veranstaltete noch ein wenig Chaos, indem sie absichtlich über einen
Stuhl stolperte, sodass hinterher vielleicht nicht mehr klar war, welcher
wo gestanden hatte, und brachte Marusch die vermeintliche Jacke. "Du
solltest an die frische Luft!", sagte sie.

Marusch zitterte, zog sich die vermeintliche Jacke an und kramte
in der Tasche nach dem vermeintlichen Stein, beziehungsweise dem
angeblich existenten Medikament. Lilið hoffte dabei,
dass Marusch die Jacke nicht zu sehr rütteln würde, damit sie
sich nicht entfaltete. Bevor Marusch das
Wasser trank, das ihr das Nautika brachte, führte sie etwas zum
Mund, das niemand sah, also wahrscheinlich nichts. Sie blickte halb zu
Boden, als sie anschließend langsam wieder zu Atem kam.

"Bringst du mich ins Bett, Liebster?", fragte Marusch schließlich
so dünn wie am Anfang.

Lilið nickte und half ihr auf. Einen Arm von ihr über der Schulter,
das Gepäck auf die andere nehmend, und Hilfsangebote freundlich
abwimmelnd, verließen sie den Imbiss.

Lilið glaubte, dass sie das Spiel mit dem Risiko noch nicht hinter sich
hatten.
