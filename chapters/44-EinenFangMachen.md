Einen Fang machen
=================

*CN: Fantasie-Gewitter, Misgendern, Nacktheit, Vergiften,
fast Ertrinken, Reden über Massaker.*

"Du hast es also geschafft und bist nun Leicht-Nautika." Heelem
trat neben Lilið an den Kartentisch und beobachtete ihr Werk. Er
hatte Lilið gebeten, schon einmal die Rückreise nach Belloge zu
planen, und ihr einen Vorsprung gelassen, bis er dazustoßen
und mitplanen würde.

Lilið blickte auf in sein Gesicht und fragte sich, wie sie
ihm beibringen könnte, dass sie nicht bloß Leicht-Nautika
war. Sie fühlte ein Unbehagen dabei, ein inneres Schwitzen,
vielleicht Angst? "Nautika.", korrigierte sie leise.

"Wurde mein Schreiben einfach mal wieder als zu knausrig angesehen
oder hast du später ein weiteres Zertifikat bekommen?", erkundigte
sich Heelem.

"Ersteres." Lilið seufzte. "Der Mensch, der es mir erstellt
hat, meinte, ich wäre benachteiligt in dieser Welt und ich
würde dem schon gerecht werden."

"Zurecht, Lilið." Heelems Stimme war zwar nun ebenso leise
wie Liliðs, aber irgendwie betonte das die Aussage um so
mehr. "Spätestens jetzt verdienst du es. Ich sehe, wie routiniert
und sicher du inzwischen bist. Und eine Kagutte hast du
auch durch die vornederoger Inselplatten navigiert. Das ist nicht
ohne."

Lilið fühlte, wie ihr Blut ins Gesicht stieg. Und Glückseligkeit.
Für ein paar Momente. Sie genoss sie einfach und hinterfragte
dieses Mal nicht, ob sie das Gefühl richtig platziert fand oder
ob es mit verinnerlichter Verknüpfung zwischen Selbstwert
und Leistung zusammenhing. Sie fragte sich, wie sie im Vergleich
mit Heelem abschneiden würde. Sie vermutete zwar, dass
Heelem viel besser sein müsste, aber stellte fest, dass sie
keinen Beweis dafür hatte. "Ich habe dich noch nie navigieren
sehen."

"Möchtest du?", fragte Heelem freundlich. "Und falls ja, willst
du dabei gern verstehen, was ich tue, oder willst
du sehen, wie ich normalerweise vorgehe."

Lilið kicherte. "Eigentlich beides. Kannst du erst letzteres tun,
weil ich wirklich neugierig wäre, wie das aussieht, und dann
wiederholen, was du getan hast, und erklären, was ich nicht
verstanden habe?"

"Schon.", meinte Heelem. "In meine Art zu navigieren, gehen
allerdings eine Menge Erfahrungen und Schätzungen
ein. Ich habe für viele übliche Startsituationen
schon so oft alle Fälle durchgespielt, dass ich
Richtungen ausschließen kann, von denen es lange dauern
würde, zu erklären, warum ich so allgemein weiß,
dass sie nie zum Ziel führen würden."

"Ich bin bereit dazu." Lilið versuchte, möglichst einladend
zu lächeln.

Heelem nickte. Er setzte, ohne in irgendein Buch zu schauen,
die Karte wieder zurück in den Zustand, der die Lage zu diesem
Zeitpunkt widerspiegelte. Das konnte Lilið noch nachvollziehen. Alles
danach wirkte irgendwie wie ein fauler Trick. Heelem bewegte das
Kartensteinchen über die Karte, ohne Zwischendurch innezuhalten,
drehte die Räder und verschob die Reiseinseln einfach
zeitgleich, aber nicht einmal alle, sondern nur die, die zum
Geschehen gerade beitrugen. Er entfernte sogar eine Inselscheibe
aus der Karte, die sich sonst verklemmt hätte, was hieß, dass
sie einen Einfluss auf den Weg genommen hätte. Später,
als das Kartensteinchen in Belloge anlegte, korrigierte er die
Karte von dort aus und setzte die fehlende Insel an ihrer
neuen Position wieder ein. "So sollte das gehen.", sagte
er. "Bei der kurzen Strecke brauche ich meistens nur einen bis
zwei Durchläufe."

"Ich gebe zu, ich habe derzeit keine Chance, hinterherzukommen." Lilið
lächelte trotzdem. Es hatte etwas Schönes an sich gehabt, etwas
Physik ignorierendes. "Aber ich denke, ich verstehe schon, wie du
zu diesen Fähigkeiten kommst und habe Hoffnung, dass ich vielleicht
in einem Jahr oder zweien so weit bin. Keine Ahnung, ob die
Schätzung sinnvoll ist."

"Das kann ich dir auch nicht sagen.", antwortete Heelem. "Es
gibt auch viele Nautikae, die nie so zu navigieren lernen
wie ich. Und es gibt viele, die ihre ganz eigenen effizienten
Wege finden."

Heelem passte auf, dass sie sich beim zweiten Durchgang, bei dem er
eine Menge erklärte, nicht verzettelten, sodass sie am späten
Vormittag aufbrachen. Er legte wert darauf, dass Lilið verstand,
was sie taten, einfach falls er aus irgendwelchen Gründen ausfiele. Der
Wind war etwas stärker als am Vortag. Der
Himmel war klar. Lilið spürte geradezu, wie der Rumpf des Schiffes
durch das Wasser schnitt und auf welche Art die Geschwindigkeit
zustande kam. Vielleicht würde sie sie dieses Mal nicht unterschätzen.

Sie war müde, aber nicht mehr so erschöpft wie am Vortag. Die
Erlebnisse belasteten sie immer noch nicht mehr als eine zu lange,
etwas aus den Fugen geratene Woche. Vielleicht sollte sie Drude doch
fragen, warum ihre Gefühle denn nun nicht falsch wären, aber Drude spülte im
Schiffsbauch und wollte dabei allein gelassen werden. Dey hatte sogar
den Niedergang mit den einsetzbaren Holzbrettern verschlossen.

Lilið beschloss, über das Deck zu spazieren und sich die Teeseufel
genauer anzusehen. Heelem hatte darauf wert gelegt, dass nicht er
die Rennyacht Teeseufel genannt hatte, sondern irgendeine Person,
die sich für besonders witzig gehalten haben mochte, von der Marusch und
er die Yacht auf Nederoge übernommen hatten. Lajana fand den Namen durchaus
sehr witzig und Marusch nannte ihn wohlklingend. Marusch war ja auch
immer für Tee zu haben. Lilið erinnerte sich zurück an ihre erste
Begegnung und musste grinsen. Ein gemütlich getrunkener Tee bei
einem Einbruch. Lilið fühlte sich wohlig bei dem Gedanken, gleich
vorzuschlagen, gemeinsam einen Tee zu trinken, sobald Drude die
Kombüse und damit verbundene Privatsphäre nicht mehr für sich brauchte. Lilið
bemühte sich, sich keine Sorgen um Drude zu machen. Dey brauchte
einfach Alleinzeit. Das war normal. Es war eben nur noch nie
bisher vorgekommen, dass Lilið es so sehr mitbekommen hätte. Sie
wusste zwar, dass Drude sich auf der Kagutte manchmal Auszeit genommen
hatte, -- dann meinst unterwasser -- , aber gleichzeitig
hatte sich Drude von Lilið nie zurückgezogen. Trotzdem war es
eigentlich nichts, was Lilið Sorgen bereiten sollte.

Ohne es richtig zu merken, war sie bis zum Bug gegangen. Es war
kein weiter Weg gewesen. Die Yacht war irgendwas zwischen 15 und
20 Meter lang. Sie war aus Holz gebaut, aber aus festerem, dünnerem
als Lilið es von den Kagutten kannte. Holz, das vermutlich für sich schon
sehr viel wert und dann noch mit komplexer Magie in eine gebogene Form
herausgearbeitet worden war. Es fühlte sich glatt unter den nackten Füßen an, fast
wie Fels, aber eben mit der Wärmeleitfähigkeit und dem Klang von Holz. Die
Reling war eine niedrige Holzleiste mit lang gezogenen Löchern darin,
oder alternativ ein sehr niedriges, dünnes Geländer. Sie hielt von selbst
niemanden davon ab, ins Wasser zu fallen, aber die Löcher eigneten
sich dazu, sich festzubinden, um auch bei Sturm sicher zum Bug zu
gelangen. Neben der Reling befand sich auf beiden Seiten jeweils ein
schmaler Weg zum Bug und zwischen den Wegen war das Deck erhöht. Es
ging eine Stufe hinauf, in der sich schmale Luken befanden, durch
die das Unterdeck gelüftet werden konnte. Das erhöhte Deck
war gleichzeitig das Dach des Unterdecks. Der Mast war durch die
Decke in das Unterdeck hineingelassen und stellte im Unterdeck
eine Säule mitten im Raum dar.

Die Teeseufel hatte zwei sehr hohe Segel. Das Vorsegel reichte noch am
Großsegel vorbei. Es war also eine sogenannte Genua. Im Prinzip hatte
die Yacht Ähnlichkeiten mit der Ormorane, die Lilið mit Marusch
gesegelt war, außer, dass sie größer war, ein Unterdeck hatte und
ein Kielschwert, das dauerhaft daran befestitgt war. Die Teeseufel konnte
nicht einfach an Land gezogen werden, sie müsste dafür mit Kränen aus
dem Wasser gehoben werden.

Lilið war im Rahmen ihrer Segelprüfungen ein paar Mal auf dieser
Art Schiffstyp mitgesegelt. Also, Schiffen mit einer Größe irgendwo
zwischen den Kagutten und den kleinen Jollen. Viel Erfahrung
hatte sie trotzdem nicht. Sie konnte erkennen, dass die Teeseufel
ein anderes Verhältnis von Länge und Breite hatte als die Yachten,
auf denen sie geprüft worden war. Die Teeseufel war schmaler und
schnittiger. Es war eben eine Yacht, die auch für Wettkämpfe ausgelegt
war. Sonst hätten Heelem und Marusch es vielleicht auch nicht so
schnell hergeschafft.

Lilið stellte sich ganz an den Bug, eine Hand am Vorlik der Genua, um
sich festzuhalten, die Füße ungleichmäßig belastet, weil die Teeseufel
krängte, und ließ sich den Wind durch Gesicht, Haare und Kleidung
wehen. Kleine Tröpfchen kamen mit ihm mit, jedes Mal, wenn die
Yacht durch eine Welle schnitt. Die Abe hatte Kreise
um den Mast gedreht und landete auf Liliðs Schulter. Der kleine
Kopf schmiegte sich gegen Liliðs Kinn und Lilið strich mit den Fingern
der freien Hand über das weiche Gescheder der Abe. Lilið überlegte, dass
sie sich gerade episch fühlen sollte, aber so ganz kam das Gefühl
nicht bei ihr an. Es war schon schön, aber es lag auch noch Anstrengung
vor ihnen. Am meisten mochte sie vielleicht, dass sie ihren Körper
wegen der Krängung asymmetrisch belastete. Auf diese Weise fühlte
sie ihn mehr.

Sie schloss einen Moment die Augen, um ganz in sich hineinzufühlen,
atmete. Sie runzelte die Stirn. Sie fühlte das Igeldings nicht mehr. Oder
nicht mehr so gut.

Sie spazierte zurück zum Heck, wo die anderen außer Drude saßen, und
stieg zu ihnen hinab in den Sitzbereich. Das immerhin war auch
schön: Sie mochte zu große Stufen steigen und die Hände dabei
benutzen. Die Abe klammerte sich dabei um ihren Hals und in
ihrem Kragen fest.

"Ich fühle das Igeldings nicht mehr richtig.", teilte Lilið den
anderen mit. "Das Signal ist gestört."

Heelem grinste. "Das hat Marusch auch gerade mitgeteilt. Das sind
interessante Auswirkungen. Es naht ein Gewitter."

Lilið suchte den Horizont ab. Ja, diese gelblichen Wolken dahinten
mochten ein Gewitter werden. Als sie genauer hinsah, erschreckte sie
sich fast. "Ein übles!"

Heelem nickte. "Wir haben leider keine Möglichkeit, auszuweichen.", teilte
er mit.

"Aber Segel einzuholen?", fragte Lilið.

Heelem nickte und atmete genüsslich langsam ein und aus, die Arme hinter
dem Kopf verschränkt. Die Pinne bediente er mit einem Bein. "In ein paar
Minuten."

Lilið schüttelte grinsend den Kopf und setzte sich schließlich
dazu. "Tiefenentspannung, bevor alles drunter und drüber geht?"

Heelem zuckte mit den Schultern. "Ich sehe keinen Sinn, sich
zu sorgen. Es ist nicht mein erstes Gewitter. Wenn uns Blitze
falsch treffen oder der Sturm uns ungünstig umpustet, haben wir
verloren, aber es gibt, außer Segel einholen, nicht viel zu
entscheiden." Heelem runzelte plötzlich die Stirn. "Moment:
Drude ist Aquaristika, richtig? Dey beherrscht ein gutes Stück
Hydromagie."

Wie als Reaktion auf die Frage rüttelte Drude von innen an den
eingesetzten Platten zum Niedergang, bis sie sich lösten. Lilið
fand eher unbehaglich, dass sie klemmten. "Es ist ja nicht so, als
würden mir eure Gefühle da unten entgehen. Und das Gewitter fühle
ich auch.", meinte dey. "Aber Segel eingeholt habt ihr noch nicht?"

Die Abe sprang sofort von Liliðs Schultern, als sie Drude wahrnahm. Die
Krallen drückten sich dabei in Liliðs Haut am Schlüsselbein, aber sie
konnte dem Drachen nicht böse sein. Er landete mit
zwei Sätzen in Drudes Armen und fiepste.

Drude legte beruhigend die Arme um die Abe. "Lil weiß auch,
was los ist. Dey bleibt vermutlich unter Deck."

Heelem richtete sich aus seiner legeren Haltung auf und führte die
Pinne wieder mit der Hand. "Na gut. Dann vielleicht besser zu
früh als zu spät. Marusch und Lilið, holt ihr die Segel ein?"

"Aye!", bestätigte Marusch.

Lilið folgte ihr und lauschte dabei dem Gespräch zwischen Heelem
und Drude. Heelem fragte: "Kannst du Blitze beeinflussen?"

"Jein." Drude betonte das Wort überzeugt, als handelte es sich nicht um
ausgedrückte Unklarheit. "Ich kann Wellen formen, die höher als der
Mast sind. Das halte ich nicht lange am Stück durch, aber es
erhöht, solange ich es tue, die Chancen, dass Blitze stattdessen
dort einschlagen. Ich wurde schon öfter instruiert dazu, mir das
Wetter anzugucken, und wenn Blitze häufig sind, die Hydromagie so
anzuwenden. Aber ich bin nicht routiniert."

"Dann sage ich dir jeweils, wann du das tun sollst.", erwiderte
Heelem.

Drude gab einen bestätigenden Laut von sich, der auch 'Aye' sein
mochte, aber er ging für Lilið halb in der ersten Windböe unter.

"Kann ich irgendwas tun?", fragte Lajana.

"Ja. Siehst du das hellbraune Seil?" Heelem wartete wohl ein
Zeichen ab. "Leg es um die Winsch, das ist dieser Zyllinder dort,
und zieh es stramm."

Lilið hörte auf, Heelems Unterhaltung mit Lajana zu folgen. Sie
hatte sich ein paar Momente gefragt, ob Heelem Lajana nur deshalb
etwas zu tun gab, damit sie sich nicht nutzlos fühlte und keine
Panik bekäme. Aber es schien ihr nicht der Fall zu sein und
war gerade auch nicht ihre Verantwortung. Der
Wind nahm so rasch zu, wie sie es nur von den richtig schlimmen
Gewittern kannte. Binnen weniger Sekunden war das satte Blau
des zuvor klaren Himmels überwuchert mit eiligen Wolken. Schönen
Wolken, kam Lilið nicht umhin festzuhalten. Blaues und
grünes Flimmern schoss darunter und in ihnen entlang wie ein Netz aus Ranken und
kribbelte unter Liliðs Haut, -- ähnlich wie das Igeldings und
doch wieder ganz anders. Waren Blitze und Bitzeln physikalisch wirklich
nicht so weit auseinander? Wenn das Igeldings Wellen auf Basis
dieser Energieform kommunizierten, war dann ein Gewitter für
es unerträglich laut? Wie cool war das eigentlich, sich über solche
Distanzen unterhalten zu können, wenn nicht gerade ein Gewitter im
Raum stand? Könnte sie das mit Marusch lernen?

Marusch und Lilið hatten gerade das Segel gefaltet, als der Regen einsetzte,
und zwar, wie kaum anders zu erwarten, schräg von der Seite. Sie
banden das Segel am Baum fest. Lilið auf der einen Seite davon und
Marusch auf der anderen. Während Liliðs vordere linke Seite trocken
blieb, musste es bei Marusch die hintere rechte sein. Lilið hatte
versäumt, die Segeljacke anzuziehen, weil es eben noch so warm
gewesen war. Wasser strömte in ihren Nacken und hämmerte
auf ihr eines Ohr und die zugehörige
Gesichtshälfte. Sie konnte nicht bestreiten, dass sie dieses Wetter
liebte.

Heelem schrie irgendetwas. Über die vielleicht fünf Meter Distanz
und das Flattern des Vorsegels drang fast nichts zu ihnen
durch. Aber als sie zu ihm blickten gab er Anweisungen mit den
Armen, die Marusch für Lilið übersetzte. Während Lilið am Bug die
Genua bergen sollte, sollte Marusch Lajana dabei helfen, das Fall
nur langsam kommen zu lassen, damit sich das sich senkende Segel
nicht in der Saling am Mast verfangen würde. Lilið blickte den
Mast empor zur Saling, so etwas wie Ausleger am Mast, über die
die Seitenverspannung (Wanten) den Mast am seitlich Umkippen
hinderte, während Vor- und Achterstak, die weiter oben am
Mast angebracht waren, das Umkippen nach vorn oder hinten
verhinderten. Lilið wollte schon über das Dach des Unterdecks
nach vorn gelangen, um Heelems Befehl Folge zu leisten, als Marusch
sie noch einmal am Arm festhielt. "Du gehst keinen Schritt weiter
ungesichert vom Mast weg." Lilið hatte sie vielleicht noch
nie so eindringlich erlebt.

Marusch trat zwei Schritte Richtung Heck, wo ihr Lajana einen Gurt samt
Seil übergab, den Lilið sich anziehen sollte. Es waren im Prinzip
nur zwei Schlaufen für die Beine mit so etwas wie Hosenträgern, und vor dem
Bauch waren zwei weitere Seile befestigt. Eines eher kurz, das
andere länger. Lilið befestigte das lange um den Mast mit einem
passenden Knoten und schwankte durch Wind und Regen zum Bug. Hier
hörte sie nichts als das Wetter. Zum Peitschen des Regens und
Singen des Windes in der Takelage war nun endlich auch Donnerkrachen
gekommen. Inzwischen war sie völlig durchnässt. Ihre baren Füße
hielten sie einigermaßen stabil auf dem Deck, aber sie merkte
doch, dass Festbinden eine gute Idee gewesen war. Die Wellen wurden
höher und unberechenbarer und auch der Wind peitschte in kaum
vorhersehbaren Attacken ins Segel. Sie konnte sich halten, aber
eine Unachtsamkeit, ein verspätetes Reagieren, und es könnte um sie
geschehen sein.

Sie zog kräftig am Segeltuch, das sich gegen ihren Griff
wehrte. Erst passierte gar nichts, aber wenige Momente später
ließ es sich nach unten ziehen. Wahrscheinlich gaben Lajana und
Marusch nun nach, da sie hier war. Lilið fragte sich, ob Heelem
nicht doch hätte etwas früher zum Einholen der Segel auffordern
können. Aber die Arbeit war überraschend schnell und relativ einfach
erledigt. Vielleicht spielte dabei eine Rolle, dass sie gut
darin war, zu falten. Sie machte aus dem Segel ein Bündel, das dem
Wind kaum Angriffsfläche bot, löste es vom Vorstag
und hängte es sich über die Schulter. Neben dem Mast stand inzwischen
Drude und blickte in die Wellen hinaus.

"Brauchst du etwas?", schrie Lilið demm ins Gesicht.

Drude schüttelte kurz den Kopf. Also löste Lilið ihre Verbindung
zum Mast, um beim Heck ankommen zu können, da berührte Drude sie
noch einmal am Oberarm. Lilið wandte sich ihr zu.

"Danke. Dass es dich in meinem Leben gibt.", sagte dey, gerade
laut genug, dass Lilið demm gegen den Wind hören konnte.

Einen Moment fühlte Lilið sich zu überwältigt, etwas zu sagen. Sie
lächelte. "Gleichfalls, Drude."

"Husch!", fügte Drude hinzu.

Lilið beeilte sich, samt Segel zurück ins tiefer gelegene Heck zu
gelangen. Marusch wartete unter Deck, um das Segel entgegenzunehmen
und zu verstauen. Lajana saß auf einer der Bänke und band sich
fest. Heelem war auch festgebunden, also wartete Lilið gar nicht
erst seinen Hinweis ab, dass sie es ebenso tun sollte, und
folgte ihrem Beispiel. Sie blickte zu Drude, dey sich inzwischen
an den Mast gebunden hatte. Wie in solchen Sagen, dachte
Lilið, in denen alle ins Wasser sprangen, wenn sie den Gesang der
Nixen hörten, und nur die an den Mast gefesselte
Person überlebte, die es dann aber mit Hunger zu tun bekam.

Lilið war gespannt, was passieren würde, denn sie hatte so ein
Gewitter noch nie auf dem Meer erlebt. Es hieß wohl
abwarten. So richtig losgegangen war das noch nicht. Es war
kühl, düster und stürmisch, aber entgegen der Windrichtung
kam eine noch viel dunklere Wand auf sie zu. Der starke
Regen zeichnete sich gegen den Horizont ab.

"Marusch, was machst du da unten?", schrie Heelem. "Die Luke
muss bald zu!"

"Ich mache Tee!", schrie Marusch zurück.

Es hatte etwas Witziges, Heelems Gesichtszüge ihm entgleiten
zu sehen. "Was.", murmelte er, gerade so laut, dass Lilið
es verstehen konnte, aber auch nur, weil sie die Lippen
mitlas. "Marusch! Auch wenn ich für Quatsch zu haben bin, ich
weiß nicht, wie das funktionieren soll!"

Marusch stieg die Treppe hinauf und kam mit zwei tiefen
Holzbechern wieder, die halb gefüllt waren. "Wer möchte denn
noch?"

Lajana meldete sich. Maruschs Lächeln war warm, als sie Lajana
einen Becher reichte. "Versuch, dir nicht die Zähne damit
auszuschlagen."

Lilið blickte zwischen Heelem und Marusch hin und her. "Nimm
ruhig.", murrte Heelem ihr zu.

Also meldete sie sich auch und bekam den anderen Becher.

"Drude? Willst du Tee?", schrie Marusch gegen den Wind an.

Drude schüttelte den Kopf. "Wenn alles vorbei ist, gern!", ergänzte
dey schreiend.

Marusch verschwand noch einmal im Unterdeck, um sich auch einen
Becher zu holen, reichte auch diesen Lilið, um die Hände zum
Verschließen der Luke freizuhaben, sowie um auch sich
festzubinden, und nahm ihn Lilið wieder ab, als alles getan
war. "Auf ein gemütliches Gewitter."

"Auf gemütliche Ungemütlichkeit!", fügte Lajana hinzu und
hob den Becher.

Es dampfte aus den Bechern, während der Regen hineintropfte. Lilið
fand es durchaus urgemütlich.

"Ich bin gespannt, wieviele Becher wir verlieren.", brummte Heelem.

Der Übergang zum schlimmeren Wetter war fast unscheinbar. Es wurde
einfach noch dunkler, das Donnern lauter, die Yacht schlingerte
mehr. Ein Blitz erhellte für volle fünf Sekunden gefühlt die ganze
Welt. Er sog eine Wassersäule aus dem Wasser, die in gleißendem
Licht erstrahlte, bevor sie sich wieder ins Meer ergoss, -- nicht
sehr weit von ihnen --, und eindrucksvolle, tiefe Dunkelheit
hinterließ. Heelem schrie Drude zu, dey möge mit der Magie
beginnen. Auch dass war beeindruckend. Immer in einiger Entfernung
erhob Drude Wassermassen aus dem Wasser, die die Teeseufel weit
überstiegen und im Geflimmere des Wetters schillerten. Sie
schaukelten und schlingerten. Lajanas Tasse ergoss
sich über das Deck. Lilið nahm ihr die Tasse ab, weil Lajana weinte
und sich zu Marusch in den Arm schmiegen wollte. Sie rutschten
trotz Fesselung und obwohl sie sich alle an etwas festklammerten, auf
den Bänken hin und her, und wie Lilið es schaffte, die Teetassen
festzuhalten und aus ihrer nicht viel zu verschütten, wusste
sie nicht. An Trinken war jedenfalls nicht so richtig
zu denken, da hatte Heelem wohl recht. Lilið probierte es
trotzdem, als sie sich an alles einigermaßen gewöhnt
hatte. Der herbe Geschmack, der ihre Lippen und dann direkt auch
ihr ganzes Gesicht bedeckte, war angenehm. Und als sie die Augen
wieder öffnete, sah sie den ersten Blitz krachend in eine von
Drudes Wellen einschlagen. Wenig später brauchte Drude gar nicht
mehr zu werkeln, weil die Wellen durch das Wetter von selbst so hoch
wurden. Heelem kommandierte mit vereinbarter Zeichensprache.

Als die Blitze um sie herumzuckten und Lilið wusste, dass sie sie
nur um Haaresbreite verfehlten, setzte auch bei ihr endlich Panik
ein. Warum eigentlich? Was half das? Wie Heelem gesagt hatte, sie
könnten eh nichts tun. Das Krachen und Donnern grollte fast
ununterbrochen. Der Regen peitschte wie Sand im Sturm auf
die Haut. Lilið mochte den Schmerz. Das war interessant. Sie mochte
auch die Kälte. Und als die erste riesige Welle sich über die
Teeseufel stülpte, vergaß Lilið ihre Angst auch wieder. Heelem steuerte, was
kaum möglich war, weil ihre Nusschale keine Fahrt hatte, aber
irgendwie schaffte er es doch, sie mehr mit dem Bug in die Wellen
zu drehen. Das salzige Wasser ergoss sich über sie. Ein paar
Sekunden konnten sie jeweils nicht atmen (außer Drude), bis das
Wasser wieder vom Deck herunterfloss. Es schäumte dabei an der
niedrigen Reling, floss zwischen den Ritzen ab, verweilte in
ihrem Fußraum noch am längsten, aber hatte durchs Heck hin
Platz, um wegzuströmen. Es hatte einen Grund, dass sie festgebunden
waren.

Vielleicht hätte Lilið es ab jetzt nur noch genossen, bis sie
überlebt hätte oder gestorben wäre, wenn Lajana nicht so
gewimmert hätte. Marusch redete auf sie ein. Lilið wusste nicht,
was sie tun sollte. Schließlich beschloss sie einfach, ein
Seelied zu singen. Heelem lachte laut, als er es hörte. Lilið
dachte zunächst, er hielte die Idee für ähnlich albern wie
den Tee, aber er stieg schief mit ein. Es half gegen die
Panik. Lajana setzte beim zweiten Lied mit ein und bestimmte
das dritte selbst. Sie grölten ein Lied nach dem
anderen in den Sturm, bis das Unwetter endlich
weniger garstig wurde. Hatten sie es tatsächlich
überstanden, ohne dass ein einziger Blitz eingeschlagen war?

Im heller werdenden Licht weit entfernt erblickte Lilið eine
Kagutte, die gerade wieder Segel setzte und auf eine Insel
zusteuerte, die sie in der Ferne sehen konnte. War das schon
Belloge? Oder war es eine ganz andere Insel, weil sie so sehr
vom Kurs abgekommen waren? Die Kagutte blieb nicht lange im
Sichtfeld. Sie war zu schnell.

Heelem brüllte Drude zu, dey möge wieder Wellen
erzeugen. Lilið beobachtete demm dabei. Sie hatte
sonst nichts zu tun. Heelem wartete noch mit einem Befehl, Segel
wieder zu setzen.

Drude wirkte müde. Halb in derer Fischform
hob dey Arme, spannte Muskeln an wie um zu flexen. Lilið
hatte keine Ahnung, wie sich diese Art Magie auszuüben
anfühlte. Es war so anders als Falten. Mit deren Bewegungen,
immer wenn die Anspannung am höchsten war, schossen Wellen
um die Teeseufel herum aus dem Wasser, die sie überragten. Eher
Wassersäulen. Drude erhöhte damit Wellen, die ohnehin da waren. Ihre
Spitzen wurden vom Wind abgerissen und Gischt sprühte zu ihnen
herüber, aber sie ergossen sich nicht aufs Deck. Keine
von ihnen fing einen Blitz ab, weil zufällig keine in
ihrer Gegend einschlugen. Aber besser, sie waren vorsichtig.

Eine hastige Bewegung in Liliðs Rücken ließ sie
herumfahren. Heelem war aufgesprungen, soweit die Fesselung
das zuließ und hatten seine Hand wie für einen geraden
Schnitt bewegt. Es knackste und ein schmerzhaftes Gefühl
durchrann den Schiffsrumpf, drang Lilið über ihre
am Deck verkramfpten Hände in die Glieder. Als sie
sich wieder zu Drude umblickte, konnte sie gerade noch sehen,
wie der obere Teil des Mastes abriss, von einem Blitz erwischt
wurde und ins Meer gerissen wurde. Er flammte grün und rot
auf, bevor er zwischen den Wellen verschwand. Heelem musste
ihn abgeschnitten haben.

Lilið fühlte ihren Blutdruck und ihre Angst noch einmal
ansteigen, mehr als vorhin. Es war noch nicht vorbei. Woher
Heelem gewusst hatte, dass der Blitz die Teeseufel treffen
würde, erschloss sich Lilið nicht, aber es hätte Drude
erwischt, wenn er durch den Mast weiter nach unten ins Schiff
durchgedrungen wäre.

Lilið zitterte am ganzen Leib und die Teeseufel mit ihr. Der
Mast war an seiner Spitze durch Vorstag und Achterstag
gehalten worden, etwas tiefer saßen die Wanten, die ihn zur
Seite hielten. Vor- und Achterstag hatte Heelem mit zerschnitten. Das
würde der Mast nur stehend überleben, wenn keine großen
Erschütterungen mehr das Schiff ergreifen würden. Wenn er nach
vorn oder hinten umkippte, würde er das Dach des Unterdecks
gleich miteinreißen. Lilið fühlte panisch in die
Takelage hinein, um sie zu stabilisieren, und schlotterte heftiger,
je mehr sie sich konzentrierte, konnte
kaum denken, hoffte nur darauf, dass es endlich
vorbei wäre. Sie verlor das Zeitgefühl. Und als endlich irgendwann
der Regen aufhörte und sie die Blitze hinter sich ließen, konnte sie
sich kaum daran erinnern, was sie eigentlich genau getan
hatte.

Sie hätte vielleicht damit gerechnet, dass Drude nun erschöpft
zu ihnen kommen würde und sie sich gemeinsam ein wenig erholen
würden, bevor sie Pläne machen würden, wie sie mit dem Schiffsbruch
verfahren sollten. Aber stattdessen löste dey hektisch die
Seile von derem Körper und... sprang über Bord.

"Was.", sagte Heelem ein weiteres Mal.

"Es ist halt umgekehrt zu diesen Sagen.", meinte Lilið. "Hier
geht die Person möglichst zügig über Bord, die am Mast festgebunden
ist." Sie nahm wie von außerhalb ihres Körpers wahr, wie sehr sie ihre
Angst überspielte.

Heelem blickte sie mit gerunzelter Stirn an. "Ergibt Sinn. Vor
allem, dass es die nixen-ähnlichste Person ist, die ich kenne,
die über Bord geht. Das ist dann noch umgekehrter." Er sagte es,
als meinte er es todernst. "Aber mal scherzlos: weiß irgendwer von
euch, warum?"

Alle anderen schüttelten den Kopf.

"Und was nun?", fragte Heelem.

"Warten wir ein bisschen ab?", schlug Marusch vor. "Lilið, du
kennst Drude am besten, oder Lajana, was meint ihr?"

"Zuletzt, als dey ohne ein Wort von Bord gegangen ist, war es, um
mir mein Leben zu retten.", überlegte Lilið.

"Ich habe keine Ahnung!", sagte Lajana. "Aber sie wird schon
wiederkommen. Nicht 'sie'. Drude."

Lilið überlegte und kam zum selben Schluss. Drude war treu. Es
gab gerade nur irgendetwas sehr Wichtiges. Wobei dey sich schon
den ganzen Tag seltsam verhalten hatte. Oder nicht?

"Hm.", machte Heelem. "Bei jeder anderen Person, die über Bord
gesprungen wäre, hätte ich große Bedenken, dass sie ertrinken
könnte. Aber Drude hat Kiemen, richtig?"

Lilið nickte. "Drude passiert nichts." Sie sagte es sicherer, als
sie sich fühlte. Aber sie könnten ohnehin nichts für Drude tun, selbst
wenn dey Hilfe bräuchte.

Heelem blickte am Mast hinauf. "Marusch, du übernimmst das Ruder.",
bestimmte er. "Lilið und ich gehen mal schauen, ob wir den
übrigen Mast sichern können und ob wir einen Fetzen Stoff gehisst
kriegen. Die Kagutte, die uns hätte schleppen können, ist uns leider
schon zu weit davon gesegelt. Ich sehe eine Insel, das müsste Oesteroge
sein. Also nicht unser Ziel, aber wir haben mit dem Schiffsbruch keine
Möglichkeit, Belloge zu erreichen. Oesteroge hat immerhin eine Werft. Wenn
wir bis in einer Stunde einen vernünftigen Fetzen gehisst haben, sollten
wir bis Mitternacht dort sein können."

---

Schiffsbau war durchaus auch ein Beruf, für den Lilið sich mal
interessiert hatte, aber sie war nie tief ins Thema eingestiegen. Navigation
hatte sie mehr interessiert. Heelem und sie hatten eine Ansammlung
an Ersatzseilen zum Mast geschleppt und machten sich nun Gedanken darüber,
wie und mit welchen Knoten sie den Mast nun nach vorn und hinten absichern
könnten. Dabei mussten sie so gefährliche Dinge tun, wie bei immer
noch chaotischem Seegang auf mehrere Kisten zu steigen, selbst
gestapelt (Lilið saß auf Heelems Schultern), um Seile weiter oben
am Mast zu befestigen. Denn es war ja auch die Führung oben am Mast
mit in den Fluten verschwunden, sodass die Fallen nicht mehr dazu geeignet waren,
Segel oder anderes zu hissen.

Als sie endlich Drudes Stimme wieder hörten, war der Mast
ausreichend gesichert, aber noch kein Fetzen Tuch gehisst. "Hilft
mir wer?", fragte dey. Die Stimme kam von unterhalb des Hecks.

Heelem blickte ins immer noch sehr aufgewühlte Meer. "Klar! Moment!" Und
ließ die Arbeit am Mast vorübergehend liegen.

Auch Lilið half. An zwei Seilen befestigte Drude einen Körper einer
erschöpften Person, die kein Wort über die Lippen brachte, aber
noch atmete und bei Bewusstsein war. Heelem und Lilið zogen sie
herauf. Sie konnte kaum einen Schritt gehen, also platzierten sie
sie erst einmal auf einer Bank, bevor sie Drude an Bord halfen.

"Ich hatte einen Menschen im Wasser wahrgenommen.", berichtete
Drude. "Schon während des Gewitters, aber da konnte ich logischerweise
nicht weg. Trockene Kleidung, Tee und etwas zu essen würde ich
verordnen. Marusch machst du Tee? Ich kümmere mich um trockene
Kleidung für dich."

Die Person ließ sich von Drude unter Deck tragen. Lilið fühlte
sich mit einem Mal unbehaglich und wusste nicht so genau warum. War
es einfach, dass sie nun eine fremde Person mehr an Bord waren? Lilið
hätte sich außerdem am liebsten selbst umgezogen. Lajana hatte bereits
trockene Kleidung an. Aber Lilið wollte da unten nicht stören und
es gab auch oben noch etwas zu tun, wofür es vielleicht praktisch
war, noch ein wenig in der nassen Kleidung zu schlottern.

"Es ist also noch umgekehrter!", sagte sie grinsend zu Heelem, als
er wieder zu ihr an den Mast trat, um einen Teil des Großsegels zu
hissen. "Wir hatten eine nixen-ähnliche Person in der Crew, die am Mast
festgebunden war, aber statt eine Person in den Fluten zu
verlieren, haben wir nun eine zusätzliche aus dem Wasser
gefischt."

Heelem lächelte. "Bedrohliche Situationen machen so interessante
Sachen mit Menschen.", sagte er. "Manche weinen, manche wollen
Tee trinken, -- wobei ich glaube, dass das zur Beruhigung
beigetragen hat. Manche verhalten sich extra gelassen, als
könnte sie nichts aus der Ruhe bringen. Damit meine ich
mich. Manche arbeiten einfach sehr viel und du singst Lieder
und erzählst humorvolle Geschichten."

"Der Tee hat tatsächlich beruhigt.", bestätigte Lilið.

"Deine Strategien funktionieren auch gut." Heelem lächelte
sie an.

Sie fühlte sich trotzdem irgendwie nicht, als wären sie nun
sicher. Das unbehagliches Gefühl von vorhin, vielleicht, als
sollte sie etwas wissen, ließ sie weiterhin die ganze Zeit nicht
los, während sie mit Heelem ein Reff im Segel mit ein paar
Seilen improvisierte. Es war immerhin ein befriedigendes Gefühl,
als sie das Segel dicht holten und es sich mit überraschend viel
Wind füllte. Es flatterte seltsam, aber sie standen nicht mehr
still zwischen den Wellen. Sie nahmen Fahrt auf.

Erschöpft und weiterhin durchnässt ließ sich Lilið im
Sitzbereich im Heck nieder. Sie übernahm das Steuer von
Marusch. Marusch hatte gemeint, Lilið solle sich umziehen, aber
Lilið brauchte zunächst eine Pause von der vielen Anspannung und
Bewegung. Also zog sich Marusch zuerst um und Lilið war für
wenige Augenblicke mit Heelem allein an Deck. Einfach
nur kurz ein wenig zur Ruhe kommen.

Die neue Person stieg den Niedergang hinauf und wirkte
dabei so schwach, dass Lilið sich fast überlegte, die
Pinne zu fixieren und ihr entgegen zu kommen,
um sie zu stützen, aber sie wäre zu langsam gewesen. Die
Person ließ sich ihr gegenüber nieder. Lilið
erstarrte. Ihre Anspannung stieg wieder rapide an. "Allil?"

Allil seufzte und nickte. "Du solltest dir auch was Trockenes
anziehen.", meinte sie. "Also, das sagt Marusch, und du siehst
sehr mitgenommen aus."

"Du denkst wohl, ich würde dich aus den Augen lassen." Lilið
runzelte die Stirn und fragte sich, ob sie es irgendwann
würde tun müssen.

"Eigentlich dachte ich, du schmeißt mich gleich wieder ins
Wasser.", murmelte Allil.

Lilið wusste nicht, wie sie darauf reagieren sollte. Deshalb
starrte sie Allil einfach an.

"Ihr scheint euch zu kennen und keine guten Erfahrungen miteinander
gemacht zu haben." Drude war lautlos in den Niedergang getreten
und hatte sich dort verkeilt, damit das Schlingern durch die
Wellen demm nicht umwarf. Auch
dey sah mitgenommen aus. "Ich habe dich zwar aus dem Wasser
gefischt, aber wenn du Lilið auch nur ein Haar krümmst, komme
ich deinem Vorschlag nach und werfe dich zurück in die Fluten,
wo du hergekommen bist."

Allil nickte. "Ich habe verstanden und ich habe nicht vor, hier
irgendwem ein Haar zu krümmen.", sagte sie.

"Hm.", machte Drude skeptisch. "Leider merke ich dir an, dass
du wahrscheinlich gut genug im Vorspielen wärest, um mich zu
täuschen." Dey wandte sich an Lilið. "Du solltest dich umziehen. Wenn
du möchtest, halte ich diese Person so lange fest, sodass sie
sich nicht wehren kann. Du weißt, dass ich das kann."

"Allil arbeitet mit Giften.", informierte Lilið. "Das macht
sie gegebenenfalls weniger wehrlos, als du denkst."

"Gut zu wissen." Drude kam die letzten Stufen hinauf und nahm
neben Allil Platz, direkt an der Pinne, Lilið gegenüber. "Ich
bin Wache gewesen, Lilið. Zieh dich um."

Lilið verharrte noch, aber Drude blickte sie so auffordernd
an, dass sie schließlich nachgab, Heelem die Pinne überließ und
aufstand. Sie spürte ihre Beine kaum noch. Sie
hätte vielleicht um Hilfe bitten
sollen. Auf den unteren Stufen des Niedergangs verließ auch
ihre Arme die Kraft, sie konnte sich nicht mehr halten und
fiel hin.

Marusch war sofort bei ihr und wenig später auch Lajana. Lilið
erkannte, dass sie gerade damit beschäftigt gewesen waren, aus
ihren übrigen Vorräten ein Abendessen auf Tabletts anzurichten. Aber
sie blieb erst einmal einfach liegen. Und weinte, stellte sie fest.

"Du musst keine Zeit mit ihr verbringen." Marusch sprach
in beruhigendem Ton, aber es gab Lilið kein Gefühl von
Sicherheit. "Du kannst zum Beispiel hier unten bleiben, oder
alternativ sie."

"Ich möchte sie nicht aus den Augen lassen." Lilið richtete den
Oberkörper auf. Sie zitterte nun so sehr, dass sich selbst diese
Haltung instabil anfühlte. Lajana stützte sie. "Nur Trockenes
anziehen und dann an Deck aufpassen, dass sie niemandem was antut."

"Sie tut niemandem etwas an." Maruschs Worte klangen leise und sanft,
aber auch ohne jeden Zweifel. "Wir haben ihr gerade das Leben
gerettet. Sie ist selbst am Ende. Sie ist gerade von uns abhängig. Und
sie hat kein Motiv."

Lajana strich Lilið über das nasse Haupt, stand auf und kramte
trockene Kleidung aus dem Stauraum. Sie kam mit einem Kleid zurück,
einem ähnlichen, wie Marusch es trug. "Hose gab es nicht mehr in
deiner Größe. Ist ein Kleid für dich in Ordnung?"

"Wird schon gehen. Könnt ihr mir beim Umziehen helfen?" Lilið
fühlte sich seltsam dabei, um diese Art Hilfe zu bitten. Sie war
vielleicht noch nie so erschöpft gewesen.

Beide stimmten zu, aber am Ende half ihr vor allem Marusch. Lajana
brachte das Abendbrot nach oben. Marusch half ihr aus der Kleidung,
die am Körper klebte, und das Zittern machte das Unterfangen auch nicht
leichter. Sie trocknete Lilið ab, hielt sie dabei sanft fest, aber sonst
hielt sie sich mit Zärtlichkeiten zurück.

"Du nutzt die Situation gar nicht aus.", bemerkte Lilið. Sie
hatte aufgehört, zu weinen, stellte sie fest.

"Natürlich nicht.", antwortete Marusch.

Lilið fragte sich, ob sie enttäuscht war, oder ob das eigentlich
ganz gut so war. Sie war zu erschöpft, um solche Entscheidungen
zu fällen. Im Nachhinein kam ihr seltsam vor, die Frage gestellt
zu haben. Marusch hatte recht, natürlich tat sie so etwas
nicht. Liliðs Kopf hatte aus dem Wunsch nach Nähe, der sich mit
der fehlenden Kraft dafür stritt, diese merkwürdige Frage
geformt. Oder so. Sie war zu erschöpft, um sich selbst zu verstehen.

---

Als Lilið sich wieder an Deck schleppte, wurde es still und relativ
eng. Drude, Heelem und Allil saßen auf der einen Bank, Heelem bediente
immer noch die Pinne, und Lilið setzte sich zwischen Lajana und Marusch auf
die andere. Marusch reichte ihr vom Brot, von dem sich Lilið etwas
abriss, um von den Pasten und dem Gemüse zu nehmen.

"Es tut mir leid, dass ich dich fast umgebracht hätte.", leitete
Allil ein Gespräch ein. Eines, der seltsamen Sorte. "Ich kann
auch verstehen, wenn du mir nicht verzeihen kannst."

Lilið reagierte einfach nicht, sondern schob sich das Stück Brot
in den Mund. Eigentlich schmeckte es lang nicht so gut wie auf
der Kagutte. Sie merkte dem Gemüse an, dass es alt war, und auch
handelte es sich insgesamt nicht um so eine gute Auslese. Es war
egal. Gerade hätte Lilið alles geschmeckt.

Sie versuchte, Restkonzentration
darauf zu richten, beim Schmecken auf Gift zu achten. Es
wurde ihr dadurch erschwert, dass ihr Körper in regelmäßigen
Abständen erbärmlich zitterte, obwohl ihr inzwischen
eigentlich wieder warm war.

"Es ist schön, dass du noch lebst, auch wenn du mir so sehr viel
Ärger machen konntest.", fügte Allil hinzu. "Ich
glaube, das ist keine gute Einleitung. Vielleicht habe ich
es auch verdient."

"Wer verdient schon irgendetwas?", murmelte Marusch. "Wenn es ums
Verdienen ginge, was soll dann mit mir geschehen?"

Lilið blickte sie an. Vielleicht verstand sie jetzt zum ersten
Mal richtig, wie Marusch zu ihrer Haltung Allil gegenüber kam. Lilið
fand die Situationen immer noch nicht vergleichbar. Aber wenn Menschen
in der Sakrale überlebt hätten, die Marusch nicht hätten angreifen
wollen, dann wären sie vielleicht nun so sauer auf Marusch wie
Lilið auf Allil. Vielleicht gab es aber auch einfach kein
Aufwiegen.

"Darf ich wissen, wie du das Gift überlebt hast?", fragte Allil.

"Ich habe kein zusätzliches zu mir genommen. Nur den Schluck im
Tee. Ich bin dann zu einer Reiseinsel geschwommen und gerade so
dort angekommen, bevor das Fieber eingesetzt hat. War eine gute
Insel, mit Medika, kann ich empfehlen." Lilið
kicherte über ihren eigenen Tonfall und riss sich noch einen Schnipsel
vom Brot ab. "Darf ich wissen, wie du im Meer gelandet bist, wo
Drude dich freundlicherweise herausgefischt hat?"

"König Sper sucht verzweifelt nach Wegen, um unser Königreich unter
Druck zu setzen, mit dem Ziel dass König Sper darüber herrschen wird oder dass
alternativ Marusch es täte. Und da Marusch den Blutigen Master M
sucht, hat König Sper veranlasst, ihn zu entführen, um Marusch
dann einen Handel vorzuschlagen. Den Blutigen Master M gegen
Regentschaft.", erklärte Allil. "Als über den Blutigen Master M
herausgefunden wurde, dass Lilið von Lord Lurch dahinter
stecke, -- das habe ich dir gar nicht zugetraut! --, war
ich natürlich dran. Weil ich ja so tue, als wäre ich
du. Ich wurde von einigen mächtigen Wachen überwältigt und auf
einer Kagutte von König Sper gefangen gehalten. Ziel war Mazedoge,
also der Sitz von König Sper, aber da nun irgendwie auch die
Bundesorakel mitmischen und so halb auf König Spers Seite sind,
war neues Ziel die Zentral-Sakrale auf Belloge. Ich habe in diesem
Gewitter, als viel Gewusel an Bord war, die letzte Möglichkeit gesehen,
von Bord zu fliehen. Ich hoffte, weil ich die nächste Insel schon
habe sehen können, dass ich sie schwimmend erreichen können würde. Ich
wusste, dass es lebensgefährlich würde. Allein wegen des Gewitters,
aber wohl auch wegen der Entfernung. Ich hätte es wohl nicht
geschafft, meint Drude."

"Sehr unwahrscheinlich.", bestätigte Drude.

"Vielleicht kommt dir entgegen, dass ich König Sper ermordet
habe." Marusch sagte es fast beiläufig.

Allil starrte sie an. "Schon. Aber,", sie unterbrach sich. "Scheiße!
Wie geht es dir damit?"

Die Frage überraschte Lilið.

"Im Moment fühle ich mal wieder wenig.", antwortete Marusch. "Ich
denke, es kommt einfach mit auf den Stapel der Dinge, die ich nicht
verarbeiten können werde. Aber ich möchte als Täterin auch lieber kein
Mitgefühl haben."

"Nun ja, du kennst meine Einstellung zu Leben und Sterben. Und
dass ich da sehr anders gepolt bin als du. Lass mich trotzdem
kurz ausholen, um dir zu erzählen, warum ich Empathie mit
dir habe, egal was für ein Gräuel du anrichtest.", sagte
Allil. "Ich habe beinahe einen dir sehr wichtigen Menschen
getötet. Das war mir, als ich es getan habe, nicht klar, aber
darum geht es mir gerade auch nicht. Du hättest allen Grund, wütend
auf mich zu sein und dich an mir rächen zu wollen. Aber du bist
das absolute Gegenteil einer rachsüchtigen Person. Du würdest
nie eine Person aus dem Grund töten, dass du findest, dass sie eine
schlimme Person ist. Du strafst nicht, du willst niemandem weh
tun, du wünschst dir kompromisslos einen Grundrespekt für alle
Wesen dieser Welt. Dafür, dass du tötest, muss Gefahr im Verzug
sein und es muss üble Ungerechtigkeit am Passieren sein."

Marusch nickte und nahm die Beine mit auf die Bank, um sie
zu umarmen. "Es hat sich schrecklich angefühlt. In der
Zentral-Sakrale darüber zu reden, wie die Politik
denn zu laufen hätte, weil diese und jene Regel existiere. Niemand
in der ganzen Sakrale hat auch nur
probiert, zuzulassen, darüber nachzudenken, worum es eigentlich
geht. Niemand der Anwesenden hätte ein System gewollt, in dem es
Menschen wie meiner Schwester möglich ist, mehr zu sein,
als eine geduldete Kreatur. Das habe ich gefühlt, und es war
schrecklich."

Lilið war nicht klargewesen, dass Marusch auch Stimmungen erfühlen
konnte, aber sie zweifelte nicht an der Wahrheit. Sie wusste
es irgendwo selbst. Sie wusste, was das für Leute gewesen waren
und dass schon ein aktives sich nicht gegen Macht wehren dazu
gehörte, dass sie die Positionen innehatten, die sie besetzten. Sie
wusste es zum Beispiel von ihrem Vater.

"War eigentlich die Wache auch darunter, die immer so freundlich
zu mir war, außer, dass sie mich wie ein Kleinkind bemuttert
hat?", fragte Lajana. Sie fügte stirnrunzelnd hinzu: "Beeltert. Aber
das klingt wie älter machen, aber die wollte mich jünger machen."

"Die war auch dabei." Lilið bestätigte es, bevor sie darüber
nachdenken konnte, ob sie es Lajana irgendwie vorsichtiger
hätte beibringen sollen.

"Ich schließe, du hast direkt auch gleich ein paar Wachen mit
umgebracht?", fragte Allil.

"Ich habe etwa ein Drittel der Zentral-Sakrale zerstört mit
allen Anwesenden darin, außer Lilið und mir.", antwortete Marusch.
"Darunter war ein Gutteil des entscheidungstragenden Adels aus
dem Königreich Sper, sowie ein Großteil von König Spers Garde, viele
Sakrals-Wachen und ein paar Mitmischende aus dem Königreich Stern. Es
waren so um die sechzig der mächtigesten Leute des Königreichs Sper."

"Wow.", hauchte Allil. "Das wird politisches Chaos nach sich ziehen. Und
was zur Unterwelt hast du bitte für eine Macht!"

"Eine, mit der kein Mensch existieren sollte.", antwortete Marusch
ohne Zögern.
