Die Qual der Karrierewahl
=========================

*CN: Dominition-Submission-Spiel, Befehle, reden über Strafen
und Hauen, Beißen - erwähnt. Breath Play?, Würgen - erwähnt, sexuelle
Übergriffigkeiten - oft erwähnt, (binärer) Sexismus, Religion
mit Tradition, Ritualen und Kleiderordnung, vermutlich
am ehesten auf katholische Rituale anspielend.*

Irgendwie hatten sie sich in der Nacht so gedreht, dass Drude nun
in Liliðs Armen lag. Drudes nackte Schulter drückte sanft gegen
Liliðs Gesicht. Die Haptik der Haut auf Liliðs Lippen verriet, dass
Drude sich im Schlaf unwillkürlich etwas verfischlicht hatte. Lilið
fühlte ein warmes Gefühl unter der Haut, das vielleicht Liebe war. Hatte
sie es tatsächlich geschafft, sich endlich in Drude zu verlieben? Aber
es war irgendwie anders. Es war mehr, dass sie sich nun einfach wohl
mit demm fühlte. Sie konnte sich fallen lassen. Sie befürchtete von
Drude keine Verletzungen mehr, sondern wusste, dass dey für sie
da war, so gut dey konnte. Ohne es wirklich zu steuern, formten Liliðs
Lippen einen zarten Kuss auf Drudes Schulter aus. Und das war ein Fehler.

Drude war auf einmal hellwach, rollte sich aus der Umarmung und
schubste Lilið von sich. "Ih, bah!", rief dey. "Baah! Lass das!" Drude
schüttelte sich. "Ich will das nicht. Mach das nie wieder, ja?"

"Es tut mir leid.", sagte Lilið kleinlaut. Sie wollte versprechen,
es nie wieder zu tun, aber auf der anderen Seite hatte sie eigentlich
gewusst, dass sie mit so etwas bei Drude vorsichtig sein müsste, und
es wie ausversehen getan. Sie musste es trotzdem schaffen, das ging
so nicht. "Nie wieder.", sagte sie. "Das war nicht in Ordnung. Wenn
du willst, hau mich." Warum hatte sie das hinzugefügt?

"Das hättest du wohl gern!" Über Drudes Gesicht huschte ein Grinsen. Und
vielleicht war es dort sehr zurecht. Dey hatte Liliðs Gedanken, die
sie sich selbst noch nicht einmal ganz bewusst gemacht hatte,
bereits erraten.

"Ich glaube, hauen ist nicht so ganz meins, aber mit der Richtung, in
die ich denke, hast du an sich wohl leider recht." Lilið fühlte sich nicht
weniger kleinlaut als eben. "Ich glaube, meine Gedanken sind sehr
schnell durch die Gegend assoziiert. Das ist nicht fair von mir, damit
jetzt anzuschließen. Es geht darum, dass ich eine Grenze überschritten
habe und das ist nicht in Ordnung."

"Ich wäre interessiert an den Assoziationsketten, wenn du teilen
magst." Drude rollte sich wieder näher zu ihr. Etwas verspielt
Bedrohliches schwang in derer Stimme mit.

"Ich hatte mich gefragt, wo deine Grenzen sind.", leitete Lilið
wahrheitsgemäß ein. "Denn beißen finde ich nicht so weit weg von
küssen. Du berührst mich dabei mit den Lippen und tust etwas, nun
ja, etwas Zärtliches." Lilið wurde heiß, als sie es aussprach. Es
war extrem zärtlich gewesen, aber empfand Drude es genau so? "Und
ich hatte gestern Abend schon Lust, dass du mich wieder auslieferst. War
aber zu müde." Sie hörte zu reden auf. Machte sie es nicht nur noch
schlimmer?

"Du möchstest anders bestraft werden als mit Schlägen.", erkannte
Drude. "Und eigentlich möchtest du genießen und gar keine Strafe
haben, aber so, dass ich davon etwas habe. Du möchtest mir zur
Verfügung stehen."

Es war provokant, was Drude aussprach, aber stimmte. Und es erregte
Lilið. Sie nickte.

Im nächsten Augenblick rollte Drude sich auf sie, klemmte ihr Becken
und ihre Oberschenkel zwischen deren starken Beinen ein und drückte
Liliðs Oberkörper an den Armen nieder. Drudes Hände verflossten
sich, was selbst aus dem Augenwinkel ein schöner Anblick war, und wanderten
langsam zu Liliðs Hals. Sie legten sich dieses Mal sehr sanft
darum, ohne zu drücken.

In Liliðs Körper zog sich alles zusammen, auf eine Weise, die sie
schneller atmen ließ. Sie konnte es immer noch nicht ganz fassen,
wieso sie das so sehr genoss.

"Macht dir das jetzt mehr Angst? Meine Hände um deinen Hals,
meine ich.", fragte Drude.

Lilið schüttelte den Kopf. Den Raum dazu hatte sie. "Ich vertraue
dir."

"Das hast du schon ein paar Mal gesagt und dann doch nicht getan.", hielt
Drude sachlich fest. "Ich glaube, es kommt immer drauf an, und das ist
auch in Ordnung so. Du vertraust mir bei diesen Spielen ganz und gar. Das
ist schön."

Lilið atmete schneller. Sie wollte wieder gebissen werden. Oder irgendetwas,
was Drude mit ihr machen wollte.

Drude beugte deren Kopf zur ihrem Ohr herab. "Ich würde gern mehr in der
Richtung mit dir machen.", raunte dey warm und dunkel. "Es ist etwas sehr
Besonderes, so viel Kontrolle bekommen zu dürfen. Aber auch wenn wir
unser Leben riskieren werden und dies deshalb die letzte Gelegenheit
sein könnte, möchte ich mit dir lieber eine Kronprinzessin zurückstehlen,
als diese Situation jetzt auszunutzen. In Ordnung?"

Aha, dachte Lilið. Etwas Vernünftiges vorschlagen, aber das in
einer Weise und mit einem Vokabular, dass ihr Verlangen, benutzt
zu werden, unweigerlich in die Höhe trieb. 'Die Situation
auszunutzen' zum Beispiel löste viel in ihr aus. Lilið versuchte, sich
am Hyperventilieren zu hindern und sich irgendwie wieder
zusammenzusammeln, aber ihr wurde fast schwarz vor Augen. Ihr
Magen knurrte.

Drude ließ von ihr ab. "Hunger war es.", murmelte dey. "Ich wusste,
dein Energiehaushalt stimmt nicht. Und es ist vollkommen logisch,
dass du Hunger hast. Ich dachte zuerst, es wäre noch dein psychischer
Zusammenbruch."

Drude stand auf und trug den schweren, großen Rucksack heran, aus
dem dey in Wachstuch eingeschlagenes Brot hervorholte. Mit einem
mäßig dafür geeignetem Messer schnitt dey dicke Scheiben davon ab
und reichte Lilið eine.

Lilið richtete sich auf und nahm sie entgegen. Dabei tanzten
ein paar helle Lichtblitze durch ihr Sichtfeld und sie fühlte
den Sauerstoffmangel im Gehirn ähnlich wie zum Zeitpunkt, als
Drude sie auf dem Schiff gewürgt hatte. Erst dachte sie, dass
sie trockenes Brot ohne Belag gerade schwer herunterkriegen
würde, obwohl sie Hunger hatte. Aber als sie es probierte, stellte
sie fest, dass Nüsse und Früchte darin waren, es also keineswegs
trocken war. "Was ist das schon wieder für gutes Brot? Ich
werde dich auf immer mit übertrieben gutem Essen verknüpfen, glaube
ich."

Drude nahm sich selbst eine Scheibe, betrachtete sie und zuckte mit
den Schultern. "Das ist einfach sehr typisches belloger Brot.", sagte
dey. "An Bord hatten wir sehr gutes Essen, selbst für meine
Verhältnisse. Das lag daran, dass der Smutje als Koch am Hof
von Königin Stern spioniert hat, und wir außerdem durch König
Spers Unterstützung die Mittel hatten. Aber dieses Brot ist
eigentlich einfach zu backen und es wundert mich anderswo immer,
dass ihr da so merkwürdiges, trockenes Brot habt, das zum Essen
aufgehübscht werden muss, weil es nicht für sich taugt. Das
ist unpraktisch."

"Du hast in dieser Gegend mal länger gelebt, oder?", fragte
Lilið. "Du kennst die Höhle, du kennst Essen von hier."

Drude grinste kurz und nickte. "Ich bin hier zehn Jahre
ausgebildet worden. Was uns mehrere Vorteile verschafft.", antwortete
dey. "Ich wurde im Dienst der Sakrale ausgebildet. Das fand
ich im Nachhinein betrachtet unerfreulich, aber ich kenne daher
zumindest die kleinere Zivil-Sakrale in der Hafenstadt ganz
gut. Die Zentral-Sakrale habe ich auch einmal von innen gesehen,
aber da war ich noch jugendlich und sehr aufgeregt. Es kann
sein, dass meine Erinnerungen schlecht sind oder sich inzwischen
viel verändert hat."

"Du warst Sakrals-Wache?", fragte Lilið überrascht. Vielleicht
sollte es sie nicht so sehr überraschen. Drudes Abneigung
gegen Orakel und Sakral-Dinge hatte dann wahrscheinlich
eine vielleicht persönliche, aber trotzdem fundierte
Grundlage. Das war interessant zu wissen.

Drude schüttelte den Kopf. "Ich habe die Schwüre nicht
geleistet.", widersprach dey. "Was so ein kleines bisschen
skandalös war."

Lilið nahm den Tonfall als zynisch wahr und musterte kauend
Drudes Gesicht aufmerksam, ob es noch mehr Anzeichen dafür
gab.

"Es war so gemein.", fuhr Drude fort. "Ich habe gefragt, ob es meine
freie Entscheidung wäre, das nicht zu tun. Es läuft ja
so ab, dass die Wachen bei der Vereidung gefragt werden,
ob sie dies und jenes schwören, und ich hatte gefragt, ob
die Fragen nur Formsache wären, oder ob ich ablehnen
dürfte. Der Sakralet hat mir unter vier Augen
geantwortet, im Prinzip dürfe ich schon, es wäre
nur ein kleines bisschen skandalös. Ich habe es wörtlich
genommen und gedacht, ein kleines bisschen kann ich in
Kauf nehmen, aber es war der schlimmste Tag meines Lebens."

"War es während einer Zeremonie?", fragte Lilið. Sie hatte
Schwierigkeiten, nicht zu grinsen. Nicht, weil sie sich über
Drude lustig machte, -- sie hatte größtes Mitgefühl --, sondern
weil sie es eigentlich als ein starkes Statement empfand, sich
während einer Zeremonie vor vielen Leuten gegen eine Vereidung
zu entscheiden.

Drude nickte. "Ich habe das immer noch nicht verarbeitet.", murmelte
dey. "Ich bin empfindlich, was das Thema angeht."

"Ich verstehe das.", antwortete Lilið. "Ich versuche, einfühlsam
zu sein." Wieder knurrte ihr Magen.

Drude reichte ihr eine weitere Scheibe. "Iss. Ich habe noch ein
Brot und ich weiß, wie ich an mehr komme. Beziehungsweise bekommen
wir im Dienst der Sakrale auch ohne Schwierigkeiten welches."

"Fällt nicht auf, wenn plötzlich zwei Sakral-Dienende mehr da
sind?", erkundigte sich Lilið.

"Es kommen oft welche aus anderen Städten oder sogar von anderen
Inseln mal für ein oder zwei Wochen für Austausch vorbei. Das
sollte ich begründet kriegen.", beruhigte Drude. "Mir macht
mehr Sorge, ob ich dir innerhalb der nächsten Stunden all die
Dinge beigebracht bekomme, die für dich in der Rolle Routine
sein sollten. Gleichzeitig wüsste ich gern, was für dich als
Diebesperson Routine ist. Was kannst du?"

"Ich kann Rollen spielen." Lilið überlegte.

"So mittelgut.", kommentierte Drude.

Lilið runzelte die Stirn und nickte. "Es war schon manches Mal
ausreichend, aber ich bin nicht herausragend gut darin.", stimmte
sie zu.

"Ich finde dich schon herausragend, weil ich glaube, ich könnte das
so ohne Ausbildung nicht. Der Teil der Ausbildung fiel mir besonders
schwer.", relativierte Drude. "Trotzdem. Ich kenne Menschen, die dir
erfolgreich verkaufen, dass sie die Königin persönlich sind. Das
könntest du nicht, glaube ich. Und du hast Schwierigkeiten, wenn Leute
schon einen Verdacht hegen, dass du spielst, das zu erkennen
und dagegen anzuarbeiten."

Es war wie mit Heelem, dachte Lilið. Drude schätzte ihre Fähigkeiten
sachlich ein, ohne dass es ihr weh tat, weil es nicht dazu da war,
sie als Person zu bewerten, sondern einzuschätzen, was sie für
Ressourcen hatten. "Danke. Dass du das in dieser Weise aussprichst."

"Oh, ich dachte, ich wäre vielleicht arrogant." Drude runzelte einen
Moment die Stirn. "Was kannst du noch?"

Was konnte sie noch. Ihr fiel nichts ein. "Was könnte ich
können können?" Lilið kicherte. "Welch Frage. Sehr Können-lastig."

"Leuten unbemerkt in deren Taschen greifen und Dinge daraus
mitgehen lassen?", schlug Drude vor. "Ich weiß nicht. Wenn du
magst, leg mal was offen, was du abgezogen hast."

Lilið kicherte bei dem Gedanken an die Aktion mit dem Mantel
des Nautikas. Das war im Nachhinein betrachtet ein schon
beeindruckendes Ding gewesen, das sie da mit Marusch gedreht
hatte. "Ich habe vor allem Leute abgezogen, indem ich ihnen Dinge
aus ihren Taschen gestohlen habe, wenn sie mir ohne mein Einverständnis
nahe gekommen sind.", berichtete sie. "Wenn sie mich begrabbelt
haben. Der Ausdruck klingt so eklig, wie ich das gerade
wiedergeben möchte, vielleicht ist er unpassend."

Drude nickte. "Es ist mir in sofern sympathisch, als dass das
eine Gewichtung aufbaut, dass Leute, die du bestielst, vielleicht
nicht ganz so unschuldig sind."

"Genau.", stimmte Lilið zu. "Es ist immer noch ethisch fraglich. Da
stimme ich zu, wenn das jemand behaupten sollte. Aber es macht es
irgendwie besser. Meine Erfahrung ist allerdings recht begrenzt."

"Bist du schonmal erwischt worden?", fragte Drude.

"Guter Punkt!" Lilið grinste. "Bisher nie. Ich entscheide mich immer erst spät
im Vorgang dazu, wirklich etwas zu greifen und zu stehlen. Vorher nähere ich
mich dem Moment in einer Art, dass ich meine Hand zurückziehen könnte, ohne
dass es wie ein Diebstahlversuch gewertet würde. Ich mache erst dann aus
Annäherungen Tatsachen, wenn ich mich sicher fühle."

"Behalt das bei, dass du dich nicht selbst überschätzt." Drude
biss vom Brot ab und zeigte mit der Hand an, dass Lilið weiterreden
möge.

"Ansonsten stehle ich meistens Dinge aus geschlossenen Räumen, wenn
niemand zuguckt.", fasste sie zusammen. "Eigentlich war meine
Absicht immer eher, Schlösser zu knacken, als tatsächlich etwas zu stehlen. Ich
habe dann manchmal Überbleibsel von etwas gefunden, vielleicht ein
Edelsteinchen oder so, und eben einmal einen Teil des Schatzes der
Monarchie. Das war mir damals nicht bewusst." Leiser fügte sie
fast seufzend hinzu: "Ich hoffe so sehr, dass es geklappt hat,
ihn wieder zurückzubringen. Das wollte Marusch tun."

"Lilið, ey!" Drude kaute auf, bevor dey deren Gefühlsbruch
erklärte. "Du kannst also in verschlossene Räume gelangen und
erzählst davon, als wäre das so etwas wie atmen und keine besondere
Fähigkeit. Machst du es mit Magie? Oder magieloses Schlösserknacken?"

"Oh!" Lilið kicherte über sich selbst. "Es gehört wohl einfach so
sehr zu mir, dass ich vergessen hatte, dass es etwas Besonderes
ist. Und hilfreich beim Stehlen. Ich kann ohne Magie
Schlösser knacken. Nicht beliebig schwierige, aber ich habe
einiges an Training, Routine und Ruhe dabei."

"Das ist eine gute Fähigkeit, weil wir auf die Art zum Beispiel
voraussichtlich viel leichter an Sakralutten kommen.", hielt Drude
fest. "Es ist praktisch, sie zu haben, bevor wir mit jemandem
interagieren, weil wir dann leichter schon als dazugehörend
durchgehen. Ungebrauchte sind in einer abgeschlossenen Kleiderkammer
im Keller. Kein komplexes Schloss, aber ich habe es erst einmal geknackt
bekommen. Alle anderen Versuche habe ich abbrechen müssen, bevor
doch jemand vorbei kam und mich erwischt hätte, wenn
ich nicht aufgehört hätte. Und ich habe die Spuren, die ich hinterlassen
habe, gesehen. Es kam zu unangenehmen Befragungen. Ich gehe mal nicht
ins Detail." Drude besah sich die Brotkante, biss aber dann doch
noch nicht hinein. "Magst du? Von einem Ding erzählen, dass du
durchgezogen hast?"

Lilið grinste und begann, als sie den Rest ihrer Scheibe verspiesen
hatte. "Zufälligerweise handelte sich das bisher wohl interessanteste
Ding, das ich durchgezogen habe, auch um den Diebstahl von
Kleidung.", leitete sie ein. "Der Mantel des Nautikas ist eigentlich
nicht meiner."

"Von Kleinigkeiten aus Taschen von übergriffigen Menschen und
Überresten aus an sich leer vermuteten Kisten nun also zum
wichtigesten Hab und Gut einer Person der eigenen
Berufsgruppe.", kommentierte Drude. "Ich sollte nicht werten. Das
hilft hier nicht. Ich nehme an, es ist der Mantel des Nautikas, das
ursprünglich zu uns stoßen sollte? Das, das Herr Hut irgendwo
aufgetrieben hat?"

Lilið nickte. "Ich fühle mich auch nicht wohl damit.", gab sie
zu. Sie erzählte vom Ereignis in chaotischer Weise, weil sie
nicht direkt eine gute Reihenfolge fand. Sie fühlte
sich auch im weiteren Verlauf der Erzählung nicht unbedingt
wohl. Aber die Erinnerungen an Marusch waren schön.

"Marusch klingt also nach dem Kaliber, über das wir vorhin
gesprochen haben. Sie könnte sich als Königin
ausgeben und Leute würden ihr glauben.", kommentierte
Drude trocken. "Ich bin unsicher, ob ich ihr begegnen möchte."

Lilið konnte ein Kichern nicht ganz unterdrücken. "Die Vorstellung
ist witzig. Marusch ist so antiauthoritär. Vom Schauspieltalent
kann sie es vielleicht, aber ich frage mich, ob sie es aushalten
würde."

"Bevor du hier ins Schwärmen über diese Marusch gerätst, zurück zur
Vorbereitung.", ermahnte Drude. "Die Geschichte ist hilfreich für
mich. Als nächstes: Wieviel Erfahrung hast du so mit Sakrals-Dingen?"

Lilið grinste, weil sie glaubte, dass Drude sich damit auskannte,
aber trotzdem so unkonkret von 'Sakrals-Dingen' sprach. "Mit
meinem Namen?", fragte sie. "Als uneheliches Kind, angebliche
Tochter um genau zu sein, einer Köchin und eines nicht gerade
für seine Sakralstreue bekannten Lords?"

"Du bist also wirklich Lilið von Lord Lurch?" Drude kniff für einen
Moment die Augen zusammen.

Lilið nickte. "Das bin ich." Sie fühlte einen interessanten
Stolz darauf, wer sie war. Es kam ihr unsinnig vor. Weil ihr
ihr Vater in seiner politischen Rolle inzwischen unsympathisch
war. Vielleicht war sie stolz, anders zu sein. Ein Nautika, verwickelt
in eine politische Intrige, quer über den Ozean von ihrer
Heimatinsel entfernt, und das alles gegen die Zwänge ihrer
Herkunft. Der Titel 'Lilið von Lord Lurch' hatte einen berüchtigten
Beiklang, fand sie. Besonders, wenn daran noch die Identität
des Blutigen Master M haftete. Lilið grinste ob ihrer unsinnigen
Gedanken. Ob es ihr je etwas bringen würde?

Drude reichte ihr noch eine Scheibe Brot, die Lilið zögernd annahm,
und stand auf. "Dann hast du vermutlich eine Sakrale noch nie von
innen gesehen.", hielt dey fest. "Vom Orakel durch die Namensgebung
schon als Säugling von Sakrals-Diensten ausgeschlossen. Noch
etwas, was ich an dem Laden nicht mag." Dey suchte Trinkbeutel
aus dem Gepäck hervor und füllte sie wenig entfernt am Rinnsal
Wasser auf.

"Ich habe eine recht diebische Natur oder so.", widersprach
Lilið. "Sie hätten mich nicht freiwillig eingelassen, aber ich habe
mich einige Male hineingestohlen. Ich habe mich bei dem einen
Ritual, bei dem ich mit etwa sieben im Gebäude zugegen gewesen bin, zu
unwohl gefühlt und war nie wieder bei einem dabei. Mit Abläufen
kenne ich mich also überhaupt nicht aus. Ich kenne nur die, hm, ich sage
mal vorsichtig, äußerst sexistische Kleiderordnung?"

Drude kam zurück und reichte ihr einen der Trinkschläuche. "Ein
Punkt, den ich damals weniger unangenehm emfpunden habe, als ich
vielleicht sollte.", sagte dey. "Als ich in die Pubertät kam, wollten
meine Eltern, dass ich mich etwas bedecke." Drude betonte die
letzten beiden Worte durch einen ähnlich zynischen Ton wie vorhin
bei der Erzählung zur mies gelaufenen Nicht-Vereidung. "Mir war
in der Kleidung, die meine Eltern für mich vorgesehen haben, immer
zu warm und ich mochte eigentlich den Wind
auf den Beinen. Ich mochte eng anliegende Kleidung, weil ich
mich darin besser bewegen kann und ein stärkeres Körpergefühl
darin habe."

Lilið warf einen Blick auf Drudes Kleidung. Den Mantel hatte dey
abgelegt, der entsprach nicht dem Kriterium eng anliegend. Ansonsten
stimmte es schon: Drude trug eher eng anliegende Kleidung. Lilið
war das schon einmal aufgefallen, aber sie hatte davon keine
grundsätzliche Regel abgeleitet und dann nicht weiter darauf
geachtet. Sie mochte daran, dass Drude
ungeniert deren Körper betonte, der breitschultrig, muskulös und
schwer war. Drude hatte zwar einen durchaus sehr beweglichen, aber nicht
gerade filigranen Körperbau.

Dey fuhr fort: "Die Sakrale fand den Trend im Volk dazu, dass Frauen
sich nicht zeigen dürfen, ungut. Frauen und alle, die irgendwie
für Frauen gehalten werden, du weißt."

Lilið nickte einfach. Sie sagte nichts, damit Drude weitererzählen
möge, und als dey es nicht einfach tat, sondern wie versunken auf
deren Trinkschlauch blickte, machte Lilið ein einladendes Zeichen
mit der Hand.

"Die Einstellung der Orakel war:", stieg Drude nachdenklich
wieder ein, "Frauen sollten nicht geschützt werden, indem sie
sich verstecken müssen, sondern indem das soziale Gefüge dahinter
sexualisierte Übergriffigkeiten nicht zulässt. In dem Zuge wurden
die Sakralutten für Frauen körperbetonter gestaltet. Nicht alle
Sakralutten, aber die Standardsakralutten für viele Aufgabenfelder
zeigten seit dem mehr Haut und lagen eng an. Und wenn eine
Frau sich in den Dienst der
Sakrale begab, hatte sie die Garantie, dass zumindest körperlich
übergriffige Sakrals-Dienende entweder einen Läuterungsvorgang
durchlaufen oder die Sakrale verlassen mussten."

"Hm.", machte Lilið. "Ich habe auch mal gehört, dass Sakralen
bezüglich sexueller Übergriffigkeiten sicherere Orte sein
sollen als die Welt da draußen. Ich kenne selbst eigentlich nur
dieses draußen. Aber als Wache Luanda davon erzählte, hat es
mich für Lajana tatsächlich ein bisschen beruhigt."

Drude kicherte lautlos. "Stell dir vor, du wirst entführt,
instrumentalisiert, erniedrigt und für unfähig gehalten, aber
wenigstens grabbeln dich keine Leute an.", murmelte dey. "Ja,
ich verstehe, was du meinst. Das klang sarkastischer, als es
ist, glaube ich. Es macht Dinge schon ein bisschen besser."

"Ich hätte mich in den Sakralutten für Frauen, die ich auf
Nederoge gesehen habe, aber trotzdem sehr unwohl gefühlt.", betonte
Lilið. "Und zwar nicht nur, weil ich keine Frau bin." Zumindest
glaubte sie, dass das nicht der einzige Grund war. Es war schon
ein starker Grund.

"Das verstehe ich.", antwortete Drude. "Es gab genügend Frauen,
mit denen ich das Zimmer geteilt habe, die diese Kleidung unangenehm
körperbetont und freizügig fanden. Für mich war sie eben genau
richtig. Ich mochte die Luftigkeit und Haptik, die die Kleidung
auf der Haut hatte. Ich habe damals gedacht, dass die anderen deshalb
weniger freizügige Kleidung haben wollten, weil sie den Mist
verinnerlicht hatten, den meine Mutter mir einbläuen
wollte. Dass ich dadurch ein Schauobjekt der Sakrale wäre. Oder dass
ich an meinem Äußeren als potenzielle Ehefrau eingeschätzt
werden würde, statt für meine inneren Werte. Ich habe damals überhaupt
nicht an Heiraten gedacht." Drude seufzte. "Ich verheddere
mich. Jedenfalls habe ich erst später verstanden, dass jede Form
von Kleidervorschrift übergriffig ist. Den Körper nicht zeigen
zu dürfen, ist genau so übergriffig, wie den Körper zeigen zu
sollen. Und wenn eine Zugehörigkeit zu einer sozialen Gruppe dazu
führt, dass eine bestimmte Kleidung bevorzugt wird, dann
verstehe ich das zwar nicht, aber es ist trotzdem nicht falsch
oder schlecht. Wir sollten dafür kämpfen, dass wir frei
entscheiden dürfen. Und das andere aufhören, unsere Gründe
erraten zu wollen und zu verurteilen. Leute sollten aufhören,
davon auszugehen, dass sie unsere Lage für uns besser einschätzen
könnten als wir selbst. Ich wollte nicht schön oder
attraktiv sein. Ich wollte nur Luft an den Armen und
Beinen, die richtige Haptik auf der Haut und nicht so
sehr schwitzen."

"Ich kann mir bei dir irgendwie gut vorstellen, dass
das für dich wichtig ist. Das passt zu dir.", überlegte
Lilið. Sie stellte fest, dass sie schon wieder im Thema abschweiften und
führte sie dieses Mal selbst wieder auf mögliche Problematiken
zurück, die sie bedenken sollten. "Wie wahrscheinlich ist
eigentlich, dass du hier wiedererkannt wirst. Vor allem, wenn
besagte körperbetonte Kleidung nichts versteckt? Ich
meine, du warst hier zehn Jahre lang, hast du gesagt. Die Belegschaft
wird sich nicht komplett erneuert haben, oder?"

Drude schüttelte den Kopf. "Die Gedanken habe ich mir auch schon
gemacht. Ich denke, wir geben uns als Männer aus.", sagte dey. "Es
gibt Dienste, bei denen es üblich ist, viel zu schweigen oder
leise zu reden. Ich denke, das kann klappen."

Lilið versuchte sich zu erinnern, wie die Sakralutten für Männer
ausgesehen hatten. Waren das nicht einfach lange Kutten gewesen? Sie
deutete auf ihre Brüste. "Wenn ich nicht falten darf, dann könnte
das hier ein Problem werden."

Über Drudes Gesicht huschte ein Grinsen. "Die Sakralutten für
Männer haben breite Schulterverstärkungen, die darunter sehr viel
Gewebe verstecken, ob Bauch oder Brüste, das wird nicht
auffallen."

Lilið runzelte die Stirn. "Daran kann ich mich nicht erinnern."

"Soweit ich das mitbekommen habe, sind die Sakralutten auf Nederoge
und Angelsoge anders." Drude wirkte nachdenklich. "Die hierzuinsel
verstecken alles. Auch das Gesicht ist tief verborgen. Es geht darum,
dass Männer nicht mit Körpereigenschaften konkurrieren können sollen
oder so. Sondern nur durch ihr geistiges Können. Eigentlich war es immer
mein Traum, mir eine solche Sakralutte zu stehlen und auszuprobieren,
wie es ist, so gelesen zu werden."

"Hast du deshalb versucht, in Kleiderkammern einzubrechen?", fragte
Lilið.

"Genau." Drude nickte. "Ich würde nun gern zwei bis drei Stunden investieren,
um dir die wesentlichen Abläufe beizubringen. Es gibt ein paar Antworten, die
du unbedingt kennen musst, ein paar Orte in Sakralen, um die du Bögen machen
musst, einen Knicks, den du machen musst, wenn die Glocke erklingt. Haltung,
einen gewissen gemessenen Schritt. Alles machbar innerhalb von ein paar
Stunden, denke ich."

"Antworten?", fragte Lilið.

"Ja, wir haben Sprechrituale. Wenn du zum Beispiel 'Beðem Ajad?' gefragt
wirst, ist die Antwort 'Hatinan'.", erklärte Drude. "Das passiert dir
auf allen Fluren."

"Hatinan.", widerholte Lilið. "Muss ich die Frage auch stellen? Ist das
wie eine Begrüßung?"

Drude schüttelte den Kopf. "Wir werden so tun, als hättest du einen sehr
niedrigen Rang und ich bin dein Mentor. Dann musst du so etwas nicht fragen."

Lilið nickte. "Hatinan.", wiederholte sie. "Und was heißt das?"

"Was 'Beðem Ajad?' und 'Hatinan' heißt?", fragte Drude. "Eigentlich
ist das recht wörtlich gemeint."

"Ist das Alevisch?", fragte Lilið.

Drudes Augenbewegungen erstarrten auf Liliðs Gesicht. Ein paar Momente
reagierte sie gar nicht. "Du sprichst kein Alevisch?"

Lilið schüttelte den Kopf. "Ich hatte zwei Jahre Alevisch und
Helisch im Schulunterricht, aber auf Nederoge sprechen wir nur
Baeðisch, also habe ich fast alles vergessen. Und zwei Jahre
ohne Praxis reichen für mich und die meisten auch nicht, um
eine Sprache zu lernen."

Auf Inseln, die oft die Herrschaft wechselten, wuchsen die Menschen
meistens zweisprachig auf, und die Sprache, die auf der Insel gesprochen
wurde, wechselte etwa alle dreißig Jahre mit dem Herrschaftswechsel, wenn
einer stattfand. Nederoge stand seit mindestens einem Jahrhundert unter
der Herrschaft der Königsfamilie Stern. Die Insel Lettloge, auf
der sie fast entkommen waren, war mit Sicherheit dicht genug an der
Grenze, um zweisprachig zu sein. Und Belloge war es anscheinend auch.
Bloß dass hier umgekehrt, weil die Insel schon zum Königreich Sper gehörte,
derzeit Alevisch die auf den Straßen gesprochene Sprache war. Lilið ging
das erst jetzt auf. Im Nachhinein fiel ihr wieder ein, dass sie seit
der Grenzquerung die fremde Sprache öfter an Bord gehört
hatte, aber vermutet hatte, dass Teile der Crew hinter ihrem Rücken
hatten reden wollen. Dabei war es so logisch, dass sie lediglich
die Sprache sprachen, die sie nun im Herrschaftsgebiet von König
Sper zu sprechen angehalten waren, wenn nicht gerade Personen zugegen
waren, die die Sprache nicht sprachen.

"Scheiße!" Drude atmete durch zu einem Kreis gerundeten Lippen tief
und geräuschvoll aus. Es wurde in der Höhle durch den sacht zischenden
Hall extra betont. "Mit dem Problem habe ich nicht gerechnet."

"Hm.", machte Lilið. Sie wollte fragen, ob nun der ganze Plan scheitern
würde. Immerhin würde sie eine Person spielen, die nicht viel
sprach. Oder konnte sie gar spielen, von einer ausländischen Insel
zu kommen und daher Sprachschwierigkeiten zu haben? Aber
sie brauchte eine Weile, sich zu trauen, die Frage
zu stellen, weil sie sich nicht sicher war, ob
die Antwort für sie hätte offensichtlich sein müssen. "Scheitert nun
der ganze Plan? Oder gibt es Möglichkeiten?"

"Klar gibt es Möglichkeiten.", erwiderte Drude. Immerhin klang es
nicht so, als hätte Lilið sich das doch bitte selber denken
müssen. "Ich bringe dir die Antworten bei, dazu musst du die Sprache
nicht kennen. Nur hilft uns dein paar Ohren beim Lauschen nicht. Es
hilft uns nicht, uns aufzuteilen. Daher habe ich kurz darüber
nachgedacht, ob es irgendwie sinnvoll ist, dass du nicht zur Sakrale
gehören würdest." Drude gab ein brummendes, nachdenkliches Geräusch
von sich. "Aber wenn wir als Sakrals-Dienende am Ende in die
Zentral-Sakrale gelangen wollen, dann ist es sinnvoll, wenn du
auch zum Sakrals-Dienst gehörst. Und die Sakralutten verbergen
dich unauffällig, ohne dass du Magie ausüben musst. Mir fällt kein
besserer Weg ein, dich vor einer Horde Wachen zu verbergen, die
eventuell bald hinter dir her sein wird. Also, abgesehen von dieser
Höhle hier, wo du einfach gar nichts tun kannst."

"Wo wir von Wachen reden, die hinter mir her sind: Sagtest du nicht,
dass Wache Schäler für Lajanas Bewachung dageblieben ist?", fragte
Lilið plötzlich alarmiert.

Drude nickte. "Ja. Wenn deine Frage ist, ob unser Plan ist, Lajana
unter der Nase einer mächtigen, brutalen Wache wegzustehlen, gegen
die ich in einem Kampf keine Chance hätte, dann ist die Antwort: Wenn
wir herausfinden, wann Wache Schäler gerade Wachpause hat, und wir
darauf Rücksicht nehmen können, tun wir das. Sonst, nun ja, ja."

"Hm.", machte Lilið. "Warum würdest du es jetzt probieren, während du
uns auf Lettloge von jedem Weglaufversuch abgehalten hast?"

"Weil wir auf Lettloge schon so tief im Problem steckten, dass das
die einzige Möglichkeit gewesen ist, die ich gesehen habe, da lebend
herauszukommen.", sagte Drude weniger geduldig als sonst. "Es wird
auch jetzt nicht einfach und ab irgendeinem Punkt kann ein 'zu
spät' eintreten. Aber wir können wenigstens im Vorfeld planen. Wir haben neue
Optionen, wie die Tarnung durch Sakralutten. Oder das Betreten eines
sakralen Ortes, an dem es einschränkende Gesetze zur Ausübung von
Magie gibt. Zu hoher Wahrscheinlichkeit wird Lajana in einem der
Räume untergebracht sein, in dem nicht Sakrals-Dienende nur im
Notfall Zutritt haben. So viele Möglichkeiten und Unmöglichkeiten. Wir
sollten erst einmal der Zivil-Sakrale beitreten und lauschen, ob wir an
mehr Informationen kommen. Ich sollte lauschen. Steh
auf! Schreite!"

Die Befehle kamen unvermittelt. Lilið trank noch einen Schluck, den
Blick auf Drude gerichtet, und überlegte, nicht zu gehorchen. Aber
es ergab für Lajana und ihre Pläne mehr Sinn, es zu tun. "Du möchtest
ausnutzen, mich herumzukommandieren?", fragte sie mit einem Schmunzeln
und legte den Trinkschlauch beiseite.

"Wenn es nicht zu sehr ablenkt, mache ich daraus ein Spiel." Drudes
Stimme klang mindestens eine Terz tiefer als eben noch. "Wenn
du möchtest."

Lilið stand auf. "Ich weiß nicht, ob ich Befehle so sehr mag, aber
wir können es probieren."

Drude sprang auf und stand so rasch hinter ihr, dass Magie im
Spiel gewesen sein musste. "Darf ich deine Haltung korrigieren,
wann immer ich will?" Ein Raunen an Liliðs Ohr.

Lilið lief ein Schauer über den Rücken. Sie nickte. Und als nächstes
spürte sie unnachgiebig starke Hände und Unterarme an Schultern, Nacken
und Wirbelsäule, die sie in eine Form pressten. Und durch die Höhle
führten. Drude lenkte eine fingerbreite Bahn Wasser von der Quelle
aus über den Boden, malte damit, Umrisse von Tischen, Bänken
und Ritualgegenständen. Anhand der abstrakten Bilder erklärte dey
Abläufe. Dey bog Liliðs Körper zu Knicksen, ließ sie sich
verbeugen, begleitete Liliðs Gang und Bewegungen mit
anfangs viel und dann zunehmend weniger Korrektur durch
führendes, bestimmendes Eingreifen. Für Tanz hätte Musik
und Eleganz gefehlt, aber dafür fühlte Lilið
Drudes Dominanz in allen Gliedern. Sie empfand es
dieses Mal weniger stark als in ihren bisherigen Spielen, aber das wäre
auch eher ablenkend gewesen. Sie hatte den Eindruck, dass sie sich
durch die Stimmung besser konzentrieren konnte und die Bewegungen
leichter ins Muskelgedächtnis übergingen. Vielleicht trug dieses
Spiel dazu bei, dass sie ein Gefühl von epischer Finalität hatte,
als sie endlich mit Drude die Höhle verließ, um sich in Richtung
Hafenstadt aufzumachen.
