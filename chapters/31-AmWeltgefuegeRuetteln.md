Am Weltgefüge Rütteln
=====================

*CN: Nadeln vielleicht? Könnte assoziiert werden. Sex.
Masturbieren. Erinnerungen an ein Massaker und an Leichen. Gedanken
über sexuelle Interessen von Kindern. Sanism. Internalisiertes
Kinkshaming, Dominition-Submission-Spiele, Breath Play.*

"Lil ist weg. Kannst du mich ablenken?", fragte Drude. "Auch gern, indem
wir den Plan noch einmal durchgehen. Aber vielleicht ist das morgen
mit mehr Abstand besser." Drude lehnte sich von innen gegen die
Tür zum Kartenraum, die dey gerade verschlossen hatte. Es war spät
am Abend, kurz vor ihrer ersten nächtlichen Schlafenszeit.

"Ich würde wieder meine ersten drei Stunden Schlaf schwänzen und
Lajana aufsuchen.", informierte Lilið. Wenn Drude es ohnehin mitbekam,
dann konnte Lilið auch gleich mit offenen Karten spielen.

"Das dachte ich mir schon.", sagte dey. "Sieh weiterhin zu, dass
dich außer mir niemand bemerkt, sonst fliegt auf, dass ich dich
decke und behaupte, du schliefest."

"Ich werde mir große Mühe geben, wie immer.", versprach Lilið. Es
berührte sie ein wenig, dass Drude sie deckte.

"Bleibst du die ganzen zwei Stunden im Raum? Also, die, die von
der jeweiligen halben Stunde umrahmt sind, die du dir zum Hin-
und wieder Zurückkommen nimmst?", fragte Drude.

Lilið nickte.

"Dann schlafe ich in dem Zeitraum. Und hoffe, dass alles gut geht
und die Wache sich dran hält, dass sie Lajana Privatsphäre geben
möchte.", beschloss Drude.

"Du sagtest, es wäre der sicherste Raum auf der Kagutte?", fiel
Lilið wieder ein.

"Ja!" Drude durchschritt den Raum und setzte sich Lilið gegenüber an
den Kartentisch. "Die Wache schirmt Licht, Geräusche, Präsenz von
Menschen und Magie darin so gut ab, dass nicht einmal ich Magie durch
die Tür wahrnehme. Ihr könntet darin sonst etwas tun und ich würde
es nicht merken."

Lilið runzelte die Stirn. "Gehst du nicht ein großes Risiko damit
ein, dass du mir das jetzt sagst?", fragte sie.

"Als hätte ich dich nicht genügend mit anderen Dingen im Griff, wenn
ich nur wollte, sodass das Machtgefälle zwischen uns geklärt
ist.", erwiderte Drude. "Ich fände gut, wenn das Machtgefälle zwischen
uns ausgeglichener wäre. Deshalb erzähle ich dir das. Und ich werde
dir auch noch andere Dinge erzählen. Über die Macht, die du haben
könntest. Morgen. Lieber nicht so kurz vorm Schlafengehen."

Lilið nahm eines der Messinstrumente aus der Schublade im Kartentisch,
das sie gleich brauchen würde. "Vor meiner letzten Messung vorm
Schlafen hätte ich tatsächlich noch eine Frage, die Ablenkungscharakter
haben könnte."

Über Drudes Gesicht huschte wieder so ein kurzes Lächeln. "Her damit!"

"Hast du das Igeldings gesehen?", erkundigte sich Lilið.

Drude nickte.

"Hältst du es für gefährlich?" Das war die Frage, mit der sich Lilið heute
bei Lajana auseinandersetzen wollte, aber vielleicht wusste Drude bereits
etwas.

Drude schüttelte den Kopf. "Es ist nicht magisch.", sagte sie.

Richtig, Drude konnte so etwas ja wissen, fiel Lilið ein. Lilið
runzelte die Stirn. "Nicht?"

"Sicher nicht. Es bitzelt manchmal ein bisschen, wenn es angefasst
wird.", fuhr Drude fort.

"Meinst du damit, dass es piekst?", fragte Lilið.

Drude schüttelte den Kopf. "Dass Stacheln pieksen, brauche ich dir wohl
nicht extra zu verraten.", sagte dey. "Es ist mehr wie ein Feuerstein,
oder noch eher wie ein Zitteraal vielleicht, aber den kennt ihr
landliebenderen Menschen meistens auch nicht so gut, dass ihr
wisst, wie das ist. Es ist wie ein unangenehmes,
spitzes Kribbeln unter der Haut. Nicht nur auf der Haut. Es zieht in den
Körper hinein. Probier es einfach aus!"

Lilið beschloss, mit der Idee, es einfach auszuprobieren, lieber
noch zu zögern. "Du findest das nicht unheimlich?", fragte sie.

"Vielleicht sollte es mir unheimlich sein, aber ist es nicht.", antwortete
Drude. "Vielleicht ist dafür relevant, dass Lajana das Igeldings schon
bei sich hatte, als wir sie entführt haben. Es kommt also nicht von
unserer Seite. Das würde es unheimlicher machen. Aber da
draußen gibt es immer wieder neue, spannende
Naturphänomene. Ich gehe da eher mit Neugierde dran als mit
gruseligen Gefühlen. Ein bisschen Vorsicht schadet sicher nicht. Aber
ich habe, glaube ich, auch eine Grundsympathie für ungewöhnliche
Kreaturen und Naturphänomene, weil ich abgelehnt werde, weil
ich irgendwie anders bin, und mich deshalb mit all dem identifiziere."

Lilið nickte nachdenklich. "Ich glaube, das kann ich verstehen.", sagte
sie. "Du glaubst also, das Igeldings ist irgendwie natürlichen
Ursprungs? Also, nicht von Menschen gemacht?"

"Das ist mein Eindruck.", antwortete Drude. "Aber gute Frage. Vielleicht
liege ich auch falsch."

---

Lilið grinste in sich hinein, als sie sich eine gute halbe Stunde später
vor Lajana im sichersten Raum der Kagutte entfaltete und ihr bewusst
wurde, wie stark sie Drudes Ansicht beeinflusst hatte. "Darf ich
dein" -- sie korrigierte sich -- "das Igeldings berühren?"

Besagtes Igeldings lag unbewegt zwischen ihnen. Lilið hatte
es wieder geschafft, beim Entfalten direkt eine sitzende Haltung
mit gesenktem Kopf einzunehmen. Aber sich daran erinnernd, dass Lajana
nicht wollte, dass Lilið sich unterordnete, hob sie den Kopf sofort wieder
und blickte erst sie und dann das Igeldings an.

"Igeldings!" Lajana sprach die Entität laut und deutlich an. "Der
andere Mensch möchte dich gern anfassen. Erlaubst du das?"

Konnte es sie hören? Lilið war erst kurz irritiert, dann fasziniert. Hatte
es Ohren?

Das Igeldings rührte sich nicht sofort, aber noch während Lilið sich
Gedanken machte, wie ein Ohr an diesem Wesen aussehen könnte, streckte
es die unteren Stacheln durch und rollte ihr entgegen.

"Ich verstehe das als Zustimmung.", informierte Lajana.

Lilið nickte und grinste nun nicht mehr nur innerlich. "Das
würde ich auch so verstehen.", sagte sie. "Ich bin gerade
sehr fasziniert."

Kurz bevor das Igeldings ihre Knie berühren würde, streckte Lilið
eine Hand aus, sodass es stattdessen gegen jene rollen würde. Sie konnte
beobachten, wie die haardünnen Stacheln auf sie reagierten,
als der Abstand vielleicht noch einen Zentimeter betrug. Dort
hielt es an.

Lilið schloss die Augen, weil sie den Eindruck hatte, die Nähe
zu spüren und möglichst nur auf dieses Gefühl fokussieren wollte. War
es einfach Körperwärme? Aber da war etwas, was ihre Härchen
aufstellte. Sie näherte sich sehr langsam mit den Fingerspitzen. Das
ganz zart ziepende Gefühl unter ihrer Haut wurde stärker und als sie
das Igeldings berüherte, fühlte sie nicht nur das sachte Pieksen von
vier Stacheln auf ihrer Haut sondern auch das Bitzeln, von dem
Drude erzählt hatte. Sie trennte die Berührung kurz, obwohl sie
sich kaum erschreckt hatte, und berührte die Stacheln ein
zweites Mal, noch vorsichtiger. Dieses Mal bitzelte es weniger. Sie
fühlte nur eine ziehende, leichte Anspannung unter der Haut, die
sie in einem anderen Kontext als unangenehm bezeichnet hätte.

Sie behielt die Berührung aufrecht, während sie darüber nachdachte, was
sie als nächstes tun wollte. Das Nachdenken fiel ihr schwer. Erst
bloß, weil sie eben immer noch müde war und eigentlich mit dem
Kennenlernen neuer Personen, Fluchtplanung und Navigation ohnehin
schon genügend Themenkomplexe gleichzeitig konzentriert bearbeiten
musste. Dann lenkte sie aber auch ab, was dieses Igeldings tat: Es
bitzelte abermals, einfach so aus dem Nichts. Dieses Mal zuckte das
Igeldings zurück, aber berührte sie kurz darauf von sich
aus wieder. Als nächstes spürte Lilið,
wie das Ziehen unter der Haut, das ganz leicht nur bis in ihren
Ellbogen hineinzog, langsam stärker wurde und dann wieder
schwächer. Es begann zu pulsieren. Und dann hörte es auch wieder
auf. War das die Art des Igeldings zu kommunizieren?

"Hast du etwas gesagt?", fragte Lilið es so laut und deutlich
wie Lajana es getan hatte.

Lajana schüttelte den Kopf. "Ich habe nichts gesagt und das
Igeldings kann nicht sprechen. Es hat gar keinen Mund.", sagte
sie.

"Vielleicht spricht es nicht mit dem Mund.", erwiderte
Lilið. "Sondern mit dem Gebitzel über die Stacheln."

"Ich habe schon Ja-Nein-Spiele probiert. Kennst du so
etwas?", fragte Lajana.

"Meinst du das Vorgehen, in dem du Fragen stellst, die mit 'ja' oder
'nein' beantwortet werden können, und im einen Fall rollt es
von rechts nach links und im anderen umgekehrt oder so etwas?",
fragte Lilið.

"Genau!" Lajana wirkte erfreut und flatterte kurz mit den Armen.

Lilið erinnerte sich, dass sie diese Geste als Kind auch
als Ausdruck von Freude getan hatte, aber sie hatte sich das
irgendwann abgewöhnt. Warum eigentlich?

"Also fast.", korrigierte Lajana. "Ich habe gesagt, 'ja'
ist auf mich zurollen und 'nein' von mir wegrollen. Weil
ich eine Rechts-Links-Schwäche habe. Und außerdem, weil
wir uns dann nicht einigen müssen, von wem aus rechts oder
links." Lajana seufzte. Es war kein schweres Seufzen. "Aber
wir haben uns nicht verstanden. Manchmal dachte ich, es
klappt, aber dann hat es einfach gar nicht mehr reagiert
oder ist doch nach links oder rechts gerollt oder hin
und her. Ich glaube, es wird schnell müde."

Bei der Erwähnung von Müdigkeit überkam Lilið ein Gähnen. "Das kann
ich gerade sehr gut verstehen.", murmelte sie.

"Dich belasten bestimmt ganz viele Dinge gleichzeitig.", sagte
Lajana. "Möchtest du lieber schlafen, als mit mir zu spazieren
und von Marusch zu erzählen? Möchtest du vielleicht auf einer
Decke liegen, während ich dich streichle und du erzählst
von Marusch, bis du einschläfst?"

Mit einer Königin kuscheln, dachte Lilið. Ihr Kopf versuchte,
gleichzeitig in widersprüchliche Richtungen zu denken: Dass
das doch sehr verlockend klang und dass es sich überhaupt
nicht gehörte und sie sich doch noch kaum kannten. Aber
ihr Blick wanderte zu Lajanas Händen, die mit sticken beschäftigt
waren. Die Haut hatte einen ähnlichen Braunton wie
die von Marusch. Die Finger waren vergleichsweise kurz und ihre
Bewegungen beim Sticken versprachen eine Sanftheit, die Lilið
vermisste. Sie nickte einfach. "Eigentlich schon sehr gern.", gab
sie zu. "Ich habe ein bisschen Angst davor. Davor, dir zu nahe
zu kommen. Weil wir uns kaum kennen."

"Was soll denn schon passieren?", fragte Lajana.

Was sollte denn schon passieren, wiederholte Lilið in ihrem
Kopf. Sie spürte das Kribbeln, dass das Igeldings in ihr
auslöste, in ihren Fingern und ihrem Arm. Sie
merkte, dass sie sich darauf
nicht konzentrieren konnte. Aber es hatte was mit Physik zu
tun, wurde ihr unvermittelt klar. Sie hoffte, dass sie noch
einmal Gelegenheit haben würde, sich darum zu kümmern. Ihr
Körper ließ los und legte sich einfach zur Seite ab, die
Hand in der Nähe des Igeldings belassend, sodass sie die
Schwingungen immer noch leicht spürte. Wie ein
Hintergrundrauschen.

Lajana stand auf, stopfte ihren Pullover und eine dünne Decke
um Lilið herum und rollte Liliðs Oberkörper in ihren Schoß. "Hattet
ihr Sex?"

"Was?" Lilið war plötzlich wieder viel wacher, als sie vermutet
hätte, dass es ihr im Moment möglich wäre.

"Marusch hat mit vielen Leuten Sex, die sie mag, glaube ich.", sagte
Lajana.

Liliðs Anspannung fiel wieder ab. Sie war es wirklich nicht gewohnt,
so offen darüber zu reden. Eigentlich, fand sie, war verwunderlich,
dass das Thema Sex mit Marusch, Drude und nun auch mit Lajana so
schnell auf den Tisch kam. Bei Drude hatte sie damit angefangen, fiel
ihr auf. Und Drude war nicht so richtig darauf angesprungen, oder
doch? Lilið gab sich Mühe, nicht wieder an diese Situation zu
denken, weil ihr Körper so stark darauf reagiert hatte und
sie das bei Lajana nicht wollte.

"Marusch und ich hatten Sex.", sagte sie. "Es kommt vielleicht
darauf an, was genau Sex genannt wird. Aber wir haben es so genannt."

Lajanas Hand streichelte Lilið durchs Haar. Es war so eine zarte,
zutrauliche Berührung, dass Lilið fast geschnurrt hätte.

"Hattest du Sex mit Marusch?", stellte Lilið die selbe Frage zurück.

"Nein.", antwortete Lajana. "Ich wollte eigentlich. Vielleicht. Ich
würde gern mal Sex mit einer Person haben und nicht nur mit mir
selbst. Aber Marusch meinte, wir kennen uns schon so lange. Und
sie hat viel Erfahrung und ich wenig. Das
Risiko ist zu hoch, dass sie mich dann ausnutzen kann." Nachdenklich
fügte Lajana hinzu: "Sie hat mich noch nie ausgenutzt."

"Marusch ist die wenigst ausnutzende Person, die ich kenne.", murmelte
Lilið. Schneller als sie denken konnte. "Bei Sex zumindest. Sie
hat so sehr darauf geachtet, dass ich mich wohl fühle."

"Sie achtet immer darauf, dass alle sich wohl fühlen.", stimmte
Lajana zu. "Aber das ist nicht immer gut für sie. Manche Menschen
nutzen das furchtbar aus. Also sie wird eher ausgenutzt als dass
sie ausnutzt."

"Das kann ich mir vorstellen.", murmelte Lilið. Sie versuchte, in ihrer
Erinnerung ein Beispiel zu finden. Zunächst fragte sie sich, ob sie
es getan hatte. Aber dann drangen fast gegenteilige Erinnerungen in
den Vordergrund. "Wir haben uns kennengelernt, als sie bei mir eingebrochen
ist. Sie war sehr charmant und ich habe sie wiedersehen wollen. Vielleicht
war ich verliebt." Lilið bemerkte, wie bei der Erinnerung dasselbe Gefühl
von damals in ihr aufkam. "Ich bin es immer noch.", korrigierte
sie. "Marusch wollte mich auch wiedersehen. Sie hat mir eine Option
eröffnet, wie ich aus dem Plan aussteigen konnte, den andere für mich
festgelegt hatten. Und mir dann ein Angebot gemacht, dass wir uns
danach wiedertreffen könnten. Nur wäre ich bei dieser Option fast gestorben."

Lajana sog erschrocken die Luft ein. "Was ist passiert?"

Lilið versuchte, sich gedanklich zu sortieren, was nur halb klappte, und
erzählte von den Erlebnissen von ihrer ersten Begegnung mit Marusch
bis zu ihrer zweiten eher chaotisch.

Lajana hörte genau zu und stellte Nachfragen, sodass sie alles
verstand. "Der Ball, wo ihr dann getanzt habt, war der Angelsoger
Adelsball?", versicherte sie sich.

Lilið nickte. Dabei verhedderten sich kurz Lajanas Finger in ihrem
Haar.

"Da war ich am Anfang kurz, aber ich soll bei so etwas nicht zu
lange sein, weil ich allen zu peinlich bin.", sagte sie. "Jetzt
verstehe ich, warum Marusch so aufgeregt war."

"Marusch war aufgeregt?", fragte Lilið nach. Sie dachte erst, der
warme Gedanke an Marusch, dass sie sich damals auf Lilið gefreut
hätte, sollte sie glücklich machen, aber stattdessen fühlte Lilið,
wie ihr Tränen kommen wollten. Es war schon mehrfach in diesem
Gespräch passiert, aber sie hatte es sich bis jetzt nicht eingestanden.

"Ich glaube, sie war in Sorge, dass dir etwas passiert sein
könnte, aber sie hat darüber nicht viel erzählt. Ich habe das
nur gespürt." Lajanas Berührungen wurden bei den Worten besonders
weich. "Sie hat alles getan, um dir zu helfen, aber wusste, dass
du Probleme kriegen könntest."

Lilið war nicht derselben Meinung. So sehr sie Marusch auch
verteidigen wollte. "Wenn sie mich schon in Gefahr bringt,
hätte sie wenigstens eine Möglichkeit
finden müssen, mich besser vorzuwarnen."

"Ich bin sicher, das hätte sie getan, wenn sie gewusst hätte,
wie.", beharrte Lajana. "Und ich glaube, das ist auch der Grund
für das schlimme Gewissen, das sie geplagt hat. Sie glaubt, was
falsch gemacht zu haben." Nachdenklich fügte Lajana hinzu: "Aber
vielleicht hat sie auch was vorhergeahnt und ihr ging es
deswegen schlecht."

"Vorhergeahnt?", fragte Lilið.

"Ich war noch ein paar Tage in einem Besuchshaus untergebracht, aber
auf der Rückreise zu unserem Schloss wurde ich entführt.", erklärte
Lajana. "Vielleicht hat Marusch geahnt, dass es zu so etwas kommen würde. Sie
hat mir oft erklärt, wie ich mich in so einem Fall verhalten soll."

"Willst du mir davon erzählen?", fragte Lilið.

"Nein.", antwortete Lajana schlicht.

Dann schwiegen sie eine Weile. Liliðs Erinnerungen streiften über
den zweiten Teil ihrer Reise mit Marusch. Marusch hatte versucht, ihr
zu helfen. Und doch wäre sie ohne Marusch vielleicht nicht an Bord
der Kriegskaterane gelandet, während diese zerstört worden war. Es
war nicht Maruschs Schuld. Lilið hatte sich im vollen Bewusstsein
und freiwillig ein Leben ausgesucht, in dem so etwas nun
passieren konnte. Sie hätte die Gelegenheit, sich zu diesem
Leben zu entscheiden, in jedem Fall ergriffen, selbst
wenn es nicht Marusch gewesen wäre, die
diese Türen für sie geöffnet hätte. Sie war hier auf einer
Kagutte mit Menschen, deren Macht und Brutalität wahrscheinlich
die der Wachen im Hofstaat ihres Vaters weit übertrafen. Sie
war hier um die Prinzessin zu retten. Die Königin, korrigierte
sie sich mal wieder. Und bei solchen
Aktionen war vorprogrammiert, dass sie eben früher oder später noch
dichter sinnlose, grausame Gewalt miterleben würde als daheim.

Sie erinnerte sich an die Leiche des Langfingers damals, die sie
im Kühlhaus betrachtet hatte. Sie war nicht so zerstückelt
gewesen wie Teile der Besatzung der Kriegskaterane. Aber als
sich Lilið nun an die Bilder erinnerte, die sich versucht
hatten, in ihr Gehirn zu brennen, als sie wie im Traum über
das zerberstende Deck gegangen war, konnte sie sie so wissenschaftlich
begutachten wie die Leiche damals. Es war kein schreckliches Bild.
Sollte sie sich dafür schämen, dass sie die Bilder in
ihrem Kopf eher faszinierten als erschreckten?

Sie hatte keinen Bezug zu diesen Menschen. Sie fand schlimm,
dass sie einfach ermordet worden waren, und wie sinnlos das
Ganze gewesen war. Wofür? Menschen mussten sterben, weil es
deren Aufgabe war, ein Schiff zu überfallen, das eine vermeintliche
Prinzessin an Bord hatte, die eigentlich längst auch offiziell
hätte Königin sein sollen. Andere Menschen hatten sie an Bord, um einen
Krieg zu vermeiden, der nur wegen etablierten Gesetzen und
existentem Machtgefälle geführt würde, weil die Königin einen
zu niedrigen Skorem hatte. Es war vertrackt und so sinnlos. So
große Mächte, die durch die Gegend rollten, dabei dauernd
irgendetwas platt machten und überhaupt nur existierten, weil
sie an irgendwelche abstrakten Werte geknüpft waren, die
nichts mit Glücklichkeit oder logischen Gemeinschaftsgedanken zu
tun hatten. Werte, die einfach nur ein menschenfeindliches
Weltgefüge aufrecht erhielten, aber inzwischen zu groß
gewuchert waren, als dass Menschen sie überdenken und das
Gefüge zerstören könnten. Sie steckten ja selbst mittendrin.

---

"Würdest du vielleicht Sex mit mir haben?", fragte Lajana.

Lilið schreckte aus dem Schlaf hoch, in den sie beim Nachdenken
über das Weltgefüge hineingerutscht war. "Du willst Sex mit mir?"

"Ich möchte, wie gesagt, mal Sex mit einer Person haben.", wiederholte
Lajana. "Und du bist schön weich."

Lilið wartete einen Moment, ob Lajana noch etwas sagen würde, aber
diese blieb still. "Stehst du auf mich?", fragte Lilið. "Sexuell
meine ich."

"Muss ich das?", fragte Lajana.

Lilið schüttelte den Kopf. Dieses Mal verstrickten sich Lajanas Finger
dabei nicht in ihren Haaren. Lilið bemerkte erst jetzt, dass Lajanas
andere Hand inzwischen ihren Oberarm streichelte. Das war schön. Lilið
merkte, wie ihr müder Körper auf Lajanas Zärtlichkeit in Kombination
mit der Nachfrage reagierte. Sie mochte begehrt werden, schien ihr. "Du
musst mich jedenfalls natürlich nicht sexuell anziehend finden."

"Ich glaube, das tue ich auch nicht.", sagte Lajana. "Ich fühle mich nur
sicher bei dir. Und wir sind uns eh schon nahe. Daher frage ich. Können
Menschen, die bloß befreundet sind, keinen Sex haben?"

Liliðs Körper beruhigte sich wieder. "Doch, ich denke, das geht.", murmelte
sie. "Ich habe mir darüber noch keine Gedanken gemacht. Oder doch?" Sie
dachte daran, dass sie kürzlich Drude so eine Frage gestellt hatte. "Ich
glaube, ich hätte Lust zu Sex." Von Kuscheln mit einer Königin also nun
zu Sex mit einer Königin. Lilið konnte ein Schnauben nicht
unterdrücken. "Entschuldige, ich will dich nicht auslachen.", sagte
sie schnell. "Mir kommt die Situation gerade nur absurd vor. Hast du
genauere Vorstellungen von Sex mit mir?"

"Es kommt vielen absurd vor, über Sex zu reden.", stellte Lajana fest. "Bist
du prüde?"

Lilið kicherte. "Nicht besonders, denke ich. Aber vielleicht prüder als
du."

"Ich mag gern Decken zwischen meinen Beinen reiben. Und ich
stelle mir vor, dass ich das dann einfach mit deinem Bein mache.", überlegte
Lajana. "Aber ich habe eigentlich keine Ahnung von
Sex. Meine Mutter möchte mir Sex mit Leuten allgemein verbieten. Sie
meint, ich würde ewig ein Kind bleiben und Kinder sollten keinen Sex
haben. Ich bin aber kein Kind."

Lilið hörte Wut aus Lajanas Stimme und fühlte sofort mit. Sie grub,
einem Impuls folgend, ihren unteren Arm unter ihrem Körper hervor und
legte ihre Hand auf Lajanas. Auf die, die über Liliðs Oberarm streichelte. "Ich
hatte auch relativ spät das erste Mal Sex.", sagte sie. "Mit Marusch übrigens. Das
war mein erstes Mal mit einer anderen Person. Aber ich hatte als Kind schon
Interesse an Sex. Ich finde, dass du kein Kind bist. Du bist älter als ich. Aber
selbst wenn du ewig Kind bleiben würdest, solltest es dir nicht verboten
werden."

Was sagte sie da, fragte sich Lilið. Sie meinte es, aber es war ein
haariges Thema. Sie würde sicher nicht mit einem Kind Sex haben wollen. Und
als Kind hätte sie nicht mit irgendeiner erwachsenen Person Sex haben
wollen, zumindest mit keiner von denen, die sie gekannt hatte. Sie konnte auch
nicht leugnen, dass sie sich fürchtete, dass es hier ein Machtgefälle
geben könnte, das in eine ähnliche Richtung ging wie jenes zwischen
Erwachsenen und Kindern. Eigentlich hielt sie Lajana wirklich nicht für ein Kind. Aber
sie dachte einfacher als Lilið. Auf irgendeine Weise. Und dann wieder
auch nicht, dann wieder verstand sie ganz viel. Das war allerdings bei Kindern
auch der Fall. Kinder verstanden manche Dinge viel besser als die
meisten Erwachsenen. Das war Lilið einmal aufgefallen, als sie als
jugendliche Ausbildungshilfe beim Segeln die Interaktion zwischen den
Segelkindern und den erwachsenen Aufsichten beobachtet hatte. Kinder
wurden ständig unterschätzt und hatten ihre ganz eigene Art von
Weisheit.

War Lajana doch ein Kind? Im Alter von irgendwas zwischen 25 und 35 Jahren? Wie war der
Begriff Kind definiert? War es nicht viel sinnvoller, den Begriff
Kind an Körperentwicklung festzumachen, weil die Entwicklung des Denkens
so individuell ablief? Allein durch ihr Alter hatte Lajana mehr Erfahrungen
als ein Kind gemacht.

Die Überlegungen waren zwar interessant aber eigentlich Unsinn, stellte
Lilið fest. Sie spürte deutlich, dass Lajana kein Kind war. Sie war
vielleicht in manchen Punkten naiv, aber anders naiv als die
Segelkinder. Sie kannte ihre Probleme und wusste, was sie wollte.

"Ich bin kein Kind!", wiederholte Lajana.

"Ich weiß.", antwortete Lilið.

"Aber du hast jetzt trotzdem Angst davor, Sex mit mir zu haben." Lajana
sagte es weniger emotional, eher, als wäre es einfach eine Feststellung.

Woher wusste sie das? "Ein wenig.", gab Lilið zu. "Du hast in Zusammenhang
mit Marusch auch von der Gefahr erzählt, dass sie dich ausnutzen
könnte. Ich habe Angst, dass mir das passiert."

"Warum?", wollte Lajana wissen. Und fügte hinzu: "Bedränge ich dich?"

"Du bedrängst mich nicht." Lilið schüttelte den Kopf. "Weil ich", sie
seufzte, "weil, wenn du mich erotisch anfasst, ich dich vielleicht
zurückanfasse, und dabei in eine Art Rausch geraten könnte, in dem ich
mir Dinge von dir nehme, weil du bereitwillig wirkst, die dich überfordern
könnten." Lilið atmete tief ein und aus. "Vielleicht ist das Unsinn. Ich
bin selbst nicht sehr erfahren. Und ich würde vielleicht immer nachfragen
und sehr vorsichtig sein. Ängste sind nicht immer rational."

"Ich könnte dich festhalten.", schlug Lajana vor.

"Also, dass ich gar nichts machen kann?", fragte Lilið. Dieses Mal
brauchte ihr Körper einen Moment zum Reagieren, aber er tat es. Als
sie sich vorstellte, wie Lajana ihre Arme festhielt und sich an ihr
reiben würde, zog es sich in ihr zusammen, und sie mochte das.

"Genau!", bestätigte Lajana. "Ich sage dir ein Geheimnis, ja?"

Lilið fragte sich einen Moment, ob sie Lajana sagen sollte, dass
sie vorsichtig mit dem Teilen von Geheimnissen sein sollte. Aber Lilið
hielt sich selbst für vertrauenswürdig genug und war zu müde zum
Diskutieren. Also nickte sie einfach.

"Ich mag die Vorstellung, mit Leuten Sex zu haben, die das genießen
und sich nicht wehren können.", erklärte Lajana. "Ich mag meistens
keine Machtgefälle, aber bei Sex schon. In meinem Kopf. Ich habe
Angst, darüber zu reden, aus zwei Gründen. Erstens, weil ich Angst
habe, dass die Personen, mit denen ich Sex hätte, hinterher nicht
mehr raus kommen. Also, dass es nicht wieder normal zwischen uns
werden kann. Und zweitens, weil ich immer denke, das ist doch nicht
normal. Irgendwas in mir ist kaputt."

Lilið drehte sich das erste Mal, seit Lajana sie in Pullover und Decke
verpackt hatte, und zwar so auf den Rücken, dass sie
Lajana ins Gesicht sehen könnte. "Du
bist nicht kaputt.", sagte sie eindringlich. "Ich habe kürzlich herausgefunden,
dass ich sexuell reagiere, wenn ich mich nicht wehren kann. Das war
irritierend. Aber ich glaube, Vorlieben im Zusammenhang mit sexuellen
Interessen sind einfach sehr verschieden und kurios. Du bist nicht kaputt
deswegen."

Lajana strich ihr mit den Fingern durchs Gesicht. Und legte dann
ihre Hand sachte auf Liliðs Mund und Nase.

Die Wirkung traf Lilið mit Wucht. Diese kleine Geste genügte, um sie
zu elektrisieren. Sie wehrte sich nicht. Aber für einen Moment fragte
sie sich, ob ihr das zu viel wäre und wohin das hinführen würde.

"Darf ich mich auf dich legen?", fragte Lajana.

Lilið nickte.

Lajana legte sich aus ihrer sitzenden Haltung erst vorsichtig
neben sie und rollte sich dann auf Liliðs
Körper, eines von Liliðs Beinen zwischen ihre geklemmt. Dazu hatte
sie die Hand wieder von Liliðs Mund genommen.

So schnell ging das mit Sex, dachte Lilið. Sie rührte sich nicht,
atmete nur etwas rascher. Aber etwas in ihr sperrte sich. Richtig,
sie hatten kaum noch Zeit. Wieviel Zeit hatten sie eigentlich?

Lajana drückte Liliðs Arme, die eh schon neben Liliðs Körper lagen,
an Liliðs Körper und versuchte, sie dort zu fixieren. Aber als sie
ihren Schritt über Liliðs Bein schob, zuckte Lilið doch und
kam dabei, ohne es zu wollen, mühelos gegen Lajanas Griff an. Sie
versuchte, die Arme zu entspannen.

Auch wenn nichts weiter daraus werden sollte, -- und so fühlte es
sich für Lilið gerade an --, war es ein wunderschönes Erlebnis. Sie
genoss es, nichts tun zu müssen und einfach mit ihrem Körper
dienen zu können. Sie wünschte sich, abschalten
zu können. Sich nicht darauf konzentrieren zu müssen, ihren Körper
locker zu lassen. Bei Drude war sie tiefer in diesen Zustand
gerutscht. Ein Zustand, der sie zu dem Zeitpunkt noch sehr beängstigt
hatte, und es eigentlich immer noch tat, aber nun war sie bereit dazu,
glaubte sie. Wenn sie nur abschalten könnte.

Lajana rollte von ihr wieder herunter. "Du bist stärker als ich.", murmelte
sie.

Lilið nickte. "Das ist mir auch aufgefallen." Sie versuchte, diesen einen Moment
zu greifen und abzuspeichern, den sie gerade so genossen hatte. "Ich mochte, als
du mir die Hand auf den Mund gelegt hast."

"Ja!", rief Lajana mit einem Grinsen in der Stimme. "Da hast du kurz
aufgegeben, glaube ich."

"Stimmt.", sagte Lilið. Das war es! Lajana hatte recht. "Ich würde
gern besser aufgeben können."

"Wir können das morgen weiter probieren, wenn du willst.", sagte Lajana. "Du
musst los."

Lilið wandte ihr den Kopf zu und lächelte. "Ich bin dir sehr dankbar
gerade. Weißt du das?", sagte sie. "Ich habe jetzt auch das Gefühl, weniger
kaputt zu sein."
