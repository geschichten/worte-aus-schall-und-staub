Aus Staub machen
================

*CN: Suizid als Thema, emotionale Erpressung, Misgendern, Reden
über Mord, Reden über BDSM, vor Füße spucken - erwogen,
Fantasy-Religion - mehrfach erwähnt.*

Marusch und Lilið unterhielten sich. Das machte zwar viel Spaß, war aber
bisher nicht unbedingt ergiebig. Marusch hatte sich in der Werft
ebenfalls nach einem Stück Draht umgesehen. Ihrer war nicht so gut wie
Liliðs, aber taugte trotzdem. Nun saß Lilið zwischen den Öwenen auf der
Kaimauer im Wind mit Blick in den Hafen und Marusch spazierte in
die Hafenstadt hinein. Das Signal wurde schwächer und stärker. Sie
versuchten, herauszufinden, was es schwächte. Entfernung war ein
Faktor, aber auch Häuserwände, manche mehr, manche weniger.

Sie vermuteten, dass das Igeldings sie nicht empfangen konnte, weil
sie mit ihren Drähten nicht einmal über die Insel Oesteroge hinweg
senden konnten. Aber vielleicht hatte das Igeldings auch einen viel
empfindlicheren Empfang als sie beide über ihre Haut.

Marusch hatte aus Texten im Buch geschlossen, dass es im Prinzip möglich
war, über die magnetischen Wellen sozusagen ähnliche Laute zu erzeugen
wie durch Sprechen. Sie hörten, weil Schall sich durch Luft
bewegte, und auch durch anderes Medium wie Mauern, aber schlechter
als durch Luft. Schall hatte etwas mit Druckveränderungen zu
tun, so viel hatten sie verstanden. Die magnetischen
Schwingungen hatten entgegen etwas mit der Veränderung von Magnetfeldern
und ähnlichem zu tun. Diese Schwingungen konnten sogar
leeren Raum durchdringen, erklärte das Buch. Auf
diese Weise konnten die Alligel im Weltraum über große Distanzen
kommunizieren. Aber es gab Materialien, die auch diese Schwingungen
abdämpften.

Marusch und Lilið hatten noch nicht herausgefunden, wie sie über diese
Schwingungen Buchstaben unterscheidbar senden konnten. Sie konnten
starke und schwache Schwingungen erzeugen. Es war ihnen auch
möglich, schnellere und langsamere Schwingungen zu unterscheiden. Das
war in sofern interessant, als dass es genau die Charakteristika
waren, die bei den geschriebenen Buchstaben der Alligel deutlich
hervorstachen, also auch umgekehrt waren, was für die Alligel an
akustischer Sprache hervorstach: stärkere oder
schwächere Schwingungen entsprachen lauteren oder
leiseren Tönen, und langsamere oder schnellere Schwingungen
höheren oder tieferen Tönen.

Sie hätten mit der Bandbreite an unterscheidbarem Signal durchaus
ein Alphabet entwickeln können, oder alternativ eine Ansammlung
hilfreicher Wörter, aber so weit waren sie noch nicht. Zum einen, weil
sie einfach noch nicht sicher genug darin waren, das zu senden, was
sie senden wollten, und zum anderen, weil sie eine andere Fragestellung
priorisierten: Sie wollten herausfinden, was die Schwingung abdämpfte. Zu
dem Zweck hatten sie nur wenige Zeichen vereinbart. Hauptsächlich sendeten
sie ein möglichst konstantes Signal und untersuchten, welche Faktoren außer
Distanz beeinflussten, wann es schwächer wurde.

Die Zentral-Sakrale hatte immerhin das Signal des Igeldings vollkommen
abgeschirmt. Sie hofften, mit den Erkenntnissen vielleicht ein Brett
oder so etwas finden oder basteln zu können, dass die Schwingung abschirmte. Sie
hofften, damit dann die Richtung ermitteln zu können, aus der das Igeldings
sendete. Sie hatten keine Ahnung, ob die Idee vielversprechend wäre, aber
das Forschen machte ihnen Spaß und sie hatten nichts Besseres zu
tun, bis der Mast ausgetauscht wäre. Und so saß Lilið auf der
Kaimauer, Öwenen kreisten um sie herum, schrien in den Wind und
spien Feuer, und fühlte unter der Haut das gleichmäßige gewohnte
Signal des Igeldings, sowie das neue, unstete Signal von Marusch,
dem sie fortwährend antwortete.

Es begann gerade, nicht mehr all ihre Konzentration zu kosten, als
Allil an die Kaimauer trat. "Kann ich zu dir kommen?"

Lilið schluckte. Das sollte sie vielleicht weniger tun, wenn Allil
näher wäre, überlegte sie.

"Du kannst 'nein' sagen. Das ist dein Recht!", erinnerte Allil.

Lilið tat kurzentschlossen eine einladende Handbewegung. "Ich
bin beschäftigt und werde den Draht priorisieren."

Allil nickte, lächelte und schritt über den Kai zu ihr. Sie ließ
sich in brauchbar angenehmem Abstand neben Lilið nieder. Das Wasser
gluckste unter ihnen gegen die Kaimauer. "Ich
war in der örtlichen Sakrale. Das ist oft ein guter Ort, um
aktuelle Informationen über das Geschehen zu bekommen. Auch auf
Baeðisch, mein Alevisch ist noch nur bruchstückhaft.", berichtete
Allil. "Und was ich dort erfahren habe, wollte ich noch mit euch
teilen, bevor ihr abfahrt. Aber die Teeseufel ist gerade nicht
da und du bist gerade die einzige Person, die ich aus eurer Crew
gefunden habe. Außer Heelem, aber Heelem beaufsichtigt die
Reparatur und ist nicht abkömmlich."

"Drude und Lajana sind durchs Feld spazieren gegangen und
Marusch ist irgendwo in der Hafenstadt unterwegs.", informierte
Lilið. Es war nicht so einfach, zu senden, während sie gleichzeitig
sprach, aber sie bekam es hin. "Du hättest gern mit einem
anderen Crewmitglied gesprochen, nehme ich an?"

"Mit Marusch am liebsten, natürlich.", murmelte Allil. "Ich
würde gern mit dir besser auskommen, aber wir hatten einen
unverzeihlich miesen Start. Das verstehe ich."

"Sagen wir, ich habe gewisse Schwierigkeiten, dir zu
vertrauen, dass du mich nicht plötzlich aus mir nicht
ersichtlichen Gründen aus dem Hinterhalt angreifst.", hielt
Lilið fest. Sie grinste dabei. "Dein Mordversuch kam eben
schon recht unvermittelt für mich. Vielleicht ist das
üblich bei Mordversuchen. Unvorhersehbarkeit erhöht
Erfolgschancen. Aber ich könnte dir mehr vertrauen, wenn
ich nicht voll bereit gewesen wäre, unseren Deal
durchzuziehen."

Allil seufzte. "Ich habe dich sehr falsch eingeschätzt. Was
keine Rechtfertigung sein soll. Soll ich ein bisschen was
über mich und meine Motive erzählen?"

Lilið nickte. Ein paar Sekunden vergaß sie, zu senden, aber
ihr fiel es sofort ein, als Marusch darauffolgend im Senden
sozusagen stutzte.

"Ich bin nach den meisten Ethiken kein sehr korrekter Mensch. Mir
bedeuten Menschenleben wenig.", leitete Allil ein.

Lilið nickte. Der erste Satz kam ihr von Marusch bekannt vor. Der
zweite wirkte selbst für Marusch eher weniger typisch. Oder
nicht?

"Ich bin gewohnt, dass mir mein Leben bedroht wird, einfach weil ich eine Frau
bin oder weil ich den Leuten zu unskorsch bin oder weil ich nicht adelig bin
oder auch meinetwegen, weil ich stehle. Ich messe deshalb vielleicht Leben
weniger Wert bei. Weil ich nicht damit zurechtkommen könnte, dass mir etwas
Wertvolles ständig bedroht wird, was der Fall wäre, wenn ich Leben einen großen
Wert zuornden würde. Ergibt das Sinn für dich?" Allil kratzte sich am Kopf.
"Also, ich möchte klarstellen, dass das keineswegs eine normale Reaktion auf
das System ist. Ich kenne auch Leute, die haben ähnliches erlebt wie ich und
sind da ganz anders. Nur bei mir persönlich kam es dazu."

Lilið fühlte sich unter Druck gesetzt, zu sagen, dass es
Sinn für sie ergäbe, was es aber nicht unbedingt tat. Sie
hatte keine Einschätzung dazu. "Kann ich dir erst einmal zuhören
und dann irgendwann später herausfinden, ob es für mich
Sinn ergibt?"

"Klar!", stimmte Allil zu. "Ich hatte das vor allem gefragt, weil
ich wissen wollte, ob ich mehr dazu erklären muss oder soll. Hm."

"Musst du nicht. Ich denke, ich habe deine Argumente
verstanden." Lilið verhaspelte sich fast, weil eine Sendelücke
durch Marusch sie wieder ans Senden erinnerte. Dieses Mal
blieb Maruschs Antwort jedoch aus. Entsprechend sendete
sie das Zeichen dafür, dass sie nicht empfing, und rechnete
damit, dass es nicht ankäme. Zumindest war es bisher symmetrisch
gewesen: Entweder es ging in beide Richtungen oder in beide nicht.

"Natürlich heißt das nicht, dass ich einfach jede beliebige
Person ermorden würde. Es ist weder ungefährlich, noch macht
es Spaß." Allil grinste, aber es wirkte nicht glücklich. "Vor
allem möchte ich Menschen nichts antun, die
mir etwas bedeuten. Marusch zum Beispiel. Aber wenn ich eine
Person vor mir habe, die durch ihre Position in der Gesellschaft
eher zu den Leuten gehört, die, sobald ihnen etwas an mir
nicht passt, durch ihr bloßes Erscheinen bewirken können, dass
ich hingerichtet werde, komme ich dem lieber zuvor."

"Jemand wie ich, meinst du." Lilið runzelte die Stirn. Das
Signal war immer noch nicht wieder da.

"Ich meine, selbst wenn du vorhattest, dich an die Zusage
zu halten, dass du vom Erdboden verschwindest: Wieviel Energie
investierst du darein, dass du nicht ausversehen doch gefunden
wirst?", fragte Allil. "Wie sehr priorisierst du in einer
Situation, die für dich vielleicht unangenehm werden könnte,
dass ich geschützt bleibe?"

"Viel.", sagte Lilið ohne Gefühlsregung. "Immer noch
übrigens."

"Das weiß ich jetzt. Da habe ich dich eben sehr falsch
eingeschätzt.", sagte Allil. "Ich hätte dich eher für eine
Person gehalten, die ihren Vorteil nutzt. Eine, die im Fall, dass
zur Debatte steht, enterbt zu werden oder in schwierigere
Lebensumstände zu geraten, schreit 'eigene Sicherheit
geht vor' und mich mit gutem Gewissen ausliefert. Und vor
allem habe ich dich nicht für eine Person gehalten, die
beabsichtigt, das System zu ändern. Sondern eben
eher für eine, die ins System zurückkehrt, sobald es anders
zu unbequem wird."

Lilið nickte, dann schüttelte sie den Kopf, dann erschreckte
sie sich, als das Signal wieder einsetzte. Also sendete
sie wieder zurück. Was war da zwischen Marusch und ihr
gewesen? Was hatte das Signal so stark unterbrochen? Sie
war sehr neugierig auf Maruschs Erzählungen. Aber das
müsste warten. Sie sendete weiter und fand
den Gesprächsfaden mit Allil wieder. "Ich habe immer
gesehen, dass dein Leben an meiner Unsichtbarkeit hängt, und
mir war wichtig, mit der Verantwortung gut umzugehen.", versicherte
Lilið. "Zu dem Zeitpunkt, als wir uns kennen gelernt hatten,
war ich mir noch nicht im Klaren darüber, dass ich selbst dabei
mitmischen möchte, das System zu ändern. Insofern
hattest du da vielleicht recht. Aber ich wäre nicht leichtfertig
mit deinem Leben umgegangen."

"Nun ja." Allil streckte die Beine aus und tauchte die Füße
ins Wasser. Eine Öwene, die in der Nähe gebadet hatte, spie
ihr erschreckt Feuer entgegen und flatterte davon. "Du
hast vielleicht nicht beabsichtigt, der blutige Master
M zu werden und dann irgendwann in der Identität auch
noch als Lilið von Lord Lurch entarnt zu werden, aber das
gefährdet eben mein Leben auch, weil wir zusammenhängen. Ich
kann nicht wieder einfach Allil werden und hätte dann nichts
mehr damit zu tun. Es existieren Nachweise, dass ich du bin."

Lilið nickte. "Das tut mir leid."

"Red keinen Quatsch, das ist nicht als Vorwurf
gemeint.", widersprach Allil. "Eigentlich verbündet mich das eher
mit dir. Weil du nun auch in der Situation bist, dass dir
Fehler, wie von der falschen Person Essen bekommen, das Leben
kosten können. Wie mir eben."

Lilið kicherte. "Du magst Leute abhängig davon, wie sehr sie
in Lebensgefahr schweben?"

Allil schnaubte und schüttelte den Kopf. "Ich fühle mich
solidarisch mit Leuten, die das in ähnlicher Weise tun wie
ich. Das heißt nicht, dass ich sie mag.", stellte sie klar.
"Ich mag Leute, wenn sie sich Mühe geben, mich zu verstehen,
glaube ich. Ich weiß es nicht genau."

"Du magst Marusch.", murmelte Lilið. "Und ja. Marusch versucht
wirklich, dich zu verstehen."

"Du vielleicht jetzt auch.", mutmaßte Allil. Sie ließ den Kopf
hängen und beobachtete die schwappenden und gluckernden
Hafenwellen unter ihnen, in denen sie sich sehr verzerrt
spiegelte.

"Vielleicht.", stimmte Lilið zu. "Was, selbst wenn, bei mir
übrigens nicht heißt, dass ich dich mag."

"Ich weiß." Allil erzeugte mit den Füßen Wellen, die ihre
Spiegelbilder noch mehr verzerrten bis nicht einmal mehr
erkennbar war, dass es welche waren. "Mir würde reichen, wenn du dich
nicht mehr vor mir fürchtest."

Maruschs Senden war über die letzten Minuten immer schwächer
geworden und wurde nun wieder unterbrochen. Also sendete Lilið
noch einmal das vereinbarte Zeichen für diesen Fall, das Marusch
vermutlich nicht empfing.

"Jedenfalls weiß ich aus der Sakrale, dass das Bundesorakel und zwei
der drei Monarchiefamilien, die noch im Rennen sind, Adel zusammensuchen, der
Regierungsaufgaben übernehmen kann." Allil setzte sich gerader hin, als
wäre sie erst jetzt richtig angekommen, und strich ihren Rock
glatt. Ein blass lavendelfarbener Rock, den Lilið noch
nicht an ihr kannte. Ob sie ihn frisch gestohlen hatte? "Königsfamilie
Stern wird vermutlich
wegen verschiedener Vorfälle nicht weiter regieren, aber auch das
wird erst auf dem Treffen endgültig beredet und ist noch nicht
klar. Es steht in den Sternen, sozusagen." Allil grinste
kurz, aber hörte damit auch rasch wieder auf, als Lilið
nicht darauf einging. "Ich habe nur Informationen über den Fall, dass
die Sterne aus dem Rennen sind, und informiere dich über den
Fall, ohne auf andere einzugehen. Ja?"

Lilið nickte verwirrt. Sie beschloss erst einmal zuzuhören
und sich erst später Fragen auszudenken.

"Es sollen mächtige Adelige Vasallschaften
annehmen, und die Kriterien dafür gehen nicht nur um skorsch sein. Auch,
aber nicht nur.", berichtete Allil. "Königin Dornrose, bei der
es gut möglich ist, dass ihr das alte Königreich
Sper und Inseln darüber hinaus bis einschließlich Nederoge
zugesprochen werden, möchte mehr Frauen in die
Politik einbinden, selbst wenn sie nicht so skorsch sind. Aber auch
nicht unskorsch, sonst würden die Orakel da nicht mitziehen. Und natürlich
denkt sie bei Frauen nicht an Marusch, aber durchaus an dich. Grah!"

Lilið überrasche Allils Wut ein wenig. Ihr war nicht einmal
bewusst gewesen, wieviel Allil über sie in dem Punkte wusste. Sie
erinnerte sich aber auch nicht, ob sie damals ein Gespräch dazu
gehabt hatten. "Damit, dass ich geschlechtlich irgendwie benachteiligt
bin, hat sie wohl schon recht, aber ich bin eben keine Frau."

"Ja, ich weiß. Und es nervt. Also, dass wir drölfzig Jahre gewartet
haben, bis sich Leute damit auseinandersetzen, etwas gegen geschlechtliches
Ungleichgewicht, oder wie das heißt, zu tun, aber dann überlegen sie bei
der Gelegenheit nicht, wie sie den Kram zu Ende denken." Allil sprach
immer schneller und unterbrach den Redefluss nur mit einem Wutgeräusch. "Es
muss so beschissen sein, eigentlich mitbetroffen zu sein, und dann
bei den Problemlösungsansätzen nicht mitbedacht zu werden, obwohl
das Problem dich und Marusch vielleicht noch härter trifft. Immerhin
ist im Königreich Dornrose schon etwas bekannter, dass es Leute wie
euch gibt, aber Königin Dornrose meint, sie wolle ein Problem nach
dem anderen lösen. Sie realisiert dabei nicht, dass es dassselbe
Problem ist und dass sich das nicht zerteilen lässt." Allil
rang die Hände und schüttelte sich. "Ich schweife ab, ich wollte
da gar nicht so ins Detail gehen."

"Betrifft dich das auch irgendwie?", fragte Lilið.

Allil schüttelte den Kopf. "Ich habe mich nur mit Marusch
unterhalten. Übrigens auch über dich.", sagte sie. Sie
atmete ein und seufzend aus, um sich zu beruhigen. "Zurück zum
Thema: Ich habe mich nicht nur unter den Sakrals-Dienenden
umgehört, was wegen meines gebrochenen Alevisch ohnehin
nicht ganz leicht war, ich bin auch in ein Büro eingebrochen und habe
Briefe gelesen. Lesen in der Sprache fällt mir leichter. Und Listen
habe ich gefunden, dazu komme ich später. Außerdem habe ich die Sakraleten belauscht, die
eine dolmetschende Person dabei hatten, weil eine Sakralet
aus Angelsoge angereist ist. Ich hoffe, du nimmst mir mein
kriminelles Vorgehen dabei nicht allzu krumm."

Lilið schüttelte den Kopf. "Ich habe zum einen gerade gar keine
Energie, dir so etwas krumm zu nehmen, und zum anderen habe ich
auch ein Ding gedreht, dass Gespräche in Sakralen belauschen
mit einschloss." Und auch da, fiel Lilið ein, war Alevisch eine
Barriere für sie gewesen. In dem Zusammenhang wurde ihr bewusst,
dass es spannend werden würde, welche Sprachen sie in Zukunft
sprechen würden. Eigentlich wurde die Sprache durch die
amtierende Regierung vorgeschrieben. Aber im Königreich Dornrose
wurde Gymlisch gesprochen, wenn Lilið sich richtig erinnerte, eine
Sprache, die sie nicht einmal im Schulunterricht gehabt hatte. Sie
hatte neben Alevisch noch Helisch im Unterricht gehabt, weil
das zugehörige Königreich mit ihrem politisch mehr zu tun
gehabt hatte.

Allil grinste (das Grinsen bezog sich wohl noch auf Liliðs Bericht über ihr
betrügerisches Vorgehen in einer Sakrale), holte tief Luft und berichtete:
"Königin Taktika ist noch sehr jung, aber bekommt wahrscheinlich Angelsoge und
den Rest des alten Königreichs Stern zugeteilt. Ihr Königreich ist ohnehin
schon sehr groß. Daher fällt ihr Nederoge wahrscheinlich nicht auch noch zu.
Angelsoge und Umgebung wird herausfordernd genug für sie werden. Sie lässt sich
strategisch von ihrem Vater und einigen treuen Schutzbefohlenen mit Ahnung von
Politik beraten, wer in Machtpositionen das geringste Konfliktpotenzial
verursachen würde." Allil blickte sich um und rückte näher an Lilið
heran. Leiser fuhr sie fort: "Es gibt
also diese geheimen Listen, auf denen die Namen der Leute stehen, die wohl für
Vasallschaften gefragt und entsprechend auch zur großen Versammlung eingeladen
werden, wenn sie abkömmlich sind. Für Angelsoge, also von Königin Taktika
ausgewählt, ist Heelem auf Platz zwei. Für Nederoge hat dich Königin Dornrose
auf Platz eins mit einem großen Fragezeichen notiert."

"Was?", fragte Lilið entgeistert. "Ich bin eine gesuchte Verbrechensperson!
Verstehe ich das richtig? Ich soll trotzdem eine Insel quasi regieren?"

"Deshalb das Fragezeichen.", erklärte Allil. "Königin Dornrose hat
sich deine Geschichte berichten lassen, und kommt zu dem Schluss, dass
dir vielleicht Unrecht getan wurde. Sie würde gern mit dir
reden, um herauszufinden, wer du bist und ob du das Erbe deines
Vaters antreten kannst."

"Ist meinem Vater etwas passiert?", fragte Lilið.

Allil blickte sie einige Momente mit einem Gesichtsausdruck
an, in den Lilið vielleicht den Versuch las, Sorge für sie
zu empfinden und zu signalisieren. Wenn das die Absicht war,
scheiterte er an beidem. Der Versuch machte ihr allerdings
durchaus Angst. War er gestorben? Interessanterweise
fürchtete Lilið sich als erstes davor, nicht zu trauern.

"Ihm wird Machtmissbrauch vorgeworfen.", sagte Allil. "Ich
weiß kaum Details. Er soll manche Menschen, die ihm sympathisch
waren, sehr bevorteilt haben, hingegen Menschen, die ihm egal
waren, erpresst, damit sie Entscheidungen zu seinen
Gunsten fällen. Es gibt Verhandlungen gegen ihn. Eigentlich
schon lange, aber sie wurden nun wieder aufgenommen. Königin
Stern hatte bis jetzt versucht, es unter den
Tisch zu kehren, weil sie ihn wiederum mag. Königin Dornrose
hat andere Prioritäten. Wie gesagt: Frauen. Wenn das
alte Dornröschen leicht an eine Möglichkeit kommt, einen Mann
zu entmachten und eine vermeintliche Frau zu ermächtigen, tut
sie es."

"Ah.", sagte Lilið. Sie hatte von den Verhandlungen vor
mehreren Jahren am Rande mitbekommen. Ihr Vater hatte sie als
reine Formsachen abgetan. Lilið hatte das nie hinterfragt, weil
er es immer so selbstverständlich und beiläufig wie etwas
Normales dargestellt hatte, was eben zu dem Status eines
Lords dazugehörte. Üble Nachrede gab es immer. Letzteres
stimmte wahrscheinlich trotzdem. Nun wusste Lilið nicht, was
sie denken sollte. Ihr fehlte Einblick, vor allem
unvoreingenommener Einblick.

"Für Mazedoge samt Inselkomplexen kommt Drude in Frage. Dey
steht auf der Liste auch auf Platz eins, und zwar nicht, weil
dey für furchtbar geeignet gehalten wird, sondern weil
alle besseren Anwärtenden tot sind.", beendete Allil
die Aufzählung.

"Was heißt hier eigentlich Vasallschaften?", fragte Lilið. "Dass
die Inseln am Ende im Prinzip zu Königreich Dornrose und Königreich
Taktika gehören und die entsprechenden Königinnen das letzte
Wort hätten, aber wir, weil die beiden Königinnen zu weit weg
sind, die eigentliche Regierungsarbeit machen und im Wesentlichen,
wenn wir nicht offensichtlichen Unfug bauen, Entscheidungsfreiheit
haben?"

"Ich denke, genau das, ja.", bestätigte Allil. "Also, abgesehen vom
Titel bekämst du zum Beispiel fast die Rolle einer Königin. Oder,
wenn du es dann durchgesetzt bekämst, dass Leute dich
so nennen, einer Königsperson oder wie du auch immer
bezeichnet werden wolltest."

"Einer Vasallsperson dann. Hm.", machte Lilið. Sie runzelte die
Stirn und lauschte für einen nachdenklichen Moment unaufmerksam
dem Öwenengekreisch, die am Ende der Kaimauer ein Spektakel
veranstalteten. "Was wenn ich nicht will?"

Allil zuckte mit den Schultern. "Du wirst nicht gezwungen.", erwiderte
sie. "Aber ich würde dich gern auf dem Posten sehen wollen. Du
würdest was verändern, was Dinge für mich besser macht. Und
für andere."

"Hm.", machte Lilið noch einmal. Ein großer Haufen Gefühle und
Gedanken zum Für und Wider überflutete sie. Sie brauchte
nicht lange, um eins der Gefühle herauszugreifen: Pflichtgefühl. Sie
hätte vielleicht die Möglichkeit dazu, in eine Position zu
gelangen, die sie ermächtigte, Dinge besser zu machen. Müsste
sie die nicht ergreifen?

Aber wenn sie Vasallperson werden würde, hieße das auch, dass
Lajana nicht Königin werden würde. Lilið erwischte sich
dabei, dass sie eigentlich auch nicht geglaubt hatte, dass
es dazu kommen könnte, so sehr sie es sich wünschte. Sie würde sich
trotzdem, wenn ihr Wort schon vorübergehend Gewicht haben sollte,
auf jeden Fall dafür einsetzen.

"Ich würde verstehen, wenn du nicht wolltest.", unterbrach
Allil ihre Gedanken. "Es gibt sicher viele Gründe, aber einen
möchte ich noch einmal explizit nennen, weil ich mich damit
auskenne: Als Blutiger Master
M wirst du immer besonders gefährdet sein, selbst wenn du
entlastet wirst. Das, was dir angehängt wurde, kann zu jederzeit,
wenn du gerade nervst, wieder ausgebuddelt werden. Es ist
besonders für dich ein riskanter Posten. Das Antreten selbst
allein schon. Deine Chancen, ernst genommen und entlastet
zu werden, sind am besten, wenn du zur Versammlung
des Bundesorakels und der Monarchie gehst. Und du weißt selbst,
wie gefährlich das für dich wäre."

Lilið nickte und blickte nachdenklich aufs Meer hinaus. Sie
realisierte, dass sie schon seit einer Weile zu senden aufgehört
hatte. Das war ungünstig. Marusch musste denken, dass sie
sie nicht empfing. Aber jetzt würde es ihr Experiment
vermutlich nur noch mehr durcheinander bringen, wenn sie
mit dem Senden wieder anfinge.

"Das war, was ich erzählen wollte, bevor ich mich aus
dem Staub mache.", schloss Allil.

"Danke!", sagte Lilið. Und sie meinte es.

---

Von den Königsfamilien Dornrose und Taktika hatte Lilið bis jetzt nur
am Rande gehört. Die Monarchien waren einfach zu weit weg. Nederoge
gehörte zu dem Rand des Königreichs Stern, das an Königreich
Sper angrenzte. Sie bräuchte selbst mit den schnelleren Reisefragetten
zu fast jeder Zeit im Jahr jeweils Wochen, um in einem der anderen
Königreiche anzukommen. Zum Spätsommer und zum Ende des Winters gab es
jeweilss ein paar Wochen, zu denen der größte Teil des
Inselkomplexes von Königreich Taktika erreichbarer war. Spätsommer
hatten sie gerade. Vielleicht traf sich das für die besagte
Versammlung.

Jetzt sollten also die Königreiche Stern und Sper, die sie
kannte, mangels Nachwuchs bei König Sper und wegen anderer
Probleme bei der Königsfamilie Stern von den Königreichen
außen herum geschluckt werden. Das war ein
interessantes Gefühl. Es versprach sehr viel Veränderung. Aber
versprach es gute Veränderung? Wieviel davon hätten sie
wirklich in der Hand?

Lilið spazierte durch die Stadt auf der Suche nach Marusch, während
sie drüber nachdachte. Es war eine verhältnismäßig uninteressante
Stadt. Sie war klein und unscheinbar. Eigentlich wäre Lilið
am liebsten einfach auf die Yacht mit
den anderen zurückgegangen. Und es war ein seltsames Gefühl, dass
das gerade nicht ging. Es war doch noch gar nicht so lange so, dass
sie dort zusammen wohnten, aber es fühlte sich bereits nach Zuhause an.

Marusch sendete hin und wieder etwas. Aber
Lilið hatte nach wievor Schwierigkeiten damit, eine Richtung
auszumachen. Und ihr Kopf war zu voll, um sich über Strategien
Gedanken zu machen.

Sie fand Marusch schließlich in einem öffentlichen Garten in der
Nähe der kleinen Sakrale. Sie saß auf einer Steinbank im Schatten einiger
Bäume, aus denen sich träge Drachenschwänze herabringelten, und
blickte auf, als sie Lilið kommen sah. Etwas lag in ihrem Ausdruck,
das Lilið besorgte. Sie setzte sich dazu. "Ist etwas passiert?"

Marusch hob die Brauen. "Mir nicht."

"Sondern?", fragte Lilið.

"Du hast irgendwann aufgehört, zurückzusenden.", sagte
Marusch. "Das wäre ja nicht so schlimm gewesen. Vielleicht
ist dir etwas dazwischen gekommen. Wer weiß? Jedenfalls
habe ich dich dann im Hafen gesucht, wo du nicht mehr warst. Ich
habe dich und niemanden gefunden. Das hat mir Sorgen gemacht."

"Oh." So weit hatte Lilið nicht gedacht. "Es tut mir leid."

"Egal.", meinte Marusch. "Du wirkst durch den Wind. Wenn
Kommunikation gerade nicht klappt und du irgendwie weg
sein musst, ist das immer in Ordnung. Mit den Sorgen
werde ich schon fertig. Die sind nur gerade so arg, weil
um uns herum so viel passiert und du gesucht wirst. Und weil
in unserem Umfeld eine Entführung eben schon mal vorkommt
und so weiter. Aber gleichzeitig verstehe ich um so
mehr, wenn du dann mal Rückzug brauchst. Auch nicht
abgesprochenen."

"Halt warte!", unterbrach Lilið Maruschs Redefluss. "Du
hast mich nicht gefunden, weil ich dich gesucht habe. Was
keinen Sinn ergibt, weil du wusstest, wo ich war und natürlich
kommen würdest, wenn ich nicht sende, während ich nicht
wusste, wo du warst. Aber ich habe verpeilt, sinnvoll
zu denken."

Marusch grinste auf einmal und öffnete die Arme, damit
Lilið sich gegen sie lehnen könnte. Das tat Lilið auch. Nun
roch sie nicht nur die harzigen Bäume und die vielen
verschiedenen Blüten in diesem Garten, sondern auch
Marusch. Ganz nah. "Allil hat mich gefunden und mit mir
Informationen über die Zukunft der Politik geteilt.", fasste
Lilið zusammen. "Aber eigentlich möchte ich das nachher auch
den anderen erzählen und gerade mit dir Zeit teilen, die mehr
für uns ist."

"Gern." Maruschs Stimme klang ganz sanft, aber vielleicht
auch ängstlich. "Was stellst du dir darunter vor?"

"Mehrere Möglichkeiten.", antwortete Lilið. "Wir können über
Drähte reden. Darüber, was du herausgefunden hast. Wir können
uns irgendein Gebüsch suchen und zärtlich werden. Oder", Lilið
zögerte etwas, suchte nach Worten. "Oder du kannst mir
erzählen, was dich belastet. Wenn du das brauchst."

Marusch küsste ihr ins Haar. "Da ist tatsächlich eine
Sache, die ich teilen möchte, weil sie in mir bohrt. Aber
sie ist vielleicht schon sehr belastend."

"Dafür bin ich gern da.", versicherte Lilið.

Marusch begann nicht sofort mit dem Reden. Lilið
fühlte ihren Brustkorb gegen ihre Seite und ihren
Unterarm atmen. Marusch fühlte sich so weich und
verletzlich an. Lilið fädelte ihre Arme durch Lücken,
die erst dafür geschaffen werden mussten, um Marusch
zurückzuumarmen.

"Ich habe es damals sehr befreiend und schön empfunden, mit
dir über Suizid reden zu können.", sagte Marusch schließlich. "Das kann
ich nicht, wenn Lajana dabei ist. Sie bekommt sicher
mit, dass ich nicht stabil bin, aber ich möchte nicht, dass
sie weiß, wie weit das reicht. Kann ich dich darum bitten,
dass du darüber nicht mit ihr redest?"

Lilið nickte. "Das hätte ich auch so nicht gemacht.", sagte
sie. "Das ist ein Teil von dir, von dem ich denke, dass nur
du entscheiden solltest, wer ihn sehen darf."

"Danke." Marusch flüsterte es in ihr Haar. "Danke, dass
ich diesen Raum bei dir haben darf, wo das bleibt und
sein darf. Und sag bitte, wenn es für dich zu viel wird. Und
wenn du mit einer Person darüber reden musst, dann ist das
in Ordnung. Nur bitte nicht mit Lajana."

"Ich muss mit niemandem darüber reden.", versicherte
Lilið. "Im Moment zumindest nicht."

Marusch schloss die Arme für einen Moment fester um
sie. Das war schön. "Und dann, ein anderes Thema: Wie
war die Situation für dich, als ich mich Drude
unterworfen habe?"

Lilið kicherte. "Ich fand dich niedlich dabei.", sagte
sie. "Darf ich dich niedlich finden? Ich kann mir vorstellen,
dass das vielleicht, hm, objektivizierend ist? Oder abwertend?"

"Nein, ist es nicht." Marusch schüttelte den Kopf. Ihr Haar
kitzelte dabei Liliðs Stirn. "In manchen Kontexten und von
manchen Menschen finde ich das unangenehm, niedlich empfunden
zu werden. Aber du darfst mich
immer niedlich finden." Marusch küsste sie aufs
Ohr. Und grinste, als Lilið die
Luft einsog. "Nach meinem Zeitgefühl haben wir die
Teeseufel in einer halben Stunde wieder.", sagte Marusch. "Ich
denke, wir wollen dann auch direkt aufbrechen. Daher
sollten wir uns, wenn wir uns ein Gebüsch suchen, nicht
zu lange darin aufhalten. Möchtest du trotzdem?"

Lilið nickte.

---

Aber sie fanden keins. Keins, dass sie als geeignet genug
empfanden. Im Nachhinein hätten sie vielleicht den
Schatten einer verwinkelten Mauer gewählt, aber da hatten sie
noch gehofft, eine noch bessere Möglichkeit zu finden. Der
Spaziegang durch die Nachmittagssonne mit den vielen
Überlegungen über das Wenn und Wie war trotzdem schön
gewesen. Auch die vielen kleinen, neckenden Küsschen
zwischendurch und das Gespräch über die Schwingungen, das
sich ihnen immer wieder als Thema aufgedrängt hatte: Marusch hatte
herausfinden können, dass auch die Sakrale in dieser Hafenstadt
die Schwingungen besser abdämpfte als alle anderen Gebäude. Sie
hatte heimlich einen Stein aus dem Keller der Sakrale
stibitzt, in einem unterirdischen Gewölbe, in dem
umgebaut wurde, und schleppte ihn nun im Rucksack mit sich
herum. Dort im kühlen, feuchten Keller war der Empfang
für Liliðs Gesende zum Beispiel ganz weg gewesen. Die Schwingungen
des Igeldings war noch geschwächt durchgedrungen.

---

Als sie die Teeseufel erreichten, waren Drude und Lajana schon
da und bereiteten ein Abendessen vor. Heelem traf kurz nach
ihnen ein. Er hatte sich noch in der Zentrale der Nautikae
verabschiedet und sich für die Unterstützung bedankt. Er brachte
eine weitere kleine Kiste Vorräte mit, die die Nautikae hatten
entbehren können.

Beim Essen berichtete Lilið alles, was Allil ihr erzählt
hatte. Es wühlte sie alle auf verschiedene Weisen auf.

"Nun ergibt mehr Sinn, dass Muddern Lilið vorher sehen will.", überlegte
Marusch.

Drude nickte. "Willst du zum Vorabtreffen mit Königin Stern?" Dey
richtete sich an Lilið. "Ich würde mitkommen, wenn du möchtest, und versuchen, auf
dich aufzupassen. Ich bin unabhängiger von ihr als Marusch und
traue mich vielleicht, eher und weniger extrem einzuschreiten. Mit
dem aktuellen Stand der Dinge darf ich vermutlich mit dazukommen."

Lilið nickte. "Ich habe auch Angst, aber ich denke, ich möchte
mit.", sagte sie. "Und wenn es ist, um die Gelegenheit zu
nutzen, einigen Monarchie-Leuten vor die Füße zu spucken." Sie
räusperte sich. "Das mache ich natürlich nicht. Aber es war
auch ein erheiternder Gedanke, als ich in der Zentral-Sakrale
festgehalten worden bin."

Marusch kicherte. "Da hättest du mir vor die Füße spucken
können.", sagte sie. "Und ich hätte es verstanden. Fühl
dich frei."

Lilið schüttelte den Kopf.

"Falls das nicht ohnehin klar ist, ich begleite Marusch
und Lajana, und auch euch zwei, wenn ihr wollt, natürlich
auch überall hin.", versprach Heelem.

Einen Moment aßen sie schweigend. Lilið mochte die
Stimmung. Den Zusammenhalt.

"Wenn du mit dabei bist, Lilið, dann traut sich Muddern vielleicht
wenigstens die Suiziddrohungen nicht.", murmelte Marusch.

Lilið hob entgeisternd die Brauen. "Sie sagt so etwas
wie, wenn ihr das nicht macht, bringe ich mich um?"

Marusch schüttelte den Kopf. "Perfider.", sagte sie. "Immer
dann, wenn ihr nicht in den Kram passt, wer Lajana und ich
eigentlich sind, redet sie fast wie zufällig darüber, wie
sehr ihr das Leben zuwider ist. Dass sie nicht leben
will oder ihr Leben keinen Spaß mache. Darüber, dass sie eh nichts kann und dass sie
sich oder wir sie einfach umbringen sollen. So lange, bis
sie jemand tröstet und einen Rückzieher macht."

"Autsch!" Dieses Mal fühlte Lilið rapide eine Wut in sich
aufsteigen, für die sie keine Worte kannte.

"Aber ihr macht Leben wirklich keinen Spaß.", warf Lajana
ein. "Sie sagt auch sonst, dass sie des Lebens so müde
ist, weil alles so anstrengend ist."

"Schon.", erwiderte Marusch. Sie seufzte. "Aber wenn sie
nicht leben mag, weil ich nicht Königin und schon gar nicht
König sein will, dann ist das ihr Problem und nicht meins."

Lilið fragte sich, ob Marusch deshalb nicht mit Lajana darüber
reden konnte, Suizidgedanken zu haben. Weil sie um keinen
Preis diesen Mechanismus bei ihr auslösen wollte, den
ihre Mutter bei ihnen auslöste. Vielleicht war für sie beide
das Thema immer mit Erpressungsgefühlen verbunden.

Sie versuchte, sehr allgemeine Worte zu wählen, die keinen
Bezug zu Maruschs Gedanken hätten, als sie
sagte: "Es sind zwei sehr verschiedene Dinge, nicht
mehr Leben zu wollen und diese Gedanken mit einer
Person zu teilen, die gerade Kraft dafür hat und
einverstanden damit ist, das mitzutragen, oder
unaufgefordert über die eigene Lebensmüdigkeit immer
dann zu reden, wenn andere Leute nicht das tun oder
nicht das sind, was man will."

Lajana nickte. "Ich glaube, das stimmt.", sagte sie.

Marusch nickte ebenfalls aber sagte nichts dazu. Sie
blickte mit fast entspanntem Gesichtsausdruck nachdenklich
in die Ferne.

"Habt ihr etwas über die Richtung herausfinden können, in
der das Igeldings von uns liegt?", wechselte Heelem das
Thema.

Marusch schüttelte den Kopf. "Vielleicht sind wir nah
dran. Ich habe einen Stein der hiesigen Sakrale geklaut. Damit
lässt sich vielleicht was machen."

"Du hast ein Ding dafür, Sakralen auseinanderzunehmen,
oder?", fragte Drude.

Marusch grinste.

Nach dem Essen, als die Sonne im Meer versank, brachen
sie auf. Die Teeseufel fühlte sich nach der Reparatur anders
an, aber nicht schlechter. Alles zerbrach und wurde neu
geschaffen. Zerstörung und Neuanfang.
