Einleben
========

*CN: Erotik, Küssen, BDSM, Knien, Sowas wie Blutegel,
Nacktheit, Körpergeruch, Genitalien, Untersuchung, questionable
consent, masturbieren, Vergiften - erwähnt, Leichen, Sanism,
Ableismus, häufige Benutzung des Wortes 'dumm', Schlafmangel.*

Sie träumte davon, dass sie Marusch küsste. Leidenschaftlich
und verzehrend. Dass ihre Körper dazu eng aneinander gepresst
waren, ohne das Arme dazu überhaupt notwendig gewesen wären. Sie
träumte, Marusch dabei sanft zu halten und ihr Gesicht zu
berühren. Aber sie fühlte im Traum die Haptik der Lippen nicht.
Interessanterweise war es kein großer Verlust, vielleicht sogar
im Gegenteil. Das Gefühl entsprach wieder mehr der Vorstellung von
Küssen, bevor sie je geküsst hatte. Und die Vorstellung war in
vielen Punkten besser gewesen als die Realität. Die Realität
mochte sie eigentlich trotzdem lieber, weil sie weniger flüchtig
war. War sie das wirklich nicht?

Sie wachte auf und wünschte sich sofort zurück in den Traum. Er
war intensiv gewesen. Das Sehnen und Wollen, das Verlangen und
gegenseitige Verschlingen waren genügend starke Gefühle gewesen,
um dieses dumpfe Ding in ihr drin, von dem sie nicht einmal wusste,
ob es ein Gefühl war, einen Moment nicht zu spüren. Ob
Marusch sie deshalb geküsst hatte? War Maruschs Grund, Lilið
dieses Gefühl und diese Leidenschaft entgegen zu bringen, dass
sie dadurch für eine gewisse Zeit etwas verdrängen konnte, was
schlimm in ihr lastete?

Lilið hätte es ihr nicht verübelt. Aber sie glaubte eigentlich
sogar, dass es nicht so war. Vielleicht am Anfang. Vielleicht
auch in dem Moment, als Marusch entflammt war. Und da hatte es
gar nicht so gut geklappt. Aber zum einen waren
manche Dinge für Marusch in der Beziehung wichtig, die völlig
außerhalb dieser Leidenschaft lagen. Zum Beispiel, dass Lilið
zugehört hatte und bestimmte Seiten von Marusch akzeptiert
hatte. Und zum anderen war Maruschs Fokus, wenn sie miteinander
körperlich wurden, immer so sehr darauf gerichtet gewesen, dass es Lilið
dabei gut ging. An so etwas hatte Lilið im Traum nicht gedacht. Sie
hatte sich einfach genommen.

Aber nun war der Traum vorbei, und wie das mit Träumen bei
ihr so war, würde er nicht auf Kommando wiederkommen, wenn sie
später irgendwann wieder Gelegenheit hätte, zu schlafen. Ein
alberner Teil von ihr fragte sich, ob sie einfach Drude fragen
sollte, ob dey mit ihr leidenschaftlich küssen wollte. Sie
merkte, dass sie verglichen mit vor ihrem Anheuern hier
verhältnismäßig wenig Hemmungen hatte, es tatsächlich zu tun,
aber eben doch genug, es zumindest fürs erste zu lassen.

Beim Frühstück stellte sie sich so unsinnige Fragen, wie, ob
es für sie leichter wäre, zu wissen, dass Marusch tot wäre, weil
sie dann eine unaufgelöste Sorge los wäre, oder ob die Hoffnung die bessere
Alternative war, die irgendwo in das Gefühlsgewusel in ihr verzwirbelt
war, das sie mühevoll zu verdrängen versuchte. Ob sie die Hoffnung alleine
fühlen könnte? Lilið hatte Sorge, dass, wenn sie dieses
Gefühl von Hoffnung aus dem Ballen auszufädeln versuchen würde,
dieser sich in ihr völlig entfalten würde. Und vielleicht
wäre es gut, wenn er es täte, aber wer würde sie dann auffangen? Was
würde dann passieren? Wäre sie dann zu der notwendigen Rolle, die
sie spielte, noch in der Lage?

Fehlte ihr Fokus, weil sie diese Hoffnung hatte? Hätte sie ohne
die Hoffnung eher versucht, hier wegzukommen, weil ihr die
Kronprinzessin in Wirklichkeit egaler war, als sie zugeben wollte,
und sie es doch ein Stückweit für Marusch tat? Sie wollte diese
Frage unbedingt verneinen. Sie glaubte, dass sie hinter dem
stand, was sie tat, aber eigentlich wusste sie auch nicht genau,
ob sie überhaupt noch irgendwo stehen konnte.

Badete sie in Selbstmitleid? Sie kicherte lautlos. Nein,
das tat sie nicht. Eher in einer negativen Gedankenspirale. Keine,
die sie in die Tiefe riss, sondern eher eine, die sie
auseinanderfächerte, sodass sie nichts mehr beurteilen
konnte. Mitleid hätte sie hingegen aus ihrer Sicht mit sich selber durchaus
haben gedurft. Viel mehr, als sie derzeit aufbringen könnte.

Nachdem sie nach dem Frühstück anhand von Sonnenstand und der
erwarteten Insel, die sie wie vorgesehen passierten, Daten gemessen und
notiert hatte und die Karte im Kartenraum nach ihnen neu
justiert hatte, fing sie an, sich selbst zu streicheln. Zunächst nur ein
paar Mal über die Arme, aber dann unterbrach sie sich. Ihr wäre es
vielleicht peinlich gewesen, wenn jemand zugesehen hätte. Deshalb
guckte sie gründlich in alle Ecken, ob sie Drude finden würde. Sie
vertraute nicht darauf, dass Drude nicht trotzdem irgendwo
gut versteckt wäre, vielleicht mit Magie, aber wenn dey
sich nicht finden lassen wollte, dann müsste dey wohl damit leben,
einen intimen Moment mitzuerleben. Lilið legte sich dazu auf
den Boden, die Tür im Blick und streichelte sich sehr zartfühlend
über das Haar, das Gesicht, den Hals. Und schließlich über den
ganzen Körper. Um an die Beine zu kommen, zog sie sie an.

Sie brach abrupt ab, als sich die Tür öffnete, und erwartete
Ärger vom Kapitän oder dem Matrosen zu bekommen, weil sie
einfach faul auf dem Boden herumläge, aber es war bloß Drude. Und
die Abe hopste sofort begeistert von einer Tasche, die
Drude bei sich trug, auf Liliðs Bauch, wo sie sich kraulen ließ.

"Ich glaube, ich bin ein bisschen größer als du, aber
vielleicht passen dir trotzdem ein paar Sachen von mir.", sagte
dey. "Deine sehen so aus, als hättest du in ihnen mindestens
drei Wochen am Stück abwechselnd im Salzwasser und in der
Sonne gebadet und gleichzeitig Schwerstarbeit mit ihnen verrichtet, wenn
ich das so sagen darf. Außer der Mantel, der für sowas gemacht
ist, natürlich."

Lilið lachte. Sie war überrascht über diesen eindeutigen
Laut der Freude aus ihrem Körper. "Das kann jedenfalls
hinkommen.", sagte sie. "Es waren gute drei Wochen. Ich
hatte nur einen Satz
Kleidung, den ich, immer wenn das Wetter ihn auch trocknen
konnte, im Meer gewaschen habe."

Drude stellte die Tasche auf dem Tisch ab und suchte ein
Hemd daraus hervor. "Steh auf, damit ich schauen kann, ob
es passt, oder ob ich weitersuchen muss."

Lilið atmete resigniert einmal tief ein und aus. Auf Aufstehen hatte
sie so gar keine Lust, aber wenn Drude schon so freundlich war,
Kleidung zu verleihen, dann sollte sie auch entgegenkommend sein.
Sie schob die Abe von ihrem Bauch und rappelte sich hoch. "Es
ist so lieb von demm, an mich zu denken.", teilte Lilið der
Abe mit, die darauf nicht reagierte. Lilið streifte noch
im Aufstehen den Mantel ab. Sie musste sich eingestehen, dass sie
ihn auch deshalb dauernd trug, weil er den müffelnden Geruch ihrer
Kleidung darunter daran hinderte, allzuweit von ihr wegzuwabern
und andere zu stören.

Drude verzog allerdings nicht einmal das Gesicht, als dey ihr
das Hemd vor den Oberkörper hielt. "Könnte passen.", sagte
sie. "Probier an!"

Lilið haderte mit sich. War das eine Aufforderung von demm, sich
hier vor demm im Kartenraum umzuziehen? Vielleicht sollte sie
vorsichtshalber fragen. "Meinst du, ich soll mich hier ausziehen?"

"Oh, macht es dir etwas aus?", fragte Drude.

Lilið dachte an ihre Gedanken vorhin. Sollte sie Drude doch fragen,
ob sie küssen wollten? Sollte Lilið ausprobieren, was mit demm passierte,
wenn sie versuchte, sich erotisch auszuziehen? Sie beeilte sich, den
Kopf als Antwort auf Drudes eigentliche Frage zu schütteln, bevor
sie gedanklich den Faden verlieren würde.

Sie fragte sich, ob es nicht irgendwie übergriffig wäre,
wenn sie sich erotisch auszöge, und zeitgleich fragte sie sich,
ob sie überhaupt wüsste, wie das ginge. Also zog sie sich
am Ende einfach nur aus, versuchte dabei selbstsicher rüberzukommen
und sich wenigstens selbst dabei schön zu finden.

Lilið bemerkte, dass Drude sie dabei durchaus ansah, aber vermutlich
in einer ähnlichen Weise, wie dey es getan hätte, hätte Lilið
einfach neue Kleidung getragen. Eindrücke aufnehmend, nicht
wertend. Soweit Lilið das ablesen konnte, was, wie immer, nicht
einfach bei Drude war.

"Dreh dich mal um!", forderte dey Lilið auf.

Lilið runzelte die Stirn, aber drehte Drude den Rücken zu. Was
sollte das?

"Du hast da zwei Schluppen in der linken Pobacke.", informierte
Drude. "Ich hole eine Schluppenzange, Desinfektionsmittel und
einen Eimer Wasser. In Ordnung?"

"Was?", fragte Lilið. Sie schüttelte den Kopf leicht, um
zu versuchen, das Gefühl der Absurdität abzuschütteln.

"Kennst du keine Schluppen?", fragte Drude.

"Doch, klar! Es würde an ein Wunder grenzen, wenn irgendwer
nicht über die Jahre hinweg nach dem Schwimmen im Meer mal
welche gehabt hätte.", erwiderte Lilið. "Also, klar, du
hast Recht. Ich komme da wahrscheinlich nicht gut selber
dran." Lilið widerstand dem Drang, nachzufühlen.

Aber als Drude den Raum wieder verlassen hatte, verrenkte sie
sich so, dass sie die zwei dunklen Punkte auf ihrem geröteten
Po sehen konnte. Ausgedacht hatte dey sich das also nicht. Dass
Lilið sich ausgerechnet beim Baden in den Überresten der Kriegskaterane
zwei Schluppen eingefangen hatte.

Sie krachte fast auf den Boden, genau auf die Pobacke, als
die Abe sie anflog und sich mit Schwung auf ihren
verrenkten Schultern niederließ. Sie hatte sich gerade wieder
aufgerappelt, als die Tür sich wieder öffnete. Das war zu früh,
dachte sie, und hatte Recht. Der Matrose schaute herein,
gab ein sehr verwundertes, peinlich berührtes Geräusch von
sich und schloss die Tür wieder. "Ich komme dann später
wieder!", rief er durch die Tür.

"Ich habe gerade frische Kleidung bekommen!", rief Lilið
zurück. "Es wird nicht wieder vorkommen."

Ein Brummeln drang zu ihr durch, dass ihr wenigstens verriet, dass
der Matrose es noch gehört hatte. Dann hörte Lilið ihn mit
Drude reden, und kurz darauf kam dey wieder herein. "Ott meinte,
irgendetwas wäre peinlich gewesen?", fragte dey.

"Ein wenig.", antwortete Lilið. "Außerdem ist mir wieder
eingefallen, warum ich so auf die Information über die Schluppen
reagiert habe. Ich habe realisiert, dass ich ganz schön fertig
sein muss, dass ich sie nicht eher bemerkt habe."

"Bist du.", antwortete Drude trocken. "Und ich kann dir die
Schluppen nur entfernen, wenn du mir den Rücken zudrehst."

Lilið folgte der indirekten Aufforderung und fühlte nur Momente
später eine angenehm kalte Hand in Hüftgegend, die sie leicht
fixierte, während die andere mit der Zange die zwei Lebewesen,
die es gewagt hatten, ihre Köpfe in Liliðs Körper zu bohren,
kurz und schmerzlos herauszupfte.

"Die Viecher sind häufig so gemein, dass sie sich auch
in Vulvalippen beißen. Wäre es dir eher unangenehm, wenn ich
nachsähe? Oder fändest du gut, wenn ich das kurz mache?", fragte
Drude. "Ich hätte kein Problem damit."

Lilið schluckte und atmete einmal langsam ein und aus, um
sich zu beruhigen. Sie hoffte, dass es sie nicht erregte, denn
inzwischen war sie recht sicher, dass Drude nichts im Sinn
hatte, wozu Erregung passen könnte. Aber Schluppen kamen selten
allein und Drudes Vorschlag war technisch sinnvoll. "Beides.", antwortete
Lilið also wahrheitsgemäß.

Auch diese Inspektion tat Drude wie angekündigt kurz und schmerzlos. Dey beugte
sich geschickt über Kopf, sodass dey von unten einen Blick in
Liliðs Genitalbereich hatte, zog mit den kühlen Fingern Liliðs
Vulvalippen einmal auseinander und blätterte sie sozusagen um. Es
ging sehr schnell. Das war gut. "Nichts.", versicherte Drude.

Als nächstes desinfizierte Drude Liliðs Po. Als dey auch damit
fertig war, fühlte Lilið sich irgendwie atemlos, und sie wusste
selbst nicht, ob es daran lag, dass sie bei Untersuchungen nie
gern im Genitalbereich angefasst worden war, oder ob sie Drude
als attraktiv empfand. "Findest du mich anziehend?", rutschte ihr
heraus, als sie sich wieder zu demm umwandte. "Entschuldige, das
war nicht in Ordnung zu fragen."

"Doch, war in Ordnung.", widersprach Drude. "Und nein, ich finde
dich nicht anziehend. Sexuell jedenfalls nicht, das meintest du
doch, oder?"

Lilið nickte. Und zuckte dann innerlich sehr zusammen, als Drude
einen Schritt auf sie zumachte und deren Zeigefinger gekrümmt unter
Liliðs Kinn legte. "Was?", brachte Lilið hervor.

Drude nahm den Finger wieder weg. "Würdest du dich vor mir
hinknien?", fragte dey.

"Was zum!" Lilið merkte, dass sie fast wütend klang, als sie das
sagte, aber vielleicht auch sehr verwirrt. Sie war auch tatsächlich verwirrt. Sie
fühlte, wie sie die Frage allein erregte, wie ihre Vulvalippen, die
gerade noch durchaus recht unauffällig eine Untersuchung
überstanden hatten, mit dieser simplen Frage anschwollen oder so etwas.

"Ich dachte, ich frage mal.", sagte Drude. Dey vergrößerte
ihren Abstand dankenswerter Weise wieder. "Ich kann Menschen nicht
gut lesen, aber du wirktest kurz auf mich, als ob du an dem Gedanken
durchaus Gefallen gefunden hättest."

Lilið konnte das nicht leugnen, so sehr sie wollte. "Was hättest
du davon?"

"Spaß.", antwortete Drude schlicht. "Habe ich eine Grenze übertreten?"

Mit dieser Frage klang die Erregung endlich ein kleines bisschen wieder
ab. Was sollte das? "Ich bin nicht sicher, aber ich glaube, wenn nicht,
warst du zumindest nah dran.", sagte Lilið. Ein Teil von ihr wollte, dass Drude
es wieder täte. Dieser Teil nahm ihren Vorstellungsapparat an die Hand und spielte
Lilið noch einmal die Sequenz vor, in der Drude dicht vor ihr gestanden
hatte und sie gefragt hatte, ob sie sich hinknien wolle. Sie wollte doch
nicht neue Kleidung direkt einschleimen!

"Es tut mir leid. Ich war wohl zu offensiv.", sagte Drude. "Ich bringe
Zange und Desinfektionsmittel weg. Das Wasser im Eimer ist frisch und
der Lappen darin auch. Wasch dich und zieh dich an. Du riechst nach
Erregung."

Lilið schluckte noch einmal schwer. Drude hatte ihren Geruch ansonsten
unkommentiert gelassen, fiel Lilið auf. Diesen hatte dey angemerkt. Demm
war also bewusst, dass dey da etwas ausgelöst hatte, aber benutzte es
nicht als Begründung dafür, Lilið zu bedrängen, sondern sie in Ruhe
zu lassen. Das gefiel Lilið schon, auch wenn ihr die Gesamtsituation
gerade sehr unangenehm war.

Drude verließ einfach den Raum, und nahm die Abe leider mit.

Lilið stand noch einige Momente da. Wie vom Donner gerührt? Lilið
hatte die Redewendung noch nie als so passend empfunden wie jetzt. Ihr
Körper reagierte also auf die Vorstellung von Unterordnung. Lilið
musste sich mit Gewalt davon abhalten, sich das Ganze noch einmal
auszumalen. Oder weiterzugehen. Sich tatsächlich vorzustellen, zu
knien. Sie atmete hastig ein, um sich abzulenken.

Warum funktionierte das jetzt? Warum nicht schon, als sie Drude
kennen gelernt hatte und dey sie auf den Boden gedrückt hatte? Aber
jetzt auf einmal war auch die Erinnerung daran erotisch. Gab es einen
Ort an Bord, wo sie unbemerkt masturbieren könnte, damit ihr
Vorstellungsapparat vielleicht in den Griff zu kriegen wäre?

Sie würde Navigieren. Das würde helfen. Sie kannte zwar genug Routen,
aber sie würde einfach noch ein paar Alternativen erkunden oder
sich mit sehr abstrakten Nautikaufgaben beschäftigen, die
unauffällig wären. Keine, die so wirken könnten, als würde sie
einen Weg nach Nederoge zurücksuchen.

Sie wusch sich also, zog sich an, navigierte, ging dann ihren
Pflichten wieder nach, und überprüfte, ob sie noch auf Kurs
waren. Leider waren sie es nicht mehr ganz, und zwar auf unpraktische
Weise: Sie waren zu schnell. Dadurch ergab sich die Möglichkeit,
auf eine Route zu wechseln, die sie einen Tag früher ans Ziel bringen
könnte. Und da Lilið noch in ihrer selbst gesetzten Probezeit war, musste
sie es verkünden und umsetzen. Vielleicht würde ihr, weil sie einen
ganzen Tag herausholte, nun schneller vertraut und sie könnte es
doch irgendwann ausnutzen. Immerhin hatten sie durch die Abdrift
in den ersten Tagen der Reise bis zum Angriff durch die Kriegskaterane
auch einige Tage verloren und die neue Route würde sie immer noch etwa
zwölf Tage kosten.

---

Beim Mittagessen fiel ihr auf, was sie an der Sache besonders
störte, die sie gerade mit Drude über sich herausgefunden hatte: Es
war noch etwas, was sie nun irgendwie verarbeiten musste, was
in ihr Raum einnahm und sich ungefragt immer wieder in ihre Gedanken
schob, was ihr Fokus und Konzentration für wichtigere Dinge raubte. Sie
hatte ohnehin einen einigermaßen eng getakteten Plan als Nautika, und
ihre Arbeit wurde weiterhin regelmäßig vom Matrosen Ott überwacht, wenn
er nicht gerade schlief, was er ja nicht zu den gleichen Zeiten tat
wie Lilið. Aber in den kurzen Zeitspannen, in denen es gerade nichts
zu tun gab, wollte sie mit Drude politische Gespräche führen und
die Prinzessin suchen.

Während Drude an sich eine hilfreiche Person war, ohne die Lilið
sich weniger wohl gefühlt hätte, zum Beispiel eben, weil frische
Kleidung auf der Haut nach all der Zeit ein Gefühl von im größten
Luxus leben mit sich brachte, stand Drude Lilið bei
dem Vorhaben sehr im Weg, die Prinzessin zu finden. Dey erwischte
Lilið direkt beim ersten Versuch, die Kagutte unauffällig zu
durchsuchen. Dey tauchte jedes Mal aus den Schatten auf und konfrontierte
Lilið damit, dass das bestimmt Liliðs Plan wäre.

"Nutz die Zeit lieber für Gespräche mit mir.", riet dey. "Wenn
du die Prinzessin aufsuchst, ist es für den Rest der Crew verdächtig, und
außerdem sehr kontraproduktiv, was deine
Möglichkeiten angehen, mich von irgendwas zu überzeugen."

Leider musste Lilið demm recht geben. Dere Argumentationsweisen
folgten so oft einer unbarmherzig stechenden Logik. Lilið würde nicht mehr viel
Zeit haben, Drude von etwas zu überzeugen. Ungefragt drängte sich
ihr der Gedanke auf, dass sie versuchen könnte, Drude zu beseitigen. Mit
Gift zum Beispiel. Sie aßen oft genug zusammen. In einem Raum, in
dem keine politischen Gespräche möglich waren, weil zu viele Ohren
zuhören konnten, was ihre Zeit für politische Gespräche noch weiter dezimierte.

Lilið hatte keine Ambitionen, Drude zu töten, aber in ihren ersten
drei Stunden Schlaf der folgenden Nacht träumte sie intensiv davon,
wie sie das Gift, das sie noch von Allil bei sich trug, in Drudes
Speisen mischte, und sich dann überfordert sah, eine Leiche zu
entsorgen. Sie wurde geweckt, als sie gerade dabei war, Drudes
Leiche zwischen den Toten von der Kriegskaterane zu verstecken,
die in ihrem Traum immer noch hinter der Kagutte herschwammen, denn
wo eine Leiche besser verstecken, als unter anderen Leichen?

---

Lilið war nicht überrascht nach ihrem Gang an Deck, um Messdaten zu
sammeln, Drude im Kartenraum vorzufinden. Dieses Mal trat dey direkt
aus den Schatten, gab sich keine Mühe, unbemerkt zu bleiben. Dey
setzte sich Lilið gegenüber an den Kartentisch und sah ihr dabei zu,
wie sie die Karte auf ihren neuen Stand brachte. Das war
unheimlich, fand Lilið. Drudes Blick war dabei wach und aufmerksam, als
würde dey alles verstehen, und auf die Frage, ob es so wäre, antwortete
dey einfach nicht. Drude war eine sehr aufgeweckte Person und
Lilið traute demm zu, eine Menge Fähigkeiten erfolgreich zu
verbergen. Das würde irgendwann vielleicht ein Problem bedeuten, wenn Lilið
versuchen würde, die Crew zu betrügen.

Aber warum hätte Drude dann das Schiff nicht navigiert, als Lilið
noch nicht als das Nautika eingesetzt worden war?

Lilið legte das Kartensteinchen an seine aktuell richtige Position
und blickte auf. "Fertig."

"Hältst du mich für dumm?", fragte Drude.

"Was?" Lilið fühlte schon wieder Fassungslosigkeit. Warum sollte
sie das von Drude denken? "Ich habe gerade eher das Gegenteil über
dich gedacht."

"Viele denken von mir, dass ich dumm wäre.", informierte Drude. "Ich
weiß, dass ich es nicht bin. Aber egal, wo ich hingehe, mir werden
zum Beispiel Dinge zunächst möglichst einfach erklärt. In so einer
miesen Art, als wäre ich ein kleines Kind. Wobei ich finde, dass man
auch mit kleinen Kindern nicht so reden sollte."

Lilið runzelte die Stirn, sagte aber erst einmal nichts. Sie
konnte es sich eigentlich nicht vorstellen. Aber sie würde es
demm auch niemals absprechen. "Warum erzählst du mir davon?"

Drude gab ein tonloses Lachen von sich, das Lilið schon
kannte. "Ich habe den Eindruck, du bist nicht dumm, aber schon
eine Ecke weniger schlau als ich.", sagte dey. "Es wird
hoffentlich irgendwann in diesem Gespräch noch um die Kronprinzessin
gehen, die, so schätze ich es ein, tatsächlich dumm ist. Und
das meine ich nicht abwertend, aber es hat halt trotzdem
Konsequenzen." Drude zog die Augenbrauen zusammen. "Ich
habe Angst vor dem Gespräch, weil es Tabuthemen sind. Menschen
sind kaum in der Lage, nicht zu werten, gehen also im Gegenzug
davon aus, dass ich werten würde, sobald ich bestimmte
Wörter benutze, aber sie sträuben sich auch gegen jede
Alternative, sobald sie ausdrückt, was ich meine. Es
ist schwer, darüber zu reden, ohne dass jemand an die
Decke geht. Darf ich ein bisschen ausholen?"

Lilið konnte nicht leugnen, dass ein Teil von ihr jetzt
schon ein starkes Bedürfnis danach hatte, an die Decke zu
gehen. Ein Teil, den sie zumindest derzeit noch gut im Griff
hatte. Sie nickte.

"Menschen widersprechen sich dauernd oder denken nicht
logisch zu Ende. Sie sagen, niemand ist dumm.", leitete
Drude ein. "Zum Beispiel sagen sie, manche bräuchten einfach
nur mehr Zeit, um etwas zu lernen, aber irgendwann könnten
sie es doch lernen. Glaubst du daran?"

Lilið zögerte. Sie kannte diese Behauptungen durchaus. Und
sie hatte sie instinktiv für nicht richtig, aber auch nicht
schlimm falsch gehalten. "Ich habe mir nicht ausreichend
Gedanken gemacht, um eine abgeschlosse Meinung dazu
zu haben.", antwortete sie.

"Ich finde gut, dass du das so sagst. Und dass du es kannst,
zu einem Thema, das für dich emotional belastet ist, keine
fertige Meinung zu haben, mit der du gegen miese, abwertende
Leute antreten kannst.", sagte Drude. "Die Sache ist die,
dass Menschen eine bestimmte Lebensspanne mit einer bestimmten
Varianz haben. Sagen wir 80 Jahre, plus minus 20. Vielleicht
ist die Varianz etwas kleiner, aber ich kann nicht gut
rechnen, also übertreibe ich. Wenn es stimmen
sollte, dass manche Menschen bloß langsamer lernen
würden, dann müsste dieser Umfang von 40 Jahren, -- also zwei Mal
die 20 --, ausreichen, diese
Geschwindigkeiten auszugleichen, damit wir am Ende sagen könnten, dass
alle Menschen durchschnittlich das Gleiche in ihrem Leben
lernen könnten. Ich glaube nicht, dass die Varianz ausreicht, die
Unterschiede der Lernfähigkeiten von Menschen aufzufangen,
sodass es weiterhin signifikante Unterschiede dazwischen gibt, was Menschen
in ihrem Leben lernen können. Und das ist nur das oberflächlichste Problem
an der Idee, abzustreiten, dass es dumme Menschen gibt. Kannst
du mir folgen?"

Lilið nickte. "Schon.", sagte sie. "Ich brauche auch nicht
mehr Erklärungen dafür, dass es eine Hierarchie gibt. Ich
sehe zum Beispiel Skorem als eine Unterkategorie von
Intelligenz an, die sich konkret auf Magie bezieht. Und
da werden wir ständig einsortiert. Das
ergibt eine sehr klare Hierarchie."

"Genau!" Drude bestätigte es so schnell, dass dey Liliðs
Gedankengang damit abriss. "Es gibt auch immer wieder Leute, die sagen,
eigentlich wären doch alle skorsch und alle können jede Magie
lernen. Nur manche eben langsamer. Alles dasselbe wie mit Intelligenz. Und
recht haben die Leute damit, dass manchen Menschen zu unrecht
einfach abgesprochen wird, skorsch oder
intelligent zu sein. Frauen zum Beispiel, oder
Leuten, die für welche gehalten werden, oder Leuten, die irgendwie
sonst nicht in ihr Bild von skorsch fallen. Aber das heißt halt
nicht, dass das bei allen Menschen zutrifft, dass da ein Vorurteil
im Spiel ist. Manche Menschen sind einfach nicht skorsch."

Lilið nickte abermals. "Die Frage, die ich mir stelle, ist, warum
wollen wir das überhaupt einordnen und vergleichen?"

"Na ja, mit einem Grund dafür habe ich das Gespräch eröffnet." Drude
lächelte beinahe für einen Moment, aber irgendwie hoben sich
die Mundwinkel dabei nicht. "Auf Basis dessen, dass Leute mich
für dumm halten, behandeln sie mich irgendwie. Und zwar unpassend,
nicht hilfreich für mich. Ich behaupte nicht, dass ihre Art und
Weise bei Leuten automatisch richtig wäre, die tatsächlich dumm sind, aber
es wäre wahrscheinlich eine gute Sache, wenn wir lernen würden,
wie wir uns beim Kommunizieren mit Leuten darauf einstellen, ob
unser Gegenüber mitkommt oder nicht, und wie wir auf es eingehen
können."

Das klang im ersten Moment einleuchtend. Aber Lilið störte
noch etwas daran. Drude ließ ihr die Zeit,
nachzudenken. "Meinst du, es hilft uns, vorgefertigte Muster
zu erlernen, die sich auf diese Skala beschränken? Ist
es nicht sehr unterschiedlich, auf welche Weisen Menschen
kognitiv Schwierigkeiten mit etwas haben?", fragte Lilið. "Ist
das nicht auch etwas, was bei dir ein Problem sein könnte? Leute
schauen dich an und sehen irgendetwas, was sie auf die Idee kommen
lässt, dass du dumm wärest, obwohl es nur irgendeine Eigenschaft
von dir ist, die sie auf diese Skala runterbrechen, und dann wenden
sie ein Muster auf dich an? Ich weiß, dass Muster schon irgendwie
notwendig dafür sind, um uns überhaupt zu orientieren, aber
die Dumm-Schlau-Skala kommt mir viel zu einfach heruntergebrochen
vor, als dass nicht tausende Menschen darunter leiden würden." Sie
bemerkte, wie mies es sich angefühlt hatte, das Wort 'dumm'
auszusprechen. Es haftete so viel Stigma daran. War es möglich,
das loszuwerden?

"Hm.", machte Drude und schwieg dann erst einmal.

Das tat dey so ausgiebig und lange, bis Ott verschlafen an die Tür klopfte,
kurz darauf eintrat und Lilið fragte, ob sie auf ihre zweiten drei Stunden Schlaf
verzichten wollte.

"Wir reden danach weiter.", bestimmte Drude. Dey verließ einfach den
Raum.

Lilið würde also nicht auf den wertvollen Schlaf verzichten. Oder
zumindest versuchen, das nicht zu tun. Es war ein merkwürdiges Gefühl von
mittendrin Abbrechen und Lilið fragte sich, ob sie überhaupt
schlafen können würde. Und falls ja, welchen Unfug sie dieses Mal
träumen würde.
