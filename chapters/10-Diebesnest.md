Diebesnest
==========

*CN: Sex, Nacktheit, Menstruation, andere Körperflüssigkeiten, Erektion,
Genitalien, sexuelle Übergriffigkeit als Thema.*

Lilið trat einige Schritte näher, bis sie dicht vor Marusch stand, ohne
sie zu berühren. Sofort nahm sie ihren Geruch stärker war. Sie überlegte,
Marusch wieder anzufassen, aber stattdessen blieb sie einfach stehen
und atmete langsam.

"Was möchtest du?", fragte Marusch.

Lilið wusste es doch auch nicht. Zumindest hatte sie keinen Plan darüber, wie
es weitergehen würde, nach einem Anfang, von dem sie sehr wohl eine Idee
hatte. "Ich würde dich gern küssen.", sagte sie. Noch während sie es aussprach,
zogen sich ihre Eingeweide zusammen und irgendein fisseliges Gefühl rann
über ihre Flanken in ihren Genitalbereich. "Ich habe noch nie geküsst.", fügte
sie hinzu.

"Die Vorstellung von Küssen kann besser sein, als das Küssen selbst dann
ist.", warnte Marusch. "Mindestens eine Person, die ich geküsst habe, war
hinterher sehr enttäuscht. Und es lag nicht an mangelnden heißen Gefühlen
und auch nicht daran, dass ich grundsätzlich nicht küssen kann. Eine andere
Person hat es sehr gemocht." Marusch stockte. "Rede ich zu viel?"

"Wieviele Menschen hast du schon geküsst?", fragte Lilið, statt die
Frage zu beantworten. Das holte sie dann aber rasch nach: "Nein, du
redest nicht zu viel."

"So zehn, schätze ich.", antwortete Marusch.

"Warum?" Lilið hoffte, dass die Frage so sachlich klang, wie sie sie
meinte.

"Es haben mich Menschen attraktiv gefunden und gewollt, und ich habe nicht
'nein' gesagt.", anwortete Marusch.

Lilið blieb nicht verborgen, dass Marusch nicht unbedingt glücklich
wirkte. "Hast du gewollt?", fragte sie vorsichtig.

"Meistens.", antwortete Marusch.

Lilið bohrte nicht weiter nach. "Willst du jetzt?" Diese Frage kam
ihr nach Maruschs Eröffnung umso wichtiger vor.

"Ja." Maruschs Stimme war kaum ein Flüstern, und doch klang es
überzeugt.

Lilið hob eine Hand und strich ihr mit dem Daumen über die Wange. Doch
bevor sie entschloss, sich zu ihr vorzubeugen, bemerkte sie, dass die
Frage, die sie eigentlich hatte runterschlucken wollen, doch bohrte: "Wäre
ich also eine Person von vielen?"

Marusch senkte den Blick. "Wäre das schlimm?"

Das war die Frage, die Lilið am liebsten mit 'nein' beantwortet
hätte. Sie wusste nicht, ob es im Allgemeinen für sie stimmen würde. Vielleicht
käme es für sie nicht nie drauf an. Aber als sie sich an Maruschs Wertschätzung
erinnerte, schüttelte sie den Kopf. "Nein.", sagte sie. "Ich komme
mir wichtig für dich vor."

"Das bist du.", sagte Marusch ohne Umschweife.

Lilið fühlte sich etwas umständlich, als sie sich ihr noch weiter näherte
und sie küsste. Es war ein seltsames Gefühl, fand sie. Marusch hatte mit
der Warnung vielleicht nicht ganz unrecht gehabt. Das Gefühl der
Lippen aufeinander gab ihr nicht so viel, wie sie sich ausgemalt
hatte, aber das Kribbeln, dass dabei
über ihre ganze Haut lief, und Maruschs rascherer Atem waren es allemal
wert. Sie wollte Maruschs Kopf halten, ihr von unten durch die Haare
fahren und hielt sich nicht davon ab.

Maruschs Arme lagen unterhalb ihrer, also kamen sie eher an Liliðs Hüfte und sie
fasste sie dort an. Ihre Hände glitten unter den Anzug, über das Hemd darunter in ihren
Lendenwirbelsäulenbereich und umarmten sie, zogen Lilið an ihren Körper.

Wieder fühlte Lilið, als würde ihr Blut heißer durch ihren
Körper rauschen. Sie spürte, wie ihr Atem zitterte. Sie küsste den Mund noch einmal, dann die
Wangen, die sich sehr weich und zart anfühlten, und schließlich wieder den
Mund.

Marusch hielt sie noch etwas fester und übernahm beim Küssen die Führung,
sog Liliðs Lippe vorsichtig in ihren Mund, ließ sie wieder los, und
schob ihr zaghaft die Zunge zwischen die Lippen.

Als Marusch sich kurz wieder löste und einen neuen Kuss
ansetzte, überlegte Lilið, es umgekehrt auch zu tun, auch wenn sie noch nicht
völlig überzeugt vom Konzept der Zunge in ihrem Mund war. Aber die Erektion die sie durch alle
Lagen Kleidung hindurch gegen sich drücken spürte, lenkte sie
ab. Sie atmete rascher, viel rascher, und löste den Kuss schließlich
auf.

"Mochtest du es?", fragte Marusch.

Nach dem ersten Gefühl, dass noch seltsam gewesen war, hatte Lilið sich
tatsächlich daran gewöhnt. Es war nur sehr anders, als sie es sich
vorgestellt hätte. "Ja. Wir haben uns auf ein paar Weisen geküsst, und
ich bin noch nicht sicher, ob ich sie alle mag, aber", sie küsste
Marusch ein weiteres Mal, weich und ohne Zunge, fühlte das rasche
Einatmen des Körpers, den sie mit kontrollierte, und löste sich
wieder, "das schon."

"Möchtest du weiter küssen?", fragte Marusch.

"Ich möchte mit dir über Verhütung reden.", legte Lilið den
Grund ihrer Unterbrechung dar.

Marusch atmete heftig ein und dann langsam wieder aus. Ihre
Arme in Liliðs Rücken wurden etwas weicher, vielleicht zittrig. "Ich
habe noch zwei Kondome dabei.", sagte sie. "Ich bin auf der Feier
eins losgeworden. Es gab ein Paar mit einem Bedürfnis, und ich sah
wohl vertrauenserweckend genug aus, nach so etwas gefragt zu
werden."

Lilið konnte nicht anders und lächelte breit. "Diese Darlegung
gibt mir mehr Sicherheit, als ich erwartet hätte, und ich hatte
eine nicht zu unterschätzende Erwartungshaltung."

"Wenn du mit mir diese Art von Sex möchtest, fände ich es gut, wenn
wir uns zumindest die Hände gründlich waschen.", fügte Marusch
hinzu. "Vielleicht auch duschen. Aber ich bezweifle, dass es hier
eine gibt."

"Du hast recht. Mit beidem." Lilið atmete tief durch, bevor sie
es wagte, die Arme von Marusch zu lösen. Es fiel ihr
sehr schwer. "Lass uns schauen, was das Bad hier hergibt."

Marusch löste ebenfalls ihre Arme von Lilið und wendete sich in
Richtung der Tür. Sie schlichen beide, als wären sie einbrechend. Was
sie auch waren, fiel Lilið ein.

"Dass du noch nie geküsst hast, heißt nicht, dass du noch nie
diese Art von Sex hattest.", sagte Marusch, als sie das Bad gefunden
hatte. "Ist die Frage zu persönlich, ob du hattest?"

"Ich hatte noch nicht.", antwortete Lilið. "Ich habe viel darüber
gelesen und bin gut aufgeklärt, aber ich hatte noch keine Gelegenheit. Ich
war nicht gerade beliebt unter Gleichaltrigen, die dafür für mich
in Frage gekommen wären."

"Möchtest du dann wirklich mit mir?", fragte Marusch. "Ich erwarte
das nicht."

"Aber du würdest wollen?", fragte Lilið. Ein flüchtiger Gedanke
daran, wie sich ein Penis zwischen Vulvalippen anfühlen könnte, rann
durch ihre Vorstellungen und ließ sie abermals innerlich auflodern. Aber
vielleicht wäre auch das, wie küssen, sehr anders, als sie es
sich vorstellte.

"Wenn du es möchtest, ja.", antwortete Marusch. "Mir fehlt nichts, wenn
nicht."

Im Badezimmer gab es nicht einmal fließendes Wasser. Es gab einen Kanister,
der vermutlich regelmäßig mit Wasser aus einer Pumpe außerhalb des Hauses
befüllt wurde. Er hatte immerhin einen Hahn und es gab einen Abfluss darunter. Marusch
hatte Seife im Gepäck, die sie jetzt für sie beide zur Verfügung stellte. Sie
wuschen sich die Hände gründlich damit. Und, um anschließend ihre
Körper zu waschen, so gut das ginge, holten sie die Sache mit dem
Ausziehen nach.

Lilið zog Marusch zuerst aus. Sie mochte es, es langsam zu tun, immer
wieder die Hände nicht davon abzuhalten, die Haut zu berühren, Maruschs
schöne Konturen zu streicheln. Sie fühlte abermals diese leichte Gänsehaut,
aber glaubte nun, dass es vielleicht kleine Haarstoppeln waren, weil Marusch
sich den Körper rasierte. Es fühlte sich schön an. Wobei sie ihre
Haare auf ihren eigenen Beinen auch gern fühlte.

Maruschs Körper durchliefen mehrere Schauer, als Lilið ihr Kleid über
den Kopf zog. Darunter trug Marusch nichts als eine Spitzenunterhose,
gegen die von innen ihre Erektion so sehr drückte, dass sie
sich selbst in der Dunkelheit unübersehbar abzeichnete. Liliðs Hand fuhr knapp oberhalb
der Knie zwischen Maruschs Beine, die sie leicht nach hinten stolpernd
bereitwillig etwas mehr öffnete. Sie gab einen zittrigen, hellen Laut
von sich, als Lilið die Hand an einer der Oberschenkelinnenseiten entlang
nach oben bewegte, bis sie die seidige Unterhose berührte, und das
steife Gewebe durch den Stoff in die Hand nahm. Marusch lehnte sich sachte
gegen ihre Hand.

Etwas schepperte. Lilið ließ sofort los und sah sich um. Maruschs Atem
bebte noch, als sie sich bückte und ein Kehrblech wieder in einen
Eimer steckte, den sie beim Stolpern wohl erwischt hatte.

Lilið ließ ihr kaum eine Verschnaufpause. Als sie sich wieder umwandte,
fädelte Lilið kurzerhand ihre Finger links und rechts in Maruschs Unterhose und
zog sie herunter. Sie gab darauf Acht, beim Bücken nicht auch etwas
herunterzuwerfen. Marusch stieg vorsichtig aus der Hose aus, während
Lilið unter ihr hockte und den eigenen Schweiß in den Kniekehlen
spürte.

Dann war Marusch dran. Lilið mochte ihre vorsichtigen Hände, als Marusch
ihr die Anzugjacke abstreifte, und dann all ihre Knöpfe öffnete. Trotzdem
waren die Knöpfe nur im ersten Augenblick interessant. Die Länge
der fortwährenden Prozedur, die, weil die Knöpfe noch nicht oft benutzt
worden waren, auch nicht so einwandfrei lief und für ein paar Knöpfe
mehrere Versuche brauchte, wurde Lilið tatsächlich zum Ende hin
langweilig oder beschwerlich. Marusch merkte es wohl und kicherte.

Aber als auch Hose und Hemd sorgfältig auf den Kleiderstapel
abgelegt worden waren, drehte Marusch sie kurzerhand
schwungvoll um, um an die Brusthalterverschlüsse in ihrem
Rücken zu kommen. Es erinnerte Lilið an das Ende des Vortanzens
und sofort fühlte sie wieder die Leidenschaft. Besonders
als die Verschlüsse gelöst waren, Marusch die Enden des Brusthalters
losließ und stattdessen sie von hinten in eine Umarmung zog, die
Lippen wieder an ihrem Hals, den Lilið bereitwillig zur Seite legte.

Sie fühlte vorsichtige Zähne in der Haut, die ihr ein Fiepen entlockten,
weil das Gefühl überraschend und unbeschreiblich stark war, was
sie dort auslösten. Sie fixierten sie dort, nicht mit festem
Biss sondern eher als Anweisung, als Marusch die
Umarmung wieder löste, um ihren Brusthalter abzustreifen, und ihre Hände sich
anschließend von unten an ihre Brüste legten. "Magst du das?", fragte
sie.

Lilið nickte kurz, sprechen konnte sie kaum. Nun trug sie auch nur
noch eine Unterhose. Marusch kopierte vielleicht Liliðs Vorgehen
von vorhin. Auch sie ließ Lilið kaum Zeit sich wieder zu fangen, sondern
wanderte mit den Händen an ihren Hüften hinab bis zur Unterhose, und zog
sie ihr langsam vom Po.

Lilið bemerkte, wie sie im Schritt an Körperflüssigkeiten haftete
und einige Fäden mit sich zog. Für einen Augenblick fühlte sie sich
nicht auf eine angenehme Weise nackt. Aber es hielt nicht lange an.

Marusch befeuchtete einen Handtuchzipfel aus dem Gepäck, dass sie dabei
hatte. "Möchtest du, dass ich dich wasche?", fragte sie.

Lilið nickte vorsichtig. Ihr war unangenehm, dass sie sich überhaupt
waschen mussten, aber sie sah die Notwendigkeit. Und sich selber
waschen wäre noch unangenehmer gewesen, als gewaschen zu werden.

Marusch rieb ihren Körper ab. Es fühlte sich interessanterweise
nicht sehr erotisch an, eher sachlich. Sogar als sie mit dem
Handtuch erst die Innenseite ihrer Beine entlangfuhr und schließlich
ihr Genital wusch. Es erregte sie kaum.

Marusch ließ sie los, um einen neuen Teil des Handtuchs nass
zu machen, und stutzte. "Du menstruierst.", sagte sie.

"Oh, Mist.", brachte Lilið hervor. Auch das noch. Es brauchte
einen Moment, bis ihr klar wurde, dass das nicht nur Auswirkungen
auf jetzt hatte. "Ich habe nicht einmal Binden dabei. Die waren
im Gepäck, das ich bei meinem übereilten Verlassen der Reisefragette
an Bord gelassen habe."

"Ich kann mir gut vorstellen, dass dieser Haushalt so etwas
hat. Ich glaube, ich habe welche gesehen, als ich die Bettlaken
gesucht habe.", beruhigte Marusch. "Wenn du möchtest, ruh dich aus und
ich suche danach."

"Ich habe Angst, das Bett voll zu bluten.", gestand Lilið.

Marusch bückte sich erneut nach ihrem Gepäck und holte eine
Decke hervor, die fast Liliðs Körpergröße hatte. "Das ist eine
Decke, die ich unterlege, wenn ich nur auf feuchtem Boden schlafen
kann. Sie lässt keine Flüssigkeit durch. Taugt die?"

Lilið nickte und nahm sie dankend an.

"Möchtest du dich damit ins Bett legen, ich wasche mich, suche
Binden und komme nach?", fragte Marusch. Sie musste bemerkt haben,
dass Lilið diese Mitteilung sehr mitgenommen hatte.

Lilið zögerte, nickte dann aber noch einmal. "Ich habe nicht damit
gerechnet, dass das jetzt passiert.", fügte sie noch hinzu. "Ich
menstruiere meistens nur ungefähr alle zwei Monate."

---

Die Wartezeit war ihr unangenehm. Sie lag eigentlich nicht gern
nackt in Betten, vor allem nicht, wenn sie menstruierte. Sie versuchte,
das unangenehme Gefühl, dass diesen schönen Moment zerstören
wollte, irgendwie niederzuringen. Es war so viel trotzdem schön. Marusch
war so eine liebenswerte Person. Vielleicht gehörte sogar zu Maruschs
Liebenswürdigkeit, dass sie potenziellen Mörderinnen half. Das
war ein seltsamer Gedanke.

"Ich habe welche gefunden.", sagte Marusch, als sie wieder zur Tür
hereinkam. Nackt, und derzeit ohne Erektion, stellte Lilið fest. "Dann
wird das hier wohl nicht ohne Diebstahl ablaufen, aber bei so etwas
sind Konsequenzen oft auch eher mild, sollten wir erwischt werden."

Lilið nickte einfach und hob die Decke an, damit sich Marusch
zu ihr kuscheln konnte. Marusch legte, was sie bei sich trug,
neben dem Bett ab und schob sich zu Lilið unter die Decke.

Lilið hätte kaum damit gerechnet, aber ihre Erregung und das
romantische Kribbeln kehrten sofort zu ihr zurück, als
sich ihre Körper berührten, noch gepaart mit dem
verblassenden Gefühl, dass ihr alles unangenehm war. Sie vergrub
ihre Hand in Maruschs inzwischen wirren Haaren und zog sie in
einen weiteren Kuss. Sie unterbrach ihn sofort wieder, als ihr
einfiel, zu fragen: "Möchtest du überhaupt noch?"

Als Antwort legte Marusch ihren Arm über Liliðs Rücken und schmiegte
ihre nackten Körper aneinander. "Ja." Und erwiderte den Kuss.

Lilið spürte abermals die Erektion gegen ihr Bein drücken. Und nun
war nichts mehr zwischen ihnen. Zu wenig, als dass es sich
angenehm und sicher angefühlt hätte. "Kondom?", flüsterte
sie. "Wenn dir das mit Blut nicht zu eklig ist."

Marusch schüttelte den Kopf. "Ich ekele mich ziemlich selten.", sagte
sie. Sie beugte sich vom Bett hinab, fand das bereitgelegte
Kondom und zog es sich an.

Lilið beobachtete genau, wie sie es tat. Sie fand den Vorgang nicht
besonders ansprechend. Und als sie sich wieder in den Armen lagen,
zeigte die kurze Pause ihre Nachwirkungen. Lilið fühlte sich
wieder viel zu sehr im falschen Denkuniversum für Sex. Und die Angst, dass
es vielleicht doch weh tun könnte, tat ihr übriges. 

Marusch küsste ihr den Hals entlang, was sie elektrisierte,
strich mit der Hand ihre Hüfte entlang, über den Bauch und zwischen
ihre Beine. Lilið atmete rascher.

Sie war aufgeregt. Abwechselnd durchströmten sie Gefühle von Unsicherheit
und davon, genau das zu wollen. Maruschs Finger fuhren ihre Vulvalippen
entlang, was sehr schön war, aber eigentlich mochte sie die Küsse und zarten
Bisse im Schulter- und Halsbereich viel lieber.

Sie bemerkte, wie sie selbst passiver wurde und wunderte sich deshalb nicht,
als Marusch sie noch einmal fragte: "Willst du das wirklich?"

Sie antwortete nicht. Aber sie wollte. Also ergiff sie selbst Initiative
und fand ihrerseits mit den Händen einen Weg von Maruschs Rücken zu
ihrem Penis. Marusch fiepste auf und küsste Lilið als Reaktion zart auf
die Stirn.

Lilið hatte gar nicht daran gedacht, dass ihren Penis zu berühren, ja
auch ein krasses Gefühl in Marusch auslösen musste. Die Bewegung war
eigentlich dazu gedacht, den Penis einzuführen, aber Maruschs Reaktion
war wunderschön und ließ Lilið rascher atmen.

Trotzdem drehte sich Lilið auf den Rücken, unter Marusch, und versuchte,
ihren Penis an die richtige Stelle zwischen ihren Beinen zu sortieren. Was
sich als viel schwieriger herausstellte, als sie vermutet hätte. Er
rutschte ab, verirrte sich mehrere Male eher zu ihrem Anus, und wenn sie
den Winkel anders sortierte, dann lag er eher nach oben gekippt auf ihr,
stimulierte dabei aber ihre Klitoris. Was sich ebenfalls überraschend
wenig überzeugend anfühlte.

Wollte sie wirklich? Die Antwort, die Lilið sich gab, war immer noch
'ja'. Sie wollte wissen, wie es war. In ihrer Vorstellung musste
es auch schön sein, wenn sie sich nicht mehr um das Verheddern
vorher kümmern musste.

Marusch führte Liliðs Beine in eine angewinkelte Position, was ihr
Becken etwas kippte, und sortierte dann selbst ihren Penis, sodass
er nicht wegflutschte.

Lilið atmete zügig ein. Das fühlte sich tatsächlich schön an, fand
sie. Dieser Moment, bevor es passierte. Sie atmete zittrig wieder
aus und gab einen flatterigen, aber bestätigenden Laut
von sich. Sie wusste nicht genau, wie sie sich bewegen müsste,
damit der Penis eindrang, aber Marusch interpretierte das Geräusch
richtig. Sie verharrte in der Bewegung aber wieder sofort, Liliðs
Gesicht beobachtend, dass vielleicht ein wenig Schmerz
wiederspiegelte.

Es war kein starker Schmerz. Sie fühlte vielleicht die Eichel in
sich, aber es fühlte sich auch alles dumpf an. Und irgendwie
unangenehm. Das musste sie sich eingestehen, auch wenn sie
eigentlich nicht wollte. Vielleicht kam es einfach dadurch, dass
sie verkrampft war oder dass sie Angst hatte. Aber sie konnte nichts
dagegen tun. Sie spürte, wie ihr Tränen in die Augen traten.

Marusch löste die Position auf, drehte sie beide wieder in Seitenlage
und nahm sie in den Arm. "Was brauchst du?", fragte Marusch.

Die Frage war zu viel für Lilið. Sie fühlte, wie sich ihre
Lunge verkrampfte und eine Träne ihr über die Nase lief, die sie
nicht davon abhalten konnte. Ihre Nasenschleimhäute schwollen
an, sodass sie nicht einmal sprechen können würde, ohne dass das Weinen
raushörbar wäre. "Ich schäme mich so.", flüsterte sie.

"Du brauchst dich nicht zu schämen, es gibt keinen Grund dazu.", versicherte
Marusch. "Warum hast du das Gefühl? Hilft es dir, wenn wir es zerreden? Oder
wärest du lieber allein? Oder brauchst du etwas anderes?"

"Ich habe dich, seit wir uns wiederbegegnet sind, verführt." Lilið
hatte vor dem letzten Wort gezögert, weil sie sich nicht ganz
sicher gewesen war, ob es traf, was sie meinte. Aber es passte. "Und
nun lasse ich dich mitten drin mit einer Erektion hängen." Einer
Erektion, die immer noch gegen ihren Körper drückte.

"Du schuldest mir nichts.", versicherte Marusch. "Klar habe ich
irgendwo ein Bedürfnis nach Befriedigung, aber nur weil ich
ein Bedürfnis habe, stehst du nicht in irgendeiner Verpflichtung,
es zu decken. Vor allem bei so etwas nicht."

"Aber ich habe es ausgelöst.", wandte Lilið ein.

"Du hast dir dabei nicht gedacht, dass du mich in eine mich nach
dir verzehrende Lange bringen und darin gefangen halten möchtest.", sagte
Marusch. "Und selbst das wäre ein Spiel, dem ich nicht grundsätzlich
abgeneigt wäre. Aber das war nicht deine Absicht. Du hast
da aufgehört, wo du dich ungeplant nicht mehr wohl gefühlt hast, und
das ist absolut richtig so."

"Möchtest du dich selbst befriedigen?", fragte Lilið.

"Wie würde es sich für dich anfühlen?", fragte Marusch.

Lilið schluckte. Wieso sollte Marusch in dieser Hinsicht auf
sie Rücksicht nehmen? Nach so etwas? Aber sie dachte trotzdem ehrlich über
die Frage nach und fühlte sofort, dass es für sie eher
ein Ertragen wäre, das sich auch unangenehm in sie einbrennen
könnte. Einen Moment später konnte sie beschreiben, was
sie fühlte: "Mein Gefühl von Erregung ist unangenehmerweise
in eines von Abstoßung oder gar Ekel übergegangen. Ich glaube,
ich mag nicht, wenn Körperteile in mir drin sind. Auch
Zungen nicht. Bei der Zunge ging es noch, aber jetzt fühle
ich mich widerlich.", gab sie zu. Sie fragte sich, ob die
Worte verletzten. Trotzdem fuhr sie fort. "Ich würde
mich eigentlich nicht wohl damit fühlen, davon
mitzubekommen, wenn du dich selbst befriedigst, oder
mir gar Gedanken machen zu müssen, ob
ich in diese Art Sex vielleicht als Gedanke doch involviert wäre."

Marusch nickte. "Es ist schön, dass du das so ehrlich sagen
kannst.", sagte sie. "Es tut mir leid, dass du dich so
fühlen musst. Brauchst du noch etwas, um dich wohler zu
fühlen, oder etwas, was dir Sicherheit gibt."

"Ich mag deine Erektion nicht mehr fühlen.", gab sie zu. "Es
fühlt sich beschissen an, das zu sagen."

Marusch stand kurz aus dem Bett auf, um sich das Kondom sorgsam
zu entfernen, und nutzte wieder das Handtuch, um Körperflüssigkeiten
abzuwischen. "Möchtest du eine Binde und eine Unterhose haben?"

Lilið nickte und stand ebenfalls auf, um dem Vorschlag
nachzukommen.

Marusch reichte ihr das Handtuch und wartete, bis sie
fertig war, räumte dabei irgendetwas auf. "Möchtest du gern
mehr Abstand? Oder magst du im Arm gehabt werden?"

"Ich würde gern im Arm gehabt werden.", flüsterte sie. Sie
kroch wieder ins Bett und machte Platz. "Wenn du magst."

Sie hatte Angst, gleich doch wieder die Erektion zu fühlen, obwohl
sie gesehen hatte, dass sie abgeklungen war, aber Marusch sortierte
vorsorglich eine Deckenfalte zwischen ihre Becken.

"Warum bist du so lieb zu mir?", fragte Lilið.

"Ich würde gern behaupten, dass ich einfach so so wäre, was
höchstens zum Teil stimmt.", sagte Marusch. "Aber ich kenne
das beschissene Schuldgefühl. Und ich weiß, wie fehl am
Platz das ist, aber dass es nicht so einfach loszuwerden ist." Marusch
streichelte ihr sanft über den Rücken. "Und mein Verhalten
sollte nicht lieb genannt werden müssen, sondern eher der Standard
sein. Ich frage mich, ob sich die Welt je in eine entwickeln
kann, in der wir dieses Schuldgefühl beim Sex loswerden. Und
wenn ja, wie lange das dauern wird."

"Du kennst es?", fragte Lilið. Im nächsten Moment kam ihr
die Frage unbedacht vor. Sie fragte da nach negativen
Erfahrungen mit Sex.

"Sie war älter als ich, wir hatten, nun, über einige Wochen hinweg
geflirtet. Sie hat anzügliche Bemerkungen gemacht, und ich fand
das spaßig und habe sie erwidert. Schließlich haben wir uns
mal zu zweit getroffen und geküsst. Und als wir uns körperlich näher
gekommen sind, habe ich mich auf einmal nicht mehr wohl
gefühlt.", berichtete Marusch.

Lilið wartete ab, ob sie noch mehr sagen würde, aber ein
Schauder von Ekel durchlief sie jetzt schon. Eine völlig
andere Art Ekel, als Blut und Schleim auslösen könnten. Das war nicht
in Ordnung von dieser Person, dachte sie. Und dann wurde
ihr bewusst, dass sie das Ende der Geschichte nicht kannte. Natürlich
ging es sie eigentlich nichts an. "Ich habe gerade festgestellt,
dass ich automatisch schlimme Dinge auf diese Person
projiziert habe. Ich habe mir vorgestellt, dass sie dich bedrängt
hat. Aber sie könnte genauso gut abgebrochen haben und
gefragt haben, ob du überhaupt willst."

"Beides nicht.", sagte Marusch. "Sie hat mich zu nichts
überredet. Ich habe mich von selbst schuldig gefühlt, weil
ich ja vorher schon hätte sagen können, dass ich nicht will."

"Dann ist sie trotzdem nicht safe.", hielt Lilið fest.

"Für mich nicht.", sagte Marusch. "Ich möchte trotzdem festhalten, dass
sie keine Schuld trägt. Sie hat jedes 'nein' akzeptiert, das
ich ausgesprochen habe, als ich mich dann
überwunden habe. Die Schuldgefühle kamen nicht durch
sie. Die waren in mir, weil sie in unserer Gesellschaft
oder Kultur irgendwie in uns gepflanzt werden."

Lilið nickte. "Das stimmt.", sagte sie. Unpassenderweise überkam
sie ein Gähnen. "Ich frage mich, wie genau. Denn eigentlich hat mir meine
Mutter mitgegeben, dass ich nichts tun soll, was ich nicht will. Und
in der Situation, die du geschildert hast, denke ich automatisch, dass
diese Person etwas falsch gemacht hat. Weil du dich nicht
sicher fühlen konntest. Warum hat es mich so viel Mut und gutes
Zureden deinerseits gekostet, zu sagen, was sich für mich gerade
schlecht anfühlt? Und warum fühlt es sich immer noch falsch
an, dass ich nicht möchte, dass du in meiner Gegenwart masturbierst?"

"Letzteres kann ich beantworten: Weil du eben doch involviert
wärest. Selbst wenn du nicht hinsiehst macht es
Geräusche, Gerüche und verursacht rhythmische Bewegungen,
bei denen dein Körper mitschwingt, wenn ich es im Bett
tue. Du partizipierst automatisch.", erklärte
Marusch. "Wenn du mir Vorschreiben wolltest, dass ich auch in
deiner Abwesenheit nicht mehr masturbieren dürfte, wäre das
etwas anderes, aber in diesem Fall fühlst du dich einfach damit
gerade nicht wohl, und das ist verständlich und in Ordnung."

"Was bist du eigentlich für ein Diebeswesen, dass Wörter
wie partizipieren im normalen Sprachgebrauch hat und auf
Bällen tanzt?", fragte Lilið.

"Glaub mal nicht, dass Gesellschaftschichten da ein zwangsläufiges
Argument gegen wären. Hast du Allil reden gehört?", fragte Marusch, ohne
Umschweife den Themenwechsel mitmachend.

"Ohja, sogar wenn sie meinen Vater gespielt hat. Oder
noch veredeltere Personen.", erinnerte sie sich. Es war trotz
allem eine schöne Erinnerung. Sie schmunzelte. Dann sog sie
heftig die Luft ein. "Ich denke, dass Lord Lurch, den du vorhin
erwähntest, mein Vater ist, ist dir nicht entgangen, oder?"

"Nein, das habe ich mitbekommen.", bestätigte Marusch. Sie gähnte
nun auch. Und küsste Lilið sanft auf die Stirn. "Wollen wir
schlafen?"

Lilið küsste ihren Hals. "Das wäre wohl vernünftig.", sagte
sie.

Aber so richtig vernünftig waren sie nicht. Als sie sich fürs Schlafen
zurechtkuschelten, fanden ihre Lippen wieder gegenseitig Stellen
an ihren Körpern, die den Atemrhythmus mit Schlaf inkompatibel machten. Die
Deckenfalte zwischen ihren Becken sortierten sie immer wieder
neu dorthin. Sie schliefen ein, als der Atem nichts mehr hergab
und sie nicht mehr konnten.
