Untertauchen
============

*CN: Versagensgefühle, Schlafmangel, Würgen - erwähnt, Fantasy-Religion
mit Kleiderordnung.*

Als Drudes Schwimmbewegungen wieder schnell und gleichmäßig
wurden, schlief Lilið auf derer Brust ein. Dadurch nahm sie
die Entäuschtheit und ihre Versagensgefühle nicht so sehr
wahr. Sie waren eigentlich unsinnig. Aber sie konnte nicht
vermeiden, dass es sich beschissen anfühlte, dass sie zunächst
fast allein die Organisation eines Plans übernommen hatte, der
nicht geklappt hatte, und bei dem sich Drude mies gefühlt hatte,
weil sie demm wichtige Details verschwiegen hatte, und nun
hatte sie nicht einmal angefangen zu planen, da riss schon alles
auseinander und sie war auf Drudes Hilfe angewiesen. Drude
war ursprünglich nicht von der Partie gewesen und nun übernahm
dey alles für sie, weil sie hilflos wie eine Fliege in einem
Haus war, die den Ausgang nicht fand.

Lilið fragte sich, ob sich Marusch, sollte sie noch am Leben sein,
ungefähr so fühlen musste, wie Lilið jetzt. Als würde das Staffelholz,
wer Lajana zu retten versuchte, weitergereicht, je nachdem, wer gerade
bessere Möglichkeiten dazu hätte.

Eigentlich war die ganze Situation nicht gerade
ermächtigend für Lajana, dass sie als
eine behinderte Prinzessin gerettet werden musste und keine Rolle
in dem Ganzen spielte. Lilið wünschte sich so sehr, dass Lajana
irgendwann die Hauptrolle in deren Leben spielen würde.

Sie glitt in Träume über, in denen schwarze Nebelfische ihnen
durchs Wasser folgten. Sie bewegten sich wie Tinte unter Wasser, ein Omen
des Bösen, aber Lilið hatte im Traum keine Angst, sondern fand sie
eigentlich sehr friedlich. Sie liebte sie auf eine Weise, wie sie
Drude liebte, dachte sie, als Drude sie weckte. Und dann, als sie
sich unmittelbar nach dem Aufwachen noch einmal an den Traum erinnerte,
musste sie kichern.

"Ist etwas komisch?", fragte Drude.

"Ich habe von Fischen geträumt, die sich im Wasser bewegen wie
Tinte, und mir wurde erst gerade bewusst, dass Tintenfisch eine
intuitive Bezeichnung für die Wesen wäre.", erklärte Lilið. "Das fand
ich gerade witzig."

Drude lachte nicht. "Schön, dass du schlafen konntest."

Schwang Neid in derer Stimme mit? "Es tut mir leid, dass du
es nicht kannst.", murmelte Lilið. "Das wird ein harter Tag
für dich. Ich werde Angst um dich haben."

Drudes Griff wurde für einen Moment fester. "Ich habe im Gegensatz
zu dir in den letzten Tagen viel mehr auf meinen Schlafhaushalt
geachtet.", erwiderte dey. "Du hast nicht nur einige Schlafstunden
auf den Beinen verbracht, sondern hast zusätzlich auch in deinen
Schlafphasen einen unruhigen Schlaf gehabt. Dein Kopf war
ständig voll mit vielen Themen. Ich
meine das ernst. Wenn du dir jetzt Schlaf holen kannst, hol ihn
dir."

"Kannst du auch Erfühlen, was andere Leute denken oder so etwas?",
fragte Lilið.

"Nicht was, aber ich fühle, wenn sie angespannt sind. Ich fühle
ihre Energie im Körper sozusagen.", erklärte Drude. "Das ist auch
der Grund, warum wir nicht reden konnten. Wache Luanda saß nicht
allzu weit von uns in einem Einzelraum aufbruchbereit und
ich war mir bei deren konzentrierter Wachheit, die ich wahrgenommen
habe, recht sicher, dass sie auf jedes Wort lauscht. Manche
Antimaga können über große Distanzen sehr fein hören."

Dann war es wohl nötig gewesen, die Kagutte bald zu
verlassen. Oder nicht? Wieviel Zeit hatten sie miteinander
gerungen? Hatte Wache Luanda absichtlich nicht eingegriffen? "Ich
hatte mich beim Gespräch beim Mittagessen mit ihr gefragt,
ob sie vielleicht gar nicht so sehr hinter der Entführung
steht. Aber ich habe in den letzten zwei Tagen gefühlt alle
Menschen falsch gelesen." Lilið seufzte.

"Ich habe zu Wache Luanda keine Einschätzung, außer, dass sie sehr
genau aufpasst.", sagte Drude. "Ich habe dich übrigens gerade geweckt, weil
wir kurz vor meinem Ziel sind. Ich würde dich in einer Höhle
unterbringen. Wenn du dann noch ein Redebedürfnis hast, können wir
dort noch ein wenig weiterreden. Aber da ich vor Sonnenaufgang
zurück sein muss, haben wir nicht viel Zeit."

Lilið stimmte zu. "Muss ich irgendetwas tun?"

"Die Luft anhalten.", erklärte Drude. "Ich tauche dich durch einen
unterirdischen Eingang in eine Höhle in den Felsen hier."

Lilið überstreckte den Nacken, um hinter sich in Richtung
Küste zu sehen, aber sie sah aus verschiedenen Gründen reichlich
wenig. Es war dunkel, Drudes Kopf und Schulter lagen in ihrem
Sichtfeld und Wasser schwappte ihr ins Gesicht. Sie konnte
ausmachen, dass die Küste felsig wirkte, weiter oben ein
wenig dichter Wald aus Bäumen war, die sie nicht kannte, und
irgendwo weit weg an einer Inselnase links Abendlicht von
Zivilisation leuchtete.

"In eine Höhle tauchen, sodass ich nicht abhauen
kann?" Lilið versuchte, das Misstrauen
niederzuringen, das in ihr aufkam. Drude hatte manchmal auf
den ersten Blick seltsame Ideen, aber dey hatte
sich trotzdem immer als vertrauenswürdig
herausgestellt. Oder nicht? Warum hatte dey sie bis zur
Bewusstlosigkeit gewürgt?

"Ich möchte dich niemals gefangen halten.", sprach Drude direkt
in ihr Ohr. "Du kannst zum einen selbst gut genug schwimmen und
tauchen, dass du, sobald du ein bisschen ausgeschlafen bist,
ohne mich rauskommen kannst, und zum anderen gibt es auch oben
einen Eingang. Ich weiß nicht, wie gut du klettern kannst, aber
unmöglich ist das für die meisten Menschen nicht. Es ist nur
ein schwer zu findender Ort mit einer Wasserquelle, wo du sicher
wärest. Deshalb."

Lilið entspannte sich wieder. "In Ordnung.", sagte sie. "Ich
würde gern ausprobieren, ob ich selber rauskäme, bevor du mich
da alleine lässt."

"Das verstehe ich." Drudes Griff wurde wieder fester, aber dieses
Mal nicht sanft. "Ich zähle von vier runter."

---

Die Höhle war für Lilið in der Nacht zu dunkel, um viel zu erkennen. Sie
hatte es geschafft, hinaus- und wieder hineinzutauchen. Das
war in der Finsternis unter Wasser besonders aufregend gewesen, aber
Drude hätte sie gerettet, hätte sie sich verschwommen. Sie würde
es trotzdem erst frühestens am Morgen wieder probieren, wenn Licht da
wäre. Nun lag sie in den Mantel des Nautikas auf eine glatte
Stelle im Fels gekuschelt, neben ihr das Plätschern eines kleinen
Rinnsals Süßwasser auf dem Boden. Gemütlich war das eigentlich
nicht, aber Lilið war ermattet und müde genug, um fast unmittelbar
tief einzuschlafen, sobald Drude verschwunden war.

Lilið wachte von Hunger auf, als das Sonnenlicht schon längst
durch einen Spalt in die Höhle flutete. Es mochte Mittag sein und
Hunger war um diese Uhrzeit wohl etwas sehr Übliches. Lilið
musste bei dem Gedanken Grinsen, dass eine der ersten Mahlzeiten,
die sie im Zusammenhang mit Drude gehabt hatte, eine letzte Mahlzeit
hätte sein sollen, die exquisit und reichhaltig gewesen war. Die
jetzige Situation fühlte sich sehr gegensätzlich an. Sie war
von Drude gerettet worden, hatte ihre erste Mahlzeit außerhalb
der Kagutte und diese bestand nur aus
Wasser. Vielleicht hätte sie aus der Höhle hinausklettern und
sich nach Nahrung umsehen können. Sie hatte keine Ahnung, wie
Belloge bebaut war, aber sie rechnete damit, nicht direkt in
eine Stadt zu gelangen, wenn sie die Höhle verließ, und irgendwo
wohl Plantagen oder Felder zu finden, wo sie sich an Lebensmitteln
etwas hätte zusammenstehlen können. Wenn sie nicht so furchtbar
resigniert und erschöpft gewesen wäre.

In der Höhle war es angenehm kühl. Vielleicht etwas zu kalt. Lilið
wunderte sich, denn sie glaubte, ihr Körper hätte zu jedem anderen
Zeitpunkt diese Temperatur als ausgezeichnet empfunden. Neben
dem Plätschern des Rinnsals war auch das Geschnattere, der Gesang
und das Getute verschiedener Drachen zu hören und gelegentlich
ein Rascheln von was auch immer für Tieren. Einige der Rufe erkannte
Lilið. Etwa das tiefe Summen der Aubendrachen oder das fiepsende
Geräusch einer jungen Auze, die nach Futter verlangte. Sie roch
harzigen Geruch. Vielleicht war es ein Nadelwald da oben. Aber einer,
der etwas fruchtiger roch als die Nadelwälder, die
Lilið kannte. Sie erinnerte sich, dass Belloge auf eine Weise
über den Planeten reiste, durch die die Insel mehr Sommer abbekam als
etwa Nederoge oder die meisten Inseln auf der anderen
Seite der Grenze. (Es gab tatsächlich sogar manch kleine Reiseinsel, die
mit den Jahreszeiten ganz um den Planeten reiste und immer Winter
oder immer Sommer hatte.) Lilið wusste zumindest aus der
Schule, dass es auf Belloge auch Palmen gab. Vielleicht
war es ein Nadelwald wärmerer Natur. Aber warum war ihr dann fast
zu kalt?

Tatsächlich fühlte Lilið sich so energielos, dass sie zunächst
nicht einmal schaffte, überhaupt aufzustehen. Da sie ausreichend
trinken sollte, tat sie es irgendwann doch. Das Wasser war angenehm
frisch, anders als das Wasser an Bord zuletzt geschmeckt hatte. Dabei
hatten sie es in Lettloge wieder aufgefüllt. Sie trank das frische
Wasser aus dem Rinnsal in kleinen Schlucken und genoss
es dabei fast wie ihr letztes
Mahl. Anschließend, weil es ja einfach so dahinfloss und dadurch nicht
verschwendet wurde, wusch sie sich sehr gründlich. Und als sie damit
fertig war, war sie noch erschöpfter und energieloser. Sie breitete den
Mantel des Nautikas auf den Sonnenstrahlen aus, legte sich darauf
und schlief wieder ein.

Sie wachte noch das ein oder andere Mal auf und fragte sich, ob sie
etwas Besseres tun könnte, als herumzuliegen und zu schlafen. Es erschien
ihr alles sehr hoffnungslos. Wenn sie versuchte, sich einen Plan
auszudenken, bekam sie bereits Versagensgefühle, bevor sie überhaupt
Gedanken über die Realisierung ihrer Situation hinaus fand. Schlafen
war wenigstens gut gegen dieses Gefühl. Was brachte das schon. Also
schlief sie. In einer der kurzen Wachphasen zog sie ihren
Schlafplatz noch einmal um, als ihr sehr kalt wurde, weil der Streifen
Sonne auf dem Höhlenboden weitergewandert war. Wieso kannte Drude
eigentlich diese Höhle?

Warum hatte sie sich so sehr gegen Drude gewehrt? Hätte dey sie
vielleicht nicht überwältigt, wenn sie sich ruhig verhalten hätte? Eigentlich
wusste sie es. Drude hatte das nicht gewollt. Lilið versetzte sich
in Drudes Lage hinein, so gut sie konnte. Eigentlich hatte
Drude mit Lilið reden wollen, aber darum
herum Menschen wahrgenommen, die lauschten. Und vielleicht in
Wache Luandas Fall auch die Energie zwischen ihnen spürten. Sie
hatten keine Möglichkeit gehabt, etwas abzusprechen und schnell
weg gemusst. Es war schon ohne Liliðs Sträuben für demm ein
riskantes, nicht gut kalkulierbares Treffen gewesen. Wie es
Drude wohl jetzt ginge? Wenn Wache Luanda so genau einzelne Personen
herausfühlen konnte wie Drude, hatte sie gewusst, dass Drude und
Lilið sich nachts getroffen hatten. Oder müsste Wache Luanda sie
öfter berührt oder wahrgenommen haben, um sie identifizieren
zu können? Hatte sie sich deshalb zum Essen ihr gegenüber
niedergelassen? Aber wenn sie Drude und Lilið erkannt
hatte, war vielleicht von Vorteil, dass sie nun denken
musste, dass sie gekämpft hatten?

Wenn Lilið doch Drude einfach nur zugehört und vertraut hätte. Wenn
irgendetwas jetzt einen Sinn ergäbe. Wie lange sollte Lilið hier
warten, ob Drude zurückkäme? Wieder schlief sie ein, dieses Mal
unruhiger.

---

Als sie die Augen abermals öffnete, war es fast dunkel und Drude saß neben
ihr auf dem Höhlenboden. Die Flecken Licht, die noch von oben in die
Höhle fielen, waren gerötet.

Lilið richtete sich auf. Erschöpft und leer fühlte sie sich immer
noch. Vollkommen kraftlos. "Ich glaube, ich habe mir einen psychischen
Zusammenbruch eingefangen.", murmelte sie.

Drude blickte sie an und grinste, versuchte ein lautloses Kichern
zu vermeiden.

"Ist das irgendwie witzig?", fragte Lilið.

Drude schüttelte den Kopf, hörte mit dem Grinsen auf, fing aber
direkt wieder damit an. "Der erste Kommentar, der mir dazu einfiel, war
'Endlich!'. Und das ist vermutlich kein sehr einfühlsamer Kommentar. Vielleicht
sogar eher einer, der sozial voll daneben ist.", gab dey zu. "Aber
irgendwann musste das halt kommen. Du warst ganz schön lange psychisch
arg überlastet."

Einen Moment führten Drudes Worte dazu, dass Lilið sich weniger schlecht
fühlte. Es war normal, dass das nun mit ihr passiert war. Aber sie hätte
es auch ohne Drudes Kommentar besser über sich akzeptieren können,
wenn es nicht wichtige Probleme gegeben hätte, um die sie sich kümmern
wollte. "Wieso kannst du noch? Und wie waren sie zu dir?", fragte Lilið.

"Ich bin zum einen sehr belastbar, weil ich Wache und entsprechend
ausgebildet bin, hatte aber zum anderen dir gegenüber
einige gewaltige Vorteile.", antwortete Drude geduldig. "Ich habe
nicht spielen müssen, eine andere Person zu sein. Ich durfte auch
gestern noch Magie ausüben, also war ich in meinen Mitteln nicht
eingeschränkt. Ich habe zu keinem Zeitpunkt Magie aufrecht erhalten
müssen, du schon. Ich hatte und habe immer
noch das Vertrauen meiner Crew. Ich wurde als Wache eingestellt und war
bei der Entführung wesentlich beteiligt. Da bin ich nicht stolz drauf,
aber das führt zu einem Grundvertrauen in mich, das jetzt, so scheint
es mir, höchstens leicht angekratzt ist. Mich wollte jedenfalls niemand
töten und für dich stand die Bedrohung die ganze Zeit im Raum, wenn
du auch nur einen kleinen Fehler gemacht hättest."

Lilið legte sich wieder zurück auf den Mantel des Nautikas. Ihr
war eigentlich inzwischen sehr kalt, aber sie tat aus
Faulheit nichts dagegen. "Und ich habe nicht einmal dir
vertraut.", flüsterte sie. "Es tut mir leid."

"Lilið!", sagte Drude energisch, aber sprach dann sofort ruhig
weiter. "Ich habe dir schon einmal gesagt, dass es keine Grundlage dafür
gab. Es hätte vielleicht manche Momente für mich einfacher
gestaltet. Deshalb war ich sauer, und das tut mir leid, das stand
mir nicht zu."

Dey blickte Lilið einige Momente an. Lilið ging inzwischen davon aus,
dass dey sie viel besser sehen konnte als umgekehrt, aber Lilið konnte
schon das flüchtige Runzeln in derem Gesicht erkennen. Drude seufzte,
beugte sich nach hinten zu einem großen Rucksack und löste eine darauf
festgegurtete Decke, die sie Lilið zuwarf.

Lilið fühlte ein jähes Gefühl von Dankbarkeit dafür. Vielleicht
ein unverhältnismäßig starkes. Sie wickelte sich in die Decke und
merkte sofort, wie sie weniger fror. "Kannst du auch fühlen, dass
Leute frieren?"

Drude nickte. "Einigermaßen. Ich bekomme mit, dass sie zittern, und
wenn keine Angst im Spiel ist, spricht das für frieren."

"Es ist beeindruckend.", murmelte Lilið. "Dann war für Wache Luanda
wohl wahrnehmbar, dass wir gekämpft haben und du mich erwürgt hast."

"Gewürgt, nicht erwürgt.", korrigierte Drude. "Ja. Ich habe ihr
gegenüber behauptet, dass ich dich vom Fliehen habe abhalten wollen
und du auf mir nicht nachvollziehbare Weise plötzlich verschwunden
wärest. Immerhin aber ohne die Kronprinzessin. Ich hätte dann
versucht, dir hinterher zu schwimmen, aber dich nicht mehr
gefunden." Drude räusperte sich. "Das verschärft deinen Ruf
auch, und ich bin nicht sicher, wie gut das für dich ist. Auf der
einen Seite haben Leute mehr Angst vor dir, auf der anderen werden
sie dir mit größerer Brutalität begegnen, wenn sie dich erkennen."

Lilið seufzte lautlos. "Ich tauge in Wirklichkeit gerade so
zu einer mittelmäßigen Diebesperson. Eigentlich." Plötzlich
musste sie grinsen. Wie hatte sie es geschafft, sich so einen
Ruf anzueignen? Und wie könnte sie das ausnutzen?

"Es tut mir jedenfalls immer noch sehr leid, dass ich dich gewürgt
habe.", sagte Drude. "Einfach falls dir das hilft: Ich wusste, was
ich tue und dass du dadurch höchstens ein paar Minuten ausfallen würdest
und keinen langfristigen Schaden davon haben wirst."

"Ich weiß.", erwiderte Lilið sanft. Sie hatte die Art des Würgens
schon so eingeordnet, als es passiert war, fiel ihr wieder ein.

Sie atmete noch einmal tief ein und aus und blickte sich in der Höhle
um. Drude hatte neben dem großen Rucksack noch eine mittelgroße,
vollgestopfte Tasche mitgebracht. Waren die Gepäckstücke
wasserdicht? Oder waren sie von oben in die Höhle gelangt? Oder
hatte Drude Magie darauf angewandt? Dey konnte immerhin die
Unterwassertür öffnen, ohne dass Wasser in die Kagutte
eindrang. War das alles Gepäck, was dey an
Bord dabei gehabt hatte? "Wo ist eigentlich Lil?"

"Dey verbringt Zeit im Wald mit anderen Aben. Dass macht Lil meistens,
wenn wir nach einer längeren Zeit auf See wieder an Land sind.", antwortete
Drude.

"Wieso hast du die Abe eigentlich Lilið genannt?", fragte Lilið
nachdenklich.

Drude suchte aus derem Gepäck weiche Kleidung hervor und sortierte
sie neben Lilið als Unterlage für sich. "Ich fand den
mythischen Namen passend für so einen
kleinen schwarzen Drachen.", sagte dey. "Ich mag vieles an dem Charakter
Lilið. Ich mag, dass Lilið eine anti-patriarchale Figur und gleichzeitig
ein Dorn im Auge der Sakrale und Orakel ist."

"Oh, über letzteres habe ich noch nie nachgedacht!", fiel Lilið auf. Aber
es stimmte wohl. Sie war mit diesem Namen nie wissentlich
in eine hineingelassen worden. Sie überlegte, ob sie darauf genauer eingehen
wollte, aber sie war immer noch zu müde. Hatte sie nicht den ganzen
Tag geschlafen? "Warum heißt du eigentlich Drude?"

Drude lachte eins der lautlosen, nicht glücklichen Lachen. Dey
kuschelte sich an Lilið heran und nahm sie einfach von hinten
in den Arm, so wie sie das in den letzten Tagen in ihren gemeinsamen
zwei Stunden Schlaf immer gemacht hatten. Nur dass nun eigentlich
genug Raum da war, sodass es nicht notwendig gewesen wäre. Lilið
beschwerte sich nicht. Ein Teil von ihr wünschte sich, dass Drude
sie fester fixieren möge, aber dieser Teil war auch sehr müde.

"Ja, Drude ist nicht der Name, den ich vom Orakel bekommen habe. Das
ist wahrscheinlich unschwer zu erraten.", antwortete dey
schließlich. "Druden sind unheimliche, dunkle Wesen, die Menschen
Albträume bescheren. Damit wurde ich schon von klein auf verknüpft.
Irgendwann haben mich viele so wie diese Horrorgestalten genannt, um
mich zu ärgern. Ich fand das erst nicht so gut, aber
habe mich dann irgendwann daran gewöhnt und bin dem Ruf noch
gerechter geworden. Nur dass ich eher ein wässriges als ein
baumiges Drudenungeheuer bin."

"Du bist mein liebstes wässriges Drudenseeungeheuer.", murmelte
Lilið.

Sie grinste, als sie Drudes Körper gegen ihren Rücken drücken fühlte, in
einer Weise, die Lilið als Gerührtheit oder Freude las.

"Danke.", flüsterte dey. "Es ist schön, dir etwas zu bedeuten."

Lilið rührten Drudes Worte und die Art, wie dey sie sagte, fast zu
Tränen. Dass jemand wie Drude keine Freundespersonen hatte, war
so nicht richtig. Auf der anderen Seite hatte Lilið selbst auch sehr
wenige.

Sie seufzte innerlich, weil sie ein Thema ansprechen wollte, was den
Zauber aus der Situation nehmen würde. Aber sie fühlte auch, dass Drude
müde war und bald schlafen wollen würde. Sie wollte das Thema vorher
zumindest angerissen haben. "Was ist mit Lajana?"

"Lajana wurde von Sakral-Dienenden und einigen Sakralswachen in die
belloger Zentralsakrale verbracht.", berichtete Drude. "Leider ist
es ein sehr sicheres Gebäude. Mein Plan für einen Überfall ist
noch sehr grob. Ich denke, wir sollten uns als Sakral-Dienende
ausgeben. Deren religiöse Kleidung verdeckt Körper und Haare und
auch das Gesicht ist darin im Schatten. Wir können auf diese Weise
vielleicht gut untertauchen."

"Hieß die Kleidung nicht Sakralutte?" Lilið erinnerte sich nur vage. Sie
hatte mit Religion bisher wenig zu tun gehabt. Ihr größter Berührungspunkt
war ihre Namensvergabe gewesen. Damals war sie ein Säugling gewesen, aber
die Geschichte war ihr immer wieder erzählt worden. Ein Orakel nannte
nicht einfach so ein Kind Lilið.

Sie bemerkte von Drudes Seite eine klare Ablehnung, die
sie, vielleicht mangels Erfahrung, bisher nicht teilte.
Aber wenn sich die Orakel einmischten und diese
Entführung guthießen, dann hatte sie wenig Hemmungen, bei der Art,
wie sie sich in Diebesdingen oder im Fall der Entführung verhielt,
keinen Unterschied zwischen Sakrals-Leuten und anderen zu machen. Sie
konnte immer noch versuchen, im Rahmen der Dinge, die sie tun musste,
so respektvoll wie möglich zu bleiben.

Drude nickte, was Lilið im Nacken spürte. "Genau, ich denke, wir sollten
uns Sakralutten stehlen und uns unter dem Orakel-Volk umhören. Aber
ich schaffe das erst morgen. Ich brauche Schlaf.", sagte dey. "Ich glaube
auch, dass es gut ist, bis morgen zu warten. Ich glaube, heute ist die Crew
noch sehr zusammen und könnte für uns eher eine
Gefahr darstellen, wenn wir Crewmitgliedern begegnen, weil sie ihr Wissen kombinieren
können, wenn sie dich sehen oder wahrnehmen. So weiß noch niemand, dass du
falten kannst. Wache Luanda kann dich wahrscheinlich noch nicht
identifizieren, wenn du nicht gerade an einem Ort bist, wo
sie dich auch erwartet. Zumindest, wenn sie dich nicht sieht. Sprich, wenn
sie dich getrennt von anderen beim Falten wahrnimmt, weiß sie wahrscheinlich
nur, dass jemand faltet, aber nicht, dass du es bist, und wenn irgendwer
faltet, ist das ja im Normalfall nicht unbedingt ein Problem. Es ist
ja nicht grundsätzlich verboten, Magie auszuführen. Ergibt das Sinn?"

"Vielleicht.", murmelte Lilið. Vielleicht ergab es einen, aber
vielleicht war auch vermeidbar, auf die Crew zu treffen, und ein
Teil der Begründung hatte den Grund, dass Drude Schlaf
rechtfertigen wollte. Sie wäre aber selbst dann nicht
sinnlos. "Ich verstehe das Prinzip zumindest: Sie
wissen verschiedene Dinge von mir und wenn sie uns allein begegnen, ist
es sicherer, als wenn wir ihnen allen zusammen begegnen."

"Genau!", stimmte Drude zu. "Und sie werden sich morgen früh mehr
zerstreuen. Einige, zum Beispiel Wache Schäler, sind
für die Bewachung der Kronprinzessin dageblieben, aber ein paar
aus der Crew werden morgen wieder woanders anheuern, und einige
werden König Sper entgegenreisen, um ihn auf seinem Weg hier her zu
schützen und zu informieren. Der Kapitän bleibt mit zwei Wachen bei
der Kagutte."

"Wie funktioniert die Erpressung eigentlich?", fragte Lilið. "Lajana
wird sich ja weiterhin weigern, etwas zu unterschreiben. Was hilft
dann ihre Gefangennahme?"

"Königin Stern kommt persönlich, um sie abzuholen. Sie würde extrem
an Beliebtheit und Vertrauen verlieren, wenn sie es nicht täte. Es
wird von ihr erwartet. Wenn sie ankommt, -- das passiert in
voraussichtlich einer Woche --, befindet sie
sich natürlich in feindlichem Gebiet. Und daraus ergeben sich allerlei
Möglichkeiten.", berichtete Drude. "Es gibt verschiedene Gerüchte
darüber, ob sie den Kronprinzen gleich mitbringt. Täte sie
es, dann könnte er zum Beispiel sehr offiziell einer Krönung
zustimmen, wenn andernfalls das Leben der Königin bedroht wäre. So
etwas. Ganz genau kenne ich mich allerdings auch nicht aus."

Lilið fühlte sich leicht schwindelig. "Ich sollte noch einmal etwas
trinken.", sagte sie und schälte sich aus Umarmung und Decke. "Aber
ich glaube, du hast recht. Noch eine Nacht schlafen, und dann als
Sakral-Dienende verkleidet die Lage ausforschen, klingt nach dem
besten Plan, den wir haben."
