Schweigen
=========

*CN: Herzprobleme - erwähnt, emotionaler Druck, Ratte, Eingesperrtsein -
vielleicht an Klaustrophobie erinnernd, Religion mit Tradition und Kleiderordnung
sowie einem Ritual, vermutlich
am ehesten auf katholische Rituale anspielend.*

Auf dem Weg zum Hafen hinab malte Lilið sich aus, was alles
wie scheitern könnte. Was, wenn die Fälschung des Buchs
auffliegen würde? Oder ihr einfach kein Boot gegeben würde, weil
ihr Vorhaben als zu dreist empfunden würde? Was
wenn Drude im Leuchtturm erwischt würde? Was, wenn sie in der
Sakrale waren und die Wachen sie entlarvten?

So unkonkret halfen die Fragen nicht. Lilið malte sich also
genauer aus, wie sie das Buch zur Unterschrift abgeben würde,
das Zeichen für das Reinigungsritual täte und die Person, die
ihr das Boot übergeben sollte, stattdessen mit ihr versuchen
würde, zu diskutieren. Sie würde einfach das Zeichen
wiederholen. Und wenn sie das Boot nicht
bekäme? Würde sie eines zu stehlen versuchen. Alles sehr heikel.

Drude holte sie aus den Gedanken. "Hier trennen sich unsere Wege
vorerst."

Lilið blickte auf. "Wo muss ich hin?" Es roch bereits nach
Hafenwasser. Etwas fischig, etwas weniger nach Natur und doch
nach Salz. Die Öwenendrachen dominierten mit ihrem langgezogenen
Gequietsche die Geräuschkulisse.

"Durch diese Häuserreihe hindurch, dann siehst du direkt das
Bootshaus. Ich habe dich fast hingebracht.", erklärte Drude. Sie
standen in einer sehr schmalen, menschenleeren Gasse. Es gab keine
Fenster in den Gebäudewänden, die sie einzwängten. Drude zog dere
Sakralutte aus und reichte sie Lilið.

Lilið faltete sie mit einer verdrehten Fächerfaltung. "Wenn
du sie entfaltest, einmal so zwirbeln", sie deutete eine Drehung
an, "dann schütteln. Wenn du dich nicht sehr zackig bewegst, sollte
die Faltung nicht von allein aufgehen." Und, wie Drude erklärt
hätte, würde die Magie der Faltung auch nicht spürbar sein, solange
keine aufgewendet werden musste, um sie zu halten.

Drude bedankte sich mit aneinander gelegten Händen und einer
angedeuteten Verbeugung, wie Lajana das mal getan hatte, und
steckte das kleine, feste Stoffbündel in die wasserdichte Tasche, die
sie unter der anderen Kleidung trug. Dann zog sie Lilið noch einmal in
eine feste Umarmung. Lilið erwiderte sie, spürte den großen Körper
schwer gegen sie atmen. Sie sprachen kein weiteres Wort, bevor
Drude die andere Richtung einschlug und verschwand.

Drude hatte Recht gehabt: Die Bootshalle war nicht zu verfehlen. Der
Himmel war bedeckt, die See spiegelte die grauen Wolken
in der ungewöhnlich glatten Wasserfläche vor ihr, von der sie an manchen Stellen am Horizont
nicht ausmachen konnte, wann das Grau in den Himmel überging. Flaute. Segeln
würde verhältnismäßig langsam gehen. Öwenen
kreisten im Hafen. Sie waren vielleicht etwas größer als
die, die Lilið von Nederoge kannte. Der Leuchtturm erhob sich
in nicht allzu weiter Entfernung aus dem Wasser, schwarz
und weiß gestreift, und mit dem interessanten Grundriss eines
vierzackigen Sterns. Zwischen den Strahlen war es jeweils möglich,
mit einem kleinen Boot anzulegen. Das konnte sie von hier sehen,
weil gerade eines am Leuchtturm befestigt war. Es war so
klein, dass Lilið vermutete, von hier bei diesem Wetter eine knappe halbe Stunde zum
Leuchtturm zu brauchen, wenn sie auch ein solches hier bekäme. Auf
der einen Seite wäre sie gern schneller gewesen, auf der anderen
war das wahrscheinlich eine gute Sache, weil Drude sicher
auch Zeit brauchen würde, den Schleusenmechanismus zu betätigen. Es
könnte hinkommen, dass sie etwa gleichzeitig am Fuß des Leuchtturms
ankämen.

Lilið atmete tief durch, fühlte die Salzluft und den Dreck des Hafens
in der Lunge, aber auch den typischen Duft von Sakralen, und betrat
das Boothaus. Hinter einem Schreibtisch saß eine Person in einer
Sakralutte, einer blauen, die aber interessanterweise hochgeschlossen
war, und schüttelte grinsend den Kopf, als sie Lilið erblickte. Lilið
wusste nicht, wie sie es deuten sollte. Egal. Einfach so tun, als hätte
alles schon dere Richtigkeit. Sie trat an den Tisch heran, machte das
Zeichen, das Drude ihr gezeigt hatte, und händigte das Buch aus.

Die Person schüttelte abermals den Kopf, dieses Mal lachte sie sogar,
nahm aber das Buch entgegen, füllte rasch, und ohne viel zu überprüfen,
eine Zeile aus, und beförderte es in eine Schublade. "Ihr Halunken.", sagte
sie.

Lilið verstand das Gesagte, weil sie das Wort 'Halunke' im Alevischen sehr
mochte und es sich deshalb gemerkt hatte. Sie versuchte, sich einen Reim
darauf zu machen, als die Person den Finger auf die Lippen legte, Lilið
Zeichen gab, ihr zu folgen, und mit ihr ein schmales, kleines Boot aus einer
kleineren, noch mehr nach Sakrale riechenden Halle neben dieser
Hauptbootshalle zum Wasser trug. Es war, wenn Lilið nicht
alles täuschte, wirklich ein ebensolches Boot, wie es am Leuchtturm angelegt
hatte.

War das Lachen und die Beschimpfung vielleicht einfach
ein lieb gemeintes 'Ich sehe, dass ihr das
Ritual doch ausführen wollt, und schüttele deshalb euch belächelnd den
Kopf darüber.'? Das könnte passen. Lilið wünschte sich so sehr, dass sie
mit dieser Einschätzung recht hatte.

Die Person hielt das Boot am Steg fest, bis Lilið eingestiegen war, das
kleine Segel gehisst und bereit zum Ablegen war. Dann ging die kleine
Gestalt zurück ins Bootshaus und Lilið segelte aufs Meer hinaus. Allein.

Das sakralierte Boot würde nicht ihr Lieblingsboot werden, dachte
sie. Es war filigran,
könnte leicht kaputt gehen, und bot im Ausgleich dafür nicht einmal
Schnelligkeit oder Wendigkeit wie die leichten Boote, die Lilið bisher
gesegelt hatte. Aber es war ein leises Boot. Es glitt durchs Wasser
wie ein Fisch. Es konnte kaum die Rede davon sein, dass es das Wasser
vor ihm durchschnitte. Es fügte sich einfach ein, glitt still
und gemächlich dahin. Und das hatte eine ganz eigene Schönheit.

Dadurch, dass Lilið schwieg und sich des Schweigens bewusst war, hörte
sie die Natur um sich herum um so deutlicher. Statt die Wellen gegen
das Boot schwappen zu hören, was sonst beim Segeln ein dominantes
Geräusch sein konnte, hörte sie sie unabhängig von ihrer eigenen Existenz
und der Existenz des Bootes um sie herum schwappen. Die Öwenen
flogen hier freier, stießen manches Mal
Feuerstöße in den Himmel. Lilið fühlte auch viel genauer in die Natur
hinein und genoss sie. Es war seltsam, was so ein Schweigen mit sich
brachte.

Trotz der Ruhe und Entspannung, die die Fahrt in ihr ausgelöst hatte,
fühlte sie sich durchgeschwitzt, als sie am Leuchtturm ankam. Sie
entschied sich, zwischen den dem Land abgewandten Zacken des Sterns anzulegen. Sie
wusste nicht, ob das Boot, das auf der gegenüberliegenden Seite immer noch
vertäut war, dort einfach dauerhaft lag, oder ob irgendwann eine Person
zurückkommen und damit wegsegeln würde. Sie konnte auf eine Begegnung
mit Fremden verzichten. Die dem Land abgewandte Seite hatte außerdem den
Vorteil, dass sie vom Land aus nicht ewig hier abwartend gesehen werden
würde. Aber als sie sich am Steg festgemacht hatte, zweifelte sie
an ihrer Entscheidung. Weniger weit entfernt von hier als das Land auf der anderen
Seite des Leuchtturms lag eine Kagutte vor Anker. Lilið war sich nicht sicher, ob es
die Kagutte war, mit der sie hergereist war. Wenn, dann war sie sehr
überholt worden. Sie hatte Ähnlichkeiten, sah aber viel neuer aus.

Lilið hielt den Atem an, beobachtete, und entschied dann doch wieder zu
atmen. Sie blickte zur Sakrale hinüber. Sie war vielleicht noch einmal
eine knappe halbe Stunde von ihr entfernt. Hoffentlich waren sie für
das Ritual nicht zu spät, aber es sollte eigentlich zeittechnisch
alles passen. Wenn Drude nur bald käme.

Lilið atmete erleichtert aus, als sie von hier aus beobachten konnte,
wie eine Wand unterhalb der Sakrale heruntergelassen wurde. Das musste
die Schleuse sein. Drude hatte es also geschafft. Irgendwann würde
Lilið demm fragen, wie. Nun erst einmal bereitete Lilið das Boot zum
Ablegen vor, damit sie im Zweifel, sollte Drude es besonders eilig
haben, fast sofort weg sein könnten.

Nur Momente später eilte die hochgewachsene Gestalt aus dem Leuchtturm
zu ihr. Die Sakralutte hatte dey also erfolgreich entfalten können. Drude
stieg zu Lilið ins Boot und verscheuchte sie von der Pinnensteuerung. Das überraschte
Lilið. Aber es würde wohl einen Sinn haben. Sie rechnete damit, es bei
der Schleuse zu erfahren. Drude wusste vielleicht etwas über das
Hineingelangen, was Lilið noch nicht wusste.

Sie stießen sich vom Steg ab und Drude steuerte sie auf die Sakrale
zu, führte auch das Segel. Lilið fühlte sich mit
einem Mal zittrig und angespannt. Die
innere Ruhe, die die erste Hälfte der Fahrt mitgebracht hatte, war
verflogen. Dabei hatten sie nun schon so viel geschafft! Und sie
hatten einander wieder! Lilið versuchte, langsam und tief ein- und
auszuatmen, um sich zu beruhigen. Ihr war gar nicht klar gewesen, dass
Drude so gut segeln konnte, fiel ihr auf. Vielleicht sogar besser
als Lilið. Wieder fühlte sie den Schweiß unter ihrer Sakralutte über
ihren Körper und ihr Gesicht rinnen. Angstschweiß. Drude musste
es fühlen. Vielleicht wünschte sich Lilið, dass Drude ihre Hand drücken
würde oder so etwas. Aber das tat dey nicht.

Dann verstand Lilið, woher nun die Anspannung kam: So gut Drude auch
segelte, Lilið wäre lieber selbst gesegelt. Es hätte sie beruhigt,
etwas in der Hand zu haben. So blickte sie einfach gen näherkommender
Sakrale, beobachtete das ruhige Meer um sie herum und musste mit
sich machen lassen. Sie vertraute Drude, das war dieses mal nicht
das Problem. Sie hätte einfach nur gern etwas zu tun gehabt.

Ein kleiner schwarzer Drache schoss über sie hinweg. Lilið blickte
auf. Auch Drude hob kurz den Kopf, aber senkte ihn direkt
wieder. Die Abe schwebte über sie hinweg, ließ ein kurzes Funkengestöber
hinter sich und flog dann, Tempo aufnehmend, wieder aus ihrem Sichtfeld
gen Leuchtturm. War es die Abe Lil? Entweder war sie es nicht und
kam deshalb nicht zu ihnen ins Boot, oder die Abe erkannte, dass sie
gerade nicht stören sollte.

Lilið versuchte fast krampfhaft, die Angst wegzuatmen, die sich
zunehmend mehr aufbaute, und die sie kaum mehr aushielt, als sie
endlich die Sakrale erreichten. Irgendetwas stimmte nicht, dachte
sie. Stimmte wirklich etwas nicht, oder war das nur die Angst? Bisher
lief alles sehr gut. Vielleicht war es das. Vielleicht dachte sie,
dass nicht einfach alles gut gehen konnte. Das war auch bei
ihrem ersten Befreiungsversuch der Fall gewesen.

Drude steuerte sie unter die Sakrale in das Becken nahe des seeseitigen
Eingangs. Lilið merkte sich, in welcher Richtung der Ausgang
sein musste, wenn sie oben ankamen, um, wenn es so weit war,
rasch fliehen zu können.

Drude tat nichts, was Lilið überrascht hätte. Dey steuerte
nicht von irgendeiner anderen Seite hinein und machte auch sonst
keine aufregenden Manöver. Dey steuerte einfach auf die Stange zu,
an der sie sich festhalten konnten, wenn der Wasserpegel stieg, und zog
an einem Seil, das den Schleusenmechanismus in Gang setzte. Lilið
wurde schwindelig, als sich das Schleusentor langsam schloss.

Und fühlte sich so eingesperrt, wie sie sich noch nie im Leben gefühlt
hatte. Dabei war der Raum groß. Und sie befand sich mit einem Segelboot
auf Wasser. Das war ihr Zuhause, alles was sie brauchte, hatte sie immer
geglaubt. Aber das Schleusentor verschloss nicht nur einen abgedichteten
Raum, in dem Wasser aufsteigen konnte, sondern sperrte auch die Welt da draußen
aus. Lilið hatte kein so starkes Gefühl für die Beschaffenheit
der Dinge gehabt, wie Marusch damals beschrieben hatte, dass es möglich
wäre. Aber sie spürte, dass sie eben doch zuvor eine Verbindung zu all
dem gehabt hatte, die nun fehlte. Das Schleusentor und die Wände der
Sakrale sperrten Magie aus. Das ganze Sein der Welt. Lilið weinte
fast, weil es ihr sofort fehlte. Und noch etwas fehlte, von dem Lilið
sich nicht einmal bewusst gewesen war, dass sie es die ganze Zeit wahrgenommen
hatte: Das Igeldings der Prinzessin. Die Schwingungen dieser Kreatur
hatten wie ein ständiges Rauschen unter ihrer Haut sachte vibriert. Waren
mal etwas stärker, mal schwächer gewesen. Und nun war es still. Die
Welt schwieg hier drinnen.

Was bedeutete das? Dass sie das Igeldings draußen wahrnahm, aber hier
drinnen nicht, musste heißen, dass es nicht hier drin war. Lilið durchströmte
eine plötzliche Angst, dass alles umsonst gewesen wäre, dass auch Lajana
nicht hier wäre. Freiwillig hätte sie sich sicher nie vom
Igeldings getrennt.

Das Schleusentor war selbst nicht leise gewesen. Für ein Schleusentor vielleicht
schon, aber es war immer noch ein monströses, schweres Tor, das nicht
vermeiden konnte, ein leichtes Donnern durch den Raum erschallen zu
lassen, als es einrastete. Der Wasserspiegel hingegen stieg lautlos an. Das
Wasser flüsterte nicht, schwieg. Lilið
konnte Alawin auf der anderen Seite vielleicht doch verstehen. Auch wenn
sich das Schweigen der Welt für sie beklemmend anfühlte, bewunderte sie diesen
Raum sehr. Er roch feucht und nach Stein, dem typischen Geruch von Sakralen,
aber reiner. Die Akustik war hallend wie in einer Tropfsteinhöhle. Eigentlich
wusste Lilið nicht, wie eine Tropfsteinhöhle klang. Aber so stellte
sie es sich vor. Es gab nur wenige Geräusche, die hallen konnten,
aber das machte diese umso schöner und spürbarer. Es
war relativ dunkel, aber über ihnen fiel fahles Licht
in die Sakrale, dem sie sich mit dem Steigen des Wasserspiegels
näherten. Auf der einen Seite hatte Lilið das Gefühl, diesen Ort niemals
verlassen zu können, wenn er es ihr nicht selbst erlaubte (was auch immer
das heißen sollte), auf der anderen verstand sie, dass hier geschwiegen
werden sollte, weil ihr die Atmosphäre dadurch etwas gab, was sie nie zuvor
gefühlt hatte. Etwas Magisches, hätte sie gesagt, wenn Magie nicht gerade
das gewesen wäre, was fehlte. Etwas Überirdisches, das zugleich sehr
Hier und Jetzt war.

Der Wasserspiegel hörte sanft auf zu steigen, als er mit dem Beckerand
abschloss. Einige flache Wellen schlugen über den Rand hinweg und
nässten den Boden der Sakrale um sie herum. Drude stieg aus und knotete
das Boot fest. Lilið folgte demm. Sobald ihre nackten Füße den Boden
der Sakrale berührten, wurde das unbehagliche Gefühl, gefangen zu sein,
stärker. Der Boden fühlte sich falsch an, als wäre er nicht fühlbar. Als
böte er ihren Füßen nur physischen Widerstand. Das war nicht ganz richtig,
eigentlich fühlte sie ihn schon. Aber der Boden hatte keine Temperatur. Keine
Schwingungen, keine Magie. Auch der Boden schwieg. Lilið
wünschte, er wäre kühl gewesen. Immerhin war das Wasser kühl, mit dem
Teile davon benetzt waren. Wenn sie doch Magie brauchen würden, würde Drude es
vielleicht benutzen können.

Lilið hoffte, dass sie in dieser Sakrale überhaupt falten könnte. Sie
überlegte, es auszuprobieren, aber Drude hatte sie gewarnt: Erst
Magie ausüben, wenn es nicht mehr anders ginge. Magie würde die
Wachen wieder auf den Plan rufen. Eine Wache, die Magie erfühlen
und vielleicht sogar unterdrücken konnte, war mit Sicherheit dabei.

Lilið folgte Drude aus dem Halbdunkel des Schleusenraums durch eine schwere
Flügeltür in einen Raum mit Ausmaßen, über die sich Lilið vielleicht im
Vorfeld hätte klarer sein müssen. Pompös war eine passende Beschreibung. Das
weiße Material mit der rostbraunen Maserung wurde hier durch ein paar
schmale, sehr hohe Fenster in diesiges Licht getaucht. Die ganze
Inneneinrichtung, einschließlich der Bänke, bestanden daraus. Es war
ein Saal der Verkündung, erkannte Lilið. Aber einer, der vermutlich nie
von niederem Volk betreten wurde, wenn es nicht gerade Sakrals-Dienende
waren. Und hinten in diesem Raum befand sich tatsächlich Lajana, sicher
zwölf Meter vom Ausgang auf die Terrasse auf der anderen Seite
der Halle entfernt. Drude würde also die Wachen aus dem Raum bitten. Sie
würden Lajana fesseln, wenn sie gingen. Lilið würde die Schlösser der Fesseln
knacken, sie würden auf die Terrasse eilen und erst dort würde
Lilið das erste Mal Magie anwenden, um Lajana und sich zu falten. Der
Plan wirkte nun so greifbar. Vorausgesetzt, die Wachen ließen sich
auf Drudes Aufforderung zu gehen, ein.

Lajana saß dort, wo sonst Sakraleten ihre Ansprachen
hielten, auf einem schweren, verzierten Stuhl mit blauer
Polsterung, der nicht zur Einrichtung des Rests des Raums
passte. Neben ihr saßen in weniger verzierten, aber ebenso
unpassenden Stühlen zwei Wachen, von denen zum Glück
keine Wache Schäler war. Die eine war die
Wache, die an Bord immer gedöst hatte. Lilið fand interessant, dass diese
hier sein durfte. Die andere kannte Lilið nicht. Zumindest erinnerte
sie sich nicht an das Gesicht. Aber die Wache war größer, als Wache
Schäler es war. Drude tat einige Schritte voraus, der Gang
selbstbewusst, als wäre dey hier zu Hause. Lilið folgte und
versuchte, ein ähnliches Bild von Selbstsicherheit abzugeben. Die
Wachen blickten ihnen mit skeptischen Gesichtsausdrücken entgegen und
standen auf.

Die Wache, die Lilið nicht kannte, sagte etwas auf Alevisch. Vielleicht,
dass sie hier nicht sein durften. Das könnte zu den Vokabeln passen,
die Lilið verstand.

Die Person neben ihr sprach in der selben, ihr fremden Sprache, wie
erwartet, aber mit einer Stimme, die ganz sicher nicht Drudes war.

Liliðs Herz schlug bis zum Hals und verhedderte sich dabei ein paar
Mal. Sie versuchte, ruhig zu atmen. Für einen Moment wagte sie zu
hoffen, dass Drude einfach dere Stimme erfolgreich sehr verstellt
hatte (etwa eine Oktave tiefer), aber ihr kam die Stimme bekannt
vor. Vertraut.

Die Wache antwortete, etwas streng und trotzdem gelassen und endete
auf eine Frage.

Die Gestalt in der Sakralutte, die nicht Drude war, antwortete knapp
mit einer Bestätigung. Heelem. Das war Heelems Stimme. Was machte
Heelem hier? Und wusste er von Drude? Was war passiert?

Die Wache seufzte tief, die andere murrte genervt, aber beide standen
auf und teilten Lajana auf Baeðisch mit, dass sie sie fesseln
würden, solange die beiden Sakrals-Dienenden den Raum reinigen
würden. Lilið hörte das Klicken der Fesseln von den Wänden abprallen. Dann
verließen die Wachen den Raum.

Wenn es Drude gewesen wäre, neben der sie wie vom Donner gerührt stand,
hätte sie schon wieder gedacht, alles liefe zu gut. Lilið realisierte,
dass sie mit Heelem seit dem Leuchtturm in einem Boot gesessen hatte. Es
war nicht Drude gewesen. Nun ergab auf einmal Sinn, dass Heelem steuern
gewollt hatte. Als Nautika konnte Heelem das sehr gut. Das hätte er nicht ihr
überlassen. Oder hätte er? Wusste er, dass sie Lilið war? War es doch
Drudes Abe gewesen und sie hatte sich nicht zu ihnen niedergelassen,
weil Drude ja gar nicht an Bord gewesen war?

Lilið verpasste die Möglichkeit, Heelem Bescheid zu sagen, dass sie
womöglich nicht war, wen er erwartete, weil er
in dem Moment durch den Raum schritt, in dem die Wachen die Türen
verschlossen hatten. Er verriegelte sie rasch von innen, eilte
dann zu Lajana und öffnete ihre Fesseln. Mit Magie. Lilið schluckte.

Dann brach das Chaos herein. Heelem schloss mit der verdatterten aber
geistesgegenwärtig folgenden Lajana im Schlepptau zu
ihr auf und deutete Richtung Tür, hinter der die Wachen lauerten
und gegen diese hämmerten. Lilið fragte sich, was sie tun
sollte. Sollte sie in Richtung jener Türen gehen? Das hielt sie für nicht
so sinnvoll, denn das Holz knackste bereits und gab nach.

"Mach Feuer!", rief Heelem ihr leise zu.

Also hatte Heelem nicht sie erwartet, sondern eine Person, die
Feuer machen konnte. Lilið wusste nicht, wie sie antworten
sollte, also schlug sie die Kapuze zurück. Heelem blickte
ihr entsetzt ins Gesicht. Er hatte wohl so gar nicht mit
ihr gerechnet.

Die Türen barsten auf und von allen Seiten näherten sich Wachen.
Einige rasten, in Geschwindigkeiten, die nur mit Magie möglich waren,
an ihnen vorbei, um sie einzukreisen und ihnen
den Weg zur Tür ins Freie abzuschneiden. Heelem machte
Bewegungen mit den Armen und Lilið war recht sicher, dass er sie
irgendwie auf Abstand hielt, sie daran hinderte, sich zu bewegen.
Die Wachen strauchelten und teils wurden sie weggedrückt, wie
von einer unsichtbaren Kraft über den Boden geschoben. Heelem war
mächtig, wenn er gegen sie ankam, aber hatte es eben auch
mit gut ausgebildeten Wachen zu tun und hatte damit gerechnet, dass
sie sie mit Feuer zurückhalten könnte.

Lajana stand still und voller Angst so dicht bei ihnen, dass
sie sie berührten. Heelem wurde von irgendeiner Wucht
zurückgedrängt, sodass sein halber Rücken gegen Liliðs stieß.

Lilið fing die Wucht mit einem Ausfallschritt ab und hinderte
Lajana am Fallen. Sie selbst hatte noch keine Welle eines
Angriffs erwischt und sie wusste nicht, warum sie
noch lebte. Schützte Heelem sie alle? Schützte Lajanas Anwesenheit,
weil sie nicht sterben durfte? Führten die Wachen daher nichts
allzu Gefährliches aus?

"Was machen wir jetzt?", raunte Heelem ihr zu.

"Konntest du nicht Herzen stehen lassen?", raunte Lilið
zurück. "Zum außer Gefecht setzen?" Das musste doch sogar
gehen, ohne zu töten.

"Deins vielleicht, weil du dich nicht wehren kannst." Heelems
Körper schlug abermals gegen ihren.

Wie freundlich, dachte Lilið. "Du kannst der Sakrale nichts
anhaben, weil sie Magie abweist? Anders als die Tür?", riet
sie.

"Exakt.", antwortete Heelem. "Ich kann höchstens eine Staubwolke
aus Teilmaterie erzeugen. Für ein paar Minuten Tarnung. Einmallig,
denn so viel loser Staub ist nicht im Material. Wenn
du eine Idee hast, was du damit machst, mache ich das."

"In Ordnung! Mach das!" Hoffentlich war das die richtige
Entscheidung. Sie griff Lajana am Arm, vermutlich nicht wenig
schmerzhaft, und zog sie in eine Umarmung. "Lajana, ich werde dich
in eine Ratte falten und dich in Heelems Tasche stecken, ja?", flüsterte
sie in ihr Ohr. Nicht einmal Heelem würde sie hören können.

Sie nahm das Weinen in Lajanas Stimme wahr, als diese antwortete: "Ich
will nicht, dass dir was passiert!"

Was sollte sie antworten? Es war fast unmöglich, dass ihr nichts
passierte. "Ich werde mir Mühe geben. Ich würde dich ungern falten,
ohne dass du es mir erlaubst."

Sie fühlte den feinen Sandstaub auf der Haut, der aufwirbelte, als
Heelem die Sakrale zum Erzittern brachte. So wenig Zeit.

"Faltest du dich mit?", fragte Lajana.

Lilið hatte darüber nachgedacht. Aber wenn Heelem jetzt
gerade nicht im Stande war, mit
ihnen zusammen zu fliehen, dann brauchte es ein
Ablenkungsmanöver. Und dazu konnte Lilið nicht auch Teil der Ratte
sein. "Ich versuche, nachzukommen.", flüsterte sie. "Darf ich? Es
eilt."

"Ja.", war die zaghafte Antwort.

Lilið hasste, dass sie Lajana dazu emotional unter Druck gesetzt
hatte. Aber wie könnte sie auch im Kampf ethisch korrekt
agieren?

Sie faltete Lajana, liebevoll, in der Umarmung, in der sie noch
verweilten. Sie hatte Lajana vermisst. Sie liebte ihre Königin. Die
einzige, die sie je akzeptieren würde. Die einzige, für die sie
je eine Monarchie verzeihen würde. Selbst wenn Marusch an Stelle
von Lajana wäre, sie würde sie nicht als Königin wollen. Nur
Lajana.

Sie steckte Lajana, nun in Rattenform, von hinten in eine Innentasche
von Heelems Sakralutte. Sie war erstaunlich leicht, vielleicht
sogar leichter, als Ratten normalerweise waren. "Ich
stecke dir Lajana in die Tasche.", flüsterte
sie ihm zu. "Nicht erschrecken, wenn du mich siehst. Ich
hoffe, das alles bringt genug Chaos, dass ihr wegkommt. Ich
hoffe, ich bin lange genug sicher, dass ich euch folgen
kann." Dann nahm sie Abstand von Heelem und ging Richtung Stuhl, auf
dem Lajana gesessen hatte. "Lasst sie gehen!", brüllte sie durch
den Raum. Ihre Stimme fühlte sich anders an, aber ob sie wirklich
nach Lajanas klang, da war sie sich nicht ganz sicher. Vielleicht
spielte ihr in die Hände, dass Lajana wahrscheinlich nie laut
gesprochen hatte, sie aber
jetzt schon, die Umstehenden also auch eine nicht vertraute
Stimmlage erwarten würden. "Ich ergebe mich, aber lasst sie gehen."

Der Staub löste sich in einem Wind auf, den vermutlich
eine der Wachen verursacht hatte. Heelem war in der kurzen Zeit
weiter an den Rand des Raums gelangt, wo die Tür auf die Terrasse
führte, als Lilið vermutet hätte. Gut so.

"Wo ist der dritte?", schrie eine der Wachen auf Baeðisch.

Andere wiederholten es auf Alevisch, vermutete Lilið. Neben sie
stellten sich zwei Wachen, packten sie an den Handgelenken
und ließen sie nicht aus den Augen. Einige
der Wachen suchten den Raum ab, eine verließ sogar den Raum durch die
zerstörte Tür, um Lilið zu suchen. Weil ja nun eine Person fehlte, eine
in einer Sakralutte. Und Lilið nicht nach der Person aussah, die
fehlte.

Heelem sah sich nur noch mit zwei Wachen konfrontiert. Das
war, was Lilið zu erreichen gehofft hatte. Konnte er es
schaffen? Sie zitterte am ganzen Leib, was sie nun als Lajana auch
durfte. Sie hoffte, dass sie das Gesicht erfolgreich Lajanas angepasst
hatte. Die Faltung war nicht einfach gewesen, aber sie hatte Lajana
im Arm gehabt und sie so sehr gefühlt, dass sie glaubte, alles
in der Faltung übernommen zu haben, was wichtig war. Vielleicht
sogar die Stimmbänder. Die Hautfarbe passte wahrscheinlich nicht, aber
im verzerrenden Licht der Sakrale spielte das keine so große
Rolle. Jedenfalls schienen die Wachen darauf hereinzufallen.

Der Kampf zwischen Heelem und den beiden Wachen schien
ausgeglichen. Heelems Sakralutte flog um ihn herum, wenn er
sich mal der einen, mal der anderen zuwandte. Es reichte nicht, dachte
Lilið. Sie trat nach dem erstbesten sakralen Gegenstand, den sie
mit den Füßen erreichen konnte, sodass er geräuschstark über
den Boden kullerte. Sie hatte keine Ahnung, was das für ein
Gegenstand war. Als Belohnung bekam sie einige Momente die
volle Aufmerksamkeit. Genügend, dass Heelem nach einem letzten,
entsetzten Blick auf sie, die Flucht ergreifen konnte, die Flügeltür
ins Freie öffnete, und von der Terrasse sprang. Gut zwölf Meter
in die Tiefe, schätzte Lilið. Unangenehm, aber überlebbar. Und ab
dort? Die Türen fielen wieder zu, bevor Lilið ein Platschen
hätte hören können. Würde er es schaffen? Und wie lange würde
es brauchen, bis aufflog dass sie nicht Lajana war.
