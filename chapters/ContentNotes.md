### Anmerkungen zu den Content Notes

Ich versuche hier eine möglichst vollständige Liste an Content Notes
zur Verfügung zu stellen, aber weiß, dass ich nicht immer alles
auf dem Schirm habe. Hinweise sind willkommen und werden ergänzt. Über
die Content Notes hinaus darf mir gern jede Frage nach Inhalten gestellt
werden und ich spoilere in privaten Konversationen nach bestem
Wissen. Es bedarf dafür keiner Begründung oder Diskussion. Ich mache das
einfach.

Ich nehme außerdem teils sehr seltene Content Notes für Personen mit auf, die
ich kenne, weil sie sich für meine Kunst interessieren.

### Für das ganze Buch

**Zentrale Themen:**

- Schlafmangel.
- Body-Horror (eine Magieform, bei der sich Körper verändern.)
- Ratten.
- Stehlen.
- Betrügen.
- Transfeindlichkeit, Cis- und Binär-Normativität.
- Damsel in Distress - eine zentrale, behinderte Frau wird über einen längeren Zeitraum gefangen gehalten.
- Sanism, insbesondere häufige Verwendung des Wortes "dumm".
- Religion vielleicht besonders anspielend auf katholische Kirche und Rituale.
- Reden über Krieg, sehr abstrakt
