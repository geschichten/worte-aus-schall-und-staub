Einen guten Draht haben
=======================

*CN: Eifersucht, internalisierte Ace-Feindlichkeit, reden
über ein Massaker, unangenehme Gefühle am Auge, Misgendern,
Gaslighting ?, toxische Beziehung, emotionaler Missbrauch ?,
BDSM, Knien, Dominition/Submission, Befehle.*

Heelem schickte Lilið und Lajana zu Bett (beziehungsweise zu
Koje), noch bevor sie Oesteroge erreichten. "Ihr seid sehr erschöpft
und außerdem sind eure Gesichter öffentlich bekannt.", erklärte
er. "Wir haben inkognito vielleicht ein paar Vorteile, wenn wir
im oesteroger Hafen Hilfe brauchen."

"Maruschs Gesicht ist nicht bekannt?", fragte Lilið.

Marusch schüttelte den Kopf. "Ich bin in der Zentral-Sakrale das
erste Mal ohne Schleier vorm Gesicht in der Öffentlichkeit
gewesen.", erklärte sie.

Und es hat außer uns niemand überlebt, ergänzte Lilið in Gedanken.

Lajana stieg vor ihr die Treppe hinab und hielt sich bereit, um
Lilið aufzufangen, sollten ihr wieder die Kräfte versagen. Lajanas
Sorge war nicht unbegründet. Lilið war einfach fix und fertig mit
allem.

"Teilen wir uns die Koje?", fragte Lajana.

Lilið überlegte, dass sie längst schlafen würde, wenn Marusch käme,
also nicht so viel davon hätte, wenn sie eine Koje mit Marusch
teilte. Sie nickte. "Wenn du möchtest, gern."

Sie zog sich nicht einmal um, bevor sie sich auf die halbwegs weiche Unterlage
legte. Lajana zog mit einigen kräftigen Zügen die Decke unter Lilið
hervor und deckte sie zu. Wie, als wäre Lilið ein Kind, und so fühlte
sich Lilið gerade auch. Irgendwo von oben drangen vertraute Stimmen
nach unten. Sie konnte keine Worte aber schon Gefühle darin
ausmachen. Sie fühlte sich in die Zeit als Kind zurückversetzt, in der sie
die Stimmungen unter den Erwachsenen beim Einschlafen als Einschlafmusik
wahrgenommen und ohne jene nicht schlafen gekonnt hatte. Die
gelassene Gemütlichkeit und die Vertrautheit ihrer Eltern, wenn Lilið
in einem benachbarten Raum bei geöffnerter Tür einschlief. Lord
Lurch und ihre Mutter liebten sich sehr. In Liliðs müdem Schädel
wirkten diese zwei Erkenntnisse inkompatibel wie Öl und Wasser, dass
sie so zart miteinander waren, aber sie ihren Vater politisch
zunehmend kritischer sah. Auf eine Weise, dass sie unignorierbare
Abscheugefühle entwickelte. Innerhalb des Systems verhielt er sich
vielleicht überdurchschnittlich positiv, aber er hätte es in der
Hand, das System mehr zu durchbrechen. Und er tat es nicht, weil
er sein Ansehen nicht verlieren wollte, seinen Status nicht riskieren
wollte. Er hinterfragte nur, was zu hinterfragen für ihn nicht
anstrengend würde.

Sie sollte schlafen, ermahnte sich Lilið. Mehr auf die wohligen
Gefühle des Jetztes achtend. Aber auch die Stimmung, die sie
von ihrer Crew wahrnahm, war etwas eingetrübt. Überraschenderweise
lag es nicht an Allil sondern an Drude. Wann immer Lilið
Drudes Stimme hörte, bohrte etwas in ihr. Da war eine Fremdheit
oder eine Traurigkeit im Klang. Lilið wusste es nicht genau.

Sie schlief ein, aber als die Teeseufel anlegte, bekam sie es doch
wieder mit. Der Seegang im Hafen war ein anderer. Sie nahm Heelems
leise Befehle wahr. Marusch und Drude bargen das Restsegel,
nachdem sie die Teeseufel auf einem freien Hafenliegeplatz vertäut
hatten.

Drude sagte den anderen dreien 'Gute Nacht', als alles getan war,
und ging allein unter Deck. Lilið tat vielleicht das mühsamste, was
sie seit Tagen getan hatte: Obwohl keine Lebensnotwendigkeit oder
etwas in der Art dafür bestand, kroch sie, endlos erschöpft, wie
sie war, aus der Koje. Lajana gab ein fragendes Geräusch von
sich. "Ich komme gleich wieder.", versprach Lilið.

Sie schleppte sich vor die Tür und lehnte sich von außen daran, stand
Drude gegenüber. Hatte Drude damit gerechnet? Das wäre nicht
unwahrscheinlich, weil dey immer alles spürte. "Was ist los
mit dir?"

"Du lässt dir echt keine Zeit damit, mich mit der Frage zu
konfrontieren."

Lilið konnte nicht ausmachen, ob Drude ungehalten oder albern
sarkastisch war. Sie nahm vorsichtshalber ersteres an. "Es tut
mir leid." Tatsächlich wusste sie nicht einmal, ob Drude
es ironisch meinte. Ob Lilið jetzt mit einer anderen Einleitung
hätte anfangen sollen oder ob Drude Liliðs Frage schon viel
eher erwartet hätte. Aber es spielte gerade keine Rolle.

"Kein Ding.", sagte Drude und trat näher an Lilið heran. Dey
seufzte schwer und tief. "Ich bin eifersüchtig, glaube ich. Ich
will nicht eifersüchtig sein, aber ich bin es."

"Auf mich und Marusch?", riet Lilið.

"Genau." Drude atmete noch einmal ein und ließ die Luft
seufzend entweichen. "Die Gefühle sind Unfug. Ich habe auch
eine Menge Gedanken, die wahrscheinlich auch Unfug sind. Willst
du sie trotzdem hören?"

"Ja." Lilið war fix und fertig, aber um Drudes Gefühle zu sortieren,
musste noch Zeit und Kraft da sein. "Gib mir all die Gedanken, wenn du
sie teilen magst!"

"Ich bin nicht interessiert an Sex. Das hast du schon herausgefunden,
glaube ich. Du hingegen magst Sex. Marusch auch.", führte Drude aus. "Ich nehme
wahr, wie sehr du ein Interesse an Nähe zu Marusch hast. Ich
biete dir nicht dasselbe. Ich habe Angst, deshalb unwichtig für
dich zu sein."

"Drude, du bist mir so unbeschreiblich wichtig!" Lilið achtete
nicht darauf, auf Lajana Rücksicht zu nehmen und leise zu
sprechen. "Du bist mir so wichtig, dass ich meinen Körper dazu
gezwungen habe, jetzt noch einmal für dich aufzustehen. Hm, das
klingt irgendwie nicht nach viel, aber es ist viel." Lilið fühlte
sich nicht überzeugend genug.

"Ich weiß.", sagte Drude einfach. "Wenn du dich nicht um mich gesorgt
hättest, dann hätte ich mich gar nicht erst getraut, mit dir darüber
zu reden."

Lilið fühlte, wie ihr die Tränen kamen. Weil sie für Drude da sein
und demm diese Last abnehmen wollte. Sie fragte leise: "Würde es dir
helfen, wenn ich meine Gefühle für dich und Marusch etwas
aufdrösele? Damit du weißt, was Sache ist?"

"Ja, bitte." Drude klang fast kleinlaut. Dey setzte sich auf den
Boden neben Lilið und lehnte sich an die Tür. Irgendwo aus
der Dunkelheit sprang plötzlich die Abe auf sie zu und schmiegte
sich an demm.

Lilið ließ sich ebenfalls nieder. "Du hast wohl recht, dass ich zu
Marusch eine ziemlich starke, romantische und körperliche Anziehung
habe. Vielleicht sexuell, aber vielleicht sogar eher vor allem
für leidenschaftliche Zärtlichkeit. Ich habe keine Ahnung, wo die
Grenze ist. Das habe ich in der Form nicht für dich. Aber das
hielte ich angesichts der Tatsache, dass du kein sexuelles Interesse
an mir hast, auch eher für nicht hilfreich.", erklärte sie. "Meine
Gefühle zu dir und zu Marusch sind sehr verschieden, aber beide
sehr stark. Die zu Marusch sind gerade vielleicht oberflächlich
sehr präsent. Aber nichts davon wird dazu führen, dass du mir
unwichtig werden würdest." Lilið hatte das Gefühl, sich gedanklich
zu verheddern. Sie hätte vielleicht gern einen glatteren Übergang
zu dem gehabt, was sie nun hinzufügte: "Wir haben eine Sache mit
Fetischen ausprobiert. Das ist etwas, was ich gern wiederholen möchte,
wenn du willst. Und ausbauen. Das ist etwas, was ich mir so nicht
mit Marusch vorstellen kann. Hilft dir das?"

"Es hilft.", sagte Drude.

Es klang in Liliðs Ohren noch eingeschränkt. Aber sie gab
Zeit, bohrte nicht nach.

Drude seufzte noch einmal. "Ich klinge wahrscheinlich sehr undankbar
oder so, wenn ich frage: Wenn du die Wahl hast, mit wem du einfach nur
kuschelst, würdest du dich je für mich entscheiden? Und es wäre halt
völlig in Ordnung, wenn du 'nein' sagst. Außerdem ist vielleicht nicht
in Ordnung, wenn ich die Frage stelle, weil es dich unter Druck setzt. Wie
ich dich kenne, würdest du jetzt direkt fragen, ob wir eine Koje teilen
wollen."

Lilið fühlte sich erwischt. "Mit mir und Lajana hätte ich
vorgeschlagen, stimmt."

"Weißt du, Lilið, wenn du lieber mit Marusch kuscheln
wolltest, dann müsste ich das halt aushalten, egal welche
Gefühle das auslöst.", erklärte Drude. "Ich muss dann einen Weg
finden, mit meiner Eifersucht klarzukommen. Ich muss ihre Ursachen
suchen. Die sind gar nicht so schwierig. Ich war einfach immer allein. Und
nun habe ich Angst, dich zu verlieren. Aber wenn du gehen müsstest, dann
wäre das eben so."

"Ich gehe aber nicht.", hielt Lilið fest.

"Ich weiß.", flüsterte Drude. Dey fügte wieder in normaler Murmellautstärke
hinzu: "Aber ich möchte nicht klammern. Nur weil ich ein Bedürfnis
habe, dich sehr eng bei mir zu haben, weil ich nie jemanden hatte, heißt
das nicht, dass du das leisten müsstest oder dass ich ein Recht darauf
hätte. Das sind keine perfekten Worte. Weißt du, was ich meine?"

"Du hast ein Bedürfnis, ununterbrochen mit mir zusammen zu sein?", fragte
Lilið verwundert.

"Das ist auch wieder kompliziert.", antwortete Drude. "Gerade habe ich
das Bedürfnis dazu. Aber manchmal will ich allein sein, und dann streiten
sich zwei widersprüchliche Bedürfnisse in mir. Aber mein größtes Bedürfnis
ist, dir Raum zu geben und dich du sein zu lassen."

Liliðs Inneres schnürte sich zusammen. "Fosh, ich habe dich einfach schon
sehr lieb.", sagte sie. "Du bedrängst mich nicht."

Die Tür in ihrem Rücken drückte sich gegen sie, also rollten sich
Lilið zur Seite und Drude stand auf, die Abe im Arm. "Kommt ihr
zwei schlafen?", fragte Lajana. "Entschuldigung, ihr drei. Marusch
schläft eh mit Allil in einem Bett, weil Marusch sie nicht aus
den Augen lassen wird. Also kuschelt miteinander und mit mir. Dass
ihr kuscheln wollt, ist doch klar, aber wenn ihr es einfach tut,
merkt ihr dann schon, wie doll ihr das wollt oder nicht. Das erspart
das Diskutieren."

Lilið kicherte. Völlig unrecht hatte Lajana vielleicht nicht.

"Das funktioniert so nicht.", gab Drude entgegen Liliðs Gedanken
zu verstehen. "Aber lasst uns das trotzdem tun und morgen darüber
reden."

"Und darüber, dass meine Gefühle nicht falsch sind.", fügte
Lilið hinzu.

"Oh!", machte Drude. Ein Lächeln huschte ihr über das Gesicht. "Das
hilft mir gerade. Denn meine Gefühle, auch wenn ich sie da nicht
haben will, sind auch eben einfach da und nicht falsch."

---

Sie schmiegten sich also zu viert in die viel zu Enge Koje. Es
war zu eng, um sich umzudrehen, ohne sich mühsam zwischen Körpern
auszufädeln und wieder hineinzuschmiegen. Lilið fragte sich, ob
sie irgendwann aufwachen würde, weil sie viel zu lang auf der
einen Seite gelegen hätte, und sich nicht umdrehen könnte, weil
sie sonst die anderen wecken würde. Egal.

Bevor sie endlich wieder einschlief, hatte sie noch eine
Erkenntnis. Drude hatte Eifersuchtsgefühle, die Lilið zwar
nicht kannte und die Drude nicht wollte, aber sie waren nun
einmal einfach da. Es bestimmte nicht Drudes Handeln. Oder
es hatte vielleicht bewirkt, dass dey sich vorübergehend
zurückgezogen hatte, aber durch das, was Drude sagte, war
Lilið absolut klar, was Drude für sie wollte, deren Gefühlen
zum Trotz. Sie könnten jetzt gemeinsam überlegen, was sie
tun könnten, um mit den Gefühlen zu arbeiten. Sie hatten
zum Beispiel geredet. Aber Drude hatte nicht nur Eifersuchtsgefühle,
sondern auch und vor allem Werte oder Wünsche, die dere Handlungen
bestimmten. Lilið nahm Drude dere Eifersuchtsgefühle nicht
übel. Warum auch?

Das ließ sich tatsächlich auf Liliðs Gefühle bezüglich des
Massakers übertragen. Sie hatte es ästhetisch
schön empfunden, was passiert war. Sie hatte ein Gefühl
von Entspannung und Auflösung in sich. Die Gefühle bestimmten
aber nicht ihre Handlungen. Sie waren erst einmal einfach
da. Sie konnte sich anhand ihrer Werte und Wünsche Gedanken
machen, wie sie handeln wollte. Wenn sie in dem Gewusel
mal für eine Sache sich überraschend unpassned anfühlende
Gefühle hatte, dann war das nicht schlimm. Sie musste sich
das nicht übelnehmen. Das unpassend anfühlen war auch ein
Gefühl, das ihr sagte, was sie eigentlich für ein Mensch war.

Nun fingen ihre Gedanken wieder an, zu detailliert zu werden,
aber die Erkenntnis darunter war angenehm entspannend und
sie konnte endlich einschlafen. In Löffelhaltung: Sie umarmte
Lajana von hinten und Drude sie. Irgendwo zu ihren Füßen
räkelte sich manchmal die Abe. Das war schön.

---

Erst zwei Tage später war Lilið wieder so ausgeruht, dass sich
ihr Körper wie ein von ihr kontrollierbares Gebilde anfühlte. Sie
saßen beim Frühstück, oder viel eher beim Spätstück, im Heck der
Teeseufel. Der Mast war immer noch abgeschnitten. Sie hatten einen
Termin mit der Werft am frühen Abend, wo er dann komplett ausgetauscht
werden würde. Heelem hatte das alles organisiert. Sie wollten
sich nicht mit Marken neue Lebensmittel und Reparaturen beschaffen,
wenn sie hier für länger lagen, weil ihre Marken nahegelegt hätten, dass
die Kronprinzessinnen an Bord wären. Lilið hätte zwar vielleicht Marken
fälschen können, aber zum einen hätte sie dafür auch eine Vorlage
gebraucht und zum anderen war Lajana dagegen, dass sie betrögen, wenn
es andere Möglichkeiten gäbe.

Heelem war als Nautika bekannt und ihm wurde einfach so von den anderen
Nautikae geholfen werden, wann immer Ressourcen frei wären. Sie hatten
Lebensmittel bekommen und einen Termin für die Reparatur, der gar nicht
so weit in der Zukunft lag.

Lilið hatte Lajanas und ihr eigenes Gesicht gefaltet, wie immer,
wenn sie von Fremden gesehen werden könnten. Lajana war es unangenehm. Lilið
verstand das. Auch wenn sie es nicht aussprach, sie empfand es genau
so. Die Falten im Gesicht (keine, die wie schöne Alterungsfalten
aussahen) klemmten, fühlten sich immer etwas falsch an, schlimmstenfalls
wie eine Wimper, die sich im Auge verhakt hatte.

"Marusch, was ich die ganze Zeit vor mir herschiebe, zu fragen:", leitete
Lilið ein Gespräch ein. "Ist das Buch eigentlich wieder an Ort und Stelle? Habe
ich je wieder die Möglichkeit, einfach Lilið zu sein und nicht verfolgt
zu werden?"

<!--Wenn über Liliðs Mutter reden, dann wohl hier.-->
Marusch rollte mit den Augen. (Das tat sie selten.) "Vorsicht, Zynismus.", sagte
sie. "Also, das Buch ist zurück. Muddern wurde über das angebliche
Missverständnis in Kenntnis gesetzt, dass es eigentlich nie weggewesen
wäre. Sie hat das geschluckt. Sie hat deshalb deinen Vater, Lord Lurch,
entlastet, aber meinte, bei dir wäre es besser, vorher zumindest
noch ein Gespräch zu führen, um sicher zu gehen, dass von dir nichts
Schlimmes zu erwarten wäre."

"Lilið hat immerhin auch angeblich eine Kriegskaterane der Königin
versenkt.", wandte Drude ein.

"Muddern hat das so beschlossen, bevor das passiert ist. Bevor
sie überhaupt die Kriegskaterane losgeschickt hat.", korrigierte
Marusch.

Lilið hörte die Bitterkeit in Maruschs Stimme, aber hatte trotzdem
das Gefühl, nicht alles zu verstehen. "Was
genau daran ist nun der Zynismus?"

"Was dahintersteckt, ist eigentlich, dass sie mit Lord Lurch einen loyalen
Schutzbefohlenen mit Macht hat, der tun wird, was sie möchte.", erklärte
Marusch. "Während das Buch verschwunden war, hatte sie einen Vorteil
davon, dass er unter Druck steht. Nun hat sie einen Vorteil davon,
dass er sich sicher fühlt. Also entlastet sie ihn. Sie hat
aber keinen Vorteil davon, dass du in Sicherheit
wärest. Daher möchte sie dich entweder beseitigt
oder mit dir ein Gespräch geführt haben, in dem sie dich auf
taktisch sinnvolle Art unter Druck setzt, zu handeln, wie sie es
will. Solltest du mal den Hof und das Gefolge von Lord Lurch
übernehmen, was als uneheliches Kind ohnehin in den Sternen
steht." Marusch gnarfzte. Zumindest hätte Lilið das Geräusch
so beschrieben. "Sie will die Macht der Königsfamilie Stern
sichern. Und dazu benutzt sie erklärende
Worte, die alles, was sie tut, im Rahmen von Gesetz
und gesellschaftlicher Wahrnehmung rechtfertigen,
die aber nur nachträglich draufgestülpt werden und nichts, gar
nichts, mit den Entscheidungen zu tun haben, die sie fällt. Sie
ist so unehrlich."

"Hm. Ich verstehe.", sagte Lilið. Sie merkte, wie Wut in ihr
aufsteigen wollte, aber sie zu müde für diese Art Gefühle war. Die Wut drang
nicht durch. "Fazit: Ich habe nur dann eine Möglichkeit, mich
irgendwann wieder ohne Gefahr für mein Leib und Leben als Lilið von
Lord Lurch öffentlich zu zeigen, wenn ich bei ihr persönlich vorstellig
werde und ihr gefalle. Und die Wahrscheinlichkeit ist hoch,
dass mir das nicht bekommt. Also, so weiterlebens-technisch."

Marusch nickte und machte dazu noch ein bestätigendes Geräusch, dem
es auch nicht an zynischem Beiklang mangelte.

Dann schwiegen sie eine Weile. Immerhin war ihr Vater jetzt nicht
mehr in Gefahr. Aber ähnlich wie die Wut eben drang nun Freude
darüber nicht durch. Wobei: die Freude wollte auch nicht einmal
entstehen. Sie dachte gerade nicht besonders positiv über ihn. Was
er wohl über sie denken würde? Das eigene Kind eine schwer berüchtigte
Diebesperson?

Lajana unterbrach das Schweigen. "Ich weiß nicht, ob ich zurück zu Mama
will."

Aus Maruschs Blick, der auf Lajana haftete, schloss Lilið, dass sie
nicht damit gerechnet hatte.

Lajana sah zurück, nahm die Beine mit auf die Bank und wirkte
unsicher. "Du willst doch eigentlich auch nicht zurück, oder?", fragte
sie. "Du würdest nur für mich mitkommen."

"Ich werde immer für dich mitkommen.", betonte Marusch sanft. "Du
wolltest bisher immer wieder zurück. Und nun frage ich mich, warum
sich das ändert. Rede ich zu schlecht über sie?"

Lajana schüttelte den Kopf. "Ich habe Mama immer noch lieb.", sagte
sie. "Aber sie ist gemein zu dir. Sie ist gemein zu Lilið. Und du
sagst, sie wäre gemein zu mir. Das stimmt irgendwie, aber ich dachte
immer, sie kann nichts dafür und sie hat recht mit dem, was sie sagt. Aber
nun bin ich hier mit euch und fühle mich das erste Mal normal. Oder
in Ordnung, so unnormal wie ich bin. Mit fast siebenundzwanzig." Lajana schloss die
Arme sehr fest um die eigenen Beine, ehe sie zu ihren Knien weitersprach.
"Ich wollte Königin werden, seit ich vierzehn bin oder so. Weil eine
Königin entscheiden kann, dass es Menschen gut geht, und ich so
entscheiden würde. Ich hatte immer Zweifel, ob ich es kann. Meine
Mama hat mir immer gesagt, dass ich es nicht kann, und
du, dass ich es vielleicht doch kann. Und ich weiß nicht, woran ich glauben soll. Aber
ich weiß, dass ich will. Also, dass ich gute Entscheidungen für
die Menschen treffen will. Das wollen alle anderen nicht einmal. Außer
du vielleicht, aber du willst nicht regieren. Das macht mich, selbst
wenn ich es nicht kann, zur besten Wahl."

"Absolut!", warf Lilið ein. Sie war nicht allein mit ihrem zustimmenden
Kommentar und zu ihrer Überraschung nickte selbst Allil und hob einen
Daumen.

Lajana aber lächelte nicht einmal, nahm es kaum zur Kenntnis. "Ich
dachte immer, wenn ich Königin werden will, muss ich zurück zu Mama.", fuhr
sie fort. "Und ich habe sie eben auch lieb. Sie ist kein grundschlechter
Mensch. Sie hat halt Fehler, aber das darf man ihr nicht sagen, sonst
wird sie wütend." Lajana blickte auf und sah ihnen der Reihe nach
ins Gesicht. "Ich habe Angst, wieder bei ihr zu sein.", sagte
sie eindringlich. "Ich stelle mir vor, wie Lilið dabei ist
und wie sie dann zu Lilið ist. Ich will das nicht. Ich habe Angst. Mir
wird schlecht, wenn ich darüber nachdenke. Und ich will mich nicht
mehr minderwertig fühlen." Lajana schrie den letzten Satz fast. Irgendwelche
Leute zwei Liegeplätze weiter auf der anderen Seite des Stegs, die ansonsten
wohl kein Wort verstanden aber nun die Wut wahrnahmen, blickten kurz herüber.

Lajana weinte, und sie war nicht allein damit. Lilið konnte auch nicht
an sich halten und als sie sich umblickte, stellte sie fest, dass
auch Heelem weinte. Marusch wirkte eher wie zu Eis erstarrt.

Drude hob die Hand, -- das abgesprochene Zeichen dafür, dass sie
nun nicht mehr reden sollten, weil eine Person zu dicht wäre oder
sich näherte. Ausgerechnet jetzt. Lilið blickte den
Steg entlang. Eine Person in Uniform,
die jene als im Hafen arbeitend kennzeichnete, schritt über den
schwankenden Steg und hielt auf sie zu. Kam jetzt eine Beschwerde,
dass sie zu laut wären? Hatte ihr Gespräch sie trotz Drudes
Aufpassen verraten?

Heelem näherte sich dem Heck und hob die Hand zum Gruß.

"Hallo!", rief die fremde Person. Sie wollte tatsächlich zu ihnen
und blieb vor ihrem Heck stehen. "Oh, ich störe! Das tut mir leid.
Soll ich später wiederkommen?"

Wahrscheinlich schloss sie das aus Heelems verweinten Gesicht, mutmaßte
Lilið.

"Worum geht es denn?", fragte Heelem.

"Du bist Heelem, Nautika, ehemals im Dienst der Garde von
Königin Stern, richtig?", erkundigte sich die Person.

Heelem nickte. "Genau der."

"Ich habe hier einen Brief von Königin Stern. Eine Kopie von vielen. Er
soll an ihre Kinder ausgehändigt werden und es heißt, du hättest einen
guten Draht zum Kronprinzen." Die Person hielt eine Rolle Papier mit einem
Siegel hoch.

Lilið grinste. Wenn es viele Kopien gab, dann war ein Siegel nicht sehr
viel wert. Es verhinderte nicht, dass eine der Kopien von dritten gelesen
würde, während eine andere Kopie unbeschadet bei Marusch oder Lajana
ankäme. Außerdem grinste sie, weil beide Kronprinzessinnen mithörten und
die Hafenperson es nicht wusste.

"Ich kann schauen, was sich einrichten lässt.", sagte Heelem und streckte
die Hand aus. Der Blick, den er mit Marusch wechselte, wäre Lilið fast
entgangen. Er fügte hinzu: "Kronprinzessin. Das zweitgeborene Kind der
Königin ist auch eine Prinzessin."

Die fremde Person runzelte die Stirn, "äh", entrunzelte sie wieder
und sagte: "Du wirst es wohl besser wissen als ich."

"Genau.", bestätigte Heelem.

Die Person verabschiedete sich wieder und Drude gab ein weiteres Zeichen, als
sie wieder außer Hörweite war.

Eine kühle Windböe trocknete Liliðs Tränen. Das war ein seltsamer
Morgen.

Marusch hielt Heelem die Hand hin und Heelem legte wortlos die
Briefrolle hinein.

"Briefe von ihr machen immer nicht so gute Dinge mit dir.", merkte Lajana
an.

Lilið konnte fühlen, wie Recht Lajana hatte, noch bevor Marusch das
Siegel brach. Marusch flammte nicht, aber aus ihrer Körperhaltung
sprach eine undurchdringliche Härte wie Diamant. Lilið hatte sie
vielleicht noch nie so gesehen.

"Du kannst ihn auch vorm Lesen verbrennen.", schlug Heelem vor. Er
wirkte unpassend belustigt.

Marusch zerbrach das Siegel, ohne darauf einzugehen, und entrollte
das Papier.

"Oder Lilið fragen, ob sie etwas Makaberes daraus faltet.", fügte Heelem
dazu.

Warum tat er das? Lilið hätte sich an Maruschs Stelle wahrscheinlich
nicht ernst genommen gefühlt. Vielleicht provoziert.

Marusch seufzte. "'Meine lieben Kinder'", las sie vor. "Dass es das
erste Mal ist, dass ich von ihr einen Brief lese, der nicht damit
einleitet, mich falsch zu geschlechten, verspricht nichts Gutes."

"Nicht?", fragte Lajana zaghaft.

"Sie gibt sich immer dann Mühe, wenn sie entweder was von mir will,
was in ihren Augen extrem wichtig ist, was ich aber nicht will, oder
wenn sie mir später üble Kritik reinbrettern will.", antwortete
Marusch. "Und wenn ich dann nicht brav jedes Fitzelchen Kritik
annehme und sage, dass es mir leid tut, oder nicht mache, was sie
will, dann sagt sie, sie habe sich doch bemüht, also könne sie von
mir doch erwarten, dass ich das wenigstens ein bisschen ebenso täte."

Lajana nickte. "Das stimmt.", sagte sie. "Aber dass sie sich bemüht,
ist trotzdem gut."

Marusch wechselte mit Lajana einen langen Blick und nickte
schließlich. "Schon."

"Ich will nicht, dass du so leidest.", sagte Lajana.

"Ich wünschte, ich könnte es verstecken." Maruschs Stimme war ruhig
wie vor einer Explosion. Sie atmete nicht. Und dann reichte sie
den Brief an Heelem zurück.

Damit der Brief nicht mit ihr in Flammen aufginge, dachte Lilið.

"Was brauchst du?", fragte Heelem. "Makaberer Humor meintest
du sonst oft, aber entweder, ich treffe nicht den richtigen
Ton, oder es ist heute nicht das Richtige."

"Ich weiß nicht, was mit mir los ist.", antwortete Marusch. "So
schlimm ist es sonst nicht."

"Es ist, weil ich sie nicht mehr brauche.", wusste Lajana.

Lilið fühlte plötzliche starke Liebesgefühle diesem Geschwisterpaar
gegenüber und unbekannterweise Hass auf diese Mutter, die sie immer
für eine brauchbare Option als Königin gehalten hatte. Sicher nicht
so gut wie Lajana, aber die Seiten, die sie nun kennen lernte..., mit
jenen hätte sie nie gerechnet.

Marusch nickte. Sie atmete schwer. "Ich weiß ziemlich genau, was sie will.",
sagte sie. "Es wird irgendein großes Treffen der Mächtigen geben, weil meine
Aktion von ihnen so viele entfernt hat, dass sie jetzt ein neues
Gerüst bauen müssen. Sie will sich im Vorfeld mit uns treffen und
mich mal wieder überreden, dass ich regiere. Sie wird es im Brief
anders verpacken, weil er von zu vielen Leuten gelesen werden
kann, aber darum wird es gehen. Und von dir, Lajana, will sie natürlich,
dass du endlich deinen Verzicht auf Regentschaft aussprächest."

Heelem las den Brief ohne zu fragen und nickte. "Auf Mazedoge.", sagte
er. "Die Versammlung des Bundesorakels mit Monarchie und Hochadel soll
in vier Tagen dort stattfinden, aber ihr sollt am besten schon vorher
am Versammlungsort auftauchen. Und euch möglichst zügig melden, wann ihr da
seid. Die Königin wünscht sich mit euch im Vorfeld eine
Audienz. Sie ist ab morgen dort. Oh, interessant
ist noch: Wenn du, Marusch, zufällig mit dem Blutigen Master M in Kontakt
stündest, mögest du sie mitbringen."

Marusch schnaubte, sagte aber nichts dazu. "Danke, dass du mir das
Lesen abgenommen hast.", sagte sie. "Lajana, möchtest du auch lesen?"

"Wenn sie so wie immer schreibt, verstehe ich so wie so nichts.", lehnte
diese ab.

Heelem warf noch einen Blick auf das Papier. "Tut sie.", bestätigte
er.

"Ihr kennt euch wirklich lange, oder?", fragte Drude.

Heelem lachte. "Ja! Ich bin im Alter zwischen Marusch und Lajana und
wir haben den größten Teil unserer Kindheit miteinander verbracht.", berichtete
er. "Damals mochte ich sie noch. Eure Mutter, meine ich. Ihre Gewalt
ist für Außenstehende ziemlich kodiert. Das ist alles äußerst
unlustig."

Allil räusperte sich. Sie wartete, bis alle Blicke auf sie gerichtet
waren. "Es ist sehr persönlich und intim, was ihr hier alle teilt. Teilen
müsst, weil es gerade nicht anders geht. Und das vor mir.", sagte sie. "Wenn
ich das richtig verstehe, ist euer nächster Halt außerdem Belloge, oder
ein anderer Ort, wo der Blutige Master M intensiver gefahndet wird, also
auch ich wieder gesucht würde. Ihr wollt euch für eine Aktion in
Gefahr begeben, mit der ich bisher nichts zu tun
habe. Ich hatte mich gefragt, ob ich mich einbringe, aber würde
mich eigentlich lieber in Sicherheit bringen."

"Du schuldest uns nichts, nur weil wir dich aus dem Wasser gefischt
haben.", erwiderte Marusch. Als sie Drude die Arme verschränken
sah, fügte sie hinzu: "Ich sage das vielleicht voreilig im Namen
anderer. Das tut mir leid."

"Ich denke nicht, dass sie uns schuldet, sich für unseren Plan in
Gefahr zu bringen.", sagte Drude. "Aber während ich aus dem
Wasser fischen noch recht selbstverständlich finde, habe ich mich
durchaus nicht übermäßig damit wohl gefühlt, über zwei
Tage eine kleine Yacht mit der Fast-Mörderin des mir wichtigsten
Menschen in meinem Leben zu verbringen. Ich habe dazu nichts
gesagt, weil ich nicht anders entschieden hätte. Es zu sagen
hätte die Lage nur unangenehmer gemacht. Aber eh du in meinem Namen
sagst, dass das alles selbstverständlich wäre, widerspreche
ich dann doch lieber."

Marusch nickte. "Fair. Das tut mir leid."

"Nicht schlimm.", verzieh Drude.

"Dann ist vielleicht umso besser, dass mein Vorschlag ist, dass ich
euch hier und jetzt verlasse.", fuhr Allil fort. "Denn ob ihr danach
zurück Richtung Angelsoge oder Nederoge fahrt, steht
ja nun auch in den Sternen. Ich glaube, ich unterbreche hier gerade
ungünstig ein wichtiges Gespräch, um mich zu verabschieden, aber ich glaube
auch, dass ihr es ohne mich vielleicht entspannter weiterführen könnt."

"Das vermute ich auch.", sagte Drude. "Ich hasse dich nicht. Entschuldige,
wenn das anders rüberkam. Ich fühle, wie üblich, Stimmungen, und dass deine
Anwesenheit Angespanntheit mitbringt."

Lilið schockierte Drudes Direktheit beinahe. Auf der anderen Seite konnte
sie nicht leugnen, dass sie Drude recht gab.

"Ich würde dir eine Umarmung anbieten, aber ich halte mich gerade
für zu gefährlich." Marusch lächelte sie traurig an.

"Schon gut.", sagte Allil.

Sie schwiegen, solange Allil im Bauch
der Teeseufel eilig ihre alten, nun trockenen Sachen
wieder anzog. Anschließend schritt sie zwischen ihnen hindurch
und verließ die Yacht über das Heck. "Ich würde
gern zum Abschied ein paar Versprechen da lassen: Ich werde euch
helfen, wenn sich die Gelegenheit irgendwann bieten sollte. Und ich
werde niemanden von euch je wieder bedrohen." Sie haderte, bevor
sie aufbrach, zog dann doch noch
etwas Langes, Dünnes aus ihrer Kleidung und reichte es an Deck. Heelem
nahm es stirnrunzelnd entgegen. "Für Lilið. Ein Draht. Ich habe
ihn bei einem Erkundungsspaziergang durch die Werft gefunden."

Lilið nahm ihn Heelem ab und betrachtete ihn. Es war
einfach ein Draht, sonst nichts. Sie mochte ihn. "Danke!", sagte
sie. Aber da war Allil auch schon außer Hörweite.

"Was für eine orientierungslose Person.", murmelte Drude. "Ich
verstehe schon, warum du sie magst, Marusch."

"Weil sie orientierungslos ist?", fragte Marusch.

"Ja, vielleicht. Und auch, weil du Extreme magst.", antwortete Drude.

Marusch lachte. Es klang nicht fröhlich in Liliðs Ohren. "Extremes
ist manchmal, was mich am Leben hält."

"Und gerade bräuchtest du etwas Extremes?", fragte Drude.

Lilið runzelte die Stirn. Das war ein seltsames Gespräch, aber
irgendwie auch nicht. Es passte zu Drude und Marusch.

Marusch grinste. "Hast du etwas auf Lager?"

Drude lehnte sich auf der Bank zurück. "Würdest du dich vor mir
niederknien?"

Lilið stockte der Atem. Das war in der Tat extem. Und auch noch
in der Hinsicht, dass prinzipiell Menschen auf benachbarten
Schiffen hinüberschauen könnten. Sie waren durch die tiefergelegene
Sitzecke nur einigermaßen blickgeschützt. Vielleicht würde Drude
auch schnell genug wissen, wenn jemand hinsah.

Marusch stand auf und ließ sich vor Drude auf ein Knie
nieder. "Interessant.", sagte sie mit einem Schmunzeln in der
Stimme.

"Auf beide Knie." Drude wirkte unbeeindruckt.

Marusch kam der Aufforderung dieses Mal ohne Zögern nach. "Noch
etwas? Hättest du gern einen Titel?"

Drude legte die Beine eng aneinander. "Ich
hätte zunächst gern dein Kinn auf meinen Knien."

Lilið hielt den Atem an und spielte mit dem Draht zwischen ihren
Fingern. Wäre es nicht sinnvoll, zuvor irgendwelche Grenzen
abzusprechen, wie Drude es mit ihr getan hatte? Noch fühlte
sich das Ganze beim Zuschauen sicher an. Und irgendwie sehr
gut.

Marusch rückte auf Knien und Händen überraschend elegant näher zu Drude
heran, senkte dann das Gesäß auf die Fersen und das Kinn auf
Drudes Knie ab. Ihr Nacken war dabei leicht überstreckt. Über Drudes
Gesicht huschte ein Lächeln. Maruschs Ausdruck verlor
die Frechheit, die sie so oft mitbrachte. Niedlich, dachte Lilið.

"Schön.", murmelte Drude. "Wie fühlst du dich?"

Nun kam also das Gespräch darüber, das absicherte, dass
Marusch sich damit gut fühlte. Lilið lächelte und atmete
erleichtert aus. Aber Marusch reagierte nicht. Sie wirkte
nicht unglücklich, fand Lilið. Als das Schweigen lang
wurde, bohrte Drude weiter nach. Sanft.

"Kannst du reagieren?", fragte dey.

Marusch nickte. Das Kinn hob sich dabei aus der Knieritze und
senkte sich wieder dahinein ab.

"Ist etwas schlecht?", fragte Drude.

Marusch schüttelte den Kopf.

"Ist die Frage, wie du dich fühlst, zu kompliziert?", fragte
Drude.

Marusch nickte wieder.

Drude war gut!, fand Lilið. Aber war das der richtige Moment für
ein solches Spiel?

Lajana fing plötzlich haltlos an zu kichern. "Ich glaube, so etwas
hat sie sich immer gewünscht! Als wir klein waren, wollte sie immer
Bedienstete spielen und war enttäuscht, dass ich nicht wollte. Sie
wollte eigentlich immer ganz viele Befehle und enge Regeln haben."

"Ist das so? Hast du dir das gewünscht?", richtete sich Drude an
Marusch.

Maruschs Körper durchlief ein Schauer oder so etwas. Jedenfalls bebte
sie kurz. "Schon, ja.", flüsterte sie. "Ich schäme mich ein bisschen."

"Aber du hast gerade nicht die mentale Stabilität dafür?" Drude legte
ihre Hand an Maruschs Kinn, verharrte dort kurz, streichelte über die
Kante und ließ wieder los.

Marusch schüttelte den Kopf. "Ich möchte mich dir sehr gern
unterordenen." Ihre Stimme klang warm und bereit. "Aber um mich
gerade davon abzubringen, immer wieder destruktive Gedanken zu
haben, müsstest du mehr Gewalt gegen mich anwenden. Und
dafür bin ich offen, aber ich glaube, das ist in einem ersten
Spiel dieser Art in dieser Stimmung gefährlich. Für uns beide."

Drude nickte. "Sehe ich auch so.", sagte dey. "Aber gut zu
wissen, dass du grundsätzlich willst."

"Sehr.", betonte Marusch. "Und es hat mich auch gerade ein wenig
sortiert. Danke!"

"Ich könnte noch anbieten, dich mit Hafenwasser zu übergießen.", schlug
Drude vor.

Marusch lachte. "Sollte ich ausversehen ein Feuer legen, herzlich
gern!"

"Wir könnten auch zusammen im Meer baden gehen.", schlug Heelem
vor. "Neben dem Hafen ist ein Strand, wie sich das gehört."

Lajana sprang begeistert auf. "Baden!"

Und so gingen sie im Meer baden, planschten in der Brandung, spritzten
sich nass (nur die, die wollten), um Abstand von einer emotionalen
Krise zu bekommen, bevor sie sie wieder einholen würde. Lilið
erwischte Marusch durchaus ein paar Mal dabei, wie sie Flammen über die
Wellen rinnen ließ. Sie küssten sich, über und unter Wasser, und es
war nicht so merkwürdig wie damals, als Marusch das erste Mal
in Liliðs Gegenwart geflammt hatte. Es hatte sich einiges seit
dem verändert. Sie kannten sich nun besser, teilten mehr miteinander,
und auch wenn Maruschs tiefe Zerstörtheit ein Teil davon war,
wollte Lilið alles davon.

Später saßen sie wieder auf der Teeseufel, trockneten und warteten
auf ihren Termin mit der Werft.

"So seltsam das ist, ich würde gern zu diesem Treffen. Ich
möchte wissen, was politisch nun passiert.", teilte Marusch
überraschend mit. "Wenn du, Lajana, als Königin anerkannt werden willst,
sollten wir auf dem Strategietreffen in vier Tagen auf Mazedoge
zugegen sein. Und so unangenehm auch ein Besuch mit Muddern
werden mag, im Vorfeld Dinge zu erfahren, ist vielleicht
hilfreich. Ich denke, ich möchte da hin."

"Das ist keine so gute Idee.", murmelte Drude.

"Ich weiß." Marusch seufzte.

"Weil du dann wieder alles zu Staub machst, wenn etwas passiert?", fragte
Lajana.

Lilið nahm den Draht wieder zur Hand und drehte ihn zwischen den
Fingern. Sie beteiligte sich nicht am Gespräch. Sie würde
mitkommen, wohin auch immer sie entschieden zu reisen.

"Das war meine Befürchtung.", stimmte Drude zu.

"Ich halte die Gefahr für relativ gering.", erwiderte Marusch.

Über Drudes Gesicht huschte ein Grinsen. "Meinst du nicht, dass
die Versammlung des Bundesorakels und aller fiesen Mächtigen
der halben Welt ausreichend Verlockung für dich darstellen
würde? Du könntest vielleicht das gesamte derzeitige System
stürzen."

Revolution, dachte Lilið. Aber nicht auf eine Weise, die
sie sich wünschte. Mindestens für Marusch nicht.

"Hältst du für realistisch, dass ein Systemsturz tatsächlich
Folge sein könnte?", fragte Marusch. "Dass es
sich nicht irgendwie wieder aufrappelt? Denn so
beschissen dieses Adelssystem mit den Orakeln ist, es ist leider
schon recht stabil. Ich habe noch keine Vorstellungen darüber,
wie das ablaufen würde, aber so weit ich das verstehe, wird einfach
Herrschaftsgebiet an übrigen Adel umverteilt und weiter
geht's. Revolution müsste von innen kommen."

"Ich denke, es könnte reichen, wenn so viel des Adels
wegbricht.", widersprach Drude.

"Hm.", machte Marusch.

"Ich kann mitkommen und dich davon abhalten.", versprach Lajana.

"Du hattest gerade überlegt, Muddern nicht wiedersehen
zu wollen.", erinnerte Marusch sie.

"Dann folge ich dieses Mal dir überall hin, weil du mich
brauchst." Lajana zögerte nicht einen Moment, dies zu
versichern.

Marusch sah sie an. Liebevoll und gleichzeitig voller Angst,
wenn Lilið richtig las. Angst kannte sie von Marusch noch
nicht so gut. "In Ordnung.", stimmte Marusch zu. Dann wandte
sie sich Lilið zu. "Du sendest übrigens. Ist dir das
bewusst? Mit dem Draht. Gleichmäßige Wellen."
