Einschleichen
=============

*CN: Sanism, Ableism, häufige Verwendung des Wortes dumm,
Erregung, Selbsthass, Schlafmangel.*

Kaum hatte sie sich hingelegt, beschloss Lilið doch, auf
den Schlaf zu verzichten. Und zwar weil sie realisierte, dass
Drudes Aufgabe war, sie zu beobachten, aber auch dey irgendwann
schlafen musste, dies also vermutlich in den gleichen Zeitfenstern
täte, wie Lilið. Sie schlüpfte möglichst leise aus dem
Bett, stopfte stattdessen ihre wieder gewaschene, gerade so getrocknete
Kleidung von zuvor unter die Decke, damit es vielleicht im Dunkel
nicht so sehr auffiele, dass ihr Körper fehlte, und
schlich sich aus dem Schlafraum. Sie überlegte,
ob eine Faltung ihr gerade helfen würde, falls sie erwischt
würde. Aber sie war nicht sicher, wie gut
Drude Magie spürte, also ob demm das nicht
aus dem Schlaf holen würde.

Lilið schlich zunächst in den Gang, in dem Drude sie das erste
Mal aufgehalten hatte. Nichts passierte. Sie war sich trotzdem nicht
sicher, ob Drude ihr nicht doch folgte. Drude war schon sehr gekonnt
unauffällig und vorausschauend. Lilið wurde dieses Mal nicht
aufgehalten, aber Drude könnte ihr natürlich jetzt auch folgen
wollen und schauen, was sie mit der Prinzessin bespräche, würde sie
sie finden.

Immerhin war Liliðs Plan erst einmal nur, die Prinzessin zu finden und ein bisschen
kennen zu lernen. Sie hatte ja noch gar keinen funktionierenden Fluchtplan. Der
würde sich erst ergeben müssen. Aber bis dahin wäre hilfreich, mehr darüber
zu wissen, wie die Prinzessin gefangen war und ihren Charakter ein wenig
zu kennen, um ihn mit einzuplanen. Lilið plante also derzeit nichts,
was Drude mehr verraten könnte, als dey schon wusste.

Sie war auf der Kriegskaterane der königlichen Garde
selbst kurzzeitig in einer Zelle gewesen, aber konnte
von diesem Wissen nicht ableiten, wo hier eine sein könnte. Durch den
Speisesaal ging es jedenfalls nicht. Leute wurden eher
unten gefangen gehalten, da war Lilið sich recht
sicher. Die Kagutte war nicht gerade klein, aber trotzdem stellte
es sich dann doch nicht als allzu schwierig heraus, einen Weg nach unten zu
finden. Vorbei an der Kombüse, an einigen geschlossenen Räumen,
eine schmale Holztreppe hinab, auf der Lilið froh war, dass ihr niemand
begegnete. Die Treppe bog am Fuße mit einigen asymmetrischen
Stufen um in einen Gang, der vielleicht um die Hälfte breiter
als die Treppe war. An derem Ende saß eine Wache auf einem Hocker
an die Wand gelehnt und döste. War
es ein ausreichend dösiger Zustand, dass sie Lilið nicht bemerkt hatte?
Immerhin war Lilið barfuß und sehr leise gewesen. Aber es war
immer noch eine Wache, deren Aufgabe es war, ihrem Beruf entsprechend
wachsam zu sein.

Lilið hatte sich zunächst wieder einen Schritt die Treppe hinauf
begeben, wo sie von der Wache aus nicht zu sehen wäre. Sollte sie sich nun
doch falten? In eine Schiffsratte vielleicht? Aber Lilið würde
in der Form keine für Ratten typischen flinken Bewegungen vollbringen
können. Und ein Igel an Bord wäre schon auffällig. Eine andere
Person aus der Besatzung nachzuahmen kam auch nicht in Frage. Sie
konnte sich ja die Gesichter nicht merken. Sie hätte allerhöchstens
eine Chance gehabt, Drude zu sein, und auch das eher nicht genügend
überzeugend.

Sie sollte auch daran denken, was das Ziel wäre, außer näher heran
oder zur Prinzessin zu gelangen. Denn selbst, wenn sie es schaffen
sollte, eine überzeugende Ratte abzugeben, wäre doch auffällig, wenn
sie in die Zelle eindränge und sich als Ratte mit der Prinzessin
unterhielte. Sie war eigentlich auch immer noch
nicht ganz sicher, ob es überhaupt die Zelle der Prinzessin
war. Das wäre die Erkenntnis, die ihr das bringen könnte, aber sie
überlegte, dass sie das vielleicht auch einfacherer und auf
sichererem Wege herausfinden könnte. Irgendwann würde zum Beispiel
Wachwechsel sein und in ihrer Vorstellung machten Wachen dann meist
einen kleinen, belanglosen Klönschnack, bei dem das Offensichtliche
ausgesprochen werden könnte. So etwas wie: 'Für besondere Unterhaltung
sorgt die Prinzessin ja nicht, die könnte sich mal mehr anstrengen,
damit meine Schicht nicht so langweilig ist.' Irgendein Unsinn dieser
Art vielleicht.

Für einen Wachwechsel müsste nur eine Person diese Treppe
herunterkommen. Oder Wachen schliefen in dem Raum hinter der
zweiten Tür, die sie gesehen hatte, als sie um die Ecke gelinst
hatte. Es war nur eine der beiden Türen, die am Ende des Flurs,
bewacht gewesen. Die andere Tür hatte sich in der der
Treppe gegenüberliegenden Wand befunden.

Lilið sollte nicht auf Dauer hier auf der Treppe stehen
bleiben. Zumindest nicht ungefaltet. Und wenn sie sich schon faltete,
dann eher in etwas, was die bewachte Tür beobachten konnte. Eine
Form, die sie lange halten konnte, aber die nicht so sehr weh tat
wie ein Würfel. Und die nicht dazu anregte, fortgejagt zu werden, wie
eine Ratte. Ein Gegenstand, der nicht auffiele. Vielleicht, weil
er schon da war.

Lilið besah sich die Wand am Ende des Flurs. An ihrem Ende des
Flurs, den sie von der Treppe aus im schwachen Licht einer gedimmten
Lampe einsehen konnte. Am anderen Ende war die bewachte Tür. Sie
betrachtete die Wand so lange, bis sie den Eindruck hatte, sie
optisch gut genug nachbilden zu können. Anschließend legte sie
ihre Hand auf die mit ihr verbundene Wand am Niedergang. Holzfasern
hatten immerhin Ähnlichkeiten mit Papier, aber so eine große Fläche
ergab sich für sie nicht so leicht. Trotzdem versuchte sie, sie so
gut es ging, zu verstehen. Dann linste sie ein zweites Mal auf den
Flur. Die Wache döste genau so, wie sie sie vorhin gesehen hatte. Also
schlich Lilið die Treppe ganz hinab, lehnte sich mit dem Rücken an
die Wand, die sie doppeln wollte, und faltete sich entsprechend.

Die Wache zuckte. Lilið wurde für einen Moment sehr heiß und
Schweiß rann an irgendwelchen nach innen gefalteten Stellen ihrer
Haut hinab, klebte dort. Aber die Wache hatte nur gezuckt, vielleicht
im Schlaf. Sie blickte nicht einmal auf.

Lilið verstand das gut. Sie fühlte sich auch endlos müde. Sie hatte ohnehin
schon zu wenig Schlaf für ihr Wohlbefinden und nun verzichtete sie
noch auf weiteren. Als Wand sollte sie besser nicht schlafen. Sie
musste bei der Vorstellung innerlich grinsen, dass sich dabei ausversehen ein Ohr
von ihr oder so entfalteten könnte. Sie hatte eines passend nach außen gefaltet, sodass
ein Astloch ihren Gehörgang bildete, um dieses Mal nicht abgedumpft
zu hören. Sie grinste innerlich noch etwas mehr, weil sie sich vorstellte, wenn ihr ein solcher
Entfaltungsunfall passierte, dann der Spruch 'die Wand hat Ohren'
nicht mehr nur eine Redewendung wäre.

Jedenfalls konnte sie durch das Astloch gut hören und durch
ein anderes, gefülltes, durchaus brauchbar sehen. Atmung funtkionierte
auch, aber fühlte sich trotzdem immer noch ungewohnt an. Das war ein
Zustand, den sie zwei bis drei Stunden durchhalten könnte, und sie
hoffte, dabei einen Wachwechsel mitzubekommen, und dann auch noch
unbemerkt verschwinden zu können.

Die Zelle war eigentlich ein umfunktioniertes
Zimmer. Sie hatte eine normale Tür, in die lediglich oben ein
kleines Loch hinein gesägt worden war. Das half nicht dabei, durch
Stäbe zu beobachten, ob dahinter vielleicht eine Person schliefe, die
den Eindruck einer Prinzessin machen könnte. Ihr jedenfalls nicht, dazu
hätte sie viel dichter an die Tür herankommen gemusst.

Ihr Blick fiel als nächstes auf einen Plan unter der Lampe an der Wand
neben der Zelle. Er war zu weit weg, um alles darauf zu lesen, aber
sie konnte klar ausmachen, dass es sich um eine Tabelle handelte. Die Uhrzeiten konnte Lilið
von der Rückwand aus erkennen, die sie war, weil sie eine Ahnung hatte, was dort
stehen könnte, ihr Gehirn also erwartete Bilder mit dem abgleichen konnte,
was sie erkennen konnte. In der Spalte neben den Uhrzeiten waren Dinge
in verschiedenen Handschriften geschrieben. Namen wahrscheinlich. Ein
Wachplan, schloss Lilið. Ob sie sich einfach dreist bei Gelegenheit in
eine der freien Zeilen eintragen sollte? War es hier genau so wie überall
sonst auf der Welt mit Eintragungen in Plänen, dass sich Leute zwar
vielleicht wunderten, wie sie aufgebaut waren oder wer nun plötzlich auch
darin stand, aber sie einfach als gegeben hinnahmen?

Sie erkannte an den Wiederholungen verschiedener Schriften, wann diese
dösige Wache dran war. Jede Nacht, stellte sie fest. Das war gut. Und
sie konnte erkennen, dass genau zu dem Zeitpunkt Wachwechsel wäre, zu dem
Ott sie zu wecken versuchen würde. Das war nicht so gut. Aber vielleicht
war das Problem einfach lösbar.
Wenn Ott sie wecken wollen würde, könnte sie sich damit
herausreden, einem körperlichen Bedürfnis nachgegangen zu sein. Allerdings wäre
ihre Kleidung unter ihrer Decke schon sehr auffällig. Das Manöver hätte
sie lassen sollen. Aber es war vielleicht trotzdem gut, dass sie es getan
hatte, weil sie sonst wirklich das Risiko eingegangen wäre, den Wachwechsel
abzuwarten. Mit einer neuen Wache, die mehr im Wachzustand wäre, hätte
sie schlechte Chancen gehabt, sich heimlich davonzustehlen.

Ob sie jetzt schon gehen sollte? Aber Lilið beschloss, erst eine gute
halbe Stunde vor Wachwechsel zu gehen und so lange einfach zu beobachten. Vielleicht
kam irgendwann jemand vorbei. Vielleicht konnte sie das Dösverhalten
der Wache studieren. Und vielleicht hätte die Prinzessin auch irgendwann
ein körperliches Bedürfnis und es wäre Aufgabe der Wache, sich darum zu
kümmern. Da es keine Luke in der Tür gab, musste sie für das Entfernen
von Notdurft und das Bereitstellen von Nahrung geöffnet werden.

Aber stattdessen passierte lange Zeit nichts, und dann etwas, womit
Lilið nie im Leben gerechnet hätte. Nach dem Stundenglas, das auf der anderen
Seite an der Wand hing, war gerade die Zeit gekommen, zu der sie sich entschließen
wollte, zu gehen, als die Wache sich aufrichtete und müde zur anderen Tür schlurfte. Sie
blickte sich noch einmal zu allen Richtungen um, als würde sie etwas Heimliches
tun wollen, dann öffnete sie die Tür leise. "Deine Zeit ist gekommen!", hörte
Lilið die Wache flüstern. Die Stimme klang spielerisch dramatisch.

Hinter der Tür hörte Lilið Geräusche, die so klangen, als würde eine Person
Dinge umstellen, dann trat die Prinzessin aus der Tür. Sie trug
ein langes, blaues Kleid (zumindest wirkte der Rock wie zu einem Kleid
gehörend), darüber einen Wollpullover mit hochgekrempelten Ärmeln. Das
Kleid wirkte einigermaßen edel, der Wollpullover eher leger. "Ich
ergebe mich meinem Schicksal.", antwortete sie mit einem Grinsen und
schloss die Tür hinter sich.

Auch die Wache grinste, als sie die Zellentür mit Schlüsseln, die sie
halb erfolgreich am Klappern zu hindern versuchte, aufschloss, die Prinzessin einließ
und hinter ihr wieder abschloss. Es brauchte nicht lange, bis die
Szenerie wieder so aussah, wie Lilið sie vorgefunden hatte. Die Wache
döste vor der Tür. 

Das war interessant. Lilið hatte im Moment nicht viel Zeit, darüber
nachzudenken. Sie musste verschwinden, und zwar zügig. Sie glitt also
aus ihrer Faltung und schlich die Treppen hinauf, die Gänge entlang,
zurück ins Bett. Zweimal musste sie sich auf dem Weg dieses Mal
schnell in eine Nische schieben, weil inzwischen mehr Leben in die
Crew gekommen war.

Sie versuchte, ihren nächtlichen Ausflug nicht zu bereuen, als
Matrose Ott sie aus einer Tiefschlafphase hochholte, in die sie
in ihrer übrigen Viertelstunde geschafft hatte, hineinzugleiten.

---

"Du hast Recht.", begrüßte Drude sie. Dey saß neben der Abe auf dem
Kartentisch in einer Haltung, die anatomisch etwas inkorrekt wirkte,
aber bei demm gemütlich aussah. Der eines Knie lehnte dabei gegen dere
eingesunkene Schulter. Die Hand hielt das Navigationsbüchlein, das
Lilið hier gestern zurückgelassen hatte, aufgeschlagen direkt neben
derem Fuß.

"Immer.", wagte Lilið zu sagen.

Kontrollierte Drude ihren Kurs? Wenn, dann sollte dey zumindest bis
jetzt nichts Auffälliges finden.

Drude blickte auf und lächelte für einen flüchtigen Augenblick. "Mit
den verschiedenen Dimensionen von Dummheit.", erklärte dey. "Und
damit, dass ich selbst doch auf mindestens eine Art dumm bin."

Lilið ließ das so stehen. Sie hielt Drude die Hand hin, damit dey
ihr das Buch geben könnte, um ihre frisch gemessenen Daten einzutragen.
Und dann fügte sie doch hinzu: "Ich bin mir sicher, dass du da recht
hast, weil es auf fast alle Menschen zutreffen wird, aber die Wortwahl
mag ich trotzdem nicht."

"Kannst du mir eine Alternative nennen?" Drude reichte ihr das Buch.

Die Sonne war gerade erst aufgegangen, aber das Licht fühlte sich bereits
unangenehm schneidend in Liliðs Augen an. Sie merkte, dass sie alles intensiver
wahrnahm als sonst. Sie musste dringend mehr schlafen, aber auf der
anderen Seite wusste sie, dass sie in der kommenden Nacht wieder
ihre zweiten drei Stunden Schlaf für einen Besuch bei der Prinzessin
opfern würde. Dieses Mal würde sie in den zweiten Raum hineingehen
und herausfinden, was es damit auf sich hatte.

"Ich denke darüber nach.", antwortete Lilið auf Drudes Frage. Eigentlich
fühlte sie sich zu ausgelaugt für dieses Gespräch, aber vielleicht
brachte es etwas. "Auf der anderen Seite,
glaube ich, wird das Wort 'dumm' vor allem benutzt, um Menschen zu
beschreiben, die in unserer Gesellschaft viel schlechter
dran sind als wir zwei. Ich kenne
es auch, dass ich in den Kategorien Skorem oder Intelligenz für
weniger gut abschneidend gehalten werde, als ich bin. Das ist unangenehm
und hat Nachteile für mich. Aber die Leute, die nicht nur dafür abgewertet
werden, sondern auf die auch zutrifft, dass sie zum Beispiel einen
niedrigen Skorem haben, haben es in dieser Welt viel schwerer als
ich. Ich denke nicht, dass es mir zusteht, zu entscheiden,
mit welchen Wörtern sich jene Menschen beschreiben sollten. Vor
allem nicht, wenn jene Wörter fast ausschließlich dazu verwendet
werden, persönlich abzuwerten, also, den Menschen an sich einen
geringeren Wert zuzuschreiben."

"Ich möchte Menschen keinen geringeren Wert zuschreiben." Drudes
Körperhaltung ließ noch mehr an Spannkraft nach. Dere Finger
fädelten sich zwischen dere Zehen. Das schwarze glatte Haar
fiel demm vors Gesicht. "Aber ich glaube, ich tue es ausversehen
doch. Zumindest mir."

"Ich muss den Kurs prüfen und die Karte auf den neuen Stand
bringen." Lilið versuchte, sanft zu sprechen. Etwas schien in
Drude vorzugehen, was demm belastete. "Ich brauche nicht lange. Dann
bin ich da für dich, so gut ich kann."

"Das ist nett, aber du solltest dich nicht um meine Probleme
kümmern müssen. Mach deine Aufgaben und dann reden wir.", sagte
Drude. "Über Politisches."

Lilið schmunzelte. "Meine zwei Tage sind fast rum. Ist das sozusagen
unser Abschlussgespräch, bevor du entscheidest, ob du mich ans
Messer lieferst?" Lilið fühlte für einen kurzen Augenblick die
Erregung wieder aufflammen und kämpfte sie wütend darauf, dass
das passierte, nieder. Nicht jetzt! Doch nicht, wenn die Gefahr
real im Raum stand!

"Ich möchte dich nicht ans Messer liefern.", antwortete
Drude. "Vielleicht tue ich es trotzdem irgendwann, aber erstmal
habe ich mir einen für mich zwar
risikoreichen, -- aber dies im aushaltbaren Maße --, dritten
Weg ausgedacht, mit dem ich dir vielleicht noch ein paar Tage
geben kann, selbst wenn wir uns nach diesem Gespräch noch
nicht einig sind."

Obwohl Lilið wegen ihres noch sehr gedämpften
Emotionsapparats kaum Angst wahrgenommen hatte, fühlte sie
jetzt durchaus Erleichterung. Waren das Tränen
in ihren Augen? Sollte sie sich bedanken? Lilið atmete bewusst aus,
fühlte den Windhauch des eigenen Atems und konzentrierte sich aufs
Navigieren.

Drude sah wieder sehr aufmerksam zu. Dieses Mal machte
Lilið absichtlich einen zögerlichen, nachdenklichen Eindruck, als sie
fertig war, um herauszufinden, ob sie an Drudes Reaktionen ablesen könnte, ob
dey es bemerken würde. Aber Drude veränderte dere Haltung überhaupt
nicht. Also ließ dey sich entweder nichts anmerken, oder verstand
nicht genug von dem, was Lilið tat.

Lilið ließ das Kartensteichen los, schaute noch einmal ins Buch
und legte es anschließend ab. "Fertig."

"Ich kann nicht mit Zahlen umgehen.", teilte Drude mit. "Gar
nicht. Ich kann all diese Dinge wie logisch Denken, Zusammenhänge
erschließen, und habe, wie du, einen ziemlich hohen Skorem. Aber
Zahlen, Zahlen fallen bei mir aus dem Bild, sodass sich
Leute immer wieder wundern und dann völlig von oben herab
irgendwelche Sprüche gegen mich fallen lassen. Oder sie
kaufen es mir gar nicht erst ab. Und ich habe dadurch einen Hass
auf Zahlen entwickelt."

"Guckst du dann gar nicht richtig hin, wenn ich welche
notiere?", fragte Lilið.

"Doch.", antwortete Drude. "Und ich habe mich dabei mindestens
vier Mal so gefühlt, als würde ich implodieren. Aber das tut
hier jetzt nichts zu Sache."

Lilið nickte. "In Ordnung.", sagte sie. Ein Runzeln huschte
über ihre Stirn, als sie sich an ihr Gespräch mit Drude am Vortag
erinnerte. Drude hatte mit Wahrscheinlichkeiten gerechnet, um
abzuschätzen, ob eine unterschiedlich lange Lebensspanne
unterschiedlich schnelles Lernverhalten ausgliche. Lilið
erinnerte sich daran, dass Drude die Zahlen besonders großzügig
aufgerundet hatte, aber hieß das nicht, dass dey doch
ein klein wenig mit Zahlen umgehen
könnte? Egal, Lilið hatte das nicht zu beurteilen und
darum ging es auch nicht. "Was tut etwas zur Sache?"

"Dass ich mich dafür hasse.", antwortete Drude.

Lilið runzelte die Stirn. "Ist Wut auf sich und Hass auf sich
so ein großer Unterschied?"

"Ja!", betonte Drude. "Wut ist eher über die Sache, dass etwas
nicht funktioniert. Hass bedeutet, dass ich mich persönlich
weniger mag, weniger wertschätze, weil ich eine Sache gehirntechnisch
nicht hinkriege."

"Achso, es geht darum, dass du doch in dem Zusammenhang
persönlich abwertest. Und dir ist es zuvor nicht aufgefallen, weil
du nur dich selbst abgewertet hast, aber andere nicht?", fragte
Lilið. Sie kam sich unbarmherzig vor.

Drude nickte. "Ich bin mir nicht sicher, ob ich nicht auch andere
ausversehen doch abgewertet habe, aber mich auf jeden Fall.", sagte
dey. "Ich finde diesen Defizit an mir schlimm, es fühlt sich für
mich an, als wäre ich kaputt und würde deswegen weniger verdienen."

"Der Unterschied zwischen Wut auf sich und Selbsthass, den du
erklärt hast, ergibt für mich erstaunlich viel Sinn.", fügte
Lilið murmelnd hinzu. Wahrscheinlich tat es auch nichts zur Sache.

"Natürlich ergibt das Sinn!", erwiderte Drude. "Alles, was ich
sage, ergibt Sinn!"

Lilið sah gerade rechtzeitig auf, um ein Lächeln über Drudes
Gesicht huschen zu sehen. "Bedeutet deine Mimik, dass du es nicht
ganz ernst meinst?"

"Hast du doch vorhin mit deinem 'immer' auch nicht, oder?", fragte
Drude.

Lilið nickte. "Bei dir ist es, glaube ich, schwerer zu lesen."

Drude lachte wieder eins der lautlosen Lachen. "Ich kann es bei
anderen auch nicht lesen, sondern muss es aus dem Kontext raten.", sagte
dey. "Also weiß ich auch nicht, was ich tun muss, damit es bei mir
lesbar wird. Manchmal denke ich, ich sollte das ganz lassen. Also,
witzig sein. Meine Witze versteht eh niemand."

Lilið blickte aus dem Fenster, -- und sofort wieder weg. Sie
wollte in die Ferne blicken, aber draußen war es zu grell. Sie
war viel zu müde. Zu allem Überdruss musste sie auch noch gähnen. "Ich
würde dich fragen, ob du dich auch dafür hasst. Aber tut das was
zur Sache?"

Drude nickte und ließ den Kopf hängen. "Ich hasse mich auch dafür.", sagte
dey. "Und ich vermute darin den Hauptgrund, warum Leute mich für
dumm halten, wenn sie mir begegnen. Warum fällst du darauf nicht rein?"

"Wegen der Skala-Sache.", antwortete Lilið. "Es macht halt keine
Aussage über den Rest deines Könnens, dass du eine
schwer lesbare Mimik hast und kommunizieren mit dir auf manchen
Ebenen nicht ganz leicht ist. Obwohl mich deine Arroganz mehr stört,
als dass ich für deinen Humor noch Zeit brauchen werde, bis ich ihn
raushaben werde."

"Oh, ich bin arrogant?", fragte Drude.

"Manchmal definitiv.", sagte Lilið trocken, und hoffte, dass es
Drude nicht allzu sehr treffen würde. Besser, sie fügte es
hinzu: "Es tut mir leid."

"Ich finde eigentlich gut, wenn Menschen nicht durch Blumen
reden. Du hast das sachlich gesagt.", antwortete Drude. "Nun
muss ich nur noch herausfinden, wann etwas arrogant ist, was ich
sage." Wieder huschte ein Lächeln über der Gesicht. "Nur noch."

Lilið grinste mit. "Ich sage es dir beim nächsten Mal.", versprach
sie. "Wollen wir zurück zur Sache kommen?"

Drude strich sich mehrere Strähnen hinter das Ohr. Die helle Haut
bildete einen starken Kontrast zum schwarzen Haar. "Was würdest
du dazu denken, wenn ich Nautika werden wollte?"

"Willst du?", fragte Lilið. Irgendetwas in ihr fühlte sich bei dem
Gedanken warm an, als wären sie noch ein wenig verbündeter. Oder
würde sich warm anfühlen, wenn Drude bestätigen würde.

"Das tut wieder nichts zur Sache und ist seit heute Nacht eine
extrem schwierige Frage.", sagte Drude energisch. War das
Aggressivität? "Ich hatte früher einfach akzeptiert, dass es
mit meiner Einschränkung nie gehen wird. Und dass ich deshalb in dem
Beruf nichts verloren habe. Aber wenn ich dir zusehe," Drudes
Finger fühlten über den Rand der Karte und dere Stimme verlor
an Kraft, "dann machen mich immer noch die Zahlen aggressiv, aber
die Geometrie ist so wunder-, wunderschön!"

Lilið konnte nicht anders, als breit zu lächeln. Sie schob den Zirkel
etwas zurecht. "Ich frage mich, ob Navigation auch ohne Zahlen geht."

"Ohne Zahlen?" Drude wirkte mehr als skeptisch.

Aber Lilið ließ sich nicht beirren. "Mit den Zahlen werden vor allem
Größenordnungen umgerechnet, glaube ich.", sagte sie. "Moment, für mich
ist das so sehr Routine, dass ich gerade erst rausfinden muss, wo
überall Zahlen drinstecken. Aber so etwas wie der halbe Winkel von
etwas ist definitiv geometrisch konstruierbar."

"Lilið, ich habe Schwierigkeiten sechs minus zwei zu rechnen." Dieses
Mal war Drudes Tonfall eindeutig aggressiv. "Ich bekomme dabei
zum Beispiel oft sechs raus, wenn ich nur ein bisschen abgelenkt
bin, weil ich vergesse, die zwei auch wirklich davon abzuziehen. Einen
Winkel zu halbieren, heißt, dass ich etwas durch zwei teile, auch,
wenn es keine Zahl ist, die ich teile. Das mag noch gehen, aber
bei drei könnte es vielleicht schon schwierig werden."

"Oh.", machte Lilið. Sie korrigierte rasch ihr Bild von Drudes
Schwierigkeiten in ihrem Kopf. Seltsamerweise fügte sich
die Erinnerung an gestern nun besser hinein. "Es tut mir leid, ich hätte nicht
einfach eine Idee davon haben sollen, was du mit Schwierigkeiten mit
Zahlen meinst." Nach kurzer Überlegung fügte sie hinzu: "Darf ich
trotzdem noch so eine Art Vorschlag loswerden?"

"Ja, dazu reden wir." Entegegen dem, was dey sagte, wirkte
Drude aber trotzdem nicht, als wäre dey sonderlich auf eine
Antwort erpicht.

"Es müsste also eine Person neben dir stehen, die den Rechnenpart
übernimmt.", sagte Lilið. "Wenn ich dich richtig verstehe, aber
korrigier mich gern, ist das auch gar nicht der Teil am Navigieren, der
dich reizt. Und Rechnen können viele Leute gut, die wiederum mit
Geometrie und Trigonometrie harte Schwierigkeiten haben. Du könntest
also mit einer anderen Person zusammen ein Nautika bilden."

Drude lachte wieder. Es wirkte noch hohler als sonst. "Als ob
irgendwer freiwillig zwei Personen anheuern würde, wenn es auch
geht, nur ein Maul zu stopfen und nur eine Koje zu bieten für die
gleichen Dienste."

Lilið brummte zustimmend. "In einer nur leicht idealeren Welt hättest
du mit einer geeigneten zweiten Person echte Chancen.", sagte sie. "Es
existiert so ein Mangel an guten Nautikae. Ich meine, mir kaufen Leute
ab, das Nautika der königlichen Garde zu sein. Das einzige, was dir,
oder dann euch, im Wege stünde, wäre diese Abneigung von Leuten, etwas
zu tun, was völlig aus der Reihe tanzt. Oder die Abwertung, über die
wir die ganze Zeit reden. Wer würde ein Nautika einstellen, das nicht
rechnen kann, selbst wenn es dafür einen Ausgleich gibt? Das ist gegen
die sozialen Regeln, und diese Regeln sind nicht logisch und kacke."

"Es ist nicht das einzige Problem!", widersprach Drude. "Ich müsste
auch eine Person finden, die mit mir genügend klarkäme, und das schon,
bevor ich Grundkenntnisse hätte, also, eine Person, die
mich auch schon bei einer Ausbildung begleitet. Oder ich bräuchte
ein Nautika, dass mich trotz meiner Defizite eben in dem einen Teil
von Nautik ausbildet, den ich lernen kann."

Lilið zuckte nachdenklich mit den Schultern. "Ich habe hauptsächlich
aus Büchern gelernt. Aber ich weiß, dass das vielen Menschen schwerfällt."

"Ich kann prinzipiell aus Büchern lernen.", erwiderte Drude, nun wieder
deutlich gereizt. "Ist dir zufällig mal eins in die Hände gefallen,
in denen es Übungsaufgaben ohne rechnen gab? Also eines, das sich
auf die Geometrie beschränkt?"

"Oh Mist!", fiel Lilið ein. "Das habe ich schon wieder vergessen. Es ist
schlimm mit mir."

"Sind wir dann, was unangenehmes Verhalten angeht, in meinem
Falle die Arroganz, jetzt quitt?", fragte Drude.

"Auf jeden!", erwiderte Lilið und hielt sich die Hand an die Stirn, wie
als ob sie einen Befehl annähme.

"Aber du sagst es mir trotzdem, wenn ich arrogant bin, ja?", bat Drude.

"Auch auf jeden.", sagte Lilið sanfter und lächelte. "Sind wir immer noch bei der
Sache? Ich glaube, ich verstehe nicht mehr, was die Sache ist."

"Denkst du, dass für eine Königin, die Diplomatie und Strategie und
vor Leuten Reden und all so etwas nicht kann, auch ein Weg existiert,
regieren zu können, wenn die Welt idealer wäre?", fragte Drude. "Und selbst
wenn, meinst du, es wäre gerechtfertigt, durchzudrücken, dass sie eine
Chance bekäme, wenn daran so viele Menschenleben hängen?"

Ah, darauf wollte Drude hinaus. Lilið lächelte, weil sie sich diese
Frage tatsächlich schon einmal gestellt hatte. Nur noch nie in so
einem klaren Kontext und auch noch nie in der Konkretheit. "Bevor ich
auf die Frage eingehe,", sagte sie, "selbst, wenn dem nicht so wäre,
denkst du, es wäre nicht eine massive Abwertung der Person, ihr durch
diese Art von Zwang, wie ihr das tut, den Weg zu verbauen? Und zwar
auch eine Abwertung, die eine gesellschaftliche Aussage hat und nicht
nur sie betrifft? Sondern die aussagt: Hey, ihr alle, die ihr diese
Dinge nicht könnt, die die Prinzessin nicht kann, ihr habt kein
Recht über euch zu bestimmen. Das machen wir." Lilið bemerkte, dass
sie eine Gänsehaut davon bekam, diese Gewalt das erste Mal so klar
in Worte gefasst zu haben. Und wo sie schon einmal in Fahrt war
und Drude ihr zuhörte, bremmste sie sich
nicht, fortzufahren. "Es mag böse von mir sein, aber mir
ist es wichtig, gegen diese Gewalt einzustehen, selbst wenn daran
Menschenleben hängen. Letzteres ist beschissen und ich hasse es, in
solchen Kategorien zu denken, dass irgendetwas ein Menschenleben aufwiegen
würde. Das tue ich auch nicht. Denn da wären wir wieder bei der Frage
nach Verantwortung, über die du mit mir nach dem Angriff der Kriegskaterane
gesprochen hast. Die Verantwortung für die Gräueltaten, die passieren würden,
würde die Prinzessin die Regierung übernehmen, haben
andere. Sie plant überhaupt keine Angriffe, sondern sie plant, keine Verträge
einzugehen, und das hätte Angriffe durch andere zur Folge. Die
Verantwortung trüge auch nicht ich, weil ich mich gegen eine Gewalt stark mache, die
im Kleinen durchaus auch mich und dich betrifft, aber eben besonders Menschen
wie die Prinzessin. Selbst wenn eine erfolgreiche Befreiung
der Prinzessin diese Gräueltaten zur Folge hätte, trüge ich
nicht die Verantwortung dafür. Ich wehre mich damit auf noch
verhältnismäßig friedliche Art gegen die Gewalt, die uns abhängig
von unseren kognitiven Fähigkeiten gegebenenfalls einen so
niedrigen Wert zuordnet, dass es in dieser Welt sogar in Ordnung
ist, uns mal eben unsere Menschenrechte abzuerkennen. Der
Prinzessin werden hier ihre Menschenrechte
aberkannt, nur weil sie dassselbe Recht haben will wie
skorschere oder intelligentere Menschen und nicht irgendetwas zum
Ausgleich zu bieten hätte. Es sollte keinen
Ausgleich geben müssen. Es geht mir hier nicht nur um
die Rettung einer Person, die Spielball der Mächte geworden ist, sondern
auch um die Implikation auf alle Menschen, auf die Bedeutung dessen,
was hier passiert, dass diese Gewalt schon Ordnung wäre, weil
es sich ja nur um eine unskorsche, wenig intelligente Person handelt. Die
auch noch als Gefahr dargestellt wird, während die Gefahr eigentlich das
System darstellt."

Lilið hörte abrupt zu reden auf, obwohl sie noch ein paar Argumente
gehabt hätte, als Drude die Hand hob. Als sie zu Drude herüberblickte,
sah sie, dass Drude auch weinte. Ihr selbst waren die Tränen irgendwann
während dieser Rede in ihre Augen geflossen, als sie bemerkt hatte, wie
entlastend das war, diese Wut zu formulieren, auch wenn
sie sie an manchen Stellen als schwammig empfunden hatte. Sie hatte
die Tränen nicht zurückgehalten, es war ihr egal gewesen.

"Ich kann die Präsenz von Leuten spüren, die bald in Lauschradius
sein werden.", flüsterte Drude. Dey wischte sich mit dem Ärmel durchs
Gesicht und glitt vom Tisch. "Ich glaube, du hast mich."
