Umorientieren
=============

*CN: Schlafmangel, Folter und Mord - erwähnt, Geschlechtszuweisung,
Reden über sexuelle und andere Übergriffigkeiten, Vergiften
und Hinrichtung als Thema, Erwürgen, Bewusstlosigkeit.*

Die Kagutte lag im Hafen von Lettloge. Die Wachen Schäler und
Luanda begleiteten sie dorthin. Wenn Lilið darauf vertraute, dass
Wache Schäler tatsächlich hauptverantwortlich für des Gemetzel auf der
Kriegskaterane gewesen war, war es wahrscheinlich aktuell wirklich keine
gute Idee, zu fliehen. Aber unter diesem Gesichtspunkt fühlte es sich
auch merkwürdig an, wie gelassen Drude mit der Wache sprach. Drude
war überhaupt ungewöhnlich gesprächig. Vielleicht war dey es, um
zum einen herauszufinden, was vorgefallen war, und zum anderen
eine Geschichte recht ausführlich darzulegen, an die Lilið sich
halten könnte, wenn sie irgendwann getrennt befragt werden würden.

An sich hätte Lilið gehofft, dass die Crew mehr Zeit benötigen
würde, um von ihrer Abwesenheit mitzubekommen. Lajana wurde in
dem sichersten Raum der Kagutte ja schließlich üblicherweise
bis fast zum Ende ihrer zweiten Schlafschicht in Ruhe gelassen. Wenn
wie üblich niemand zuvor nachgesehen hätte, wäre ihre Flucht
also erst dann bemerkt worden und die Kagutte wäre so weit weg
gewesen, dass sie Lettloge nicht so rasch wieder hätte erreichen
können. Sie hätten mindestens bis zum frühen Nachmittag
für den Rückweg gebraucht, und auch nur dann nicht
länger, wenn Matrose Ott sich in seinen Navigationsfähigkeiten
selbst übertroffen hätte.

Aber Matrose Ott hatte sie natürlich zu ihrer nächtlichen
Navigationsstunde zu wecken versucht. Interessanterweise
hatte er sie allerdings gedeckt. Er hatte dem Kapitän und
Wache Schäler erzählt, dass Lilið Albträume gehabt hätte, einen
unruhigen Schlaf, und sich deshalb nachts viel die Beine vertreten
hätte. Er berichtete, dass Lilið häufig Schlafprobleme hätte,
dass er Lilið schon oft nachts nicht in ihrem Bett sondern
anderswo an Bord vorgefunden
hätte und sich schon länger Sorgen um ihre Gesundheit
machte. Er behauptete, Lilið auch in dieser Nacht gefunden zu haben, aber dass sie
das Angebot angenommen hätte, dass er die nächtliche Navigationsstunde
dieses Mal auch übernähme.

Lilið schaffte es, keine Miene zu verziehen und einfach zu
bestätigen, wenn sie dazu aufgefordert wurde und Drude unauffällig
nickte. Sie hoffte, dass Drude erkennen würde, wenn Wache Schäler
ihr Fallen stellen würde.

Matrose Ott hatte allerdings dem Kapitän gemeldet, dass sie
dicht bei einer Insel wären, und dass, wenn ein Hinterhalt geplant
wäre, bei dem ihnen ihr Fang -- damit war die Kronprinzessin
gemeint -- wieder abgenommen würde, dieser jetzt passieren
würde. Also hatten sie zum Anfang seiner zweiten Nachtschicht
nachgesehen und früher vom Verschwinden Lajanas mitbekommen,
als Lilið gehofft hatte.

Im Hafen von Lettloge hatte sich dann glücklich für die Crew gefügt,
dass nur einen halben Tag zuvor Wache Luanda aus dem Königreich
Sper einen Auftrag abgeschlossen hatte. Ein Schiff hatte sie mitbewacht,
auf dem der lettloger Inselsakralet von einer Mission nach Belloge und
zurückgereist war. Nun war sie wieder für eine Einstellung offen
gewesen mit dem passenden Wunsch, nach Belloge zurückzugelangen.

Aus den Erzählungen der beiden Wachen ging auch hervor, dass
sie zwar vom Blutigen Master M gehört hatten, aber sie sich
das Phantombild nicht näher angesehen hatten. Lilið bewunderte, wie
Drude es geschafft hatte, diese Information unauffällig aus ihnen
herauszulocken. Auf der anderen Seite war das wohl von Berufs
wegen eine derer Aufgaben.

Kaum zwei Stunden, nachdem sie alle an Bord waren, brachen
sie wieder auf. Ein neues Nautika hatten sie nicht gesucht, was
Lilið positiv überraschte. Sie hätte damit gerechnet, dass sie nun
wenigstens vom Kapitän verhört werden würde, aber das passierte
auch nicht. Trotzdem war alles anders als zuvor: Die
Wache, die Lajana immer in den anderen
Raum gelassen hatte, wurde zu Reinigungs- und Küchenarbeiten
rekrutiert und bewachte Lajana nicht mehr. Sie hatte offen eingestanden,
dass sie Lajana in den anderen Raum gelassen hatte und gab sich
selbst die Schuld dafür, dass ein Ausbruchsversuch möglich
gewesen wäre. Das war prinzipiell auch nicht falsch. Lilið fand
interessant, dass sie dafür nicht mehr bestraft wurde.

Lilið wurde nun auch von Wache Luanda bewacht. Lilið fragte
sich, ob dadurch ihre Möglichkeit ganz unterbunden
sein könnte, privat mit Drude zu reden. Lilið wurde
wieder mit der Navigation betraut und sah sich, wie am
Anfang, gezwungen, sie gut zu machen. Vielleicht war das
einfach der Grund dafür, dass sie kein neues Nautika anheuerten: Der
Weg war nicht mehr weit und Lilið hatte keine Wahl. Sie hätte
vielleicht ungünstig durch die Seenplattenströmungen navigieren
können, sodass die Kagutte dabei zu Schaden gekommen wäre, aber
das brachte ihr nichts. Wenn Lilið einen ausreichend großen
Schaden riskiert hätte, mit dem eine Reparatur in einer Werft
nötig gewesen wäre, hätte sie ihrer aller Leben mitriskiert
und das war nicht, was sie wollte.

Weil die Seenplattenströmungen ein nahes Hindernis waren, ging
sie ihrer Aufgabe als Nautika ohne Umschweife nach. Sie
nahm Wind- und Strömungsdaten auf, um davon abhängig eine besonders
gute Stelle zum Passieren anzupeilen. Sie war kaum in den Kartenraum
getreten, da stand ihr Matrose Ott gegenüber. Das war nichts
Ungewöhnliches, aber sein ernster Blick dabei vielleicht schon. Lilið
erinnerte sich, dass er sie nicht verraten, sondern sogar für sie gelogen
hatte.

"Wir haben noch knapp zwei Tage bis Belloge, richtig?", fragte Matrose
Ott. "Also spätestens in der Nacht von morgen auf übermorgen
kommen wir an."

Lilið nickte. "Und dann noch etwa drei bis zu unserem Ziel Mazedoge." Das
war der Hauptsitz der Monarchie des Königreichs Sper.

"Wir beenden die Reise auf Belloge.", korrigierte Matrose Ott. "Wir
haben in Absprache mit König Sper umorganisiert. Belloge ist dicht
hinter der Grenze. Die Sakrale dort ist gut geschützt und eine der
größten des Königreichs. Sie eignet sich gut als Verhandlungsort."

Lilið versuchte, ihr Erschrecken darüber zu verbergen. Sie zuckte
immerhin nicht, aber sie fühlte einen plötzlichen Schweißfilm auf
der Haut. Also hatte sie nur noch knapp zwei Tage, um mit Lajana zu entkommen,
und es wären zwei Tage, in denen es eigentlich keine Möglichkeit dazu
gab. Sie konnte vielleicht nicht einmal mit Drude zusammen planen. Sie
wurde verstärkt beobachtet und konnte sich nicht mehr falten, weil Wache
Luanda es sofort bemerken würde. Dass sie es konnte, war nun ein Ass
im Ärmel, das sie ausgeben würde, sobald sie es an Bord täte. Und
selbst wenn sie sich hätte unbemerkt falten können, wäre sie nicht mehr so
einfach wie bisher zu Lajana gelangt, weil diese nun über die ganze
Zeit hinweg in einer kleinen Zelle bewacht wurde, vor der jeweils
ein bis zwei nicht dösende Wachen lauerten. Außerdem kam ihr wieder in die
Quere, dass sie Schlafmangel hatte (den sie immerhin im Moment nicht
so sehr spürte) und eine Menge Aufgaben, denen sie nachgehen musste.

"Ich habe dich was gefragt!", hörte sie Matrose Ott angenervt sagen.

Lilið rief sich die Geräusche in Erinnerung, die sie nicht analysiert
aber noch abgespeichert hatte. Er hatte sie aufgefordert (nicht
gefragt), ihn auszubilden. "Im Navigieren?"

"Worin denn sonst?", fragte Matrose Ott. "Du bist gut, das lässt sich
nicht leugnen, so sehr ich es wollte. Ich habe schon so manchen Nautikae
bei der Arbeit zugesehen und selten waren sie unter erschwerten
Bedingungen so präzise und sicher wie du."

Lilið konnte nicht vermeiden, dass sie das Lob berührte. Als Matrose
hatte er mit Sicherheit ein gewisses Spektrum an
Nautikae erlebt. Unter anderem eines, dass sie kielgeholt und
wahrscheinlich getötet hatten. Eines, das zwar unwahrscheinlich
Marusch gewesen war, aber ausgeschlossen war das nicht.

Liliðs Gedanken schweiften schon wieder ab. Sie fühlte sich durch
den genervten Ton unter Druck gesetzt, aber auch, weil sie die
Bedrohung in der neuen Situation jetzt an Bord umso mehr spürte. Sie war
nicht verdächtigt worden, Lajana befreit zu haben, aber die Crew wusste
natürlich, dass irgendjemand Schuld sein musste. Vielleicht wusste
Matrose Ott, dass sie es war. Hatte er sie gedeckt, weil er sie
erpressen wollte, ihn dafür auszubilden? Oder hatte er ihr einfach
nur helfen wollen? Wäre sie andernfalls ermordet worden?

"Ich muss jetzt gerade sehr gründlich sein, damit wir die Seenplattenströmungen
mit möglichst geringem Risiko passieren werden. Du kannst mir, wie immer,
dabei zusehen, aber ich habe nicht die Kapazität, dabei auch
auszubilden.", sagte sie. "Danach und morgen finde ich sicher
irgendwann eine Stunde, in der ich dir etwas erklären kann." Lilið
überlegte, dass es auf die letzten zwei Tage nicht schaden konnte, wenn
sie Matrose Ott ein paar Kleinigkeiten beibrachte. Er hatte auch
beim Zusehen schon die ein oder andere Erkenntnis erlangt.

"Du wirst mich ausbilden, so gut das in der Zeit geht!", verlangte
Matrose Ott erneut. "Ich denke, ein Zertifikat zum Leicht-Nautika
sollte am Ende drin sein."

Lilið runzelte die Stirn und schüttelte dann langsam den Kopf. "So
gern ich wollte, und glaub mir, ich verstehe das Bedürfnis. Es
sprechen zwei Dinge dagegen!", sagte sie. "Zum einen kenne ich
deine Probleme und weiß, wie du lernst. Leute erwarten von einem
Leicht-Nautika mehr, als du in zwei Tagen lernen kannst. Selbst
wenn ich großzügig wäre. Und zum anderen bin ich Nautika ohne
Ausbildungs-Lizenz."

"Ohne Ausbildungs-Lizenz?" Matrose Ott wirkte irritiert.

Lilið nickte. "Ich bin noch nicht lange genug dafür dabei."

"Du bist doch Nautika der königlichen Garde!", wunderte sich
Matrose Ott.

Lilið nickte noch einmal, auch wenn es eine Lüge war. "Ich wurde
erst in Nederoge angeheuert.", gab sie die Geschichte wieder,
die sie in den letzten Wochen gelebt hatte. "Du stellst richtig fest, dass
ich gut bin. Daher wurde ich empfohlen. Aber ich bin nichtsdestotrotz
noch recht neu in dem Beruf."

Matrose Ott nickte verstehend. Die Enttäuschung war ihm
anzusehen. "Scheiße. Ich dachte, ich hätte eine Chance."

Lilið fühlte Angst, dass er sie nur gedeckt hatte, um ein solches Zertifikat
von ihr erpressen zu können, und sie nun, da es ihm nichts mehr brachte,
verraten könnte. "Ich verstehe dich.", sagte sie sanft. Manchmal half
es, Sympathie zu wecken, Verständnis zu äußern. "Ich war auch mal in einer
Situation, in der ich gern an Ausbildung gekommen wäre, aber es war
schwierig, weil meine Lebensumstände das nicht vorsahen."

"Das glaube ich dir sogar.", murrte Matrose Ott. Er schaute ihr auf
die Oberweite, die sie, seit Wache Luanda an Bord war, ja nicht
mehr wegfalten konnte und sie nun durch passendes Tragen des
Mantels des Nautikas zu verbergen versuchte. Er sah auch
rasch wieder weg. "Du läufst sicher nicht
ohne Grund als Mann herum. Es ist mir aufgefallen. Wir müssen darüber
nicht reden."

Lilið zuckte leicht zusammen. "Lieber nicht." Interessanterweise
hatte Lilið mit mehr Blicken seitens der Crew gerechnet. Sie sah
nun anders aus, als bevor Wache Luanda an Bord gekommen war, aber
niemand schien sich dafür zu interessieren. Vielleicht sahen
sich Menschen Körper nicht noch einmal an, oder sie waren davon
ausgegangen, dass Lilið ihre Brüste abgebunden und nun aufgehört
hätte. Lilið griff nach dem Kartenzirkel und dem Navigationsbüchlein
und machte sich an die Arbeit. Sie erklärte dabei mehr als sonst,
aber versuchte, trotzdem auszusparen, was ihr später am ehesten zum
Vorteil verhelfen könnte.

---

Ihr Gefühl, bedroht zu sein, wurde eher noch schlimmer, als sich
Wache Luanda zum Mittagessen ihr gegenüber niederließ. "Du bist
also das Nautika der königlichen Garde gewesen.", leitete diese
ein Gespräch ein.

Lilið nickte. "Nicht lange, aber ja."

"Auf einer Kriegskaterane, die eigentlich das Ziel hatte, die
Kronprinzessin zu befreien.", fügte Wache Luanda hinzu.

Lilið sah keinen Sinn darin, das nicht zu bestätigen, also tat
sie es. "Das ist richtig. Ich wurde allerdings nicht tiefer
eingeweiht, sondern war lediglich in der Funktion eines Nautikas
an Bord."

"Entschuldige, wenn ich das so frei heraus frage: Wie stehst
du zum Vorhaben dieser Crew? Was ist deine politische Einstellung
zu dem Ganzen?" Wache Luanda machte einen freundlichen Eindruck und
die Frage wirkte bei ihr beläufig, aber Lilið machte das
fast noch mehr Angst, als hätte sie sie physisch in die Ecke
gedrängt.

Hatte Lilið je hier an Bord etwas dazu geäußert? Hatte Drude eine
Behauptung aufgestellt, derer bezüglich sie sich konsistent verhalten
sollte? "Ich muss zugeben, ich schwanke ein wenig.", sagte Lilið. Wenn
sie mögliche Veränderungen in ihrer Einstellung einräumte und
gleichzeitig schwammig blieb, würde
sie sich vermutlich am wenigsten leicht verraten. "Ich bin Nautika. Mir
ist es vorwiegend wichtig, meine Arbeit gut zu machen, aber ich habe
mich natürlich mit der Frage ethisch auseinandergesetzt. Ich denke,
dass, eine Person zu entführen, im Allgemeinen keine besonders schöne
Lösung für einen Konflikt ist, muss ich wohl eigentlich nicht
aussprechen. Das wissen wir hier alle. Die Frage ist also, ob die
Lage das trotzdem rechtfertigt. Und die Lage ist komplex, aber einen Krieg
zu vermeiden, ist eine verständliche Motivation." War sie vielleicht
doch eher zu schwammig?

"Das verstehe ich.", sagte Wache Luanda zu Liliðs Überraschung. "Unsere
Landesorakel haben lange debattiert, als wir davon gehört haben, ob
wir König Spers Unterstützung des Ganzen teilen oder ihn öffentlich
kritisieren wollen. Ich bin in den vergangenen Tagen bei mehreren
Versammlungen gewesen. Es gab einiges Hin und Her bei den
Debatten. Du hast Recht, dass die Lage komplex ist. Aber
die Landesorakel haben sich schließlich geeinigt, dass
wir unter gewissen Bedingungen die Verschiebung der
Verhandlungsbasis für König Sper durch die Festnahme der Kronprinzessin
unterstützen wollen. Vielleicht hilft dir, zu wissen, dass wir
die Kronprinzessin in einer Sakrale gefangen halten werden, in
der sie geschützt vor Übergriffigkeiten ist und gut behandelt wird."

Eine Person vor Übergriffigkeiten geschützt gefangen zu halten, empfand
Lilið in sich schon als Widerspruch, aber sie sagte nichts dazu. Sie
wusste, was gemeint war. Eine Sakrale bot vielleicht wenigstens tatsächlich
Schutz vor zum Beispiel körperlichen Übergriffigkeiten von König Sper
persönlich. Sie nickte. "Das stellt schon eine Erleichterung dar."

"Uns war bewusst, dass wir die Kronprinzessin besser schützen können,
wenn wir uns einmischen und den Boden für Verhandlungen stellen.", fuhr
Wache Luanda fort. "Teil der Debatte war, ob wir das deshalb allein schon
tun sollten, weil wir dadurch schlimmeres, andernfalls unausweichliches
Übel reduzieren könnten, oder ob wir durch unsere Einmischung dem Vorhaben
an sich zustimmen. Es war erst nicht klar, ob wir letzteres wollen, aber
die Debatte darüber wäre hinfällig gewesen, wenn wir schon gewusst hätten, dass
wir uns zum Schutz der Beteiligten ohnehin einmischen müssen."

"Hätte es in eurer Macht gestanden, das Vorhaben zu unterbinden und
die Kronprinzessin zu befreien?", fragte Lilið. "Ich frage
hypothetisch, nicht weil ich denke, ihr hättet das tun
sollen, sondern nur, um eure Optionen zu kennen." Lilið wusste, dass die
Orakel bei ihr daheim keine solche Macht gehabt hätten, aber im Königreich
Sper sah das vielleicht anders aus.

Wache Luanda schüttelte den Kopf. "Nicht auf offiziellem Wege
sozusagen.", sagte sie. "Wir standen aber nicht ohne Grund unter Verdacht,
für das Entkommen der Kronprinzessin auf Lettloge verantwortlich
zu sein. Das war ein Überfall auf eure Kagutte, von dem bis jetzt niemand
einen blassen Schimmer hat, wie er möglich gewesen sein könnte, selbst
mit dem Wissen, dass die Kronprinzessin nachts nicht in ihrer Zelle
war."

Lilið nickte und runzelte gespielt nachdenklich die Stirn. Es war
wieder einer der Momente, in denen sie damit rechnete, verhört zu
werden. Vielleicht sehr subtil. Auf der anderen Seite fragte sie sich,
in wiefern Wache Luanda gegen die Entführung der Kronprinzessin sein
könnte und es ebenso wenig laut aussprechen konnte wie sie.

"Es spricht dafür, dass hier eine oder mehrere Personen an Bord
sind, die unter der Nase von Wache Drude eine eher unbekanntere
Magie-Art ausführen können.", fuhr Wache Luanda fort. "Vielleicht eine,
für die ein fortgeschrittenes wissenschaftliches Verständnis von
Nöten ist, das zu Erfühlen wir zu ungeübt sind."

"Magie erfühlen funktionierte irgendwie, indem ihr erkennt, dass
Substanz in eurer Umgebung nicht den natürlichen physikalischen Gesetzen
folgt, sondern Schwingungen darin resonieren, die von außen beeinflusst
sind, richtig?", erkundigte sich Lilið. Sie hatte in der Schule nicht
so recht aufgepasst, als es dran gewesen war, aber Marusch hatte ihr
davon erzählt, als es um die Beschaffenheit der Dinge gegangen war.

Wache Luanda nickte. "So ungefähr."

"Lernt ihr dann zuerst so etwas wie Veränderungen von Wärme zu
erfühlen, weil das das ist, was die meisten Menschen in Magie als
erstes lernen?", fragte Lilið.

Wieder nickte Luanda. "Und wenn jemand zum Beispiel Masse verändern
könnte, dann weiß ich nicht, ob ich es mitbekommen würde, weil es
sehr fortgeschrittene und seltene Magie wäre."

Wie nett von ihr, dachte Lilið, das selbst zur Sprache zu bringen. Ob
sie einen Verdacht hatte? "Das hört sich nach extrem komplexer
Magie an.", sagte sie, und meinte es auch. "Aber ich kann mir gerade nicht
vorstellen, wie eine Veränderung der Masse bei besagtem Entkommen
hätte genutzt werden können. Gibt es noch andere Magie-Arten,
die du weniger gut erfühlen könntest, aber die hilfreicher für
so etwas sein könnten?"

Wache Luanda lachte. "Du hast schon recht, dass es viel Fantasie braucht,
mit Masseveränderungen eine Flucht zu planen. Ich gebe zu, dass ich mir
seit heute Morgen, seit ich über das Rätsel informiert worden bin,
Gedanken über deine Frage mache. Ich komme auf Masseveränderungen, weil
es ungefähr das einzige ist, wovon ich mir vorstellen kann, dass ich nichts
davon mitbekäme."

Ob Wache Luanda das Pulsieren des Igeldings spürte? Liliðs Gedanken
stockten. Sie nahm es wahr. Sie wusste nicht, wo sich das Igeldings
aufhielt, aber sie rechnete damit, dass es unten in der Kagutte mit
Lajana in der Zelle war. Sie nahm das Pulsieren unter der Haut wahr wie
das Zupfen einer Kompassnadel, wenn Lilið sich beim Navigieren besonders
entspannte, nur um einiges stärker. Aber es war auch
eine Physik, die im Schulunterreicht kurz kam, kaum erforscht
war und zu der es keine Magie gab, von der Lilið bis jetzt gehört
hätte. Was hatte es mit diesem Igeldings auf sich?

Lilið widerstand gerade so rechtzeitig dem Drang, die Schwingung selbst
nachzuahmen. Ob das Igeldings sie dann auch wahrgenommen hätte? Aber
ihr war es doch etwas zu riskant, direkt unter der Nase eines Antimagas
Magie auszuüben, die sie noch überhaupt nicht gewohnt war. War es
überhaupt Magie? Es fühlte sich nicht so sehr wie welche an, einfach
Schwingungen zu produzieren, die nur mit einem Gefühl speziell
dafür wahrgenommen werden konnten und ansonsten nicht viel
Einfluss auf die Umwelt hatten. Ob sie es überhaupt konnte?

"Bist du an Bord, um den Fall aufzulösen?", fragte Lilið.

Wache Luanda schüttelte den Kopf. "Wir haben das kurz überlegt, der
Kapitän und ich, aber sind zum Schluss gekommen, wenn wir es zufällig
herausfinden, ist gut. Ansonsten stehen genug von uns hinter dem Unterfangen,
dass wir unsere Kräfte lieber bündeln wollen, um rasch anzukommen und Vorkehrungen
zu treffen, dass es kein weiteres Mal passiert.", sagte sie. "Ich
bin Antimaga. Ich kann also Magie nicht nur erfühlen, sondern auch
die Anwendung unterbinden. Das hast du vielleicht noch nicht mitbekommen?"

Lilið fragte sich, ob sie es bisher nur von Drude mitbekommen hatte
oder auch offiziell. "War das irgendwann im Gespräch zwischen Wache
Drude, Wache Schäler und dir auf dem Weg zur Kagutte gefallen? Ich
erinnere mich nicht genau.", sagte sie. "Ich hatte schlecht geschlafen
und bin dann von Wache Drude mitten in der Nacht mitgenommen worden. Es ging
alles sehr schnell, daher habe ich mir vielleicht nicht alles
genau gemerkt."

"Das verstehe ich.", erwiderte Wache Launda. "Ich bin jedenfalls
wie gesagt Antimaga. Zudem ist meine Ausbildung im Magie Erspüren wahrscheinlich
qualitativ besser als Drudes, haben wir im Gespräch herausgefunden. Es
dürfte erheblich schwerer für wen auch immer werden, einen weiteren
Befreiungsversuch zu wagen."

Lilið gab Wache Luanda in dem Punkt uneingeschränkt recht. Sie
überlegte, dass sie Wache Luanda schon entweder überzeugen oder irgendwie
aus dem Weg räumen müsste, um eine Chance zu haben, innerhalb der
nächsten zwei Tage eine Befreiungsaktion durchzuziehen.

Sie hatte noch das Gift von Allil, fiel ihr ein. Aber wie gut ein
Körper eine Dosis Gift wegsteckte, war schwer einzuschätzen. Das
wollte sie eigentlich nicht an Bord einer Kagutte mit nur einem
Schiffsmedika probieren, auch nicht, um Lajana zu befreien.

Sie seufzte innerlich, verabschiedete sich und brachte ihr
Essgeschirr zur Wache, die sie bis jetzt nur dösend kannte, die
es zum Spülen entgegennahm. Immerhin
wurde sie von dieser in kein Gespräch verwickelt.

---

Selbst wenn sie Lajana von Bord schaffen könnte, -- und sie hatte
nicht den blassesten Schimmer, wie --, bräuchte sie auch
dann eine Reisemöglichkeit zurück. Nach dem Mittagessen passierten
sie die Seenplattenströmungen. Lilið war dazu an Deck und beobachtete
die Wirbel und gegeneinander schlagenden Wellen von der Reling aus. Die
Kagutte erzitterte, als sie von ihnen ergriffen wurde, aber ansonsten
passierte nichts. Die Strömungen bildeten zugleich die Grenze zum
Königreich Sper. Es war nur die Psyche, sagte Lilið sich, als sie sich
sofort fremd und verloren fühlte. Sie hatte noch nie das eigene Königreich
verlassen und sie glaubte, hier nicht erwünscht zu sein.

Wenn Heelem Lettloge erreichte und erkannte, dass sie
dort nicht mehr waren, würde er
sich betrogen fühlen? Oder würde er auf die Idee kommen, dass etwas
nicht geklappt hätte, und ihnen nachfahren? Würde er herausfinden können,
wohin sie weitergefahren wären? Oder würde er auf Lettloge endgültig
herausfinden, dass Lilið der Blutige Master M wäre und mit ihr nichts
mehr zu tun haben wollen?

Lilið wurde gegen die Reling gerammt, als die Abe wie aus dem Nichts
auf ihrer Schulter Platz nahm und ihren Schwanz um ihren Nacken
schlug. Lilið griff reflexartig nach ihr und fühlte dabei das
erste Mal bewusst mit den Fingern in die glatten, weichen Schedern
an derem Bauch. Die Abe war also wieder da! Das erleichterte Lilið
sehr. Selbst wenn es ihr keinen Vorteil bringen mochte, war es doch
ein Hoffnungsschimmer und ein gutes Gefühl, dass Drude und die
Abe wieder vereint waren.

Die Abe drückte ihren Kopf gegen Liliðs Wange. Etwas
kratzte seltsam, fühlte sich nach Papier an. Papier würde Lilið
vermutlich mit geschlossenen Augen im Tiefschlaf am Körper
erfühlen können. Sie strich mit der Hand am Körper der
Abe entlang bis zu derem Maul, möglichst unauffällig, und
behielt Recht: Die Abe hatte ein Stück Papier
im Maul, von dem ein kleines Fitzelchen hinausragte, mit dem die Abe
sie im Gesicht berührt hatte. Lilið zog das Papier vorsichtig aus dem Maul
der Abe und betrachtete es, den Körper dem Wasser zugewandt, sodass
ihr nur Leute im Rücken stehen konnten. 'Klo' stand darauf. Lilið
steckte das gefaltete Papier in eine Tasche des Mantels des Nautikas und versuchte dabei,
die Bewegung genauso aussehen zu lassen, als würde sie die Abe
streicheln. Sie fragte sich, ob sie demm ein Danke zuflüstern sollte,
aber als sie die Hand sinken ließ, tappste der Abendrache auf ihren
Kopf und startete von dort aus einen Sinkflug über das Meer. Das Manöver,
wie die Abe direkt über dem Wasser dahinglitt, einen Bogen machte und dann
wieder hoch in die Lüfte flatterte, um die Kagutte wieder zu erreichen,
war ein wundervoller Anblick. Aber Lilið beschloss, nicht lange zu
beobachten, sondern zum Klo zu gehen.

Auf dem Klo traf sie niemanden an. Also holte sie das Papier hervor,
entfaltete es und las:

> Mitte deiner ersten Schlafeinheit bei meiner Tür. Pünktlich. Iss
> das Papier auf.

Diese Nachricht war jetzt nicht unbedingt eine, durch die Lilið
sich entspannter gefühlt hätte. Besonders beunruhigte sie, dass
Drude nicht einmal unterschrieben hatte. Aber dass die Abe die
Nachricht gebracht hatte, war vermutlich ein eindeutigeres Zeichen
dafür, dass die Nachricht von Drude kam, als hätte Drude
unterschrieben. Vielleicht war der Grund dafür, dass sie es
nicht getan hatte, derselbe wie der, dass Lilið den Zettel
essen sollte.

Oder jemand wollte Lilið vergiften. Das war unwahrscheinlich. Vermutlich
dachte Lilið permanent darüber nach, dass sie vergiftet werden
könnte, weil es ihr mal passiert war.

Sie glaubte eher daran, dass sie und Drude keinesfalls beim
Kommunizieren erwischt werden sollten. Lilið las die Nachricht
noch einmal, prägte sie sich ein, und hielt sich abermals
gerade so davon ab, sie zu etwas praktischer Schluckbarem
zu falten, bevor sie sie in den Mund steckte und aufaß. Papier
war nicht ihre Lieblingsspeise, aber es sollte ihr auch nicht
schaden.

Trotz der neuen Ängste erfüllte sie die Nachricht mit Hoffnung.
Vielleicht hatte Drude eine Idee.

---

Die Hoffnung sank zumindest zu einem guten Teil wieder in sich zusammen, als
sie die Karten im Kartenraum nach der Kreuzung der Seenplattenströmungen
anpasste und feststellte, dass sie sich keiner weiteren Insel
nähern würden, bevor sie Belloge gegen späten Nachmittag des Folgetages
schon erreichen würden. Es klappte schon wieder alles viel zu gut. Da
Matrose Ott ihr wieder zusah, ließ sie ihn die übrige Strecke unter
ihrer Aufsicht mehrfach ermitteln, machte Kommentare und stellte
Fragen, in der Hoffnung dabei eine Ausrede zu finden, die Fahrt
aufzuhalten. Eine weitere Untiefe? Aber es gab einfach nichts. Der
übrige Teil der Strecke lag so klar vor ihnen, dass Matrose Ott
nicht einen Fehler machte.

Sie fühlte sich beim Abendessen ziemlich resigniert. Sie waren so nah
dran gewesen, es zu schaffen. Und nun waren sie so weit weg davon. Lilið
kämpfte die Wut nieder. Wieder setzte sich Wache Luanda zu ihr, aber
sie sprachen dieses Mal kaum. Lilið gähnte mehrfach.

Sie wusste nicht, was sie bis zum Treffen mit Drude tun sollte und
die Zeit zog sich zäh dahin. Sie fühlte sich so verschwendet an. Und
als sie endlich im Bett lag, fiel es ihr schwer, nicht einzuschlafen.

---

Sie lauschte auf den Atem der anderen, als sie sich schließlich aus
der Koje stahl. Sie kamen ihr alle sehr laut vor, selbst durch
die geschlossene Tür noch, als sie den Schlafraum hinter sich
schloss. Damit es noch unauffälliger wäre, ging sie
zunächst wieder zum Klo, und erst anschließend einen
Niedergang fernab von den Schlafräumen ins Innere der Kagutte. Ein angenehm
leerer Gang führte sie zu Drudes Tür. Ein Teil von ihr befürchtete, dass
die große Person, die dort an der Wand lehnte, nicht Drude wäre. Aber
wer sonst?

Bevor Lilið sich auch nur ausgedacht hatte, wie sie Drude begrüßen
könnte, legte die Gestalt einen Finger auf die Lippen. Wurden
sie belauscht?

Drude deutete auf Lilið, dann auf sich und schließlich zur Tür.

Lilið interpretierte es so, dass dey sie mitnehmen wollte. Gefaltet? Aber
dazu hatte Drude nichts angedeutet. Wollte Drude sie von Bord schaffen?

Lilið deutete schräg auf den Boden in die Richtung, in der sie
Lajanas Zelle vermutete, und formte deren Namen mit dem Mund.

Drude schüttelte den Kopf und wiederholte die Geste von eben.

"Ich verlasse ohne Lajana nicht", weiter kam Lilið mit ihrem Flüstern
nicht. Sie hätte mit 'die Kagutte' geendet, aber Drude hatte ihr
eine Hand auf den Mund gelegt. Lilið sträubte sich, und vielleicht
tat sie es zu geräuschvoll. Im nächsten Augenblick hatte Drude
sie umgedreht und hielt sie mit einem Arm gegen deren Körper gepresst,
der andere verweilte auf ihrem Mund. Immerhin wusste Lilið jetzt,
dass es wirklich Drude war. Bei der Nähe konnte sie es klar
erfühlen.

"Ruhig.", raunte Drude Lilið ins Ohr.

Lilið versuchte sich zu entspannen und Drude ließ sie los. Zumindest
ihren Mund. Dey schob Lilið Richtung Tür, aber Lilið sträubte sich
erneut.

"Ich gehe nicht ohne sie.", versuchte sie ebenso leise zu flüstern,
wie Drude es getan hatte, aber Drudes Hand landete abermals auf
Liliðs Mund und dieses Mal fühlte Lilið sich nicht dazu
aufgelegt, ruhig zu bleiben. Sie war hier, um Lajana zu retten. Sie
erkannte in der Art, wie Drude sich verhielt, dass es Drude gerade
nicht darum ging. Sonst hätte dey vielleicht so etwas wie 'später'
geflüstert, oder nicht?

Lilið versuchte, Abstand zwischen sich und Drude zu bringen, aber Drude
packte sie unbarmherzig und presste ihren Körper an deren, eine Hand
weiterhin über Liliðs Mund. Lilið stemmte sich mit Kraft gegen Drudes
anderen Arm, und als das nur wenig Auswirkungen hatte, versuchte sie
es mit Kneifen und Kratzen. Drudes um ihren Torso gepresster
Arm rutschte weiter nach oben um Liliðs Hals. Lilið erschreckte sich,
als sie realisierte, was er da tat. Drude presste ihren Hals in
dere Armbeuge, Oberarm und Unterarm drückten dabei gegen je eine
Seite des Halses gegen die Hauptschlagadern, der Kehlkopf
lag geschützt in der Ellenbeuge. Wieso tat Drude das? Wieso
erfasste Lilið das so klar.

Sie merkte den Sauerstoffmangel im Gehirn zügig. Sie trat Drude mit
so viel Schwung wie ihr möglich gegen das Schienbein, aber Drude
zuckte nicht einmal. Diese Person war einfach zu stark. Lilið
fragte sich, ob sie sich wegfalten könnte. Aber dazu hätte sie viel
Konzentration gebraucht. Und es würde sie verraten. War es nun
noch wichtig, dass sie sich nicht verriete? Aber sie hätte auch
Luft gebraucht. Ihr wurde sehr schwummrig. Sie konnte längst
in der Dunkelheit nicht mehr sehen.

Ihr letzter halbwegs klarer Gedanke war, dass sie so tun könnte, als
würde ihr Körper erschlaffen, weil Drude dann vielleicht aufhören
würde, oder dey kurz darauffolgend vielleicht nicht mit einem
Angriff rechnen würde. Aber dazu kam sie nicht mehr, bevor sie das
Bewusstsein verlor.

---

Als sie wieder zu sich kam, war sie im Wasser. Drudes Flossenhand
lag sanft unter ihrem Kinn, überstreckte ihren Hals und bettete ihren
Kopf auf dere Brust. Drudes andere Hand hielt eine von Liliðs Händen
verdreht unter ihrem Rücken. Auf diese Weise war Liliðs Körper
in einer Haltung stabilisiert, in der kein Wasser in ihre Atmungsorgane
käme und sie sich nicht einfach losmachen könnte.

In den ersten Momenten nach dem Aufwachen hatte Lilið sich frei
von Emotionen und tiefenentspannt gefühlt. Nun war sie sauer. Sie
hatte Drude vertraut!

"Lilið?", hörte sie Drude Stimme sich nach ihr erkundigen.

Lilið fragte sich, was sie antworten sollte, oder ob sie so tun
sollte, als wäre sie tot. Aber als sich eine Welle über sie
ergoss, musste sie husten.

"Lilið? Bist du wieder da?" Drude unterbrach das Schwimmen und drehte
Lilið, nun auf der Stelle schwimmend, zu sich herum. "Es tut mir
leid.", sagte dey, als sie in Liliðs waches Gesicht blickte.

"Dass du mich umbringen willst?", fragte Lilið.

"Lilið!" Drude klang mit einem Mal sauer. "Weißt du eigentlich, wie
beschissen sich das anfühlt? Die beste Freundesperson, die ich je hatte,
gegen deren Willen bis zur Handlungsunfähigkeit zu würgen? Und zuvor, als
ich gemerkt habe, dass die Crew gegen dich ist und sich von nichts
anderem überzeugen lassen würde, dir in den Rücken
zu fallen, damit sie mir weiter vertrauen? Ich wollte
das nicht, nichts davon, aber ich wollte dein beschissenes Leben
retten!"

"Was ja nett ist, aber lässt du vielleicht mich entscheiden, ob ich
es riskieren möchte, um Lajana zu helfen?" Lilið versuchte,
die Wut ebenso zum Ausdruck zu bringen, aber dazu war sie noch zu
schlapp. "Vielleicht ist dir das irgendwann einmal aufgefallen, dass
ich das tun möchte, weil das so ziemlich die ganze Zeit ist, was
ich getan habe."

"Ist es, und da habe ich nichts gegen.", sagte Drude. Dere Stimme
zitterte auf einmal anders. "Ich sehe nur pragmatisch, dass du an
Bord derzeit nicht nur mit deinem Leben spielst, sondern deine Hinrichtung
zum Ende der Reise bereits fest eingeplant ist. Und tot bringst
du Lajana nichts."

Liliðs Wut schlug in Verwirrung und in irgendein unangenehmes
Gefühl von Verlorenheit um. "Meine Hinrichtung ist geplant?", wiederholte
sie. "Ich dachte, Matrose Ott hat mich gedeckt. Sie verdächtigen mich
höchstens, aber sind sich zu unsicher."

Drude schüttelte den Kopf. "Bis auf Wache Luanda gehen alle an Bord
davon aus, dass du es warst. Sie wissen alle, dass du der Blutige Master
M bist, und glauben, du wolltest Lajana aus irgendwelchen Gründen
befreien oder gar für dich rauben.", berichtete Drude. "Sie machen
dir etwas vor, damit du dich sicher fühlst und ihnen nichts tust. Und
damit du weiter deine Dienste erfüllst. Aber sobald sie dich nicht
mehr brauchen, was morgen früh vielleicht schon der Fall sein wird,
planen sie, dich rücklings und unerwartet zu ermorden."

"Du hattest doch so geschickt gefragt und Wache Schäler hatte mich
auf dem Phantombild nicht erkannt, oder war das gelogen?" Lilið
fühlte sich interessanterweise enttäuscht, so etwas nicht selbst
geahnt zu haben. Warum war dieses Gefühl jetzt wichtig?

"Das war bereits kodiert. Wir Wachen haben unsere eigene Art,
für außen Zuhörende etwas anderes zu sagen, als wir
untereinander verstehen.", informierte Drude.

Lilið seufzte schwer. Ihre im Wasser rudernden Arme fühlten sich
bleiern an. "Matrose Ott weiß es auch?"

"Ja. Das wusste ich aber, als Wache Schäler von seinen Lügen erzählt
hatte, noch nicht.", antwortete Drude. "Er hatte selbst vorgeschlagen, so zu tun,
als würde er dich decken, weil es dich vielleicht dazu bringen könnte,
wertvolles Wissen an ihn preiszugeben oder ihn auszubilden. Davon habe
ich später an Bord bei Tischgesprächen erfahren. Ich bin positiv
überrascht davon, wie wenig du darauf angesprungen bist."

Lilið hörte auf, mit den Armen zu rudern, weil sie sich fühlte, als
würde eine Welt zusammenbrechen. Eine Welt, die eigentlich nie wirklich
intakt gewesen war. In dem Moment, in dem sie unter Wasser sank, war
Drude sofort bei ihr, nahm ihren Körper sanft von hinten in den Arm
und bettete ihn auf deren. Wieder schwammen sie, aber nun
langsamer.

"Ich bringe dich heute Nacht nach Belloge in eine Höhle, die ich gut
kenne. Dann kehre ich zurück zur Kagutte. Ich sollte morgen im
Morgengrauen wieder dort sein.", schlug dey vor. "Mit etwas Glück
kaufen sie mir ab, dass ich einfach ein wenig spazierschwimmen
war. Das mache ich oft. Sie kaufen es mir vielleicht ab, weil
sie dir abkaufen, dass deine Magie und
deine Tricks beachtlich genug sind, dass du allein von Bord gekommen
bist. Ich suche dich wieder auf, wenn wir angelegt haben und
sich unsere Crew zerstreut. Mit allen Informationen, die ich
kriegen kann, um dann mit dir Lajana aus einer Sakrale zu
befreien. Was auch ein brenzliches Abenteuer wird, aber nicht
so unmittelbar totbringend, wie es für dich wäre, länger an
Bord zu bleiben. In Ordnung?"

Lilið spürte tiefe Erschöpfung in sich. Kam ein Teil davon vielleicht
noch von der Bewusstlosigkeit? Oder von dem Wissen, dass Drude sie vor
einer Hinrichtung bewahrte? "Wie sicher bist du dir, dass du
nicht hingerichtet wirst, wenn ihr ankommt?"

"Ich werde es rechtzeitig herausfinden.", antwortete Drude. "Ich
kenne die Leute. Ein Risiko bleibt, aber ich halte es für das
beste, es einzugehen, wenn wir Lajana helfen wollen."

Lilið legte eine ihrer Hände auf Drudes Arm, der sie hielt. "Ich
vertraue dir." Drude hatte vorhin so etwas Extremes gesagt. Dass
Lilið die beste Freundesperson wäre, die dey hatte. Das hatte
sie gleichzeitig berührt und zerrissen. "Es ist so
traurig, dass ich deine beste Freundesperson sein soll. Denn
ich fühle mich nicht besonders gut darin, überhaupt eine
für dich zu sein."
