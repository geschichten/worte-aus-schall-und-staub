Anziehsachen
============

*CN: Verfolgung, Überfall, Einbruch, Überhören von Stöhnen beim Sex, Misgendern.*

Lilið blickte dem Segelboot hinterher, das Hermen wieder gen
Schleseroge steuerte. Er war in einem anderen Winkel von Angelsoge abgelegt, als
sie angelegt hatten, weil die kleine Insel, die Lilið noch in
der Ferne erblicken konnte, bereits weitergezogen war. Hermen hatte
nicht lange gezögert, wieder aufzubrechen, nachdem er sein Bedauern
zum Ausdruck gebracht hatte, Lilið nicht beim Start hier begleiten
zu können. Er hatte ein Gefühl dafür entwickelt, wie er Menschen
ansprach, die er auf einer zur Monarchie gehörenden Insel
traf, sodass sie ihm wohlgesonnen wären und er sprach allerlei
Sprachen. Für Lilið war das im wahrsten Sinne Neuland. Immerhin
wurde auf Angelsoge auch Baeðisch gesprochen, die Sprache, mit der
Lilið groß geworden war. Die derzeitige Königin, zu deren Königsreich
auch Nederoge gehörte, hatte hier ihren Regierungsitz. Und die
Sprache, die auf einer Insel gesprochen wurde, war durch das Königreich
vorgegeben, zu dem eine Insel gehörte.

Lilið war nicht zum ersten Mal auf Angelsoge, aber die Insel beeindruckte
sie auf Anhieb wieder genau so wie damals. Die Küstenlinie
machte nicht einmal einen gebogenen Gesamteindruck, so groß
war die Insel. Wenn sie sie überqueren wollte, wären es sicher
mindestens zwei Tagesmärsche, eher mehr. Und sie hatte keine Idee, wo sie
auf der Insel gelandet war.

Sie war damals mit der Schule hier gewesen, ein Ausflug über vier Tage. Sie hatte
eigentlich gar nicht mitgewollt. Vier Tage mit Mitlernenden, von denen die
meisten sie nicht mochten, und die, die sie mochten, so tun würden, als täten
sie es nicht, war keine Aussicht gewesen, auf die sie sich gefreut hätte. Sie
hatte vom Ausflug nur ein paar Erinnerungen an Einzelereignisse. Der Moment, in
dem sie die Zentral-Sakrale gesehen hatte, hatte sich zum Beispiel eingebrannt.
Das beeindruckende Säulen- und Kuppelgebäude mit quadratischer
Grundfläche und einem absichtlich zugewuchertem Hof darin lag im Herzen der großen Hafenstadt
Angelsedde. Die kleinere Halbinsel von Angelsoge, auf der Angelsedde gebaut
worde war, verwalteten Lord und Lady von Pik, die zweitmächtigste
Familie der hiesigen Monarchie. Liliðs Vater trieb mit ihnen Handel. Er beschrieb
sie als strategisch. Nicht generös, aber auch nicht knausrig den ihnen
Schutzbefohlenen gegenüber. Lilið hoffte, dass sie ähnlich wie ihr Vater mit
Langfingern umgingen, die sich bloß Lebensmittel stahlen. Noch hatte sie etwas
Proviant und vielleicht fand sie rechtzeitig einen anderen Weg, an neue
Lebensmittel zu gelangen, aber es war durchaus nicht ausgeschlossen, dass
sie irgendwann aufs Stehlen angewiesen sein würde.

Zu den Dingen, die sie bei sich getragen hatte, als sie unfreiwillig
von Bord gegangen war, gehörten auch die Marken und der Ring von
Herrn Hut. Auch andere Marken, die sie bei Gelegenheit mal bei anderen
Personen mitgehen lassen hatte, die ihr nicht nett gegenüber gewesen waren.

Den Ring hatte sie schon längst genauer inspiziert. Es war der Name
'Lanja' eingraviert, also war es vermutlich ein Ehering, den
Herr Hut lieber verbarg, wenn er unseriös sein einseitiges Interesse an
Nähe bekundete. Es machte Lilið diese Person noch unsympathischer.

Sie war sich nicht sicher, wie sie davon profitieren könnte,
einen Ehering zu tragen. (Und sich als mit Lanja liiert auszugeben, wovon
aber niemand mitbekommen hätte, denn wer nahm schon anderer Leute
Ehering vom Finger, um sich die Gravur anzusehen?) Deshalb beließ sie
den Ring vorerst in ihrer Jacke.

Die Marken waren interessanter. Marken wurden Schutzbefohlenen von ihren
Herrschaften gegeben, damit sie Beschaffungen für sich oder die besagten
Herrschaften machen konnten. Kleidung wurde über Marken ausgegeben und
angepasst. Lilið überlegte, dass es tatsächlich hilfreich wäre, mehr
Kleidung zu haben, und es war in ihrem Schatz auch eine Kleidermarke
dabei. Ihr dünnes Abendkleid für den Ball war ja an Bord geblieben. Ihre
erste Überlegung, wie sie trotzdem auf den Ball gelangen könnte, war
mehr in Richtung Dienstkleidung gegangen. Zuvor herausfinden, ob dort
Uniformen getragen würden, oder ob von verschiedenen Hofstaaten
Dienstpersonal dort wäre, sodass sie sich in einer klassischen Diensttracht
vielleicht hätte ein Tablett stibitzen können, um sich so unbemerkt
hineinzumischen.

Aber sie hatte nur eine einzige Kleidermarke. Und diese war
für einen Anzug. Sie war von Herrn Hut, also würde die Zeichenfolge
auf der Marke, die Lilið selbst nicht interpretieren
konnte, vermutlich bedeuten, dass sie damit einen schniekeren
Anzug bekommen würde.

Auf ihrem Weg zwischen den Feldern hinter den Dünen hindurch, den
Schildern in Richtung der nächsten kleineren Stadt folgend, machte
sie sich Gedanken darüber, was sie für Alternativen hätte. Sie könnte
natürlich versuchen, sich möglichst unauffällig und bedeckt zu halten, aber
dann würde sie nicht auf diesen Ball gelangen können. Irgendwann
würde sie etwas riskieren müssen, damit sie es schaffen könnte. Sie
hatte auch nicht viel Zeit, sich erst einmal zurechtzufinden, weil
der Ball schon morgen Abend stattfand.

Wenn sie sich nicht daran machte, den Ball zu besuchen, dann würde
sie Marusch vermutlich nicht so ohne Weiteres wiederfinden. Aber
um Marusch zu finden, war sie schließlich hier. Also, folgerte
sie, war es vermutlich die beste Strategie, in einer kleinen
Stadt mit einer Schneiderei die Sache mit dem Anzug zu probieren,
und wenn dabei etwas zu sehr aus dem Ruder liefe, zu hoffen, dass
sie Land gewinnen konnte, um als nächstes die Strategie mit
der Dienstkleidung zu probieren.

---

Die Stadt, die sie erreichte, hatte vielleicht 30 bis 40 Häuser, eine kleine
Sakrale, die sehr traditionell wirkte, -- der Baum in ihrer
Mitte war auch sehr hoch und sehr alt --, eine Besuchsstätte und ein
paar Läden, darunter tatsächlich eine Schneiderei. Lilið überlegte,
sich ein wenig zu falten, um vielleicht einen männlicheren Eindruck
zu machen. Aber es hätte wenig Sinn. Sie würde zum Ball eine ganze
Weile in dem Anzug stecken. Solange könnte sie eine Faltung nicht
aufrecht erhalten. Und es würde besonders auffallen, wenn sie
die Faltung aufhob, wenn der Anzug dann nicht mehr auf sie
maßgeschneidert wäre. Sie brauchte eine andere Begründung. Aber so
richtig fiel ihr keine ein, außer, dass sie eben einfach Anzüge
mochte oder so etwas.

Sie betrat die kleine Schneiderei und fühlte sich sofort
fehl am Platz in ihrer salzverkrusteten Kleidung. Sie sah immerhin
nicht wie auf der Straße lebend aus. Noch nicht zumindest. Die
Situation war so kurios: Sie hatte Bedenken, bald auf der
Straße zu leben, und besorgte sich einen Anzug.

Nun ja, es war genau das Leben, das sie sich gewünscht hatte. Voller
Absurditäten und Abenteuer.

"Du schaust dich erst einmal um?" Eine der beiden Personen hinter dem
Tresen hatte deren Gespräch unterbrochen, um sie einzubinden, falls
sie wollte.

Lilið sah sich nur flüchtig um, um sich zu orientieren, trat aber dann
direkt an den Tresen und legte die Marke auf den Tisch. "Ich komme
von außerhalb. Ich musste leider unerwartet früher aufbrechen, sodass ich
mir den Anzug für den Ball nicht in meiner gewohnten Schneiderei habe
anfertigen lassen können.", sagte sie und legte dabei gelassene Überzeugtheit
in ihre Stimme.

Die Person am Tresen nahm die Marke entgegen und betrachtete sie
eingehend. Lilið suchte in ihrem Blick nach Skepsis, fand aber
keine. Als nächstes vermaß der geübte Blick jener Schneidersperson
ihren Körper. "Du trägst derzeit grün. Ist das deine Farbe?"

Damit hatte Lilið nicht gerechnet. Sie lächelte freudig
überrascht. "In der Tat!", antwortete sie. "Auch wenn ich etwas
unsicher bin, ob ich damit nicht doch zu sehr auf dem Angelsoger
Adelsball auffallen werde." Sie hoffte, dass die Marke
einen Anzug für eine Person hergab, die dort willkommen gewesen
wäre.

"Warst du noch nie dort?", fragte die zweite Person hinter dem Tresen.

Lilið schüttelte den Kopf. "Es wird mein erstes Mal.", sagte
sie. "In den letzten zwei Jahre, in denen ich eingeladen war, ist es
jeweils ins Wasser gefallen. Einmal wegen eines Krankheitsfalls
in der Familie und" Liliðs Kopf ratterte, um sich möglichst
flüssig einen zweiten Grund auszudenken "einmal ist auf der
Reise ärgerlicher Weise nicht alles glatt gelaufen."

"Der klassische Mist?", fragte die zweite Person. "Zwischen den
Reiseinseln verheddert, was ja oft zu so ein bis zwei
Tagen Verspätungen auf den einfachen Reisekagutten führt, meine
ich."

Lilið nickte bestätigend und seufzte tief. Sie hatte keine Ahnung von
Reisekagutten. Ihr Vater wählte für Reisen immer die etwas größeren,
zuverlässigeren Reisefragetten aus, und nun bekam sie einen Eindruck
davon, warum. Einfach Bestätigen erschien ihr in jedem Fall als
eine gute Möglichkeit, Sympathie und Vertrauen zu erwecken.

"Wer sich heute alles Nautika nennen darf, ist mir auch immer
wieder ein Rätsel.", seufzte die zweite Schneidersperson hinterm
Tresen. "Aber kommen wir zurück zum Thema: Es gibt auf dem
Angelsoger Adelsball durchaus gewisse Kleidernormen, wenn ich das so nennen
kann. Du wirst allein mit einem Anzug aus dieser Norm fallen,
das ist dir vermutlich klar. Es kommen aber auch sicher
gut ein Zehntel zu diesem Ball, gerade
um aufzufallen. Mach dir also keine Sorge, dass du am Ende die
einzige Person wärest, die etwas aus der Reihe tanzt. Passt das
für dich?"

Lilið nickte zögerlich. "Warst du schon auf diesem Ball?"

"Wo denkst du hin?", fragte die Schneidersperson. Dieses Mal
lag unverkennbar Skepsis in Stimme und Gesichtsausdruck. "Ich
habe als Schneider natürlich ausführliche Informationen und
bekomme Skizzen angefertigt. Aber selbstverständlich war ich noch
nie dort."

"Es tut mir leid. Das hätte mir klar sein müssen.", lenkte Lilið
rasch ein. "Ich bin ein bisschen nervös."

"Dir muss überhaupt nichts leidtun.", mischte sich wieder die erste
Schneidersperson ein. Sie warf der anderen einen unauffällig
tadelnden Blick zu, der Lilið trotzdem nicht entging. "Du sagtest
ja schon, du bist unerfahren. Ich hoffe, wir können das
Aussuchen und Anpassen so gestalten, dass du dich wohl bei uns
fühlst. Es ist uns eine Ehre, dich bekleiden zu dürfen." Sie
schob der anderen Scheidersperson die Marke zu, die nun auch
einen längeren Blick darauf warf, nickte und sich bei Lilið
entschuldigte. Diese Marke musste wirklich was bedeuten.

---

Das Aussuchen und Anpassen dauerte fast drei Stunden. Lilið
ließ sich in Sachen Farbe und Stil beraten, nahm fast
alle Vorschläge an und wünschte
sich selbst lediglich, dass sie sich gut im Anzug
bewegen können wollte und er leicht sein sollte.

Als sie sich am Ende in dem dunkelgrünen Anzug im Spiegel anblickte, dessen
Stoff zwar außen ein wenig rau war, aber der bei verschiedenem
Lichteinfall trotzdem schillerte oder glänzte, fand sie sich
selbst sehr schick. Auch wenn er ihre Figur nicht verbarg, sodass
sie vermutlich weiter als Frau wahrgenommen werden würde, fühlte
er sich nach genau dem Dazwischen an, das sie am liebsten sein
wollte. Sie bekam einen einfachen Beutel mit, um den Anzug
heile zu transportieren, weil sie ja selber keine ausreichend
große Tasche dafür dabei hatte. Sie hatte sogar flache
Tanzschuhe in einem ähnlichen Stil dazu bekommen. Es waren
einfache Stoffschuhe mit weicher Sohle. Die Schneiderspersonen
gingen davon aus, dass sie darin tanzen würde, aber sich für
den Zweck, draußen herumzulaufen, noch andere Schuhe
besorgen würde. Lilið hatte keine Marke dafür und würde darauf
verzichten, aber teilte das natürlich nicht mit.

Sie bedankte sich, und verließ die Schneiderei, nun
wieder in ihrer alten Kleidung und barfuß. Sie war froh, endlich
draußen zu sein. Es waren angespannte drei Stunden
gewesen. Und nun, da sie wieder unter freiem
Himmel war, atmete sie gefühlt gieriger die Luft
ein, als nach ihrer ähnlich langen Schwimmorgie. Es roch
grün und nach frischer Erde.

Als nächstes musste sie sich also darum kümmern, innerhalb ungefähr eines
Tages nach Angelsedde zu gelangen. In der Schneiderei hatte
sie erfahren können, dass der Ball dort stattfinden
würde, ohne danach zu fragen. Es war einfach im Gespräch gefallen.

Sie hatte nicht gewagt, die Schneiderspersonen zu fragen, wie weit
es bis dorthin wäre, weil es doch auffällig gewesen wäre, wenn
sie den Ball als ihr Ziel angab, aber nicht wusste, wie sie
dahin gelangen könnte. Also stellte sie die Frage erst Passierenden
im nächsten Dorf, als sie es erreichte. Und seufzte innerlich
sehr auf, als sie erfuhr, dass sie die Großstadt nicht
zu Fuß im vorgegebenen Zeitraum erreichen würde. Wie das wohl hier
mit per Anhalter fahren aussah?

---

Es war nicht so einfach wie auf Nederoge. Es
fuhren mehr Fahrzeuge, aber es hielten viel weniger an, und
die, die anhielten, hatten nicht Angelsedde als Ziel, sondern
fuhren immer nur ein Stück in die Richtung. Sie kannten es schon,
dass per Anhalter Fahrende nicht so weit mitgenommen würden, und
boten sich an, weil jedes Stück mitgenommen werden zählte. Am
Abend verkroch Lilið sich heimlich in einer Scheune zwischen den
Feldern, um zu übernachten. Sie hatte Hunger, aber versuchte sparsam
mit ihren Vorräten umzugehen.

Sie war darauf vorbereitet, dass vielleicht jemand nach dem Rechten
sehen könnte, und faltete sich samt Gepäck zu einem Strohhaufen, als
sich die Tür öffnete. Es handelte sich allerdings um eine andere
landstreichende Person, die sich hier umsah und es sich schließlich
gemütlich machte. Lilið tat, als würde sie sich aus dem Strohhaufen
wühlen, der sie eigentlich selbst war, und grüßte.

"Ist das okay, wenn wir hier zu zweit sind?", fragte sie.

Die andere Person war wortkarg und nickte einfach.

Lilið war auch nicht darüber überrascht, im Morgengrauen davon
aufzuwachen, dass die andere Person versuchte, Liliðs Gepäck zu
klauen. Sie wehrte sich, trat um sich, schaffte es tatsächlich, die
andere Person kurz abzuschütteln, rappelte sich auf und
rannte aus der Scheune, die andere Person dicht auf den
Fersen. Sie tat, als würde sie sich hinter der Tür verstecken,
die die andere Person einfach weiter aufsperren und ihr ins
Gesicht schlagen wollte, aber Lilið faltete sich schmal
zusammen, sodass sie sich zwischen den Holzpanelen, aus denen die
Scheune bestand, hindurchschieben konnte und
wieder in den Innenraum trat. Sie hatte in der Nacht auf ihrem
Gepäck geschlafen und ihre gesamte Kleidung so lange am
Körper gefühlt, dass sie die Faltung hinbekam. Im Inneren
versteckte sie sich hinter einem echten Strohberg.

Die landstreichende Person suchte draußen noch eine Weile nach ihr,
aber vergeblich. Die Kunst, sich durch papierbreite Ritzen
zu schieben, war nicht sehr verbreitet, also rechnete sie
vielleicht mit einer anderen Erklärung für Liliðs
Verschwinden, die ausschloss, dass Lilið wieder in
der Scheune sein könnte. Liliðs Anspannung ließ erst nach, als sie
schon eine Weile allein war. Nachdem sie sich ein wenig erholt hatte
brach sie auf und frühstückte auf dem Weg.

---

Als sie Angelsedde schließlich erreichte, war es schon fast
Abend. Sie war müde, aber auch erleichtert. Der Ball fand in einem
Schlossgebäude und auf dessen Grund statt. Das Grundstück war umgeben
von hohen Mauern. Lilið beobachtete, bereits umgezogen, eine Weile,
wie die Eingangskontrolle ablief. Einige, vielleicht bekannte,
Personen wurden ohne Umschweife eingelassen. Andere mussten etwas
Ausweisendes oder eine Einladung vorlegen, die gründlich untersucht
und mit der Haut der Besuchenden abgeglichen wurde. Das Vorgehen
wirkte auf Lilið nicht, als hätte sie eine
Chance mit einer guten Geschichte.

Als auch noch eine Person abgewiesen wurde und später einem Menschengrüppchen
in ihrer Gegend erklärte, dass ihr die Einladung gestohlen worden sei und sie
daher nicht eingelassen werden würde - womit sie jedoch schon halb gerechnet
habe -, beschloss Lilið, einen anderen Weg zu probieren.

Es gab einen Balkon im Schatten, über den sie vielleicht in
den ersten Stock des Gebäudes gelangen könnte. Leise Musik drang
aus den Türen zum Balkon, immer wenn diese geöffnet wurden, aber ansonsten
war es ruhig. Es lehnten sich keine Leute auf die Brüstung, um
hinabzuschauen. Und die zwei Wachen, die darunter positioniert
waren, schauten sich nur sehr gelegentlich flüchtig um. Lilið
beobachtete, dass sie es immer dann taten, wenn die Musik kurz lauter
wurde, also, wenn die Balkontür geöffnet wurde.

Ihr war schleierhaft, warum der Balkon nicht gründlicher
überwacht oder mehr genutzt wurde, aber sie beschloss, den
Einstieg darüber zu versuchen. Es schien ihr
nicht völlig ausgeschlossen, es zu schaffen, dort
hinaufzuklettern, und eben sich immer dann, wenn sie die Musik lauter
werden hörte, kurz in eine Ranke oder so etwas zu falten, sodass
sie nicht gesehen würde. Ranken wuchsen hier jede Menge, aber keine, die
stabil genug zum Klettern gewesen wäre. Dafür würden eine Abflussrinne
und einige Ritzen im alten Gemäuer herhalten müssen.

Sie holte einige Male sehr tief Luft, um sich in einen Igel zu
falten, und in dieser Form an der Wache vorbeizuhuschen. Das war eine äußerst
komplizierte Faltung. Sie hatte früher, um sich an Menschen
vorbeizuschleichen, auch gelegentlich probiert, ein Eichhörnchen zu sein. Aber
zum einen konnte sie nicht so flink und elegant huschen wie
ein Eichhörnchen, und zum anderen sahen Leute bei Eichhörnchen aus
irgendwelchen Gründen genauer hin. Igel waren dunkler, die
Form verwaschener. Es wunderte sich niemand, einen Igel zwar
gesehen, aber nicht so richtig wahrgenommen zu haben.

Sie schaffte es an den Wachen vorbei und hielt erst an, als sie
krabbelnd mit der Nase gegen die Mauer stubbste. Die Perspektive
war ungewohnt und sie konnte kaum atmen. Ihre Eingeweide fühlten
sich deplatziert an. Noch in Igelform drehte sie sich um, um zu sehen,
ob sie beobachtet würde. Ihr brach der Schweiß zwischen den Stacheln
aus, als sie den Blick einer Wache genau auf sich spürte. Der
Schreck und der Schweiß verursachten, dass sie ihre Form unter
den Stacheln für einen Moment nicht perfekt halten konnte, aber
sie hoffte, dass die Stacheln das gut verbargen.

Die Wache tippte der anderen auf die Schulter und zeigte auf Lilið. Sie raunte
ihr leise etwas zu, was Lilið nicht verstand, weil es in ihren
viel zu kleinen, ungewohnten Ohren rauschte, von denen sie erst
herausfinden musste, wie sie sie spitzte. Sie versuchte, sich
trotz Atemnot und leichtem Schwindel sehr zu konzentrieren, und konnte
tatsächlich die Worte ausmachen, die die andere Wache schließlich
erwiderte: "Niedlich, aber wir haben hier eine Aufgabe zu befolgen. Lass
dich nicht zu leicht ablenken." Dann blickten beide wieder zurück
auf die leere Seitengasse vor sich.

Puh. Lilið richtete sich langsam einatmend aus der Faltung wieder
auf und lehnte sich gegen die Wand in den Schatten. Sie
war ein bisschen stolz darauf, das
mit dem neuen Anzug hinbekommen zu haben. Es spielte dabei eine
Rolle, dass er so gut saß und dass er sich so gut anfühlte. Am
Anfang ihrer Faltübungen hatte sie oft mit neuer Kleidung
Schwierigkeiten gehabt, sie mitzufalten.

Sie blickte sich ein letztes Mal nach den Wachen um, und als diese
keine Anstalten machten, sich zu rühren, begann sie ihre Kletterpartie.

Sie hatte sich das einfacher vorgestellt. Sie hatte es sich auch
nicht einfach vorgestellt, nur einfacher. Immerhin kletterte sie mit Gepäck und
in einem schicken Anzug eine Mauer hinauf, die nicht zum Klettern
gedacht war, mit der Gefahr im Nacken, dass sie entdeckt werden
könnte. Aber sie kletterte auch eine Mauer hinauf, an der Gestrüpp
wuchs, das Geräusche verursachte, wenn sie darin hängen blieb, und
daran hatte sie nicht gedacht. Sie
musste ihr Klettern also mit dem Wind abpassen oder zusehen, dass
sie eben nicht hängen blieb, und wenn doch, sich vorübergehend selbst
zu Gestrüpp falten, damit sie nicht gesehen würde, wenn die Wachen
glaubten, etwas gehört zu haben, und sich umwandten. Und
das durfte ihr nicht zu oft passieren.

Sie hatte das Mäuerchen gerade erreicht, als die Musik wieder lauter
wurde, faltete sich reflexartig in eine Ranke, die im Wind
wehte, sich dort aber auch gut halten konnte, und dachte, es wäre
zu spät. Das wäre das eine Mal zu viel, dass die Wachen sich umdrehten. Aber
es war ja Musik und nicht Rascheln, das sie sich umwenden ließ. Etwas
was eben immer wieder Mal passierte. Sie schauten nicht lange zu
ihr hinauf, drehten sich gelangweilt wieder gen Gasse, und
im nächsten Augenblick glitt Lilið auf den Balkon.

Sie erkannte nun zügig den Grund dafür, dass der Balkon so leer war. Es war ein
Erotik-Balkon. Hinter einem Vorhang aus Tuch und Ranken auf der
einen Seite hörte sich rascher werdendes, heißes Atmen. Unter dem Sichtschutz
lugten grellblaue Spitzenunterwäsche und der Zipfel eines blau und
schwarz gemusterten Abendkleides hervor. Auf der anderen Seite
gab es auch so einen blickdichten Bereich, der aber den Anschein
erweckte, leer zu sein.

Lilið trat zwischen den beiden abgetrennten Bereichen hindurch zur
Tür, öffnete sie und wollte hindurchschreiten, aber eine Wache, die den Eingang
von innen bewacht hatte, stellte sich ihr in den Weg. "Ich kann
mich nicht erinnern, dich auf den Balkon treten gesehen zu
haben.", sagte sie. Ihr Tonfall war freundlich, aber machte zugleich
unmissverständlich klar, dass es hier noch etwas zu bereden
gab.

Lilið schüttelte den Kopf. "Ich habe mich auch gewundert, dass
ich dabei nicht bemerkt worden bin.", sagte sie.

"Kannst du mir deine Einladung zeigen?", fragte die Wache. "Das
lässt sich ja dann einfach und schnell klären."

"Mir war nicht bewusst, dass ich sie auf dem Balkon brauchen
würde.", murmelte Lilið eingeknickt. Sie versuchte die Rolle einer
Person zu spielen, die wusste, dass sie etwas falsch gemacht hatte, und auf
Großzügigkeit angewiesen wäre.

Die Wache musterte sie von oben bis unten. "Eine Frau in einem
Anzug. Das bedarf auch einer Sondergenehmigung, weil es gegen die
Etikette ist.", gab die Wache zu verstehen.

Lilið wurde sehr heiß. Das hatten die in der Schneiderei nicht
gewusst? Hätte sie das aus der Andeutung schließen sollen, dass
sie in einem Anzug auf diesem Ball durchaus aus der Reihe
tanzen würde? Waren sie einfach davon ausgegangen, dass Lilið
eine Genehmigung hatte? "Das wusste ich nicht."

Die Wache runzelte die Stirn. In Lilið verknoteten sich die Eingeweide
nun ganz von selbst, auch ohne sich in einen Igel
zu falten. Sie hatte keine Chance, zu entkommen. Sie
konnte sich nicht einfach vor der Wache falten. Wenn Wachen erst
sicher wussten, dass es eine Person gab, die zu fliehen versuchte, war
das etwas ganz anderes, als unter unwissenden Wachen unbemerkt
einen Balkon hinaufzuklettern. Lilið wurde dabei bewusst, dass
ihre Hände, die noch von der Mauer voller Steinstaub klebten, sie
auch verraten würden. Sie verschränkte sie hinter dem Rücken
und rieb sie erst aneinander und dann möglichst unauffällig in
der Innenseite des Anzugs ab, während sie nach vorn hin ungefähr
so verzweifelt spielte, wie sie sich fühlte, nur in unschuldig.

"Allil!", hörte sie eine andere vertraute Stimme, die sich ihre Eingeweide
auf ganz andere Weise wickeln ließ.

Marusch!

"Sie gehört zu mir.", versicherte Marusch der Wache.

"Sie braucht trotzdem eine Einladung.", erklärte die Wache.

Marusch suchte in der kleinen, zu ihrem Abendkleid passenden Handtasche
nach einem Stück Papier und reichte es der Wache. "Wir sind für die
Tanzaufführung hier. 'Das Tanzdesaster von
Lilið aus der Unterwelt' aus dem Tanzzyklus von Bangelesch. Daher bin ich
im Kleid hier und sie im Anzug. Sie hatte sich gerade nur umgezogen."

"In dem Fall müsstest du mir ein Abendkleid zeigen können, dass du
angehabt hattest." Die Wache richtete sich wieder an Lilið, immer noch
skeptisch, aber nicht mehr in einer Weise, die keine Hoffnung auf
einen besseren Ausgang ließ.

"Ich habe es auf dem Balkon liegen lassen.", sagte Lilið. "Ich
war überzeugt, dass der Balkon als Umkleide gedacht ist, wegen
der Sichtschutzvorhänge und der Privatsphäre. Ist es in Ordnung,
wenn ich kurz auf den Balkon gehe, und es hole?"

Die Wache überlegte kurz, nickte und richtete sich an Marusch. "Du
stehst im Zweifel für deine Begleitung ein, wenn sie nicht wiederkommt?"

"Selbstverständlich." Maruschs Stimme war klar und fest. Und ließ
Liliðs Nerven abermals aufflammen.

Lilið betrat den Balkon erneut. Ja, es wäre vielleicht verhältnismäßig
einfach gewesen, nun zu entfliehen und Marusch hängen zu lassen.
Und da sie auf dem Ball gewesen war, wüsste Marusch nun auch, dass
sie sie wiedersehen wollte. Es wäre vielleicht auch nur fair gewesen, wenn
sie sie im Gegenzug auch in Schwierigkeiten gebracht hätte. Aber
Lilið wollte kein Zurück.

Der Atem der Personen in der Kabine war in einen schnellen Rhythmus
übergegangen. Eine der Personen stöhnte dabei. Lilið bückte sich
und zupfte das Abendkleid, das darunter hervorlugte aus der Kabine.
Das Stöhnen und die zarten, erotisierenden Geräusche, die gelegentlich von
der anderen Person dazukamen, ließen sich davon nicht ablenken.

Lilið hoffte, dass sie Gelegenheit haben würde, das Kleid zurückzubringen, aber
konnte nicht abschätzen, wie wahrscheinlich sich
eine ergeben würde. Sie legte sich das Kleid ordentlich
über den Arm und besah es sich skeptisch. Es war schon ein
auffälliges Kleid. Wenn sich die Wache erinnern würde, dass sie
vorhin eine Person damit auf den Balkon treten gesehen hatte, dann
waren sie geliefert.

Lilið überlegte, das Kleid in ein anderes zu falten. Aber dazu kannte
sie es noch nicht gut genug. Der Gedanke brachte ihr aber die rettende
Idee. Sie legte es wieder vor der Kabine ab, in der das Stöhnen nun wieder
abgebbt und in wohligere, genießende Geräusche eines Nachspiels
übergegangen war, packte ihre Jacke aus, die sie wie kein
anderes Kleidungsstück verstand, und faltete sie in ein Abendkleid, wie
sie es an Bord der Reisefragette zurückgelassen hatte. Es kostete
sie nicht allzu viel Atem und Konzentration. Das sollte sie hoffentlich
lange genug durchhalten. Als sie sich sicher war, dass sie gut
vorbereitet wäre, trat sie mit leicht hängendem Kopf zurück durch
die Balkontür.

"Unten links hinter der Bühne findest du die Tür zu den Umkleideräumen
der Vortanzenden.", erklärte die Wache.

"Vielen Dank.", sagte Lilið leise. Ihre Stimme zitterte.

Sie konnte nicht leugnen, dass sie die Anspannung auch
genoss, aber langsam kam sie nervlich an ihre Grenzen.

"Das Chaos tut uns sehr leid.", fügte Marusch zur Wache gewandt
hinzu. Ihre Stimme klang selbstsicher und einladend dabei. "Meine eigentliche
Partnerin ist leider wegen einer schweren Grippe ausgefallen. Allil
kennt die Gepflogenheiten noch nicht und ich habe versäumt, sie
gut einzuführen. Ich hoffe, so etwas kommt uns nicht wieder vor."

Die Wache nickte. "Habt einen guten Abend. Zur Aufführung werde ich schon
abgelöst sein und werde mir euren Tanz nicht entgehen lassen."

Marusch bot Lilið den Arm an, in den sie sich einhakte. Immerhin diese
Gepflogenheit kannte sie sehr gut und führte sie formvollendet aus. Nun,
da Marusch sie berührte und ihr Geruch ihr wieder so nah war, fühlte sich ihr
ganzer Körper kribbelig unter der Haut an.

"Ich hoffe, du kannst tanzen!", raunte Marusch ihr zu.
