Stille Nacht
============

*CN: Reden über Massaker, Reden über Sex, Schlafmangel,
religiöse Rituale und Kleidung - mehrfach erwähnt.*

Lilið warf einen letzten Blick auf die halb zerstörte Zentral-Sakrale, die im
Mondlicht am Horizont schimmerte. Selbst bei Nacht und über diese
Distanz konnte sie das beeindruckende Gebäude noch klar genug
ausmachen, zeichneten sich die dunklen, durchtrennten Räume gegen
das äußere helle Material der noch stehenden Mauern ab. Sie befanden sich im
Heck einer schmalen, aber hochseetauglichen Yacht. Drude hatte
Lilið und Marusch abgeholt und auf das zu dem Zeitpunkt in
einer benachbarten Bucht ankernde Segelschiff verbracht, mit dem
sie jetzt davonsegelten. Heelem steuerte. Der Pinnenausleger ruhte
gemütlich in seiner Hand. Es wehte mäßiger Wind. Lilið
konnte die Erleichterung kaum in Worte fassen, dass Drude nichts
passiert war.

Marusch hatte sich, kaum an Bord, trockene Kleidung angezogen. Lilið
fand ihre Prioritäten zunächst überraschend, aber es ergab für sie sofort
Sinn, als sich Lajana eng an Maruschs Körper kuschelte und sie
sich gegenseitig trösteten. Oder sich auf andere Art Halt gaben. Sie
weinten beide nicht. Vielleicht hatte Marusch auch nicht
länger als nötig in königlicher Kleidung sein wollen. Nun trug
sie ein klassisches Matrosinnenkleid.

Lilið hockte mit nass am Körper klebender
Sakralutte am Heck und blickte zurück gen Bellim, wo die Überreste
der Sakrale hinter den Horizont tauchten. Sie schwiegen
seit einer ganzen Weile. Es bestand keine Notwendigkeit dazu, wie
in den sakralierten Booten, aber vielleicht war es einfach schwierig,
in dieser Situation etwas zu sagen. Was hätte Lilið sagen können? 'Mir
geht es eigentlich gut, nur erschöpft, und ich möchte wissen, was in
diesem verlurchten Buch stand!', fühlte sich einfach nicht nach einer
angemessenen Reaktion auf die Situation an.

Das letzte Massaker, das sie erlebt hatte, das auf der
Kriegskaterane, war eigentlich viel kleiner
gewesen, und hatte sie mehr mitgenommen. War es, weil sie nun mehr
Ablehnung gegenüber den Getöteten empfunden hatte? So wollte sie nicht
denken. Es war mehr das Gefüge, das quälend auf der Welt lastete, das
hier angekratzt worden war. Es hatte vielleicht mit dem Tod König
Spers einen Riss bekommen. Einen feinen.

Lilið hatte schon wieder den Eindruck, dass ihr Denken irgendwie
falsch war.

Sie bemerkte nicht sofort, dass sie vor Kälte zitterte. Eigentlich
war es eine laue, sternenklare Nacht. Aber sie war extrem erschöpft
und nasser Stoff klebte schwer auf ihrer Haut.

"Was brauchst du?", fragte Drude sie und durchbrach damit die
Stille. Es war kein schneidendes Durchbrechen, eher ein sanftes,
um das sich die Stille direkt wieder schloss, wie das Wasser
hinter den sakralierten Booten es getan hatte. Die Abe lag in Drudes
Schoß und ließ sich streicheln.

"Was sollte ich brauchen?", fragte Lilið.

"Ich weiß es nicht." Drude machte auf Lilið einen vielleicht sogar
verzweifelten, aber zumindest aufgewühlten
Eindruck. So hatte Lilið demm noch nie erlebt. "Du hast gerade
etwas Schlimmes erlebt.", setzte Drude wieder an. "Aber ich kann
nicht erfühlen, wie es dir geht. Ich weiß nicht, ob du sehr hart
verdrängst oder das alles wegsteckt. Ich wäre gern da für dich, aber
ich weiß nicht wie."

Lilið schüttelte den Kopf. "Wahrscheinlich sollte ich etwas Schlimmes
fühlen. Trauer vielleicht. Oder Wut?", überlegte sie. "Also bräuchte
ich erst einmal so ein Gefühl und dann bräuchte ich erst Halt oder
so etwas. Aber ich fühle mich eigentlich", Lilið haderte plötzlich
doch, es auszusprechen, und endete mit Verzögerung: "erleichtert."

"Fühlt es sich richtig an?", fragte Drude.

"Was passiert ist?" Wieder durchlief Lilið ein Zittern.

"Das Erleichterungsgefühl.", konkretisierte Drude.

Lilið schüttelte den Kopf. "Ich schäme mich deswegen, glaube ich. Ich
habe Angst, zuzugeben, dass ich mich erleichtert fühle, weil ich denke,
ich müsste anders fühlen."

"Zieh dir trockene Sachen an und komm wieder her. Dann erkläre ich
dir, dass deine Gefühle in Ordnung sind.", befahl Drude. Es war
ein sanfter Befehl, hinter dem sich die Ruhe genau so wieder schloss,
wie nach Drudes erstem Bruch des Schweigens.

Lilið betrachtete demm einen Moment skeptisch. Sie war immer noch nicht
davon überzeugt, auf Befehle zu reagieren. Aber als ein weiteres
Zittern ihren Körper durchlief, gab ihr innerer Widerstand auf. "Was
haben wir denn so an Kleidung an Bord?" Der Mantel des Nautikas lag
noch in der Zivil-Sakrale versteckt. Der hatte nicht unter die Sakralutte
gepasst. Höchstens in gefaltetem Zustand, und das wäre Anwendung von
Magie gewesen, zwar welche, die sie zu recht hoher Wahrscheinlichkeit
hätte verheimlichen können, aber sie wollten kein Risiko eingehen. Und
Wache Luandas Fähigkeiten waren durchaus so beeindruckend gewesen, dass
Lilið bezweifelte, dass sie davon nichts mitbekommen hätte. Liliðs
übrige Anziehsachen waren an Bord der Kagutte geblieben.

"Ich zeige dir die Kleidung!", beschloss Lajana und stand auf.

Lilið folgte ihr in den Bauch des Schiffes. Sie mochte
das Schiffsinnere sofort. Der Niedergang bestand aus einer
Klapptreppe mit fünf Stufen. Rechts neben dem Niedergang
befand sich eine kleine Nische mit Kartentisch, links davon
eine winzige, offene Kombüse, im mittleren Teil des Schiffbauchs
eine schmale Sitzecke und im Bug, sowie links und rechts im Heck
je eine Schlafnische, die für jeweils zwei Personen reichen mochten. Sie
waren zu fünft. Mit wem würde Lilið wohl heute Nacht kuscheln? Und
wer würde allein schlafen? Lilið lächelte bei dem Gedanken, dass
sie sich gegen niemanden aus der Crew wehren würde.

Lajana führte sie über den glatten Holzboden des Unterdecks
zur vorderen Doppelkoje, unter der Stauraum war, der offenbar
mit Kleidung vollgestopft war. Lajana holte mehrere Sätze
Matrosenuniformen hervor und sortierte sie zunächst auf
den Boden. Dabei landete ein vertrautes Bündel daneben: Liliðs
alte Segeljacke, die sie damals Marusch überlassen
hatte. Sie hob sie sofort auf und hielt sie umklammert,
während Lajana ihr die Uniformen anhielt. Sie entschieden
sich für einen Matrosenanzug, der nur ein wenig zu groß war. Lajana faltete
alles übrige wieder gründlich zusammen und räumte es zurück in
das Fach, während Lilið sich umzog. Lilið hängte die nasse
Sakralutte einfach über die Tür zu dieser Koje. Immerhin
gab es eine.

"Lilið, möchtest du irgendwann wieder mit mir Sex haben?", fragte
Lajana.

Lilið hielt beim Zuknöpfen einen Moment überrascht inne. "Ja, würde
ich gern." Sie fühlte sich unsicher. "Wie kommst du gerade darauf?"

"Ich habe nur gerade daran gedacht." Auch Lajana wirkte etwas
verunsichert. "Lilið, ist es in Ordnung für dich, wenn ich mit
Marusch darüber rede? Oder vor den anderen? Oder möchtest du,
dass das ein Geheimnis ist?"

"Von mir aus muss das kein Geheimnis sein.", beschloss Lilið. "Du
darfst da gern frei drüber reden, mit wem du willst."

"Ich glaube, du solltest bald schlafen."

Wieder überraschte Lilið Lajanas Themenwechsel. Aber irgendein Teil
in ihr fühlte sich sehr verstanden. Ein anderer überhaupt nicht. "Wirke
ich müde auf dich?"

"Sehr erschöpft.", antwortete Lajana. "Als hättest du übertrieben
viel erlebt."

Lilið grinste und schloss den letzten Knopf. "Da hast du wohl recht. Mit
beidem.", bestätigte sie. Trotzdem zog sie die Jacke noch an. Diese
alte, lieb gehabte Jacke. Sie fühlte sich nun anders an, aber Lilið hatte
auch nicht die volle Vertrautheit von damals erwartet. Es fühlte
sich dennoch gut an, sie wieder zu haben. "Ich glaube, ich kann noch nicht
schlafen. Da sind noch zu viele unbeantwortete Fragen."

"Welche denn?", wollte Lajana wissen.

"Was stand in dem Buch?", nuschelte Lilið. Marusch hatte es noch nicht
beantwortet, weil Drude aufgetaucht war. Lauter fügte sie hinzu: "Warum
war ich auf einmal mit Heelem zusammen? Wo kamen Heelem und Marusch her? Was
ist jetzt unser Plan? Und warum hast du gesagt, Marusch würde für dich
nicht existieren, als ich dich nach ihr gefragt habe? Moment, du hast gesagt,
dein Geschwister würde nicht für dich existieren und dann Marusch als
deine Freundin ausgegeben."

"Ich habe gesagt, mein Bruder existiert für mich nicht. Weil ich nie
einen Bruder hatte." Lajana klang beinahe verärgert. "Und ich sehe
nicht, warum ich meine Schwester nicht auch als Freundin bezeichnen
darf, wenn sie eine ist."

"Ah, das ergibt sehr viel Sinn." Lilið erinnerte sich an das Gespräch
damals mit Lajana zurück und nickte.

"Außerdem haben Marusch und ich uns abgesprochen, was ich sage, wenn
mich jemand nach meinem Bruder fragt.", sagte Lajana. "Wir gehen jetzt
wieder hoch, Drude erklärt dir die Sache mit den Gefühlen, wir gehen deine
übrigen Fragen durch, und dann schläfst du, ja?"

Lilið wiegte kichernd den Kopf. "Warum wollen sich alle um mich kümmern? Du
hast doch auch Scheiße erlebt!"

"Ich habe schon schlimmere Scheiße erlebt." Mit einem Mal war eine Verbitterung
in Lajanas Haltung, Gesicht und Stimme fühlbar, mit der Lilið nicht gerechnet hatte.

Ein Teil von Lilið fühlte eine Verpflichtung, darauf einzugehen, Raum
dafür zu bieten, aber sie fühlte, dass sie nicht die Kraft
dafür hatte, und das ärgerte sie. Sie fühlte sich wohl ähnlich
hilflos wie Drude vorhin. Wie konnte sie nun Unterstützung
und Dasein anbieten? "Möchtest du in den Arm genommen
werden?", fragte sie. "Oder kann ich anders da sein?"

Lajana schüttelte den Kopf. "Marusch ist da.", erwiderte sie. "Aber ich
will mich gerade nicht um meine Scheiße kümmern. Das braucht eh länger. Lass
mich erst einmal dich versorgen."

Lilið widerstand dem Impuls 'Ja, meine Königin' zu antworten und nickte
bloß. Es fühlte sich trotzdem seltsam an, eigentlich kein richtiges
Problem zu haben, aber nun die Person zu sein, um die sich gekümmert
werden würde, während es unter Lajanas Oberfläche zu brennen schien.

Lajana stieg vor ihr den Niedergang hinauf und verkündete an die anderen: "Lilið
ist erschöpft. Sie braucht Antworten, und dann geht sie ins Bett."

Lilið hörte ein warmes Lachen von den anderen, als sie Lajana hinterherstieg.

"Das geht nicht.", widersprach überraschend Heelem. "Für ein paar Stunden
vielleicht. Aber Lilið muss das Steuer übernehmen. Ich habe seit 48 Stunden
nicht geschlafen, und davor auch viel zu wenig. Das war nötig, um
schnellstmöglich hierher kommen zu können. Marusch ist oft nur eine
mäßige Hilfe gewesen, weil die Navigation wenig Spielraum für Fehler
beim Steuern zuließ."

"Schlafmangel. Eine ständige Begleitung.", seufzte Lilið. "Ich kann noch. Ich
bin zwar sehr erschöpft, aber mehr wegen der vielen Erlebnisse. Wenn ich
weiß, wie ich steuern muss und Ruhe einkehrt, kann ich sicher auch
entspannen. Wo geht es denn hin?"

"Das ist alles die falsche Reihenfolge!", beschwerte sich Lajana. "Erst
die Sache mit den Gefühlen. Und vielleicht kann ja auch Drude steuern?"

Drude schüttelte den Kopf. "Ich kann nicht navigieren und habe viel zu
wenig Erfahrung mit Steuerung, als dass ich das sicher machen könnte. Da wäre
Marusch sicherer als ich.", sagte dey. "Heelem und ich haben das schon
geklärt."

"Wir kehren gleich zur Reihenfolge zurück, in Ordnung?", bat Marusch
Lajana. "Ich gehe noch einmal mit Lilið zum Kartentisch und wir
schauen uns den Kurs an, den Heelem geplant hat. Es ist nicht so
kritisch wie auf der Hinfahrt. Ich bleibe mit Lilið wach, wenn es
für sie nicht zu viel Unruhe bedeutet."

Lilið schüttelte den Kopf und stieg wieder hinab. Sie hörte Lajana
etwas unmütig nuscheln, aber dann kehrte wieder Ruhe ein. Marusch
stieg ihr hinterher und entzündete eine kleine Lampe mit den
Fingern, die den Kartentisch in brauchbares Licht
tauchte.

Lilið warf einen Blick auf die Karte,
aber spürte Maruschs Nähe direkt hinter sich und konnte sich nicht
konzentrieren. Sie bemühte sich, ihren Fokus zu kontrollieren, aber
gab schließlich auf, als ihr Maruschs Geruch klar machte, dass Marusch
wirklich hier war und dass sie sicher waren. Sie drehte sich um
und blickte in Maruschs Gesicht, dichter vor ihr, als es eine
Etikette zwischen Unbekannten erlaubt hätte. Nun, sie waren einander
nicht unbekannt. Eigentlich. Sie hatten sich nur lange nicht gesehen. Es
erinnerte Lilið trotzdem an ihre erste Begegnung in der Teeküche
des Besuchshauses.

Sie genoss die Nähe ein paar Momente einfach. Dann streckte sie ihre
Hand nach Maruschs Wange aus. "Darf ich dich berühren?"

"Meinst du, wir kommen dazu, uns die Navigation zu Gemüte zu
führen, wenn du damit anfängst?", fragte Marusch. Sie schmunzelte
auf diese Art, die Lilið kannte und so sehr mochte.

Lilið atmete schneller. "Meinst du, ich kann mich auf die Navigation
konzentrieren, bevor ich", sie wusste nicht, wie sie den Satz
beenden sollte, also tat sie es einfach mitten drin.

Maruschs Hand legte sich auf ihre ausgestreckte, um Liliðs Bewegung
zu Ende zu führen, und streichelte Lilið über den Handrücken. Die
sanften Berührungen waren kaum aushaltbar. Alles war kaum aushaltbar. Lilið
wusste nicht, was sie wollte. Sie wollte Marusch körperlich nah sein, aber
näher, als das physisch möglich war. Im nächsten Augenblick klammerte
sie sich fest an Marusch und vergrub ihr Gesicht in Maruschs Halsbeuge.
Marusch erwiderte die Klammerung.

"Ich liebe dich.", flüsterte Lilið. Sofort fühlte sie
sich seltsam, diese großen Worte ausgesprochen zu haben. Aber sie
fühlte gerade genau das. Oder was war das sonst?

"Eigentlich solltest du mich lieber hassen.", erwiderte Marusch
ebenso flüsternd. "Ich habe gerade einen Massenmord begangen. Ich habe
ihn nicht schon zuvor begangen, um Lajana zu retten, weil es sie vermutlich
traumatisiert hätte. Was bei dir auch noch der Fall sein kann. In jedem Fall zeugt
der Wunsch, so einen Massenmord in erster Linie deshalb sein zu lassen, um geliebte Menschen
nicht zu traumatisieren, nicht unbedingt
von einem brauchbar intakten Werteapparat."

"Marusch!", raunte Lilið in Maruschs Ohr. "Die ganze Situation wäre nicht
entstanden, wenn die Welt nicht vollkommenen Unfug fabrizieren würde, und
zwar nicht nur albernen, sondern schmerzhaften gegen Menschen wie Lajana
oder mich oder auch Drude oder andere. Und du wolltest den Mord nicht
begehen. Oder war der Teil gelogen, dass du mit mir einfach gegangen wärest,
hätten sie uns gelassen?"

"Nein, gelogen nicht. Aber es hätte mir vorher klar sein müssen, dass sie uns sehr
unwahrscheinlich einfach gehen lassen.", antwortete Marusch. "In einer solch
großen Gruppe greifen immer einzelne zuerst an, wenn genug Bedrohung
im Raum steht. Ich wollte es nicht wahrhaben, aber im Prinzip wusste ich,
als ich die Sakrale betrat, wie das ausgehen wird. Dass eine Dynamik entstehen
würde, durch die es auch für weniger aggressive Anwesende dann nach
der sinnvolleren Option aussieht, mich mitanzugreifen, weil sie dann
überhaupt noch eine Chance haben, und dass dabei eine Menge Menschen
sterben würden, die eigentlich sinnvoller empfunden hätten, uns
gehen zu lassen. Ich wusste eigentlich, auch wenn ich etwas anderes gehofft
habe, dass es so enden würde. Beziehungsweise fast. Ich wusste nicht, dass
ich in der Lage wäre, das wirklich zu tun."

"Du wusstest nicht, ob du es moralisch könntest, also, ob du es wirklich
hinbekommst, dich dafür zu entscheiden, oder ob du es
magisch könntest, ob deine magischen Fähigkeiten dazu ausreichen
würden?", fragte Lilið. "Hm, die Frage ist fies, glaube ich. Ich
werte gar nicht."

"Wenn du irgendwann wertest, werde ich es tragen." Marusch küsste sie
zart oberhalb des Ohrs ins Haar. "Ich wusste vor allem letzteres nicht. Ich
übe selten stärkere Magie aus. Ich kenne meine Fähigkeiten nicht vollumfänglich. Ich
wusste auch nicht mit völliger Sicherheit, ob ich mich dazu entscheide, es
wirklich zu tun, aber ich habe so etwas zuvor auch schon getan, in kleineren
Dimensionen. Wobei, ab welcher Dimension gibt es eigentlich kein fälscher
mehr? Ich habe Hemmungen, aber sie fallen in bestimmten Momenten
auch in sich zusammen. Ich ahnte, dass so einer kommen würde."

Lilið streichelte Marusch sanft über den Rücken. "Ich fühle mich
wirklich merkwürdig, weil ihr euch alle um mich Gedanken macht,
außer Heelem vielleicht, aber ich bei dir auch denke, dass du eigentlich
viel mehr Rückhalt und Zuwendung brauchen könntest als ich."

Marusch löste die Umarmung auf. Es war kein schlechter Zeitpunkt
dafür. Lilið fühlte sich sortierter. "Hattest du bei Lajana
ebenso den Eindruck, dass sie eher etwas braucht als
du?", fragte Marusch.

Lilið nickte und gab auch ein bestätigendes Geräusch von sich. Sie
drehte sich wieder zum Kartentisch um.

"Gerade ist sie vor allem erschöpft.", erklärte Marusch. "Sie
will immer einen genauen Plan aufstellen, was wann dran ist, wenn
sie sehr erschöpft ist, und gerät aus der Ruhe, manchmal auch völlig
aus der Fassung, wenn der dann nicht eingehalten werden kann."

"Sie sagte, sie hätte schon schlimmere Scheiße erlebt.", murmelte
Lilið. "Schlimmere als eine Entführung." Sie beugte sich über die
Karte und fühlte sich fast bereit, sie zu verstehen.

"Hat sie.", bestätigte Marusch. "Stell dir vor, du hast eine Mutter,
die eine riesige Garde befehligen kann, und als sie mitkriegt, dass
du entführt worden bist, schickt sie dir eine Kriegskaterane
hinterher. Eine!"

Lilið wandte sich zu Marusch um, als flackerndes Licht auf die Karte
fiel, nur ihren Schatten darauf aussparte. Sie sah die Flammen
gerade noch, die auf Maruschs Haut erloschen.

"Marusch!", donnerte Heelems Stimme zu ihnen nach unten.

"Ich kriege mich wieder ein!", rief Marusch zurück und fügte
für Lilið leiser hinzu: "Heelem kenne ich auch schon sehr lange. Er
spürt das Feuer."

Wieder erinnerte sich Lilið an Heelems Aufforderung 'Mach Feuer!'. Sie
lächelte.

"Kommt ihr da unten eigentlich in die Pötte?", erschall Heelems
Stimme erneut. Sie klang nicht böse, fand Lilið. Vielleicht dieses
Mal sogar amüsiert.

Sie drehte sich um und machte sich endlich daran, die Karte und
die Notizen im Büchlein zu verstehen. Sie steuerten auf eine kleine
Reiseinsel zu, die bei den derzeitigen Strömungsverhältnissen
recht langsam reiste. Sie würden vielleicht einen Tag bleiben
können. Es half ihr beim Fokussieren, Marusch beim Verstehen
und Interpretieren der Daten alles zu erklären.

Als sie fertig waren, atmete Lilið tief durch und
stieg den Niedergang wieder hinauf. "Ich
denke, ich habe alles verstanden. Du kannst mir das Ruder
übergeben.", teilte sie Heelem mit. "Ich habe allerdings kein
Heft für dieses Schiff gefunden, in dem drin steht, bei welcher
Krängung, Takelage und Geschwindigkeit es welche Abdrift hat und
solche Dinge. Da muss ich dir wohl vertrauen."

"Oh!", meinte Heelem. "Der Kartentisch lässt sich aufklappen. Es
müsste unter einem Stapel vollgeschriebener Navigationsbüchlein
stecken. Ich brauche so etwas nicht, ich fühle das Schiff." Heelem
klappte den Pinnenausleger hoch, als er aufstand, um Lilið Platz zu machen.

"Hm, das kann ich noch nicht, glaube ich." Lilið schob sich
hinter ihm hindurch und setzte sich. Als er ihr die Pinne übergab,
fragte sie: "Du vertraust mir, dass ich das schon alles packe?"

Heelem gab ein grinsendes Geräusch von sich. "Natürlich." Mehr
sagte er nicht, bevor er unter Deck verschwand.

Lilið setzte sich und fühlte sich mit einem Mal etwas verloren. Sie
hatte noch nie ein so großes Schiff gesteuert. Auf der Kagutte
wäre sie nie ans Steuer gelassen worden, auch wenn sie wissen
musste, wie es grundsätzlich funktionierte. Sie wusste auch hier
prinzipiell, wie es ging. Aber nun hier zu sitzen und es direkt nicht unter
lehrender Aufsicht zu tun, fühlte sich nach viel Verantwortung an. Und
sie fühlte sich deplatziert, wie, als wäre dies entweder übertrieben
real oder eine Fantasiewelt.

Zu ihrer Überraschung kam Heelem allerdings wenig später wieder mit
einer Decke an Deck. Er setzte sich neben Drude auf die Backbordbank
und schmiegte sich an die Wand neben dem Niedergang. "Ich dachte, ich
folge dem Gespräch noch ein wenig oder beantworte eventuell noch
ein paar Fragen, bis ich hier einschlafe."

"Ist das gemütlich genug, oder soll ich mich zu Marusch und Lajana
setzen?" Drude klang nur halb amüsiert, wenn Lilið nicht alles täuschte.

"Ich will dich nicht vertreiben.", sagte Heelem.

Drude stand seufzend auf, holte Heelem noch ein Kissen aus
dem Schiffsbauch, damit er seinen Rücken abpolstern konnte, und
setzte sich Lilið dann gegenüber
auf einen der Steuerplätze. "Zu meiner
Ausbildung als Wache, sowie bei den Sakrals-Dienenden habe ich
gelernt, dass es in Zeiten, in denen es geht, wichtig ist, sich
zu gönnen.", hielt dey fest. "Ich spüre sehr, wie ihr euch
alle versucht, zurückzunehmen, und in einer Gruppe ist das vielleicht
manchmal auch notwendig, aber wenn ihr einfach für andere entscheidet,
dass es ihnen schaden würde, wenn ihr euch gönnt, ist das nicht
hilfreich und oft falsch. Mir macht es wirklich nichts aus, woanders
zu sitzen."

"Du willst damit sagen, ich hätte dich nicht vertrieben, oder wenn,
dann wäre das schon in Ordnung so?", versicherte sich Heelem. In
seiner Stimme klang wieder ein Schmunzeln mit.

Drude seufzte abermals. "Zum Beispiel. Also, vor allem das."

"Danke." Heelem streckte sich auf der Bank aus und polsterte
sich gemütlich. Als würde er sich erst jetzt trauen, sich
zu gönnen, wie Drude es ausgedrückt hatte. Die
Abe, die von Drude heruntergesprungen war, als
dey das Kissen geholt hatte, kroch zu Heelem unter die
Decke. "Äh", sagte Heelem verwirrt und beendete den
langgezogenen Ausdruck mit einem "Na gut." Die
Abe machte eins ihrer sacht behaglichen Geräusche,
als sie sich erfolgreich und wohl auch zufrieden
miteinander abgefunden hatten.

"Ich kann Lil da wegsortieren, wenn dey stört.", bot Drude an.

Heelem lehnte ab. "Ich habe kein intrinsisches Bedürfnis, Aben zu
kuscheln, aber es ist okay, wenn dey da halt ist."

"Intrinsisch?", fragte Lajana zaghaft.

"Keins, das von mir ausgeht." Heelem gähnte und zuppelte die
Decke etwas weiter zu seinem Kinn, -- sehr vorsichtig auf die
Abe Acht gebend.

Auch wenn er den Drachen nicht kuscheln wollte,
wirkte es auf Lilið liebevoll. Sie hinterfragte, ob es
überhaupt etwas miteinander zu tun haben musste, und kam zum
Schluss, dass nicht: Natürlich konnte
Heelem Drachen gegenüber die selbe Liebe empfinden wie sie zum
Beispiel, unabhängig von einem Kuschel- oder Streichelbedürfnis.

Eine Weile schwiegen sie wieder. Vielleicht, damit Heelem
einschlafen würde. Es war so angenehm still. Lilið besah sich
Kompass und Segelstellung. Die Segel waren hier fixiert, niemand
musste sie halten. Das war anders als auf den kleinen Segelbooten,
die Lilið bisher gesegelt hatte.

"Darf ich was sagen?", fragte Lajana.

"Klar!" Lilið wusste nicht, ob Drude oder Marusch schneller geantwortet
hatten. Sie stimmte ebenfalls zu und hörte schließlich auch ein
zustimmendes Brummen von Heelem.

"Die nächste Frage war, glaube ich, warum Lilið mit Heelem
zusammen war.", sagte Lajana.

Lilið fiel sofort auf, dass Lajana Drudes Vorhaben vergessen hatte, Lilið
darüber aufzuklären, warum es nicht falsch war, was sie fühlte. Aber
Lilið fühlte sich inzwischen nicht mehr so schlecht damit. Also
ließ sie es geschehen.

"Ich weiß nicht genau, was mit der Frage gemeint ist.", fügte
Lajana hinzu. "Seid ihr jetzt auch ein Liebespaar oder
so etwas?"

Heelem lachte. "Nein, das sind wir nicht. Ich glaube, es ging
um den Moment in der Zentral-Sakrale, in dem du nicht mich und
ich nicht dich unter der jeweils anderen Sakralutte vermutet hatten?"

Lilið nickte. Und weil Heelem die Augen geschlossen hielt,
sagte sie "Genau!" Sie zog einen Fuß mit auf die Sitzbank
und legte das Knie in die Armbeuge, die nicht mit Steuern
beschäftigt war. Eine überraschend gemütliche Haltung. Aber
eigentlich hätte sie sich tatsächlich gern so hingekuschelt
wie Heelem. Später. "Ich nehme an, du hast eigentlich Marusch
erwartet?"

Dieses Mal lachte Drude. Wie immer bei demm hielt das Lachen nicht lang an. "Wusste ich es
doch!", sagte dey. "Ich kläre mal von meiner Seite auf: Ich habe nach der
Öffnung der Schleuse, die ich tatsächlich unter Wasser in Gang gesetzt habe, vor
der Sakrale ein unbe", Drude unterbrach mitten im Wort und fuhr dann mit einem Neologismus fort,
"unbepersontes, sakraliertes Boot vorgefunden und mich gefragt, wo Lilið
bleibt. Dann habe ich ein anderes wegsegeln sehen. Es war beinahe außer
Rufweite. Das wart ihr beide dann wohl, Lilið und Heelem. Ich habe aber nicht
hinterhergeschrien, weil ich damit wahrscheinlich beide Boote entsakraliert
hätte. Ich war mir sehr unsicher, ob ich dich, Lilið, voll in eine Falle laufen
lassen würde, oder ob dich weiterfahren zu lassen die beste Chance war, die wir
noch hatten. Zum Glück war letzteres der Fall." Drude gestikulierte
in Maruschs Richtung, als dey fortfuhr: "Dann tauchte Marusch auf, von
der ich direkt gespürt habe,
dass es nicht Lilið ist. Ihr war auch gleich klar, dass ich nicht die Person
war, die sie erwartet hat. Wir haben kurz unsere Kapuzen gelüftet. Wir sind
euch dann hinterher gefahren. Das war unheimlich, weil wir uns nicht absprechen
konnten und uns nicht klar war, worauf wir uns einlassen. Aber als die Schleuse
sich wieder schloss, haben wir doch miteinander geredet. Wir haben uns
gegenseitig gefragt, wann ihr den Tausch herausfinden würdet. Ich habe darauf
getippt, dass ihr das nicht erfahren würdet, bevor jemand von euch sprechen
würde. Und ich freue mich schon irgendwie, dass ich recht hatte."

"Was hast du getippt?" Heelems Stimme klang sehr gemütlich und schon
halb verschlafen.

"Ich dachte, ihr kennt euch, und findet das früher heraus. Ich dachte
mir, dass euch vielleicht aufgefallen sein könnte, dass ihr beide
steuern wollt, während etwas anderes abgesprochen war.", gab Marusch
zu.

"Oh, das ist mir sehr wohl aufgefallen." Heelem klang mit einem Mal
wieder wacher, und auch belustigt. "Ich dachte mir, echt jetzt? In
so einer Situation willst du unvorteilhaft vom Plan abweichen
und doch steuern?"

"Es ist mir auch aufgefallen, aber ich dachte einfach, Drude
wüsste irgendetwas, was fürs Steuern wichtig wäre, was ich nicht
wüsste.", fügte Lilið hinzu.

"Die Antwort auf die Frage, warum Heelem plötzlich mit Lilið in
der Sakrale war, ist jedenfalls, dass wir den selben Plan
hatten.", fasste Marusch zusammen. "In eurem Fall hat Lilið mit
dem Boot vorm Leuchtturm gewartet und in unserem war nur das Boot
vorm Leuchtturm vertäut und Heelem und ich haben den Leuchtturm
gemeinsam nach dem Mechanismus abgesucht. Beziehungsweise,
getrennt von einander, aber eben beide."

"Und als ich durch ein Fenster sah, dass die Schleuse aufging, dachte ich,
Marusch ist so weit.", ergänzte Heelem. "Also bin ich zum Ausgang gerannt. Ich habe
dann durch ein Fenster im Treppenhaus unser Boot auf der anderen Seite liegen
sehen, und dachte, Marusch wartet da schon auf mich. Also bin ich zur anderen Seite aus
dem Leuchtturm gestürmt. Das war dann gar nicht unser Boot, aber das wusste
ich eben nicht."

"Habt ihr euer sakreliertes Boot eigentlich auch aus dem Bootshaus der Zivil-Sakrale
bekommen?", erkundigte sich Drude. "Ich dachte, das wäre der einzige Lagerort
jener Boote."

"Ja.", bestätigte Marusch.

"Hattet ihr dann das Original des Buchs, in das die Verleihungen eingetragen
werden?", fragte Drude. "Bist du da in Probleme gerannt, Lilið? Weil
schon eins vorlag?"

Lilið schüttelte den Kopf. "Die Person dort hat sich das Buch kaum
angesehen, den Kopf geschüttelt und gesagt, wir Halunken." Sie
wiederholte den Ausdruck auf Alevisch. "Ich habe
mich gewundert, aber es lief alles glatt."

Marusch gluckste. "Wir haben das Boot geklaut, indem wir in die
Seitenhalle der sakrelierten Boote eingebrochen sind. Also
ohne Buch.", sagte sie. "Ich nehme an, die Verwaltungsperson hat mitgekriegt,
dass wir es versucht haben, aber nicht, dass wir erfolgreich
waren. Und hat sich dann, als sie das Buch abgezeichnet hat,
darüber amüsiert, was zwei Sakrals-Dienende alles versuchen würden,
um ein Reinigungs-Ritual durchzuführen, das sie nicht ausführen
sollten."

"Richtig, den Hintergrund kennst du auch noch nicht.", mischte
sich Heelem wieder ein und drehte den Kopf etwas in Liliðs
ungefähre Richtung. Die Augen öffnete er dabei nicht. "Marusch
und ich waren es, die die Mitteilung an die Zivil-Sakrale eingefädelt
haben, dass das Reinigungs-Ritual nicht stattfinden dürfe, damit
wir es anstelle der eigentlich vorgesehenen Sakrals-Dienenden
ausführen könnten. Drude erzählte, wie sie sich dann lang und breit
darüber beschwert haben, was ihr dann abgehört habt und weshalb ihr dann auf
die Idee gekommen seid, den selben Plan durchzuführen, den wir
uns erarbeitet haben. So ein Chaos. Könnte fast witzig sein."

"Es ist witzig!", betonte Lilið. "Ich zumindest amüsiere mich."

"Ich mich durchaus auch." Drude klang, wie nicht selten, überhaupt nicht,
als würde dey sich amüsieren, aber Lilið glaubte demm trotzdem.

"Ja, doch, schon." Heelem klang zufrieden und müde.

"Mich braucht ihr nicht zu fragen. Ich fand es sehr ulkig. Vielleicht
einer der elegantesten Misserfolge in meiner Diebeskarriere.", fügte
Marusch hinzu.

"Ich finde das nicht so witzig, weil ich da überall nicht bei war.", hielt
Lajana fest. "Aber das ist in Ordnung. Ich freue mich für euch."

"Ich würde mich noch dafür interessieren, wie ihr das geschafft habt, vor
Ort zu sein.", sagte Lilið. Ein Themenwechsel. Sie merkte, wie sie das
Gespräch ermüdete. Wie sie den Tränen nah war, obwohl nichts Schlimmes
im Raum stand und sie nicht wütend oder traurig war, sondern einfach,
weil sie schon lange mehr als genug hatte. Es war alles viel, sehr
viel.

Marusch seufzte. "Heelem, ich weiß, du willst schlafen."

"Mehr brauchst du gar nicht sagen.", antwortete Heelem. "Natürlich erzähle
ich."

Lilið fragte sich, welche unausgesprochenen Worte im Raum standen. Ohne
es zu wissen, durchrann eine wilde Zuneigung für Marusch
ihren Körper. Schon wieder hatte sie das Bedürfnis, ihr näher zu
sein, als es physisch oder psychisch möglich wäre.

"Fassen wir zusammen: Marusch hat ihre Mutter informiert und gebeten,
Lajana zu befreien und bei der Aktion selbst mitsegeln zu dürfen, weil
sie, nun, gewinnen kann. Was wir sehr eindrucksvoll
erfahren haben." Heelem gestikulierte zum Horizont, wo aber
nichts mehr zu sehen war. Die Abe zuckte dabei, kringelte
sich aber sofort wieder schläfrig unter die Decke. "Aber die Königin hat Marusch
nicht einmal in das Ausdiskutieren von Plänen eingebunden und
entschieden, eine Kriegskaterane wäre genug."

"Lächerlich.", murmelte Drude. "Das ist nicht mehr als ein politischer
Akt, dass niemand ihr nachsagen kann, sie hätte es nicht probiert. Aber
eigentlich hat sie es wirklich nicht einmal probiert."

"Über den Teil reden wir morgen genauer." Heelems Worte ließen
keinen Spielraum für Widerspruch. "Marusch hatte beschlossen, sich
an mich zu wenden, weil ich immerhin mal Nautika der königlichen
Garde war und allerlei Beziehungen habe. Und weil wir zusammen
ein gutes Team bilden. Sie traf etwa mit eurem
Brief bei mir ein. Wir sind dann direkt mit einer Fragette nach
Nederoge übergesetzt und haben dort im Hafen mit einer Marke der
Königin eine Rennyacht bekommen. Wir waren wohl einen halben
Tag zu spät auf Lettloge, um euch zu erwischen. Wir haben ab dort
unauffällig eure Kagutte verfolgt. Als sie auf Belloge ankam,
war Lilið nicht mehr an Bord. Das hat uns Sorgen bereitet, aber
da wir auch nichts über eine Gefangennahme mitbekommen haben,
haben wir uns dann erst einmal ausschließlich auf die Befreiung Lajanas
konzentriert. Den Rest kennt ihr, denke ich."

"Es gäbe viele Details zu erzählen, aber nicht heute.", stimmte
Marusch zu.

Heelem richtete sich ächzend auf, sortierte die schlafende
Abe auf den angewärmten Platz, wo er gerade noch gelegen hatte,
und stapelte Decken und Kissen auf seinen
Arm. "Ich verziehe mich doch unter Deck. Bald geht die Sonne auf und
ich möchte nicht von ihr geweckt werden. Seht zu, dass ihr das Boot
nicht in Brand steckt."

"Ah.", machte Lilið. "Darum geht es."

"Ich beherrsche ausreichend Hyrdomagie, um im Zweifel zu löschen. Immerhin
haben wir Wasser um uns herum. Allerdings müsste ich auch bald
schlafen." Drude gähnte und stand ebenfalls auf.

"Darf ich bei dir schlafen?", fragte Lajana. "Weil ich eigentlich
bei Marusch schlafen will, aber sie will ja nicht schlafen."

"Du darfst gern bei mir schlafen.", stimmte Drude zu. "Weckt mich,
wenn ich etwas löschen soll. Beziehungsweise, wenn ihr viel Magie
ausübt, merke ich das schon von selbst."

"Wir reden einfach über das Buch. Das ist ungefährlich." Marusch
grinste.

Lilið konnte dem Verabschiedungsprozess nicht so ganz folgen. Sie hoffte,
dass sie konzentriert genug wäre, das mit dem Steuern wirklich hinzubekommen. Aber
es war eigentlich derzeit auch keine schwierige Aufgabe. Und eine
stille dazu, für die sie nicht viel Konzentration bräuchte.

Es war schön, dass Marusch dabei blieb. Als die anderen im Unterdeck
verschwunden waren, setzte sich Marusch hinter sie und Lilið konnte sich
bei ihr anlehnen. "Ich habe", Lilið zögerte, "eigentlich habe ich dich
nicht vermisst. Meistens nicht. Ein paar Mal schon.", stellte sie
fest. "Aber ich bin gerade sehr froh, dich wiederzuhaben. Und es erleichtert
mich so, dass ich immer noch dieses warme Gefühl habe, das du in mir
auslöst. Dass das nicht plötzlich weg ist, weil wir uns so lange nicht
gesehen haben."

Marusch küsste sie auf den Schopf. "Ich glaube, ich habe dich auch kaum
vermisst. Aber das ist bereits mehr, als ich je irgendwen vermisst habe. Ich
möchte dich in meinem Leben haben." Ihre Stimme klang weich in
Liliðs Ohren. Nicht flehend. "Wenn du magst."

"Ich mag."

Dann schluckte die Stille der Nacht ihre Stimmen. Sie glitten unter dem
Sternenhimmel dahin. Unter Liliðs Haut zupfte der klanglose Gesang des
Igeldings.
