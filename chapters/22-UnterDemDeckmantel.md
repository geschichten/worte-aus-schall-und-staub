Unter dem Deckmantel
====================

*CN: Misgendern, Mord, Erpressung, Entführung, Damsel in
Distress, Derealisation, vielleicht
Dissoziation.*

Lilið versuchte, zu realisieren, was sie da gerade getan und
erlebt hatte. Aber es gelang ihr nicht.

In ihrer Kindheit hatte sie sich oft Abenteuer ausgedacht, in
denen sie selbst die Hauptrolle gespielt hatte. Oft war sie
in den Tagträumen Nautika gewesen, hatte sich als Mann verkleidet
und andere in dem Zusammenhang über ihre Identität betrogen (weniger
über ihr Geschlecht, sondern weil sie ja ausweisende Papiere
mit anderem Namen gebraucht hätte), und war
zur See gefahren. Nautika war kein Beruf, der Frauen zugetraut
wurde, obwohl es auch weibliche Nautikae gab. Sie
hatte sich in diesem Träumen oft schwer
getan, sich zu entscheiden, ob sie als Protest als vermeintliche
Frau Nautika werden wollte, oder ob sie lieber nicht noch häufiger
darauf angeprochen werden wollte, angeblich eine Frau zu sein
und etwas Ungewöhnliches für eine Frau zu tun. Sie war zum
Schluss gekommen, dass sie in der Realität eher ersteres
getan hätte, aber in ihren Träumen wollte sie den Schmerz
nicht fühlen, wollte sie nicht durch diese Hölle
an fast physischen Schmerzen gehen, die es auslöste, als
Frau eingeordnet zu werden. In der Realität hätte sie trotzdem
am sexistischen Gefüge rütteln wollen und es in Kauf genommen. In
ihrer Traumwelt wollte sie Abenteuer erleben, wo anderes wichtiger
war.

Sie hatte sich Abenteuer in ihrem Kopf ausgemalt, die vielleicht
ungefähr so aufregend gewesen waren wie dieses, in dem sie gerade
steckte. Und genau so fühlte sich dieses an: Losgelöst von der
Realität, als hätte sie sich das ausgedacht. Oder jemand anderes, und
sie würde darin jetzt bloß leben wie in einem Schauspiel. Es gab darin
zu viele Elemente, die in ihrem Alltag nie vorgekommen waren, und
zu viel Grauen, das sie nicht direkt betraf (noch nicht), zu
hohes Risiko, dass alles unüberblickbar und unwiderruflich aus
dem Ruder laufen konnte, als dass sie im Stande wäre, mit dem
Realisieren erfolgreich zu sein.

Vielleicht, wenn sie hätte realisieren können, hätte sie
gewusst, was sie fragen sollte, was gerade wichtig war. So
hatte sie keine Ahnung, welche der vielen Fragen, die sie
hatte, sie priorisieren sollte.

Marusch und sie hatten sich wieder entfaltet, umgezogen und
sich dann rasch aufgemacht. Marusch führte sie am Saum der
Insel entlang, weg vom Dorf. Sie
verweilten nicht am Strand, wo die Ormorane schlief, sondern
spazierten noch etwas weiter in Richtung des kühlen, dunklen
Irkenwaldes am Rande der Hafenbucht nah an der Küste, wo
sie ihr Lager aufschlagen wollten. In den
dünnen, tagsüber silbrigen Blättern flüsterte der Wind und ein
Ulendrache schuhuhte aus der Ferne. Manchmal glimmte ein sachtgrüner
Schein im Wald auf, wenn die Ule Feuer spieh, um sich oder ihren Jungen
Beute zu grillen. Sie drangen nicht tief in den Wald ein und sahen fast
nichts. Marusch machte nur selten etwas Licht, aber war
trotzdem erfolgreich darin, einen versteckten, weniger zugewucherten
Platz nah am Waldrand für ihr Zelt zu finden.

Bis hier hin hatte Lilið geschwiegen, sich
die Zeit genommen, um sich zu sortieren, war aber gescheitert. Nun
beschloss sie, Fragen zu stellen, selbst wenn ihr nicht klar war,
wo sie mit ihnen jeweils hinwollte. Sie hatten zwar mehr Zeit
für die Vorbereitung eines Plans als vorhin, aber eben auch nur höchstens einen
halben Tag und den Rest der heutigen Nacht.

"Wieso liegt dir so viel an der Kronprinzessin?". Das war die Frage, die
sie am meisten beschäftigte. "Oder ist es quasi eine Gelegenheit, etwas
zu tun, was in diese Kategorie von Verbrechen fällt, die wir besprochen
haben? Aber so richtig antiautoritär kommt mir das nicht vor."

Marusch antwortete nicht, sondern fuhr damit fort, das Zelt
aufzustellen. Lilið packte mit an, damit es schneller ginge. Sie
hatten es schon so oft mit schlechten Sichtverhältnissen getan,
dass sie es inzwischen mit Marusch im Dunkeln konnte. Aber
als Marusch auch noch ihre Schlafunterlagen ausbreitete, bohrte
sie doch nach. "Hast du noch vor, zu antworten?"

"Ich denke nach.", sagte Marusch.

Lilið blickte alarmiert zu ihr hinüber, als etwas blau aufflackerte. Sie
hatte in der Stimme auch schon wieder dieses Leblose wahrgenommen. Die
Flammen waren fast sofort wieder weg. "Was brauchst du?", fragte Lilið.
Wann immer Marusch ihr diese Frage gestellt hatte, war sie gut gewesen.

Marusch hielt in ihrer Bewegung inne, verkrampfte einen Moment und
räumte dann ihr Gepäck zurecht. Das war für die Vorbereitung
der Nacht der letzte Schritt. "Können wir uns vors Zelt setzen?", fragte sie.

Lilið stimmte zu und verließ das Zelt. Wenige Momente später folgte Marusch
und setzte sich zu ihr vors Zelt mit Blick auf die Wasserkante, die
sie immerhin im Mondlicht glitzern sehen konnten. "Deine Frage reicht in
meine Vergangenheit rein, über die ich nicht reden möchte.", antwortete
sie. "Es ist vollkommen verständlich, wenn du deshalb jetzt lieber
aus dem Vorhaben, sie zu retten, aussteigen möchtest."

Lilið versuchte, sich nicht enttäuscht zu fühlen. Sie wollte gern
nicht enttäuscht sein. Aber ein unsinniger Teil von ihr hatte
sich gewünscht, dass sie Marusch nun nah genug war, dass sie
ihr davon erzählen würde. Vielleicht wäre es auch etwas anderes gewesen,
wenn Marusch damals nicht angedeutet hätte, dass es sich irgendwann
ändern könnte, sondern stattdessen die Grenze unabhängig von
Wohlfühlen und Vertrauen gezogen hätte.

Ihr Kopf lenkte sie von diesem Gefühl mit einer gewagten Theorie
ab. Jene Gedanken waren vielleicht noch weniger sinnvoll, aber Lilið
konnte sich nicht daran hindern, ihre Hypothese einmal grob durchzudenken:
Sie hatte ja schon damals die Idee gehabt, dass Marusch Wache der Königin
gewesen sein könnte. Auf diese Weise hätte sie eine Vergangenheit
gehabt, in der sie Menschen ermordet hätte und in der es ihr
zugleich leicht gefallen wäre, auf den Angelsoger Adeslball zu
gelangen und sogar die Königin selbst abzuziehen. Was, wenn Marusch
als Wache einmal die Aufgabe gehabt hätte, die Prinzessin zu beschützen,
und es ihr nicht gelungen war? Was, wenn die Prinzessin mit
ihrem Skorem innerhalb ihrer Blutsverwandtschaft deshalb so
aus der Reihe fiel, weil irgendetwas passiert war, ein Unfall
oder so etwas, den Marusch hätte verhindern sollen? Und nun hatte
die Kronprinzessin einen lebenslangen Stempel, all das Schreckliche,
was ihr drohte, hing davon ab, und Marusch fühlte sich in der
Verantwortung.

Dafür, dass sie sich das gerade frisch ausgedacht hatte, ergab die
These überraschend viel Sinn. Leider half sie in sofern nicht
weiter, als dass sie zwar jetzt wusste, dass sie Marusch in so
einem Fall dringend helfen wollen würde, aber sie ja gar nicht
wusste, was wirklich vorlag. Gab es eine Theorie, die sie sich ausmalen
könnte, auf der aufbauend sie Marusch keinesfalls helfen wollte?

"Würdest du bei der Rettungaktion Leute töten, wenn es nötig
wird?", fragte Lilið.

"Ja, auf jeden Fall.", antwortete Marusch ohne Zögern.

Wow, dachte Lilið. Damit hatte sie nicht gerechnet. Sie
schluckte. "Ist es in Ordnung, wenn das für mich nicht in
Frage käme?", fragte sie.

"Dass es für dich nicht in Frage käme, selbst zu töten, oder
geht es darum, dass du mit mir nicht zusammenarbeiten
würdest, wenn ich in eine Situation geriete,
in der meine Wahl zwischen Töten von Personen und Kronprinzessin
in Gefahr zurückzulassen auf ersteres fiele?", hakte Marusch nach.

Das gab Lilið zu denken und reduzierte ihr Schockgefühl von eben. "Es
geht dir nicht darum, dass du gewissenlos töten würdest, sofern
es dich deinem Ziel näher brächte, sondern darum, dass du
es sozusagen aus Notwehr tun würdest?"

Marusch legte nachdenklich einen Daumen an ihre Nase. Lilið war
diese Geste noch nie aufgefallen. "Ich finde die Frage schwierig.", antwortete
Marusch. Sie klang dabei, als würden sie über die Logik hinter
Magie reden, über ein interessantes und sehr abstraktes
Thema. "Zum einen bin ich, wie ich
dir mal erklärte, gewissenlos. Was nicht heißt, dass ich
Töten in irgendeiner Weise in Ordnung oder gar gut fände. Wie
erkläre ich mein Verhalten?" Sie holte tief Luft und blickte
dabei in den Abendhimmel. "Das wird nun technisch, aber du
verstehst so etwas ja. Von jedem Moment aus ergibt sich ein Universum
an Handlungsspielraum, an Pfaden, was von dort an passieren wird,
und die ich durch meine Entscheidungen beeinflussen, aber vielleicht
nicht überblicken kann. Ich schätze ein, welche Entwicklungen wie
wahrscheinlich sein sollten. Und von allen Pfaden, die aus meiner
Sicht am wahrscheinlichsten dazu führen, dass ich der Prinzessin
helfen kann, werde ich, so gut ich kann, den mit dem wenigsten
Schmerz wählen. Unter Schmerz zähle ich Morde, aber auch andere
Art von Schmerz."

Lilið wartete ab, ob Marusch noch etwas hinzufügen würde, aber
das tat sie nicht. "Das klingt berechnend.", sagte sie. Aus irgendwelchen
Gründen spiegelte sie Maruschs Stimmung. Obwohl sie glaubte, dass
sie sich eigentlich grauenvoll fühlen sollte, fühlte sie stattdessen
allmählich eine innere, kalkulierende Ruhe. Wollte sie das?

Marusch nickte. "Ich bin berechnend.", bestätigte sie. Vielleicht,
um Lilið jegliche anderweitige Illusionen zu rauben. "Bei diesem
Plan bin ich es. Wobei es auch sehr viel nicht Berechenbares
darin geben wird. Es wird lebensgefährlich. Unterschätz das nicht."

"Ich glaube, das tue ich nicht." Lilið bemerkte, dass sie
nicht ganz überzeugt klang, was aber, so glaubte sie, nicht
daran lag, dass sie das Risiko als zu klein einschätzte, das
sie eingehen würde. "Ich gehe davon aus, wenn ich mich
als Nautika ausgebe, als das ich an Bord einer Kagutte mit einer
Crew aus dem Königreich Sper anheuere, die die Kronprinzessin
entführt hat, dass ich da nicht einfach
rauskommen werde, wenn ich auf der Fahrt entlarvt würde." Sie
fühlte noch einmal bewusst in sich hinein, ob die Angst nun
endlich einsetzen würde, aber das tat sie nicht. "Ich bin
aber in einem Gefühlsmodus, in dem sich alles unreal anfühlt, wie
eine Geschichte, in der ich lebe, oder in einer Parallelwelt."

Marusch blickte sie lange nachdenklich an. Dann sagte sie: "Jetzt
wo du es sagst, fällt mir auf, dass ich mich vielleicht noch
nie so oft verankert in der Realität gefühlt habe, wie in den
letzten zwei Wochen mit dir." Sie ließ ihren Worten ein paar
Momente zum Klingen, atmete und lächelte. "Aber ich möchte dir
nicht zumuten, dass es dir so geht wie mir sonst. Ich möchte
dich nicht in Abgründe drängen."

"Und doch hast du vorhin von mir schon in der Rolle des
Nautikas geredet.", erinnerte Lilið sie.

"Das tut mir leid." Marusch klang nicht reuevoll, aber Lilið
wusste trotzdem, dass sie es ernst meinte. "Das ist mir in
der hektischen Planungsfindung passiert. Weil, nun, du
bis zur Abfahrt der Kagutte Nautika sein wirst und ein Nautika gebraucht
wird, verstehst du? Das Puzzle-Stück passte zu gut. Aber nichts
gibt mir das Recht, dich einzuplanen. Ich möchte, dass du
für dich entscheidest, ob es dir die Sache wert ist, und zwar
nicht für mich, sondern für dich."

"Und wenn es für mich die Sache wert wäre, weil es dir wichtig
ist?", fragte Lilið.

Marusch antwortete nicht sofort und senkte den Blick. "Das würde
ich akzeptieren.", sagte sie schließlich leise. "Vielleicht würde
ich mehr versuchen, gegen deine Motivation zu argumentieren,
wenn es nicht um die Prinzessin ginge."

"Für die du morden würdest.", wiederholte Lilið.

Marusch nickte einfach nur.

"Was wäre, wenn sich irgendwie ergäbe, dass es der Prinzessin helfen
würde, wenn du mich ermordetest?", fragte sie trocken.

Marusch hob den Kopf wieder und lachte ton- und freudlos. "Ich
halte sämtliche Situationen, in denen das der Fall sein könnte,
für extrem konstruiert und unwahrscheinlich. Ich bin sie trotzdem
durchgegangen.", sagte sie. "Grob zusammengefasst: Wenn du die
Prinzessin nicht bedrohst, sehe ich keinen Grund, wie es dazu
kommen kann, dass du dem Unterfangen, ihr zu helfen, so sehr
im Weg stehen würdest, dass dich zu töten ihr irgendwie helfen
könnte. Wenn du sie bedrohst, dann bedeutet das, dass ich
dich entweder sehr schlecht gekannt habe oder dass du Gründe
hast, die ich derzeit nicht sehe. Ich würde dir eher
vertrauen, dass sie gut sind, als irgendwem sonst, also
dich in dem Fall auch eher unwahrscheinlich ermorden. Etwas
wahrscheinlicher mag es Situationen geben, in denen ich die Wahl
hätte, dich oder die Prinzessin zu retten, aber bei so einer
Wahl, die nebenbei gesagt sehr hart wäre, würde ich dich nicht
aktiv töten, sondern höchstens zurücklassen. Hilft
dir der Einblick? Oder soll ich weiter ausführen?"

Interessanterweise führte Maruschs Ausführung nicht dazu, dass
Lilið sauer wäre oder sich unsicherer fühlte. Sie begriff, dass
sie eine von diesen ethisch miesen Fragen gestellt hatte, die
nur schlimm beantwortet werden konnten. Sie schüttelte den
Kopf. "Es tut mir leid, dass ich das gefragt habe."

"Es ist in Ordnung. Frag alles, was du fragen musst.", antwortete
Marusch. "Ich habe allerdings zugegebenermaßen damit gerechnet, dass
diese Antwort dir reicht, um zu verstehen, wie ich dazu denke."

Lilið nickte abermals. Nun, da dieser Gesprächsfaden abgeschlossen
war, brauchte sie wieder einen Moment, bis ihr neue Fragen einfielen. Die
mit der sie sich nun befasste, war: Würde sie sich dafür entscheiden,
die Kronprinzessin zu retten, wenn es nicht Marusch gewesen
wäre, die das in die Gänge geleitet hätte, sondern sie zufällig
eine Begegnung haben, aus der sich die Möglichkeit halbwegs sicher ergäbe, mit
in einen entsprechenden Plan einsteigen?

Wut loderte in ihr auf, als sie darüber nachdachte. Sie
kannte die Kronprinzessin natürlich nicht und Lilið
war zudem im Allgemeinen von
der Staatsform Monarchie nicht begeistert, aber sie hasste
alles daran, dass eine Person so sehr Spielball der Mächte
war. Ja, die Prinzessin hätte von vornherein abdanken können. Das hätte
Lilið vielleicht besser gefunden, als ihren Plan, Königin
zu werden und keine vorgesehenen Handeslverträge
einzugehen. Sie konnte sich eine Motivation dahinter vorstellen,
dass die Prinzessin so entschied, aber es war einfach
sehr egoistisch, hätte üble Folgen für alle. Aber der Grund, warum
diese Verträge überhaupt vorgesehen waren, war einzig ihr geringer
Skorem, wenn es mal auf das Wesentliche heruntergebrochen
wurde. Ja, es wurde geredet, dass es etwas anderes wäre, wenn
sie gute Kommunikationsstrategien hätte, aber jene wären
auch nur ein Ausgleich. Wenn sie einen hohen Skorem hätte,
hätte sie keine allzu überzeugende Rhethorik beherrschen
müssen. Niemand hätte das verlangt.

So sehr Lilið trotzdem bessere Möglichkeiten für die Kronprinzessin
sah, mit ihrer Lage umzugehen, sei es eben abzudanken oder sei
es bessere Kompromisse oder ein diplomatischeres Verhalten an
den Tag zu legen, um wenigstens das Volk mehr
von sich zu überzeugen, so sehr
verabscheute Lilið auch, dass sich Leute herausnahmen, sie
so entmenschlichend zu behandeln. Lilið glaubte kaum, das
je jemand gewagt hätte, den Kronprinzen oder die Königin
zu entführen. Einfach, weil der größte Teil des Volks
zu viel Respekt gegenüber jenen Personen hätte. Eine entsprechende
Erpressung würde dann nach hinten losgehen. Die Königin war
zu beliebt, weil sie souverän war und bei ihren Entscheidungen
auf das Volk und seine Wünsche einging, oder ihre Entscheidungen
trotzdem nachvollziehbar machte. Der Kronprinz
war mit seinem Skorem von wieviel?
sehr privilegiert, wurde dafür allein schon zu sehr respektiert
und zudem konnte er gefährlich werden.

Ja, Lilið hätte am liebsten die Königin auf ihrem Thron behalten,
aber das stand nicht mehr lange zur Debatte. Und darauffolgend
wollte Lilið sicherlich die Prinzessin nicht regierend wissen,
weil sie die Willkür und Machthungrigkeit fürchtete, aber sie
würde noch weniger wollen, dass das Königreich, zu dem
sie gehörte, König Sper unterfiel,
dieser sexistischen, schmierigen Herrschaftskatastrophe, oder
dass ein privilegierter Kronprinz mit dem höchsten je in einer
Monarchie-Familie gemessenen Skorem überhaupt regieren würde,
der nur sich selber sah. Beides führte vermutlich zu weniger
schlimmen Veränderungen und hatte weniger
Kriegspotenzial, und trotzdem wollte sei beides
weniger. Ihr gefiel an sich der Gedanke einer Frau mit niedrigem
Skorem auf dem Thron (noch lieber natürlich einer Person, die
nicht Frau oder Mann war mit niedrigem Skorem), wenn jene
in der Lage gewesen wäre, zum Beispiel Liliðs Interessen
gut zu vertreten. Was an sich schon schwierig war, weil es
wahrscheinlich ein Anrudern gegen Seeplattenströmungen wäre,
selbst wenn es eine erheblich geschicktere Person wäre als die
Kronprinzessin. Bei jener war aussichtslos, dass sie irgendeine
Gruppe sinnvoll vertreten konnte. Vielleicht wollte sie es
nicht einmal.

Lilið merkte, wie sie sich in Gedanken verhedderte, obwohl
sie eigentlich längst die Antwort gefunden hatte. "Ich würde
gern bei der Rettungsaktion mitmachen. Nicht nur, weil er dir
wichtig ist, sondern auch, weil ich von der Sache
an sich ausreichend überzeugt bin.", sagte sie. "Unter zwei Bedingungen: Das
Buch wird vorher zurückgegeben, sodass mein Vater außer Gefahr ist. Und
ich möchte gern über den Plan dieses Mal möglichst genau im Bilde
sein. Ich möchte gern, dass du mit mir am besten alle Eventualitäten
durchgehst, die du dir selbst schon überlegt hast. Ich möchte
mich in dem Plan am Ende fühlen können, als würde ich ihn mit
dir auf Augenhöhe durchziehen, und nicht als eine Rolle in einem
Spiel, das nur du ganz kennst."

Marusch nickte. "Einverstanden.", sagte sie. "So habe ich mir
das vorgestellt." Sie runzelte die Stirn und fuhr sich durchs
Haar. Auch eine Geste, die Lilið an ihr noch nicht oft
gesehen hatte. Sie fand sie sehr schön. "Also,
für den Fall, das du mitmachst.", ergänzte Marusch. "Für
den Fall ging ich davon aus, dass die Buch-Sache erledigt werden
muss und dass wir alles gemeinsam so gut vorbereiten, dass du mit mir
gleichberechtigt und im Zweifel eigenständig Entscheidungen
fällen kannst."

Lilið fühlte, wie Anspannung von ihr abfiel. Von ihnen beiden. Weil
ein Entschluss gefasst war. Dann ginge es jetzt wohl ans Planen und
Absprechen. Sie fingen mit einem Zeitplan an, der auch ein paar Stunden
Schlaf vorsah und auf den Informationen aus dem Brief des Nautikas
beruhte.

---

Der Mantel eines Nautikas war oft das wertvollste, was es besaß. Das hieß
nicht zwangsläufig, dass er von hohem Wert wäre, außer für das Nautika
selbst natürlich. Manche Nautikae besaßen einfach fast nichts, was
nicht bloß ideellen Wert hatte. Es
kam darauf an. Manche Nautikae trugen durchaus ihre eigenen Karten mit
sich herum. Das machte sie flexibler, weil sie dann auch von Crews angeheuert
werden konnten, die Kartenmaterial nicht selber stellten. Manche Nautikae
mochten auch einfach aus Gewohnheit ihren eigenen Kartensatz benutzen,
in den sie vielleicht sogar Notizen gemacht hatten. Manche Nautikae
kartographierten auf Reisen auch und hatten deshalb eigenes
Material dabei. Eigentlich war üblich, dass größere Schiffe und
Crews Kartenmaterial stellten, aber manche Crews waren aus verschiedenen
Gründen zu schlecht ausgerüstet oder zu
abenteuerlustig oder fanden eben, ein gutes Nautika sollte eigene
Karten mitbringen. In jedem Fall brachte es dem Nautika mehr Flexibilität,
eigene Karten zu haben, aber bedeutete auch ein höheres Risiko, überfallen
und ausgeraubt zu werden. Dieses Nautika, dem sie den Mantel gestohlen
hatten, hatte einen eigenen Satz Karten dabei gehabt, sowie ein
Büchlein, in dem es Kurse geplant hatte. Es war erst vor wenigen Wochen
angebrochen worden.

Lilið hatte trotzdem keine Zeit, alle Einträge durchzugehen. Und
da ihre Schrift erkennbar nicht die selbe war wie die des
Nautikas, war es besser, es nicht
dabei zu haben. Sie warf lediglich einen Blick hinein und besah sich
einige Routen, um sich mit dem Kartenmaterial grob vertraut zu machen
und einen Stil zu erkennen.

Interessanterweise war auch jenes Nautika von Angelsoge hergekommen, allerdings
über eine andere Route mit längeren Zwischenstopps auf größeren Inseln. Aus den
Notizen zu schließen, die es manchmal an den Rand schrieb, hatte es
die Person aus dem Königreich Sper vor zwei Tagen hier kennen gelernt. Es stand
darin, dass sie bei Gesprächen über Politik näher zusammen gefunden
hatten und das Nautika hoffte, dass sie sich einmal wieder begegnen würden. Mehr
nicht.

Neben der Einladung befand sich im Mantel noch ein Empfehlungsschreiben einer
Person, die so krakelig unterschrieben hatte, dass weder Marusch noch
Lilið den Namen entziffern konnten. Marusch hätte gern gewusst, ob dahinter
vielleicht eine Lord- oder Ladyschaft oder so stand (sie hatten sich immer noch
kein neutrales Wort ausgedacht), die sie gekannt hätte. Es wäre interessant
gewesen, zu wissen, welcher Adel der Monarchie wie gegenüberstand.

Abgesehen von ein paar Marken, die ihnen auch nicht weiterhalfen, und
einem kleinen Stofffisch enthielt der Mantel nichts. Den Fisch hätte Lilið gern
zurückgegeben, denn er war sicher von emotionalem Wert für das Nautika
und brachte ihnen überhaupt nichts. Aber dazu war es zu spät.

Jedenfalls, wenn für ein Nautika etwas von Wert war, so war davon auszugehen,
dass es im Mantel steckte, wenn es hineinpasste. Das war einer der Gründe, warum
Marusch den Mantel hatte haben wollen. Sie hatte damit gerechnet, dass sie
darin am ehesten Informationen finden würden, die sie brauchten. Die
Informationen waren mau, aber hatten ihnen weitergeholfen. Lilið hatte sich
darüber gewundert, wie wenig Persönliches darin war, etwa hätte sie mehr
persönliche Briefe erwartet, aber Marusch wunderte das nicht. Sie erklärte, dass
auch das Nautika gewusst hatte, dass es sich in eine gefährliche
Lage brachte und wahrscheinlich Dinge, an denen zu viel Information
klebte, aussortiert und irgendwo auf der Reise bei befreundeten
Menschen oder Familie zurückgelassen hatte.

Ein viel wichtigerer Grund für den Diebstahl des Mantels
war allerdings, dass so ein Mantel eines Nautikas
auch eine Ausweisfunktion hatte. Mäntel, die Nautikae trugen, waren nicht
einfach bloß Mäntel. Sie waren auf sie angepasst und wetterfest, wie Liliðs
Jacke. Sie reichten fast zum Boden, was durchaus Eindruck machte, und das war
kein unbeabsichtigter Nebeneffekt. Nautikae hatten Vorteile davon, als solche
erkannt zu werden. Für Jollen war diese Länge unpraktisch, zumindest
in der Mantelform. Mäntel von Nautikae hatten allerdings Verschlüsse und
Schnüre, mit denen sie unten in eine Hose umfunktioniert werden
konnten. Eine, die so stabil war, dass sie das Körpergewicht des Nautikas
darin tragen konnte. Ein Nautika konnte den in einen Anzug umfunktionierten
Mantel auf diese Art nutzen, um sich für Reparaturarbeiten
in den Mast zu ziehen oder weit über Bord zu hängen, die Füße am
Bootsrand, um ein Boot mit Gewichtstrimm bei noch stärkerem Wind
aufgerichteter zu segeln.

Zu Liliðs Überraschung passte ihr der Mantel fast wie angegossen, wenn
sie nur ihre Brüste wegfaltete. Was sie ohnehin schon halb geplant
hatte, jetzt, da sie es konnte. Sie würde aufpassen müssen. Dauerhafte
Faltung tat dem Körper nicht gut, schmerzte sogar nach einer
Weile, und spätestens nachts im Schlaf würde sich die Faltung wahrscheinlich
von selber lösen. Sie musste zusehen, dass sie
Zeiten an Bord der Kagutte finden würde, in denen sie sich entfalten
könnte, während niemand hinsah.

Aber erst einmal musste sie natürlich überhaupt an Bord kommen.

Sie konnte jedenfalls nicht leugnen, dass sie sich wohl im Mantel
fühlte. Sie wäre natürlich gern ehrlicher an einen gelangt, einen,
der sich dann auch wie ihr Eigentum angefühlt hätt, aber sie hatte sich
in diesen schon vorhin im Imbiss verliebt, als sie ihn das erste
mal erfühlt hatte.

Sie brachen nach einigen Stunden Schlaf wie üblich zum Sonnenaufgang
auf. Den groben Plan hatten sie vorm Schlafen noch festgesteckt, auf
der Fahrt sprachen sie über Einzelheiten und entwarfen
Alternativpläne für den Fall, dass ihr Hauptplan scheiterte.

Lilið steuerte den Handelshafen an, in dem auch das Treffen
stattfinden würde. Dort würden sie sich auftrennen. Während
sie sich im Hafen ihr Zertifikat abholen würde und anschließend
versuchen würde, anzuheuern, würde Marusch die Ormorane alleine
etwa eine Stunde an der Küste entlang segeln, bis sie einen kleinen
Jollenhafen nah des Zweitwohnsitzes erreichen würde, wo sich
so oft Liliðs Mutter aufhielt. Lilið wusste, dass es dort
eine Bootshalle gab, in der nicht selten Boote fremder Leute für einige
Wochen ruhten, ohne dass es auffällig wäre.

Sie hatten abgesprochen, dass Marusch
sich ihrer Mutter offenbaren würde, und dass sie ihre Mutter
einbinden würden. Marusch hatte als Nachweis dafür, dass sie
mit Lilið befreundet war, ihre geliebte Jacke, einen persönlichen,
kurzen Brief, den sie gestern noch geschrieben und in eine
Elikane gefaltet hatte (einem Drachen mit einem etwas überdimensioniert
breiten Maul, der in Sümpfen lebte), und ein Geheimnis, das
am Hof nur ihre Mutter kannte. Irgendetwas Peinliches aus ihrer
Kindheit. Ihre Mutter würde es wahrscheinlich schaffen, Zugang
zu den Tresoren zu bekommen, wenn sie dabei den Schatz der
Monarchie zurückbrachte. Dazu würden sich ein paar Wachen
finden, die sich bereiterklären würden, zu helfen, weil
ja schließlich alle im Hause Lurch davon profitieren würden.

Es war ein seltsames Gefühl, Marusch das Leben ihres Vaters
in die Hand zu geben, und auch ihre geliebte Jacke. Aber
sie erinnerte sich daran, dass Marusch ein eigenes Interesse an
der Rückgabe des Buches hatte, um Lilið zu helfen, weil
Liliðs Leben gefährdet war, käme raus, dass sie der Blutige
Master M war. Wäre es Marusch nur darum gegangen, ihren Vater
für sie zu schützen, dann hätte sie vielleicht nicht genug darauf
vertraut, dass Marusch sich nicht bei der kleinsten Gefahr
umentscheiden würde. Aber so? So vertraute sie. Eine
Restangst blieb trotzdem. Wie würde sich Marusch verhalten, wenn
Lilið nicht dabei wäre? Und es war darum gegangen, die Gefahr, in
der Lilið schwebte, aufzulösen. Nun begab sie sich direkt in
eine größere, neue. War es für ihr eigenes Leben dann überhaupt noch
signifikant von Vorteil, den Fall mit dem Schatz der Monarchie
abzuschließen?

Sie wäre gern mitgekommen. Aber leider passte das zeitlich nicht.
Das Treffen mit der Crew war für frühen Nachmittag anberaumt, also
so, dass das Nautika es locker hätte erreichen können, wenn es auch
zum Sonnenaufgang aufgebrochen wäre. Sie hätte etwas weniger Zeit,
dahin zu gelangen, weil sie zuvor ihr Zertifikat abholen
musste. Aufbruch der Kagutte war für heute Abend geplant, und es konnte gut
sein, dass die Crew sie bis dahin vereinnahmte. Wenn sie in alles
eingeweiht wäre, würde sie vielleicht nicht leichtfertig von
Bord gelassen. Auf jeden Fall war es unwahrscheinlich, dass sie
nach dem Anheuern und vor der Abfahrt ohne Verdächte auf irgendetwas
zu erregen, drei Stunden hätte verschwinden können, die sie gebraucht
hätte, um das Buch zurückzubringen.

Wenn sie es schaffte, an Bord zu gelangen, würde sie die Kagutte
möglichst unauffällig in der Gegend hin- und hernavigieren. Nicht
so dicht vor der Küste, dass sie Nederoge wiedererkennen könnten, sondern
zwischen den Reiseinseln, die sich so verwirrend bewegten, dass kaum
ein Mensch, der sich nicht sehr gut auskannte, davon Wind bekommen
würde. Hoffte sie. Marusch wusste, wo ungefähr sie plante, sich absichtlich
zu verirren, und würde eine Rettungscrew organisieren. Das
hörte sich für Lilið im ersten Moment schwieriger an, als es war. Aber
eigentlich musste Marusch dafür nur den richtigen Leuten Bescheid
sagen. Natürlich hätte die Königin ein Interesse daran, dass die Prinzessin
gerettet würde. Und dass Marusch im Zweifel die Königin selbst erreichen
würde, traute Lilið ihr zu.

Würde Lilið nicht an Bord gelangen, weil irgendetwas bei ihrer Vorstellung
schief ginge, würde Marusch behaupten, das Nautika zu sein und
von Lilið bestohlen worden zu sein. Falls Lilið nicht selbst
geschafft hätte, zuvor zu fliehen, würde Marusch sie von Wachen zu
Lord Lurch bringen lassen, so der Plan. Es war der, der Lilið in
dem Fall die besten Überlebenschancen einbaute, aber es war auch ein
haariger Notfallplan für ein besonders unglückliches Szenario, in dem
Lilið nicht abgekauft würde, das richtige Nautika zu sein, und sie
nicht selber fliehen konnte. Solange sie nicht an Bord wäre, standen
ihre Fluchtmöglichkeiten als Person mit Faltmagie allerdings nicht
unbedingt schlecht. Wenn sie fliehen könnte, könnte sie
auch versuchen, Marusch zu informieren,
wodurch sie aufgeflogen wäre und sie könnten gemeinsam überlegen,
was Marusch beim Anheuerversuch besser
machen müsste. Lilið mochte nicht daran denken, denn in jedem Fall wäre
ein zweiter Anheuerversuch durch Marusch nach einem gescheiterten
durch sie mit extrem hohem Risiko verbunden.

Marusch würde nach der Rückgabe des Buches also wieder
zum Handelshafen kommen, um im Zweifel gescheiterte Pläne
aufzufangen, aber eigentlich in der Hoffnung, Lilið an
Bord vorzufinden. Marusch würde dann nicht an Bord gelangen können, aber
sie hatten sich eine Reihe unauffälliger Gesten überlegt, mit denen
sie über eine Distanz hinweg kommunizieren konnten, was vom Plan
jeweils geklappt hätte.

Es war der abenteuerlichste Plan, an dem Lilið je beteiligt gewesen
war. Als sie im Handelshafen ausstieg und Marusch hinterherblickte,
wie diese davon segelte, war ihr schon ein wenig mulmig. Marusch
hingegen wirkte entspannt und winkte, verhedderte sich in den Schoten,
weil sie ja nun zwei bedienen musste, sortiere alles wieder, lachte
und winkte noch einmal. Es war ein schönes Lachen.
