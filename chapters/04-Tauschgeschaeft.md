Tauschgeschäft
==============

*CN: Misgendern, Mord, Hinrichtung, Leiche, bisschen Gore, Trauer, Verfolgung, Lebensgefahr.*

Die Leiche sah ziemlich übel zugerichtet aus. Die Haut war
aufgequollen und an einer Hand fehlten fast alle Finger. Lilið betrachtete
sie eingehend. Sie glaubte, dass es nicht Marusch war. Es sei denn, mit
den Haaren war irgendetwas passiert, was sie sehr aufgehellt hätte. Lilið
traute sich nicht zu, mit Sicherheit zu entscheiden, dass es nicht
Maruschs Gesicht war. Sie hatte ein warmes Gefühl gehabt, als sie es
in der Nacht dicht vor sich gesehen hatte, was sie nun nicht
hatte, aber das war vermutlich zum großen Teil durch Maruschs Mimik
passiert. Die Mimik der Leiche war natürlich unbewegt.

Die Tür zum Kühlhaus öffnete sich und ließ eine kurze warme Briese
hineinwehen. Die Eismagie, die es kalt hielt, wurde drei bis vier Mal
am Tag erneuert, aber die letzte Auffrischung war noch nicht
lange her. Lilið wandte sich zur Person um, die den Raum
betreten hatte. Jene schloss die Tür sorgfältig wieder und
anschließend ihren Mantel. Es war eine der Wachen, erkannte
Lilið an der Uniform.

"Was machst du hier?", fragte die Wache. Sie wirkte nicht
so richtig wach, fand Lilið, eher erschöpft.

"Es kommt nicht allzu oft vor, dass hier Leichen gelagert
werden.", erwiderte sie.

"Zum Glück nicht!", bestätigte die Wache, dankenswerter Weise
dies als Antwort akzeptierend.

Liliðs Fluchtversuch war nun zwei Tage her. Genug Zeit für Marusch, Raum
zwischen sich und diese Insel zu bringen. Aber es war heute Nacht zu einem weiteren
Einbruch gekommen, den die Wachleute dieses Mal bemerkt hatten. Das Einbruchsduo
hatte sich getrennt: Während der eine Langfinger die ihm nächste Wache abgelenkt
hatte, war der andere geflohen. Der eine Langfinger lag nun tot hier, -- das
Ergebnis des Ablenkungsmanövers --, für den anderen war heute morgen
ein Suchtrupp losgeschickt worden.

Die Person, die gerade das Kühlhaus betreten hatte, seufzte, holte sich
einen Klapphocker zwischen einem Schrank
und einem Präpariertisch hervor und setzte sich
neben Lilið. "Das war eine ziemlich wehrhafte Person."

"Hast du sie getötet?", fragte Lilið.

"Ihn.", korrigierte die Wache und räumte ein: "Das
kann man nun nicht mehr so gut erkennen."

Lilið rollte innerlich mit den Augen und hoffte jetzt erst Recht, dass
es sich nicht um Marusch handelte. Im nächsten Moment fragte sie
sich, ob der Gedanke nicht makaber war. "Bereust du es?", fragte
sie.

"Ihn getötet zu haben?", fragte die Wache.

"Ja, oder etwas am Wie. Ich weiß nicht." Lilið begann zu zweifeln, ob
ihr so eine Frage überhaupt zustand.

"Ich war auch recht übel zugerichtet nach dem Kampf!", klärte
die Wache sie auf, vielleicht eine Spur energischer als beabsichtigt. Sie
fuhr ruhiger fort: "Ich war fast den halben Tag im Spital, bis sie mich
wieder ganz hatten."

"Es tut mir leid.", sagte Lilið. Sie versuchte, es mitfühlend klingen zu
lassen, aber innerlich fühlte sie nichts.

"Es ist nicht so, dass ich mir keine Gedanken machen würde, Lilið.", sagte
die Wache. "Müssen wir töten? Ich meine, nachdem ich den Dieb erwischt hatte,
hatte er wegen der Regel kaum eine Wahl, als mich anzugreifen. Entkommen wäre er mir
nicht. Er hätte sich höchstens ergeben können, dann wäre immerhin mir
nichts passiert."

"Aber der Langfinger hätte sich dann meinem Vater im Kampf stellen
müssen.", ergänzte Lilið.

"Das wäre kurz und schmerzlos geworden." Die Wache seufzte. "Das wird
wohl eher das Schicksal des zweiten Langfingers werden."

"Ist dessen Identität bekannt?", fragte Lilið.

"Wir haben ausreichend Spuren, dass unsere Rekonstrikta ihn
identifizieren könnte. Sie könnte, wenn wir eine verdächtige Person
fangen, sicher sagen, ob sie es ist oder nicht.", teilte
die Wache mit.

Ein Grauen durchfuhr Lilið. Das würde für die Person bedeuten, dass
sie wohl besser das nächste halbe Jahr über mindestens untertauchen
sollte. Und vielleicht wusste sie es nicht, wusste nicht, was für
Spuren sie hinterlassen hatte.

"Um auf den Punkt zurückzukommen, der Dieb hatte nicht groß
die Wahl, mich anzugreifen. Nachdem ich ihn beim Einbruch ertappt
habe, meine ich. Ich verstehe seine Motivation dafür, gegen mich
mit allen Mitteln zu kämpfen, also sehr wohl.", führte die Wache
den Gedanken von eben zu Ende. "Er hätte natürlich zuvor
die Wahl gehabt, nicht einzubrechen."

"Hältst du das eigentlich für verhältnismäßig?", fragte Lilið. "Töten
für den Versuch, etwas zu stehlen?"

"Auch darüber mache ich mir berufsbedingt immer wieder Gedanken.", antwortete
die Wache. Sie stand auf, klappte den Hocker wieder zusammen
und stellte ihn in die Nische, aus der er gekommen
war. "Wenn Leute bloß Nahrung stehlen wollen, lasse
ich sie üblicherweise mit ein paar Schrammen davonkommen. Aber das war
hier nicht der Fall. Unverhältnismäßig oder nicht, wissen doch Langfinger
sehr genau, welches Risiko sie eingehen, wenn sie sich auf Schatzjagd
machen. Es ist absolut nicht lebensnotwendig, Schätze zu stehlen. Motivationen
dafür sind vor allem unnötige Gier oder das Ansinnen, sich zu
beweisen. Wenn sie sich für diesen Weg
entscheiden, dann müssen sie damit halt leben. Oder eben nicht leben."

Die Wache nickte wie zum Abschied, lächelte -- aber es wirkte sehr
aufgesetzt --, und bewegte sich zur Tür.

"Ich war dir unangenehm.", stellte Lilið sachlich und leise fest.

Die Wache blieb noch einmal an der Tür stehen und wandte sich nachdenklich
zu Lilið um. Sie schüttelte den Kopf. "Nein.", sagte sie freundlich. "So
sehr ich hinter dem stehe, was ich sage, bin ich doch aufgewühlt. Ich
hätte heute Nacht fast selbst dran glauben müssen. Und ich pflege
die Tradition, mich von meinen Toten zu verabschieden."

Dieses Mal stand Lilið auf. "Wenn du deswegen gekommen bist und dafür
Ruhe haben möchtest, so ist der Raum gern dein.", sagte sie.

Der Edemsapfel der Wache zuckte und ein paar Tränen schossen in ihre Augen. "Das
weiß ich sehr zu schätzen. Du bist eine gutmütige Dame.", sagte
sie und näherte sich wieder. "Ich würde mich freuen, wenn du
irgendwann den Hofstaat deines Vaters übernehmen kannst."

Die Wache hätte kaum etwas Schlimmeres zum Abschied sagen können, fand
Lilið. Sie schluckte den Frust darüber, wieder als Dame bezeichnet worden zu
sein und Wünsche an ihr zukünftiges Leben gerichtet zu bekommen, die
sie nicht erfüllen wollte, herunter und verließ den Raum.

---

Wärme schlug ihr entgegen und das Geschnatter der Okentendrachen, die
am See entlangwatschelten. Lilið atmete die schwere Luft ein. Alles in ihr
fühlte sich angespannt an. Die Leiche zu betrachten, hatte ihr noch einmal
klarer gemacht, was sie erwartete, wenn sie eine Diebeskarriere einschlagen
würde. Sie wollte eigentlich immer noch Nautika werden. Aber sie fürchtete, dass
es doch nicht so einfach werden würde, wie ihre Mutter sich das vorstellte. Sie
müsste sich irgendwie eine andere Identität dafür aufbauen. Denn wenn sie in
der Ausbildung jemand als das Kind von Lord Lurch erkennen würde, während
sie eigentlich auf einem gewissen Internat sein sollte, was dann?

Es belastete sie auch, dass sie nicht genau wusste, was hier los war. Sie hatte
am Morgen eine andere Wache gefragt, ob denn etwas geklaut worden wäre, und
jene hatte ihr versichert, dass sie alles durchgesehen hätten. Nichts
fehlte.

Da dies ja nun der zweite Einbruch in kurzer Zeit, und Marusch auf
der Spur nach etwas Bestimmten gewesen war, erweckte es in Lilið den
Verdacht, dass die Wachen deswegen aufgestockt worden sein könnten
und nicht ihretwegen. Aber als
sie die Idee erleichtert ihrer Mutter mitgeteilt hatte, hatte diese
gemeint, dass es auch umgekehrt sein könnte: Dass die aufgestockte Wachenzahl
bei Langfingern bloß den Eindruck erweckte, hier gäbe es etwas zu holen.

Die übrige Zeit bei ihrer Mutter wurde die unangenehmste, die sie je
hier verbracht hatte. Sie fühlte sich eingesperrt. Sie durfte nicht
einmal ohne Aufsicht schwimmen. Und es wunderte sie auch nicht, dass
sie dieses Mal daran gehindert wurde, den Weg zurück zu spazieren, als
es soweit war, dass sie gehen musste. Vielleicht hatte ihre Mutter also
doch recht mit ihrer Theorie. Oder Lilið wurde auch vor möglichen Einbrechenden
geschützt.

---

"Ich bin sehr froh, dass du dich umentschieden hast!", begrüßte ihr
Vater sie, als sie gerade ausgestiegen war. Er erwartete
sie am Eingang des Haupthauses. "Und ich freue mich über das Engagement dieser
Institution. Deine Mentorin ist gestern Abend eingetroffen. Sie
ruht sich im kleinen Besuchshaus am Hain aus."

"Meine Mentorin?" Lilið verbarg ihr Entsetzen und versuchte, einen
Ausdruck von Neugierde in ihre Stimme zu legen.

"Ich war auch überrascht!", sagte ihr Vater. "Sie ist bestimmt nicht
böse, wenn du sie jetzt schon besuchst, falls du es eilig hast. Sie
ist jung, vielleicht sogar jünger als du. Ich kann mir vorstellen, dass
ihr euch versteht."

Also war die Mentorin vermutlich selbst Schülerin, schloss Lilið. "Ich
überlege es mir.", nuschelte sie.

Eigentlich wollte sie überhaupt nicht. In ihr grummelte Frust vor
sich hin, als sie ihre Sachen auf ihr Zimmer brachte. Wenn es bis
zu ihrer Reise zum Internat nur noch knapp zwei Wochen hin war, dann
war nicht unwahrscheinlich, dass die Mentorin bis zur Abreise bliebe. Frankeroge
war, wie Marusch schon festgestellt hatte, nicht um die Ecke. Besonders
im Sommer nicht, wenn die Reiseinseln die Seenplatten unter dem Ozean, der
Frankeroge vom Nederloge trennte, auf ihrem Weg vom Norden
in den Süden ein gutes Stück auseinander schoben. Lilið hatte überlegt, ob
die Reiseinseln ein guter Startpunkt für ihre Flucht sein könnten.

Aber da ihr Plan war, überzeugend so zu tun, als würde sie das Internat
besuchen wollen, und weil es vorteilhaft war, die genauen Pläne der
Mentorin zu kennen, atmete sie tief durch, um sich mental auf einen
Besuch bei der Person vorzubereiten.

Sie kam nicht weit. Sie hatte das Haus gerade wieder verlassen, um sich
zum Besuchshaus aufzumachen, als Aufruhr aufkam. Sie folgte den Blicken der
Umstehenden gen Himmel. Es war sicher nicht der Zugdrachenschwarm, der
in Dreiecksformation gen Westen flog, den sie so interessant fanden.
Etwas weiter hinten näherte sich eine große Kreatur. Lilið dachte erst
an einen riesigen Seedrachen, aber so große Drachen gab es hier kaum.

"Das ist Elmar!", rief eine Person vom Dienstpersonal neben
ihr. "Sie taumelt, sie hat sich völlig übernommen! Du kannst
schnell laufen, Lu. Hol Medikae!"

Lu, ein Kind, vielleicht fast jugendlich, nickte und eilte davon.

Die Person von eben erteilte den Umstehenden weitere Anweisungen und
das machte sie gut. Jemand holte ihren Vater, und Lilið sollte zusammen
mit einer anderen kräftigen Person zusehen, dass sie Elmar auffingen, wenn
bei der Landung etwas nicht so koordiniert ablaufen würde. Lilið war gar
nicht bewusst gewesen, dass sie je in einen Topf mit eher starken
Leuten geworfen werden könnte. Sie hatte durchaus starke Arme und
Beine, aber an sich nicht überdurchschnittliche Kraft, hatte sie immer gedacht.

Sie hielt sich nicht lange mit Gedanken darüber auf. Zusammen mit
Heinert, den sie sogar mit Namen kannte, weil sie mit ihm besonders
oft Handwerksarbeiten gemeinsam ausführte, hielt sie sich bereit, um
Elmar Hilfestellung beim Landen anzubieten. Es war trotz der Aufregung
und Gefahr ein beeindruckender Moment, wie Elmar einen Landeanflug auf den Garten
machte, wie ihre Schwingen dabei nicht immer Halt in den Luftströmungen fanden
und Lilið mehrfach Angst hatte, Elmar könnte ausversehen in einen
Baum abdriften. Es lief alles sehr schnell ab. Kurz bevor sie den
Boden erreichte, lehnte Elmar Kopf und Knie nach vorn, und
wäre mit den Schienbeinen über die Grasnarbe bretternd zum Halten gekommen,
wenn Heinert und Lilið sie nicht an den Flügelansätzen gepackt hätten. Lilið
klammerte ihre Hände in den Muskel nahe der Schulter und drückte mit
ihrem Arm mit aller Kraft von unten dagegen. Dabei eilte sie
neben Heinert her, der auf der anderen Seite etwa das Gleiche tat. Sie
trugen das Gewicht des schmalen Körpers halb rennend, halb stolpernd, sodass
Elmars Knie fast über dem Boden schwebend kaum etwas abbekamen, bis sie
alle die Geschwindigkeit ausreichend heruntergebremst hatten, dass sie
sich sicher auf den Boden fallen lassen konnten.

Lilið setzte sich auf und atmete hastig. Das zerdrückte Gras unter ihr
roch angenehm. Elmar nahm, noch viel
rascher nach Atem ringend, zu ihrer Seite eine Embryohaltung im Gras ein, während
ihre Flügel sich wieder in Arme verwandelten. Das war an sich auch so
etwas wie eine Falttechnik, dachte Lilið. Eine, die Lilið nie
so lange am Stück hätte halten, geschweige denn zum
Fliegen benutzen können. Elmar gehörte dann wohl
zu den Informantikae, Bedienstete des Hofstaats, die Nachrichten
zustellten, und im Zweifel taten sie das sehr eilig.

Heinert bot Elmar ein Taschentuch an, dass sie dankend annahm. Sie
verharrte nicht lange in der Haltung, sondern zwang sich mit Mühe, sich
aufzusetzen, als sich Lord Lurch raschen Schrittes näherte. Sie wischte sich die Tränen
aus den Augen, den Schweiß vom Gesicht, schneuzte sich anschließend und
wollte sich erheben, doch Lord Lurch winkte ab.

"Bleib ruhig sitzen und ruh dich aus, ich kann mich auch hinhocken.", sagte
er freundlich. "So alt bin ich noch nicht." Er setzte sich tatsächlich
ins Gras. Heute trug er ein lila Gewand.

"Danke." Elmars Stimme war kaum mehr als ein Krächzen. "Es hat einen
zweiten Einbruch gegeben."

Einen dritten, korrigierte Lilið in Gedanken.

"Er war erfolgreich.", fügte Elmar hinzu. "Der Schatz ist gestohlen worden."

Also waren die Wachen vielleicht doch wegen eines Schatzes verstärkt worden. Liliðs
Gedanken verweilten allerdings nicht lange bei Theorien dazu. Ihr Blick
fiel auf ihren Vater. Sie hatte ihn noch nie so voll Angst gesehen. Ihr
Vater hatte vor nichts Angst, hatte sie immer gedacht. Er hätte vermutlich
gegen fünf Leute aus seiner eigenen Wache gleichzeitig antreten können und ihm
wäre nichts passiert, obwohl Lord Lurchs Wache zu der am besten ausgebildeten
der ganzen nederoger Inselvereinigung gehörte.

"Gibt es bekannte Verantwortliche?", fragte er.

"Nein, Lord Lurch.", antwortete Elmar. "Es gibt keine Spuren. Niemand
hat den Einbruch bemerkt. Es wurde lediglich eine Nachricht hinterlassen. Eine
Signatur!"

"Die lautet?" Lord Lurchs Stimme beruhigte sich etwas, aber es lag immer noch
ein Beben darin, das Lilið ängstigte.

"Ein mit Blut geschriebenes M. Davor das Zeichen für 'Master'.", berichtete
Elmar.

Makaber, dachte Lilið. Und mutig. Blut zu hinterlassen, ermöglichte schließlich
eine Identifikation meistens recht gut.

"Wahrscheinlich nicht mit seinem eigenen Blut.", murmelte ihr Vater, als
hätte er Liliðs Gedanken gelesen, und erhob
dann wieder die Stimme: "Gibt es weiteres zu berichten?"

Elmar schüttelte den Kopf. "Das war alles. Ich fliege zurück und werde weitere
Neuigkeiten bringen, sobald ich kann."

"Nichts da!" Lord Lurch richtete sich auf. "Also, klar fliegst du zurück, aber zuerst
lässt du dich von den Medikae aufpäppeln. Niemandem ist geholfen, wenn du vom
Himmel plumpst."

---

Auch nachdem Elmar weggebracht worden war und die Bediensteten sich wieder
ihren Aufgaben widmeten, an denen sie zuvor gesessen hatten, machte
Lilið sich noch nicht zum Besuchshaus auf. Dazu war sie viel zu
aufgewühlt. Stattdessen suchte sie ihren Vater in seinem Briefzimmer auf. Als
er sie anblickte, war von seiner sonst so guten Laune keine Spur zu sehen.

"Darf ich ein paar mehr Details erfahren, was los ist?", fragte Lilið
vorsichtig.

Ihr Vater betrachtete sie einige Momente nachdenklich, vielleicht etwas
abwesend, aber nickte dann. "Setz dich zu mir, Lil."

Lilið rückte einen schmalen Stuhl aus der Ecke heran. Sie mochte den
Geruch dieses Zimmers. Das alte Leder und die Bücher brachten einen
Duft von Geheimnissen mit sich.

"Der Kronprinz hat unserem Hof eine Kostbarkeit aus dem Schatz der
Monarchie zur sicheren Aufbewahrung zukommen lassen.", berichtete
ihr Vater. Er starrte auf das leere Papier vor ihm, dann wieder
einen Moment in Lilið Gesicht, doch so richtig, sah er sie nicht an.

"Warum?", fragte Lilið. Im stillen erinnerte sie sich daran, dass
Marusch diese Kostbarkeit erwähnt hatte.

"Das wurde mir nicht mitgeteilt. Ich vermute, aus den jüngsten Ereignissen
schließend, dass diesen Gegenstand zu stehlen zur Herausforderung
einiger Diebesbanden und einzelnen Langfingern geworden ist, die sich
beweisen wollen. Eine Art Trophäe für sie.", erklärte
er. "Wie ein Wettkampf mit immer größeren Aufgaben, verstehst du?"

Lilið nickte. Sie dachte an die Leiche, die inzwischen verbrannt und
beerdigt war. Sie konnte nicht leugnen, dass sie dieser Wettkampf
trotzdem reizte. Und das ärgerte sie. Sie war sicher nicht so gut. Und
sie wollte ihr Leben auch nicht für so etwas aufs Spiel setzen.

"Nun hat der Blutige Master M diesen Wettkampf gewonnen." Ein Lächeln
war vorsichtig auf das Gesicht ihres Vaters zurückgekehrt. "Und ich könnte mich ja
schon ein bisschen mit ihm für diesen Sieg mitfreuen, wenn er seine
Trophäe nicht von meinem gut bewachten Grundstück entwendet hätte, was ich
nun irgendwie der Monarchie erklären muss."

"Ah, daher hast du Angst.", begriff Lilið.

"Ja, Lil. Daher habe ich Angst.", sagte ihr Vater. Vom Lächeln war
keine Spur geblieben. "Der Kronprinz hat wohl einen Skorem von
über 300, heißt es. Man weiß es nicht genau, weil es schon kaum Personen mit einem
Skorem größer 200 gibt und wir gar nicht genau wissen, nach welchen
Kriterien wir Skorem in dem Bereich genau festlegen sollen. Fest
steht jedenfalls, dass ich kaum eine Chance hätte, wenn mich die
Monarchie oder gar der Kronprinz selbst herausfordert."

Lilið schluckte. Irgendetwas in ihrem Hirn fühlte sich so an, als würde
es sich krampfhaft verknoten wollen. So fühlte es sich also an, wenn
sie unerwartet Angst hatte, dass das Leben ihres Vaters in
realer Gefahr war.

Sie bemühte sich, langsam durchzuatmen und sich zu beruhigen. Ein
kleiner Teil von ihr fing ungefragt an, einen möglichen Plan vorzulegen: Wenn
sie nicht zum Internat ginge und nicht Nautika werden könnte, dann
könnte sie vielleicht den Blutigen Master M suchen. Vielleicht
sogar mit Marusch zusammen, falls sie sich je wiederfinden würden. Marusch
hatte schließlich ein Interesse, diesen Langfinger zu finden. Allerdings
vermutlich, um ihm den Schatz abzunehmen.

Wenn Lilið lang genug Zeit mit Marusch verbracht hätte, könnte sie
vielleicht herausfinden, ob es realistisch wäre, ihm anschließend
den Schatz wieder abzuluxen und ihn ihrem Vater zurückzubringen.

Ihr wurde noch einmal sehr heiß, als sie realisierte, dass der
Name Marusch mit M anfing. Vielleicht war sie der Blutige Master
M. Etwas Makaberes hatte Marusch vielleicht an sich gehabt. Tief
verborgen. Oder Liliðs Vorstellungsapparat fantasierte hier.

Dagegen sprach auf jeden Fall, dass sich Marusch wahrscheinlich nicht als
Master betitelt hätte. Oder doch? War es eine Tarnung? Oder
hatten diejenigen, die die Signatur gelesen hatten, vielleicht
den feinen Unterschied zwischen den Zeichen für Master und Mistress
nicht gesehen? Vielleicht hatte Marusch ja auch ein Zeichen
dazwischen entwickelt. Und Leute dachten meistens nur an
Männer, wenn sie sich Einbrechende vorstellten, weshalb das Zeichen
entsprechend falsch gelesen worden war.

"Mach dir keine zu großen Sorgen, Lil." Ihr Vater hatte eine
Weile geschwiegen und legte ihr nun die Hand auf die Schulter. "Die
Monarchie fällt keine übereilten Entscheidungen. Mir wird sicher
etwas Zeit zum Suchen gegeben. Und es ist nicht einmal gesagt, ob
sie mich zu einem Kampf herausfordern würden, wenn ich erfolglos
bliebe. Vielleicht lassen sie sich auf einen Handel ein, der den
Schaden ersetzen könnte."

Lilið nickte. Sie kannte sich wenig mit Politik aus, aber
vermutete, dass ihr Vater sie nur zu beruhigen versuchte. Sie
glaubte schon, dass mit der Monarchie nicht zu spaßen wäre. "Natürlich mache
ich mir Sorgen.", sagte sie. "Aber
ich werde es aushalten. Ich kann ja nicht viel tun, außer da sein."

Vielleicht sollte sie doch erst einmal das Internat besuchen. Die
Wahrscheinlichkeit, dass sie es mit erfahrenen Diebespersonen
aufnehmen konnte, war sehr klein. Ihr Vater würde schon Leute losschicken,
die besser geeignet für so eine Aufgabe wären als sie. Wenn sie jetzt
oder sehr bald entfliehen würde, dann würde ihm das auch noch Nerven
kosten, und womöglich würde er Leute nach ihr suchen lassen, die er
eigentlich für die Suche nach dem Blutigen Master M brauchen konnte.

Vielleicht sollte sie von Marusch erzählen. Aber sie wollte nicht, dass
Marusch etwas passierte. Sie wollte auch nicht, dass ihrem Vater
etwas passierte. Das war wieder ein Argument, das dafür sprach, dass
sie Marusch zumindest noch einmal wiedertreffen sollte. Bei der
Vorstellung durchlief unaufgewordert ein warmer Schauder ihren Körper.

---

Lilið fühlte sich erschöpft, als sie draußen an der Tafel zum Abendessen
Platz nahm. Sie erschreckte sich, als eine fremde Person sich neben
sie setzte und sie grüßte. Sie hatte die Mentorin fast vergessen.

"Ich bin Allil. Es freut mich, dich kennen zu lernen." Allils
Stimme war herzlich, aber etwas unsicher.

"Ich bin Lilið.", stellte sich Lilið vor. Wahrscheinlich unnötiger
Weise.

Allil trug ein blaues Sommerkleid aus Seide, das gut auf ihren
Körper zugeschnitten war. Das sprach sehr dafür, dass sie aus gutem
Hause kam.

"Ich freue mich sehr über die Initiative, die das Internat für
skorsche Damen ergreift, um meiner Tochter einen guten Start zu
ermöglichen." In der einladenden Stimme ihres Vaters fand
Lilið nichts von dem Grauen wieder, das vorhin noch so unweigerlich
darin mitgeschwungen hatte.

"Es ist mir eine Ehre.", antwortete Allil. "Als die Schule den Brief
erhalten hat, der uns die Angst und Unsicherheit der neuen Schülerin
darlegte, habe ich nicht gezögert, mich anzubieten, meine Restferien
bei ihr zu verbringen, sie zu ermutigen und ihr zu helfen, möglichst
gut Anschluss zu finden."

Ihr Vater hatte einen Brief an das Internat geschrieben? Lilið warf
ihm einen stirnrunzelnden Blick zu. Er erwiederte ihn wohlwollend.
Er wirkte eher als würde er etwas gutheißen, als schuldbewusst oder
so etwas zu sein.

Im Normalfall hätte sie geschimpft. Aber sie wollte sich immer noch
eine mögliche Flucht nicht verbauen. "Das ist sehr zuvorkommend. Damit
hätte ich nicht gerechnet.", sagte Lilið also.

---

Allil war Lilið tatsächlich nicht unsympathisch. Sie war interessant. Sie
erzählte, dass sie selbst noch nicht lange am Internat war und auch sie
am Anfang Unterstützung bei der Eingliederung bekommen hatte. Sie
erzählte von den sozialen Strukturen am Internat, darüber, wie sie
mit offenen Armen aufgenommen worden war.

Lilið brauchte eine Weile, um zu merken, dass ihre Erzählungen zwar
sehr persönlich wirkende Details und konkrete Anekdoten beinhalteten, aber
nichts davon besonders spezifisch war. Hier stimmte etwas nicht. Da sie
aber schon bei der Nachspeise waren, beschloss Lilið Allil später
im Besuchshaus allein aufzusuchen und damit zu konfrontieren. Hilfe,
wenn Lilið welche bräuchte,
wäre dann trotzdem nicht weit. Auf dem ganzen Gelände gingen Wachen auf und ab.

---

Es dämmerte bereits, als sich endlich ein Zeitpunkt ergab. Allil
war über das Gelände und durch ein Hauptgebäude geführt worden. Lilið
hatte sich in der Zeit mit Heinert daran gemacht, eine Tür abzuschleifen
und neu zu lackieren, der ein kleines Magie-Unglück passiert war. Damit
war sie noch nicht fertig gewesen, als Allil wieder wirkte, als könnte
sie Zeit haben. Aber die Arbeit hatte beruhigt, also hatte Lilið sie
nicht unterbrochen.

Als sie an die Tür des kleinen Besuchshauses am Hain pochte, erschreckte sie
sich abermals, Allil zu sehen, als diese auf der anderen Seite der Glastür
auftauchte. Es war, wie sich selbst in einem verzerrten Spiegelbild zu sehen:
Allil hatte die Hochsteckfrisur von vorhin gelöst und sich die Haare so
zusammengebunden, wie Lilið sie trug. Außerdem hatte Allil den Lippenstift
abgewischt und ihr Kleid gegen eine kurze Hosen und ein schlichtes
Hemd getauscht -- allerdings nicht in Grün.

Sie öffnete die Tür und winkte Lilið herein. Sie sprachen kein Wort, bis
sie in der Teeküche saßen. Lilið fühlte sich einen Moment weich
in den Knien, weil sie die Situation so sehr an eine in einer anderen
Teeküche vor etwa einer Woche erinnerte.

"Vielleicht denkst du dir schon, dass ich nicht deine Mentorin bin.", leitete
Allil ein.

"Ich hatte so einen Verdacht.", gab Lilið zu.

"Marusch schickt mich.", informierte Allil. "Marusch meinte, mein Wunsch
nach guter magischer Ausbildung wäre vielleicht erfüllbar, wenn du zum
Tausch bereit wärest. Du hättest deine Gründe, das vielleicht zu
wollen. Wir haben aber auch beide Verständnis, wenn du nicht möchtest. Dann
verschwinde ich sang- und klanglos wieder. Es ist nur ein Angebot."

"Wieviel Zeit habe ich, um zu entscheiden?", fragte Lilið.
Ihr Herz schlug mit einem Mal heftig und
schnell. Sollte sie einer Sache trauen, die Marusch eingefädelt hatte?

Wenn sie Allil verriete, wäre diese vermutlich geliefert. Oder war Allil sehr
mächtig und könnte es mit ihrem Vater aufnehmen? Dann hätte sie aber
keine Ausbildung mehr gebraucht. Zumindest keine, die das Internat, um das
es ging, böte. Wenn sie hier war, um Lilið irgendwie zu schaden, könnte
aber das ganze Ansinnen erlogen sein. Aber weshalb würde jemand Lilið
schaden wollen?

"Na ja, bis es losgeht. Nicht?" Allil begann Liliðs Bewegungen zu
kopieren, dass es Lilið umheimlich wurde. "Solange kann ich hier sicher
wohnen, zumindest, wenn du mich nicht verpfeifst."

"Aber du hast einen Plan, für wenn ich dich verpfiffe?", fragte
Lilið.

"Keinen konkreten.", antwortete Allil. "Ich habe ein paar nützliche
Fähigkeiten, die mir Flucht meistens erleichtern. Ich hoffe, das
wird nicht nötig?"

Lilið lächelte. "Ich weiß, welche Strafen dir drohen würden, wenn der
Betrug aufflöge. Die wünsche ich niemandem.", erwiderte sie.

Allil nickte. Lilið konnte nicht lesen, ob Allil sich fürchtete oder
nicht. "Wenn du möchtest, können wir detailliert den Plan
durchsprechen."

"Ein Plan von Marusch?", fragte Lilið.

"Im Wesentlichen.", antwortete Allil. "Ich habe außerdem einen
Brief von Marusch für dich mitgebracht."

Erst jetzt realisierte Lilið, dass diese Begegnung sehr nahe legte, egal
ob sie eine Falle darstellte oder nicht, dass Marusch noch am Leben
war.
