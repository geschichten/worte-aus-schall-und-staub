Seiten
======

*CN: Sex - erwähnt, Suizid als Gesprächsthema.*

Auch in den folgenden Tagen konnte Lilið sich nicht über einen
Mangel an Aufregung oder Anstrengung beklagen. Mit der Entdeckung
des Metallstaubs war das Dekodieren noch nicht getan. Lilið wusste
nicht einmal, ob sie das sonderlich überraschte. Sie hatte die
Zeichen nicht gekannt, die sie gefühlt hatte. Auch Marusch nicht,
die sich im Rahmen ihrer Auseinandersetzungen mit Dekodierungsmethoden
mit mindestens drei bekannten Schriftsystemen befasst hatte.

Im Gegensatz zu Lilið konnte Marusch die Zeichen nicht erfühlen. Also
hatte sie Lilið gebeten, eine Abschrift anzufertigen. Das war nicht
wenig Arbeit. Es war Arbeit, die Lilið durchaus anfangs eine gewisse
Freude bereitet hatte, aber sie wollte ja auch weiter an ihren
Faltfähigkeiten arbeiten. Während sie segelten erklärte ihr Marusch
Theorie, sehr viel spannende Theorie, und nachdem sie sich
zusammengerauft hatten und Lilið ausreichend erklärt hatte, wann
sie welche Tonfälle nicht mochte, hatte es angefangen, sehr großen
Spaß zu machen, Marusch zuzuhören und zu löchern. Nach dem Durchbruch mit dem
Seestern lernte sie fast täglich etwas Neues. Etwa, wie sie
sich falten und gleichzeitig atmen konnte. Und was damit
alles möglich war! Marusch hatte erklärt, dass Chameleon-Magie
in zwei Magiezweige fiel: In Illusions-Magie und tatsächlich
in Falt-Magie. Im ersten Fall ging es darum, das Licht so zu
verändern, dass eine Person aussah wie eine andere, und im anderen
darum, sich umzufalten.

Lilið faszinierte das. Es hatte drei weitere Abende Übung gebraucht,
bis sie sich das ganze Abendessen hindurch erfolgreich als Marusch
ausgegeben hatte. Nun ja, äußerlich. Schauspiel war
eine ganz andere Kategorie von Lerninhalt. Und die Stimme war
auch immer noch ihre. Marusch erklärte ihr, dass sie ihre Stimmbänder
auch falten könnte, aber das scheiterte bisher kläglich.

Die Abschrift fertigten sie anfangs zusammen an. Zunächst zeichnete
Lilið die verschiedenen Zeichen, die sie fand, auf ein Blatt. Anschließend
gaben sie ihnen Namen und Lilið diktierte sie, während Marusch
mitschrieb. Aber das Buch war lang und es war Lilið
nicht recht, dass dabei so viel Zeit von ihrem Lernen abging, also
arbeitete ihr Kopf nachts an einer Lösung. Es kam vor, dass sie dabei
waren, sich im Dunkeln vorm Einschlafen zärtlich zu streicheln, obwohl es
spät und sie müde waren, weil sie nicht lassen konnten, sich gegenseitig
zu genießen, und Lilið das ganze unvermittelt mit einem Gedanken zu
Magie, Falten oder dem Buch unterbrach. Der am Ende zielführende Gedanke
hatte den Nachteil, dass er
die angefangene Arbeit hinfällig machte, aber den Vorteil, dass die
Gesamtarbeit trotzdem schneller erledigt sein würde: Sie konnte ja nicht
nur mit den Fingern das Papier erfühlen, sondern auch durch bloße Berührung
Papier so falten, wie sie wollte, und das sehr gut und routiniert. In
den kommenden Abenden kopierte
sie auf diese Art die Seiten des Buches in Seiten, wo das, was sie
als Metall spürte, jeweils eine Falte war.

Es machte die bisherige Arbeit deshalb hinfällig, weil Marusch in
der ersten solchen Kopie bereits systematische Unterschiede sich
ähnelnder Zeichen entdeckte, die Lilið für gleich gehalten hatte. Vielleicht
war das der Grund, warum Marusch beim Dekodieren trotzdem keinen
Erfolg gehabt hatte.

Sie hatten noch zwei Tagesreisen vor sich, bis sie Nederoge erreichen
würden, zumindest wenn alles gut ginge, als Lilið die Kopie abgeschlossen
hatte. Marusch hatte die ersten Ideen, wie sich daraus ein verständlicher
Text ergeben könnte, aber sie beschlossen, sich heute nur noch auszuruhen.
Sie hatten eine lange Reise mit sehr schwachem Wind hinter sich
und am folgenden Tag würde eine ebenfalls sehr lange bei recht viel Wind
über die Seenplattenströmungen anstehen.

Sie lagen früher als sonst im Zelt und versuchten, zu schlafen. Der
Tag war warm gewesen. Eine dünne Wolkenschicht, die den Wind für
den morgigen Tag ankündigte, sperrte die Wärme auf der Oberfläche
des Planeten ein, sodass es Marusch und Lilið zu ungemütlich
war, um sich dicht aneinander zu kuscheln. Stattdessen lag lediglich
Maruschs Hand auf Liliðs Unterarm, wo er am zärtesten war.

Lilið konnte nicht schlafen. Sie hatte über die vergangenen zwei Wochen
ihr Denken kaum heruntergefahren, und vor allem noch nicht um diese
Uhrzeit und ohne Sex. Sie seufzte und beschloss doch endlich die
alberne Frage zu stellen, die ihr seit einigen Tagen wie ein lästiger
Ohrwurm im Kopf herumkreiste. "Vertraust du mir?" Sie hoffte, dass
Marusch noch nicht schlief und sie sie mit der Frage nicht geweckt hätte.

Aber Marusch reagierte zunächst gar nicht. Und dann mit so sanfter
Stimme, dass es Lilið den Rücken herunterkribbelte. "In den wichtigen
Punkten ja. Wieso fragst du?"

"Ich glaube, ich möchte, dass du mir vertraust, und zugleich
habe ich den Eindruck, dass ich dein Vertrauen nicht verdiene.", sagte
Lilið. "Aber eigentlich weiß ich es nicht genau. Die Frage bohrte
da so herum."

Marusch kicherte warm. "Ich glaube, mit deinem Eindruck, dass du es nicht
verdienen würdest, hast du auf manchen Ebenen nicht ganz unrecht. Aber das macht mir
nichts aus.", sagte Marusch. "Du möchtest mir gewisse Dinge nicht
erzählen. Damit du auch nicht in die Verlegenheit gebracht wirst, lügst
du mich gegebenenfalls an. Und ich frage nicht weiter. Aber ich vertraue dir
zum Beispiel sehr, dass du mir deine Gefühle für mich nicht nur
vormachst. Ich glaube, dass du mich magst und mir nichts Böses
willst, aber dass im Zweifel etwas anderes wichtiger werden kann."

Liliðs Inneres verkrampfte sich bei den Worten. Vor Scham, weil
Marusch recht hatte, und vor positiven Gefühlen, weil sie Marusch
wirklich mochte. Sie griff nach Maruschs Hand und küsste sie sanft. "Ich
kann mir gerade nicht vorstellen, dass etwas anderes wichtiger
werden würde.", sagte sie leise. "Ich glaube, selbst mein Vater
ist mir derzeit nicht wichtiger. Ich möchte das Buch seinetwegen
zurückbringen, aber würdest du dabei zu großer Gefahr ausgesetzt,
wüsste ich nicht weiter."

Wieder reagierte Marusch eine Weile nicht. Aber dieses Mal war es
anders. Lilið spürte ihre Hand in der eigenen erschlaffen, den
Atem flacher werden, und als sie Marusch musterte, so gut es
das Dunkel zuließ, wirkte sie wie leblos.

Lilið schluckte das Grauen herunter. "Ist etwas schlimm?"

"Ich habe das Bedürfnis, mich zu distanzieren." Passend zu dem, was
Marusch sagte, klang sie ein wenig wie aus der Ferne. "Ich sollte
das vielleicht nicht tun. Ich weiß es nicht. Ich bin nicht sonderlich
stabil. Wenn ich verschwinde, dann bedeutet es für Menschen, die
mich mögen, Trauer und Verlust. Jede Beziehung zu einer
Person, die ich dadurch verletzen
würde, dass ich gehe, belastet mich." Endlich tat Marusch einen
tiefen Atemzug. "Das ist ein hartes Thema. Ist es für dich in
Ordnung? Oder soll ich mich eher verschließen."

Lilið widerstand den Impuls, Maruschs Hand in ihrer fester zu
halten. "Redest du von Suizid?" Und dann tat sie es doch, so
zärtlich sie konnte. "Ja, ich kann mit dem Thema umgehen."

"Zum Beispiel. Ich habe immer Mal wieder suizidale Phasen. So
würde ich das beschreiben. Eigentlich, denke ich, besteht
die meiste Zeit über kein Risiko, dass ich mich suizidiere. Aber
für mich fühlt sich leben nur machbar an, wenn ich mir
permanent über die Option bewusst bin. Sie gibt mir
Sicherheit.", antwortete Marusch. "Und vielleicht
in dem Zusammenhang neige ich zu Risikoverhalten. Das hast du
vielleicht schon mitbekommen."

Lilið strich mit einem Finger sanft über die immer noch schlaffe
Hand. "Habe ich.", sagte sie. "Es überrascht mich auch nicht, von
deiner Seite mit dem Thema konfrontiert zu werden. Aber vielleicht kann
ich dich beruhigen: Wenn du gehst, gehst du, und ich werde damit
leben lernen. Nur selbst möchte ich dein Leben nicht riskieren." Lilið
zögerte und fügte ein weiches "Nicht allzu sehr." hinzu. "Meine
Reiseplanung ist ja nun auch nicht unriskant. Oder unser Einbruch
in Lord und Lady Piks Anwesen. Oder unser Unterfangen, das Buch
zurückzugeben. Das wird auch noch einiges an Risiko mit sich bringen."

Endlich kam wieder Leben in Maruschs Hand. Ihr Daumen lag so, dass
sie Liliðs Haut damit berühren konnte, und sie strich darüber im
Rahmen des wenigen Spielraums, den sie hatte, ohne sich aus Liliðs
Griff herauszuwinden. "Das tut tatsächlich gut, zu wissen.", sagte
Marusch. "Danke, dass ich mich dir anvertrauen darf. Menschen haben
meistens den Reflex, alles gut zu finden, was mich daran hindern
könnte, mich zu suizidieren, und darauf den Fokus zu lenken. Quasi,
Hauptsache weiterleben, egal wie. Während ich mir vielleicht wünsche,",
Marusch unterbrach sich einen Moment, vielleicht, um nachzudenken,
und strich Lilið mit der noch freien Hand über den Arm. "Ich
wünsche mir, mit meiner Suizidalität sein zu dürfen und akzeptiert
zu werden. Vielleicht ist Suizidalität ein zu starkes Wort dafür, aber
ich benutze es mal, mangels eines besseren. Es gehört zu mir
und geht nicht durch Zwang weg. Es hilft
mir nicht, wenn Leute mich nicht gehen lassen wollen und sich
dagegen wehren. Das löst in mir den Drang aus, mich zu distanzieren."

"Und gerade habe ich den Drang ausgelöst, indem ich äußerte, dass du
mir vielleicht wichtiger bist als mein Vater?", fragte Lilið. Sie
tat es in möglichst sachlichem Tonfall. "Was impliziert, dass ich
dich in ähnlicher Weise zu retten versuchen würde? Also ich an
deinem Leben hänge?"

"Ja.", flüsterte Marusch. "Ich möchte gern nicht vermisst werden. Nicht
negativ schmerzvoll zumindest. Oder am liebsten gar nicht. Ich wünschte,
dass sich niemand an mich erinnern würde, sollte ich gehen. Ich möchte
keine Spur hinterlassen. Keine Erinnerungen, keine Gefühle, nur ein
bisschen Staub."

Dieses Mal war es an Lilið, länger darüber nachzudenken, wie sie
darauf reagieren sollte. Ihre Reflexe waren durchaus, zu sagen, dass sie
gern Erinnerungen an Marusch haben wollte. Aber ihr wurde auch zügig
bewusst, dass, würde sie Marusch komplett vergessen, sie auch nichts
vermissen würde. Weil sie nicht vermissen könnte, wozu sie keine
Verbindung mehr hätte. Sollte Marusch nicht mehr sein und sie hätte
die Wahl, die Erinnerungen zu behalten oder so restlos zu löschen,
dass sie nichts vermissen könnte, sie würde trotzdem ersteres wählen. Aber
sie respektierte Maruschs Wunsch natürlich. Dann hatten sie eben
verschiedene Wünsche. Und sie wollte Marusch nicht davon überzeugen,
einen anderen zu entwickeln.

"Habe ich nun etwas zu Schlimmes gesagt?", fragte Marusch.

"Nein.", antwortete Lilið sofort. "Ich frage mich, wie du dir
wünschst, dass ich damit umgehe. Sagen wir, du wärest in Gefahr,
und ich hätte das Bedürfnis, einen Rettungsversuch zu unternehmen. Wäre
dir unlieb, wenn ich das versuche?"

"Ja.", antwortete Marusch ohne Zögern, aber korrigierte
dann doch: "Wenn etwas Politisches
dahintersteht, und du tust es vor allem für diesen politischen
Grund, ist das vielleicht etwas anderes. Aber auch da wäre es
nicht unwahrscheinlich, dass ich nur in Gefahr scheine, aber nicht allzu
sehr in Gefahr bin, sondern es Teil eines Spiels ist. Ich
würde mir wünschen, dass du kein hohes Risiko für mich
eingehst." Etwas sanfter ergänzte sie: "Aber ich möchte dir
eigentlich auch keine Vorschriften machen. Ich würde dich bitten, es dir gut
zu überlegen und mit einzuberechnen, dass ich gar nicht so
am Leben hänge. Und dass Dinge vielleicht anders scheinen können, als
sie sind."

Lilið nickte nachdenklich und untermalte die in der Dunkelheit
nur eingeschränkt sichtbare Geste mit summenden Geräuschen. "Ich werde
mit meinem üblichen Herumgeanalysiere da rangehen und mit einberechnen,
wie ich einschätze, was du möchtest.", versprach sie.

"Wow, Lilið, womit habe ich dich verdient?", seufzte Marusch. "Ich
fühle mich zumindest ernst genommen. Und nicht irgendwie in ein
gefälligeres Wunschbild verdreht. Dafür bin ich dir unbeschreiblich
dankbar."

"Was, wenn ich mich gerade nur zusammenreiße und absichtlich distanziert
bin, und heimlich doch an dir hänge?", murmelte Lilið.

"Das würde ich schade finden, aber das berechne ich bereits mit ein." Maruschs
Stimme war weich und warm, als würde sie etwas sehr Liebes sagen
wollen. "Du versuchst mich trotzdem nicht zu verdrehen. Oder mal
eben von etwas zu überzeugen. Oder für mich zu entscheiden, was das
Beste für mich wäre. Das allein ist schon selten."

Lilið strich ihr über die Wange. Marusch hielt sie stets glatt
rasiert. Lilið fühlte die zarte Haut gern. "Ich glaube, ich distanziere
mich nicht einmal.", sagte sie. "Es scheint dich einfach mehr zu
belasten, nicht mit deiner Suizidalität akzeptiert zu sein, als die
Suizidalität selbst dich belastet. Korrigiere mich, wenn ich falsch liege."

"Tust du nicht. Die Suizidalität ist manchmal belastend, aber meistens
ist sie einfach ein Teil von mir, schon von klein auf gewesen. Es
fühlt sich meistens eher entlastend an, die Option zu fühlen,
jederzeit zu gehen." Wieder sprach Marusch sehr leise. "Vielleicht
ändert sich das irgendwann, auch wenn ich mir das nicht vorstellen
kann. Aber dann muss es von mir kommen. Wenn andere versuchen, daran
etwas zu ändern, dann belastet mich das sehr."

"Dann habe ich das richtig wahrgenommen.", sagte Lilið. "Und deshalb
ergab es für mich keinen Sinn, in mir drin ein Gefühl von Grauen oder
so etwas zu spiegeln, wenn du es selbst nicht hast. Ich weiß nicht,
wie es ist, so etwas mit sich herumzutragen. Ich habe es ja nicht. Es
ergibt für mich keinen Sinn, etwas mitzufühlen, wovon ich kein Konzept
habe. Stattdessen versuche ich, deines zu begreifen."

"Danke. Das ist schön." Marusch drehte nun doch ihre Hand in Liliðs, um
umgekehrt Liliðs Hand zu ihrem Mund zu führen und zu küssen. "Wie ist es
eigentlich umgekehrt: Vertraust du mir?"

Lilið kicherte. "Ich glaube, du bist ehrlicher als ich mit den
Dingen, die du mir nicht anvertrauen möchtest. Du sagst, dass
da etwas ist, worüber du nicht reden möchtest.", sagte sie. "Ich
vertraue dir auf ähnliche Weise wie du mir. Ich glaube dir, dass
du mir nicht vormachst, dass du mich magst, sondern es wirklich tust. Und
ich glaube, dass du mein Leben nicht unnötig gefährden wirst."

"Über letzteres würde ich gern noch einmal mit dir reden.", sagte Marusch,
nun wieder mit einem breiten Grinsen in der Stimme.

Lilið runzelte die Stirn, fühlte es an den Muskeln und realisierte,
dass Marusch es wahrscheinlich nicht sah. "Würdest du?"

"Das kommt drauf an.", antwortete Marusch. "Auf Definitionen von
'unnötig' vor allem. Und darauf, was du nach der Buchrückgabe vorhast. Wenn
wir weiter zusammenbleiben wollen, würde ich mich gern nicht sehr
in meinen Plänen einschränken. In meinen unkonkreten Plänen. Das
bringt ein Risiko mit. Ich denke, ein ähnliches, wie du es schon kennst,
aber eben nicht mehr für den Zweck, deinen Vater zu retten. Also
gegebenenfalls unnötiger."

"Du sagtest, die Pläne sind unkonkret. Aber kannst du trotzdem etwas
über sie sagen?", fragte Lilið.

Marusch schüttelte den Kopf. Lilið sah es kaum, aber spürte es
in der Unterlage. "Die Optionen, die so zur Debatte stehen: Mehr Unsinn
der Kategorie machen, in die der Diebstahl des Buches fällt. Ich
mag daran die spannende Lektüre und das Auflehnen gegen Regeln, die
Machtungleichgewichte auf besonders unsinnige Weise zeigen."

"Oh, die zweite Begründung kannte ich noch gar nicht!", hielt Lilið
überrascht fest. "Ich dachte, du wolltest nur ein Buch lesen."

"Ich wollte auch einfach nur das Buch lesen.", antwortete Marusch
kichernd. "Ich möchte viele Dinge einfach tun. Und ich suche mir
unter diesen Dingen solche aus, um mich von ihnen nicht einfach abhalten zu lassen,
bei denen es eine besonders antiautoritäre Aussage hat."

"Ich glaube, das verstehe ich." Nach kurzem Zögern ergänzte
Lilið: "Und ich mag es irgendwie. Was sind die anderen Optionen?"

"Menschen retten, die auf kriminalisierte Art versuchen, etwas im
System zu bekommen, was ihnen zustehen sollte, und die dabei in
Schwierigkeiten geraten.", schlug Marusch
vor. "Aber das ist ein frustrierendes Fass ohne Boden."

Lilið nickte. "Das habe ich mir auch schon überlegt. Vielleicht
ist es dann besser, die Revolution zu planen."

Marusch kicherte. "Nehme ich mit auf die Liste
der Optionen!", sagte sie. "Auch wenn ich
mir das nicht so richtig zutraue. Und eigentlich keine Lust
darauf habe. Eine Revolution ist nicht unwahrscheinlich tödlich für die, die
sie einleiten, oder alternativ sehr traumatisierend. Die Erfolgsquoten
sind nicht riesig. Ich bin ein wenig egoistisch und würde eigentlich
auch noch gern was vom Leben haben."

"Hm.", machte Lilið. "Ich verstehe den Gedanken, aber ich finde
gerade schwierig, ihn mit einem suizidalen Charakter überein zu
bringen."

"Ist das so verwunderlich?", fragte Marusch. "Dass ich bei der
Frage, was ich mit dem Leben, das mich oft frustriert,
etwas anfangen möchte, was mir persönlich etwas bedeutet? Wovon
wahrscheinlich ist, dass es sich auszahlt?"

"Ich verstehe.", murmelte Lilið. Sie überlegte, ob sie noch
etwas dazu zu sagen oder fragen hatte, aber es gab nichts. "Noch
eine Option?"

"Ich hatte mir überlegt, vielleicht die Diebstähle und anderen
Kriminalakte weniger davon abhängig zu machen, wie antiautoritär
sie sind, sondern sie überall auf dem Planeten auszuführen und
dabei Kontakte zu schließen. Wie den zu Heelem.", erklärte
Marusch. "Das könnte ein gutes Netzwerk für eine Revolution
werden. Als Ausblick."

Lilið nickte wieder. "Ich denke, da könnte ich mich mit
anfreunden.", sagte sie. Es war ja auch kein ganz neuer
Gedanke. "Vielleicht könnten wir auch die Gruppe erweitern. Mit
mehr Booten wären wir sicherer. Was meinst du?"

"Klingt gut!", sagte Marusch. "Also sagen wir, es ergeben
sich in der Richtung Pläne und ich hätte keine Möglichkeit,
sie mit dir abzusprechen, bevor ich dich hineinplane, wäre
es für dich ein ausreichendes 'nötig' dafür, dass ich dein Leben
ein wenig riskiere?"

Lilið dachte an Allil. Aber eigentlich wusste sie, was sie
wollte. Und dass sie beide nun besser aufpassen würden, als
sie es in der Situation mit Allil getan hatten. Trotzdem
ging sie das Erlebnis noch einmal in Gedanken durch. "Ja, aber
ich möchte, wenn du mich spontan anders umbenennst, nicht mehr
Allil heißen."

Marusch nickte. "Den Gedanken hatte ich auch bereits.", sagte
sie. "Ich habe es vorwiegend getan, weil ich weiß, dass du durch
dein Training damals mit ihr auf diesen Namen reagieren wirst. Aber
mir war eigentlich nie wohl dabei, ihn für dich zu benutzen. Das
tut mir leid. Soll ich dir einen anderen Namen vorschlagen,
oder möchtest du?"

"Du hättest bereits einen Vorschlag?", fragte Lilið überrascht.

"Ich mag Aurin.", sagte Marusch.

"Oh, der ist geschlechtsneutral oder?" Aurin. Das klang schön, fand
Lilið.

"Genau! Wobei die seltsame Welt da draußen vermutlich sagen wird,
es wäre ein Name für beide Geschlechter." Marusch klang ein
wenig resigniert. "Aber der Name Lilið musste ja auch schon
lernen, für mehr Geschlechter erlaubt zu sein. Ich denke,
Aurin würde das auch lernen. Oder wie immer du heißen magst."

"Aurin.", antwortete Lilið.

"Darf ich dich in den nächsten Tagen zu Übungszwecken immer Mal wieder
so nennen?", fragte Marusch.

"Zu Übungszwecken.", erklärte sich Lilið einverstanden. "Ich heiße
immer noch Lilið."

"Das ist schön.", sagte Marusch. "Lilið. Magst du mit mir schlafen? Also,
in der müden Bedeutung?"

Lilið beugte sich über Marusch und gab ihr einen Gutenachtkuss auf
die Stirn. "Gute Nacht, Marusch.", flüsterte sie. Sie hatte dasselbe
schon einmal getan, bevor sie sich dann doch wieder unterhalten
hatten. Aber nun war sie innerlich ruhiger und konnte schlafen.

---

Lilið weckte sie, wie nicht unüblich, zum Sonnenaufgang. Da es eine
lange Reise werden würde, verschoben sie ihr Frühstück auf See. Sie
packten alles besonders organisiert, damit sie auf der langen Fahrt
mühelos an alles Wichtige drankämen, schoben die Ormorane in die
Brandung und starteten die Tagesreise. Nah vor Ufer brachen und schäumten die Wellen, sodass
das Einhängen der Ruderanlage ein frickeliges Unterfangen war. Die
Beschläge, in die sie fast gleichzeitig eingehängt werden musste, hoben
und senkten sich samt Heck. Obwohl Lilið darin geübt war, brauchte
sie dafür sicher ein paar Minuten.

Dann stiegen sie ein, die Segel dabei schon halb gefüllt, und
rauschten aufs Meer hinaus. Zunächst hatten sie Gegenwind, aber
dann führte sie die Route in einem Bogen um eine Insel herum, sodass
sie mit Wind schräg von achtern übers bald inselfreie Meer
jagten. Es war auf der Fahrt selten vorgekommen, dass sie gar kein
Land gesehen hatten, aber heute würde es über eine lange Zeit so
sein.

Der Wind war gleichmäßig stark, der Tag nicht so heiß, weil der
Himmel bedeckt und die Luft etwas diesig war. Nun, als der
Kurs einfach nur gehalten werden musste, holten sie ihr
Frühstück nach.

"Wir sollten gleich noch einmal eine Pause einlegen und
beidrehen.", beschloss Lilið. "Um uns im Meer zu erleichtern. Denn
in einer knappen Stunde oder so passieren wir die
Seenplattenströmungen und wechseln ihre Seite. Da ist über einige
Stunden eine Pause nicht drin."

Beidrehen war bei der Ormorane ein Manöver, bei dem das Vorsegel
auf der falschen Seite fixiert wurde, also, auf der Seite, von der
der Wind eher kam. Er drückte in das Segel, sodass es sich
nach innen bauchte, und trieb die Ormorane rückwärts an. Die Ruderanlage wurde beim
Beidrehen so fixiert, dass sie dabei das Boot in eine Richtung steuerte, dass der Wind
wieder ins Großsegel strich und es wiederum vorwärts antrieb. Sobald es
Fahrt nach vorn aufgenommen hatte, wirkte die Rudereinstellung umgekehrt,
sodass es das Vorsegel wieder mehr gegen den Wind drehte. Auf
diese Weise flatterten auch bei starkem Wind die Segel nicht, aber
das Boot bewegte sich kaum, immer nur leicht vor und zurück.

Sie wechselten sich bei ihrem Bad ab: Erst zog sich Marusch untenrum aus und
stieg, sich an einem Seil festhaltend ins Wasser, und dann war
Lilið dran.

Obwohl es nicht so warm war, tat die Abkühlung sehr gut. Lilið
wäre sonst bei der Aufregung danach vielleicht unangenehm heiß geworden, die
sie in sich fühlte, als sie zum ersten Mal mit einer Jolle auf
eine Seenplattenströmung zusegelte. Sie hielt lange Ausschau
nach dem Streifen an besonders starker Gischt mitten auf dem
Meer, wo die verschiedenen Strömungen aufeinandertrafen. Aber
sie spürte den Sog unter dem Rumpf bereits, bevor sie den weißen
Streifen entdeckte. Und als sie ihn entdeckte, war er schon
viel näher, als sie es erwartet hätte. Sie hatte die Diesigkeit
unterschätzt.

"Halt dich gut fest!", befahl Lilið. "Ich weiß nicht, ob ich
dich dort aus dem Wasser ziehen könnte, wenn du mir über Bord
gingest!"

Marusch nickte ihr mit einem Grinsen zu. Sie genoss das
alles, das war ihr anzusehen.

Lilið ließ sich anstecken und freute sich einfach auch. Sie
wurde sich bewusst, wie klein sie auf diesem Planeten
waren. Das war ein schönes Gefühl. Es verschob, was wichtig
und was unwichtig erschien.

Eine starke Strömung ergriff das Boot und riss es mit
einer Gewalt mit sich, dass kein Steuern der Welt einen
geraden Kurs darüber hinweg zugelassen hätte. Lilið verschenkte
keine Zeit, sich über den Kurs Gedanken zu machen. Sie steuerte
die Ormorane auf eine Art halb gegen den Wind und gegen die Strömung,
dass sie nicht umgeworfen würden, und dann erst, als die Strömung
gleichmäßiger wurde wieder senkrecht
dazu. Sie merkte, dass sie mehr seitwärts segelte, aber
das machte nichts. Sie mussten nur schnell genug von der
Strömung wieder heruntersein. Je länger sie abgedriftet
wurden, desto länger wäre hinterher ihr Weg zu ihrer
Zielinsel. Und würden sie zu weit abgedriftet werden, müssten
sie sie wechseln und die Reise würde sich um mindestens fünf Tage
verlängern. Vielleicht wären fünf Tage
länger reisen auch nicht so schlimm, überlegte Lilið. Sie
mochte das Reisen mit Marusch. Nun,
sie würden wohl ohnehin danach irgendwohin weiterreisen.

Aber als sie das Ende des Strömungsstreifens erreichte, hatte
sie sehr andere Sorgen als mögliche fünf Tage zusätzlicher Reise. Die
hoch peitschenden Wassermassen verrieten ihr, dass der
Strömungswechsel hier so stark sein würde, dass er das Boot
auseinanderreißen könnte.

Lilið steuerte über die Wellen, versuchte, die Ormorane
zum Fliegen zu bekommen, damit sie nicht irgendwann zeitgleich
mit dem Heck in einer anderen Strömung stecken würde als mit dem Bug. Aber
der Wind war nicht stark genug. Als sie die Grenze erreichten,
musste Lilið spontan entscheiden. Sie wählte einen Winkel, bei
dem sie fast entlang ihrer alten Strömung ausgerichtet in die neue eintraten. Aber
die neue riss sie trotzdem herum. Etwas knackte. Sie wurden aus
dem Strom herausgerissen und trudelten zwei Mal im Kreis. Lilið
und Marusch hatten alle Hände voll damit zu tun, die Segel daran
anzupassen. Zwei Mal schlug der Baum auf die andere Seite und
sie mussten sich ducken. Dann war der Spuk vorbei.

Lilið ließ kaum Zeit verstreichen. Nicht, dass sie wieder
auf die Strömung zutrieben! Sie setzte den neuen Kurs und holte
erst dann allmählich Atem. "Was hat geknackt?", fragte sie. "War
das einfach normal, weil Holz unter Last mal knackt, oder meinst
du, es ist was kaputt?"

"Ich denke, eher ersteres, aber fühl doch nach!", schlug Marusch
vor.

Lilið starrte sie einen Moment an. Ja, das konnte sie inzwischen. Sie
war es nur noch nicht gewohnt.

Sie legte den Handballen der Hand, die die Schot hielt, auf der lackierten
Bordswand ab und fühlte hinein. Sie fühlte die ganze Ormorane. Oder
fast die ganze. Der Mast bereitete ihr Schwierigkeiten, weil er aus
einem anderen Holz gemacht war und nicht mit der Ormorane sozusagen verwachsen. Er
konnte heruntergenommen werden. Aber sie fühlte den ganzen Rumpf. Sie
kannte die Ormorane, hatte sie unterbewusst über die ganze Zeit kennengelernt, nur
noch nie so bewusst erfühlt. Sie hätte sie falten können.

"Sie ist im Prinzip heile, nur in einem Bereich Holz am Bug
merke ich, dass sich das ganze kurz so verbogen hat, dass es
unter erneutem Einwirken von Last brechen könnte.", erklärte sie. "Ich
glaube, ich kann das verfalten. Auf Dauer. Sollte ich es jetzt
probieren? Oder, es darauf verschieben, wenn wir an Land sind,
falls ich Fehler mache? Wo es vielleicht auch eine Person tun kann, die
es gelernt hat?" Sie spürte die Aufregung, während sie das sagte,
mit ihrem Falten etwas zu tun, was wirklich nützlich war. Aber
sie hatte Angst, zu übermütig zu sein. Ein Verschmelzen zwischen
zwei Holztypen wie bei der Reparatur der Ruderanlage traute
sie sich noch nicht zu.

"Ich denke, wenn du es tust, haben wir weniger Risiko für uns, als
wenn ich noch einmal eine solche Marke einsetze.", hielt Marusch
fest. "Ich würde sagen, fühl über den Verlauf der Fahrt weiterhin
hinein. Und wenn es sich verschlimmert oder wir wieder in Strömungen
geraten könnten, machst du es dann. Und sonst erst, wenn wir angelegt
haben."

Lilið nickte. Das klang sinnvoll. Aber am Ende entschieden sie sich
doch noch einmal um: Lilið hatte den Eindruck, sie würde es auf der
Fahrt besser können. Die Ormorane fühlte sich im Wasser liegend für
sie natürlich vertrauter an als an Land. Also tat sie es, als ihre
Zielinsel in Sicht war. Es fühlte sich an wie das Heilen eines
verletzten, vertrauten Tiers, glaubte sie. Obwohl sie letzteres nie getan
hatte. Aber für sie war die Ormorane bei dem Vorgang lebendig
und ein Teil ihrer Familie.
