Staub
=====

*CN: Massaker, Horror, Hinrichtung, Misgendern - schlimm,
Sexismus, Transfeindlichkeit, Lebensmüdigkeit, Ableismus,
Sanism, Luftnot, magisches Würgen, vor Füße spucken - erwogen,
religiöse Rituale und Kleidung - mehrfach erwähnt.*

Liliðs Hände fühlten sich taub in den Fesseln an. Sie schirmten
nicht nur Magie ab, sie waren auch einfach ziemlich eng. Sie
stellten ein geringeres oder anderes Problem für Lilið dar, als
die Wachen glaubten, vermutete sie: Sie konnte zwar die Fesseln nicht falten,
aber mit etwas mehr Anstrengung sich selbst schon. Sie sah immer noch
aus wie Lajana. Sie könnte sich vielleicht sogar aus den Fesseln befreien, aber es
würde ihr vermutlich nicht viel helfen, weil in dem Augenblick
sofort alle Wachen hinter ihr her wären und wüssten, dass sie doch nicht
Lajana wäre.

Sie fühlte ein albernes, schlechtes Gewissen, weil
sie Lajana angelogen hatte. Von wegen, versuchen,
nachzukommen. Natürlich gab sie jetzt alles,
entfliehen zu können, aber dieses alles war halt
gerade nichts. Und das hatte sie vorher gewusst. Sie
saß gefesselt, mit geradem Rücken auf jenem Thronstuhl
und ihr einziger Trumpf war, derzeit noch für Lajana
gehalten zu werden. Das rettete vermutlich ihr Leben. Aber
es half ihr nicht, zu entfliehen.

Die Wache, die immer gedöst hatte, sprach mit ihr, als wäre
sie ein kleines Kind. Sie erklärte Lilið, dass es ihr leid
täte, aber die engen Fesseln leider eine nun verlangte Vorsichtsmaßnahme
wären. Und dass gegen Abend König Sper eintreffen würde. Angesichts
der jüngsten Ereignisse, dem missglückten Befreiungsversuch,
war es König Sper erlaubt worden, sich mit seiner Garde in diesen
Raum einzunisten. Außer zu einer bestimmten Nachtruhezeit. Lilið
hoffte, dass zu jener dann vielleicht die Wachsamkeit wieder genügend
gesunken wäre, dass sie eine Chance hätte, zu entfliehen. Als Igel
oder Ratte. Aber realistisch war das nicht.

Der Tag kam ihr bereits sehr lang vor. Sie hatte doch schon so
viel getan und erlebt. Trotzdem war erst früher Nachmittag. Hier
drinnen bekam sie davon erstaunlich wenig mit. Das Licht fiel
vielleicht anders durch die Fenster, aber Lilið fühlte den Tag
nicht.

Eine Wache meldete in beiden Sprachen, dass die beiden Eindringlinge
entkommen wären. Lilið atmete erleichtert auf. Damit waren sie und
Heelem gemeint, aber es bedeutete, dass Heelem mit Lajana wohl
in Sicherheit wären, soweit möglich.

"Das ist doch eine Nachricht, über die du dich sicher freust!", raunte
ihr die Wache zu, die sonst immer döste.

Lilið nickte und gab ein zaghaftes "Ich will nicht, dass jemand stirbt."
von sich. Das würde zu dem Bild der Wache von Lajana passen, überlegte sie.

Sie fühlte das schlechte Gewissen abermals. Sie hatte sich geopfert. Das
wurde ihr erst jetzt so richtig bewusst. Sie hatte einfach ihre Möglichkeiten
ausgereizt, das Ziel zu erreichen, dass Lajana die Sakrale verlassen konnte,
und dazu hatte gehört, ihre eigene Sicherheit zu vergessen. Zu
verschenken. Das hatte Lajana nicht gewollt und Lilið hatte es trotzdem
getan.

Sie bereute es nicht.

Es war alles so absurd. Drude hatte gemeint, sie könne sich nicht als
Königin ausgeben. Und nun tat sie eben dies doch, und das schon seit
mindestens einer Stunde erfolgreich. Als Kronprinzessin zumindest, weil
sie sich natürlich als die Rolle ausgab, in der die anderen Lajana
sahen.

'Mach Feuer!' Jäh widerholte ihre akustische Erinnerung Heelems Worte
in ihrem Kopf. Feuer. Marusch. Feuer, -- das hatte Marusch mehrfach
erwähnt --, war eigentlich etwas, was Menschen in der Schule eben
lernten, wenn sie vielleicht durchschnittlich oder etwas besser als
durchschnittlich in Magie waren. Es hieß nicht, dass Heelems Aufforderung,
Feuer zu machen, an Marusch gerichtet gewesen sein musste. Aber Heelem hätte
es selbst getan, wenn es hier so leicht gewesen wäre. Marusch konnte Feuer erzeugen,
ohne dass es etwas Brennbares zum Anzünden gab. Lilið erinnerte sich
an das Flammenmeer auf Espanoge, das Marusch erzeugt hatte, wie ein
Weinen. Sie konnte nicht verhindern, dass sich ihr Denken auf Marusch
versteifte, weil Heelem um Feuer gebeten hatte. Sie sollte zweifeln,
die Schlussfolgerung war einfach zu unsicher, aber die Hoffnung, die eben so jäh in
ihr aufstieg wie die akustische Erinnerung an Heelems Aufforderung,
ließ sich nicht niederringen.

Sie war in einer aussichtslosen Situation in einer Sakrale und
hatte plötzlich einen Glauben daran, dass Marusch noch lebte. Fast
eine Gewissheit.

Und wenn Marusch noch lebte, dann würde sie vielleicht veruchen,
Lilið hier herauszuholen. Lilið hoffte, dass sie es nicht tun
würde. Marusch riskierte zu leicht Dinge. Wahrscheinlich würden
sie dann beide enden. Aber vielleicht würde Heelem sie auch dazu
anhalten, genauer zu planen.

Was war eigentlich passiert? Wenn Heelem Marusch an ihrer Stelle
erwartet hatte, dann war Heelem schon beim Leuchtturm davon
ausgegangen, zu Marusch in ein Boot gestiegen zu sein. Das hieß,
zu einer gewissen Wahrscheinlichkeit war Marusch auch am Leuchtturm
gewesen, mit einem ebensolchen Boot. Vielleicht dem Boot auf der anderen
Seite. Und Heelem war einfach ins falsche gestiegen.

Heelem war in die Sakrale gesegelt. Also hatten Marusch und Heelem
dann wohl etwa den gleichen Plan gehabt wie Drude und sie: Im
Leuchtturm den Schleusenmechanismus auszulösen, mit einem sakralierten
Boot die Schleuse zu benutzen, um in die Sakrale zu gelangen, und anschließend
Lajana aus der Sakrale zu befreien. Lilið musste fast bei dem
Gedanken grinsen, wie Drude aus dem Leuchtturm gestürzt sein mochte, um dann
bei Marusch ins Boot zu steigen. Auf der anderen Seite des Leuchtturms. Nur,
dass Lilið Marusch dort nicht gesehen hatte, sondern nur ein leeres
Boot. Wo waren sie jetzt? Wie wäre ein Aufeinandertreffen verlaufen?

Lilið amüsierte außerdem der Gedanke, als ihr klar wurde, dass Drudes
und ihr Plan dieses Mal nicht an einer feindlichen
Ursache gescheitert war, sondern daran, dass zwei andere Personen fast
dasselbe geplant hatten und sie sich dabei in die Quere gekommen waren.

Die innerhalb der letzten halbe Stunde erst reparierte Tür zum
Saal der Verkündung wurde aufgestoßen und zwei Personen traten ein,
die Lilið kannte. Herr Hut und Wache Luanda. Eine denkbar ungünstige
Kombination an Personen. Nun war wahrscheinlich alles aus.

"Hallo Lajana!", grüßte Herr Hut sie. Er wagte es, sie, die
vermeintliche Kronprinzessin mit Namen anzusprechen. "Wie schön
dich zu sehen! Schön, dass deine Reise hierher trotz aller
Komplikationen noch ein Erfolg war."

Wache Luanda blickte ihr ins Gesicht. Sie wusste, was los war. Ungefähr
zumindest. Drude hatte vermutet, dass Luanda Lilið noch nicht wiedererkennen
würde, wenn sie Lilið irgendwo auf den Straßen Bellims begegnet wäre,
solange Lilið unter einer Sakralutte verborgen gewesen wäre. Aber
hier? Vielleicht schon, weil sie sie von der Kagutte kannte und
damit rechnete, dass die Person, die sich hier als Lajana ausgab,
auch auf der Kagutte gewesen sein könnte. Und dass sie nicht
Lajana war, wusste Wache Luanda eben, weil sie Magie erfühlen konnte.

Die Tür schwang erneut auf und weitere Personen sammelten sich
im Raum. Die Vorhut des Königs oder so, dachte Lilið.

"Es handelt sich nicht um die Kronprinzessin." Wache Luandas Verrat
tat interessanterweise weh.

"Red keinen Unsinn, ich weiß, wie sie aussieht.", widersprach
Herr Hut.

Wache Luanda atmete tief ein, und mit ihrem langsamen Ausatmen
spürte Lilið wie ihr die Faltung entglitt. Die Gefühlstaubheit,
die sie in den Händen spürte, durchdrang ihren ganzen Körper,
bis er schlaff im Stuhl und in den Fesseln hing. Es war ein
unangenehmes Gefühl, die Magie entzogen zu bekommen, obwohl sie
Magie eigentlich immer skeptisch gegenüber gestanden hatte. Es
fühlte sich an wie damals in der Schule, wenn sie etwas Magisches
bewerkstelligen wollte, es aber einfach nicht greifen konnte.

"Lilið?", fragte Herr Hut. "Lilið von Lord Lurch?"

"Wohl doch mächtiger, als du dachtest!" Die Stimme erkannte Lilið
als die von Wache Schäler. Konnte die Situation noch schlimmer
werden.

"Sie muss wohl doch auf dem Internat für skorsche Damen gut
dazu gelernt haben.", räumte Herr Hut ein. "Ich hatte gleich den
Eindruck, dass sie einen Faible für Illusionsmagie entwickelt."

Damit mochte er über Allil immerhin recht haben, dachte Lilið. Sie
reagierte nicht, fühlte sich innerlich abgestumpft, und wusste nicht,
ob es noch am Magie-Entzug lag oder an der Gesamtsituation.

Wache Luanda schnaubte. "Ich werte ungern Leistungen ab, ist einfach
nicht meine Aufgabe.", sagte sie. "Aber du hast nichts hier
verloren. Du spürst keine Magie, tut mir leid. Es war astreine,
ziemlich mächtige Faltmagie."

"Das kann ja mal passieren.", antwortete Herr Hut prompt. Lilið
bewunderte ihn fast für sein penetrantes Ansinnen, seine
Inkompetenz unterm Hut zu verbergen. "So mächtig kann sie
auch wieder nicht sein. Vielleicht hat jemand anderes sie
gefaltet."

"Raus!", kommandierte Wache Luanda. "Ich habe die Befugnis,
dich rauszuwerfen, und ich mache hiermit davon Gebrauch."

"Weil ich mal einen kleinen Fehler gemacht habe?", fragte Herr
Hut pikiert.

"Du gefährdest die Mission." Wache Schälers Worte waren unaufgeregt
und kalt. "Wenn ich ihm beim Gehen helfen soll, warte ich nur auf
deinen Befehl, Wache Luanda."

"Ich wollte ohnehin nur nach dem Rechten sehen." Herr Hut
verabschiedete sich und zog die Tür auf, um hindurchzugehen,
drehte sich aber dann doch noch einmal um. "Eins noch: Es sind
Gerüchte im Umlauf, dass Lilið von Lord Lurch und der Blutige Master
M eine Person sein sollen. Ich glaube da zwar nicht dran, aber Königin Stern,
oder eigentlich der Kronprinz Stern, haben bekannt gegeben, dass sie
möchten, dass der Blutige Master M ihnen lebendig übergeben werden
soll. Und dass, wer dies tue, in ihrer Gunst stünde. Was auch
immer das genau heißen soll. Aber vielleicht hilft es euch doch
noch, wenn ihr schon die Kronprinzessin nicht gefangen habt, dass ihr
wenigstens glaubhaft machen könnt, ihr habet den Blutigen Master
M gefangen genommen."

Die Tür schloss sich. Wache Luanda und Wache Schäler blickten
sich an. "Wo er dann doch mal recht hat.", meinte Wache Schäler.

Wache Luanda brummte. Es machte auf Lilið den Eindruck einer widerwilligen
Zustimmung. Dann wechslelten sie ins Alevische und Lilið verstand sie
nicht mehr. Ihre eigenen Gedanken holten sie wieder ein.

Die Frage 'Konnte es noch schlimmer kommen?' die sie sich vorhin
gestellt hatte, schien nun beantwortbarer. Es war schlussendlich etwas
besser gekommen, vorübergehend zumindest. Herr Hut war
weg, und das erleichterte Lilið, selbst wenn sein Verhalten in
der Vergangenheit eher zuträglich für sie gewesen war. Sie hatte Angst
vor Wache Schäler. Sie verknüpfte die kleine Person unweigerlich
mit zerschnittenen Körpern. Sie hegte eine gewisse Sympathie für Wache
Luanda, und das war wahrscheinlich sehr irrational. Wache Luanda konnte
genau so schlimm sein. An der Art, wie sich die beiden unterhielten,
ob nun auf Baeðisch oder Alevisch, hätte Lilið ohne Vorgeschehen keine
der Wachen als besonders unsympathisch einsortiert. Von Wache Luanda
konnte es ein Vorgeschehen an Brutalität geben, das Lilið lediglich
nicht selbst erlebt hatte. Letztendlich war es die Aufgabe einer Wache, in
gewissen Dimensionen brutal zu sein. Auch Drudes, fiel Lilið ein. Der
Punkt, der sie besonders positiv überraschte, war ihre vorübergehende
Lebensversicherung. Sie war entlarvt worden und hätte damit gerechnet, dass damit ein
schon recht zeitnahes Todesurteil im Raum stehen würde. Morgen oder
so. Das wäre ein üblicher Zeitraum, bis eine Hinrichtung an einer gefangenen,
kriminellen Person vollzogen würde. Zumindest im Königreich Stern. Aber
wenn erst Königin Stern oder
der Kronprinz kommen müsste, dann hatte sie etwas mehr Zeit. Eine
knappe Woche vielleicht. Aber wohl mindestens drei Tage. Vorausgesetzt,
König Sper scherte sich darum, dass sie für diesen Verhandlungszweck am Leben bliebe.

Die Zeit zog sich dahin. Nach und nach kamen weitere Personen in den
Saal, der Kleidung nach zu urteilen, Wachen oder Adel in hohen
Positionen. Lilið musste bei dem Gedanken innerlich fast grinsen, dass
sie heute Gelegenheit haben würde, hohem Adel und dem König selbst
vor die Füße zu spucken, wenn sie wollte. Sie würde das nicht tun. Sie
hatte Lajana versprochen, zu versuchen zu entkommen, und irgendwem vor die Füße zu
spucken verringerte ihre Möglichkeiten.

Die Fesseln schnitten so sehr in ihre Handgelenke, dass es schmerzte. Aber
viel unangenehmer empfand Lilið die tauben, prickelnden Finger. Sie
hatte schon mehrfach versucht, die Handgelenke etwas zu drehen, damit
mehr Blut in ihre Hände ströhmen könnte. Vielleicht sollte sie
versuchen, die Handgelenke unauffällig dünner zu falten. Nur ein wenig. Ob
die ausgeübte Magie auffiele? Vielleicht würden die Fesseln die
Magie ja abschirmen.

Wache Luanda blickte in dem Augenblick streng in ihr Gesicht, als sie
es versuchte. Lilið gab sofort auf.

"Die Fesseln sind sehr eng.", murmelte sie.

"Das gehört so!", erwiderte Wache Luanda schneidend. "Ich werde dich
nicht durch Unvorsicht entwischen lassen. Und nur, falls du dir
Gedanken über einen neuen Befreiungsversuch machst: Selbst wenn alle
Sakrallosen den Raum verlassen müssen wegen irgendwelcher Regeln. Ich
muss es nicht. Ich stehe im Dienst der Sakrale und darf mich aufhalten,
wo ich will. Ich werde dir nicht von der Seite weichen."

Lilið nickte. Sie wackelte mit den Fingern, aber es nützte nicht
viel. Sie fragte sich, ob sie lieber ausgepeitscht worden wäre. Ausgerechnet
die Hände waren taub. Wieder sprachen die anderen auf Alevisch weiter. Gedämpft
und doch aufgeregt. Lilið versuchte, auf anderen Gedanken zu kommen.

Warum wollte der Kronprinz den Blutigen Master M lebend? Hatte es
etwas mit dem Teil des Schatzes der Monarchie zu tun? War er vielleicht
nicht wieder aufgetaucht, weil Marusch etwas passiert war, und
nun wurde sie lebend gebraucht, damit sie ihn zurückgeben könnte? Aber
sie hatte keine Ahnung, wo das Buch war, wenn es nicht wieder
am Hof von Lord Lurch oder noch bei Marusch wäre. Und Marusch wollte
sie nicht verraten.

Die Tür wurde erneut geöffnet, aber dieses Mal änderte sich dadurch
schlagartig die Stimmung. Die Wachen und der Adel stellten sich sortierter an
den Rand, nahmen eine steifere Haltung ein. Die Person, die hinter zwei
unscheinbaren Bediensteten, die nun die Tür flankierten, den Raum betrat, war
unverkennbar König Sper. Lilið hatte nicht mit der Angst gerechnet, die
es in ihr auslöste. Er trat in den Verkündungsbereich, wo auch sie saß, blieb
wenige Meter vor ihr stehen, und hielt eine Ansprache auf Alevisch. Eine
lange. Es wurde in Gesprächspausen geraunt, aber Lilið verstand nicht, worum
es ging, bis er sie auf Baeðisch wiederholte.

"Meine verehrten Untertanen!", leitete er ein. "Ich wurde bereits
darüber unterrichtet, dass wir die Kronprinzessin Stern verloren haben und
unsere Handlungsposition sich verschlechtert hat. Aber das wird uns nicht
lange aufhalten. Schuld an der Flucht ist ein Verbrecher, oder sollte
ich sagen, eine Verbrecherin, die schon lange gefahndet wird und sich
besonders heikle Kunstverbrechen vornimmt. Ich rede von dem Blutigen Master M, oder
ab heute bekannt als die Blutige Mistress M." Leiser fügte König Sper
hinzu: "Das klingt auch gruseliger. Wir wissen alle, dass starke Frauen
gruselig sind und besiegt gehören. Und was will man von einer
erwarten, die schon kurz nach ihrer Geburt vom Orakel Lilið getauft wurde."

Ein Raunen ging durch die Menge. Lilið verstand es nicht: Sie wussten doch
alle, wie sie hieß und wer sie war. Nur, dass ihr nun wieder Weiblichkeit
zugeschrieben wurde. Und so sehr sie das unangenehm traf, so sehr fühlte
sie sich in dem Bild einer verabscheuungswürdigen, weiblich gelesenen
Kreatur wieder. Einer Furie vielleicht. Nur, dass sie kaum die Macht
hatte, die ihr zugeschrieben wurde. Immerhin hatte sie dieses
Mal das Verbrechen, für das sie angeklagt wurde, auch tatsächlich und
mit vollster Überzeugung begangen: Sie hatte Lajana befreit.

"Es ist im Prinzip ein großer Verlust, aber wenn ihr alle nun das
Richtige tut, wird hier niemand dafür bestraft. Abgesehen von Herrn
Hut, der die Lage mehrfach völlig unterschätzt hat und dafür
verantwortlich ist, dass Lilið von Lord Lurch überhaupt an Bord der Kagutte
und hierher gelangen konnte. Er verdient seine Strafe, und ich werde
sie persönlich ausführen, wenn hier alles vorbei ist. Aber ihr? Ihr
habt eine neue Chance!", fuhr König Sper fort. "Der Kronprinz Stern
möchte den Blutigen Master M lebend. Und zu unserer aller Überraschung
hat er es geschafft, seine Mutter, unsere geschätzte Königin Stern, zu überholen.
Er möchte mit uns hier und heute noch in Verhandlung treten. Seine Identität
wurde gründlich geprüft, es ist keine Falle zu erwarten, aber
natürlich sollten wir immer auf der Hut sein. Auf der Hut, haha!"

Des Königs Lachen war eigentlich kein freies Lachen, sondern eher
das ausgesprochene Wort 'haha', aber die Anwesenden fielen mit einem
adeligen Lachen ein.

Lilið registrierte es kaum. Wenn Kronprinz Stern schon hier war, dann
würde sie vielleicht doch nicht noch drei Tage zu leben haben. Und ihre vage
Fluchtidee für die Nacht wäre hinfällig. Sie brauchte am besten sofort eine neue
Idee, aber ihr Gehirn fühlte sich geschmolzen an.

"Wir bereiten gerade den Empfang vor.", verkündete
König Sper. Er verkündete ganz schön viel Unsakrales im Saal
der Verkündung einer Sakrale. "Er wird, wie sich das für Königliche, die ich
in ihre Schranken zu weisen gedenke, die Empfangstreppe zur seeseitigen Tür
hinauf nehmen, die gerade zu diesem Zweck angebracht wird. Dadurch wird die Sakrale
angreifbarer und ich brauche euch alle hier, um sie zu sichern. Alle. Ich habe alle
Antimagae meines Königreichs, die auftreibbar waren, hier herbestellt, und
das waren eine Menge, weil ich mich ja schon seit Wochen auf einen
Besuch der Königin oder des Kronprinzen einstelle. Wenn der Kronprinz
für die Übergabe der Blutigen Mistress M nicht kooperiert, dann werden wir ihn
gefangen nehmen und mit ihm das gleiche Spiel spielen, das wir uns
für seine Schwester ausgedacht haben. Nur direkter und gegebenenfalls
tödlicher." Er richtete sich an Wache Luanda, die wütend das Gesicht
verzog. "Natürlich möchte ich ihn nicht in einer Sakrale ermorden, und
eigentlich während eines Staatsbesuchs, in dem ich einen friedlichen
Verhandlungsraum verspreche, gar nicht. Es geht mir nur um den Fall,
dass er uns gefährlich wird. Wir kennen alle das Potenzial eines
hohen Skorems. Schaut mich an. Ich gehe verantwortungsbewusst damit
um, aber er? Wenn er König wird und ihm nicht reicht, was ihm
die Orakel zusprechen, verspricht das Krieg, der hier und jetzt
vermieden werden kann."

Die Flügeltüren auf der anderen Seite des Raumes öffneten sich und
für einen kurzen Moment konnte Lilið das Meer sehen. Für
diesen Augenblick beruhigte sie der Anblick, ließ sie atmen. Aber der Klang
und das Sein der Welt drang trotzdem nicht mit in den Saal. Er schirmte
auch bei geöffneten Türen alles ab.

Herein trat ein kleiner Mensch in der Uniform einer Wache. "Der
Kronprinz Stern bittet um eine Audienz."

"Jetzt schon?", fragte König Sper. "Er kann es wohl kaum erwarten."

Lilið musste zugeben, dass sie auch faszinierend fand, jetzt nicht
noch eine weitere Stunde oder etwas in der Größenordnung auf den
Kronprinzen warten zu müssen. Darauf, dass endlich alles vorbei
wäre. Vielleicht sollte sie diesen Augenblick der Ablenkung für
eine Faltung nutzen. Aber sie hatte es kaum gedacht, als sie wieder
die durch Wache Luanda verursachte Taubheit im Körper und ihren
bösen Blick auf sich fühlte.

"Die Audienz sei ihm jedenfalls gewährt.", sprach König Sper laut
und deutlich.

Es wurde von irgendwo viermal auf den Boden geklopft. Ein paar
der Wache schritten zeremoniell zur Flügeltür und öffneten sie
erneut. Und im Gegenlicht herein trat eine einzelne Gestalt. Lilið
hätte schon damit gerechnet, dass etwas Gefolge für den Kronprinzen
zugelassen würde. Aber die Gestalt betrat allein den Raum, in einem
Gewand, das Lilið überraschend gut gefiel. Es war nicht so
pompös wie das von König Sper, und trotzdem strahlte es so
etwas wie Macht aus. Es war ein Hosenanzug, fast ein Kleid,
glänzte seidig und schillerte an den Seiten, wo das Licht es
berührte, blaugrün wie das Meer dahinter. Dann schlossen sich
die Flügeltüren wieder und das Schillern verblasste.

Lilið war der Kronprinz in den Zeitungen und den Erzählungen
des Volkes nie sonderlich sympathisch erschienen. Sie fragte sich
in diesem Augenblick, ob auch für ihn ein falsches Bild gezeichnet
wurde, wie das im Falle von Lajana geschah. Auf der anderen Seite
gab es bei Lajana die Begründung, dass sie unskorsch und behindert
war. Der Kronprinz erfüllte kein Kriterium, für das eine Person
üblicherweise niedergemacht wurde. Er wurde als Luftikus abgestempelt,
gab das Bild eines Menschen ab, der alles leichtfertig und seine
Pflichten nicht ernst nahm.

Aber diese Gestalt stand dort am Anfang der Halle, von allen verlassen,
und doch, als könnte sie nichts umwerfen. Lilið konnte nicht
umhin, wenigstens den Mut dieser Person zu bewundern. Aber
wie albern war es, diesen Mut für einen vermeintlichen Verbrecher
aufzubringen?

König Sper brachte sich in eine exponiertere Position
und stellte sich lässig hin. "Du kommst allein? Ohne Garde?"

Dann hatte nicht König Sper Gefolge untersagt, sondern der
Kronprinz hatte sich dazu entschieden, allein zu kommen?

"Ich möchte niemanden mehr als unvermeidlich in Gefahr bringen."

Lilið gefror innerlich zu Eis. Das war Maruschs Stimme. Marusch! Jetzt,
wo sich ihre Augen wieder an das Licht gewöhnten, erkannte sie auch
das Gesicht und ihre Haare. Marusch brachte es und gab sich als
Kronprinz aus! Das war zum Scheitern verurteilt.

Lilið versuchte, sich zu beruhigen, als Wache Luandas Blick in ihre
Richtung schweifte. Natürlich nahm Wache Luanda ihre verräterischen
Stimmungen wahr.

König Sper lachte, dieses Mal zwar immer noch gekünstelt, aber
immerhin nicht in Worten. Interessanterweise klang das Lachen
in Liliðs Ohren freundlich und nicht hämisch. "Ich kann mich gerade nicht
entscheiden, ob du vernünftig bist und dich einfach
beliebigen Forderungen fügen willst, sodass du deshalb sicher bist, dass du
hier heile wieder herauskommst, ob du dich gnadenlos selbst überschätzt und
glaubst, dass du es mit meiner Garde im Alleingang aufnehmen kannst, oder ob
du lebensmüde bist."

"Die letzten beiden Optionen.", antwortete Marusch und ergänzte: "Bei
der Selbstüberschätzung bin ich mir nicht
sicher. Entsprechend würde ich mit meiner Forderung eröffnen."

Lilið schluckte und versuchte, nichts zu fühlen. Damit Wache Luanda
nichts ahnen würde. Zu den Gefühlen, die sie unterdrückte, gehörten
jähe Liebe für Marusch mit dieser Lebensmüdigkeit, die sie hier in
der Rolle des Kronprinzen nicht verbarg.

"Die wäre?", fragte König Sper sachlich.

"Ihr übergebt mir Lilið von Lord Lurch und ich verlasse mit ihr unbehelligt
die Sakrale.", forderte Marusch. Ebenso sachlich. Als ginge es um eine
routinierte Verabredung zum Mittagsspaziergang.

König Sper lachte wieder. "Ich weiß, dass du einen Skorem
von Drölfhundert hast." Er hob dabei die Hände, als ob er versuchte,
etwas zu zählen, was unzählbar war. "Aber dass
du in einer magiereduzierenden Sakrale voller Antimagae meine Wache
restlos ausschalten kannst, ehe du überwältigt oder noch viel früher
der Blutige Master M getötet wird, entzieht sich gänzlich meinem
Vorstellungsvermögen." König Sper korrigierte auch dieses Mal mit
einem Lächeln in der Stimme: "Die Blutige Mistress M, wie wir
herausgefunden haben."

"Ich würde das Blutige Master M als Bezeichnung wählen.", korrigierte Marusch. Es
war ein interessantes Gefühl, dass sie es vor allen korrigierte, und
Lilið zugleich wusste, dass es für die anderen als Abwertung und
nicht als Wertschätzung ankommen würde. Für sie war es
letzteres.

"Auch gut!" König Sper lachte wieder wohlwollend. "Aber du überschätzt
deine Handlungsposition gewaltig."

"Willst du es darauf ankommen lassen?" So eine sachliche, beiläufige
Frage.

"Du meinst es ernst.", erwiderte König Sper. Alles Lachen und jeder
Schalk war aus seiner Stimme verschwunden.

Marusch trat einige Schritte näher an König Sper heran, bis sie
mitten im Raum stand, viele Wachen hinter sich, und nickte bloß.

"Ich habe keine Lust auf Krieg.", hielt König Sper fest, klang
fast defensiv dabei. Als sollten sie mal wieder auf den
Boden zurückkehren. "Die ganze Sache hier hat den Zweck, Krieg
zu vermeiden."

"Das ist Unsinn.", widersprach Marusch. "Die Sache hier
hat den Zweck, einen Machtkrieg zu führen, der möglichst
wenige direkte Opfer fordert. Ein Krieg bleibt es
trotzdem. Wenn es dir darum ginge, gar keinen
Krieg zu führen, bräuchtest du einfach nicht anzugreifen und dich mit dem
zufrieden zu geben, was du hast."

"Mir steht aber mehr zu, als ich habe.", donnerte König Sper, mit
einem Mal so laut, dass Lilið zusammenzuckte und ihre Fesseln
mehr einschnitten.

Marusch hingegen blieb gelassen. Als der Hall verklungen
war, erklärte sie: "Das ist bei einem derart aus den Fugen geratenen Gefüge schwer zu
sagen. Meine Schwester wäre schon seit etwa zwei Jahren Königin, wenn
nach dem Gesetz gehandelt worden wäre, nach dem irgendwem irgendwas
zusteht. Sie würde dann von den Regelungen unter Druck gesetzt,
Verträge mit dir zu unterschreiben, aber offenbar ist es rechtlich
nicht so festgeschrieben, dass sie unterschreiben und sich fügen
muss, wie ihr das immer behauptet, sonst hättet ihr nicht so viel
Angst davor, dass sie es nicht tut."

"Ich habe keine Angst davor.", korrigierte König Sper
wütend. "Ich würde in dem Fall einfach einen wahren Krieg führen, den ich
gewinnen werde. Eine Wahnsinnige mit Machthunger sollte nicht
regieren. Alle werden mir dankbar sein."

"Ich denke, damit ist hinreichend widerlegt, dass diese ganze Sache nicht
nur dem Zweck dient, Krieg zu vermeiden.", stellte Marusch fest,
wie wenn es um einen mathematischen Beweis ginge, und holte
dann tief Luft, um eindringlich zu verstehen zu geben: "Ich mag warnend hinzufügen: Ich
werde wütend, wenn du meine Schwester als machthungrig oder wahnsinnig
bezeichnest. Zum einen, weil es entfernter von der Wahrheit kaum
sein könnte, und zum anderen, weil du es abfällig tust. Und Wut
macht mich gefährlich. Reden wir also lieber
sachlich und nicht abfällig, in Ordnung?"

"Ich mag dir eine Verständnisfrage stellen, denn ich verstehe
deine Ziele nicht.", erwiderte König Sper unbeeindruckt. "Wenn
du keinen Krieg willst, warum lehnst du dann das Amt als König
ab? Du könntest so viel Land ganz einfach haben."

"Königin, wenn." Marusch ließ die Korrektur einen Moment
im verwirrten Raum stehen.

Lilið war von der Verwirrung nicht ausgenommen, aber aus einem anderen
Grund: Marusch spielte hier keine Rolle. Lilið hatte bis eben geglaubt,
Marusch gäbe sich als der Kronprinz aus. Aber dann würde sie jetzt nicht
das Maskulinum korrigieren. Marusch war Kronprinzessin?

Maruch fuhr fort: "Ich bin zweitgeborene Kronprinzessin. Und ich möchte
nicht regieren. Ich frage mich, warum, wenn ich so viele Inseln haben könnte,
ich nicht die Regierung über die Inseln, die Königreich Stern derzeit bilden, an
die Person meiner Wahl, meine Schwester, abgeben kann. Warum möchtest
du mehr haben, als du hast, wenn sie regieren würde, aber würdest ohne
Zögern Inseln abgeben, wenn ich regieren würde, und hältst dieses System
irgendwie für sinnvoll?"

Lilið drehte sich alles im Kopf. Es war so ein Unsinn. Das System,
dieses Gespräch, es fühlte sich alles nach einer Dynamik an, von
der Lilið nie ein Teil sein könnte, weil es sich so falsch
anfühlte, dass sie nicht danach handeln könnte.

"Sobald du König wärest, stünde die königliche Garde hinter dir und
müsste dir Folge leisten." König Sper sprach die Worte wütend
und laut und Lilið verstand nicht warum, verstand nur, wie sehr
das Wort 'König' nun endgültig schmerzte und wollte, dass
alles zu Grunde brannte. "Die Bundesorakel", fuhr König Sper
fort, "haben eingeschätzt, dass
du mich mit deinem Skorem als Befehlshaber in die Tasche stecken
könntest. Was hilft es mir da, mich zu wehren? Du könntest der
mächtigste König aller Zeiten werden!"

"Immer noch, wenn, dann Königin.", dieses Mal schrie Marusch. Lilið
hatte diese Stimme nie so laut und beeindruckend gehört,
wie nun, hatte gar nicht gewusst, dass das möglich war. "Wenn
du je Behauptungen aufstellst, dass
du nur zum Schutze oder Wohle des Volkes entscheiden würdest, erinnere
dich daran, dass der ganze Sinn dieses Systems ist, Macht an die 
jeweils Stärkeren zu verteilen und nichts mit Volk und Güte zu tun hat."

"Du kannst durch die Behauptung, du wärest in Wirklichkeit eine Frau,
eure Rangfolge nicht ändern.", sagte König Sper, nun wieder
in sachlichem Tonfall, aber die Worte verbrannten Liliðs letzten
Geduldsfaden. Alles tat in ihr weh, alles war so falsch.

"Halt die Fresse!" Marusch hatte das tatsächlich durch den
Raum geschrien. Sie atmete einige Male hörbar, um sich
zu beruhigen. "Ich bin Kronprizessin, auch da gibt es nichts
zu diskutieren. Ich brauche keine Änderung der Rangfolge, um meine
Regentschaft abzulehnen. Das habe ich getan, indem ich zu meiner Krönung
nicht anwesend war. Ich glaube, ich habe klar gemacht, was ich will. Kommen
wir zur Sache?"

"Nun gut.", brummte König Sper. "Du drohst also ohne königlichen
Status mit Krieg, als Einmannarmee, gegen alle hier, sehe ich das richtig?"

"Einpersonenarmee.", korrigierte Marusch atemlos. Und fügte dann
die unverblümten Worte hinzu: "Ich würde eher von einem Massaker reden
als von Krieg."

"Ich möchte, einfach für den Fall, dass ich dich völlig unterschätze, gern
eine Machtdemonstration von dir sehen, bei der niemand zu schaden
kommt." König Sper sprach wieder so, als wolle er sie auf den
Boden der Tatsachen zurückholen, und dieses Mal fühlte es sich
tatsächlich nach etwas Sinnvollem an. "Meinst du, das ist drin."

Marusch gluckste freudlos. "Was meinst du, was in dem Moment hier passiert, in
dem ich beweise, dass ich gegen alle Antimagae und die Materie der Sakrale
Magie ausführen kann? Egal welche."

"Also sollen wir dir einfach glauben." König Spers Stimme klang bedrohlich
und skeptisch zugleich.

Marusch schüttelte den Kopf. "Ich bin schon bereit dazu.", widersprach
sie. "Egal, wie sehr ich euch und alles
hier hasse, wäre doch mein Vorzug, wenn ihr mir Lilið von Lord Lurch übergebt
und wir die Sakrale unbehelligt verlassen, wie ich sagte. Ich halte es lediglich
für Unfug, eine Machtdemonstration durchzuführen, ohne vorher abgesprochen
zu haben, dass ich in dem Moment, in dem mich oder Lilið von Lord Lurch
jemand angreift, ohne Rücksicht auf Verluste alle und
alles vernichten werde, was zwischen mir und meinem Ziel steht. Ich gehe da
kein Risiko für sie oder mich ein. Wir hatten lange keinen Krieg. Ich habe
keine Kriegserfahrung. Ich kenne keine Strategien, um Schaden zu
minimieren. Ich habe keine Lust auf dieses System und alle Personen, die es
unterstützen. Ich halte nichts von dem Unfug, der hier läuft. Ich denke
nicht, dass irgendetwas wirklich rechtfertigen darf, was ihr hier
abzieht. Ich werde im Falle, dass ich mich unsicher fühle, hier alles
zerlegen."

Eine Weile lag der Raum in Stille, nachdem Marusch geendet hatte. Die
Worte verklangen. Das Schweigen tat gut. Dieses Ritual, das Lilið
hier durchgeführt hätte, erschien ihr auf einmal um so viel sinnvoller,
als was hier ablief. Es hätte sie vielleicht innerlich aufgeräumt. Ein
Teil von ihr fragte sich, ob sie der Sakrale doch beitreten
wollte. Es war albern. Allein schon wegen ihres Namens. Vielleicht
hätte sie sich, statt sich diese Gedanken zu machen, mental eher auf ein
Massaker vorbereiten sollen, aber sie konnte nicht.

"Und du glaubst,", brach König Sper schließlich die Stille, "dass
irgendetwas so eine Drohung rechtfertigt, wie du sie hier aussprichst?"

Marusch zuckte mit den Schultern. "Ich habe keine Ahnung.", sagte
sie gleichgültig. "Es wäre schön gewesen, wenn ihr
ohne Gewaltandrohung einfach nicht so furchtbare Gräueltaten tun würdet,
wie Menschen zu entführen. Es wäre schön, wenn ein so gewaltvolles System,
dass meine Schwester dermaßen entwürdigt und niedertritt, ohne Einfluss
von Gewalt zerspränge, und das könnten Mächtige wie du bewerkstelligen,
wenn sie aufhören, danach zu handeln. Aber nichts läge euch ferner, als
Macht abzugeben, um Menschen auch nur die Möglichkeit zu geben, zu heilen."
Marusch machte eine kurze Sprechpause und stellte sich dabei in einen stabileren
Stand. "Ich behaupte nicht, dass mein Umgang mit der Situation akzeptabel wäre. Ich
weiß nicht, wie ein akzeptabler Umgang aussehen könnte, obwohl ich mich
mit der Frage seit Jahren befasse. Aber ich kann nicht mehr. Ich möchte nicht
mehr. Ich bin Kronprinzessin, nicht Königin. Ich bin hier nicht als
Regierungs- sondern als Privatperson. Und ich möchte die Menschen
mitnehmen, die ich liebe, und gehen, und was dabei von euch zurückbleibt,
ist mir egal."

Wieder wurde es still. Bis irgendwo eine Stimme laut wurde, die Marusch
des Wahnsinns bezichtigte. Andere Stimmen setzten zustimmend ein. König Sper gab
den Befehl, Marusch festzunehmen. "Diese Macht, die er hier
verspricht, gekreuzt mit seinem Wahnsinn muss eingedämmt werden.", schrie
er über das Raunen der Menge hinweg. "Ergreift ihn, und wenn er dabei
getötet wird, ist das ein Preis, den ich bereit zu zahlen und auszubügeln
bin."

Dann schoss die erste Welle an Wucht durch den Raum. Mehrere
Leute stolperten. Marusch blieb ungerührt stehen, es wehte nur ihre Kleidung.

Mit einem Mal konnte Lilið nicht mehr atmen. Physisch nicht. Magie
schnürte ihr die Kehle zu. Sie fühlte sich zu überwältigt, um
Panik zu fühlen, zu ergeben gegenüber dieser Situation, aber als Angstgefühl
doch drohte, einzusetzen, weil ihr wieder so schwindelig wurde, war
es auch wieder vorbei.

Lilið nahm die Änderung im Raum von einem Moment auf den anderen wahr, nur
über Schwingungen. Es war leise, aber das Schweigen der Welt erstarb. Die
kühle Ruhe des abgeschirmenden Gebäudes war durch eine
andere Ruhe ersetzt worden. Eine Ruhe aus Tod und Staub. Es war ein unheimlicher
Moment. Lilið wusste, dass alle außer Marusch und ihr nun tot waren, bevor sie
es sehen konnte. Sie hörte kein Atemgeräusch mehr. Vor allem fühlte sie, dass
sich die chemischen Verbindungen aufgelöst hatten, die alles zusammenhielten
und definierten. Sie blickte sich um und sah keine Menschen mehr, sondern
bloß perfekte Abbilder von Menschen. Sie fielen nicht um, dazu war zu wenig
an ihnen noch Wesen. Die Körper zerrieselten. Nicht auf einen Schlag, sondern
zunächst fast unauffällig. Zuerst die Dinge, die überstanden und von der Schwerkraft
nicht aufeinander gehalten wurden. Nasen rieselten aus Gesichtern. Haare
zerstoben gelegentlich, manche Ohrmuschel gab nach. Dann fielen halbe
Gliedmaßen von Körpern und zerschellten lautlos auf dem Boden zu Haufen
aus Staub.

Lilið blickte in eines der Gesichter, als es zerfiel, und war vielleicht
zum ersten Mal in ihrem Leben froh, dass sie kein gutes Gedächtnis für
Gesichter hatte. Sonst wäre es vielleicht persönlicher gewesen. Dann blickte
sie zu Marusch. Einen kurzen Moment hatte sie Angst, dass Marusch auch
nicht mehr lebte, weil sie so starr dastand. Aber sie fühlte das Leben. Es
war nicht schwierig, im Staub um sie herum das eine bisschen Leben zu
erfühlen, das noch da war. Das einzige Leben, dass ihr örtlich
nah war. Denn anderes Leben fühlte Lilið
durchaus, dessen Gefüge das Gebäude vor der Verwandlung abgeschirmt hatte. Sie
fühlte wieder das Wetter und das Meer um sie herum. Maruschs Magie hatte
vor der Sakrale nicht halt gemacht. Ein Gebäude, dem Heelem nichts hatte
anhaben können, der Türen aus der Ferne zerschneiden und aufwickeln konnte,
hatte Marusch in einem Wimpernschlag vollständig zu Staub zerlegt. Die
Mauern hielten länger als die Abbilder der Menschen, aber auch sie
begannen, wegzuwehen. Der Wind packte den noch haftenden Staub und riss die
Stockwerke über ihnen davon, ohne dass es ein besonderes Geräusch verursachte. Kein
Knarzen oder Ächzen. Nur das Geräusch von Wind und Meer um sie herum. Es
war seltsam erleichternd, dass das Wetter seit vorhin der Flaute entkommen
und lebendiger geworden war. Marusch schritt über den unter ihr
wegrieselnden Boden auf Lilið zu. Irgendetwas
machte sie wohl mit dem Boden zu Liliðs Füßen, der sie noch hielt.

Lilið schüttelte die Fesseln ab, die nur Momente später nicht mehr als
solche erkennbar waren, und blickte ein letztes Mal auf die Überbleibsel
von Körpern, die der Wind noch nicht mitgenommen hatte. Als die Mauern
weggebrochen waren und den Wind nicht mehr aufhielten, ging alles ganz
schnell. Marusch stand ihr gegenüber, nur noch einen Schritt von ihr
entfernt, als die Überreste vollends in Staub zerstoben und aufs Meer
hinausgetragen wurden. Marusch hatte nicht die ganze Sakrale zerstört. Ein
Teil des Gebäudes stand noch, die offenen, leeren Räume klafften als
dunkle Krater, weit hinter ihnen. Das Abendrot tauchte das Bild in
ein romantisches Licht.

Lilið fragte sich, was in dieser Situation berechtigte Gefühle gewesen
wären. Sie war sich ziemlich sicher, dass Schönheit über das Vergehen
und den Zerfall zu fühlen, eher kein Gefühl war, dass Zuhördende einer
Geschichte in diesem Zusammenhang unkritisiert stehen lassen würden, vor
allem dann nicht, wenn es sich um den Bericht von etwas Realem handeln würde. Es waren
gerade viele Menschen gestorben. Und ihre Freundin war die Mörderin. Das
waren die Fakten. Aber sie fühlte stattdessen so etwas wie schwere
Erleichterung. Sie fühlte den Wind, hörte das Meer, roch das Salz und sah Marusch. Den
Menschen, den sie gerade oder auch immer bei sich haben wollte, unabhängig
davon, was geschehen war oder was Marusch getan hatte. Es war vorbei. Es
hatte alles keinen Sinn ergeben. Es war ein undurchdringliches Gestrüpp
aus Politik gewesen, die vorgaukelte, wichtig zu sein, in Wirklichkeit
aber allen schadete, ohne dass es völlig offensichtlich greifbar
wäre. Eine fremde Welt aus Fremdbestimmung, in dem sie nie als sie selbst
hätte interagieren können, weil sie nicht darin vorgesehen war. All das war
verweht. Hatte sich fast zart im Wind aufgelöst, war zerrieselt wie eine
Sanduhr, war zerfallen und vergangen. Was wäre schon ein guter Ausweg
gewesen? Dies war ein ästhetischer. Einer, der Lilið mit einer Gewissheit
des Möglichen zurückließ, einer inneren Ruhe, dem Rauschen des Meeres, das
sinnvoller war als irgendein System, und mit Marusch.

Sie konnte nicht ausmachen, von wem der Impuls ausgegangen war, aber
sie überbrückten den letzten Abstand zwischen ihnen, bis ihre Körper
sich berührten, gegeneinander lehnten. Erst dann umarmeten sie
sich, langsam und sanft.

"Es tut mir leid.", flüsterte Marusch.

"Dass du keinen Weg gefunden hast, mir mitzuteilen, dass du noch lebst?", fragte
Lilið, denn was hier passiert war, verzieh sie viel leichter. "Dass
Lajana deine Schwester und die Identität, die du mir nicht offenbaren
wolltest, die des vermeintlichen Kronprinzen, also der anderen Kronprinzessin,
ist? Oder immer noch die Sache mit Allil?"

"Dass ich das Buch ohne dich übersetzt habe.", raunte Marusch.

War das Schalk? In dieser Situation? Fühlte Lilið Neugierde? Jetzt?

"Worum ging es?"
