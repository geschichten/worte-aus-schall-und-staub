Einbruch und Aufbruch
=====================

*CN: Brandverletzungen, Ableismus, Transfeindlichkeit, Misgendern,
sexuelle Übergriffigkeit, Einbruch, Alkohol.*

"Lilið!" Maruschs Flüstern klang fast wie ein Flehen.

Lilið hielt ihren zusammengebrochenen Körper immer noch in
ihren Armen. Sie hatte Maruschs Körpergewicht gewaltig unterschätzt,
fiel ihr dabei auf. Maruschs Knochen im kraftlosen Körper
gaben zwar auch ein wenig Halt und verlagerten Gewicht in den
Boden, aber das übrige Gewicht trug Lilið. Sie klammerte ihre Arme um
den Körper und grub ihre Lippen und Nase, sowie ihren Atem vorsichtig und sanft
in Maruschs Haar. "Ich bin da." Was sollte sie sonst sagen? Es erschien
ihr eigentlich nicht sehr hilfreich, über diese Offensichtlichkeit
zu informieren. Aber vielleicht half ihre Stimme ja.

Immer wieder flackerten kleine blaue Flämmchen um sie auf. Nur kurz und
meistens nur auf dem Boden um sie herum, aber manchmal auch auf
Maruschs Haut. Sie brannten, wo sie Lilið berührten, aber nicht so
sehr, dass sie Brandblasen hinterlassen würden, höchstens gerötete
Stellen. Es war wie ein Weinen, fand Lilið, nur dass die Heulkrämpfe
keine Tränen hervorbrachten, sondern Feuer. Feuer war also Maruschs
Magie, schloss Lilið. Das hatte sie nicht zum ersten Mal festgestellt. Sie
mochte es, weil es sie an ihre Mutter erinnerte.

Und die Magie war gerade besonders stark, weil Marusch emotional etwas neben
der Spur war, überlegte Lilið. Das hatte sie vor allem bei sich
schon oft beobachtet, aber auch nicht selten bei anderen, dass Magie
in solchen Fällen besonders greifbar war.

"Lilið!" Dieses Mal war das Flüstern schon etwas sachlicher. "Wir
müssen hier weg."

"Also nicht erst schlafen?", versicherte sich Lilið.

Wieder loderten die Flammen auf, und dieses Mal bis in Maruschs Gesicht und
Haar. Lilið verbrannte sich die Lippen und gab ausversehen einen kurzen, zischenden
Laut von sich, als sie die Luft anhielt, um den Schmerz auszublenden. Sie
schloss vorsichtig, dass es für Marusch eine sehr schlimme Vorstellung wäre, noch
eine Nacht hier zu bleiben.

Eigentlich hatte sie Marusch nicht mitbekommen lassen wollen, dass
ihr etwas weh tat. Weil es nicht schlimm war, dass es so war, aber
Marusch es sicher belasten würde, als wäre es schlimm. Daher ärgerte
sie sich ein wenig, dass sie sich nicht besser zusammengerissen
hatte, sondern ihr dieser Laut entwischt war.

"Ich will dir nicht weh tun!" Das Flehen war in Maruschs Stimme wieder
zurückgekehrt. Dieses Mal war es außerdem kein Flüstern mehr, sondern
ein gepresstes Murmeln. "Ich will keine Last für dich sein. Ich will
dir keine Schwierigkeiten machen. Ich würde dir am liebsten sagen, lass
mich gehen, lass mich allein!"

Lilið klammerte die Arme bei diesen Worten reflexartig noch fester
um Marusch. Dann ließ sie sie doch wieder lockerer. "Ich sollte dich
nicht davon abhalten zu gehen, wenn es ist, was du willst.", antwortete sie. "Aber
ich habe das Gefühl, das ist irgendwo in deinem Inneren doch nicht, was du
willst. Dass da ein 'aber'-Teil fehlt." Trotzdem spürte sie Angst. Sie
wusste nicht einmal so genau, wovor. Denn zu ihrer Überraschung war
es keine Trennungsangst.

"Als Gespann haben wir eine Chance bis Nederoge.", sagte Marusch, nun
wieder fast sachlich. "Alleine sieht das schlecht für dich aus. Es ist ein recht
pragmatisches 'aber'."

Es war nicht ganz das, was Lilið erwartet hätte, aber es passte ins
Bild, überlegte sie. Marusch hatte davon erzählt, gefühlstot zu
sein, außer die Wut oder Trauer tauchte auf. Und die Wut, oder
vielleicht sogar viel mehr der Hass, bezog
sich auf irgendetwas, was beim Essen auf der Terasse vorgefallen
war. Das Gefühl war so stark, dass es die Verliebtheit verdrängt
hatte. Oder so etwas in der Richtung. Mit dem Part der Analyse war sich Lilið
nicht ganz sicher. Jedefalls passte für sie ins Bild, dass derzeit
kein Gefühl Marusch dazu verleitete, bei Lilið zu bleiben, sondern
dieser Pragmatismus. Dass sie zusammenreisen mussten, war die
logische Konsequenz daraus, dass Marusch es sich zum Ziel gesetzt
hatte, Lilið zu helfen, zu überleben. "Sehr
aufopferungsvoll.", kommentierte Lilið. "Aber in Ordnung. Wir
ziehen weiter."

"Fühlt es sich mies für dich an, dass ich für dich gerade nichts
empfinde?", fragte Marusch leise.

"Das stimmt so nicht.", korrigierte Lilið. Es amüsierte sie fast,
dass sie nun auch so sachlich dabei klang. "Du empfindest für mich anders,
ja. Du fühlst gerade die Verliebtheit nicht, wenn ich das richtig
wahrnehme. Aber du hättest nicht meinen Namen gefleht, wenn da
gar keine Gefühle wären."

Lilið hätte kaum damit gerechnet, dass der Körper, den sie festhielt,
noch kraftloser werden könnte. "Ich weiß es nicht.", flüsterte
Marusch. "Vielleicht war da vorhin noch etwas. Ich weiß nicht einmal,
was."

"Ich auch nicht.", antwortete Lilið. "Aber um auf deine Frage
zurückzukommen: Selbst wenn nicht. Selbst wenn du nichts für mich
fühlst im Moment. Du hast mir von deiner Gefühlsleere erzählt,
und ich habe dir gesagt, dass ich dich auch damit mag. Das meine
ich so. Ich habe nicht erwartet, nur eine alberige, verspielte,
romantische Marusch voll Schalk und warmer Gefühle in
meinem Leben zu haben. Ich will dich ganz und gar darin haben und nicht
nur Teile von dir. Ich will es nicht anders. Also, solange
du darin gehabt haben willst."

Weil Marusch sich einen Moment gar nicht rührte, befürchtete Lilið,
eventuell doch wieder wie vorhin etwas Schlechtes gesagt zu haben. Vielleicht
noch etwas Schlimmeres, sodass Marusch sich nun beherrschen musste, nicht
wieder Flammen über ihre Haut rinnen zu lassen, weil sie noch
sengeder geworden wären.

Aber stattdessen kühlte der Körper, den Lilið umklammert hielt,
schließlich ab und begann, sich wieder mehr selbst zu halten. "Lilið.", flüsterte
Marusch abermals.

"Du magst meinen Namen wohl, schätze ich!", konnte Lilið sich
nicht abhalten zu sagen. Kurz darauf fürchtete sie wieder, dass
Albernheit gerade vielleicht unpassend wäre.

"Ja, sehr.", bestätigte Marusch zu ihrer Erleichterung einfach. "Und
dich, wenn ich gerade etwas fühlen würde." Marusch löste sich aus der
Umarmung und blickte ihr ins Gesicht. "Wenn ich wieder etwas sortierter
fühle und du dann immer noch so fühlen solltest, sagst du es mir dann
noch einmal?", bat sie. "Oh, großer Fosh, kostet mich das Mut, das zu
fragen. Ich komme mir so ausnutzend vor. Aber es ist die schönste
Liebeserklärung, die ich je gehört habe, und ich würde sie gern einmal
fühlen."

Lilið nickte. Sie versuchte halbwegs erfolgreich, die Tränen herunterzuschlucken, die
ihr kamen. Vielleicht an Maruschs statt, aber das wollte sie eigentlich
nicht einmal denken. Es war ein uraltes Misskonzept, dass Leute, denen
schlimme Dinge passierten, die aber kein Leid darüber fühlten, im
Allgemeinen etwas davon hätten, wenn andere an ihrer statt ihre Gefühle
fühlten. Das konnte, im Gegenteil, auch sehr stressig für sie sein. "Wenn
sich nichts ändert, sage ich dir das gern auch immer wieder.", sagte
sie mit stolpernder Stimme. Ihre Atemwege waren verklemmt, weil sie
gerade sehr viel fühlte. Nein, nicht an Maruschs statt, wie sie nachträglich
analysierte, sondern weil sie sich gut dafür fühlte, die Worte gefunden
zu haben, die Marusch hören wollte, und wegen der Bedeutung
und Emotionalität der ganzen Situation.

"Stielst du jetzt mit mir unser Boot?", fragte Marusch leise.

"Genau genommen ist das kein Diebstahl. Es gehört uns ja.", antwortete Lilið, dabei
besserwisserisch einen Finger hebend. Sie ließ ihn auch sofort wieder
sinken. "Ich werde in brenzlichen Situationen oft albern. Ich habe
den Eindruck, das ist gar nicht so hilfreich für dich."

"Doch ist es.", widersprach Marusch. "Ich fühle mich dumpf, es dringt
nicht so richtig durch, aber es ist das richtige Mittel. Bleib
dran!" Einen kurzen Augenblick hatte sich wieder ein Schmunzeln
ins Gesicht geschoben, aber es saß da nicht richtig. Der dumpfe,
leere Ausdruck von zuvor ließ sich nicht viel Zeit, um
zurückzukehren. "Ist es überhaupt möglich, mitten in der
Nacht aufzubrechen? Spricht da nautisch oder navigatorisch
oder wie sich das nennt etwas gegen?"

"Ich müsste umplanen.", sagte Lilið. "Ich kann das, das geht. Wenn
dir eine Vorinsel von Espanoge reicht, sollte ich es hinkriegen, ohne
vor Abfahrt noch einmal auf eine Karte gucken zu müssen. Das reicht dann morgen im
ersten Morgengrauen. Wenn es weiter weg sein muss, müsste ich vor
Abfahrt umplanen. Und es würde vermutlich eine Fahrt von fünf Stunden durch
die Nacht werden. Verbotender Weise ohne Licht, denn wir haben ja keines."

"Besteht die Möglichkeit, dass wir auf eine Vorinsel segeln und dort
entscheiden, ob es noch weitergeht, und du planst dort?", erkundigte
sich Marusch.

"Schon, aber es ist Nacht. Ich bräuchte dann dort Licht zum
Navigieren.", gab Lilið zu Bedenken.

Marusch hob eine Hand und ließ sie aufflammen, dieses Mal
mit blass gelblichem Feuer. "Ich tue es nicht gern, aber
ich kann im Zweifel Licht machen.", informierte sie.

Lilið nickte. "Wenn du das noch um einige Lumen heller kriegst, wenn
wir es brauchen sollten, geht das.", sagte sie mit gespielter Strenge.
"Dann müssen wir also nur noch unser Boot finden, ins Wasser schieben
und ablegen. Und bestenfalls verschwinde ich vorm Ablegen noch in
einen Busch zum Pinkeln."

"Ich setze dich gerade ganz schön unter Druck, oder?", fragte
Marusch leise. "Es tut mir leid."

Lilið streckte eine Hand aus, um Marusch über die Wange zu streicheln, einen
Moment innehaltend, bevor sie es auch tat, um Marusch die Möglichkeit
zu geben, auszuweichen. "Marusch.", sagte sie weich. "Wenn du wegmusst,
dann bin ich dabei. Glaub mir, ich habe die Gesellschaft beim Essen
auch nicht genossen. Mach dir
keine Gedanken darüber, ob es für mich schlecht sein könnte. Ich
kann einer Nacht mit dir auf See durchaus etwas abgewinnen. Ich kann auch
nicht leugnen, dass es in mir kribbelt, weil ich die Vorstellung mag,
mit dir noch einmal in ein fremdes Gebäude einzubrechen."

"Dann lasst uns die Vorstellung in die Tat umsetzen.", sagte Marusch
leise. "Nicht, dass die Gelegenheit verstreicht."

---

Die Halle war abgeschlossen. Damit hatten sie gerechnet. Sie
verabredeten, dass Marusch hier einen Moment warten und beobachten
sollte, ob hier abends generell eher niemand vorbeikam,
oder ob sie nur zufällig einen ruhigen Augenblick erwischt hatten, während
Lilið sich, wie angekündigt einen Busch suchte. Sie war ganz froh, dass
sie das Schloss nicht mit voller Blase knacken müssen würde, aber auch etwas besorgt,
Marusch allein zu lassen. Also beeilte sie sich. Als sie wiederkehrte,
meldete Marusch, dass alles still und ruhig geblieben wäre. Die Werft und
ihre Umgebung waren nicht gemütlich und in der Nacht
kein beliebter Aufenthaltsort, schien es.

"Knackst du das Schloss?", raunte Marusch ihr zu.

Lilið grinste, weil sie glatt vergessen hatte, dass sie ja gar
nicht unbedingt die Person hätte sein müssen, die das Schloss
öffnen würde. "Was bist du eigentlich für eine Diebin, die
das nicht selber kann?", flüsterte sie zurück. Trotz ihrer Worte entnahm sie
ihrer Jacke das Einbruchswerkzeug und ließ sich in eine der
Höhe des Schlosses geschuldet nicht so bequeme, halb gebückte
Haltung nieder.

"Eine, die Spuren hinterlässt.", antwortete Marusch. "Mehr als
du zumindest."

Lilið hob eine Augenbrauhe und grinste noch etwas mehr. "Ich nehme
es als Kompliment.", sagte sie leise. "Beobachtest du die Umgebung?"

"Wenn ich mich von deinem Anblick losreißen kann,
selbstverständlich.", antwortete Marusch. Den Tonfall für einen Scherz traf
sie allerdings immer noch nicht. "Und es war als Kompliment gemeint."

Marusch warf Lilið allerdings nur flüchtige Blicke zu und beobachtete ansonsten
die Umgebung, während Lilið das Werkzeug ins Schloss
einführte und Stifte setzte. Es war ein komplexes Schloss. "Ich
brauche eine Weile.", informierte sie. Es war ein Schloss, bei dem
sie auf zwei Seiten gleichzeitig Stifte setzen musste. Sie hatte
so etwas noch nie gemacht und brauchte zunächst eine
gewisse Zeitspanne, bis sie sich überhaupt eingearbeitet hatte. Ihr
machte es durchaus auch Spaß, aber sie war viel angespannter als
je zuvor, wenn sie ein Schloss geknackt hatte. Ihr war dabei
bewusst, dass sie eine ganze Weile im Prinzip gut beobachtbar damit
beschäftigt sein würde, sehr auffällig auf einem Knie vor einer Halle zu hocken
und in einem Schloss herumzustochern. Nun erstmalig wissentlich
auch noch als die meist gesuchte Diebesperson der hiesigen Monarchie.

Sie atmete langsam und ruhig dabei und erinnerte sich an die
ersten Schlösser, die sie überhaupt heimlich geknackt hatte. Damals
war sie sieben gewesen und auch sehr aufgeregt. Die Erinnerung half
ihr, sich von der Aufregung zu lösen und in die Welt einzutauchen, in
der sie nur das feine, nicht einmal hörbare Klicken des Schlosses
erfühlte, die Kanten der Werkzeuge vertraut in die Fingern drückten
und sie den Geruch der Metalle in der Nase hatte.

Wie damals sprang das Schloss irgendwann für sie eher unvorhergesehen
auf. Sie hakte es aus und betrachtete den geöffneten Bügel
in ihrer Hand. Sie genoss den Moment des Glücksgefühls, öffnete
die Tür einen Spalt, sodass sie das Schloss wieder einhängen konnte.

"Nimm das Schloss mit rein.", ermahnte Marusch leise. "Sonst können wir
eingeschlossen werden."

Das stimmte natürlich. Lilið entnahm es der Öse wieder und stand mühsam
auf. Die Haltung war nicht angenehm gewesen. In ihr Knie hatten sich
unsaft Steinchen eingedrückt, die sie daraus herausrieb, und ihre
Muskeln hatten sich bereits so verspannt, dass sie sich erst wieder an
eine aufrechte Haltung gewöhnen mussten. Niemand behauptete, Schlösser
zu knacken wäre eine gemütliche Arbeit.

Die Halle lag dunkel vor ihnen. Die Ormorane fanden sie trotzdem zügig,
weil sie dort lag, wo sie sie zurückgelassen hatten. Lilið verstaute
das Gepäck darin und schnürte es fest. Marusch half nur einen Moment
dabei und blickte sich dann um.

"Was suchst du?", flüsterte Lilið. Ihre Stimme hatte ein angenehmes
Echo, das in ihr wieder das Kribbeln auslöste, das Einbrüche für sie mit
sich brachten.

"Die Ruderanlage.", antwortete Marusch ebenso leise.

Lilið tastete mit ihren Händen durch die Schemen, die sie im Bootsrumpf
liegen sah, aber erkannte, dass Marusch recht hatte. Sie lag nicht
in der Ormorane. "Meinst du, wir haben eine Chance, sie ohne Licht
zu finden?", fragte sie.

"Wenn wir einen Werktisch finden, vielleicht.", murmelte Marusch. "Aber
vielleicht ist etwas Licht weniger auffällig, als wenn wir beim Suchen
alles umschmeißen."

Lilið nickte und gab ein leises zustimmendes Geräusch von sich. Aber
als Marusch die Hand hob, packte sie sie am Arm, weil sie am Eingang
eine Bewegung wahrnahm. Sie blickten beide auf das Tor zur Halle, das
sich langsam einen Spalt weiter öffnete und dann wieder die Stellung von
vorher einnahm. Die Person, die hereingekommen war, war leise. Lilið
hörte keine Schritte. Sie versuchte, so leise zu atmen, wie es ihr möglich
war, um nicht gehört zu werden, aber sie würden wohl früher oder
später doch entdeckt werden. Es sei denn, die neue Person könnte sehr schlecht
sehen.

Eine Weile blieb alles ruhig, in der Lilið versuchte, die Umrisse der
Person auszumachen. Es fiel ihr schwer, weil sich die Schatten, die
sie sah, nicht zu etwas zusammenfügten, was sie irgendwie erwartet
hätte. Hatte die Person sich hingesetzt und wartete jetzt einfach? Das
erschien Lilið unstimmig, also versuchte sie etwas anderes zu erkennen.

Marusch, die auf der der Tür zugewandten Seite der Ormorane
stand, führte eine Bewegung aus, durch die sich die Lampe entzündete, die
mittig in der Halle an der Decke hing. Sie schaukelte ein wenig und
hüllte sie in schwaches Licht, die Schatten ihrer Halterung dabei
auf den Boden werfend.

Lilið erschreckte sich ziemlich. Mit dieser Entscheidung hatte sie
nicht gerechnet. Und auch nicht damit, die rollstuhlfahrende Person
von der Terasse nun hier zu sehen. Lilið versuchte, ihren Atem zu
beruhigen und konnte beobachten, wie jene Person es mit der Hand
auf der Brust ebenfalls tat.

"Ich hatte gehofft, unbemerkt zu bleiben.", sagte die fremde Person, gerade so
laut, dass Lilið und Marusch sie gut hören konnten. "Und dass ihr
mich einschließt, wenn ihr geht. Würdet ihr das tun?"

"Selbstverständlich.", antwortete Marusch.

Lilið runzelte die Stirn. Was verstand sie nicht? Die Konversation
kam ihr merkwürdig vor. "Was ist, wenn uns die Person verrät?", richtete
sie sich leise an Marusch.

"Die Person. Ich bin anwesend, weißt du? Kannst du nicht einfach
'er' sagen, so als würdest du mich als Mensch wahrnehmen?", murrte
die Stimme, dieses Mal etwas lauter.

Lilið war wohl nicht leise genug gewesen. Unwohlsein hatte sie
auch dabei gehabt, als sie so von ihm gesprochen hatte, weil sie
wusste, dass behinderte Menschen oft übergangen wurden, als
wären sie keine Menschen oder als wären sie nicht dabei. "Es
tut mir leid.", sagte sie. "Ich wusste nicht, ob 'er' richtig
ist." Wieso diskutierte sie das? Machte es nicht alles noch schlimmer?

Die Person im Rollstuhl kam näher und wirkte unverkennbar verärgert. Lilið
entdeckte die Schifferklave, die an ihren Gurten an der Rücklehne des
Rollstuhls befestigt war. "Man sieht doch wohl, dass ich ein Mann
bin!", beschwerte er sich, fügte dann aber fast nachdenklich
hinzu: "Zumindest wirkst du nicht, als hättest du Probleme mit den
Augen."

"Es tut mir leid.", widerholte Lilið. "Das ist wohl ein mieser
Konflikt zwischen einer Person, der regelmäßig Menschlichkeit und
vermutlich auch Dinge im Geschlechtszusammenhang aberkannt wird, was
mir eigentlich durchaus bewusst ist, daher wünschte ich, ich
wäre damit anders umgegangen. Und einer Person, mir, die wahrscheinlich
von den meisten Menschen als weiblich wahrgenommen wird, aber nicht
weiblich ist. Also auch anderen kein Geschlecht zuschreiben möchte."

"Aber du bist doch weiblich." Die Reaktion des Mannes kam so schnell,
dass Lilið ihm verzieh, dass er widersprach, weil ihm das neue Konzept vermutlich
nicht so schnell hätte klar werden können.

"Nein.", widersprach Lilið schlicht.

Der Mann blieb vielleicht knappe zwei Meter neben Lilið stehen. Ein
Abstand, der auf Lilið so wirkte, als wolle er eine gewisse, sichere
Distanz nicht unterschreiten. Er musterte sie von oben bis unten, runzelte
die Stirn, aber nickte.

"Jedenfalls,", richtete sich Lilið wieder an Marusch, "hättest
du keine Angst, dass er uns" -- sie unterbrach sich und wandte
sich wieder dem fremden Mann zu. "Warum willst du eingeschlossen
werden."

"Endlich stellt sie sinnvolle Fragen.", mokierte er sich, aber
korrigierte: "Er. Er stellt endlich sinnvolle Fragen."

"Dass sie keine Frau ist, heißt nicht automatisch, dass 'er'
richtig wäre, oder sie ein Mann wäre.", informierte Marusch.

"Oh!", sagte der Mann und musterte Lilið erneut. "Das muss hart
sein. Es tut mir leid. Dann war meine Beschwerde wirklich nicht
angebracht. Ich hatte keine Ahnung."

"Schon gut." Lilið hatte keine Lust auf ein tiefer greifendes
Gespräch darüber, also erinnerte sie an die Frage von zuvor. "Mir
erschließt sich nicht, warum du" -- ihr fiel erst jetzt ihre
Wortwahl auf, die sich nun ungefähr doppelte -- "eingeschlossen
werden möchtest."

"Hast du mitbekommen, wie diese angetrunkene Meute mit ihm umgegangen
ist?", fragte Marusch.

Lilið hatte es nicht mitbekommen, sie war zu sehr damit beschäftigt
gewesen, sich selbst zu schützen. Aber allein der Gedanke löste
ein widerlichen Ekel in ihr aus, den sie mit einem "Wah,
Scheiße!" zum Ausdruck brachte, was sie aber als Beschreibung ziemlich
unzureichend empfand. "Es tut mir so leid!", fügte sie hinzu. "Klar
schließen wir dich ein. Oder, keine Ahnung, können wir einen Zwischenstopp
auf Portuoge einlegen, wenn ihm das helfen würde?" Mit der letzten
Frage richtete sie sich an Marusch.

"Das würde mir nicht helfen.", blockte der Mann ab. "Ich arbeite hier. Ich
habe morgen früh ab sechs wieder Dienst. Und wenn euch das hilft, habe ich
euch hier nicht gesehen." Nun traute er sich doch etwas näher heran. "Ihr
wollt doch nur euer Boot haben und verfrüht aufbrechen, oder? Weil
euch die Meute auch zu wider ist, wenn ich das vorhin richtig mitbekommen
habe."

Marusch nickte. "Wir suchen nur noch unsere Ruderanlage."

"Die sollte nicht schwer zu finden sein.", sagte der Mann und machte
sich auf eine Weise auf die Suche, die klar erkennen ließ, dass er
sich auskannte.

"Wenn du hier arbeitest, warum hast du dann keinen Schlüssel?", fiel
Lilið ein. "Warum sollen wir dich einschließen? Mit einem Vorhängeschloss
von außen?" Sie runzelte die Stirn, weil das alles für sie noch nicht
so richtig Sinn ergab. "Es tut mir leid, wenn ich zu indiskret frage." War
es nicht auch ziemlich gefährlich, auf eine Weise eingeschlossen zu
sein, auf die sich der Mann nicht selbst hinauslassen könnte? Gab es
einen Hinterausgang?

Der Mann hielt vor einem Tisch in einem Bereich, in dem keine Boote
standen. Auf der einen Seite des Tischs gab es eine niedrige Rampe auf
eine Empore, sodass er von der einen Seite gut am Tisch arbeiten
könnte, und Anni wohl von der anderen. "Ist sie das?", fragte er.

Marusch war ihm hinterhergangen und betrachtete die Ruderanlage. Auch
Lilið näherte sich. Der untere Teil war nun hellblau und schloss
an den schwarzen Teil, dort, wo die Abbruchkante gewesen war, so fließend
an, als wäre sie nur in zwei Farben lackiert worden.

Marusch nickte. "Vielen Dank.", sagte sie.

"Und zu deiner Frage:", fügte der Mann an Lilið gewandt hinzu. "Mir wird nicht
zugetraut, die Halle zu verteidigen. Zurecht wohl. Ich bin klein, körperlich
behindert, und einen hohen Skorem habe ich auch nicht. Entsprechend kriege ich
keine Schlüssel." Er seufzte. "Und auf dieser Seite der Tür bin ich sicherer
als auf der anderen."

Lilið schloss in einem Versuch, das neue Gefühl von Widerlichkeit
sinnvoll zu verarbeiten, die Augen und fluchte abermals. "Es ist
so eine beschissene Welt!"

"Eine, die ich gewohnt bin." Der Mann klang resigniert
und, als wolle er das Thema rasch abhaken. "Die Meute ist
nicht jede Nacht so schlimm. Ich hätte vielleicht damit rechnen
sollen, dass sie es heute Nacht ist, des Sturmes wegen. Anni
hatte vorhin angeboten, mich einzuschließen, aber da hatte ich
nicht geschaltet. Also, freut mich die Gelegenheit, die Bekanntschaft
mit Kriminellen zu machen, die ich tausend Mal lieber habe,
als, naja, ihr wisst."

---

Lilið fühlte sich gleichzeitig sehr erleichtert und beunruhigt, als
sie den Anhänger mit der Ormorane darauf ins Wasser zogen und das kühle
Wasser ihre Beine umspülte. Erleichtert, weil es ihr das Gefühl gab,
dass der Aufbruch klappen könnte und die stille Kühle sie ins Hier
und Jetzt zurückholte, erdete. Und beunruhigend, weil sie dabei erneut
fühlte, wie erschöpft oder möglicherweise sogar fiebrig ihr Körper war. Sie
sollte vielleicht Marusch darüber informieren.

Sie würde es bis zur Vorinsel schaffen, und wenn sie von dort noch
weiterwollten, würde sie es ansprechen, nahm sie sich vor.

Marusch hielt die Ormorane, während Lilið den Anhänger zurück
in die Halle rollte. Er
war ziemlich schwer, stellte sie fest. Ihre Muskeln brannten unter
der Last des behäbigen Gestells. Als sie es in der Halle parkte, war
der Mann gerade dabei, sich schlafen zu legen. Er war aus seinem
Rollstuhl aufgestanden, faltete ihn, um ihn neben einer Liege
einzufädeln und setzte sich auf die gepolsterte Fläche. "Habt eine
gute Reise!", wünschte er.

"Hab eine möglichst gute Nacht!", wünschte Lilið zurück.

Sie fühlte sich schon etwas unbehaglich dabei, das Schloss wieder einschnappen
zu lassen, während in der Halle ein Mensch war. Trotzdem tat sie
es. Durch ihre eigentlich zu ermatteten Muskeln schoss Feuer von
Wut auf die Gesamtsituation, und darauf, dass sie sie so hinterlassen
musste. Die Wut erinnerte sie an Maruschs Reaktion vorhin. Ein
Teil von ihr wünschte sich, sie auch durch Flammen ausdrücken
zu können. Oder durch Fluten.

Sie sprach nicht mit Marusch, als sie ohne irgendwelches Hadern
die Ruderanlage einhängte und das Groß hisste, die Müdigkeit
im Körper ignorierend. Eine angenehme Brise
wehte, mit der Lilið in der Nacht nach der Flaute kaum gerechnet
hatte. Sie brachte kühlen Atem mit sich, Geruch nach Blüten, der
sich nachts besser in der Lunge anfühlte als am Tag.

Lilið manövrierte sie umsichtig aus dem Hafen und anschließend, selbstsicher
einen Kurs auswählend, einigermaßen dicht aber nicht zu dicht
am Schmetterlingsstrudel vorbei zu einer Vorinsel auf halbem Weg
nach Portuoge, die sie sich vorhin in Gedanken bereits ausgesucht
hatte.

Jetzt hätte sie noch einmal einen Sturm gebrauchen können, dachte sie. Es
war Unfug. Natürlich hätte sie keinen gebrauchen können. Sie war
erschöpft. Aber sie war auch unsäglich wütend.

"Ich wünschte, ich hätte ein Musikinstrument.", sagte sie.

"Ich habe eine kleine Maulphonika.", antwortete Marusch. "Ich fände
schön, wenn du spielst."

Lilið blickte Marusch stirnrunzelnd an. Und das sagte sie erst jetzt? Hatte
sie die, weil sie sie selbst spielen konnte? Würde Lilið sie spielen
können?

Auf dieser Reise, die nun schon wie lange andauerte, eine Woche?, reichte
Lilið Marusch also nun erstmalig die Pinne herüber und tauschte mit
ihr Plätze, als Marusch diese annahm. Sie war etwas nervös, als sie
Maruschs Anweisungen folgend, ihr Gepäck nach der Maulphonika
durchsuchte, aber sie war nicht schwer zu finden. Sie steckte in einem
kleinen Holzkästchen, das nach nicht viel aussah, aber die Maulphonika
darin lag schwer und schön in der Hand und wirkte edel. Lilið strich mit
den Fingern über die Markierungen für die Grundtöne. Dann entdeckte sie sogar
einen Haken an der Seite, der alle Töne um einen Halbton verschieben
würde. "Eine chromatische.", sagte sie, fast ehrfürchtig.

Marusch reagierte mit einem matten, bestätigenden Geräusch.

Lilið atmete mit geschlossenen Augen zweimal tief ein und aus, bevor
sie den ersten Ton ansetzte. Er klang zunächst etwas quäkig, aber
Lilið brauchte nur einige Momente, bis sie ihn mit ihrem
Mundraum zu einem eher klagenden umformte.

Es war ein ungewohntes Instrument. Sie verspielte sich oft. Aber
es beruhigte. Sie konnte trotzdem ausdrücken, was sie fühlte. Und
als sie geendet hatte, weinte sie. Ihre Lunge fühlte sich wieder
wie ein Organ an, das ihr Freude bereiten könnte, und nicht nur
wie ein verkrampfter Knoten mit zu viel Druck darauf.

Als sie sich zu Marusch umwandte, um ihr Anweisung zu geben, den
Kurs zu korrigieren, hatte auch sie feuchte Augen. Und wirkte
endlich nicht mehr wie eine steindernde, hassende Maske. "Danke.", sagte
Marusch leise. "Für alles, was du heute für mich getan hast." Und fügte
sehr leise und sanft hinzu: "Lilið."
