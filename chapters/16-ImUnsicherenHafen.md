Im unsicheren Hafen
===================

*CN: Verbrennungen, Alkohol, Betrunkene, sexuelle Übergriffigkeit,
Ableismus, Krieg, Nacktheit in der Öffentlichkeit, Sexismus, wirbellose
Tiere.*

Lilið versuchte, sich mehrfach klarzumachen, dass sie nur eine
Rolle spielte. Marusch gab ihr Anweisungen, das Gepäck zu
tragen und ihr zu folgen oder sich bereit zu halten. Es wäre
vielleicht erträglicher gewesen, wenn Marusch es einfach schroff
wie einen Befehl hätte klingen lassen, aber sie drückte sich
dabei so wohlwollend und freundlich aus, wie eine Wache, die
sich an einem Hof sicher Beliebtheit sowohl unter Vorgesetzten
als auch unter Untergebenen erfreut hätte. Nur war sie eben
distanziert, als würden sie sich nicht privat kennen.

Das Spiel war so überzeugend, dass Lilið sich einige Male
innerlich automatisch anfing, damit auseinanderzusetzen, ob sie
Marusch vielleicht etwas getan hätte. Und dafür schämte sie sich
fast, immer, wenn es ihr auffiel, weil sie gern schauspielen
können wollte.

Kurz bevor sie die Halle verließen, nahm Marusch Lilið beiseite
und flüsterte ihr in der gleichen, distanzierten Art wie
zuvor im Schauspiel zu, dass sie, sobald sie unter Leuten
draußen wären, eine privatere Beziehung vortäuschen sollten,
damit sie nicht so sehr auffallen würden. Lilið blickte
sich um und sah Anni schmunzeln. Sie hatte es mitgehört. Lilið
begriff erst verspätet, dass das beabsichtigt war. Sie nickte.

"Gibst du mir einen der Beutel?", fragte Marusch immer noch
leise raunend.

Lilið schluckte. Wie konnte diese Person dieses doppelte Spiel
so perfekt beherrschen? Lilið fühlte im Raunen immer noch
die ungewohnte Distanz.

Aber es gab keine Zeit, es zu bewundern. Sie sollte tunlichst
auch so ein Spiel hinlegen. Sie blickte Marusch verstört an, und
veränderte den Gesichtsausdruck sofort so, als würde sie
es überspielen wollen. Nicht ganz überzeugt reichte sie Marusch das
leichteste Gepäckstück. Und mit einem Mal machte ihr die
Sache doch etwas Spaß.

---

Marusch folgte Annis Wegbeschreibung zu einem der größeren Häuser
am Hafen, zügig gehend, aber so, dass Lilið neben ihr gut mithalten
konnte. Öwenengeschrei drang vom Himmel, wo die gräulichen Drachen
kreisten. Lilið mochte ihre schwarzen Flügelspitzen. In Häfen waren Öwenen
oft aggressiv und stahlen essenden Menschen ihre Töffelstäbchen
oder Asamenspieße aus den Händen. Das Feuer der Öwenen war nicht sehr
heiß, weshalb es nur selten dabei auch zu Brandverletzungen kam.

Über das quietschige Gekreisch der Öwenen hinweg drang Musik einer
Schifferklave zu ihnen hinüber. Sie näherten sich den Tönen des
Blasebalginstruments auf ihrem Weg an den Stegen vorbei. Als sie die
Terasse vor dem Haus erreichten, das ihnen Anni beschrieben
hatte, sahen sie die Quelle der Musik: Eine
ziemlich kleine Person saß in einem Rollstuhl direkt hinter dem Zugang
zur Terasse links. Sie schien vertieft in die eigene Klavenmusik, die
zurückhaltend genug für Tischgespräche, also wohl als Hintergrundmusik
gedacht war.

Lilið folgte Marusch die flache Rampe auf die
alten Holzdielen hinauf, die mit ihren wetterverursachten
Unebenheiten sicher nicht mehr bestens für einen Rollstuhl
geeignet waren, zu einem Tisch am Rande nahe der Brüstung zum
Hafen hin. Marusch zog für sie den Stuhl zurück, tat dabei
verunsichert. Lilið räumte das ganze Gepäck griffbereit zwischen
Tisch und Brüstung und setzte sich. Es war ein sehr ulkiges Gefühl
von Marusch mit einer Höflichkeit behandelt zu werden, wie das
vielleicht von Bediensteten für den Adel vorgesehen war, während
ihr Anblick weiter von Adel weg kaum hätte sein können. Sie fühlte
deutlich, dass sie mit einer Hose, die mal dunkelgrün gewesen war,
aber von deren Farbe Wasser, Salz und Sonne nur noch marmorierte Erinnerungen
zurückgelassen hatten, in diesem Hafenhaus sehr underdressed war.

Sie setzte sich mit so viel Würde hin, wie sie vermochte. Als
die noch nasse Hose wieder gegen ihren Po drückte, fröstelte sie ein
wenig. Das war nicht gut. Frieren war für sie meist ein Alarmsignal dafür,
dass sie sehr erschöpft wäre oder krank würde. Ob sie Gelegenheit
haben würden, auszuruhen, bevor sie Hals über Kopf wieder
fliehen müssten?

An einem der benachbarten Tische saß eine Gruppe aus drei Personen
und starrte sie unverholen an. Vielleicht war das der Grund, warum
Marusch und sie kein Wort sprachen, bis eine Person in Dienstkleidung auf sie
zutrat. "Ich bin Lenste und ich bringe heute Speis und Getränk zu den
Tischen.", verriet sie. "Möchtet ihr hier nur sitzen oder auch
etwas zu euch nehmen?"

"Wir würden gern essen und trinken.", antwortete Marusch und
legte unaufgefordert den Zettel vor, den die Hafenmeisterin geschrieben
hatte.

Lenste berührte ihn beiläufig kurz. Das fand Lilið interessant. Also
schrieb Anni auch auf geeichtem Papier! "Wir haben heute im Angebot:", holte
Lenste aus, las aber dann doch von einer kleinen Tafel vor, die
an Lenstes Gürtel baumelte und überbrückte die Zeit mit
Randinformationen. "Ich bin noch nicht so lange dabei heute. Der
Sturm hat ein bisschen mehr Arbeit verursacht, es geht ein bisschen
drunter und drüber. Wir haben Ulaschsuppe, Schrimpenreis und Öffelstäbchen
mit Schnepsel."

Das hörte sich doch ziemlich brauchbar an, dachte Lilið. Öffelstäbchen
mit Schnepsel waren in Häfen ein Standardessen, weshalb Lilið sich
zwischen einem der anderen Angebote zu entscheiden versuchte.

"Ich hätte gern die Öffelstäbchen ohne das Schnepsel.", bestellte
Marusch. "Hättet ihr so etwas wie eine Kräuterbrause dazu?"

"Zart-Rohnen-Brause haben wir." Lenste wirkte grüblerisch. "Wirklich
nur Öffelstäbchen? Vielleicht ein kleiner Salat dazu?"

"Einen Salat nehme ich gern.", antwortete Marusch.

Lilið blickte Marusch überrascht an. Sie brauchte einen Moment, um
eine andere Beobachtung, die sie gemacht, aber nicht entsprechend gewertet hatte,
nun neu einzuordnen: Marusch hatte für ihren Proviant kein Fleisch
gestohlen. Lilið hatte es weniger Überzeugung zugeordnet, sondern eher vermutet,
dass es sich vielleicht einfach praktisch ergeben hätte.

Ob Marusch unangenehm wäre, wenn sie Fleisch äße? Sie wusste noch
nicht, was es damit auf sich hätte, aber beschloss, vorsichtshalber
eher zu viel Rücksicht zu nehmen als zu wenig, bis sie wüsste, wie Marusch zum Thema
allgemein fühlte. "Ich hätte gern das Gleiche.", bestellte sie also.

Vom benachbarten Tisch klang eine leise Stimme herüber, die sie vermutlich
eigentlich nicht hätten hören sollen. "Banausen." Als Lilið kurz den
Kopf zu den drei Leuten umwandte, blickten diese gezielt irgendwo
andershin.

Lilið überlegte, in Zukunft auswärts kein Fleisch mehr zu bestellen,
einfach schon, weil sie diese Verachtung verabscheute. Sollte sie
je wieder mit ihrer Familie auswärts speisen, würde sie auch mit
Diskussionen rechnen, kam ihr kurz darauf in den Sinn. Ein Grund
mehr, es zu tun, denn sie hatte Kraft, zu trotzen, und es würde
vielleicht Leuten den Weg ebnen, einfacher zu entscheiden, wie sie
entscheiden wollten.

"Auch Zart-Rohnen-Brause bei dir?", versicherte sich Lenste.

Lilið nickte. Ein Getränk aus der Kategorie trank sie eigentlich
immer, wenn sie auswärts etwas bestellte.

"Es kann ein wenig dauern.", verabschiedete sich Lenste. "Chaos,
wie gesagt."

Bevor Marusch das Papier wieder einstecken konnte, berührte auch
Lilið es mit der Hand. Sie sog die Beschaffenheit so rasch in sich
auf, wie sie konnte. Sie erkannte nichts Besonderes daran. Aber
vielleicht musste sie mehr als ein geeichtes Papier angefasst haben, um
überhaupt eine Möglichkeit zu haben, die Eichung herausfühlen zu können.

Das Chaos wurde noch um einiges unangenehmer, als die Rettungscrew, die
sie an Land geschleppt hatte, einkehrte, und sich aus vielen kleinen
Tischen eine lange Tafel in der Mitte aufbaute. Erst dachte Lilið, sie
würden sich unverschämt viel Platz verschaffen, was zu ihnen gepasst
hätte, aber nur eine knappe Viertelstunde später kehrte eine zweite
Rudergruppe ein, die sich dazugesellte. Lenste hatte viel damit zu tun,
Getränke zu verteilen, und für die Uhrzeit waren dem Geruch nach
bedenklich viele alkoholische dabei. Lilið verabscheute den
Geruch, weil sie ihn damit verknüpfte, dass übergriffige Leute
ihre Griffel noch weniger bei sich behielten als ohnehin schon. Die
ganze Begebenheit erfüllte sie mit Unwohlsein. Hätte sie keine
Rolle gespielt, hätte sie Marusch vermutlich gebeten, möglichst
direkt nach dem Essen hier zu verschwinden.

Immer wieder hoben sich kurz einzelne Stimmen aus den lauten Unterhaltungen
und der leisen Hintergrundmusik hervor. Anfangs waren es vor allem weitere Bestellungen. Eine Person,
deren Stimme nicht so dominant klang, obwohl sie laut war, bestellte
eine Zeitung. Eine Zeitung hätte Lilið auch interessant gefunden. Vielleicht
könnte sie sie am Ende mitgehen lassen. Oder auch nicht, fiel ihr ein,
weil sie hier nicht mehr als nötig auffallen sollten.

"Heutzutage hat ein Titel ja irgendwie keine Aussagekraft mehr!", verschaffte
sich eine andere Stimme Gehör. Vielleicht, Lilið war sich nicht sicher, gehörte
sie zu der schmierigen Person, die das Rettungsboot gesteuert hatte. "Ich
dachte, ich sehe nicht richtig, als da diese portuoger Batrosse, dieses
Fünfmann-Schiff, wo ja in diesen Gewässern ein Nautika an Bord Vorschrift
ist, direkt in den Engelsstrudel manövrieren wollte!"

"Nichts ist Vorschrift.", korrigierte die Stimme mit der Zeitung
halbwegs sachlich. "Es ist eine Empfehlung, und die Lords und Ladys
schreiben ihren Schutzbefohlenen meistens vor, dass dieser nachzugehen
wäre. Unabhängige Leute müssen das nicht."

"Aber wer ist denn heute noch unabhängig?", fragte eine weitere
Stimme. "Also, mal abgesehen von diesem Monarchie-Pack da oben."

"Könnt ihr mal diese Korinthenkackerei lassen?", mischte sich die schmierige
Stimme wieder ein. Inzwischen war sich Lilið sicher, die Steuerperson
an der Stimme wiedererkannt zu haben. "Jedenfalls befindet sich der Engelsstrudel zwischen Portuoge
und Espanoge seit ich ein kleines Kind bin. Ich bin ja selbst kein Nautika, aber
sowas sollte doch einfach nicht passieren! Das müssen die doch drauf haben, oder
hat ihnen das Stürmchen vorher solche Angst gemacht, dass sie nicht mehr klar
denken konnten?"

Das Stürmchen hatte Lilið durchaus zu schaffen gemacht. Sie versuchte,
sich nicht vorzustellen, wie diese Leute über sie lästern würden. Vielleicht
hatten sie sozusagen Glück im Unglück und sie täten es nicht, weil
Lilið und Marusch anwesend waren und sie sie erkannt hatten.

"Vielleicht haben sie sich auch gedacht, diesen Engelsstrudel besiegen
wir mit links!", brüllte eine weitere Stimme und lachte dann schallend. "Titel
verleiten ja heutzutage auch zu, sagen wir, sehr riskanten Vorhaben. Und zu
der Vorstellung, Naturgesetze galten für diese Titel-Fuzzis nicht." Aus
der immer noch vom Lachen bebenden Stimme glaubte Lilið, einen bösen
Sarkasmus herauszuhören.

"Spielst du auf die Kronprinzessin an?", fragte die sachlichere Stimme
mit der Zeitung.

"Das hast du gut erkannt.", antwortete die zuvor.

Mit einem Mal war es etwas ernster und ruhiger an der Tischkette. Lilið
verstand das. Die Monarchie-Familie, die derzeit über Angelsoge, Nederoge,
Espanoge und Frankeroge und alle kleineren Inseln, die mit ihnen vereinigt
waren, herrschten, bot oft Stoff, um sich über sie lustig zu machen oder
sich bitter zu beschweren, aber an sich war das Thema sehr ernst. Von den
Entscheidungen der Monarchie-Familie hing die Zukunft des Reiches ab. Wie
viel Freiräume wurde Schutzbefohlenen gewährt, welche Handelsvereinigungen
wurden geschlossen, wie wurden sie gepflegt, wie sicher waren abhängig
davon die Meere und vor allem, bekamen sie ihr Aushandeln, welche
Familie gerade über was herrschte, ohne Krieg gebacken.

Eine Übergabe der Macht des aktuell herrschenden Königspaares an
ihre Kinder stand bevor, vor der sich viele fürchteten, gerade
weil das Thema Krieg dabei ständig auf den Tisch kam.

Eigentlich wollte niemand in dieser Inselvereinigung,
dass die Königin abdankte. Oder vielleicht
nicht niemand, aber an sich war sich das Volk recht einig, dass
sie das kleinste Übel wäre. Leider hätte sie eigentlich bereits nicht mehr an der
Macht sein sollen. Die vom Bundesorakel schon vor knapp zwei
Jahrhunderten in Kraft gesetzte übergeordnete Regelung der Monoarchie-Vereinigung
sah vor, dass Regierende ab einem Alter von 50 Jahren abzudanken hätten und
die Macht an die nächste Generation übergeben werden sollte. Der König
war 54 und hatte die Macht vor vier Jahren ohne Diskussionen
oder komplexe Verhandlungen an seine Frau übergeben. Er hatte
ohnehin nie in die stereotypen Vorstellungen eines Königs gepasst. Er
war als Mann von hohem Adel eingeheiratet worden. Der entscheidende
Grund -- und das spielte in der Monarchie immer eine große Rolle -- war
sein hoher Skorem von knapp 200 gewesen, soweit Lilið sich erinnerte. Aber
eigentlich hatte er in der Regierung nur gemacht, was seine Frau gewollt
hatte, wovon das Land nicht unbedingt einen Nachteil gehabt hatte.

Die Königin war nun 52. Der Grund, weshalb sie noch regierte, war
der, dass der Kronprinz bei seiner Krönungszeremonie nicht auffindbar
gewesen war. Er war später auf einem Lustbalkon mit einer Frau bei
einem Techtelmechtel erwischt worden, mit der er
sich lieber beschäftigt hatte, als gekrönt zu
werden. Er hatte der Presse zwar erzählt, dass es ihm leid täte,
aber das kaufte ihm niemand ab. Lilið erinnerte sich an die albernen
Bilder in der Presse des halb nackten Kronprinzen mit seiner
Geliebten. Albern deshalb, weil der Kronprinz zwar keine Hose,
sehr wohl aber den Krönungsschleier noch getragen hatte, den er bei
allen öffentlichen Auftritten zu tragen hatte, bis er bei der
Krönung enthüllt würde. Anscheinend war es durchaus im Rahmen für
so einen zukünftigen Monarchen zuvor aber untenrum schon enthüllt
aufzutreten.

Die Kronprinzessin war fünf Jahre älter als der Kronprinz und nur
wegen dieses sexistischen Gesetzes nicht an seiner Stelle gekrönt
worden, weil sie eine Frau war. Entgegen der Vorschriften
lief sie allerdings seit dem Tag der gescheiterten Krönung enthüllt herum und
betitelte sich als Königin. Sie hatte beschlossen, wenn der
Kronprinz seine Krönung verpasst hatte, dann war sie dran. Niemand
sah sie in der Rolle, also brachte ihr das wenig. Ihr
Verhalten sorgte im Volk teils für Belustigung, unter
denen, die sie nicht ernst nahmen und keine Herrschaft unter
ihr als möglich annahmen, und teils für Bedenken, weil sie den
Regeln nach durchaus eine Chance hätte, an die Macht zu kommen, und
Leute das Chaos fürchteten.

"Ich kann mich echt nicht entscheiden, wer das schlimmere Übel
ist.", hörte Lilið die schmierige Steuerperson sagen, als sie
aus ihren Gedanken zurückkehrte und wieder zuhörte.

Anscheinend
hatte sie nicht viel verpasst. Wenn sie das halb noch Wahrgenommene
richtig verstanden hatte, hatten sie gerade lediglich etwas abfälliger
die Begebenheiten in der Monarchie-Familie dargelegt, als sie
es getan hätte.

"Die Kornprinzessin, ganz klar!", entschied der Zeitungsmensch.

Lilið blickte sich flüchtig um und stellte fest, dass die Zeitung
ebenso wie das Essen noch nicht da war.

"Diesen Luftikuss willst du lieber?", fragte eine helle
Stimme, aus der ziemlich viel Verachtung sprach.

Lilið tippte auf eine Person, die Erfahrung damit gemacht hatte,
in Beziehungen mies behandelt worden zu sein, oder mit Übergriffigkeiten
im Allgemeinen. Ein solcher Hintergrund sorgte häufig für stärkere
Meinungen bezüglich des Kronprinzen.

"Ist mir egal, was der Kronprinz in seinem Privatleben abzieht.", murrte
der Zeitungsmensch. "Also nicht egal, ich habe auch keinen Bock, über
Pfeselrennen durch die Felder zu lesen. Für den verursachten Schaden
müssen wir dann gerade stehen! Aber er hat einen Skorem von etwa 300. Die
Gelehrten und Bundesorakel haben daher jetzt schon eroiert, dass ihm
nicht nur die vier Großinseln, die unserer Monarchie bereits unterstehen,
wieder vermacht werden, sondern auch noch sechs weitere. Es ist noch
nicht ganz klar welche. Das heißt, egal wie beschissen wir durch ihn regiert
werden würden, bedeutet es auf jeden Fall eine Handelsvereinigung, die uns viel
Stabilität und Sicherheit gewährt."

"Ich bin ja nicht so sicher, ob sich wirklich sechs Großinseln finden,
deren Monarchie-Familien ihre Macht an uns abgeben werden.", meldete
sich die hellere Stimme von eben wieder. "Ich rechne da mit Krieg,
ehrlich gesagt. Und darauf habe ich, genauso ehrlich gesagt, echt keinen
Bock."

"Wer hat schon Bock auf Krieg!", mischte sich die schmierige Stimme wieder
ein. "Aber Krieg kriegen wir mit der Kronprinzessin erst recht! Diese
herrschsüchtige Frau hat öffentlich festgehalten, dass sie sich nicht
den Regeln nach unterordnen will. Das werden sich die anderen Monarchien
nicht gefallen lassen. Zurecht, wenn du mich fragst. Der mächtigeren und
skorscheren Monarchie steht die Verwaltung eines Landes ja nicht
ohne Grund zu. Und sie werden sich brutal einfordern, was die Frau
versucht, sich da unter den Nagel zu reißen."

"Ein bisschen verstehen kann ich das ja.", sagte eine alte, brüchige
Stimme, die Lilið bisher noch nicht gehört hat. "Sie fällt in der
königlichen Familie mit einem Skorem von vielleicht 70 sehr aus
der Reihe. Die Bundesorakel
haben für sie festgelegt, dass sie gleich alle vier Inseln an verschiedene
andere Vereinigungen vermachen muss. Und sich selbst einschließlich ihrer
Geburtsinsel Angelsoge an König Sper von Belloge untergeben muss. Dem würde ich
als Frau nicht direkt untergeordnet oder schutzbefohlen sein wollen."

Die helle Stimme gab ein angewidertes Geräusch von sich. "Ja, total
würde ich das nicht wollen!", fügte sie hinzu.

"Aber wenn sie sich auflehnt, haben wir Krieg am Hals, hundert
Pro!", sagte eine laute Stimme, von der Lilið nicht wusste, ob sie sie
schon einmal gehört hatte, weil sich verschiedene Leute laut begonnen
hatten, zuzuprosten.

Als es wieder ruhiger wurde, hörte sie wieder die Stimme der
Zeitungsperson sich räuspern, und auch ein Rascheln, das dafür sprach, dass sie
ihre Zeitung erhalten hatte. Lilið blickte sich abermals kurz um
und lächelte, nicht nur, weil sie recht hatte, sondern auch, weil
Lenste die ersten Platten mit Essen verteilte.

"Ich verstehe die Kronprinzessin in dem Punkte sehr gut. Aber im Prinzip
ist das Problem strategisch einfach in einer Art lösbar, sodass die Regierung
hinterher genau so aussieht, wie das Orakel es vorsieht, aber sie
als Person nicht König Sper untersteht.", erklärte die nun halb hinter der Zeitung
leicht abgeschirmte Stimme. "Sie müsste nur direkt nach der Krönung abdanken und
die Regierung ganz an die vorgesehenen Parteien abgeben. König Sper
kann auch ohne sie an seiner Seite regieren. Sie hätte mit ihren unsinnigen
Ideen und ihrem schlechten Verständnis von Politik ohnehin keinen
Einfluss auf seine Entscheidungen. Sie kann sich einen feinen Lebensabend
machen."

"Und das würdest du wollen?", fragte die helle Stimme von eben. "Dass
unsere Inseln einfach anderen Monarchie-Familien vermacht werden?"

Die Zeitungsperson gluckste und raschelte. "Nein, ich habe doch
eben gesagt, was ich will.", sagte sie. "Die Zukunft sieht unter
dem Kronprinzen einfach rosiger aus, wie schlecht er auch seinem
Titel Ehre machen mag."

"Es ist ohnehin zu überlegen, ob wir eine
Person mit einem Skorem deutlich unter 120 in der Regierung haben wollen. Wenn
sie wenigstens strategische Fähigkeiten zum Ausgleich hätte.", überlegte
der schmierige Mensch. "Aber ihr Plan ist einfach: Ich bin jetzt Königin
und ich regiere diese vier Inseln. Ich möchte, dass es dem Volk gut geht,
aber entscheide in jedem Falle so, dass es defintiv einen Krieg auslöst,
in dem ich dann sterbe, weil ich voll keine Verteidigungsfähigkeiten
habe."

Lilið erinnerte sich, dass die Monarchie-Familie zu einem großen Teil
in Kriegen selber mitkämpfte und viel von ihrem Erfolg dabei von
ihrem Skorem abhing, oder viel mehr von der Verteidigungs- und
Angriffsmagie, die sie schon in jungen Jahren zu lernen begonnen. Der
Skorem sagte ja nur etwas über ihr Potenzial aus, darin gut zu sein. Lilið
hatte sich damit nicht tiefgreifend auseinandergesetzt. Das Thema
Krieg war ihr zuwider, so sehr, dass sie in der Schule nie über
sich gebracht hatte, besonders aufzupassen, wenn das Thema war.

Aber so, wie diese Leute hier redeten, würde es in den nächsten Jahren
einen Krieg geben. Das hatte sie auch schon zuvor aus manchen Gesprächen
zwischen ihrem Vater und Besuchenden halbwegs rausgehört, aber noch
eher als Möglichkeit in der Ferne wahrgenommen, nicht als wahrscheinlich
für die nächsten Jahre. Es fühlte sich unwirklich an. Sie konnte es nicht
richtig fassen, was das bedeutete. Vielleicht würde es helfen, später mit Marusch
darüber zu reden. Marusch schien ein wenig Ahnung von Politik
zu haben, und auch eine differenzierte Meinung als die Leute hier.

"Am besten wäre, wenn wir einfach noch ein paar Jährchen die Königin
behalten könnten.", sagte die alte, brüchige Stimme freundlich. "Vielleicht
wird der Kronprinz in den nächsten Jahren ja doch noch erwachsen. Wenn
sie es schafft, den Zeitraum zu überbrücken, können wir darauf hoffen,
glimpflich davonzukommen."

"König Sper vor allem, aber auch andere Monarchien bauen aber allmählich
Druck auf die Königin auf.", sagte der Zeitungsmensch. "Sie sagen, dass offiziell
jetzt die Kronprinzessin gekrönt werden muss und die Verträge eingehen
muss, und die erste Kriegsdrohung ist schon zwischen den Zeilen
gefallen. Es steht bisher nur in den Kolummnen in den
Zeitungen, aber es ist nur eine Frage der Zeit."

"Aber es ist so ein Unfug!", mokierte sich die helle Stimme. "Es ist
doch richtig, dass die Regelung für Königliche, mit 50 abdanken zu
müssen, sich darauf begründet, dass der Skorem mit dem Alter sinkt und
ab einem Alter von 45 allmählich niedriger wird als der der Kinder."

"Ja, vor allem.", antwortete der Zeitungsmensch. Lilið hasste jetzt schon
den Tonfall, mit dem er seine Bildung heraushängen ließ und auf die fragende
Person stimmtechnisch herabblickte. "Im Prinzip ist es eine Anti-Kriegs-Regelung
und wir sollten dankbar sein. Je älter die Herrschaften, desto
schlechter können sie auch kämpfen. Deswegen wurde geregelt, dass
sie abzudanken hätten, bevor es durch Krieg passiert. Von der Regel
profitieren alle. Seither können auch Königliche alt werden und
sind nicht einem Tod in einem Krieg geweiht."

"Ich gönne es ihnen so sehr. Die haben ja sonst nichts." Die Ironie
aus der schmierigen Stimme war nicht zu überhören.

"Aber die Königin argumentiert doch, dass sie selbst mit 60 noch
einen höheren Skorem haben wird, als ihre Tochter!", hielt die
hohe Stimme fest. "Das sollte doch dieses Orakel-Gesetz mit
dem Abdanken ad Absurdum führen!"

"Schön wäre es, meine Liebe, schön wäre es." Der Tonfall des
Zeitungsmenschen widerte Lilið nun noch mehr an. Am Anfang
hatte sie sie vergleichbar angenehm empfunden, erinnerte sie
sich. "Das würde stimmen, wenn eben nicht jüngere Königliche aus
anderen Monarchien mit hohem Skorem einen Anspruch geltend
machen wollten. König Sper zum Beispiel ist knapp vierzig und
hat einen soliden Skorem von 180. Die Königin zögert durch
günstigen Handel, dessen Auswirkungen wir allmählich zu spüren
bekommen, eine Entscheidung hinaus, verspricht, dass es
zu einer Krönung des Kronprinzen kommen wird und stellt
ihn, so diplomatisch sie kann, irgendwie doch als
positiven Charakter dar, der gewillt wäre, auch weiterhin
günstige Handelsverträge einzugehen. Aber eigentlich klingt
durch, dass sie kein Verständnis für ihn hat und die Welt
wird ungeduldig."

Die Person mit der Zeitung fuhr noch eine Weile fort zu
lamentieren, aber inzwischen hob ein Geklapper mit Geschirr
an, andere Stimmen wurden laut und bekundeten, ob ihnen
das Essen mundete, und all das übertönte das Gespräch
allmählich. Lilið war dankbar darum. Aber ihre Töffelstäbchen
und der Salat wollten ihr nicht so recht munden. Obwohl
ihr Proviant in den letzten Tagen nicht mehr sehr frisch
gewesen war, hatte sie ihn besser genießen können, zu zweit
mit Marusch auf einer einsamen Insel, als ihr Ritual, das
sie immer entspannt taten, bevor Marusch sich ans Buch machte
und Lilið an ihre Magie-Übungen.

Die Person mit der Schifferklave fing ihr Repertoir zum
dritten Mal von vorn an. Lilið hätte im Normalfall nicht
gestört, wenn eine Person Musik erst übte oder auch ein
einzelnes Stück wiederholt spielte, aber gerade war ihr jeder
Reiz zu viel.

Als sie gegessen und getrunken hatten, ergriff Lilið einfach
die Initiative und stand auf. Marusch folgte ihr, als Lilið das
Gepäck schulterte und auf diese Weise klar wurde, dass sie nicht
bloß nur zu einer Toilette hätte gehen wollen. Sie bräuchte
bald eine, aber ihr war gerade wichtiger, wegzukommen. Die
Horde neben ihnen hatte ausgelassen zu grölen und zu feiern angefangen und
stolperte dabei auch regelmäßig über die Terasse, um Plätze
zu wechseln. Lilið drückte sich zwischen ihnen hindurch ins
Freie und es passierte dabei nicht nur einmal, dass sich ihr
ein Körper mehr näherte als nötig. Immerhin landete keine
Hand auf ihrem Po oder so etwas.

Draußen vor der Terasse standen drei der Personen der Feierbande
und bliesen Seifenblasen. Das war eine typische Beschäftigung
von Leuten, die sich kurz aus einem Gelage ziehen wollten, um
sich von den vielen Eindrücken zu erholen und sich abzukühlen. Diese
Menschen hier wirkten auf Lilið aber eher wie
Leute, deretwegen sie Entspannung bräuchte, nicht wie welche, die
selbst welche bräuchten.

"Seid ihr nicht die zwei Hübschen mit dem Sturmschaden, die wir
geangelt haben?" An der nun besonders schmierigen Stimme erkannte
sie schon wieder die Steuerperson wieder.

"Das war alles ziemlich anstrengend. Lässt du uns bitte durch?", fragte
Lilið auf eine unmissverständliche Weise, die keinen Spielraum
zuließ.

Die Steuerperson kümmerte das nicht. Sie näherte sich Marusch, die
neben Lilið stand, und streckte eine Hand nach ihrem Gesicht aus.

Lilið war auf einmal völlig egal, welches Risiko sie dabei eingehen
könnte. Sie drängte sich zwischen Marusch und die Steuerperson, hielt
ihr Gepäck vor sich in einer Hand und drückte mit der anderen gegen
die Brust der Person. "Finger weg!" Sie schrie nicht, bedrohlich
klang sie vielleicht trotzdem.

"Lass mich, ich darf das! Ich bin betrunken!", säuselte die Steuerperson,
aber dankenswerter Weise bot sie Liliðs Hand nur für einen kurzen Moment
Widerstand. Dann machte sie tatsächlich Platz.

Lilið schritt mit überzeugtem, stabilem Gang, den sie sich, müde und
erschöpft wie sie war, eigentlich kaum zutraute, durch die entstandene
Lücke und sah sich dabei lediglich um, um zu kontrollieren, dass
Marusch ihr auf den Fersen war. Sie spazierten ein Stück in die
Nacht hinein. Lilið hatte keine Ahnung, wo sie hinmussten, aber Hauptsache
erst einmal weg von diesen Leuten. Irgendwann ging sie etwas langsamer, damit
Marusch neben sie treten konnte.

Lilið hatte bewusst einen möglichst hellen Weg gewählt. Sie gingen direkt an den
Anlegestegen für die Schiffe entlang, wo die Fallen träge im nur schwachen
Wind gegen die Masten baumelten. Es sirrte nichts. Und die Öwenen waren
auch schon müde. Eine ruhte auf einer Laterne, den grauen Schwanz um selbige
geschlungen, um sich dort zum Schlafen festzuhalten.

Als Lilið in Maruschs Gesicht blickte, erschreckte sie sich fast. Es
wirkte leer, kein kleines Schmunzeln mehr, nicht einmal
ein Stirnrunzeln. Es war gefühllos oder aber voller Hass. "Kann
ich dir helfen?", fragte Lilið.

Marusch blickte sie an, aber der Ausdruck änderte sich nicht. "Lenk
mich ab!", sagte sie. "Ich hasse alles. Am liebsten würde ich diese
Welt in Schutt und Asche zerlegen. Lenk mich bitte ab, damit ich
nicht einen kläglichen Versuch hinlege, es zu tun."

Einen kurzen Moment loderten auf dem Boden um Maruschs Füße herum
kleine, blaue Flämmchen auf. Ein bisschen wie ein sengendes Meer.

"Kannst du Wasser anzünden?", fragte Lilið. Sie versuchte es mit
Belustigung. Vielleicht lenkte Albernheit ja ab. "Eine ganz schöne
Verantwortung, die du mir persönlich zumutest, aufzupassen,
dass du nicht mal eben die Welt abfackelst."

Maruschs einer Mundwinkel zuckte, aber nicht auf eine Weise, die
Lilið entspannt hätte. "Du wärest nicht verantwortlich.", flüsterte
Marusch.

War das die Wut, von der Marusch im Brief geschrieben hatte? "Hast
du einen Lieblingsweg, wie ich dich ablenken kann?" Der Schalk
hatte Lilið verlassen.

"Magst du mich küssen?", fragte Marusch. "Aber wirklich nur, wenn du
magst. Es ist eigentlich nicht okay, dass ich dich das frage, nachdem
ich dir quasi befohlen habe, mich abzulenken."

"Streich das 'quasi'.", sagte Lilið. "Aber ich mag. Ich frage mich
nur, ob das unsere Situation komplizierter machen könnte, wenn das
Nautika die Wache küsst, und wir beide vermutlich weiblich gelesen
werden."

"Egal.", sagte Marusch. "Das würde ich geklärt bekommen."

Lilið nickte, tat den Schritt an Marusch heran, der sie trennte, stellte
das Gepäck dicht neben sie ab, sodass sie es mit den Beinen noch fühlte, und
küsste Marusch. Es war ein seltsamer Kuss. Marusch wirkte am Anfang trotz
der Aufforderung nicht einverstanden damit. Und dadurch, dass die
romantischen Gefühle gerade fehlten, fühlte es sich für Lilið irgendwie an,
wie eine Schnecke zu küssen. Was sie nicht schlimm empfunden hätte, aber
eben auch höchstens für Experimente gemacht hätte. Ihr Harndrang fiel ihr
dabei unangenehm auf, aber es wäre wohl noch eine Weile aushaltbar.

Irgendwann beruhigte sich Marusch trotz der fehlenden Gefühle in Liliðs Armen. Sie atmete
ruhiger dabei, bis sie erschlafft gegen Lilið sackte und sie mit dem
Küssen aufhörten. "Es tut mir leid.", flüsterte Marusch.

"Das braucht es nicht. Mir gegenüber jedenfalls nicht.", versicherte
Lilið.

Sie verstand, dass die Situation zuvor sehr stressig gewesen war. Vielleicht
war es für Marusch das erste Mal, dass sie so viel Übergriffigkeit erlebt
hatte. Vielleicht auch nicht. Vielleicht hatten sie Marusch, als sie sich
durch die Körper gequetscht hatten, noch mehr belästigt als Lilið. Aber
Lilið musste es nicht genau wissen. Wichtig war, dass sie am besten bald
ein Zimmer zum Schlafen fanden, wo sie vorerst sicher wären.
