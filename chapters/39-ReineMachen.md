Reine machen
============

*CN: Ratten - erwähnt, Objektivizierung als
Zärtlichkeitsding, Religion mit Tradition und Kleiderordnung, vermutlich
am ehesten auf katholische Rituale anspielend.*

Lilið hatte das Gefühl, dass das Bett schaukelte, in dem
sie lag. Es war Unsinn. Die Zivil-Sakrale stand stabil
auf einem Hügel mitten in Bellim. Nichts schaukelte hier. Wahrscheinlich
würde es noch ein oder zwei weitere Nächte brauchen, bis sich
für Lilið nicht mehr alles danach anfühlte, über eine Kagutte
zu gehen.

Es war ihre erste Nacht hier und es würde vielleicht auch ihre
letzte sein. Das Reinigungsritual, über das sich Alawin und Lenja
unterhalten hatten, fand zweimal in der Woche statt, aber die
beiden Sakrals-Dienenden waren darüber unterrichtet worden, dass
es für die Dauer des Aufenthalts der Kronprinzessin nicht zur
Durchführung kommen sollte. Sie waren darüber nicht angetan und
hatten sich, wann immer sie sich unbelauscht gefühlt hatten, darüber ausgelassen. Mal
ernsthafter, wie in dem Gespräch auf Baeðisch, mal scherzender, indem
sie sich ausgemalt hatten, wie sie das Ritual einfach trotzdem
ausführen würden. Und darin sahen Drude und Lilið ihre Chance. Sie
würden herausfinden, wie es ausgeführt wurde, und auf diese Art
als Sakrals-Dienende in die Zentral-Sakrale gelangen, so
der noch nicht ausgereifte Plan. Sie würden
dabei so tun, als wären sie sauer, dass es ihnen verboten worden
wäre, und sie würden klar machen, dass sie sich das nicht gefallen
lassen würden. Wenn ihnen das abgekauft würde, -- und das erschien
ihnen nicht als Ding der Unmöglichkeit --, wären sie für einen
gewissen Zeitraum Lajana sehr nahe. Alawin und Lenja rechneten
damit, für das Doch-Ausführen des Rituals im Wesentlichen mit viel Arbeit in der
Sakrale bestraft zu werden. Diese Regel zu brechen, schien also
nicht allzu gefährlich zu sein. Es wäre lediglich so etwas wie
eine Übertreibung, zu viel des Guten, würde aber wohl Augen rollend
akzeptiert werden, hatte Drude interpretiert.

Die Redewendung 'den Hammer gleich mit an die Wand Nageln' bedeutete
genau dies: Übertreiben, etwas übers Ziel hinausschießen, zu viel
des Guten und manchmal auch einfach bloß übergründlich sein. Im
Alevischen gab es eine einzelne Vokabel für 'an die Wand
nageln', die ein Teekesselchen war. Das alevische Wort für 'Schrank'
wurde lediglich in der Schreibweise durch einen zusätzlichen Strich,
der es als Substantiv statt Verb kennzeichnete, von der Vokabel für
'an die Wand nageln' unterschieden. Daher war eine sehr schlechte
Übersetzung der Redewendung ins Baeðische: 'Den Hammer gleich mit
Schrank'. Eine auf andere Art schlechte Übersetzung, wenn das Wort
'Schrank' in ein Verb umgewandelt würde: 'Den Hammer gleich mit
beschränken'.

Drude und Lilið waren sich einig, dass sie am liebsten mehr Zeit
zur Vorbereitung gehabt hätten. Einen ganzen Tag vielleicht, viel
mehr auch nicht. Sie hätten gern etwas mehr Sicherheit gehabt, wollten
Lajana aber auch nicht zu lange warten lassen oder riskieren, dass
die Königin oder etwaige Verhandlungspersonen vor
ihnen da wären. Doch das Ritual fand am nächsten Mittag
statt. Und dann erst vier Tage später wieder. Dann konnte alles zu
spät sein, oder wäre zumindest sehr knapp. Es war besser, wenn sie die Gelegenheit jetzt ergriffen.

Das Gespräch über Schleusen hatten die beiden gemischt auf beiden
Sprachen fortgeführt. Lilið hatte also etwa die Hälfte verstanden, die
andere hatte Drude ihr im Nachhinein erklärt: Um den Eingang von der
Seeseite zu passieren, fuhren die sakralierten Boote in eine Schleuse
ein. Im Schleusenbecken konnte dann vom Boot aus ein Mechanismus in Gang gesetzt
werden, der das Schleusentor schloss und dafür sorgte, dass das Wasser
im dann entstandenen Becken sie hinauftrug, und zwar nicht einmal vor
den Eingang, sondern in eine dunkle Halle direkt in die Sakrale. In
dieser Halle war Alawin ein paar Mal zu einem vergangenen
solchen Ritual zugegen gewesen und hatte angetan davon geschwärmt.

Das Problem an der Sache war, dass das Becken im Normalfall geschlossen
war. Sonst könnte ja einfach jede beliebige Person mit einem genügend
flachen Segelboot unter den Säulen der Sakrale hindurch in das Becken
einfahren und sich hinauftransportieren lassen. Der Mechanismus, der
die Schleuse zur Einfahrt vorbereitete und öffnete, konnte im Leuchtturm
ausgelöst werden. Lilið erinnerte sich an das kleine Segelboot, das
von der Sakrale Richtung Leuchtturm gesegelt war. Vielleicht hatte es
etwas damit zu tun gehabt. Vielleicht war es dort für ein anderes
Ritual gewesen.

Wie der Mechanismus im Leuchtturm ausgelöst wurde, wussten sie noch
nicht. Ihr Plan hatte also einige Lücken. Immerhin wussten sie, dass
die Kronprinzessin in dem Raum gefangen gehalten wurde, in dem
das Reinigungsritual normalerweise durchgeführt wurde. Es war nicht
unbedingt ein gemütlicher Raum, eher ein bombastischer. Das war eine
Information, die Drude vom anderen Paar erhascht hatte, das sich über
die Prinzessin unterhalten hatte: König Sper hatte ausgehandelt, dass
sie dort gefangen gehalten würde, damit sie jederzeit bereit für
einen angemessenen Empfang von Königin Stern wären. Einen, bei dem
es ums Prahlen ging. Das Ganze hatte allerdings für sie den Vorteil, dass in
jenem Raum der Sakrale Sakrals-Dienende für ihre Rituale darauf
bestehen durften, dass alle Sakrallosen den Raum verließen, außer
der Prinzessin. So war der Pakt geschlossen worden. Und das hieß,
sie konnten, sollte Wache Schäler zugegen sein, jene Person einfach
vor die Tür verweisen. Sie hofften trotzdem, einen Moment zu
erwischen, in dem sie nicht anwesend wäre.

Lilið lag mit offenen Augen im schmalen Bett und fühlte das eigentlich nicht
vorhandene fortwährende Schaukeln, während Drude neben ihr noch
im Licht einer Kerze ein Heft las, in dem das Reinigungsritual grob
beschrieben wurde. Solche Hefte lagen am Eingang des Saals der
Verkündung aus, beschrieben aber auch allerlei andere Rituale und
waren nicht ausführlich. "Aha, es geht also dabei einfach darum, den Raum
zu putzen.", murmelte dey.

"Bestimmt auf irgendeine sakrale oder ritualisierte Weise.", vermutete
Lilið.

"Na sicher!", stimmte Drude zu. Dey las noch eine Seite, bevor dey
Lilið das wichtigste zusammenfasste. "Ich bin nicht sicher, ob ich
mich gewappnet fühle.", schloss dey.

"Ich auch nicht.", antwortete Lilið.

"Wir sollten schlafen. In der Nacht redet niemand, sodass lauschen
zwecklos ist.", meinte Drude. "Kuscheln wir?"

Warum kamen Lilið denn jetzt die Tränen? Sie hatte keinen Plan,
was das sollte. Sie stand auf und kroch zu Drude ins Bett, in
dere Arme. Sie kam sich vor wie ein Kuscheltier, und das war
kein schlechter Gedanke. "Findet Lil dich eigentlich?"

"Hier eher nicht.", antwortete Drude. "Ich denke, dey ist bei
meinen Eltern."

"Du hast mir nie über deine Eltern erzählt.", fiel Lilið auf.

"Ich habe dir über vieles nicht erzählt.", entgegnete Drude. "Ich
weiß nicht, ob ich meine Eltern wirklich mag. Ich komme mit ihnen
aus. Ich besuche sie aus Pflichtgefühl und manchmal ist es ganz
nett. Ich hatte da das Brot her, dass ich an dich verfüttert habe,
und wenn alles vorbei ist und ich den Mist überlebe, werde ich Lilið
abholen, und, hm. Dich fragen, ob du mich mitnimmst. Wo auch immer
hin."

Es erinnerte Lilið unweigerlich an Marusch. Sie hatten auch geplant,
nach der Rückgabe des Buchs miteinander zu reisen und vielleicht
eine Revolution anzuzetteln. Oder zumindest einen Weg dafür
zu ebnen, während der Weg auch einen Selbstzweck hatte. Und
dann hatten sie sich nie wieder gesehen, kurz bevor Mission
Buch abgeschlossen gewesen war.

Nun entwickelte sie mit Drude Ideen über einen Zukunftsplan, kurz
bevor Mission Lajana abgeschlossen wäre. Alles war ziemlich
heikel. Interessanterweise hatte sie keine Angst, Drude
zu verlieren, aber der Gedanke behagte ihr natürlich trotzdem
nicht.

Lilið schmiegte sich fester an Drude. "Ich würde Lajana zurück
gen Nederoge, oder besser noch Angelsoge bringen und dann Marusch
suchen.", sagte sie. "Ich würde dich gern mitnehmen. Aber du sagtest,
dass du Marusch vielleicht lieber nicht begegnen willst."

"Ich dachte erst einmal an kurzfristige Pläne. Hier wegkommen. Neu
orientieren. Wenn wir diese Marusch treffen, dann kann ich immer
noch weitersehen, ob ich mich mit euch doch wohl fühle, und bin erst
einmal zeitlich und örtlich von hier weg." Drudes Körper
entspannte sich gegen Liliðs.

"Ein wenig frage ich mich schon, ob das nicht ein bisschen des
Zufalls zu viel ist, dass wir hier direkt ein Sakrals-Dienenden-Paar
treffen, das ununterbrochen über einen perfekten Plan für
uns spricht." Erst im Nachhinein fiel Lilið auf, dass sie das
Thema einfach gnadenlos gewechselt hatte.

"Wo du recht hast." Drude seufzte. "Auf der anderen Seite ist das
sehr realistisch. Wenn eine Kronprinzessin in einer Sakrale gefangen
gehalten wird, dann reden Leute darüber. Es kommt zu Planänderungen
in Abläufen. Jene werden nicht von allen gutgeheißen. Leute beschweren sich
darüber. Wenn jetzt nur ein einzelnes Gespräch stattgefunden hätte, und
das auch noch perfekt abhörbar auf Baeðisch, wie es passiert ist,
wäre ich vielleicht skeptisch. Aber es war nicht nur ein Gespräch, sondern
mehrere. Und der größte Teil des Gesprächs, das sie auf
Baeðisch geführt haben, war eine Liebeserklärung. Sie hatten Gründe
für die Sprache, die ich gut verstehe. Und es waren auch nicht nur
diese zwei Sakrals-Dienende, die darüber geredet haben. Ich halte eine
Falle oder so etwas für sehr unwahrscheinlich."

Lilið entspannte sich etwas. Wenn Drude das sagte, dann traute
sie der Einschätzung zumindest mehr als ihrer eigenen. "Du
bist nicht reinzufällig so etwas wie ein wandelnder Lügendetektor?"

Drude schnaubte in ihren Nacken. "Ich bin so gut darin ausgebildet,
das zu sein, wie es eben geht. Das ist als Wache mein Beruf. Aber es
geht auch nur so lange gut, wie Leute ihre physischen Reaktionen
nicht völlig im Griff haben. Gegen geübte Lügende, denen ersonnene Geschichten
so lässig über die Lippen gehen wie die Wahrheit, bin ich gegebenenfalls
machtlos." Dey strich Lilið über das Haupt. "Schlaf.", bat dey. "Lass
uns morgen früh noch etwas lauschen und genauer planen. Ich brauche
gerade Schlaf."

---

Lilið fühlte sich beim Frühstück wieder recht panisch, weil sie nicht
sprachen. Natürlich sprachen sie in einer großen Halle, in der
gespeist wurde und auch sonst niemand ein Wort sprach (außer einer zum Essen
gehörigen Floskel am Anfang), ebenfalls kein Wort. Nur wie sollten
sie sich so absprechen?

Immerhin fand das Frühstück sehr früh statt, sodass sie noch ein wenig
Zeit haben würden. Hoffentlich. Im Nachhinein war jedenfalls gut,
dass Drude sie zum Schlafen gedrängt hatte, sonst hätten sie davon
wenig bekommen. Aber statt, dass sie nach dem Frühstück zurück auf ihr
Zimmer gegangen wären oder eines der Paare erneut zum Belauschen
aufgesucht hätten, führte Drude sie als nächstes in die Bibliothek.

"Das ist die Bibliothek!", informierte dey unnötigerweise mit gesenkter
Stimme. Die alevischen Worte verstand Lilið noch, alles weitere, wie
immer, kaum.

Drude führte sie langsam durch die Bibliothek und entnahm manchem Regal
dabei ein Buch. Eines war groß und wirkte zerfleddert, wie ein
Schulheft in Norm8-Format. Die anderen waren viel kleiner, vielleicht
eher ein Norm4-Format, aber Lilið war sich bei dem Seitenverhältnis
nicht ganz sicher. Als sie in einer Ecke der Bibliothek alleine waren,
schob Drude das zerfledderte, große Buch ohne vorher zu fragen einfach
unter Liliðs Sakralutte. Lilið klemmte es dort mit ihrem Körper ein,
so gut es ging. Dann schritten sie zurück, zwischen hölzernen
Bücherregalen mit den wegweisenden Informationstafeln daran Richtung
Ausgang. Eigentlich mochte Lilið Bibliotheken sehr, -- da entsprach
sie wahrscheinlich einem Mainstream. Aber heute konnte sie die Atmosphäre
nicht so sehr in sich aufsaugen.

Neben dem Ausgang gab es einen Tresen, hinter dem eine Person in einer
blassblauen Sakralutte saß. Die Kleidung machte den Eindruck, als
wären für sie eine Bluse, eine Schürze, ein Rock und eine Scherpe zu
einem vernäht und verschmolzen worden. Sie ließ Oberarme frei, war
nicht hochgeschlossen, aber machte auch keinen anzüglichen Eindruck
auf Lilið. Sie hatte nach Drudes Bericht mit Schlimmerem gerechnet. Dann
wiederum hatte sie keine Ahnung, was Menschen im Allgemeinen anziehend
fanden.

Die Person fragte Drude etwas und Lilið erkannte die alevischen
Worte für Interesse und Leuchtturm in gebeugter Form wieder.

"Geschichte.", antwortete Drude, ebenso auf Alevisch. "Ich führe Sakrals-Diener
Aurin in die Örtlichkeiten und Geschichte der Sakralen in Bellim ein." Zumindest
glaubte Lilið, dass Drude das sagte.

Die Person am Tresen nickte. "Beðem Ajad?", richtetete sie
sich an Lilið.

"Hatinan.", antwortete Lilið und hoffte, dass ihre Haltung nicht zu
steif war und preis gab, dass sie ein Buch in der Sakralutte mit sich
führte.

Nachdem die anderen Bücher in ein Heft ganz ähnlich dem, das Lilið
verborgen hielt, eingetragen waren, führte Drude sie endlich zurück
auf ihr Zimmer. Lilið atmete erleichtert aus und reichte Drude
das Buch. "Wieviel Zeit haben wir noch?"

"Bis wir an der Zentral-Sakrale sein müssen?", fragte Drude. "So,
wie ich das verstanden habe, kommt es auf den genauen Zeitpunkt nicht
an, aber in fünf Stunden sollten wir spätestens dort sein."

"Uffz!", machte Lilið. "Ist das realistisch? Dass wir bis dahin wissen,
wie?"

"Keine Ahnung.", meinte Drude. "Wollen wir uns mit Zweifeln oder mit
Planen aufhalten?"

"Planen!", entschied Lilið. "Und sollte sich das dann nicht als durchführbar
herausstellen, können wir immer noch kurzfristig einen Rückzieher machen."

"Genau!" Drude blätterte durch das Buch, das Lilið demm gereicht hatte. Das
hatte dey in der Bibliothek schon, aber nur flüchtig.

Es hatte zwischen anderen solcher Bücher in einer Art Archiv
gestanden. Lilið mutmaßte, dass jenes so etwas wie ein Aufbewahrungsort
von Logbüchern oder ähnlichem war.

Drude reichte ihr das Buch zurück. "Soweit ich das verstehe, lassen sich die Boote
mit einem Anlass und so einem Buch leihen.", informierte dey. "Das Buch wird
am Bootshaus abgegeben und der Anlass wird samt Datum eingetragen und
abgezeichnet. Das Buch hat natürlich eine Eichung."

"Und es ist voll.", fügte Lilið hinzu. Ein offensichtlicher Hinweis
auf eine Problematik, die sie mit einem Schmunzeln und gehobenen
Augenbrauen unterstreichte.

"Richtig.", bestätigte Drude. "Das Buch, das aktuell in Benutzung und noch
nicht archiviert ist, befindet sich im Empfangsraum von Sakralet Henre, wo
wir schon einmal waren. Meine Frage an dich wäre: Sollen wir dort einbrechen
und es stehlen, oder kannst du dieses Buch hier irgendwie so umfalten, dass es
mindestens noch eine leere Seite hat und die Daten auf den Seiten davor
realistisch als kürzlich vergangen durchgehen?"

"Ich sehe es mir an." Nun, Lilið konnte Alevisch nur bruchstückhaft lesen, aber
die Buchstaben waren ihr bekannt und die Zahlzeichen sogar die gleichen wie
im Baeðischen. Sie würde bei der Manipulation nur eine Seite löschen
und Datumsangaben auf vorherigen verändern müssen. Sie blätterte durch das
Buch, um eingermaßen ein Muster ableiten zu können: Wie oft
wurden Boote geliehen? Waren es verschiedene Personen, die abzeichneten? Ja, waren
es. Zeichneten sie immer um die gleichen Zeiten herum ab?

"Um in den Leuchtturm zu gelangen, braucht es wohl auch ein Zertifikat.", murrte
Drude, inzwischen auf dem Rücken auf derem Bett liegend, das Gesicht
halb unter einem Buch verborgen, in dessen Einband skizzenhaft
der Leuchtturm eingraviert war. Es roch alt. Lilið nahm den angestaubten
Geruch über die Lücke ihrer Liegen hinweg wahr, wann immer Drude
umblätterte. "Ich habe keine Ahnung, ob
das noch aktuell ist oder wie das aussehen soll. Oder ob ein Buch über die
Geschichte des Leuchtturms wirklich die beste Quelle für so ein Wissen
darstellt."

"Vielleicht gibt es einen Geheimgang.", schlug Lilið vor. Sie hatte die
Finger auf das Papier  gelegt, um sich auf eine Faltung vorzubereiten, zu
sehen, was ihr möglich sein würde.

"Am besten ein umbewachter Geheimgang. Der vom Land direkt in einen Leuchtturm
auf dem Meer führt. In dem
ein Mechanismus das Schleusen auslösen kann, um in den Hochsicherheitstrakt
der Sakrale zu gelangen." Drude klang eindeutig sarkastisch. Ernster
fügte dey hinzu: "Der Name des Traktes ist eigentlich offiziell nicht Hochsicherheitstrakt. Aber er
ist so geschützt, dass selbst dieses Buch ihn scherzhaft so bezeichnet."

"Du hast ja recht.", brummte Lilið. Natürlich war die Idee, dass
ein Geheimgang in den Leuchtturm existieren könnte,
absurd.

Sie konzentrierte sich wieder auf ihre Aufgabe. Ihre Finger fanden die eingesogene
Tinte im Papier, aber wagten noch nicht, zu falten. Sie konnte temporär
falten, wie sie es bei ihrem Nautika-Zertifikat gemacht hatte, oder permanent. Im
ersten Fall könnte sie den Originalzustand, ohne Spuren zu hinterlassen,
wieder herstellen, aber sie müsste das Buch berühren, solange es gesichtet
und abgezeichnet würde. Das käme nicht in Frage. Trotzdem versuchte sie zunächst
eine temporäre Faltung, um sich das Ergebnis anzusehen, bevor sie
es endgültig täte. Dabei stellte sie fest, dass sie das doch nicht konnte. Sie
konnte nicht zugleich so viele Stellen im Buch in einer temporären Faltung
halten. Sie würde also auf gut Glück permanent falten müssen. Sie
seufzte. "Was findest du heraus? Müssen wir noch ein Zertifikat
stehlen?"

"Ich lese noch.", antwortete Drude. "Ich sage dir später alles
Wichtige."

"Was, wenn ich Ideen hätte, die dir nicht kommen? Ist es nicht sinnvoller,
wenn du mir den Text immer mal wieder auf Baeðisch zusammenfasst?" fragte
Lilið.

Drude schüttelte den Kopf. "Das würde mich sehr bremsen. Können
wir Aufgaben aufteilen und du vertraust darauf,
dass ich dir schon alles Wichtige sagen werde?", fragte Drude.

Stimmt, dachte Lilið. Es ging schon wieder um Vertrauen. Es
fühlte sich seit gestern schon unbehaglich
an, dass Drude den Sprachvorteil hatte und ihr nicht dauernd erzählte,
was los war. Lilið fühlte sich ausgeschlossen und allmählich stieg die
Angst wieder an, dass Drude etwas gegen sie im Schilde führen könnte. Wie
unsinnig. "In Ordnung."

Drude blickte auf. "Wir müssen gut zusammenarbeiten können.", sagte
dey. "Dazu müssen wir uns auf einander verlassen. Wenn du mir
immer noch nicht vertraust, verstehe ich das, aber, hm. Wenn du
meinst, dass du das brauchst, dass ich dir mehr offenlege, dann
tue ich das, auch wenn ich dann weniger effizient bin."

"Ich vertraue dir.", antwortete Lilið fest. "Und selbst wenn nicht,
ist auf der Vermutung aufzubauen, dass du mir keine Fallen stellst,
rein pragmatisch schon die Option mit den besten Chancen."

Über Drudes Gesicht huschte ein Grinsen. "Wie weit bist du?", fragte
dey.

"Ich brauche gleich einmal meine volle Konzentration dafür und wollte
mein Gehirn vorher kurz ausruhen.", erklärte Lilið. "Ich mache mich
jetzt an die Faltung."

"Ich habe dich sehr gern, Lilið.", sagte Drude leise.

Lilið hielt sich zurück, zu fragen, ob dey sie asexuell sehr
gern hätte. Sie wusste nicht, wie Drude es bezeichnen würde, aber
sie wusste Bescheid, was Drude wollte und was dey nicht wollte. Die
Frage, ob aromantisch, hätte vielleicht mehr Sinn ergeben. Vielleicht.
Aber Lilið lächelte demm einfach an, nur einen Moment, bevor sie
die Augen schloss und sich auf das Buch konzentrierte.

Sie versuchte zunächst, Tinte von Papierpartikeln zu trennen und
aus der Seite heruszufädeln, sodass sie die Tinte abstreifen
könnte wie Staub. Aber die Tinte hatte sich so sehr mit den
Papierfasern verbunden, dass sie wie ein einheitliches
Material unter Liliðs Fingern verbunden blieben. Lilið musste
also mit der Tinte auch jedes Mal ein paar Fasern des
Papiers an die Oberfläche falten. Es
war frickelig und erforderte viel Konzentration. Und hätte sie
nur die Fasern entfernt, die wirklich notwendig gewesen wären,
hätte sie Stunden gebraucht, die sie nicht hatten. Also entfernte
sie größere Stücke aus der Seite. Leider wurde sie dabei sehr
dünn. Als sie das Ergebnis anblickte, war sie nicht sonderlich
zufrieden. Sie sah der Seite sofort an, dass sie anders als die
anderen war.

Sie fragte sich, ob sie Drude Bescheid geben sollte, dass sie
ein Buch verhunzt hatte. Vielleicht ließe es sich wiederherstellen. An das
Schriftbild erinnerte Lilið sich. Die schwarzen Krümel des abgetragenen
Papiers hatte sie auf einen Beistelltisch zusammen gefegt. Die
Idee des Widerherstellens führte schließlich zu einer des
Flickens: Sie fühlte in all die anderen Seiten hinein, wo es
Fasern an dickeren Stellen des Papiers gab, und faltete sie
aus jenen Seiten hinaus, um sie in die gelöschte Seite
zum Reparieren einzupflegen.

Sie seufzte wieder, einigermaßen zufrieden, als sie
fertig war und Drude das Ergebnis vorlegte. Es würde
unter sehr genauer Betrachtung vielleicht auffallen, aber
wahrscheinlich nur, wenn die Leute, die es in die
Hand bekämen, bereits einen Verdacht hegten. Die
Daten anzupassen war am Ende das kleinste Problem
gewesen. "Ob das Buch, das eigentlich gerade in Betrieb ist,
vielleicht gerade erst frisch angefangen ist, sodass es
auffällt, dass hier nur etwas mehr als eine Seite
frei ist?", fragte Lilið.

Drude nahm das Buch entgegen, zunächst ohne es genauer
anzusehen. "Ich würde damit rechnen, dass es
fast voll ist.", erwiderte dey. "Dieses umfasst einen
Zeitraum von etwas mehr als zwei Jahren und ist auch
fast zwei Jahre alt. Es kann natürlich sein, dass ich
mich dabei sehr verrechnet habe. Kontrollier das bitte."
Drude reichte Lilið das Buch abermals und wartete ab, bis
sie hineingesehen und den Kopf geschüttelte hatte. "Oder es
kann sein, dass das nächste Buch gerade erst voll geworden ist und noch nicht
archiviert, es also bereits ein weiteres frisches gibt. Dann
müsstest du dir irgendeine Geschichte ausdenken. Behaupte
irgendetwas, dass das alte Buch doch noch nicht voll war
oder so. Es wird dann schwierig."

"Ich? auf Alevisch?", fragte Lilið.

"Oh richtig. Wo bin ich mit meinen Gedanken." Drude strich sich
das dunkle Haar zurück, eine Geste, die dey selten tat.

"Das wäre dann eher deine Aufgabe, uns da herauszureden. Schon
wieder. Fast alles ist gezwungenermaßen deine Aufgaben.", hielt Lilið fest.

Aber Drude schüttelte den Kopf. "Derzeit denke ich, dass du das
Boot alleine leihst und mich am Leuchtturm abholst. Wenn es
dir recht ist.", sagte dey. "Du hattest überraschend doch Recht
mit dem Geheimgang."

Lilið runzelte die Stirn. "Durch einen Geheimgang zu gehen, braucht
weniger Sprachkenntnisse, oder? Sollten wir dann nicht vielleicht
tauschen?"

"Wenn du den Trick mit dem Unterwasser Atmen innerhalb der nächsten
Stunden plötzlich doch lernst, wäre ich für einen Tausch zu haben.", erwiderte
Drude.

Lilið konnte sich nicht davon abhalten, leise zu kichern. "Ein
Geheimgang, der Unterwasser steht, also."

"Die Schleuse funktioniert, indem ein Pumpsystem vom Leuchtturm aus betrieben
wird. Dazu verlaufen Rohre unter Wasser von der Sakrale zum Leuchtturm.",
erklärte Drude. "Ich habe das noch nicht ganz genau verstanden. Erst dachte
ich, ich könnte den Mechanismus vielleicht unter Wasser auslösen, und da ich
etwas Hydromagie beherrsche, ist das noch nicht ausgeschlossen. Ich müsste es
weit genug weg von Sakrale und Leuchtturm tun, damit die Magie nicht entdeckt
wird. Jedenfalls führt auch ein Gang am Rohr entlang, der mal ein Trockengang
war und erst zur Fertigstellung geflutet worden ist. Von diesem Gang aus wurde
das Rohrsystem gebaut. Und es mündet bei einer alten, verschlossenen Tür im
Keller des Leuchtturms. Die Tür wird vor Überflutungen geöffnet, weil der
Leuchtturm stabiler steht, wenn er unten voll Wasser ist. Aber normalerweise
ist sie dicht und bildet einen Zugang in einen trockenen Keller."

"Das ist wirklich interessant!" Lilið beglückwünschte Drude innerlich
für die Wahl dieses Buches. "Ich sehe zwei Probleme: Bekommst du die
Tür von außen auf? Ich meine nicht wegen des Wassers, da hast
du Hydromagie, sondern wegen Verriegelung. Und wie behältst
du eine trockene Sakralutte?"

"Die Tür konnte damals, als der Tunnel noch nicht geflutet war, von
beiden Seiten geöffnet werden, damit die Arbeitenden sich nicht
darauf verlassen mussten, eingelassen zu werden.", erklärte
Drude. "Hier steht nichts darüber, dass das inzwischen eingerostet
wäre oder so. Es kann Schwierigkeiten geben, weil sie bestimmt lange
nicht mehr von außen geöffnet worden ist, das
stimmt wohl." Drude klappte das Buch zu und tauschte
es gegen ein anderes aus. Immer noch betrachtete dey Liliðs
Werk nicht genauer, das sie Drude inzwischen wieder gereicht
hatte. "Ich hatte gehofft, du könntest mir meine
Sakralutte handlich falten, sodass ich sie in meiner wasserdichten
Tasche mitnehmen kann. Auf eine Art, dass ich die Falten dort dann ausschütteln
kann."

<!--Ist das nicht Magie, die nicht in den Leuchtturm soll? Sollte ich
das fixen? Idee wäre, dass es keine Magie braucht, es in der Form zu
halten-->

Lilið nickte. "Stimmt, das sollte das kleinste Problem sein."

"Ansonsten haben dort sicher viele Sakrals-Dienende Sakralutten gegen
Arbeitskleidung und zurück getauscht. Das wäre typisch. Es
ist nicht unwahrscheinlich, dass ich dort noch eine finde.", überlegte
Drude. "Aber die Faltvariante ist sicherer. Zumal die Sakralutten dort
beliebig alt sein können, wenn es welche gibt."

Lilið nickte. "Dann bleibt das Problem, dass ich mir ein Boot
leihen möchte, ohne ein Wort Alevisch zu sprechen."

Drude nickte, schlug das nächste Buch auf und grinste. "Und hier steht
die Lösung, gleich in den ersten Sätzen.", sagte dey. "Für dieses
Problem. Es könnte woanders Probleme erzeugen. Das Reinigungs-Ritual
wird von vorn bis hinten schweigend ausgeführt."

"Also wundert sich niemand, wenn ich ein Buch abgebe und keinen Mux
von mir gebe?", fragte Lilið. "Wobei ich Skepsis dann auch nicht
wegreden könnte."

"Genau, es wundert dann niemanden. Es gibt ein Zeichen für
Reinigung, das du machst, dann wissen die Abzeichnenden das
Anliegen.", bestätigte Drude. "Alles andere
ist ein bisschen Glück. Wichtig ist vor allem, dass wir im Boot kein
Wort sprechen. Die sakralierten Boote, mit denen die Einfahrt in
die Sakrale zulässig sind, merken, wenn gesprochen wird und der
Schleusenmechanismus, der uns hinauftransportieren würde, wird
eventuell nicht ausgelöst, wenn das Boot durch ein Brechen
des Schweigens entsakraliert ist."

"Hui.", machte Lilið. "Das sehe ich nicht unbedingt als Vorteil,
wenn es darum geht, ein Verbrechen auszuüben, das wir nicht bis ins
kleinste Detail planen können."

Drude nickte. "Handzeichen und so etwas sind in Ordnung. Wir können
versuchen, vorher welche zu vereinbaren."

"Gern.", stimmte Lilið zu. "Mir wäre vor allem gerade noch wichtig,
wie wir am Ende wieder aus der Sakrale hinausgelangen."

"Das ist einfacher, als hinein." Drude legte das Buch, dass das
Reinigungs-Ritual beschrieb, noch einmal zur Seite und begutachtete
endlich Liliðs Fälschungsarbeit, tastete mit den Fingern darüber,
nickte und reichte es Lilið zurück. "Komplex wird der Teil, in dem
wir im Raum mit Lajana und den Wachen sind und ich letzteren sagen
werde, dass sie den Raum verlassen müssen. Erst mit Zeichen, und
dann mit Worten, falls sie nicht darauf eingehen. Das bricht das Schweigegebot,
aber wir sind dann ja schon drinnen und reden mit Sakrallosen. Das
ist dann im Ritual geduldet für den Zweck. Sobald sie draußen
sind, wäre deine Aufgabe, Lajanas Fesseln zu lösen. Sie wird
gefesselt sein, das ist sicher. Und nicht mit Seil, sondern mit
Schellen, die Magie unterdrücken. Ich kenne die Dinger aus dem
Wachtdienst. Die sind etwa so schwierig zu knacken wie die
Tür zu den Sakralutten, denke ich. Das wäre deine Aufgabe. Dann
faltest du dich und Lajana zu einer Ratte oder so, ich stecke
euch in die Tasche und springe die zwei Stockwerke tief ins
Wasser. Das kann mein Körper ab, so etwas bin ich gewohnt. Das
sind alles Dinge, mit denen die Wachen nicht
rechnen werden, zumindest, solange sie uns nicht erkennen."

Lilið versuchte sich den gesamten Ablauf zu merken. Es würde
alles sehr heikel werden. "Meinst du, du könntest an der Stimme erkannt
werden?"

"Ich werde sie ein wenig verstellen." Drude atmete tief durch und
sagte dann mit ungewohntem Klang in der Stimme, sodass sie für Lilið vor allem
als Drudes erkennbar war, weil sie demm inzwischen so gut kannte: "So
etwa? Kann das funktionieren?"

Lilið nickte skeptisch. "Es könnte klappen, wenn dich die Leute weniger
gut kennen als ich."

"Hm.", machte Drude. "Es muss reichen, und es wird der größte
Risikofaktor sein, denke ich."

Lilið versuchte, sich auf ihren Atem zu konzentrieren. Es war ein so
unsicherer Plan. Aber es war gleichzeitig jetzt schon besser geplant
als jegliches Verbrechen, das sie je bisher geplant hatte. Sie versuchte, sich auf alles
einzustellen, alles zu durchdenken, während sie Drude beobachtete. Dey
las nun wieder über das Reinigungsritual in einem der Bücher. Die Sakralutte hatte sich an
deren Beinen hinaufgeschoben, sodass jene an Luft kamen. Dey
sah schön aus darin, fand Lilið. Das schwarze Haar, das glatt an einer
Seite des Gesichts herunterfiel, und die durch die Sakralutte noch mehr verbreiterten
Schultern.

"Beim letzten Plan, den ich ausgeführt habe, der so viele unsichere
Komponenten hatte wie dieser hier, habe ich Marusch verloren.", murmelte
Lilið.

"Willst du es lieber lassen?", fragte Drude. Dey blickte nicht einmal
auf.

"Nein, ich möchte es nicht lieber lassen.", antwortete Lilið fest. "Ich
wünschte mir nur, ich weiß auch nicht. Dass dir nichts passiert."

Nun sah Drude doch zu ihr hinüber. "Weißt du, ob Marusch noch lebt?"

Lilið schüttelte den Kopf.

"Dachte ich mir fast.", meinte Drude. "Und ich glaube, es ist ein ungünstiger
Moment, um darüber sauer zu sein, dass du mir noch etwas vorgemacht und nicht
aufgelöst hast: Du hast damals einen Brief an Heelem und Marusch geschrieben. Ich
hätte gern gewusst, wie wackelig das alles ist. Nun wissen wir wenigstens beide,
was wir riskieren."

"Es tut mir leid." Lilið fühlte, wie ihr heiß wurde und ebenso heiße Tränen in ihre Augen
schossen. "Ich wollte dir nichts mehr verschweigen, was wichtig für
dich sein könnte, und habe es wieder getan."

"Schon gut. So wichtig ist das auch nicht für mich.", erwiderte Drude. "Es
tut mir leid, dass du so sehr im Unsicheren sein musst. Die Angst ist
vermutlich belastend. Und es tut mir leid, dass ich direkt die nächste fiese
Frage nachschiebe: Wie sicher bist du dir über Heelem, dass er lebt?"

Lilið grinste, aber fühlte sich nicht glücklich dabei, also ließ sie es
rasch wieder bleiben. "So sicher, wie ich es mir bei dir wäre, würde ich
etwa einen Monat keinen Kontakt zu dir haben. Ich habe ihn nicht in
einer gefährlichen Lage zurückgelassen. Und wir haben den Hinweis
von der Abe. Aber Heelem ist ein Mensch, der nicht wenig risikoreich
lebt, glaube ich."

Drude nickte. "Das reicht mir. Danke."

Dey versenkte sich wieder in das Buch. Lilið beobachtete demm dabei,
wie dey noch zwei Seiten las, und fühlte sich seltsam endgültig. Dann
klappte Drude das Buch unvermittelt zu. Es machte ein dumpfes Geräusch
dabei, das Lilið mochte. "Gehen wir? Es wird Zeit."
