Vorhaben
========

*CN: Schlafmangel, Sanism, Verwendung des Wortes dumm, erwähnt: Blut, Gewalt.*

Natürlich konnte Lilið auch im zweiten für den Schlaf vorgesehenen
Zeitfenster lange nicht schlafen. Nicht tief zumindest. Zuerst fühlte sich
ihr Körper in das Kennenlernen einer neuen Person hinein. Einer,
für die sie sofort eine starke Sympathie entwickelt hatte. Sie hatte
von der Prinzessin, -- der Königin --, bisher nur aus Tratsch und aus der Zeitung
mitbekommen. Vielleicht waren auch besonders ungünstige Stellen ihrer
wenigen Reden zitiert worden. Vielleicht war sie in den Zeitungen
absichtlich unbeliebt gemacht worden. Möglich wäre das. Aber Lilið
hielt auch für möglich, dass Lajana kein gutes Auftreten vor
Leuten hatte. Oder dass Lajana vor allem auf Lilið
einen guten Eindruck machte, während
sie einen schlechten auf Leuten machen würde, die sie in die
Muster einer Politik zu sortieren versuchten, in die sie eben nicht
hineinpasste. Sie machte auf Lilið einen recht souveränen Eindruck,
aber es war eine Souveränität, die nichts mit der scheinbaren
Unangreifbarkeit anderer Regierender zu tun hatte, die niemals
über sich sagen würden, dass sie vielleicht zu etwas nicht in
der Lage wären. Lajanas Souveränität hing auch nicht mit
Fachbegriffen oder einem Verständnis irgendeiner vielleicht
sogar albernen politischen Struktur zusammen. Sie war mehr
dadurch gegeben, dass sie ein Gefühl dafür hatte, was richtig
und wichtig war, und sich darin nicht verunsichern ließ.

Lajana verstand Dinge. Lilið hatte vor dieser Begegnung vermutet,
dass diese Königin überhaupt kein Verständnis von Politik
hätte, keinen Plan haben würde, was sie da täte, und ihre Politik
dann eben von Willkür geprägt wäre, wie es die Überzeugung des
größten Teils der Bevölkerung war. Lajana hatte aber durchaus
ein Gefühl dafür, wo sie eigentlich stand, was in der Welt
lief und dass sie sich da einen Brocken vorgenommen hatte.

Lilið spürte, wie ihr die Tränen kamen, weil sie sich so sehr
wünschte, dass diese Frau den Inselkomplex, zu dem Lilið
gehörte, regieren möge. Und
weil sie wusste, wie unmöglich diese Idee wäre. Trotzdem. Es
war nur diese eine Begegnung notwendig dafür, dass Lajana
Liliðs volle Unterstützung hatte, wie aussichtslos es auch
sein mochte.

Lilið wollte außerdem eigentlich unbedingt wissen, was es mit diesem Igeldings
der Prinzessin auf sich hatte. Dem Igeldings der Königin. Warum
fiel es ihr so schwer, das Wort Prinzessin loszuwerden? Eigentlich
fand sie, dass die Bezeichnung sogar irgendwie ungünstig
verniedlichte. Lajana war sicherlich älter als Lilið, vermutlich
so um sieben Jahre.

Lilið hatte gerade ausreichend erfolgreich aufgehört, über
Verniedlichungen und über das Igeldings nachzudenken, um
schlafen zu können, als ihr in den Sinn kam, dass ein Plan
notwendig war. Wenn sie Lajana befreien wollte, dann sollte
sie sich mit dem Vorhaben Rettung dringend auseinandersetzen.
Sie hatte bereits einen weiteren Tag einfach getan, was der
Kapitän von ihr wollte. Es wurde Zeit, sich etwas Realisierbares
zu überlegen.

Wie sollte sie vorgehen? Es fiel ihr sehr schwer, sich darauf
zu fokussieren. Aber es musste sein. Also sortierte sie das,
was ihr schon klar war: Sie würde es nicht schaffen, die Kagutte
zurück nach Nederoge zu segeln und auch nicht zurück in den
Bereich, den sie mit Marusch vereinbart hatte. Das Schiff
war abgefahren. Sie hätte sie dort halten können, wären sie
jetzt noch dort gewesen, aber nun
wäre auffällig, wenn die ganze Fahrt über die Sonne von der
falschen Seite schiene. Klar, es gab auf komplexen Routen
oft auch mal Abschnitte, wo sie entgegen der eigentlich
anvisierten Himmelsrichtung fahren müssten, um eine Strömung,
die über den Tag an einer bestimmten Stelle ein Hindernis
bilden würde, zu umfahren, oder um in einen Strom zu segeln,
der sie dann mit stark beschleunigter Geschwindigkeit zwischen
Inseln hindurch antrieb, die sonst in ihrem Weg landen
würden. Aber zum einen wären solch komplexe Abschnitte
eher charakteristisch für den Bereich gewesen, den sie
mit Marusch vereinbart hatte, den sie also nun hinter sich
gelassen hatten, und zum anderen wäre es auch dort nicht
über mehr als einen Tag passiert, dass sie in die falsche
Himmelsrichtung hätten segeln müssen.

Noch waren sie vor der Grenze zum Königreich Sper, aber sie
waren auch höchstens noch fünf Tagesreisen entfernt. Es wäre
also eher eine hilfreiche Strategie, wenn sie in der Nähe
einer der letzten Inseln mit Zivilisation vor der Grenze
vorbeinavigieren würde, dicht genug dafür, dass sie irgendwie
mit Lajana von Bord kommen könnte, um dort dann ein Schiff
zurück Richtung Nederoge zu finden. Oder eine Jolle zu
stehlen, denn als blutiger Master M käme sie vielleicht
anders nicht weit. Aber vielleicht gäbe es eine Möglichkeit,
Lajana einer Reisekagutte oder -fragette nach Nederoge zu
übergeben, ohne dass Lilið mit an Bord bliebe.

Das war alles sehr unsicher. Auch Lajana, unbeliebt wie
sie war, wäre auf einer einfachen Reisekagutte oder -fregatte
sehr gefährdet. Vielleicht sogar mehr als hier. Hier war
sie in einem Hinterhalt und würde als Geisel benutzt werden,
um politische Entscheidungen zu erzwingen. Unter anderem
die Entscheidung, dass sie als Königin keine Macht
über irgendeine Insel haben könnte oder gar nicht erst Königin
werden würde. Offiziell nicht.

König Sper würde sie nicht töten, dazu hatte sie lebendig viel
zu viel Nutzen für ihn. Wenn die falschen Leute aus der
Bevölkerung des Königreichs Stern herausfinden würden, dass
Lajana mit ihnen gerade ungeschützt an Bord wäre, würde Lilið
für wahrscheinlich halten, dass deren Gewalt schlimmer
oder gar tödlich wäre.

Ob sie als Blutiger Master M ein guter Geleitschutz wäre? Auf
einer kleinen Reisekagutte würde sich die Crew vielleicht nicht
zutrauen, sie eigenhändig festzunehmen, weil sie als
Blutiger Master M einen recht beeindruckenden Ruf hätte. Wenn
sie dann einfach behauptete, sie mochten doch bitte die Prinzessin
nach Nederoge bringen und Lilið würde ihnen dafür nichts tun?

Aber selbst wenn der Plan aufginge, dass sie sie maßlos
überschätzten, wäre keinesfalls ausgeschlossen, dass sie nicht
eine Nachricht an die königliche Garde schicken würden oder
wer auch immer den Blutigen Master M so suchte, die
sie dann auf See abfangen würden, wie die Kriegskaterane es
getan hatte. Wenn die gewählte kleine Kagutte
von einer Kriegskaterane oder einem anderen Schiff der
Garde Unterstützung bekommen würden, wäre Lajana wiederum sicher.

Lilið bemerkte, dass sie mit zu vielen Unbekannten zu planen
versuchte. Sie wusste nicht, wie die königliche Garde agierte. Sie
kannte ihren Ruf als Blutiger Master M dann doch
nicht so genau. Sie konnte sich auch vorstellen, dass sie
sicherer wäre, würde sie sich als Lilið
von Lord Lurch vorstellen, als würde sie irgendein Experiment
wagen, in dem sie den Ruf eines Verbrechers nutzte.

Aber die Überlegung hatte sie trotzdem auf eine Idee gebracht:
Briefverkehr. Vielleicht würde ihr Drude die Abe leihen. Wenn
Drude es sich nicht anders überlegt hätte.

Lilið sollte herausfinden,
ob Drude bei einem Vorhaben, Lajana zu befreien, mitmachen würde. Sie
traute Drude eigentlich noch nicht restlos, aber es wäre trotzdem
gut, eine weitere Person mit im Boot zu haben und für den
Fall, dass Drude doch nicht hinter dem Vorhaben stünde, etwas
in der Hinterhand zu haben, als von vornherein allein
zu planen.

Im nächsten Augenblick verwirrte Lilið, warum sie die Idee
so erfreut hatte, die Abe mit einzubinden. Sie hätte nicht
gewusst, an wen sie schreiben sollte. An die Königin? Die
Wahrscheinlichkeit war hoch, dass die Königin täglich von viel zu
vielen haarsträubenden Gerüchten überschüttet würde, was
mit Lajana wäre. Außerdem, fiel Lilið auf, hatte
Lajana ihre Mutter nicht mit aufgezählt, als es um Leute
gegangen war, die sie unterstützten. Lilið hatte ihre Mutter
eigentlich für eine sehr durchdachte Frau gehalten, die
durchaus alles für ihre Kinder tun würde. Aber es war kein
Geheimnis, dass sie Lajana nicht für regierungsfähig hielt. Bisher
hatte Lilið das für richtig gehalten und sie konnte
sich vorstellen, dass das auch einfach nicht realistisch
war, dass Lajana regieren könnte. Dazu war die Welt nicht
reif. Sie verstand also den Standpunkt von Lajanas Mutter
durchaus. Aber sie stellte sich unter einem Elter, das
sein Kind in allen Punkten unterstützte, eher eine Person
vor, die in dem Fall wenigstens höchstpersönlich der Welt
erzählte, dass sie kacke war oder sie zumindest in
irgendeiner Weise kritisierte, weil diese Welt Lajana aus miesen,
feindlichen Gründen nicht als Königin zu akzeptieren bereit
wäre. Aus sogenannten diplomatischen Gründen tat Lajanas
Mutter natürlich nichts dergleichen. Dem eigenen Volk erzählte
eine pofessionelle Regierungsperson nicht, dass es sich
kacke verhielt.

Lilið bemerkte, dass sie gedanklich abdriftete. Wohin also,
wenn nicht an Lajanas Mutter, würde sie einen Brief schicken,
um um Hilfe zu bitten? Marusch kam ihr als erstes in den
Sinn, aber vielleicht lebte Marusch nicht mehr. Und selbst
wenn, wo würde sich Marusch aufhalten?

Sollte Lilið ihrer eigenen Mutter schreiben? Aber
was hätte diese für eine Möglichkeit, Lilið
zu helfen? Sie war Köchin am Hofe von Lord Lurch. Und ihr Vater
könnte zwar helfen, aber so sehr es Lilið sich auch nicht gern
eingestand, sie wusste doch, dass ihr Vater ihr in diesem Punkte
nicht helfen würde. Sie wusste es einfach.

Heelem.

Der Gedanke kam unvermittelt und stellte auch keine sichere Idee
dar, weil Heelem ja angeblich so schwer aufzufinden wäre, wie sie
in der Zentrale der Nautikae im nederoger Handelshafen erfahren
hatte. Aber Heelem hatte immerhin eine Familie, von der Lilið
wusste, wo diese wohnte, und die Heelem wahrscheinlich im Zweifel
einen Brief weitergeben könnte.

Heelem würde vielleicht wissen, wie er Marusch finden könnte, und
wenn nicht, hätte er vielleicht eine Idee, wie er helfen könnte. Er
war Nautika mit beachtlichem Ruf. Lilið durchströmte ein Gefühl
von Stolz und von einer Art Glückseligkeit, weil sie Teil eines
wenn auch kleinen Netzwerkes von Leuten war, die aufmüpfig vor
sich hinlebten und einen gewissen Ruf hatten. Menschen, die sie
zudem mochte.

Wenn Drude es erlaubte, würde sie Heelem schreiben und ihm die
Lage schildern. Sie würde es tun, sobald sie wüsste, auf welcher
Insel sie versuchen würde, Lajana und sich von Bord zu schmuggeln. Sie würde
hoffen, dass Heelem die richtigen Leute mobilisieren könnte, sodass sie
dann dort auf Geleitschutz treffen würden. Wenn sie die Fahrt ein
bisschen hinauszögerte und Heelem Nachrichten entweder an passende Leute
versendete oder die schnellsten Schiffe nutzte, konnten sie es schaffen, dass sie
sich dort träfen. Und vielleicht, wenn Marusch noch lebte und Heelem
eine Ahnung hätte, wie er sie auftreiben könnte, würden sie sich
dort auch wiedersehen. Aber vielleicht wäre das auch zu viel des
Guten.

---

Matrose Ott weckte sie unbarmherzig aus tiefstem Schlaf, den sie
dann doch noch irgendwie gefunden haben musste. "Ich
würde dich ja am liebsten schlafen lassen, weil du so müde bist.", raunte
er. "Aber es regnet."

Lilið versuchte ihm den Blick zuzuwenden, aber die Augen gingen nicht
auf. Die Stirn zu runzeln schaffte sie aber. "Was ist am Regen das
Problem?"

"Ich sehe nichts!", antwortete Matrose Ott. "Navigieren nur anhand
von Strömung, Kurs und Geschwindigkeit kann ich nicht gut."

Das war in der Tat schwieriger, dachte Lilið. Sie versuchte, noch einmal die Augen
zu öffnen, aber stand dann auf, ohne viel zu sehen, als es wieder
nicht so recht klappte. Sie tastete sich zum Waschbereich vor, wo
sie sich das Gesicht nässte und sich dadurch endlich nicht mehr halb in
einem Traum fühlte.

Sie hätte für dieses Vorhaben eigentlich auch gleich an Deck gehen
können. Es regnete in dichten Fäden, die vielleicht in einem 20° Winkel
zur Vertikalen auf das Deck pladderten. Der Mantel des Nautikas war
natürlich dicht. Lilið kontrollierte die Verschlüsse und hielt dabei,
möglichst nicht allzu auffällig genießend, das Gesicht dem kalten
Regen entgegen.

Leider musste sie dem Matrosen recht geben. Sie konnte keine Landmasse
sehen, etwaige Leuchttürme waren zu klein und gingen im dichten
Regen unter. Sie konnte mit keinem der Messinstrumente irgendwas anvisieren. Sie
konnte auch keinen im Morgengrauen noch sichtbaren Stern anpeilen,
nicht einmal irgendeinen Schein der Sonne in Form eines helleren Fleckens
durch das Grau hindurch.

Sie suchte also die geloggten Daten über ihre Geschwindigkeit und über
die Kompassanzeige zusammen und begab sich in den Kartenraum. Den nun
tropfenden Mantel hängte sie an einen Haken neben der Tür. Drude saß
wieder auf dem Tisch und wartete. Die Abe war mit ihrem Frühstück
beschäftigt und beachtete niemanden. Lilið fragte sich, wie sie dieses
Kunststück hinbekommen sollte, müde, wie sie war, auf eine Art zu
navigieren, die sie nicht gewohnt war, während sie eigentlich ein Redebedürfnis
mit Drude hatte.

Erst jetzt ging ihr auf, dass Drude in der nächtlichen Navigationsstunde
gar nicht da gewesen war. Lilið hatte es am Rande wahrgenommen, aber direkt
wieder vergessen, als sie die Ruhe zum Navigieren genutzt hatte.

"Du hast also mit Lajana geredet. Im best geschützten Raum auf
der ganzen Kagutte.", stellte Drude fest. "Brauchst du
wieder Stille?"

Lilið starrte demm ins Gesicht. Dey hatte davon also mitbekommen. Lilið
nickte. "Beides." Es hatte keinen Sinn zu lügen.

Sie bemerkte, wie ihre Gedanken flimmerten, als sie auf die Karte blickte
und kontrollierte, was Matrose Ott dieses Mal angerichtet hatte. Es war
nicht so, dass die Arbeit, die er investierte, vergebene Liebesmüh gewesen
wäre. Sonst hätte sie sich beschwert. Sie konnte aus seiner Veränderung
der Karte und seinen Notizen Schlüsse darüber ziehen, was in der Zwischenzeit
passiert war, auch wenn sie nie ganz vollständig waren und auch dieses Mal
wieder eine kleine Falle für sie in den Daten enthalten war. Etwas, was
er für sie offenliegend absichtlich falsch gemacht hatte, um zu
sehen, ob sie es korrigierte und ihre Arbeit gut machte.

Aber dieses Mal war sein Chaos schlimm. Matrose Ott hatte nicht untertrieben, als er
gesagt hatte, er habe größere Schwierigkeiten, im Regen zu navigieren. Lilið setzte,
nachdem sie nicht mehr durchstieg, die gesamte Karte auf ihren Stand von vor
drei Stunden zurück und versuchte sich daran, die wenigen vorhandenen Daten
so in Einklang mit der Karte zu bringen, dass es Sinn ergab. Dazu musste
sie teils in Verzweigungen denken, von denen sie nach und nach die meisten
ausschloss, die sich irgendwann später mit etwas widersprachen. Und
eigentlich war sie dafür zu müde. "Ich muss dringend mehr
schlafen.", murmelte sie. Ihr wurde schwindelig, weil sie zu atmen
vergessen hatte.

"Ich organisiere das.", versprach Drude.

Lilið blickte verwirrt auf. "Meinst du, der Kapitän gibt mir ein
weiteres Zeitfenster für Schlaf, wenn du ihn darum bittest?"

Drude nickte. "Ich bin auch müde, weil ich dich bewachen soll und
ich eigentlich eher acht bis neun Stunden Schlaf brauche.", sagte
dey. "Navigation läuft gut, meint er. Ich denke, wenn ich
sage, dass ich zwei Stunden am Tag zusätzlich schlafen möchte und
du auch ein Interesse an mehr Schlaf hättest, könnte er zustimmen,
wenn wir dabei die Koje teilen. Dann kannst du halt auch keinen
Unfug machen, weil ich dich automatisch im Schlaf bewachen
würde. Ist dir das zu intim oder so etwas?"

Lilið schüttelte den Kopf. Ihr Kopf versuchte nicht einmal in
Gedanken zu irgendwelchen Unterwürfigkeitsüberlegungen anzusetzen. "Ich
bin so müde, ich würde gerade mit einem quirligen, schnarchenden Seeaal das
Bett teilen und es würde mich nicht stören."

Drude kicherte, und das nicht einmal lautlos. Vielleicht sogar
ausgiebig. "Ich fasse das mal als Kompliment auf. Wobei Seeungeheuer
mir lieber wäre."

"Auch damit.", fügte Lilið lächelnd hinzu. "Aber wie bewältige ich
diese Navigationsunmöglichkeit, müde wie ich bin?"

"Bist du ihr dieses Mal nicht gewachsen?", fragte Drude.

"Im Wachzustand wäre ich es vielleicht.", erwiderte Lilið.

"Ich hätte zwei Ideen.", sagte Drude. "Ich mache dich wacher, oder
du erzählst mir, was du tust. Manchmal hilft es mir zumindest beim
Denken, Dinge laut auszusprechen."

"Mit was für Methoden würdest du mich wacher machen?" Lilið rieb sich
durchs Gesicht. "Eigentlich stelle ich die Frage nur aus Neugierde. Ich
würde letzteres versuchen."

"Ich habe mir auch noch nicht endgültig etwas ausgedacht.", meinte
Drude. "Ich könnte dich treten oder dir sonstige Gewalt zufügen. Ich
könnte dich nass machen oder dir Angst machen. Manchmal macht
auch der Anblick von Blut Leute wach. Es wäre nicht das erste Mal,
dass ich dich verletzt hätte und du heilst gut. Ich könnte etwas laut Klirrendes
auf den Boden schmeißen, aber das könnte Teile des Rests der Crew auf
den Plan rufen."

"Drastisch.", kommentierte Lilið. "Aber bei meinem Grad von Müdigkeit
vielleicht nicht unangebracht. Ich probiere trotzdem erstmal, dir
alles zu erklären."

"Mir ist übrigens egal, ob du es dabei darauf anlegst, dass ich dich verstehe,
oder ob ich nur so zuhöre. Das wollte ich noch hinzufügen.", informierte
Drude und drückte den Rücken durch, wodurch dere Schultern nun der angezogenens
Knie überragte.

Lilið fing an, zu erklären, was sie tat, und es half tatsächlich. Es
ergab sich eine Variante zwischen denen, die Drude vorgeschlagen hatte:
Drude verstand ein paar Dinge, und wenn das gerade der Fall war, legte
Lilið es drauf an, Information für demm anzuknüpfen, aber ansonsten
ließ sie sich nicht davon aufhalten, wenn Drude nicht mitkam. Dieses Mal
ergab alles einen Sinn. Aber sie musste sich eingestehen, dass sie ohne die
viele Erfahrung, die sie auf der Reise mit Marusch und bei ihrer
Vorbereitung darauf mit Heelem gesammelt hatte, hiermit
überfordert gewesen wäre. Das erschreckte und erfreute sie gleichzeitig.

"Und nun planen wir eine Flucht?", fragte Drude.

"Ich..." Lilið blickte demm wieder überrascht an. Diese Person schaffte
sie ein bisschen. "Also, ja, ich hätte dich da gern mit an Bord. Wenn
du das so vorschlägst, also gern!" Ein kleiner Teil von ihr warnte sie
vor, dass es eine Falle sein könnte. Aber sie würde sich einen Alternativplan
ausdenken und versuchen, wachsam zu sein. Es konnte trotzdem
nicht schaden, nachzubohren. "Kannst du mir glaubhaft darlegen,
dass ich dich wirklich überzeugt habe?" Es war
schließlich nur dieser eine Kommentar nach Liliðs emotionaler Rede
gewesen, dass dey glaubte, Lilið hätte demm, der Lilið hätte überzeugen
können.

Drude zuckte mit den Schultern. "Ich bin nicht gut darin, Leute zu
überzeugen.", antwortete dey.

"Außer mit Gewalt.", korrigierte Lilið und grinste.

Drude runzelte die Stirn. "Ich dachte immer, mit Gewalt kann ich
Leute zu etwas zwingen, von dem sie dann aber meistens trotzdem
nicht überzeugt sind."

"Hm, stimmt.", murmelte Lilið. "Ich hätte trotzdem gern irgendwie
etwas mehr Information oder Gefühl von dir dazu, was dich jetzt überzeugt
hat."

Drude umschlang die Knie mit den Armen. "Ich mag Lajana.", sagte
dey leise. Plötzlich sprang die Abe zu demm auf den Tisch und
schmiegte sich an, als wüsste sie, dass Drude Trost bräuchte. "Ich
bin groß geworden mit so drastischen Sprüchen wie: Hört auf, Dumme
berühmt zu machen. Ich erinnere mich gerade an diesen Spruch besonders, weil
er in mir immer gebohrt hat, obwohl ich eigentlich von ihm überzeugt
war. Kurzgefasst habe ich ihn so verstanden: Es tut zwar weh, dass
manche Leute von höheren Positionen ausgeschlossen werden, aber es
ist notwendig, damit der Laden funktioniert."

Drude strich über die schwarzen Schedern unterhalb des Köpfchens
der Abe, die dabei die Augen schloss und sich zustimmend gegen
dere Finger drückte. Drudes Augenfarbe war fast schwarz, fiel
Lilið auf. Drudes Mimik war unbewegt, aber irgendwie machte das
Bild, wie dey deren eigenen Fingern beim Streicheln zusah, einen
verletzlichen Eindruck auf Lilið.

"Ich habe mich sehr angestrengt, meine naturwissenschaftlichen
und darunter vor allem meine magischen Fähigkeiten
auszubauen und das wurde mir aus verschiedenen Gründen nicht
leicht gemacht. Geschlecht und meine Unfähigkeit mit Zahlen sind
nur zwei der Gründe.", fuhr dey fort. "Ich wurde, wie ich mal sagte, auch
oft für dumm gehalten. Ich habe erst in den letzten Jahren
gemerkt, dass ich mir als Verteidigungsmechanismus angeeignet habe, mein Können
ständig unter Beweis zu stellen, zu zeigen, dass ich besser bin
als möglichst alle anderen, dass ich zu einer Elite gehöre. Dadurch
bin ich überhaupt hier gelandet. Ich habe trotz meiner Schwierigkeit
mit Zahlen fast die Freiheitsprivilegien eines Nautikas. Ich
werde als Wache und zu allerlei weiteren Aufgaben angeheuert, weil
ich Arbeiten unter Wasser verrichten kann, unbemerkt an Land
oder wo anders an Bord gelangen kann und im Zweifel Schiffe
versenken. Und ich habe ein Händchen für Briefdrachen."

Bei den letzten Worten hob die Abe ihren Kopf und blickte
Drude aufmerksam an. Aber Drude machte eine ablehnende
oder beschwichtigende Geste, auf die die Abe
ihren Körper wieder ablegte und ihren Schwanz um Drudes
Fußgelenk wickelte.

"Ich bekomme manchmal mehrere Angebote parallel und kann mir das
beste aussuchen.", setzte Drude wieder an. "Das ist ein gutes Gefühl. Und
ich dachte, ich hätte es mir verdient. Und nun bin ich verwirrt,
ob ich das wirklich habe. Ich habe hart dafür gearbeitet, aber
wäre ein System nicht schöner, in dem Leute mich einfach so mit
Respekt behandelt hätten, ohne dass ich mir das erst hätte erarbeiten
müssen? Und was ist mit Leuten, die das nicht erreichen
können? Ich dachte lange, Leute, die das nicht erreichen können,
müssten auch rücksichtslos sein, weil Rücksichtnahme eine
schwierige Sache ist, die sie dann auch nicht
können. Ich habe außerdem geringe Intelligenz
oder geringen Skorem mit irgendwelchen Bargesprächen verbunden,
die mich immer angewidert haben, wo ständig mit sehr einfachen
Worten über Leute hergezogen wird, die nicht in Muster
passen." Drude holte tief Atem und strich sich durchs
Haar. "Und dann habe ich Lajana kennengelernt. Natürlich habe
ich mit ihr geredet. Ich war mitverantwortlich für die
Entführung und wollte, dass es ihr möglichst gut geht, selbst
wenn sie lästermäulig wäre, was ich eigentlich erwartet
hätte. Aber stattdessen war mein Eindruck,
dass die Welt mit ihr in diesem Raum, wo ich, bevor du an
Bord warst, auch häufiger Zeit mit ihr verbracht habe, viel mehr Sinn ergab
als die Welt da draußen, in die sie nicht passt. In die ich
auch nie gepasst habe. Mir wurde bewusst, dass Lästern
und Abfälligkeit nicht weniger wird, wenn Leute intelligenter
sind, sondern nur anders. Perfider. Ich denke, ich
musste erst rausfinden, was an meinem Weltbild nicht passt. Es
war schon längst am Wanken, bevor wir uns trafen. Aber dein
Puzzle-Stück hat gefehlt. Ergibt das irgendwie Sinn für
dich?"

Lilið nickte. Sie weinte schon wieder fast, stellte sie
fest. "Danke.", sagte sie. "Das war sehr persönlich. Mir
wird es jetzt leichter fallen, mit dir zu planen."

"Hast du schon einen Plan?", fragte Drude. "Falls er
übrigens Lil involviert, sollten wir uns beeilen. Dey
wird heute Abend mit Nachricht an König Sper ausgestattet
<!--Sollte hier demm stehen?-->
abgeschickt. Ich kann ihr auf dem Weg noch einen zweiten
Brief mitgeben. Das fällt dann verhältnismäßig wenig auf."

Lilið erschreckte sich, als die Abe sich von Drude losmachte, Feuer
spuckte und fauchte. War das Wut?

Drude legte der Abe einen Finger unter den Kopf, strich
ein weiteres mal über die weichen Schedern unter dem
Kinn der Abe. "Wir haben schon darüber gesprochen.", teilte
Drude mit. Lilið war sich nicht sicher, wem. "Ich hatte
vermutet, wenn du Lil einen Brief mitgeben möchtest, dass
er dann eher in Richtung Nederoge geht. Das hieße, wir sehen
uns mindestens vier Tage lang nicht. So lange sind wir
selten getrennt."

Die Abe umklammerte Drudes Arm und schloss die Augen. Es
wirkte, als würden sie dadurch weiß werden, weil das
die Farbe der Augenlider war.

Lilið mochte das Bild. "Aber ihr würdet es für mich
tun?", fragte sie sachte.

Drude nickte. "Nicht wahr?", fragte dey leise.

Die Abe kroch deren Arm hinauf und blickte von Drudes Schulter
aus Lilið entgegen. Lilið hatte keine Ahnung, wie sie das
interpretieren sollte, aber über Drudes Gesicht huschte ein
Lächeln.

"Also hast du vor, einen Brief zu schreiben?", bohrte dey nach.

Lilið nickte. Und dann breitete sie den Plan aus, den sie sich
in der Nacht überlegt hatte.

---

Ja, wenn die Abe am Abend abfliegen würde, dann war das wenig
Zeit, um eine Route unauffällig umzuplanen und außerdem einen
Brief an Heelem zu schreiben. Einen kodierten am besten. Aber
Drude schaffte es beim Frühstück tatsächlich, beim Kapitän
zwei Stunden Mittagsschlaf für sie auszuhandeln. Lilið gestand
sich ein, dass der Plan sicherer werden würde, wenn sie diese
nutzte.

Die Kiste, auf der sie schliefen, war so schmal, dass sie
sich zwangsläufig berüheren mussten. Drude roch herb, fand
Lilið. Sie fand den Geruch weder gut noch schlecht,
aber trotzdem half er ihr beim Einschlafen.

Wie als hätte irgendjemand doch einen Verdacht entwickelt, lauerte ihr
Matrose Ott nach dem Schlaf auf und kontrollierte alles, was
sie tat. Sie würde die Route wohl unauffällig unter
seiner Nase planen müssen. Vielleicht wäre das ohnehin gut,
weil es dann für ihn nicht aus dem Nichts käme, sondern sie
Gelegenheit hätte, einen erfundenen Grund anzugeben, dass die
Route auf einmal anders verliefe.

Die Umplanung war am Ende sogar weniger umfangreich, als Lilið
befürchtet hatte. Sie hatte Glück, dass die Insel Lettloge
dicht genug in die Nähe ihrer Route rücken würde, wenn
sie sie nur kurz zuvor um einen halben Tag verzögern würde. Und
diese Verzögerung musste sie nicht einmal jetzt schon begründen.

"Ich habe heute gesehen, wie leicht uns etwas dazwischen kommen
kann, wenn wir wegen Regen mit der Navigation ungenauer
werden müssen.", erklärte sie dem Matrosen. "Ich plane diese
mögliche Alternativen für den Fall ein, dass es eng
wird."

---

Leider konnte sie beim besten Willen keinen Brief
schreiben, während sie überwacht wurde. Sie fühlte Panik in
sich aufsteigen, als es später und später wurde. Erst kurz nach
dem Abendessen war sie endlich wieder mit Drude allein.

"Haben wir noch die Zeit?", fragte sie leise.

"Kaum.", antwortete Drude. "Deshalb habe ich einen Brief
vorgeschrieben, in den wir nur noch Ort und Zeit eintragen
müssen. Du kannst noch drei Zeilen dazuschreiben. Und du musst
Lil erklären, wo dey hinmuss. Dey wird dich verstehen. Es muss
alles schnell gehen."

"Hast du kodiert?", fragte Lilið.

Drude schüttelte den Kopf. "Ich habe länger überlegt, wie, aber
Lil ist auch noch nie abgefangen worden. Das wüsste ich. Leute
unterschätzen, was sie mir mitteilen kann.", erklärte Drude. "Ich
hoffe nur, dein Kontakt belässt auch wirklich den Brief an König
Sper bei Lil. Das würde uns sonst wahrscheinlich zu früh in große
Schwierigkeiten bringen. Wir erwarten eine Antwort von ihm. Wenn
Lil nicht oder leer bei ihm ankäme, wäre das schlecht. Ich habe es
deinem Kontakt geschrieben. Den Namen musst du auch noch eintragen."

Lilið schluckte. So gut kannte sie Heelem eigentlich auch nicht. Aber
das verheimlichte sie lieber. Sie hatte vor Drude außerdem auch nur
abstrakt vom Kontakt gesprochen. Sie fragte sich, ob sie nun an
Heelem schreiben sollte oder an Marusch und entschied sich schließlich
für beide Namen. Anschließend drängte Drude sie, Lil die
Adresse zu erklären. Das fühlte sich merkwürdig an, weil es Treffpunkt
für Kriminelle war. Aber das wusste Drude ja nicht.

Auf einem kleinen Zusatzzettel ergänzte sie für Heelem,
dass Marusch eventuell etwas passiert sein könnte. Zu mehr kam sie
nicht, weil Drude drängte. Dey nahm Lilið das Papier aus der Hand, stopfte
es in dere Hosentasche und nur Momente später klopfte der Kapitän, um
Drude mit der Abe zu sich zu rufen.

Lilið fühlte sich seltsam hohl. Es war gut, einen Plan geschmiedet
zu haben, aber nun musste er auch klappen. Und was, wenn nicht?
