Plan A
======

*CN: Über Sex und BDSM nachdenken, über Dominition/Submission, Reden
über gegessen werden, Verletzung
bzw Knochenbruch erwähnt. Gore und Ekeldinge werden besprochen.*

Sie sprachen noch eine Weile, -- so lange, wie sie hatten --,
über ihre Gefühle. Das war gut. Liliðs Angst, dass sie vielleicht
durch das Abgeben jeglicher Kontrolle Lajana geschadet oder
sie nicht genügend berücksichtigt haben könnte, ließ dadurch
nach.

Lajana fühlte, wenn eine Person sich für sie fallen ließ. So
würde Lilið ihre Ausführungen in eigene Worte fassen. Lajana
beschrieb außerdem, dass sie dieses hilflose Straucheln
mochte, wenn Menschen aufgeben mussten, Kontrolle über sich
zu behalten. Sie mochte, selbst Ursache davon zu sein, und
das hatte über die meiste Zeit für sie auch ausreichend
geklappt. Drude fragte, ob dey das mit dem Beißen hätte
lassen sollen, weil dadurch Liliðs völliges Eintauchen in dieses
andere Denkuniversum durch demm ausgelöst worden war. Und
Lajana stimmte zu, dass es für sie in dem Moment weniger um
sie gegangen war, aber fand das in Ordnung. Sie meinte, sie
habe Lilið in dem Moment gern geteilt, weil es sehr hübsch
gewesen wäre. Und sie hatte vorgeschlagen, so etwas zu
wiederholen, nur dass sie Lilið dann beißen würde oder
Drude es erst täte, wenn sie das sagte. Aber sie einigten
sich darauf, das nicht gleich in der nächsten Nacht zu
probieren, sondern sich Zeit zu lassen, sich in das Erlebte
hineinzufühlen.

Lilið hatte ein schlechtes Gewissen, als sie sich darauf einigten,
weil der benannte Grund für das Abwarten für Drude und sie nur die halbe Wahrheit war: Es
gab eine Flucht zu planen. Sie hatten damit eine Wartezeit
möglichst schön ausgestaltet. Aber spätestens ab der folgenden
Nacht sollten sie sehr wachsam und vorbereitet sein. Davon
hatten sie Lajana nichts erzählt. Drude und Lilið waren sich
einig, dass sie Lajana gern davon erzählt hätten, aber es auf
Lajanas Wunsch hin, damit sie nicht ausversehen in einem Gespräch
mit der Wache wertvolle Hinweise an sie weiterreichen könnte,
erst so spät wie möglich und nur so ausführlich wie nötig
tun würden.

Es gab noch einen weiteren Grund dafür, dass sie nicht gleich in der
nächsten Nacht mit Drude oder Lajana sexuelle Dinge ausprobieren
wollte: Interessanterweise war es so, dass Lilið erst einmal
genug von Sex hatte. Es war ein angenehmes Gefühl, es getan zu
haben und nun keine so starke Lust dazu mehr zu spüren. Sie fragte
sich, ob ihre Lust deshalb so groß gewesen war, weil
sexuelle Gefühle und solche im Zusammenhang mit einem
Fetisch sehr stark sein konnten, so stark, dass diese Gefühle dazu
in der Lage gewesen waren, ihren Kopf ganz von Belastendem freizufegen.

---

Als sie nach ihrer nächtlichen Navigationsstunde wieder in ihre
Koje schlüpfte, formulierte sie, ohne eine Absicht, ihn je
zu schreiben, sondern eher für sich, einen Brief an
Marusch.

> Marusch!
> 
> Ich fühle mich in Bezug auf dich merkwürdig. Ich denke, ich müsste
> dich eigentlich vermissen. Irgendetwas Bohrendes in mir fragt sich,
> wo du jetzt bist. Oder ob du noch bist. Ich versuche, nicht daran
> zu denken, denn was bringt das schon. Und heute Nacht habe ich
> diese bohrenden Fragen nicht einmal ausversehen gespürt. Ich habe
> zwischendurch gar nicht an dich gedacht. Obwohl ich Sex hatte und
> ich Sex mit dir kennen gelernt habe, und das sehr schön war.
> 
> Hallo Marusch, wir hatten schöne sexuelle Erfahrungen miteinander,
> aber ich habe nun etwas kennen gelernt, was einfach noch viel besser
> für mich funktioniert hat als alles mit dir. Kann ich mir vorstellen
> oder würde ich mir wünschen, so etwas mit dir zu tun? Ich glaube,
> interessanterweise nicht.
> 
> Ich habe herausgefunden, dass mein Körper darauf reagiert, dass
> ich mich unterwerfe. Und ich glaube, du bist eher ein Mensch, den
> ich unterwerfen würde.

Lilið stutzte an der Stelle im imaginären Brief über sich selber. Der
Brief war ohnehin schon albern angelegt gewesen, stellte scheinbare Unsinnigkeiten
oder Widersprüchlichkeiten dar, die vielleicht gar keine waren, sondern
sich nur so anfühlten. Außerdem bestand er aus Gefühlen, die Lilið
vielleicht hatte, aber von denen tabuisiert war, sie einer geliebten Person
ins Gesicht zu sagen. Marusch war allerdings ein Typ Person, bei der
Lilið es vielleicht doch tun würde. Mochte Marusch Konfrontation? Mochte
Lilið diese Art Konfrontation mit einem alberigen Gefühl, dass darin hinein
gewebt war?

Aber gestutzt hatte sie, weil ihr Körper auch auf den Gedanken reagiert
hatte, Marusch zu unterwerfen. Sehr anders als darauf, unterworfen zu
werden.

Als Drude sie festgehalten und gebissen hatte, war in ihrem Kopf kein Raum
mehr für irgendeine Art von Klarheit gewesen, die sie hätte steuern können. Es
war ein von dieser Welt komplett losgelöstes Universum gewesen. Es war auch
ein losgelöstes Universum in ihrem Kopf, wenn sie etwas erfühlte, bevor
sie es faltete, oder beim Navigieren, wenn ihr Gehirn komplexe Strukturen
mehr in diesen wortlosen Netzwerken erschloss, was viel effizienter war, als
in Sprache zu denken. Aber als Drude sie gebissen hatte, war es ein
emotional überwältigender Sog gewesen, es hatte positive Dinge mit
ihrem Selbstwert getan und an dem Gefüge, das sie war, gerissen wie
Musik. Und das gekoppelt mit starker Erregung auf physischer und emotionaler
Ebene. Lilið konnte sich nicht vorstellen, dass sie ohne den Aspekt, ausgeliefert
zu werden, in dieses Gedankenuniversum gelangen könnte.

Der Gedanke, Marusch zu unterwerfen, war für sie eher sadistisch und
verkopft. Sie mochte den Gedanken, ein Spiel gegen sie zu spielen, das sie
gewinnen würde. Sie mochte es, auf alberne Weise und auf Grenzen achtend
zu ärgern. Sie mochte die Geräusche auslösen, die aus der komplexen
Kreuzung aus Verzweiflung und Genuss hervorgingen. Und sie wusste, dass
Marusch es mochte, wenn sie es tat. Darauf hätte sie schon irgendwann wieder
Lust. Aber es würde sie nie in so einen Abgrund stürzen, in dem sie
durch Lajana und Drude gelandet war. Und interessanterweise war das
ein Abgrund, der sich nicht passend für eine Interaktion mit Marusch
für sie anfühlte. Nicht, weil sie nicht genug vertraute. Sondern vielleicht,
weil Maruschs Dominanz, wenn überhaupt vorhanden, dazu nicht passte.

Während sie in einer ihrer ersten Nächte hier aus einem Traum
aufgewacht war, in dem Marusch sie leidenschaftlich küsste, schlief
sie nun mit dem Gedanken ein, an sie gekuschelt zu sein und ihr
von ihren neuen Erfahrungen zu erzählen. Marusch würde gut zuhören
können und vielleicht interessante, eigene Erfahrungen teilen, durch
die sich Lilið mit dem Ganzen nicht so alleine fühlte. Denn auch,
wenn es positiv war, es fühlte sich nach zu viel an, um es allein zu
tragen. Sie wollte es teilen.

Sie vermisste Marusch doch. Und weinte nun das erste Mal deswegen, bis
sie endlich schlief.

---

Der nächste Tag war glücklicherweise nicht besonders warm. Sonst
wäre vielleicht aufgefallen, dass Lilið den Kragen hochschlug, um die
Bissspuren zu verbergen. Daran hätten sie vielleicht denken sollen. Das
hatten sie beide nicht. Lilið bereute es trotzdem nicht. Jedes Mal,
wenn sie daran dachte, fühlte sie ein Gefühl wie den Nachhall von
Streicheln oder Atem, aber unter der Haut und kaum
darauf, am Hals, und den Wunsch, dass Drude es irgendwann
wieder täte. Aber sie einigten sich darauf, auch damit
zu warten. In der folgenden Nacht wollten sie sich mit Lajana überlegen,
wie sie die Kagutte verlassen wollten, damit sie es in der
darauffolgenden tun könnten.

Plan A war an sich einfach: Sobald die Kagutte in der übernächsten Nacht
nah genug bei der Insel Lettloge wäre, was nach ihrem Plan
in ihrer ersten nächtlichen Schlafeinheit passieren würde, würden
Drude, Lilið und Lajana irgendwie von Bord
gelangen und sich auf jene Insel durchschlagen. Da Drude sie dorthin
schwimmen konnte, war nicht einmal ein Beiboot notwendig. Die Distanz
mochte etwa der Entfernung entsprechen, die Lilið einst krank geschwommen
hatte. Drude würde einige Male auftauchen müssen, damit Lajana und Lilið
atmen könnten. Drude meinte, es böte sich an, dass Lilið Lajana und sich
für den Transport falten würde. Lilið musste demm rechtgeben, aber unheimlich
war es ihr doch. Sie war schon einmal in gefalteter Form von Drude
transportiert worden und das hatte nicht gut geendet. Es war eine Art
von Auslieferung, die Lilið nicht mochte, stellte sie fest.

Umgekehrt schien Drude kein Problem damit zu haben, Lilið zu vertrauen.
Jedenfalls hatte dey nicht einen Moment gezögert, als Lilið sich mit demm
zusammen in eine Ratte gefaltet hatte.

Sie saßen in den Morgenstunden zusammen im Kartenraum und nutzten die Zeit,
in der Matrose Ott noch schlief.

"Du wunderst dich, dass ich dir vertraue?", fragte Drude.

Lilið nickte. Sie hatte es vielleicht irgendwodurch deutlich gemacht, oder
Drude war schon wieder zu gut darin, in sie hineinzugucken.

"Das Machtgefälle ist geklärt, das hatten wir schon.", sagte Drude. "Ich
weiß, dass du mich nicht ewig gefaltet belassen kannst, und dass ich mich
auch selbst durch Zappeln vielleicht nicht auf die angenehmste Art, aber
doch entfalten kann."

Lilið nickte. "Ein Rippenbruch wäre wohl das schlimmste, was du dir
zuziehen könntest, und dazu müsstest du dich sehr unglücklich
anstellen. Eigentlich sollte das nicht passieren können."

"Und du brauchst eine gewisse Konzentration, um mich in einer Faltung
zu halten, sollte ich mich wehren. Wenn du dann auch noch etwas
vorhast, was ich nicht will, ist dein Risiko recht hoch, dass nichts
so läuft, wie du willst.", fuhr Drude fort. "Wenn
du von dieser Crew dabei erwischt wirst, mir etwas anzutun, bist du
tot."

"Du willst damit sagen, dass ich gegen dich ohnehin keine Chance habe,
selbst wenn du in einer etwas unvorteilhafteren Lage wärest als jetzt." Lilið
sortierte die benutzten Navigationswerkzeuge zurück in die Schublade.

Der halbe Tag Verzögerung war nun angezettelt. Der veränderte Wind
reichte aus, um zu erklären, warum auf ihrem Weg durch die Strömung eine
Untiefe entstehen würde, die sie für zu gefährlich hielte, um mit der
Kagutte darüberzufahren. Eigentlich war sie nicht gefährlich, aber
weder Kapitän noch Matrose Ott hatten gezögert, daran zu glauben, als
Lilið es mit ruhiger Stimme vorgetragen hatte. Also fuhren sie mit gerefften
Segeln nun langsamer, um die Stelle zu passieren, wenn die Strömung
drehte und wieder mehr Wasser dorthin schieben würde.

"Du würdest zu viel riskieren.", meinte Drude. "Du hättest vielleicht
eine Chance gegen mich, aber es wäre wahrscheinlich, dass du dabei
erwischt würdest. Was brächte es dir dann?"

"Ist nun der Moment gekommen, in dem du mich über meine Macht
aufklären möchtest?" Lilið fragte dies skeptisch. Sie war sich
nicht sicher, ob sie überhaupt eine so geartete Macht haben wollte, an
die Drude vielleicht dachte.

Drude nickte und eins dieser Lächeln berührte der Gesicht. "Hast
du Probleme mit dem Magen, wenn ich von blutigen Dingen erzähle?", fragte
dey. "Ich habe so etwas nicht und kein Gefühl dafür. Aber als Kind
wurde ich oft aufgehalten, wenn ich von blutigen Dingen oder
Leichen und so etwas erzählt habe. Leute könnten dann nicht mehr schlafen, haben
sie gesagt."

Lilið schüttelte den Kopf. "Ich habe keine Probleme damit, aber ich
kann auch nicht so falten, dass es reißt. Nicht magisch.", sagte sie.
"Ich habe das mit Marusch besprochen. Weil Papier ja auch reißt, wenn
es mit der Hand zu scharf gefaltet und dann die Falte noch unter
Spannung gesetzt wird. Aber wenn ich falte, dann geht es dabei nur
um Umsortieren von Material. In dem Moment, wo es reißen würde, kann
ich die Falte einfach nicht mehr machen."

Drude zuckte mit den Schultern. "Aber du kannst dich in einen Würfel
falten, ohne dass am Würfel etwas auffällig wäre.", hielt dey
fest.

"Ja." Lilið nickte und fühlte sich doch, als wäre ein 'aber'
angebracht, von dem ihr nicht einfiel, wie sie es füllen sollte.

"Hast du dir überlegt, was passiert, wenn jemand den Würfel runterschluckt
und du dich dann entfaltest?", fragte Drude.

Lilið unterdrückte die Frage, warum jemand einen Würfel herunterschlucken
sollte. Darum ging es wohl nicht. Es ging um die Frage, was passierte,
wenn sie sich in etwas oder eben in jemandem entfaltete. "Der Koffer
ist auseinandergesprungen.", erinnerte sie sich.

"Mit Wucht und Gewalt.", fügte Drude hinzu. "Ich habe mir einfach
vorgestellt, was wäre, wenn das ein Brustkorb wäre, oder ein Bauch,
in dem du dich entfaltest."

Lilið wurde tatsächlich für einen kurzen Moment flau im Magen, als
sie es sich genauer ausmalte. "Ich kann mir nicht so gut
vorstellen, dass mir Magensäure bekommt. Oder dass eine Person
vorm Schlucken nicht auf mir herumkaut.", überlegte sie.

Drude nickte und wieder war da ein kurzes Lächeln. "Du könntest
bei einigen in dieser Crew beobachten, welche Medikamente sie
zum Frühstück einnehmen, die sie nicht kauen dürfen. Du
kannst, wenn du dich faltest, den Mantel des Nautikas mitfalten
und um dich herumbelassen, sodass er vor Säure für einen
kurzen Zeitraum schützen sollte.", führte Drude
aus. "Es ist sicher nicht einfach, das für dich
gefahrlos durchzuführen, aber du kannst mit deiner Magie
nicht nur flüchten. Du kannst damit auch töten, wenn du
dich einigermaßen geschickt anstellst und willst. Und geschickt
genug bist du."

Lilið nickte nachdenklich. "Ich glaube, das würde ich nicht
wollen, aber es ist ein guter Ansatz zum Nachdenken.", sagte
sie. "Vielleicht sollte ich mal in die Richtung weiter
nachdenken, um herauszufinden, was für Potenzial das Ganze
hat."

---

Das Navigieren über den Tag hinweg machte Lilið ziemlich nervös. Wie
immer achtete Matrose Ott, solange er wach war, auf jeden ihrer
Schritte und fragte sie sehr genau aus. Er verhielt sich nicht
auffällig. Aber sie erzählte dieses Mal Lügen. Lügen, die sie
gut als Wahrheit verkaufen konnte, was die Sache leichter machte,
aber eben doch welche, die sie selbst hinterfragt hätte, hätte
Heelem sie ihr zum Beispiel als Wahrheit verkaufen wollen. Wobei,
wenn er beharrt hätte, hätte sie ihm das wohl abgekauft. Matrose
Ott erleichterte ihr das Ganze, indem er gar nicht erst nachfragte.

Gegen Abend passierten sie die Untiefe zu Liliðs ausgerechneter
Zeit, zu der sie außerdem ziemlich genau so aussah, wie Lilið
es aus der Karte abgeleitet und erklärt hatte: Die
Wellen brachen hier anders und sie malte sich als
grünliche Fläche gegen das dunkle Meer ab, die schon von
Weitem zu sehen gewesen war. Eigentlich
war es immer noch ein Dunkelgrün, aber gegen das fast schwarze Meer
darum herum wirkte sie aus der Ferne fast wie ein Leuchten im Meer.

Die Kagutte lag leicht schräg im Wasser und fuhr nun wieder
mit vollen Segeln darüber. Lilið würde sie vermissen, stellte
sie fest. Das erste größere Schiff, dass sie navigiert hatte. Sie
fühlte einen gewissen Stolz darauf, dass alles so gut geklappt
hatte. Nun war das Zertifikat als Nautika definitiv auch
gerechtfertigt.

Ob sie Heelem wiedersehen würde? Ob es ihn auch glücklich machen
würde? Oder ob er wütend wäre, weil sie nicht erst ein Zertifikat
zum Leicht-Nautika erhalten hatte, wie er es vorgesehen hatte?

Sie lehnte sich neben Matrose Ott über die Reeling und betrachtete
den hellen Flecken unter sich. Das Wasser war klar, aber bis zum Grund
konnten sie deshalb trotzdem nicht sehen. Irgendwo tief unten
konnten sie die Umrisse eines Wals ausmachen, der aber keine
Lust zu haben schien, sich für die Kagutte zu interessieren.

Obwohl sie nicht fror, hielt Lilið den Mantel des Nautikas zu, damit
der Kragen vom Wind nicht von ihrem Hals weggepustet
werden könnte. Sie hoffte, dass es nicht zu auffällig wäre, aber
auch Matrose Ott hielt den eigenen Körper umklammert. Lilið
fühlte den mit salzigen Sprühtropfen versetzten
Wind im Gesicht. Wie lange hatte Lajana keinen Wind mehr gespürt?

---

Auch, als sie sich zusammen mit Drude faltete und sich mit demm wieder
unter der Tür hindurch zu Lajana in den sichersten Raum der Kagutte
schob, war sie nervös. Sie rollten ausversehen übereinander, als Lilið sie
entfaltete. Kontrolle darüber, dann direkt in einer sitzenden Haltung
zu landen, hatte Lilið zu zweit noch nicht.

Lajana kicherte. "Kommt ihr jetzt immer zu zweit?", fragte sie. "Ich
glaube, ich wüsste das gern lieber vorher. Sonst stresst mich das."

Lilið und Drude blickten sich an. Sie hatten sich nicht abgesprochen,
wieviel sie Lajana erzählen wollten.

"Morgen Nacht kommen wir auf jeden Fall noch einmal zu zweit.", informierte
Lilið. "Danach weiß ich es noch nicht."

"Du hast eine Routine, wie du mit allen Wachen hier sprichst, oder?", fragte
Drude.

"Eine Routine?", fragte Lajana.

Lilið erinnerte sich daran, wie Lajana mit der Wache gesprochen hatte, die
sie in diesen Raum gelassen hatte. "Du sagst zu den Wachen immer
das Gleiche.", erklärte Lilið.

Lajana schüttelte den Kopf. "Zur Begrüßung sage ich zum Beispiel etwas
anderes, als zum Abschied.", sagte sie. "Aber Marusch hat erklärt,
dass ich ein Ritual daraus machen soll, das sich wie in einem Theaterstück
immer wiederholt, damit ich mich daran halten kann, wenn ich
etwas nicht verraten soll." Lajana wirkte kurz nachdenklich und
fügte hinzu: "Eigentlich hat sie nicht gesagt, dass ich das machen
soll. Wir haben das zusammen als Trick herausgefunden. Aber er klappt
nicht immer."

Als Lilið dieses Mal zu Drude hinüberblickte, sah sie wieder das
kurze Lächeln, und dieses Mal hatte sie es auch dort erwartet.

"Traust du dir zu, bis morgen Nacht das Ritual einfach
durchzuziehen?", fragte dey.

"Fliehen wir morgen Nacht?", fragte Lajana. Sie hielt eine Hand hoch,
die Lilið und Drude daran hinderte, weiterzureden, dachte nach und
ließ sie dann sinken. "Ich möchte wenig wissen, aber ich glaube,
es ist sinnvoll, wenn ich Dinge vorher weiß, die mich erschrecken
könnten."

Drude nickte. "Das habe ich mir so gedacht.", sagte dey. "Ich denke, es
ist hilfreich, wenn du folgende Dinge weißt: Es passiert morgen
Nacht. Eigentlich, dachte ich, dass nur Lilið dich abholt. Und zwar
würdet ihr euch zusammen zu einer Ratte falten. Das übt ihr jetzt. Wenn
ihr unter der Tür durch gekommen seid, krabbelt Lilið euch bis
zur Treppe. Dort sammele ich euch ein und stecke
euch in meine Tasche. Ich verlasse das Schiff durch meine Unterwassertür
und schwimme euch an Land."

Es klang so einfach, wenn Drude es so darlegte. Aber das war es
eigentlich nicht. Vor allem dadurch nicht, dass Lilið es immer noch
nur dieses eine Mal bei ihrer Faltung in den Würfel geschafft hatte,
ihr Körpergewicht so weit zu reduzieren, dass es zum gefalteten
Gegenstand passte. Manchmal, mehr durch
Zufall, schaffte sie es ein wenig, aber sie mussten damit rechnen,
dass die Ratte, in die sie sich mit Lajana zusammen falten würde,
ihrer beider Gewicht hätte. Und Drude war stark, aber eine so schwere
Ratte unauffällig durch die Kagutte zu tragen, war abenteuerlich.

Im Wasser traute Drude sich das Gewicht ohne
Probleme zu schleppen zu. Dey hatte durchaus schon
bei so manchem Rettungsmanöver zwei Menschen gleichzeitig geschleppt. Keine
so langen Strecken zwar, aber dey war dabei auch nicht an dere körperlichen
Grenzen gestoßen.

Lilið machte sich darum Sorgen, weil sie als Ratte gefaltet
eine höhere Dichte und dadurch weniger Auftrieb hätten als noch
so schwere Menschen. Sie könnten Drudes Tasche kaputt reißen. Drude
meinte, sie müssten ohnehin irgendwann zum Atmen auftauchen, und
da wäre wohl eine Umfaltung möglich, durch die Lilið und Lajana in
getrennte Taschen aufgeteilt werden könnten. Und im Zweifel würde Drude
sie in menschlicher Form schleppen, sobald sie weit genug von der
Kagutte weg wären.

Drude hatte einfach die Ruhe weg. Lilið erinnerte es ein wenig an
Marusch, aber sie glaubte, dass die Ruhe bei beiden sehr verschiedene Ursachen
hatte. Marusch hatte im Zweifel mit allem abgeschlossen, oder
fühlte manchmal einfach gar nichts. Oder sie war albern und gab anderen
Gefühlen Raum. Drude war eher pragmatisch und ruhte in derem eigenen
Selbstvertrauen.

Lajana blieb gefasst, aber Lilið spürte ihre Angst. Da war es sehr
gut, dass Drude so ruhig und sicher blieb und in der Lage war, bei
den wichtigen Informationen zu bleiben. Es war so gut gewesen, demm
mit einzubinden. Hoffte Lilið. Unter derer Anweisungen übten Lilið
und Lajana das gemeinsame Falten. Dey beschrieb Lajana im Vorfeld,
wie es war, gefaltet zu werden. Lajana meinte dazu, dass es ganz
schön intim klänge, und das wiederum verunsicherte Lilið. War es
das?

Aber als sie Lajana von hinten in den Arm nahm, löste sich ihre Angst
auf seltsame Weise auf. Sie erinnerte sich an ihre erste komplexere
Faltung mit Marusch. Sie hatte damit Schwierigkeiten gehabt, weil
Marusch eine Rolle gespielt hatte, und Marusch hatte wenigstens ein
bisschen loslassen müssen. Das hatte vielleicht etwas mit Intimität
zu tun. In jedem Fall mit dem Teilen von etwas Persönlichem. Aber
sie war über die Zeit besser geworden. Und Lajana spielte nicht, oder
nur sehr wenig. Sie waren sich auch bereits ein paar Mal sehr nahe
gekommen, sowohl körperlich als auch emotional. Lilið brauchte
nicht mehr, um eine gute Verbindung aufzubauen,
sich hineinzufühlen und sie beide gemeinsam zu falten.

Sie mussten es einige Male üben, weil Lajana glaubte, in der gefalteten
Form keine Luft zu bekommen. Sie weinte dabei und meinte erst, sie müssten
eine andere Möglichkeit finden. Wie auch immer Drude es schaffte, sie
zu überzeugen. Zum Ende hin hielten sie eine halbe Stunde in gefalteter
Form aus und Lajana fühlte sich hinterher zerknittert, wie sie sagte,
aber in Ordnung.

---

Später, bei Liliðs nächtlicher Navigationsstunde, sagte Drude zu
ihr: "Wenn Lajana doch Panik bekommen und das mit der Faltung nicht
klappen sollte, holst du mich mit rein. Ich glaube, ich kann sie
besser beruhigen als du. Dann kommt erst ihr zu zweit raus, du
versteckst sie hier im Kartenraum und holst dann mich ab."

Lilið nickte. "Hast du doch Angst?", fragte sie.

Drudes Stirn runzelte sich einen Moment. "Große Angst.", gab
dey ohne Umschweife zu. "Wirke ich so furchtlos?"

"Schon irgendwie.", antwortete Lilið. Konnte sie Drude wirklich so
schlecht lesen?

"Ich hoffe, Lil kommt vor morgen Abend irgendwann zurück.", sagte
dey. "Sonst wird es schwierig, dass dey uns findet."

Wann hatte Drude eigentlich angefangen, das Pronomen auch für die
Abe zu verwenden? Aber es ergab vielleicht Sinn, wenn Drude es als eines
empfand, durch das kein Geschlecht zugewiesen würde. "Ich mag, dass
du für die Abe auch 'dey' nimmst.", sagte sie also. "Das sollte ich
mir auch angewöhnen." Und dann, als Lilið klar wurde, was Drude
ängstigte, sagte sie noch: "Brauchst du eine Umarmung oder so etwas?"

Drude schüttelte den Kopf. "Es sei denn, du willst mit mir ringen
und den Kampf verlieren.", murmelte dey. "Aber ich bin gerade nicht
in Stimmung auf eine erregte Reaktion dabei. Und ich glaube, für dich
fällt immer beides zusammen. Erregung und, hm, Aufgabegefühl? Sind
es zwei halbwegs trennbare Dinge für dich?"

Lilið schluckte. Immerhin hatte sie die extreme Erfahrung von
so etwas wie einer Extase erst in der Nacht zuvor gemacht. Ihr
Körper reagierte zwar auf Drudes Worte, aber hätte es vielleicht
mehr getan, wenn er in diesem Zusammenhang nicht angenehm erschöpft
gewesen wäre. "Ich glaube, es sind zwei sprachlich trennbare
Gefühle, die aber immer zusammen auftreten.", murmelte sie. "Es
tut mir leid."

"Es braucht dir nicht leid zu tun.", sagte Drude. "Ich gehe einfach
gleich schwimmen. Das mache ich öfter. Körperliche Auslastung
hilft. Brauchst du mich noch?"

Lilið dachte kurz nach und schüttelte dann den Kopf. "Ich wollte die
Nacht nutzen, um unsere Wartezeit auf Lettloge besser einzugrenzen. Ich
wollte schauen, wann Heelem ankommen könnte, wenn er alles daran
setzt, schnell zu sein."

"Klingt gut.", meinte Drude. Einen Moment stand dey noch unschlüssig im
Raum. Dann, ohne Abschiedsgruß oder auch nur ein Winken, trat dey aus
der Tür und verschwand.

Lilið atmete gegen die Anspannung langsam ein und aus. Sie ließ sich nicht
lange aufhalten.

Laut Drude flog die Abe ziemlich schnell. Lil mochte einen halben Tag
bis Tag gebraucht haben, um den Brief an Heelem zuzustellen. Dann würde
dey von dort etwa zwei Tag zu König Sper benötigt haben und sollte nun
von dort auf dem Rückweg zur Kagutte sein. Die Reise der Abe, nachdem
sie Heelem verlassen hatte, war für Liliðs Planung eigentlich nicht
wichtig.

Wenn Heelem sofort eine schnelle Fragette nach Nederoge genommen hätte, wäre er
einen bis eineinhalb Tage später dort gewesen. Lilið prüfte die
Karte dahingehend erneut, weil ihr das schnell vorkam, aber Distanz
und Strömungen sollten ihm in die Hände gespielt haben. Angelsoge wurde derzeit
von einer Reihe Reiseinseln näher an Nederoge herangeschoben. Und von
Nederoge waren sie mit der Kagutte eigentlich bereits etwa eine Woche
unterwegs. Allerdings waren sie in den ersten beiden Tagen weit vom
Weg abgekommen, in jene Reiseinselgebiete, aus denen es nicht leicht und
durchaus langwierig gewesen war, sie wieder heraus zu manövrieren. In
ihren ersten vier Tagen der Reise hatten sie eine Strecke zurückgelegt,
die sie andernfalls an einem bis maximal zwei Tagen geschafft hätten. Sie
hatten außerdem einen halben Tag Verzögerung, sodass sie insgesamt eine
Strecke geschafft haben mochten, die etwa vier bis fünf Reisetagen
entsprechen mochte, hätte Lilið sie von vorherein mit der Kagutte
mit der Absicht navigiert, schnell zu sein. Das war immer noch ein großer
Vorsprung, aber die Kagutte war groß und schwer. Es gab eine Kategorie
von Segelfahrzeug zwischen Boot und Schiff, das sehr schnell war, auf dem
üblicherweise für bis zu fünf Personen Platz war, wenn sie sich zusammenferchten,
das mit ausreichend befähigter Crew Tag und Nacht segeln konnte und mit dem
eine solche Strecke zwischen zwei und drei Tagen machbar wäre. Es
war schwierig an so einen Bootstyp zu kommen. Aber Heelem hatte
vielleicht gewisse Privilegien, mit denen das ginge.

Wenn Heelem wirklich viele gute Kontakte hätte, könnte er auch Post
in Richtung Lettloge versenden und ihnen Leute direkt vor Ort
schicken, aber daran glaubte Lilið eigentlich nicht. Lettloge war
eine kleine Grenzinsel. Wie wahrscheinlich war, dass Heelem dort
Menschen kannte?

Lilið navigierte über den Rest der Zeit ihrer nächtlichen Wachstunde
von verschiedenen Abfahrtszeitpunkten auf Nederoge ausgehend, die für
Heelem realistisch wären, je nachdem, wann er eine Fragette bekommen
hätte, Routen auf die Insel Lettloge. Wieder brauchte sie eine
Weile, bis sie die Veränderungen der Karte gut genug kennenlernte, dass
sie ein Gefühl dafür bekam.

Am Ende räumte sie mit Hoffnung und Zweifeln zugleich alles wieder
so auf, dass nicht auffiele, dass sie ganz anders navigiert hatte
als vorgesehen. Hoffnung, weil die Reiseinseln zwischen Nederoge
und ihnen viele Möglichkeiten zuließen, dass Heelem vielleicht
sogar noch einen Tag rausholen könnte, sodass sie, wenn alles gut
klappte, nur einen halben Tag auf Lettloge auf ihn warten müssten.

Und Zweifel, weil, ja, weil viele Dinge nicht gut
laufen konnten. Was, wenn Heelem nach diesem
einen Treffen mit ihr einfach nicht das Vertrauen zu ihr gefunden hatte,
auch nur irgendetwas für sie zu tun? Und schon gar nicht, wenn
die Idee dabei war, einer Kagutte mit einer im Kampf ausgebildeten
Besatzung in die Quere zu kommen,
die eine königliche Kriegskaterane im Alleingang
zerlegen könnte? Was, wenn Heelem eigentlich nur
eine oberflächliche Freundschaft mit Marusch hatte, in der sie sich
eben körperlich nahe kamen, das aber keine tiefe Bedeutung hatte? Was,
wenn Marusch Heelem falsch einschätzte und er, wenn die Gelegenheit
reizvoll genug wäre, ihnen in den Rücken fallen würde? Was, wenn Heelem
von der Sorte Mensch wie Allil wäre?

Und etwas positivere Fragen: Was, wenn Heelem sich als erstes aufmachen
würde, Marusch zu suchen, und diese Suche aussichtslos wäre, weil
Marusch tot oder verschollen wäre?

Was, wenn Heelem nicht so viele Privilegien hätte und einfach zwei Wochen
brauchen würde, bis er ankäme?
