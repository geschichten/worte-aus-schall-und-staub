Nach uns die Sinnflut
=====================

*CN: Misgendern, scharfe Klingen, Knebeln,
toxische Mutter-Kind-Beziehung, emotionale Manipulation, Fäkalien,
Demütigende Gesten, sexualisierte Übergriffigkeit.*

Sie sprachen nicht, bis sie an der Teeseufel ankamen. Dort hielt
Drude sie davon ab, an Bord zu gehen. "Ich denke, das Igeldings ist
auf der sternschen Kagutte. Ich fange an, es als Körper einer Person
wahrzunehmen, ich bin nicht ganz sicher.", informierte dey sie leise. "Ich
glaube, wir sollten die Gelegenheit jetzt ergreifen und schauen, ob
ich Recht habe. Jede zukünftige Gelegenheit wird komplizierter werden. Jetzt
reicht vielleicht eine geschickte Ausrede."

"Richtig!", stimmte Marusch zu. "Ich hatte vorhin auch den Eindruck, dass
das Signal klarer ist. Und ja, vielleicht ginge es aktuell einfach,
indem Lajana oder ich behaupten, dass es an uns übergeben werden
soll, weil wir schneller zurück auf Angelsoge sein werden."

"Aber wollen wir denn nach Angelsoge?", fragte Lajana.

"Wir haben noch keinen Plan, wohin es hingeht.", widersprach
Marusch. "Aber wir können die Wachen auf der sternschen
Kagutte anlügen."

Lajana blickte Marusch einige Momente nachdenklich an, dann
nickte sie. "Ich möchte das machen."

Marusch runzelte die Stirn. "Bist du sicher? Traust du
dir das zu?"

"Ich bin nicht sicher, aber ihr seid ja in der Nähe.", sagte
Lajana. "Es ist nur: wir sind alle noch wütend. Und ich kann
nichts Schlimmes anstellen, selbst wenn ich wütend bin. Ihr
schon. Versteht ihr?"

Marusch nickte. "Ich würde mit dir ein Zeitfenster
vereinbaren, für das ich mich raushalte. Drude
würde, wenn dey mag, von außen mit achtgeben. Und
wenn unsere Sorge zu groß ist, kommen
wir nach.", sagte sie. "Du hast recht. Ich habe ein bisschen
Zerstörungswut."

"Ein bisschen mehr als ein bisschen.", korrigierte Lajana.

"Würdest du in Ordnung finden, wenn ich mitkäme?", fragte
Lilið. "Ich bin auch wütend, aber ich kann vor allem
Falten. Ich habe nie gelernt, irgendetwas wirklich Destruktives
zu tun."

Lajana musterte sie und nickte. "Du kannst gern
mitkommen."

---

Der Anfang ihres Überfalls verlief sehr einfach und
widerstandslos. Mehrere der sternschen Wachen erkannten Lajana
und fragten nicht einmal nach, bevor sie Lajana und entsprechend
dann auch Lilið Zutritt gewährten. Lilið fragte sich, als sie
über die Rampe an Bord schritten, ob sie vielleicht besser
in gefalteter Form hätte mitkommen sollen. Aber es
waren sicher Wachen an Bord, die Magie mindestens spüren
konnten.

Lilið folgte Lajana ins Unterdeck. Die Kagutte war sehr anders
als jene, auf der Lilið ungefähr zwei Wochen gewohnt hatte. Das
Holz war besser verarbeitet und edler. Es roch nach
Gewürzen und kaum nach Feuchte. Die Raumaufteilung war
anders, aber Lajana kannte sich hier
ein wenig aus, das merkte Lilið ihr an. Es war angenehm kühl
unter den Planken. Wahrscheinlich befanden sie sich bereits halb
unter der Wasseroberfläche.

Sie liefen an zwei Personen vorbei, die sie auch einfach
passieren ließen. Lilið kam die Kagutte verhältnismäßig verlassen
vor. Wahrscheinlich hatten die meisten, die nicht
draußen zwischen den Wachen standen, gerade frei und
Landgang. Aber an einem Tisch am Rand in der Messe saß Herr
Hut und schrieb etwas in ein großes Buch. Lilið hoffte, einfach
an ihm vorbeigehen zu können, ohne dass er sie entdeckte, aber
natürlich blickte er auf.

"Wartet!", rief er, als Lilið, Lajana hinterhergehend,
versuchte, ihn zu ignorieren. "Oder..."

Weiter ließ Lilið ihn nicht kommen. Sie drehte sich hastig
zu ihm um. "Oder was?" Zu Lajana sagte sie: "Geh ruhig schon
weiter. Ich kümmere mich." Binnen dieser wenigen Sekunden
hatte sie entschieden, dass es wohl besser wäre, Herrn Hut zu
beschäftigen. Er hatte in der Zentral-Sakrale auch Lajana
schlimm behandelt. Und Lilið wiederum traute sich plötzlich
zu, mit ihm zielführend umzugehen.

"Drohst du mir?", fragte Herr Hut.

Lilið wartete, bis Lajana zustimmend genickt hatte und
in den hinteren Teil der Kagutte weitergegangen war. Sie
lehnte die Tür hinter Lajana an, drehte sich abermals zu Herrn
Hut um und schritt langsam durch den Raum auf ihn zu. "Ich hatte
gerade den albernen Eindruck, du würdest versuchen, mir
zu drohen." Sie setzte sich lässig auf die Bank ihm gegenüber
an den Tisch. Es stimmte sie zufrieden, die versteckte
Panik unter seiner Oberfläche wahrzunehmen.

"Zugegeben, ich habe dich einst gewaltig unterschätzt, Lilið
von Lord Lurch.", sagte Herr Hut. "Oder sollte ich sagen, die
Blutige Mistress M?"

"Ich bin der Blutige Master M. Du kannst dir schenken, es
als eine Enthüllung klingen zu lassen, mit der du mich
am Haken hättest. Das hast du nicht.", sagte sie.

Vielleicht sollte sie doch vorsichtiger provozieren. Oder
war das gerade gut so? Eigentlich fühlte sie sich
recht sicher, dass sie den Bluff, sehr mächtig zu sein, gut
herüberbrachte, aber was, wenn er sie doch angriffe? Sie
war eben, wie sie Lajana ja schon erklärt
hatte, keineswegs gut darin, mit Magie zu
kämpfen. Obwohl? Lilið legte die Unterarme auf
den Tisch und stützte sich darauf, legte alles an Lässigkeit
in ihre Haltung, was sie aufbringen konnte.

"Mag sein." Herr Hut wich ein Stück an die Wand zurück und
legte den Stift ab. Aus der angefangenen Bewegung heraus
verschränkte er die Arme hinter dem Kopf. Die Geste
überzeugte Lilið nicht von seiner doch vorhandenen
Selbstsicherheit. "Mir ist aber auch klar, dass
nicht du die Zentral-Sakrale zerstört hast.", sagte
er süffisant. "Sonst hättest du
es noch in meiner Anwesenheit getan. Du warst wehrlos zu dem
Zeitpunkt. So schnell hast du nicht gelernt, mir etwas
entgegenzusetzen. Du kannst mir drohen, so viel du
willst. Ich habe keine Angst vor dir. Was kannst du mit Falten
schon ausrichten? Jetzt plötzlich wie die Prinzessin aussehen,
nützt dir gar nichts."

Lilið spielte mit dem Gedanken, sich zu Maruschs Äußerem zu
falten. Aber wahrscheinlich wusste Herr Hut gar nicht, wie
sie aussah. Sie fühlte in den Tisch und lächelte. "Ich hoffe
einfach für dich, dass es nicht zu einem Kräftemessen
kommt.", sagte sie. Sie realisierte erst jetzt, dass Herr
Hut sie wahrscheinlich hatte provozieren wollen, etwas
über sich preiszugeben, und war im Nachhinein froh, keine
Magie ausgeübt zu haben.

"Oh, das hoffe ich auch.", sagte Herr Hut. "Für
dich allerdings." Er versuchte weiterhin, besonders lässig
auszusehen, und streckte auch noch die Beine aus.

Als einer seiner Füße Liliðs ausgestrecktes, nacktes Bein sanft
berüherte, wäre sie fast zurückgezuckt. Sie riss sich zusammen,
sodass nicht einmal ihr Lächeln verging. Nur die Tischplatte
faltete sie an der Kante, an der er saß, fast bedrohlich langsam
nach, bis sie scharf wie eine Klinge war. Sie überlegte, ob
sie ihm empfehlen sollte, den Fuß wieder wegzunehmen, aber
entschied sich gegen noch mehr Provokation, hoffte, dass er
den Wink auch so verstand.

Dann geschah alles sehr schnell. Lajana trat zurück in die
Messe. Sie hielt das Igeldings nicht in der Hand, verriet
Liliðs kurzer Blick in ihre Richtung. Aber sie wirkte
zufrieden. Herr Hut allerdings nutzte den kurzen Moment
der Ablenkung und tat eine Geste, die Lilið im Augenwinkel
sah und die ihr von Heelems Magie bekannt vorkam. Sie
zögerte keine Sekunde, sondern gab dem Tisch einen kräftigen
Impuls in seine Richtung. Die Befestigung vom Tisch am Dielenboden
hatte sie im Vorfeld schon gelöst. Sie selbst behielt
ihre Verbindung zum Tisch bei, um bei der komplexen Faltung, die sie
vorhatte, genau bei der Sache zu sein: An den Stellen, an
denen die Klingenkante so dicht vor Herrn Huts Körper war, dass
sie durch den Tisch die Nähe spüren konnte, bremste sie sie jeweils
ab. Sie nutzte die kinetische Energie, um die Klinge in einen
Bogen um ihn herum zu falten, sodass ihn die Kante weiterhin
nicht berührte. Es schleuderte Herrn Hut gegen die Rückwand,
das verschärfte Tischmaterial schlug zu seinen Seiten ins Holz
dahinter ein und verband sich mit der Schiffswand. Herr Hut konnte sich nicht
mehr bewegen. Die zuvor erhobenen Arme und sein Brustkorb
waren dicht umgeben von der glänzenden, diamantharten und scharfen
Holzkante, sodass er nur noch sehr flach zu atmen wagte.

Dünn war seine Stimme, als er maulte: "Wenn du mich nicht sofort
befreist, schreie ich!"

Lilið trat näher und riss zwei der frisch beschriebenen Seiten
aus seinem Buch, das zu Boden gefallen war. "Dagegen habe ich
ein Mittel.", sagte sie. Die eine Seite zerknüllte sie (nicht,
wie die meisten Menschen das machen, sondern durch bloße
Berührung) und stopfte sie in seinen Mund, als er wieder zu
reden ansetzte. Seine Zähne waren bloß zu einem schmalen
Spalt geöffnet, weil er hatte sprechen wollen, aber das stellte
für Lilið kein Problem dar. Sie faltete das ganze Papier
durch die Ritze hindurch und formte in seinem Mund ein stabiles Dodekaeder
daraus. Das zweite Papier legte sie auf seinen Mund, faltete
es um seine Lippen herum und am Rand selbiger
mit einer Ziehharmonikafaltung um seinen Hinterkopf. "Alles
nicht lebensgefährlich, wenn du still hältst.", beruhigte sie.

Herr Hut gab ein wimmerndes Geräusch von sich.

"Genau, das hast du richtig verstanden.", erwiderte Lilið. "Wir
hatten einen Vertrag, was ungewollte sexualisierte Berührungen
angeht. Ich zähle die Fußberührung dazu und sehe mich ab jetzt
im Recht, dass ich deine Weichteile jederzeit mit meinem Ellenbogen
Bekanntschaft machen lassen darf. Ich werde es heute aus
persönlichen Gründen bei dieser Verteidugung belassen. Aber
sei dir im Klaren, dass ich in Zukunft bei jeder Begegnung bereit
sein werde, mich stets ohne Zögern und äußerst unglimpflich zur Wehr
zu setzen."

Herr Hut nickte sehr vorsichtig. Es fühlte sich für Lilið
erstaunlich wenig genugtuend an. Sie blickte zu Lajana hinüber,
die einfach nur dastand und Lilið machen ließ. "Fühlst du dich
ausreichend wohl mit mir?", fragte Lilið.

Lajana nickte. "Ich hasse ihn auch. Aber ich bin trotzdem
froh, dass du das so machst und nicht schlimmer."

"Puh. Das verstehe ich.", sagte Lilið. "Hast du es gefunden? Oder
müssen wir noch weitersuchen."

"Ich habe es in meiner Tasche.", sagte sie. "Aber an der
Garderobe im Schatzraum ist dein Mantel des Nautikas. Willst
du den wiederhaben?"

Lilið nickte. Ob für sich oder ob sie ihn irgendwann dem Nautika
zurückschicken würde, dem der Mantel eigentlich gehörte, war
gerade egal. Beides wäre besser, als wenn er hier bliebe. Obwohl:
vielleicht war es ihr doch nicht einmal gerade egal. Ein Lächeln schlich sich
auf ihr Gesicht, als sie sich wenige Momente später, den Mantel
über den Arm gelegt, vorstellte, dass der Stofffisch, der sich darin
befunden hatte, einst wieder in die richtigen Hände
gelangen konnte.

---

Der Himmel war grau, aber es regnete nicht, als sie
Mazedoge hinter sich gelassen hatten und mit Wind
von der Seite über das Meer
segelten. Das war sehr angenehm, fand Lilið. Das Igeldings
und auch das Buch, das sie damals gestohlen hatte, lagen
neben ihnen auf der Bank. Lajana hatte das Buch beim
Igeldings gefunden und einfach auch mitgenommen, weil Marusch
es so gerne mochte.

"Wenn ich nicht Königin werde, ist es vielleicht nicht mehr
so schlimm, dass ich gestohlen habe.", murmelte sie.

Es klang traurig, fand Lilið. Und sie konnte es verstehen. Es
fühlte sich so enttäuschend an. Alles in ihr war wund und
aufgescheuert. Sie war zu verletzt, um wirklich einen
Triumph zu fühlen. "Wir hatten einen epischen Abgang.", sagte
sie. "Fühlt sich das für irgendwen von euch befreiend oder
so etwas an?"

Die Gesichter um sie herum schwankten zwischen grinsen und
stirnrunzeln. Vielleicht ging es den anderen wie ihr.

"Ein wenig.", sagte Marusch schließlich. "Eigentlich nicht. In
mir ist alles dumpf. Aber ein kleiner Teil von mir ist nun
leichter. Die Belastung fällt weg, bald zu ihr
zurückzumüssen. Die Fronten sind geklärter. Ich freue
mich, dass nun wohl vorbei ist, dass ich gebeten werde,
doch zu regieren. Das Spiel ist endlich durchgespielt."

"Ich glaube, ich habe am wenigsten abbekommen. Und wenn
ich irgendwie da sein kann, bin ich das. Ich würde für
euch die Welt abfackeln, auch wenn das eigentlich
eher Maruschs Ding ist, aber mir wird schon was
einfallen.", versicherte Heelem. "Ich muss sagen,
Lilið, du warst echt stimmungserhellend!"

"Auf den Tisch kacken." Drude grinste kurz und zwinkerte
Lajana zu, die bei der ausgesprochenen Erinnerung wieder
kicherte. Dann wurde Drudes Gesicht wieder
mimikfrei, noch mehr als sonst. "Das
war schon kackendreist. Das hat mir gefallen." Dey seufzte. "Ich
fragte mich, ob ich irgendetwas hätte tun sollen. Ich war
so still. Ich habe niemanden verteidigt. Ich bin nicht
gut in so etwas. Ich wusste gar nicht, wo ansetzen, weil
alles auf so unsinnige Weise verletzend war und auf so
verstrickte Art voll an den eigentlichen Problemen vorbei, die
für Regierung und Völker gerade wichtig
wären. Ich habe einfach geschwiegen. Bin ich
überhaupt gut für euch?"

"Was fragst du überhaupt!" Lilið sprang fast auf, hätte
Drude umarmt, wenn dey das zuverlässig gemocht hätte. "Du
warst so derbst oft da für mich. Da kann ich das auf den
Tisch kacken auch selbst mal übernehmen. Würde ich auch für
dich tun, natürlich."

Lajana prustete wieder los und konnte kaum aufhören zu
kichern. "Ich fand das so witzig!" Sie versuchte
zu Atem zu kommen, was erst kaum funktionierte und dann
war sie unvermittelt von einem Moment auf den anderen
wieder ernst. "Es ist nicht Mamas Humor. Sie mag das
nicht. Gar nicht. Sie fühlt sich durch so etwas sehr
verletzt."

Marusch bot ihr eine Umarmung an, aber Lajana lehnte
ab. "Und das tut dir weh.", sagte Marusch leise.

Lajana nickte. Ihre Augen wurden wieder feucht und
das Weinen steckte Lilið an. Lajana sagte: "Ich hasse Lilið."

Lilið schluckte. Damit hatte sie nicht gerechnet. Und
auch nicht mit dem Schmerz, den es in ihr auslöste. Meinte
Lajana das so? Für einen kurzen Moment spielte es keine
Rolle für Liliðs Gefühle.

Lajana blickte sie an, bis Lilið es schaffte, den Kopf
zu heben und Lajanas Blick zu erwidern. "Es
tut mir leid, das wollte ich nicht.", sagte Lajana. "Sie
macht das. Mama macht, dass ich Hass auf dich fühle. Ich
kann da nichts gegen machen. Und ich weiß, dass es nicht
stimmt. Ich habe dich nämlich auch ganz schön lieb. Ich
hasse, dass ich den Hass fühle. Wie mache ich den weg?"

"Ich kenne das." Marusch blickte in die Ferne, irgendwo
in ein Gewölk, das sehr angenehm für die Augen war. Sie
atmete tief ein und aus. "Ich glaube, bei mir hat Trotz
geholfen. Und ich habe mir klar gemacht, dass ich nicht
um jeden Preis will, dass Muddern sich wohl fühlt."

"Dir ist doch egal, ob sie sich wohl fühlt oder
nicht.", sagte Lajana.

Marusch schüttelte den Kopf. "Meistens ja. Aber das war
nicht immer so. Früher wollte ich es um jeden Preis. Wenn
ihr eine Person in meinem Leben nicht gepasst hat, dann
war sie darüber sehr wütend und hat es nicht ausgehalten,
es mir nicht immer wieder aufzubinden. Sie hat mir vermittelt,
dass es ein Charakterfehler an mir wäre, nicht zu verstehen,
wie schlimm die Person jeweils wäre. Ich hatte die Wahl dazwischen,
die Person zu hassen und aus meinem Leben zu entfernen, oder
in ihren Augen irgendwo falsch abgebogen zu sein. Letzteres
bedeutete, dass ich Schuld daran war, dass es ihr nicht
gut ging, und dass sie mich stets an mein menschliches
Versagen erinnerte. Ich habe mich oft für ersteres
entschieden. Bis Heelem unsere Familie verlassen hat und mir die
Bindung zu wertvoll vorkam, um sie über Bord
zu werfen. Da hatte ich das ganz stark. Hassgefühle ihm
gegenüber, die nicht richtig waren und die ich immer wieder
bekämpfen musste."

"Ich wusste, dass ich dich in eine Zwickmühle gesteckt habe,
was mir immer noch leid tut, aber was eben sein musste, aber
mir war bis jetzt nicht klar, wie extrem das für dich war.", murmelte
Heelem. "Und ich bin sehr froh, dass du gekämpft hast. Ich
liebe dich. Willst du auf den Arm?"

Marusch nickte. Heelem platzierte sich so um, dass er wieder
mit dem Bein steuerte, was bei dem Wind versprach, nur mäßig
gut zu funktionieren (aber sie wussten ohnehin noch immer nicht,
was das Ziel war). Marusch kuschelte sich auf seine Brust. Heelem
küsste sie auf die Nasenspitze.

"Das fühlt sich angenehm vertraut und wunderschön schwul
an." Maruschs Stimme war ein Gemisch aus traurig und
behaglich.

"Bist du mein schwules Mädchen?", fragte Heelem.

Marusch nickte. "So ungefähr."

"Mama hat mich aber nicht gezwungen, dich zu hassen.", meinte
Lajana zu Heelem. "Gezwungen eh nicht, aber diese Sache gemacht."

"Emotionalen Druck aufgebaut.", konkretisierte Marusch. "Sie
hat dir nicht so sehr reingedrückt, dass es falsch wäre,
Heelem zu mögen. Ich weiß bis heute nicht, warum sie das
bei dir nicht gemacht hat aber bei mir. Vielleicht, weil
ihr Heelems und meine Beziehung zu innig war."

Lajana lächelte. "Ich will jetzt mit Lilið so eine
Beziehung. Und mit Drude!", sagte sie. "Aber ich weiß nicht,
ob Mama Drude hasst."

"Bestimmt." Drude wirkte halb abwesend, den Blick in die
Ferne gerichtet. Eine Weile schwieg dey, dann fügte dey
leise hinzu: "Spätestens, wenn sie mich näher kennen
lernt. Immerhin habe ich Lilið lieb."

"Es ist schön, dass wir uns kreuzweise alle so lieb
haben.", sagte Marusch.

"Was bedeutet eigentlich antiautoritär?", fragte Lajana.

Marusch kicherte. "Du bist immer für einen überraschenden
Themenwechsel zu haben!"

"Ich habe mir das Wort endlich gemerkt! Und es ist mir gerade
wieder eingefallen, dass ich das fragen wollte.", erwiderte
Lajana. Sie klang ein bisschen stolz.

"Hm, lass mich nachdenken, wie ich das erkläre. Weißt
du, was eine Autorität ist?", fragte Marusch.

Lajana runzelte die Stirn. "Ich bin mir nicht sicher.", sagte
sie. "Eine Person, die bestimmen darf?"

Marusch nickte und legte nachdenklich einen Finger an die
Lippen. "So ungefähr, denke ich."

"Und du bist dagegen, dass eine Person über andere bestimmen
darf?", fragte Lajana.

"Nicht immer." Marusch machte ein nachdenkliches Geräusch. "Ich
finde nicht, dass eine Person aus Gründen über andere bestimmen
dürfen sollte, die mehr auf Zufall basieren. Also zum Beispiel
sollte aus meiner Sicht eine Person nicht einfach Macht über eine
andere bekommen, weil sie älter ist, oder weil sie einen Titel
hat, oder weil sie zufällig adelig ist, oder weil sie
stärker oder skorscher ist oder sonst etwas in der
Richtung. Hm."

"Zählt dazu, Königin sein, weil ich Kind einer Königin
bin?", fragte Lajana.

Marusch nickte. "Ja. Wenn du Königin wärest, bekämst du
von mir nicht automatisch deswegen Respekt. Im Gegenteil. Sobald
du deine Machtposition zum Nachteil deiner Untergebenen und zu deinem
Vorteil nutzt, kacke ich dir spätestens auf den Tisch."

Lajana kicherte schon wieder. Es fiel ihr sichtlich nicht so
leicht, ihre Gedanken zu sortieren. "Aber wäre für
dich in Ordnung, wenn überhaupt wer Königin ist?", fragte
sie. "Denn du würdest ja nie Königin werden. Und auch die
Garde nicht anführen. Und zwar, weil du antiautoritär
bist, oder? Glaubst du, du würdest die Macht dann ausnutzen?"

Die Frage stimmte Marusch so nachdenklich, dass sie sich aus
Heelems Umarmung löste und sich gerade hinsetzte. "Du hast
schon recht. Eigentlich bin ich gegen Monarchie im
Allgemeinen.", sagte sie. "Der Trick ist: Wenn wir
keine Monarchie hätten, was hätten wir dann? Ich glaube,
dass unsere derzeitige Gesellschaft nicht damit umgehen
könnte, keine Person in einer koordinierenden Rolle zu
haben und sich einfach selbst zu organisieren. Und für so eine
koordinierende Rolle würde ich eine Gruppe von Personen
suchen, die verschiedene Lebensrealitäten kennen. Personen, die
wissen und selbst erlebt haben, was in unserer Gesellschaft
besonders weh tut. Damit wir wirklich gegen unterdrückende
verinnerlichte Gedanken und existierende Machtgefälle
vorgehen können, glaube ich, müssen
Menschen die Vertretung für uns bilden, die die Ungerechtigkeit
am eigenen Leib erfahren haben. Und der Rest der Bevölkerung
ist dazu da, ihnen zu ermöglichen, die Rolle auszuüben. Das
ist vielleicht nicht völlig zu Ende gedacht, aber so in
etwa stelle ich mir vor, was wir tun müssen, um Machtgefälle
wirklich aufzubrechen." Marusch holte langsam tief Luft und ließ
sie sanft lächelnd entweichen. "Das Ding ist, zufälliger
Weise bist du eine in eine Königsfamilie geborene Person,
die ich als Volksvertretung wählen würde. Mit voller
Überzeugung. Nicht, weil du da hineingeboren wurdest, hätte
ich dich als Königin gewollt, sondern weil ich dich in
jedem System, das ich für einen guten Ersatz für die
Monarchie halten würde, in eine führende Rolle wählen würde. Du
bist die einzige Königin, die ich je respektiert hätte."

"Änsehaut.", nuschelte Lilið.

Heelem nickte. Er hatte nun auch Tränen in den Augen.

"Aber dich selbst hättest du nicht als Königin
respektiert?", fragte Lajana. "Obwohl du diese Geschlechtersache
kennst, die dir weh tut, die ich nicht kenne?"

Marusch nickte. "Es wäre was anderes, wenn ich nicht im Stande
wäre oder wenigstens mehr zögern würde, für meine Liebsten
zu morden und zu zerstören. Aber eigentlich reicht mir die
Fähigkeit, so etwas zu tun, allein schon aus, um zu sagen, mir
sollte niemals eine Regierung in die Hände gelegt werden."

"Hm.", machte Lajana. "Aber da kannst du doch auch nichts
für."

"Das stimmt.", sagte Marusch lächend. "Es führt trotzdem dazu,
dass ich eine höhere Machtposition in der Gesellschaft habe als
du. Ich habe es leichter als du. Ich bekäme viel
leichter Regierungsaufgaben zugeordnet als du. Und wenn ich
sie angenommen hätte, hätte ich zwar vielleicht Entscheidungen
für Menschen wie dich fällen können. Vielleicht wäre das
am Ende besser gewesen, weil es etwas bewirkt hätte. Aber ich
hielt mehr von Radikalität. Ich sah stattdessen als
logische Konsequenz, als mir Macht zugeordnet
wurde, den Platz für dich zu räumen. Ich
frage mich oft, ob diese Radikalität falsch war und ich als
Königin mehr hätte bewirken können, aber ich glaube nicht. Wo
hätte ich dann die Grenze gezogen? Ich wäre nicht nur mir
unglaubhaft vorgekommen, es hätte sich auch nicht viel
geändert, glaube ich."

"Du bist zu schnell, aber ich bin auch müde.", sagte
Lajana. "Du wolltest ja auch einfach nicht regieren. Du
meinst nur immer, ich kann das besser als du. Aber du kannst
so komplex reden und ich nicht. Du hast in der Sitzung
mit Mama etwas verstanden und ich nicht. Ich kann das
doch eigentlich alles gar nicht. Warum denkst du, dass ich
es besser kann als du?"

"In dem Moment, in dem du und nicht jemand für dich
die Entscheidungen fällst, musst du
verstehen.", antwortete Marusch. "Also müssen
alle Beteiligten sich so erklären, dass du sie verstehst. Sie
müssen dann auf einmal mit dir reden. Und am Ende hilft das
nicht nur dir, sondern der ganzen Bevölkerung, weil dann auf
einmal alle Informationen in einfacherer Sprache verfügbar
sind und alle Entscheidungen, die du fällst, transparenter."

"Das einzige Problem, das ich sehe, ist, dass Lajana auch
manipuliert werden könnte.", murmelte Drude. "Ich habe
eure Mutter nun aus der Nähe kennen gelernt. Ich traue
ihr zu, dass sie mich dazu bringen kann, ausversehen etwas
für gut zu halten, was ihr einfach in den Kram passt. Sie
würde ihre Meinung in so passende Worte kleiden, dass
ich sie reflexartig glaube."

Marusch nickte. "Das Problem habe ich auch immer
gesehen.", sagte sie. "Ich hätte mir gewünscht, dass ich
da gegenhalten kann. Und Heelem. Wir sind dagegen auch
nicht immun, aber Heelem hat mir so viel über ihre Tricks
erklärt, dass ich mir zutraue, einiges abzufangen." Marusch
grinste plötzlich zuversichtlich. "Und ich kenne Lajana. Sie
will alles genau verstehen, bevor sie etwas entscheidet
und ist vorsichtig damit. Die Angst vor Manipulation
bleibt durchaus trotzdem." Sie seufzte. "Ich habe es dennoch
für die beste Option gehalten, die wir haben. Selbst wenn
Muddern uns irgendwo noch im Griff gehabt hätte, es gibt
Dinge, da ist Lajana kompromisslos. Es wäre in jedem
Fall besser als vorher geworden, denke ich. Es wäre
es wert gewesen, es zu versuchen."

"Kann ich mit dir kuscheln?", fragte Lajana Drude.

Drude nickte und öffnete die Arme. Nun war nur noch
Lilið nicht bekuschelt, denn Marusch hatte sich wieder
an Heelem gelehnt. Die Abe hatte bei ihrem Ablegemanöver
einen Spazierflug begonnen und war noch nicht wieder
zum Kuscheln oder so etwas zurück. Aber das war vielleicht gerade
nicht schlimm. Lilið wickelte sich in den Mantel des
Nautikas. Es war auch schön, den anderen beim Kuscheln
zuzusehen, während die Dunkelheit des Abends auf sie herabsank.

"Wo segeln wir eigentlich hin?", fragte Heelem.

Niemand antwortete. Liliðs Geist spielte ihr schon den
halben Tag immer wieder ein Ziel vor, für dass sich
ein Teil von ihr längst entschieden hatte, während der
andere Teil deshalb nicht mehr klar über andere Optionen
nachdenken konnte. Sie seufzte und gab nach. "Nach
Schleseroge?"

Heelem lächelte. "Ich habe von der Reiseinsel gehört.", meinte
er nachdenklich. "Es soll ein kleiner, eigenständiger Staat
darauf gegründet worden sein, der irgendwie, hm, ja, sehr
antiautoritär existiert, erzählt man sich. Wir könnten danach
suchen und herausfinden, ob dieser Staat wirklich existiert,
oder ob das bloß ein hoffnungsvolles, modernes Märchen
ist."

"Ich war dort.", sagte Lilið knapp. Nun, knapp reichte den
anderen natürlich nicht, und auf entsprechend fragende Blicke
hin, lieferte sie ihren verschwommenen Bericht nach.

"Ich finde das gut.", sagte Drude. "Das klingt, nach einem Ort,
an dem wir zumindest heilen können. Und lernen, wie wir über
diese Igelwellen kommunizieren. Und dann, in einem halben
Jahr oder so, wenn wir weit herumgekommen sind, können wir
uns ausdenken, ob wir da bleiben oder mit den neuen Erkenntnissen
und der neuen Kraft eine Revolution anzetteln."

Heelem kicherte. "So etwas in der Richtung habe ich mir
tatsächlich vorgestellt, als ich mich mit dem Angebot
verabschiedet habe, das auf den Tisch kacken beim nächsten
Besuch nachzuholen."

Lilið grinste, weil die anderen das mit dem auf den Tisch kacken einfach
alle übernommen hatten. "Und irgendwann wenn wir alt sind, sollte
der Fall eintreten, dass wir den Mist überleben, erwartet uns
eine freundliche Invasion Außerirdischer. Darauf freue ich mich!"

"Ich finde, es ist ein angenehm absurdes Gefühl, dass wir
bei einer Revolution von Außerirdischen unterbrochen
werden könnten und diese zwei sehr unterschiedlichen Dinge
dann einfach gleichzeitig dran sein müssen.", sagte
Marusch. "Das hört sich so angenehm albern an."

"Ich möchte außerdem auch noch ein bisschen leben.", fügte
Lajana hinzu. Es klang traurig und warm zugleich. "Nicht nur
geduldet sein, sondern mich ganz und richtig fühlen. Mit euch."

---

Schlimmen Autoritätspersonen gegenüber unmissverständlich
Missachtung zu zeigen, ist ein ermächtigender Zug. Einer, der
durchaus befreiend ist, weil er ermöglicht, sich von der akuten Situation
abzugrenzen. Aber solange die Probleme an sich da bleiben, solange Leute
in den Räumen, wo wir auch sein wollen, die Regeln vorgeben, die
uns nicht respektieren, wird es sich nie nach leben anfühlen.

---

Lilið glitt verrichteter Dinge am Himmel entlang über das weite
Meer. (Es geht hier also um die Abe.) Die Teeseufel mit
Drude und den anderen darauf war bereits fast am Horizont. Lilið
hatte noch einiges an Flug vor sich und war eigentlich schon
müde. Aber auch sehr zufrieden mit sich selbst: Lilið hatte
sich nicht getraut, auf deren Tisch zu kacken. Zwischen so
vielen hochkarätigen Magie-Futzis war das doch recht
riskant, wenn Drude nicht dabei war. Lilið hatte stattdessen
in deren Schuhe geschissen. Drachendreck empfanden diese
Leute vermutlich auch weniger schlimm, als hätte das andere
Lilið sich darin erleichtert. Aber es war ein Anfang. Lilið,
die Abe, hatte eine Revolution begonnen. Oder so.

Die Abe freute sich darauf, sich später selbstgefällig in
Drudes Schoß zu schmiegen. Das Schöne: Drude wäre stolz
auf Lilið, immer, einfach so. Drude würde nicht wissen, was
Lilið so zufrieden machte, aber es war auch nicht
wichtig. Es reichte, wenn Lilið sich selbst deswegen sehr
gefiel. Den anderen gefiel Lilið auch so, einfach weil.
