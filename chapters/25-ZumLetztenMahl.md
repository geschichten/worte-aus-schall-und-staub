Zum letzten Mahl
================

*CN: Schmerzen, Sanism, Messer, Blut, Gore/Splatter, Apathie, Mord
an mehreren Personen, Gift, Ratte, Panik vor Luftnot, Fesseln.*

Lilið war eigentlich ganz froh, dass sie dieses Gespräch nicht sofort
führten. Drude wollte an sich, aber war auch sehr müde. Sie hatte
im Gegensatz zu Lilið kaum geschlafen, weil sie den Moment hatte
abpassen wollen, in dem Lilið sich entfaltete. Sie hatte natürlich
auch Pflichten gehabt, die sie in dem Vorhaben eingeschränkt hatten.

Lilið machte es sich in einem hinteren Winkel des Raums auf einer
Decke gemütlich, die Drude ihr vorbeigebracht hatte, trank das
ebenso mitgebrachte Wasser aus dem Trinkschlauch in kleinen Schlucken
und hoffte irgendwie, dass es auch ein bisschen gegen den Hunger
helfen könnte. Das Drachenfutter rührte sie nicht an, auch wenn sie
den Neid nicht ganz verdrängen konnte, den sie fühlte, als sie der
Abe beim Fressen zusah. Die Mahlzeit der Abe bestand aus Nüssen und
rohem Fleisch. Beides röstete sie mit gezielten Flammen. Es sah
sehr genüsslich aus.

Dann kroch die Abe einfach zu Lilið unter die Decke und kuschelte
sich an. Lilið war wirklich noch nie so eine zahme Abe begegnet. Sie
sortierte sehr vorsichtig ihre eine Hand auf den Rücken des Tiers und kraulte
es dort. Die Abe gab ein sehr leises, wohliges Geräusch zwischen
Krähen und Glucksen von sich, das Lilið auch noch nie von einer
Abe gehört hatte, aber positiv bewertete, weil sich der kleine Drache
nicht wehrte.

Lilið war auch müde, aber zwei Dinge hielten sie noch vom Schlafen ab: Die
Schmerzen, die sie immer noch in ihrem ganzen Körper fühlte, auch
wenn sie inzwischen dumpfer geworden waren, und das Wissen, dass dies eine
verstreichende Gelegenheit war, sich darüber Gedanken zu machen, wie
sie sich in dieser Lage verhalten sollte und wie sie sie am erfolgreichsten
verbessern konnte.

Sie war also tatsächlich auf das Schiff mit der Kronprinzessin
gelangt. Sie hatte sie allerdings noch nicht gesehen, Drude könnte auch auf
ihre Vorlage gekonnt mit einer Lüge eingegangen sein, aber eigentlich
ging Lilið davon aus, dass wenigstens der Teil des Plans geklappt
hatte. Aktuell hatte sie die Wahl dazwischen, versteckt zu bleiben
oder sich eine geschickte Geschichte auszudenken, warum sie als
Nautika an Bord wäre. Von der zweiten Alternative müsste sie
Drude überzeugen, obwohl Drude sehr klar gemacht hatte, dass das
nicht zur Debatte stünde. Aber der Plan hatte noch einen weiteren
Haken, den Drude bereits erwähnt hatte: Die Crew hatte schon ein
Nautika beseitigt, sie würde ihr sehr genau auf die Finger
schauen. Lilið würde also ein sehr gefährliches Spiel spielen, wenn
sie versuchen würde, Maruschs und ihrem Plan nachzukommen. Selbst
wenn sie die Kagutte nicht zurück Richtung Nederoge steuern würde,
also weniger offensichtlich falsch navigieren würde. Vielleicht
müsste sie sich erst einmal Vertrauen erarbeiten und ein oder zwei
Tage sinnvoll navigieren, bevor sie etwas wagte, aber dann wären sie
aus der Region bestimmt schon raus, die sie mit Marusch abgesprochen
hatte. Vielleicht war es sogar wahrscheinlicher, dass die Kagutte sich in
der Gegend verführe, wenn kein Nautika sie leitete, was ja der
Fall war. Sie hatten ja kein Nautika!

Lilið grinste. Es war in jedem Falle besser, unbemerkt zu
bleiben. Vielleicht wäre ein guter Plan, zu versuchen, Informationen
über das Schiff zu sammeln, wo die Prinzessin darin versteckt
wäre und wie sie beim Navigieren ohne Nautika vorgingen, sich
dann irgendwann in der Nähe einer Insel vom Schiff zu stehlen
und Nachricht an die Königin zu versenden oder so. Wenn je
die Nachricht einer so niederen Person wie Lilið bei der Königin
ankommen würde.

Ob die Abe tatsächlich ein Postdrache war? Mit der eine Nachricht
verschickbar wäre? Bei den Gedanken strich sie besonders
weich zwischen den Flügeln hindurch über den Rücken des Drachens. Ob
sie nur auf Drude hörte oder ob auch Lilið sie von etwas überzeugen könnte?

Lilið merkte gar nicht, wie sie mitten in ihren Überlegungen zu schlafen
anfing und auf einmal selbst auf Drachen ritt. Ein alter Menscheheitstraum,
aber es gab keine Drachen, die groß genug waren. Oder auch nur willens. Das
aber kümmerte ihren Traum wenig.

---

Der Schlaf war nicht von langer Dauer. Der Raum hatte zwar kein Fenster,
aber zugige Ritzen, durch die nur sehr schwaches, graues Licht
fiel, als Drude die Tür vorsichtig aufschob und hineinlugte. Licht,
das darauf schließen ließ, dass der Tag gerade erst am anbrechen
war, noch wohl so eine Stunde vor Sonnenaufgang vielleicht. Die Abe
hopste sofort unter der Decke hervor und auf Drude zu, aber Drude
versuchte sie halbwegs erfolgreich abzuwimmeln. Sie trug ein großes
Tablett in den Händen, schloss die Tür mit dem nackten Fuß und kam
auf Lilið zu.

"Kannst du im Dunkeln sehen?", fragte Lilið.

"Nein, aber mich orientieren schon.", antwortete Drude. "Lil, mach
mal Licht an, bitte!"

Die Abe tat ihr den Gefallen, sich zur Deckenleuchte aufschwingend. Diese
schaukelte wieder im ersten Moment, aber dieses Mal war es Lilið nicht
so unangenehm. Die Augen erholten sich allmählich. Sie blickte hinab
auf das Tablett, wo die verschiedenen Schalen mit Früchten und Bällchen,
Soßen, Fladen und Gemüse wandernde Schatten warfen. "Wow, das ist
ziemlich krasses Essen.", sagte sie.

"Ein letztes Mahl.", antwortete Drude und setzte sich ihr gegenüber auf
dem Boden. Die Abe flatterte auf sie zu und hängte sich in einer alberig
verspielten Weise an Drudes Arm. Drude kam wahrscheinlich nur deshalb
nicht ins Straucheln, weil sie es gewohnt und außerdem sehr kräftig war.

"Meinst du so etwas wie Henkersmahlzeit?", fragte Lilið. "Wobei, wenn
du das meinst, mir deine Variante besser gefällt! Da steckt weniger Geschlecht
drin."

"Interessante Prioritäten." Drude wirkte irgendwie belustigt, obwohl
sie nicht einmal lächelte. "Aus meiner Erstsprache übersetzt heißt es
letztes Mahl oder letzte Mahlzeit, letztes Essen, so etwas. Ich glaube,
in deiner heißt es tatsächlich Henkersmahlzeit." Drude zog die Augenbrauen
etwas zusammen. "Henkendenmahlzeit?"

"Willst du mich also doch töten?", fragte Lilið, die Prioritäten nun
vielleicht mehr nach Drudes Erwartungen ausrichtend.

"Eigentlich mag ich gar nicht so gern töten. Das würde ich lieber
vermeiden." Drude riss sich ein Stück vom Fladen ab, griff sich
damit ein Bällchen und tunkte es in einen Dip. "Iss. Sonst weckst
du mit deinem Magen noch den Rest der Crew."

Lilið schluckte. Immer noch fühlte sie sich erstaunlich wenig
ängstlich. Aber sie fühlte sich auch nicht mehr so unreal wie
an den vergangenen Tagen. Vielleicht war sie einfach nicht so
der Angst-Typ. "Wirst du es trotzdem tun?", fragte sie. "Oder
es eine andere Person tun lassen?" Lilið fragte sich nur kurz,
ob die Lebensmittel vielleicht vergiftet sein könnten. Sie würde
sie jeweils vorsichtig probieren und hoffen, dass ihr der Geschmack
etwas verriete, aber eigentlich glaubte sie nicht, dass Drude es
mit Gift probieren würde. Zumal sie mitaß. Lilið tat ihr einfach
mit Fladen, Bällchen und Dip nach.

"Du bist in einer Lage, die du nur mit viel Glück überleben
wirst, selbst wenn ich dich nicht verrate. Es würde schon halb
an ein Wunder grenzen, wenn du über ein bis zwei Wochen nicht
entdeckt würdest, du musst zwischendurch aufs Klo wie gestern
vorm Einschlafen, all diese Dinge. Und eigentlich ist
mir das zu heikel, weil ich nicht bei allen in der Crew beliebt
bin und sie mir etwas antun könnten, wenn herauskäme, dass ich dir
helfe.", informierte sie. "Ich werde dich also in eine andere
Lage bringen, die du nur mit viel Glück überleben wirst. Aber
eine Lage, in der wir gemeinsam vielleicht einige andere Leben
retten können."

Das Bällchen schmeckte nicht nach irgendeinem Lilið bekannten
Gift, und außerdem ausgezeichnet. Sie hatte vielleicht noch nie
so etwas Gutes gegessen. Sie überkam ein merkwürdiges Gefühl dabei,
dass Drude tatsächlich meinte, dass dies vielleicht ihr letztes
Mahl sein könnte. "Hast du vor, mehr darüber zu erzählen, oder
mich", Lilið überlegte, ob sie den Witz wirklich machen sollte,
aber hielt sich dann doch nicht davon ab, "einfach ins kalte
Wasser schmeißen?"

Drude lachte tatsächlich leise. "Du bist nicht aus der Ruhe zu
kriegen. Das ist mir gestern schon aufgefallen.", sagte sie. "Mit
kaltem Wasser liegst du aber auch halbwegs richtig. Also: Es ist
ein Schiff gesichtet worden, das uns folgt."

Lilið hielt einen Moment im Abbeißen inne, aber versuchte dann,
unbeeindruckt weiter zu essen. Hieß das vielleicht, dass Marusch
erfolgreich ein Schiff hinter ihnen hergeschickt hätte? Zeitlich
konnte das passen. Dann wäre Marusch wirklich nicht das beseitigte
Nautika gewesen. Oder hätte vorher schon Gelegenheit gehabt, etwas in
die Gänge zu leiten.

"Eine Kriegskaterane aus dem Königreich Stern.", fuhr Drude
fort.

Lilið war nicht gewohnt, dass der Name Stern dazugesagt wurde, weil sie
ja im Königreich Stern lebte. Aber ihr fiel wieder auf, dass
sie den Namen 'Königin Stern' durchaus mochte.

"Wir werden sie hoffnungslos platt machen, wenn sie sich zu weit
nähert.", fügte Drude hinzu.

"Bist du sicher? Eine Kriegskaterane der Monarchie?" Lilið runzelte
die Stirn. "Einfach hoffnungslos platt?"

Drude nickte. "Wir haben die Prinzessin entführt. Wir haben
eine bestens bestückte Crew, was Offensive und Defensive angeht. Offiziell
steht König Sper nicht auf unserer Seite, aber er hat uns mehrere seiner
Wachen mitgegeben. Die natürlich hinterher nicht mehr offiziell in
seinem Dienst stehen dürfen, aber einen sehr erholsamen Abend fristen
werden, wenn die Mission erfolgreich ist.", erklärte Drude. "Eine
Flotte aus so vier bis fünf Schiffen könnte uns vielleicht etwas anhaben. Eine
einzelne Kriegskaterane? Definitiv nicht."

"Warum erzählst du mir das?", fragte Lilið.

"Weil ich dich auf die Kriegskaterane bringen
werde.", informierte Drude. "Bevor sie nah
genug dran ist. Und du musst sie davon überzeugen, dass sie sich nicht
nähern sollen. Erzähl ihnen, dass wir übermächtig sind. Erzähl ihnen, dass
wir trotzdem die Kronprinzessin nicht haben entführen können und dass
sie wahrscheinlich irgendwo anders untergetaucht wäre. Auf diese Weise
kannst du deren Leben retten."

Lilið fühlte nun doch ein leichtes Grauen, als sie sich Drudes
Idee ausmalte. Von einer feindlichen Kagutte kommend auf
einer Kriegskaterane der Monarchie vorzuschlagen, sie mögen
doch bitte umkehren, war schon eine Nummer. "Wieso denkst
du, dass ich lügen würde?", fragte sie.

"Es ist deine Sache. Ich wünschte, ich könnte dich zwingen.", erwiderte
Drude. Sie strich sich den Schwanz der Abe aus dem Ausschnitt
und nahm sich ein neues Bällchen in einen Brotschnipsel. "Und
das ist auch etwas, was mir am Plan nicht passt. Du bist
dann mit wertvollem Wissen von Bord. Aber ich sehe es pragmatisch. Entweder,
du schaffst es, sie anzulügen. Dann dreht die Kriegskaterane um, segelt
zurück, und sobald du Gelegenheit hättest, dein Wissen dann doch
einzubringen, wären wir zu weit entfernt, um eingeholt zu werden. Oder
du lügst sie nicht an. Dann greifen sie uns wahrscheinlich an und die
volle Besatzung der Kriegskaterane geht drauf, weil gewisse Leute hier an Bord keine
Überlebenden übrig lassen würden, die später von dem Überfall berichten
könnten. Du hast keine Möglichkeit, uns groß zu schaden. Du hast
nur eine Möglichkeit, größeren Schaden zu verhindern und ich
schätze dich so ein, dass du sie nutzt."

Lilið aß ein paar Antomantinen und blickte Drude dabei nachdenklich
an. "Es ist schon ein recht merkwürdiges Gefühl, dir so ausgeliefert
zu sein.", sagte sie. "Sagen wir, alles läuft nach deinem Plan: Ich
komme dort an Bord, lüge erfolgreich und sie drehen um. Dann, deiner
Bezeichnung dieser Köstlichkeiten als mein letztes Mahl nach zu
urteilen, gehst du immer noch davon aus, dass ich nicht überlebe."

Drude nickte. "Ich würde es dir anders wünschen.", sagte sie. "Ich
wünsche dir, dass du irgendwie gerissen genug bist, eine Geschichte
geschickt einzufädeln oder so etwas. Aber du wärest dann eben erst einmal
als gesetzeslose Person in den Händen der königlichen Wachen. Vielleicht
kämst du auch aus der Lage irgendwie raus, wenn du dich im richtigen Moment
faltest. Ich weiß nicht, wie sie Gefangene handhaben."

"Ich habe gehört, die Gefängnisse der Monarchie werden von Wachen
bewacht, die Magie anderer nicht nur aufspüren, sondern auch
unterdrücken können.", murmelte Lilið.

Drude nickte. "Aber solche sind sehr sicher nicht an Bord.", sagte
sie. "Dazu sind sie zu wertvoll und im Königreich Stern noch
zu selten. Die bewachen nur die Hochsicherheitsgefängnisse."

Die, wo der Blutige Master M eingekerkert würde, wenn er gefangen
genommen würde, dachte Lilið. Sie entschied sich, falls es
plötzlich eilig gehen musste und falls sie auf dem anderen Schiff
nichts zu essen bekommen würde, sich mehr auf Fladen und Bällchen
zu konzentrieren, die über längere Zeit satt machen würden als
die leckeren Antomantinen. "Wie hast du vor, mich zu zwingen?"

"Wenn du nicht in irgendetwas Praktisches zum Mitnehmen gefaltet
bist, wenn ich mit dir hier rausgehen will, werde ich der Crew
von dir erzählen.", informierte Drude sachlich.

Lilið lächelte und wusste selbst nicht genau, warum. Das wirkte
so vorhersehbar auf sie. Sie kaute den frisch in den Mund geschobenen
Bissen auf. Selbst die Fladen, die normalerweise nicht so furchtbar
spannend waren, schmeckten wundervoll. Leicht mehlig und kräuterig
und sehr weich. "Wie fühlst du dich dabei so, mich so fies zu
nötigen?", fragte sie.

"Nicht gut.", antwortete Drude. "Ich wünsche mir heimlich, dass du
irgendwann so etwas sagst wie: Ja, Drude, ich verstehe, dass auch du
keine Wahl hast und arrangiere mich gern mit der Situation, weil es
der beste von all den miesen Wegen ist."

Lilið fühlte sich an Marusch erinnert. Marusch hatte auch von
Pfaden geredet und davon, nach Wahrscheinlichkeiten einen
auszuwählen. Was würde sie an Drudes Stelle tun? Und was
würde sie tun, würde Drude ihr die Wahl lassen? "Du könntest
an meiner statt auf die Kriegskaterane gehen und denen Lügenmärchen
erzählen.", schlug Lilið vor.

"Muss ich dir erklären, warum das Unfug ist?", fragte Drude. "Also,
selbst wenn ich mein Leben gewaltig aufs Spiel zu setzen bereit
wäre?"

"Hm.", machte Lilið. "Ja, ich glaube schon, dass du das musst."

"Ich könnte nicht entfliehen, indem ich mich irgendwie falte. Ich
habe nur eine weitere Form.", erklärte sie. "Wenn ich es wirklich
dazu brächte, dort an Bord zu verhandeln, lassen sie mich nicht wieder
zurück zur Kagutte. Selbst wenn sie dann nicht angriffen, wäre
ich dann dort gefangen und quasi tot, und du ohne mich
hier. Was soll das bringen?"

Lilið nickte. "Das ergibt mehr Sinn, als ich vermutet hätte.", gab
sie zu. Die Bällchen waren fast alle. Lilið nahm sich nun eine
Traube, von denen sie noch nicht gegessen hatte. "Wie käme
ich an Bord?"

"Ich schwimme dich hin und schmeiße dich.", antwortete Drude. "Ich
würde dir so eine Form wie einen Fisch empfehlen, einen eher
stabilen. Dann kommen sie wahrscheinlich nicht auf die Idee, dass
du allgemein falten kannst, sondern dass du so wärest wie ich. Das
erhöht deine Fluchtchancen später."

"Ich kann als Fisch trotzdem nicht unter Wasser atmen.", erinnerte
Lilið.

"Damit habe ich auch nicht gerechnet. Dass ich mich entsprechend
verändern kann, liegt daran, dass ich in meinen ersten zehn Lebensjahren
quasi im Wasser gelebt habe. Das ist eine extrem seltene Fähigkeit." Drude
wirkte nicht stolz darauf.

"Das ist schon ziemlich cool!", sagte Lilið trotzdem. "Ich wünschte
manchmal, meine Magie wäre besser für Offensive oder Defensive
geeignet. Aber es ist bloß falten. Das ist nicht so mächtig. Ich
kann damit vielleicht brauchbar fliehen."

"Und Leute nachahmen.", fügte Drude hinzu und schnaubte. "Ich verhalte
mich in meinem Sinne sicher nicht vorteilhaft, wenn ich dir sage, dass
du deine Fähigkeiten nicht unterschätzen solltest, und dass es keineswegs
nur falten ist, was du tust."

Lilið runzelte skeptisch die Stirn. "Was denn noch?"

"Ich mag dich daran erinnern, dass du ein Würfel warst.", sagte
Drude. Sie zeigte zwischen Daumen und Zeigefinger die Größe an. "So
klein. Und zwar einer, der auch nicht irgendwie auffällig dadurch gewesen
wäre, dass eine Person ihn aufzuheben versucht hätte und dabei bemerkt
hätte, dass er so 80 bis 100 Kilogramm schwer ist. Ich kann nicht sehr
genau dein Gewicht einschätzen. Passt das?"

Lilið legte die Traube, die sie in der Hand hielt, vorsichtig wieder
ab. "Oh, daran habe ich gar nicht gedacht.", sagte sie.

Drude lachte wieder ein lautloses Lachen. "Nicht einmal dran gedacht
hast du.", sagte sie. "Aber dass es irgendwie vor sechzig Jahren oder so in
eurem Königreich diesen Ober-Magiegelehrten gab, auch so ein
sexistischer Kackmensch, der das erste Mal überhaupt festgestellt
hat, dass es möglich ist, durch tiefes Verständnis relativistischer
Magie die Masse von Materie zu verändern, und es auch heute nur
eine handvoll Menschen gibt, die das nach langem Studium beherrschen,
weißt du schon, oder?"

"Ich habe es gestern das erste Mal getan. Ich habe nicht darüber
nachgedacht.", verteidigte Lilið sich. "Aber an sich ja, von dem
habe ich gehört. Und habe eine ähnliche Meinung über ihn."

"Unterschätz nicht, was du damit tun kannst!", wiederholte
Drude. "Aber unterschätz das lieber wann anders nicht. Iss
noch drei Bissen, und dann sollten wir auch los."

---

Es war ein merkwürdiges Gefühl, sich in einen Fisch zu falten und
in der Form einfach in eine Jackentasche gesteckt zu werden. An der
Art, wie Drude sie anhob und wie sie nun mit ihrem Gewicht am Stoff
zog, bemerkte sie, dass das mit dem Gewicht nicht so gut geklappt
hatte wie bei ihrer Faltung zum Würfel. Sie fragte sich, um wieviel
sie zu schwer für eine Dornflunder war. Sie hatten sich auf eine
Dornflunder geeinigt, weil Dornflundern stabile Haut hatten, also
einen Aufprall auf einem Schiffsdeck ganz gut überstehen würden (wobei Liliðs
Haut nur so aussah wie die einer Dornflunder, aber ihre ganz eigene,
noch stabilere Konsistenz hatte), und weil die Dornflunder flach
war und mit ihren Flügeln die Flugbahn im Zweifel noch beeinflussen
konnte.

Drude ging mit ihr in der Tasche zügigen Schrittes
über das Deck und irgendwo, dem Gefühl nach, eine Treppe hinunter. Dabei
grüßte sie knapp einige Leute.

In der Kagutte gab es eine Tür, die speziell für Drude eingebaut
worden war. Das wunderte Lilið ein wenig, gerade weil Drude doch
erklärt hatte, dass sie nicht bei allen beliebt wäre. Aber sie
gehörte wohl doch so sehr dazu, dass am Schiff eine Anpassung für sie
vorgenommen worden war.

Die Tür lag unter Wasser und es bedurfte Magie, sie so zu öffnen, dass
kein Wasser hineinfloss. Drude beherrschte diese Magie.

Lilið bekam deshalb davon mit, weil Drude sie vor der Tür wieder
aus der Tasche herausgenommen und in einer Ecke auf dem Boden abgelegt
hatte, um sich selbst vollständig zu
entkleiden. Ihre Sachen verstaute sie in einem an der
Decke angebrachten Schrank, deren Klappe nach oben aufging. So
etwas kannte Lilið von Fragetten: überall war Stauraum mit
Klappen, die gut klemmten.

Drude verwandelte sich vor Liliðs Augen in eine fischigere
Form. Die Verwandlung sah wunderschön aus. Ihr Körper hatte
auch vorher schon muskulös ausgesehen, aber er formte sich in etwas
Schilleriges, Geschmeidiges um, zu dem die Muskeln fast
besser passten als zu der menschlicheren Form. Sie bekam
große Hände mit Schwimmflossen, die Füße wurden
zu etwas, womit sie nicht mehr gut stehen konnte, aber
zum Schwimmen waren sie sicher sehr gut geeignet. Drude
band sich einen Gürtel mit einer Tasche um, stopfte Lilið hinein
und schwang sich ins Meer. Die Tasche war wasserdicht, sodass
Lilið Luft zum Atmen hatte. Sie würde nicht lange reichen, aber
Drude hatte versprochen, auch nicht viel Zeit zu brauchen. Lilið
fühlte das Wasser an ihr vorbeiströmen, weil sich der Stoff
der Tasche auf ihre Haut legte. Sie verdrängte die Panik, die
aufkam, weil sie zusätzlich dazu, dass sie nicht viel Luft zur
Verfügung hätte, auch noch gefaltet war, also in einem
Zustand, in dem sie früher Probleme mit dem Atmen gehabt
hatte. Nun eigentlich nicht mehr, aber die Erinnerungen
wollten hochkommen.

Sie schob die Panik weg, indem sie darüber
nachdachte, was sie gleich sagen würde. Sie hatte
eigentlich nicht den Drang, darüber zu lügen, dass die Prinzessin
an Bord der Kagutte war. Vielleicht konnte sie aber mit der
Bedrohung, die die Kagutte darstellte, argumentieren, und die
Größenordnung von fünf Schiffen für einen späteren Angriff vorschlagen, die
Drude genannt hatte. Ob jemand auf sie hören würde? Ob Drude
ihr nur deshalb dazu geraten hatte, über die Prinzessin zu lügen,
weil das für deren Vorhaben von Vorteil wäre, aber sie eigentlich
viel bessere Chancen hätte, wenn sie einfach die volle Wahrheit
eröffnete?

Lilið hatte wenig Zeit darüber nachzudenken. Kaltes Wasser
strömte in die Tasche hinein, als Drude sie öffnete, sie herausnahm,
dann aus dem Wasser schnellte und sie im selben Zuge an Bord
warf. Lilið hatte kaum Zeit, ihren Schwindel zu realisieren. Irgendein
Reflex in ihr sprang an und korrigierte die Flugbahn. Aber als sie an
Deck aufschlug, löste sich die Faltung. Lilið realisierte, wenn sie
sie jetzt wieder korrigierte oder nicht zügig ganz entfaltete,
dann wäre sie wahrscheinlicher erkennbar als eine Person
mit Faltmagie. Also beeilte sie sich, rasch in ihre menschliche Form
zurückzufinden.

Sie rappelte sich auf und sah sich direkt einer Person gegenüber,
die sie mit erhobenen Augenbrauen ansah und der Kleidung nach
zu urteilen zur königlichen Wache gehören mochte. Lilið
schluckte.

"Na, hast du dich genug erholt, um mal loszulegen, was das soll?", fragte
die Wache.

Immerhin kein Sofortangriff, dachte Lilið. Allerdings wurde dieser rasch
nachgeholt. Irgendetwas schlang sich um ihre Arme wie eine Ranke und
zog diese nach hinten.

"Ein falscher Schritt, und es geht deinen Beinen genau so.", informierte
sie eine Stimme in ihrem Rücken.

Lilið verkniff sich die Frage, was denn zum Beispiel ein falscher Schritt
wäre. Sie atmete tief durch. "Ich bin Lilið von Lord Lurch, von Nederoge.", sagte
sie und entschied sich für die volle Wahrheit als Strategie. Oder, nein, das
war nicht sinnvoll. Etwas, was aber sehr dicht an der Wahrheit war. Sie
wollte Drude nicht ins Spiel bringen. Wer würde ihr auch noch glauben, wenn
sie erzählte, dass sie Hilfe bekommen hätte? "Ich habe mich an Bord der Kagutte
begeben, indem ich so getan habe, als wäre ich das Nautika, das sie suchten, um
die Kronprinzessin zu befreien, aber ich musste feststellen, dass die
Crew zu mächtig ist. Ich habe sichere Informationen darüber, dass eure
einzelne Katerane der Entführendencrew unterlegen wäre."

"Lilið von Lord Lurch also. Sagt mir zwar nichts, aber das lässt sich
vielleicht prüfen.", sagte die Wache und nickte der anderen Person
zu, die hinter ihr hervorgetreten war, nachdem die Fesseln
saßen. Jene hob die Hand zum Zeichen, dass sie
den Befehl verstanden hatte, und ging.

War sie hier irgendwie registriert? Gab es geeichte Urkunden aller
irgendwie angeadelter Leute an Bord einer Kriegskaterane? Das erschien
Lilið nicht sinnvoll.

"Erzähl mal, wie du darauf kommst, dass wir unterlegen wären!", forderte
die Wache sie auf.

Lilið drehte sich um, um zu sehen, wie weit die beiden Schiffe wohl
noch auseinanderlagen. Sollte sie als Glück oder Pech werten, dass sie
wohl noch Zeit hätte, sich bei Erklärungsversuchen zu verheddern?

Die Person, die sie gefesselt hatte, hatte fast einen Niedergang
erreicht, aber machte noch eine unscheinbare Bewegung, ehe
sie ganz verschwand, worauf die Stricke um ihre Arme sich
dichter zogen und ihr das Blut abdrückten. Das
war wohl ein gerade so geduldeter Schritt, schloss Lilið und
drehte sich wieder zurück. "Ich habe gehört, wie die Crew der Kagutte
damit geprahlt hat, dass die Kriegskaterane für sie ein Kinderspiel
wäre." Wiederum beschloss Lilið, dass dicht an der Wahrheit ein guter
Weg wäre. "Sie haben leider nicht viel über Details geredet, aber ich
bin sehr überzeugt davon, dass sie nicht übertrieben haben. Dazu
war die Stimmung nicht locker genug. Sie meinten noch, dass mit
einer Flotte aus fünf Schiffen vielleicht eine Chance gegen sie
bestünde, aber nicht so. Ich habe nicht weiter nachgefragt, weil ich
keinen Verdacht erregen, sondern lieber früher hier sein
wollte, um euch zu warnen. Mir liegt sehr daran, dass die
Rettung der Prinzessin gelingt."

"Hmhm. Wenn du lügst, kannst du gut lügen, das muss
ich dir lassen.", sagte sie. Sie wirkte nachdenklich und winkte
eine weitere Person herbei, die gerade in Liliðs Sichtfeld getreten
war.

Der Kleidung nach zu urteilen, mit diesem dreieckigen Hut,
tippte Lilið auf die Kapitänsperson. "Ein Nautika." Die Stimme
war schwer und sonor.

Lilið nickte. Sie trug ja den Mantel, der sie als eines kennzeichnete.

"Er behauptet, Lilið von Lord Lurch zu sein.", informierte die
Wache, die sie zu verhören begonnen hatte. "Aber da kommt Luzinde
wieder. Glaubst du, du kannst es nachprüfen?"

Die Person, die vorhin weggegangen war, hatte nun ein Büchlein
dabei, sowie ein kleines Messer, das in der aufgehenden Sonne blitzte. Lilið spürte,
wie sich ihre Fesseln wieder lösten, wie das Blut wieder in ihre Arme
hineinströmte und sie kribbelten. Luzinde griff nach ihrer Hand und
zog das Messer auf der Daumenseite seitlich durch ihr Handgelenk. Nicht
tief. Luzindes Finger strichen über das Blut, das Gesicht
entspannte sich und die Augen schlossen sich. Nur Momente später öffneten sie sich
wieder, weiteten sich vor Schreck. Luzinde trat einen Schritt zurück. "Der
Blutige Master M.", informierte sie. "Lilið von Lord Lurch ist nicht
registriert, sie ist weder adelig genug, noch als Nautika bekannt
genug. Ich kann keine Aussagen darüber machen, ob es sich um Lilið von
Lord Lurch handelt, aber das Blut stimmt mit dem des Blutigen Master
M überein."

"Ich bin erst sehr frisch Nautika und meine Mutter ist Köchin.", erklärte
Lilið. Wieder fühlte sie die Fesseln ihren Körper umschlingen, dieses
Mal an Armen und Beinen.

"Das ist ja mal eine Geschichte.", sagte die Wache, die ihr bisher
die Fragen gestellt hatte. "Der meist gesuchte Dieb der Monarchie
kommt von einem Schiff, das die Kronprinzessin gestohlen hat, um mal
bei dem Vokabular deinesgleichen zu bleiben, und rät uns davon ab,
eine harmlose Kagutte anzugreifen, indem er behauptet, wir, die
königliche Wache, wären unterlegen. Zu dem Zweck gibt er sich als
Adel und Nautika zugleich aus, aber möglichst so, dass wir es
nicht nachweisen können. Da hat es sich ja doch als gut
herausgestellt, ein Imagika dabei zu haben, der über alle derzeit
bekannten Kriminellen bestens im Bilde ist. Und nun können
wir sogar ein Fandungsbild erstellen, solltest du versuchen, zu
entkommen, was sehr aussichtslos ist."

Lilið versuchte, statt zu entkommen, möglichst schnell eine neue
Strategie zu finden. Sie atmte tief durch. "Ich verstehe, wie
unwahrscheinlich nun rüberkommen muss, dass ich die Wahrheit
spreche.", sagte sie. "Aber möchtet ihr riskieren, in den Tod
zu gehen, wenn ihr stattdessen irgendwelche wenigstens etwas sichereren
Strategien fahren könntet? Ich habe keine Ahnung, nachts angreifen,
ein Beiboot nach Hilfe schicken, irgendetwas in der Art? Ich habe wirklich
ein Interesse, dass ihr angreift, aber auch eines, das wir dabei nicht
alle sterben."

"Lass das meine Sorge sein.", brummte die Kapitänsperson. "Wir
haben auch eine gewisse Verpflichtung, dem Befehl der Königin
Folge zu leisten. Aber ich verspreche dir: Wir werden
alles in diesem Spielraum tun, um uns möglichst wenig
zu gefährden."

Ohne für Lilið erkennbaren Übergang wandelte sich die noch
verhältnismäßig ruhige Atmosphäre von eben zu einer
betriebssamen. Zwei weitere Wachen bekamen den Befehl,
sie in den Rumpf in eine Gefängniszelle zu bringen, andere
wurden für Strategieplanung herbeigerufen. Niemand
sprach mehr mit Lilið, niemand hörte auf ihr Rufen. Erst jetzt fiel
ihr ein, dass es vielleicht hilfreich sein könnte, zu fragen, ob
Marusch hier irgendwo war. Aber die Wache vor der Zelle, die
bei ihr blieb, erbarmte sich erst nach einer
Weile auf ihr Nachbohren hin zu fragen, wer das
denn bitte sein sollte, wahrscheinlich
in der Hoffnung, dass sie dann Ruhe gäbe. Lilið
beschrieb Marusch, aber die Wache hörte ihr nicht weiter zu.

Sie hatte schon lange aufgegeben, mit ihren gefesselten Beinen zu
stehen. Sie lag in Embryohaltung in Rankenseil gewickelt in der Zelle, gab endlich
auf, die Wache mit Fragen und Bitten anzuschreien, und
machte sich Gedanken, was sie nun tun sollte. Gefesselt war falten nicht so
einfach, aber auch nicht unmöglich. Eigentlich wollte sie nicht zu früh zeigen, dass sie
es konnte. Zumal, was sollte sie dann tun? Würde ihr dann jemand
glauben schenken, wenn sie plötzlich wieder befreit an Deck
stünde? Oder würde sie direkt getötet werden? Ein Wunder,
dass das noch nicht getan war, als sich offenbart hatte, dass sie
der Blutige Master M war. Aber wahrscheinlich wollten sie noch
versuchen, aus ihr herauszubekommen, wo das Buch wäre oder
wie sie es gestohlen hätte.

Sie merkte, wie sie wieder in diesen Zustand überglitt, in dem
sich alles unreal anfühlte. Sie versuchte, gegen die engen Fesseln
tief durchzuatmen, und sich doch zu falten. Sie beschloss,
in so etwas wie eine Schiffsratte gefaltet aus der Zelle zu fliehen und zu
schauen, ob sie irgendwo Marusch fand. Wenn es ein Schiff der
königlichen Flotte war, das unterwegs war, die Prinzessin zu
retten, dann war doch nicht unwahrscheinlich, dass Marusch sich
unter irgendeinem Vorwand an Bord geschummelt hatte.

Ihr erster Faltversuch scheiterte, weil die gesamte Kriegskaterane
erzitterte. Lilið bemerkte erst jetzt, dass sie Schreie wahrnahm, eher
Kommandos als panische Schreie, aber so harsche Kommandos, dass
es bedeuten musste, dass es wahrscheinlich längst zu spät war. Wieder erzitterte die
Kriegskaterane und schlingerte, krängte so sehr, wie
große Schiffe das nie tun sollten. Lilið wurde gegen die
Stäbe der Zelle gedrückt. Die Wache rappelte sich hoch, rief
ihr zu, sie solle sich nicht rühren, und rannte die Treppe hinauf.

Lilið wusste nicht, wie sie es schaffte, sich in diesem Zustand
zu falten. Sie ahnte, was da oben vor sich ging. Nicht im Detail,
aber dass es nicht gut für die Crew und die Kriegskaterane aussah,
wusste sie.

Und dass es zu spät war. Warum tat sie überhaupt noch etwas? Aber
nun, da niemand hinsah, faltete sie sich in eine Schlange, weil die
Faltung gefesselt leichter umzusetzen war als die in eine Ratte, und wand
sich aus den Fesseln hinaus. Es war ein witziges Gefühl, sich
auf dem Boden entlang zu schlängeln. Sie war ja nicht plötzlich
eine Schlange, sie sah nur wie eine aus. Sie stieß sich den Kopf
zweimal beim Versuch, zwischen den Stäben hindurchzukriechen,
weil das Schiff so schlingerte. Als sie es schaffte, auf der
anderen Seite der Stäbe anzukommen, entfaltete
sie sich wieder. Alles in ihr war dumpf. Sie hatte den Eindruck,
durch ihre Augen alles dunkler zu sehen, als es eigentlich
war. Ihr Blutdruck fühlte sich vollkommen falsch an. Zu niedrig,
vermutete sie.

Sie stolperte auch mehrfach, als sie sich der Treppe näherte, obwohl
ihr Körper wieder der gewohnte war. Die Bewegung fühlte sich
alberner Weise immer noch so an, als wäre sie gefaltet.

Wasser strömte durch irgendeine Tür in der Umgebung in
die Kriegskaterane und machte schöne, dunkle Strömungsmuster
um ihre Füße. Lilið sollte schleunigst nach oben kommen, wenn
sie nicht samt Katerane untergehen wollte. Wobei sie auch
dann nicht überleben würde, wenn
sie Drudes Vorwarnung glaubte. Sie trat die knackenden Treppen
hinauf, erst ins Mitteldeck, dann den langen Gang durch die
Messe entlang, wo alle Möbel durch die Gegend geflogen
waren, ob sie vorher fixiert gewesen waren oder nicht, zur zweiten Treppe
aufs Oberdeck. Vom Oberdeck war fast nichts mehr in der Form, in
der sie es vor wohl höchstens einer halben Stunde zurückgelassen
hatte. Überall Splitter, feine und große
Stücke, von Holz und von Menschen. Die Kämpfenden auf der Kagutte
waren nicht wenig makaber, stellte Lilið fest. Der Körper der
Kapitänsperson lag in gleichmäßig große Stücke
geschnitten, wie so eine zubereitete Speise, an Deck vor ihren
Füßen. Lilið fragte sich, was mit ihren Assoziationen kaputt war, oder mit
ihren Gedanken allgemein, weil das, was sie an diesem Bild
am meisten gefangen hielt, das eine Stück des Kopfes war, das
breiter als alle anderen Streifen war. Nur der Kiefer war vom heilen
Rest des Kopfes getrennt.

Lilið blickte wieder auf, in der Hoffnung, dass sich Bilder nicht
zu sehr einprägen würden. Aber irgendetwas an dieser Situation hatte auch
etwas Schönes. Wieder fragte Lilið sich, was kaputt mit ihr war, weil
sie das dachte. Sie wurde sich ihres schweren Mantels sehr bewusst,
der sich im Wind und der Wucht von Explosionen aufbauschte, fühlte
die Gischt des Wassers auf ihrem Gesicht und ihren Händen, während
um sie herum die Kriegskagutte zu Kleinholz zersprang und
ins Meer versank. Nur Feuer hätte dem Bild noch gefehlt. Warum war
da kein Feuer? Wo war Marusch, wenn sie sie brauchte?

Und dann verlor sie das letzte Stück Boden unter den Füßen.

---

Alles fühlte sich viel zu weit weg an, um nun daran zu denken, sich zu falten. Lilið
hätte auch nicht gewusst in was. In Holz wahrscheinlich. Aber das kleine
Ruderboot, das die Kagutte losgeschickt hatte, um sich um die Überlebenden
zu kümmern, hatte sie längst gesichtet. Lilið hatte keine Lust, etwas dagegen
zu tun, dass sie sie kriegen würden. Ihre Fluchtinstinkte waren so tot wie
alles in ihr drin.

Sie sah den zwei Menschen im Ruderboot direkt in ihre Gesichter, starr
und mit dem verzehrenden Wunsch, dass sie mit ihrem Blick starken Schmerz
zufügen könnte, als sie auf sie zu ruderten.

"Du bist Nautika?", fragte die eine der beiden Personen.

Das schlossen sie wohl aus dem Mantel, der sie einhüllte und umgab. Der eigentlich nicht
ihrer war. Lilið nickte trotzdem. Wenn sie sterben würde, dann war es auch nicht verkehrt,
es noch einmal so deutlich zu bejahen, wie ihr Körper das gerade zuließ: Sie
hatte ihren Traum erreicht.

"Wir nehmen dich mit."
