Einbruch bei Dunkelheit
=======================

*CN: Sex, Erotik, Vergiftung als Thema, politische Morde.*

Die feuchte Nachtluft strich kühl und angenehm über Liliðs
Gesicht. Der Anzug, so gern sie ihn hatte, war ihr zu
warm. Vielleicht hätte sie die Anzugjacke ausziehen und über
ihren Arm hängen sollen, aber dazu hätte sie die Berührung
zu Maruschs Hand auflösen müssen.

Der Weg war zwischendurch schmaler geworden, weshalb sie näher
zusammengerückt waren, und sie hatten die Berührung ihrer Schultern
nicht aufgelöst, als er sich wieder verbreitert hatte.

"Wo schlafen wir?", fragte Lilið. Es war das erste, was sie
nach einer ganzen Weile hervorbrachte, die sie nur auf den
Weg, die Berührungen und ihre Gefühle geachtet hatte, die sich
physisch wie Adrenalin durch ihren Körper bewegten, nur weniger
warnend und heißer.

"Ich war gedanklich noch beim Ob." Maruschs Hand drehte sich
und ihre Fingerspitzen berührten Liliðs Handrücken. "Wir
wollten noch Dinge abklären. Und wir sollten schlafen. Aber
können wir das, bevor wir", Marusch brach ab und strich
extrem sanft Liliðs Hand und Arm hinauf in ihren Ärmel.

Liliðs Atem verhedderte sich. Als er wieder einen halbwegs
sinnvollen Rhythmus gefunden hatte, hatte Maruschs Hand ihren
Weg wieder zurückgefunden und verweilte sachte, den Handrücken
gegen Liliðs geschmiegt. "Du möchtest mich ausziehen?", interpretierte Lilið,
versuchte dabei einen verschmitzten Stimmausdruck, der
aber nicht ganz gelang.

"Schon, ja." Maruschs Kopf wandte sich ihr zu. Die Gesichtszüge
wurden nur vom blassen Leuchten der Wolken beschienen, aber
sie waren nah genug für Lilið um eine Idee davon zu bekommen.

Lilið erwiderte das zarte, verspielte Lächeln. "Aber?"

"Ich bräuchte dafür deine Erlaubnis.", sagte Marusch. "Ohne, dass
ich von dir weiß, dass du das willst, tue ich nichts dergleichen."

Auch wenn sie nichts anderes von Marusch erwartet hätte, fühlte
sich diese Zusage erleichternd an. "Hätte ich deine, dich ausziehen
zu dürfen?", fragte sie. Dieses mal gelang es ihr, es frech zu tun.

Marusch grinste etwas mehr. "Ja, hast du."

Liliðs verhakte ihren Blick mit Maruschs, wanderte mit ihrer
Hand um Maruschs herum, bis sie auf der anderen Seite
auf ihrem Rock ankam und dagegen drückte. Sie spürte
Maruschs Oberschenkel unter den dünnen Lagen Stoff.

Maruschs Mund zuckte. Ihr Atem wurde unruhiger, dann kontrollierte
sie ihn wieder. Lilið konnte ihr ansehen, dass sie es anstrengte, sie
vielleicht mit Fassung rang.

Lilið strich mit den Fingern über den Oberschenkel in einer
Weise, dass sie sich eine Rockfalte erzeugte, an der sie den
Stoff dicht am Bein entlang hinaufzog, bis der Saum ihre
Finger erreichte. Sie schob sie darunter und erfühlte Maruschs
nackte Haut. Es hatte sich eine feine Gänsehaut darauf gebildet,
nur ein wenig.

Marusch lehnte sich in die Berührung und auf diese Art noch
weiter an Lilið heran. "Möchtest du es mitten auf dem Feldweg
tun?", raunte sie.

Lilið war sich nicht sicher, ob Marusch eine genauere Vorstellung
von diesem 'es' hatte als sie. Sie hatte keine genaue. Aber
bisher gefiel ihr es gut. "Ein weiches Bett wäre mir natürlich
lieber.", raunte sie zurück.

"Das ließe sich vielleicht einrichten." Maruschs Stimme hatte
wieder einen etwas selbstsichereren, verschmitzten Ton wiedergefunden,
der nicht weniger warm war.

"Hast du doch so etwas wie eine Behausung in der Gegend?", fragte Lilið.

Ein kleiner Teil in ihr lenkte sie kurz mit dem Gedanken ab, dass
sie doch auf der Suche nach dem Blutigen Master M war und ob sie
überhaupt die Zeit hatte, sich Marusch auf diese Art hinzugeben. Oder sie sich
ihr hingeben zu lassen. Aber ein anderer verdrängte diesen sofort: Sie
hatten sich gefunden. Auf ein paar Stunden käme es nicht an. Und
Lilið wollte es sich nicht nehmen lassen.

"Ich nicht.", widersprach Marusch. "Aber Lord und Lady Piks zweitgrößter
Wohnsitz sollte nicht weit sein. Und Lord und Lady Pik richten große Teile
des Balls aus. Sie werden also recht wahrscheinlich bis morgen Mittag irgendwann
außer Haus sein, zusammen mit einem Großteil ihrer Wachen."

"Meinst du, sie lassen ihr Gut unbewacht?", fragte Lilið.

"Nicht komplett. Aber die Wachen, die zurückgelassen wurden, werden sich
auf deren Haupthäuser konzentrieren, eben darauf, wo es Wertvolleres
zu bewachen gibt. Besuchshäuser am Rande des Grundstücks werden vermutlich
weniger bewacht sein.", legte Marusch dar.

"Riskant.", kommentierte Lilið. Ihre Hand verweilte ruhig an Maruschs
Oberschenkel, zumindest so ruhig das ging, weil er sich ja beim Gehen
bewegte. Sie waren langsamer geworden, damit sie das koordiniert
bekamen. Langsam und mit angespannten Muskeln, um eventuelles
Stolpern abzufangen, weil sie nicht einmal auf den
Weg sahen, der zum Glück schnurgerade aus verlief.

"Ist es dir zu riskant? Möchtest du lieber das Feld?", fragte Marusch.

"Ich überdenke das noch.", widersprach Lilið. "Was kann passieren? Was
riskieren wir?"

"Wir wollen nur in einem Bett schlafen.", antwortete Marusch. "Das
ist kein Diebstahl. Da hält sich Härte der Konsequenzen vielleicht
in Grenzen. Es sei denn, sie finden bei uns gestohlene Schätze
oder erkennen uns als gesuchte Langfinger oder Schlimmeres."

"Trägst du Diebesgut bei dir?", fragte Lilið.

"Außer dir? Nein.", sagte Marusch schnippisch, und fügte
vielleicht beschwichtigend
hinzu: "Du bist zumindest diebisch und gut und bist bei
mir, deshalb passt die Beschreibung vielleicht. Du
gehörst mir natürlich nicht. Und du?"

"Ich habe ein paar Marken, die aber eher unwahrscheinlich zu Lord
und Lady Pik gehören. Und einen Ehering von einem Skoremetrika,
der sicher keine Verbindung zu Lord und Lady Pik hat." Lilið
wusste, dass Herr Hut mal zur nederoger Inselvereinigung gehört
hatte und nun vielleicht zu den Herrschenden auf Frankeroge, aber
vielleicht war er auch sozusagen entliehen.

"Und ein Buch.", fügte Marusch hinzu.

Lilið hatte es nicht vergessen. Sie fragte sich, warum sie
es in der Aufzählung ausgespart hatte. Es war ihr nicht klar. "Wieso gehst
du davon aus, dass es gestohlen ist?", fragte sie.

"Du trägst es in einer Jacke bei dir, die du nur ungern
aus den Augen lässt.", erklärte Marusch. "Du trägst auch verhältnismäßig wenig
Gepäck bei dir. Die Tasche, in der du es dabei hast, wirkt
nicht wie eine, die du dir absichtlich daheim ausgewählt hast,
um vermeintlich eine Reise zum Internat für skorsche Damen
auf Frankeroge zu besuchen." Marusch musterte
Lilið, während sie es aussprach, abermals aufmerksam. "Entweder ist nicht alles
glatt gelaufen oder du hast irgendwo eine Basis. In jedem
Falle bedeutet deine Entscheidung, das Buch dabei zu haben,
dass es wertvoll für dich ist."

"Es hat Tagebuchcharakter für mich.", erklärte sie, was nicht einmal
extrem weit von der Wahrheit entfernt war. Sie fand es interessant.
Vielleicht würde sie irgendwann hineinschreiben. Oder es hatte
doch eine Kodierung. "Und, sagen wir, ich bin etwas unfreiwillig
frühzeitig von Bord gegangen. Mit einer kleinen Dosis Lärchenwurz
intus." Sie hoffte, dass dieser Themenwechsel Marusch vom Buch
ausreichend ablenken würde.

Marusch sah angenehm entsetzt aus. Allerdings für einen zu
kurzen Moment, fand Lilið. "Ich werde, sollte sich eine ähnliche
Situation wiederholen, mehr Vorkehrungen treffen.", versprach
sie. "Möchtest du Details erzählen?"

"Auf dem Weg zu einem Bett?", fragte Lilið. Sie löste ihre Hand von
Maruschs Oberschenkel und fühlte den Wind, den die Röcke verursachten,
als sie Maruschs Beine wieder verhüllten, sowie den seidigen Stoff
auf dem Handgelenk entlangstreichen.

Maruschs Atem stolperte, aber fing sich rasch wieder. "Ist das
ein Entschluss, es zu riskieren?"

Lilið nickte. Sie strich Maruschs Rock glatt und ging dann wieder
in die Haltung über, in der sich ihre Handrücken berührten. Und
dann, kaum war ihr bewusst geworden, was sie da entschieden
hatte, fühlte sie bohrend, dass sie für eine Nacht mit
Marusch in einem Bett doch eine Menge riskierte. Marusch hatte
fairerweise dargelegt, moralisch kein einwandfreier Mensch zu
sein. Was sie automatisch alle beide nicht waren, wenn sie
in ein fremdes Besuchshaus zum Übernachten eindrangen. Aber
wer wusste, was Marusch noch ohne mit der Wimper zu zucken tun
würde? Oder bei was Marusch zwar schon mit der Wimper zucken,
aber es trotzdem riskieren würde?

"Was ist passiert?", fragte Marusch sanft.

Lilið berichtete. Sie tat es nicht allzu ausschweifend,
aber sie ließ nichts aus, weder Tomden, noch Barb.

Marusch blieb noch eine Weile still, nachdem sie geendet hatte. Sie
hatte schön zugehört, fand Lilið. Obwohl Lilið wenig Emotion in
ihre Stimme geflochten hatte, hatte Marusch verstanden, wann es
ernst gewesen war, und wann eklig. Ihr Gesicht hatte einen angewiderten
Ausdruck gehabt und Sorge gespiegelt, als Lilið von Tomdens
Aufdringlichkeit erzählt hatte.

"Schleseroge.", sagte Marusch leise. "Klingt ein bisschen unrealistisch. Als
könnte der Teil eher noch zu deinem Traum gehören."

"Die Fieberträume waren eher unangenehm.", sagte Lilið zweifelnd. Sie
kramte in der Tasche, die sie mit dem Anzug zusammen bekommen hatte, nach
dem Beutel mit dem übrigen Brot und dem einen Stück Obst. Viel war
es nicht mehr, aber immerhin hatte sie auf dem Ball zwischen ihrem
zweiten Übungstanz und der
Aufführung einiges an Häppchen gegessen. Sie reichte Marusch das
zusammengebundene Tuch.

Marusch nahm es entgegen und begutachtete es. Sie roch am
Brot. "Gutes Brot.", kommentierte sie und verpackte alles wieder,
bevor sie es Lilið zurückreichte. "Es ist ein sehr klassisches, gewachstes
Tuch, das es fast überall weltweit auf Märkten gibt, wenn Leute
Lebensmittel abholen und Taschen vergessen haben. Ich glaube dir
selbstverständlich. Aber erlaube mir trotzdem die Frage: Hältst
du es für ausgeschlossen, dass du irgendwann an der Angelsoger
Küste angeschwemmt worden bist, dir vom Markt etwas gestohlen
hast, und dann erst wieder klarer hast denken können? Und dein
Gehirn hat sich aus den Träumen etwas anderes zusammen rekonstruiert?"

Lilið runzelte die Stirn und versuchte sich, diese Möglichkeit
vorzustellen. War sie überhaupt an einem Markt vorbeigekommen? Durch
ihre Erinnerungen blitzten Bilder, Gerüche und Gedränge von
Marktgetümmel. Aber die  Erinnerung konnte auch viel älter sein.

Marusch hatte recht, dass diese Insel mit ihrem Gefüge nicht sehr
realistisch wirkte. Wie eine Utopie. Und dass Liliðs Erinnerung
daran unscharf war, traf auch zu. Sie hatte es auf das Restgift, die
Erschöpfung und all das danach Erlebte geschoben, dass sie bereits
verblasst war. "Ich werde irgendwann herausfinden, ob es Schleseroge
gibt, wenn ich Nautika werde.", sagte sie. Und auch das war ein
Element, das die Skepsis bestärkte, die Marusch ins Spiel gebracht hatte: Leute,
die auf einer Insel wohnten, die angeblich nur in wenigen Karten
verzeichnet war. Davon hatte Lilið auch noch nie gehört. "Es ist
nicht völlig ausgeschlossen.", beantwortete sie Maruschs Frage. "Aber
eigentlich halte ich für unwahrscheinlich, dass mein Kopf so viel
und so überzeugend korrigiert hat."

Marusch nickte und lächelte sanft. "Ich glaube dir.", betonte
sie. "Ich möchte das. Ich wünsche mir, dass diese Utopie irgendwo
im Kleinen existiert. Und vielleicht irgendwann auch im Großen."

"Wo wir bei Utopien sind: Hat es für dich was mit einer Utopie zu
tun, eine potenzielle Mörderin den Weg zu ebnen, ein Internat
zu besuchen?", fragte Lilið.

"Tatsächlich ja!", antwortete Marusch. Sie wirkte überraschend
ernst, vielleicht sogar etwas streng dabei. "Ich finde, Bildung sollte
ein Grundrecht sein."

Lilið blieb stehen. Nicht nur, weil die Zufahrt zum Gut
nun unverkennbar in Sicht war, von dem Lilið hoffte, dass es zu besagtem
Zweitwohnsitz von Lord und Lady Pik gehörte, weil ihre
Beine allmählich ermüdeten. Sie konnte noch, aber hätten
sie zu einem anderen Gut gemusst, dann hätte es noch einmal
einiges an Strecke bedeutet.

Marusch lenkte sie seitlich am Gut vorbei und drängte sie dazu,
wieder zu gehen. "Wir sollten nicht zu lange hier verweilen.", sagte
sie. "Es gibt sicher einen Nebeneingang, von wo wir auf das Gelände
gehen können."

Lilið sah ein, dass es besser wäre, nicht im Sichtfeld
der Hauptzufahrt stehen zu bleiben, aber als sie einen finsteren
Winkel der Außenhecke erreichten, an den sie der
Graspfad an einem Bach entlang führte, hielt sie doch
wieder an. "Sie wollte mich umbrigen!", raunte sie,
arktikulierte wütend.

"Grundrechte sind Dinge, die allen zukommen, egal welche Absichten
sie haben.", erinnerte Marusch sie.

Würde das ein Streit werden? "Aber du hast ihr geholfen! Wir
haben das getan, aber du wissentlich!"

Marusch seufzte und schob sich neben sie in
den noch dunkleren Schatten, ohne ihr näher zu kommen. "Allils
Leben ist dauernd bedroht. Und weißt du von wem?"

Lilið wusste nicht, worauf Marusch hinaus wollte,
aber startete einen Versuch, die Frage zu beantworten. "Von", unterbrach
sich kurz und führte die unangenehme Wahrheit aus, die
ihr zuerst einfiel, "Leuten, die sie zur Rechenschaft ziehen würden. Ich
bin auch nicht gerade begeistert vom System."

"Ja, genau. Von Leuten, die mehr an der Spitze von Macht stehen.", konkretisierte
Marusch. "Leuten, die oft genug nicht einmal selbst töten, sondern töten
lassen. Die Schutzbefohlene haben, die für sie diese Arbeit und andere
verrichten. Also, wenn du mich fragst, warum ich einer Mörderin
zu Bildung verhelfen würde, finde ich auch die Frage relevant,
warum besagten mächtigeren Leuten, ohne dass es viel Aufruhr
bedeutet, geholfen wird, ein todbringendes System aufrecht
zu erhalten. Leuten, die auch ganz sicher keine
Probleme haben zu irgendwelchen Schulen zu gehen, wenn sie
wollen."

"Wenn du auf mich anspielst, weil ich hätte zu dieser Schule gehen
können, bedenke, dass ich dich hätte verraten können und
es nicht getan habe." Lilið verstand den generellen Ärger, den
Marusch ansprach, den hatte sie auch. Dass Mächtige dieses
System ohne Konsequenzen aufrecht erhielten, störte sie. Aber
sie fand, dass sie ein Recht hatte, beides schlimm zu finden, und
nur, weil über das eine Problem zu wenig geredet wurde, das
andere ansprechen durfte, wenn sie beinahe Opfer davon gewesen
wäre.

"Hättest du.", räumte Marusch ein. "Bis zu diesem Gespräch
bin ich übrigens davon ausgegangen, dass du mich hast vergiften
wollen."

Lilið schluckte. Sie hatte Marusch tatsächlich vergiften wollen. Nicht
töten zwar, aber das wusste Marusch ja nicht. "Das Gift hätte
dein Gedärm etwas durcheinander gebracht.", erklärte sie. "Ich
hatte mir ausgerechnet, mich im Zweifel besser gegen dich
wehren zu können, wenn du geschwächt bist, oder dich eher vom
Gelände komplimentieren zu können, wenn du dringende, körperliche
Bedürfnisse hast, die versprechen, schlimmer zu werden."

Marusch nickte. "Es tut mir leid, dass ich dich falsch eingeschätzt
habe. In der Diebesszene rechne ich wahrscheinlich stets vorsichtshalber
mit Schlimmerem.", sagte sie. "Vielleicht habe ich deshalb auch
ein bisschen zu unvorsichtig eingeschätzt, dass du schon mit einer
Giftmischerin zurecht kommen würdest. Trotzdem ist das
weder der Grund, warum ich dir eine Giftmischerin
vermittelt habe noch rede ich von dir. Ich rede von
Leuten wie dem König. Oder Lord Lurch, wenn du einen
kleineren, freundlich wirkenden Lord genannt haben willst, den
du wahrscheinlich kennst."

Lilið verzichtete darauf, Marusch zu informieren, dass Lord
Lurch ihr Vater war. Wahrscheinlich wusste sie es ohnehin, oder
hielt es zumindest für wahrscheinlich. "Ich frage mich,
ob, dass es ein System gibt, in dem Königliche, Lords und
Ladys, -- und wer weiß, ob es auch Herrschaften gibt, für die diese
binären Begriffe nicht passen --, sich daneben
benehmen und morden, rechtfertigt, dass
Leute, die ganz unten stehen, morden, wenn es nicht gerade
Notwehr ist. So als Vorsichtsmaßnahme gegen eine Person, die
eigentlich helfen möchte."

Maruschs Gesicht, dass Lilið nur deshalb gerade so noch
in der Dunkelheit der Schatten ausmachen konnte, weil es ihr
wegen des Flüsterns wieder ausreichend herangenaht
war, wirkte mit einem Mal wieder weicher. "Ich kann
dir die Frage nicht beantworten, ich finde sie
schwierig.", sagte Marusch. "Ich möchte aber betonen, dass ich Allil nicht
verteidige. Ich will, dass sie zur Schule gehen kann, wie
alle anderen Personen, einschließlich Verbrechenden,
auch. Das hat nichts mit Verteidigung der Taten zu tun."

Lilið fühlte, wie sie sich auch wieder entspannte. Es
löste sich etwas in ihr. Es war schwer für sie zu verstehen, aber
sie wollte mehr darüber nachdenken. Und sie konnte damit
leben, dass Marusch sich Zugang zu Bildung für eine potenzielle
Mörderin wünschte, jetzt, wo sie verstand, dass es mit Verteidigung
nichts zu tun hatte. Wenn Marusch ihr Bildung ermöglichen
wollte, was wäre die Alternative gewesen? Sie nickte. Und
dann fuhr sie zusammen, als eine Ulene über ihre Köpfe
hinweghuschte. Ein Nachtdrache, ein Geschöpf mit so leisen
Schwingen, dass Lilið sie stets erst bemerkte, wenn sie direkt
über ihr waren.

Auch Marusch zuckte zusammen. Einen Moment grinsten sie
beide. "Kannst du mit meiner Einstellung leben?", fragte
sie. "Es ist jederzeit in Ordnung, wenn sich unsere
Wege trennen."

Lilið schüttelte rasch den Kopf. "Ich finde deine Ethik
interessant. Aber nicht offensichtlich falsch oder
schlimm. Im Gegenteil.", sagte sie. "Ich würde deine Einstellung
gern ausprobieren und herausfinden, ob sie auch zu mir passt. Vielleicht habe
ich nur noch nicht genug von der Welt gesehen. Oder ich
habe zu starke Gefühle gegenüber einer Person, die mich
beinahe ermordet hätte, dass es mir schwerfällt, ihr etwas
zu gönnen."

"Das kann ich verstehen.", sagte Marusch bloß.

Liliðs Hand suchte vorsichtig wieder die von Marusch. "Gehen
wir weiter?", fragte sie.

Marusch nickte.

---

Es gab tatsächlich einen Nebeneingang, einen Hügel hinauf etwas
abseits vom Haupthaus. Marusch hielt Lilið wieder den Arm hin, sodass
sie eingehakt, als wären sie ein müdes Tanzpaar nach einem anstrengenden
Abend, das aber hierher gehörte, das Gelände zwischen zwei Hecken
hindurch betraten. Lilið versuchte, sich nicht allzu auffällig
umzusehen. Sie waren hörbar. Ihre Füße machten Geräusche auf dem
sandigen Weg, der die Häuser miteinander verband. Aber niemand
war zu sehen. Alles war ruhig und dunkel.

Sie peilten ein kleines Häuschen an, von dem sie sich einig
waren, dass es die klassischen Charakteristiken eines
Besuchshauses erfüllte. Ganz sicher waren sie noch nicht. Es
war abgeschlossen. Das sprach dafür, dass es leer stand.

"Stellen Schlösser für dich ein Hindernis dar?", raunte
Marusch.

Lilið begutachtete das Schloss einen Augenblick. "Hältst du Wache?", fragte
sie. "Ich brauche höchstens fünf Minuten."

"In Ordnung." Marusch wandte sich dem Gelände zu.

Das Einbruchswerkzeug, das Lilið auch immer in der Jacke versteckt
trug, fühlte sich so vertraut in den Händen an, das Lilið in sich
hineinlächeln musste. Sie mochte die prickelnde Spannung unter
der Haut, ein fremdes Schloss zu knacken, dieses Mal in einer brenzlicheren
Lage.

Sie hatte das Schloss maßlos überschätzt. Sie knackte es fast so schnell, wie
sie mit einem Schlüssel gebraucht hätte. Das sprach wirklich dafür, dass es
hier nicht viel zu holen gäbe.

Sie drückte die Tür vorsichtig und leise auf, um sich nach Schuhen im Eingang
umzusehen. Vielleicht hatte sich ja bloß jemand eingeschlossen. Aber es roch,
als wäre hier schon ein paar Tage nicht gelüftet worden. Das hielt sie davon
ab, so etwas zu sagen, wie 'die Luft ist rein'. Stattdessen betrat sie einfach
den Raum und Marusch folgte ihr.

Sie suchten sich ein Schlafzimmer im Erdgeschoss aus, mit einem Fenster
nach hinten, damit sie die besten Chancen hätten, zu fliehen, wenn sie doch
entdeckt würden. Lilið lüftete und bewachte das Fenster, während Marusch
Bettbezüge suchte, um die Deckeninnenleben zu beziehen. Lilið hätte am
liebsten laut gelacht, weil die Situation so herrlich schön und absurd
war. Aber sie beließ es bei einem leisen, unauffälligen Grinsen.

Sie schloss das Fenster wieder und zog die Vorhänge in ihre alte
Position, als Marusch fertig war. Nun standen sie sich in der Dunkelheit
gegenüber, ein paar Schritte von einander entfernt. Es war nicht
stockfinster, aber Lilið sah von Marusch nicht viel mehr als
eine Silhouette. Ihr wurde heiß, allein bei der Vorstellung, was jetzt
passieren könnte. Oder wollten sie einfach in diesem Bett schlafen? Vielleicht
ein wenig kuscheln?
