Welten
======

*CN: Sexuelle Übergriffigkeit, Erinnerungen an Mobbing,
Durchfall, Gift (erinnernd an KO-Tropfen?), Lebensgefahr, Cliffhanger.*

Lilið brach das Siegel und fühlte sich an Kindertage
zurückerinnert. Es war kein professionelles Siegel, sondern
einfaches Kerzenwachs, in das ein M geprägt war. Ein
M, das darauf schließen ließ, dass es eine Person in ein
Stück Holz oder ähnliches Material geschnitzt hatte, die
weder gut im Schnitzen war, noch darin, spiegelverkehrt
zu schreiben. Ein M hatte diesbezüglich immerhin
eine vorteilhafte Symmetrie.

Lilið hatte zu Schulzeiten mit ein paar Mitlernenden manchmal Briefe
mit ähnlich beschaffenen Siegeln ausgetauscht. Die Erinnerung
war schön, obwohl die Beziehungen zu ihren Mitlerndenden
insgesamt eher unangenehm gewesen waren. Sie
war in der Schule nicht sehr beliebt gewesen. Wer sich mit ihr
offen angefreundet hätte, hätte es schwer gehabt. Also
hatte niemand gewagt, sichtbar mit ihr befreundet zu
sein, aber einige hatten heimlich mit ihr Briefe
geschrieben. Sie hatte sich damals
nicht bewusst gemacht, wie es hätte besser sein können. Sie
hatte alles einfach genossen, was doch schön gewesen war,
als wäre es losgelöst und unbelastet. Die Briefe mit
selbst kreierten Siegeln und erfundenen Geheimschriften hatten
für sie dazu gehört. Unbeschwertheit war vielleicht der Name für das
Gefühl, mit dem Lilið diese separaten Erinnerungen verband.

Es war gut, wenigstens die heimlichen Freundschaften gehabt zu haben, aber
zu ihrem Schulabschluss hatten alle ein großes Fest gefeiert
und auch die, die mit ihr insgeheim befreundet gewesen waren, hatten
ihr nicht einmal Bescheid gesagt. Es hatte in ihr so ein mieses
Gefühl ausgelöst, dass sie kurzerhand jeglichen Kontakt abgebrochen
hatte. Nun war sie allein. Sie träumte von Freundschaften und
sie wusste, dass es schwer wäre, welche zu finden, wenn sie
nicht in die Welt zöge. Sie hatte aber auch Angst, dass sie einfach
nicht geeignet wäre, welche zu führen. All diese gemischten
Gefühle hatten dafür gesorgt, dass sie sich ein paar Monate
gönnen gewollt hatte, zur Ruhe zu kommen, bevor sie sich um eine
Ausbildung zum Nautika bemüht hätte. Nun waren die Pläne also
durchkreuzt worden.

Sie entrollte den Brief. Vielleicht war es Sehnsucht
nach Freundschaft, die sie so sehr an Marusch denken ließ und
sie mit Hoffnung füllte, ausgelöst durch das Papier mit dem
nun gebrochenen Siegel in Händen.

> Verehrtes Lilið!
> 
> Es mag vielleicht nicht die beste Anredeoption sein. Räch dich gern an mir,
> wenn nicht. Gegebenenfalls habe ich schöne Neuigkeiten für dich. Ich habe
> etwas über den Namen Lilið recherchiert. Falls ich das richtig verstehe,
> handelt es sich nicht unbedingt um einen Frauennamen. Tatsächlich lesen sich
> viele Quellen so, dass die Kreatur Lilið eigentlich nicht, wie oft
> dargestellt, gegen die Unterdrückung von Frauen kämpft, sondern unsere
> komplette Geschlechter-Ordnung zerrütten möchte. Und das ist natürlich noch
> kein Beweis dafür, dass sie keine Frau und ihr Name kein Frauenname ist.
> Natürlich nicht. Gleichwohl denke ich, dass es naheliegt, dass auch sie,
> ähnlich wie wir zwei, selbst gar nicht erst ins binäre Gefüge passt. Sie hat für
> mich, wenn ich die Quellen lese, ein starkes Geschlechtsgefühl außerhalb der
> Kategorien Mann und Frau. Gegebenenfalls aber auch von beidem zusätzlich
> etwas. Eine Quelle verwendet für Lilið sogar niemals den Begriff Frau. Furie,
> -- das ist das Wort, das diese Quelle an Stelle von 'Frau' verwendet --, ist
> wohl eigentlich negativ konnotiert, aber ich mag den Begriff doch. Als Furie
> bezeichne ich mich gern, wenn ich wütend werde, was mich dann irgendwie
> bestärkt und mich mehr wie ich fühlen lässt, mich weniger schämen lässt,
> wütend zu sein. Hat dir der Einblick gefallen? Rede ich über zu persönliche
> Dinge?
> 
> Dann zu den wichtigen Dingen. Um euren Tausch unauffällig hinzubekommen,
> solltet ihr im Vorfeld üben, eine realistische Rolle der jeweils anderen
> Person einzunehmen, wobei du als Mentorin von Anfang an offenlegst, dass du das
> vermeintliche Lilið nicht die ganze Reise lang begleiten willst, sondern
> sie für eine Besorgung unterbrechen und später anderweitig fortsetzen wirst.
> Rollentausch ist auf der Reise früher besser als später, sollte also unmittelbar
> passieren, wenn ihr an Bord geht, sodass ihr bereits in den getauschten Rollen
> kennen gelernt werdet. Chaos halte ich für wirksamer als Ablenkungsmethode
> für einen gelingenden Rollentausch bei Fahrtantritt als Chameleonmagie, weil Allil diese
> sonst womöglich sehr lange aufrecht erhalten müsste. Höchstens in dem Moment, wenn ihr
> an Bord geht, kann ihre Chameleonmagie zusätzlich zu Chaos nützlich sein, um
> zu verwirren, nicht als Alternative.
> 
> Allil überschätzt sich manchmal ein wenig, aber sag ihr nicht, dass ich dir
> das geschrieben habe. Lieber solltest du ihr Mut machen, das braucht sie
> manchmal. Langsam wird der Briefbogen voll... Ich wünsche euch alles Gute!
> 
> Liebevolle Grüße, Marusch.

Auf den letzten Rest des Bogens war noch in
friemeliger Handschrift mit einer feineren Feder hinzugefügt:

> PS: Ich halte mich, damit ich nicht aufdringlich bin, aus dem Fluchtplan
> raus. Wenn du mich wiedersehen möchtest, versuch es auf dem Angelsoger
> Adelsball.

In Liliðs Luftröhre bildete sich so etwas wie eine Verstopfung, die sie
ein paar Augenblicke nicht atmen ließ. Das passierte ihr oft, wenn
sie stark fühlte, vor allem, wenn sie begeistert war. Nein, dachte sie,
das waren nicht zu persönliche Dinge. Sie liebte alles am ersten
Abschnitt. Wie Marusch sich Gedanken über eine gute Anrede für
sie gemacht hatte, die sich allein für den Versuch schon gut anfühlte. Der
Vorschlag, sich an Marusch zu rächen, die Albernheit. Wie der Text
dann übergegangen war in dieses wunderschöne, sie bestätigende Wissen
über ihre Namensherkunft. Eigentlich kannte sie die Namensherkunft, aber
sie hatte sich nie mit Details dazu befasst.

Sie mochte das Wort Furie ebenfalls. Es klang schön. Allerdings konnte
sie sich weder selbst, noch Marusch in laut wütender oder anders
furioser Verfassung vorstellen. Nun, sie kannte Marusch noch nicht
lange.

Die Atemblockade kam aber auch durch die Unsinnigkeit des letzten Satzes. Den
Angelsoger Adelsball hätte Lilið nicht einmal in der Rolle des Kindes
von Lord Lurch ohne Weiteres besuchen können. Vielleicht, wenn ihre Mutter
auch adelig gewesen wäre. Wie sollte sie dann erst auf diesen Ball gelangen
können, wenn sie identitätenlos unterwegs wäre? Oder die von Allil übernahme, die
aber gerade auch nur spielte, adelig zu sein und es eigentlich nicht
war?

Lilið müsste dafür sagenhaft gekleidet sein, sich irgendwie hineinschummeln
und dann gut schauspielen. Das würde ein sehr riskantes Unterfangen
werden. Aber sie würde vorsichtshalber ein gutes, dünnes Abendkleid
einpacken.

Lilið faltete den Brief nach einiger Überlegung und legte ihn in das
Buch, das sie in ihrer Jacke aufbewahrte. Sie hätte ihn gern
faltenlos gelassen, aber der Drang, ihn immer bei sich zu haben, war
größer. Im Buch war er gut geschützt. Die Jacke ließ Lilið unauffällig
nie aus den Augen.

Den Gedanken an den Ball verschob sie erst einmal. Selbst
wenn es realistisch werden würde, auf diesen Ball zu gelangen, waren
Überlegungen dazu für Lilið noch zu weit weg. Es gab so viele Stationen
bis zu ihrer Freiheit zu planen, die alle scheitern könnten, dass sie
keinen Raum hatte, sich viele Gedanken über ein Danach zu machen.

---

In den kommenden Wochen lebte Lilið, -- so fühlte es sich zumindest
an --, abwechselnd in verschiedenen Welten. Sie spielte die Rolle der an sich zwar nicht
völlig vom Konzept Internat für skorsche Damen überzeugten Tochter, die
aber doch fand, dass der einzig sinnvolle Weg für sie wäre. Das war in
ihrem Kopf die Welt aus Familie und Hofstaat, in der sie groß geworden war, die sie
kannte, und die sich doch nicht mehr so vertraut anfühlte. Sobald sie
das Besuchshaus betrat und mit Allil allein war, fühlte sie sich in
einer völlig losgelösten Dimension. Als würde Zeit und Gefüge mit Allil
in den abgedunkelten Räumen anders funktionieren. Es fühlte sich
wie eine neue Welt an, die anderen Regeln gehorchte, in die Lilið eigentlich schon
immer gehört hatte. Es war, als hätte diese Welt schon immer in Lilið geschlummert.
Sie war gefährlich, aber Lilið konnte in ihr freier atmen, fühlte sich
nicht so eingesperrt oder eingeengt. Das war witzig, bedachte sie, dass sie aktuell
nur in einem kleinen Besuchshaus existierte.

Allil trainierte sie in allerlei Dingen. Die ersten vier Tage tauschten sie
lediglich Rollen, sobald Lilið das Besuchshaus betrat. Allil verkörperte
gleich mehrere Personen, mal Lilið, mal zum Beispiel ihren Vater, und
Lilið sollte versuchen, Allil in der Rolle der Mentorin darzustellen. Sie
spielten immer ein paar Stunden durch, ohne dass Allil unterbrach, um
Lilið etwas zu erklären. Sie ließ Lilið im Spiel einfach auflaufen, wenn
sie Fehler machte. Lilið fand sehr beeindruckend, wie Allil zum Beispiel
Lord Lurchs Denkweise kopierte und Liliðs Schauspiel dabei entlarvte, wie
ihr Vater es realistisch getan hätte.

Erst irgendwann am Abend unterbrachen sie das Spiel jeweils und Allil gab
theoretischere Lektionen, die Lilið über Nacht verinnerlichen
sollte.

Wenn Allil nicht gerade eine Person schauspielerte, die
Emotionen zeigte, verhielt sie sich streng und unaufgeregt. So, als
würde sie Lilið gegenüber weder Sympathie noch Antipathie hegen. Es war
vielleicht einfach eine Zweckbeziehung für sie.

Lilið schwankte eine Weile, ob sie versuchen sollte, durch
die Fassade hindurchzudringen und Allil
persönlich näher kennen zu lernen. Sie wollte sich nicht
aufdrängen und auf der anderen Seite wollte sie mehr Einblick
in Allils Leben haben, wo sie doch selbst mindestens vorübergehend
ein verruchteres Leben für sich plante. Sie mochte die
Eigennützigkeit ihres Interesses nicht, aber eines Abends fand
sie die Frage, die sie schon von Anfang an interessiert
hatte, harmlos und unaufdringlich genug, um sie zu
stellen: "Warum möchtest du das Internat besuchen?"

Allil legte lautlos ein Messer auf den Tisch. Heute hatte Allil
angefangen, Lilið den Umgang mit Messern zu zeigen. Nicht
nur, um Leute zu bedrohen, sondern vor allem, wie sie sie unauffällig bei sich
tragen oder griffbereit halten konnte.

"Weil ich Magie lernen will?" Allil runzelte die Stirn.

"Du hast wohl recht, dass ist offensichtlich.", beeilte sich Lilið,
einzuräumen. Sie fühlte sich so unfähig, eine Freundschaft anzufangen.

Draußen vor den Fenstern, -- sie hatten zur Abwechslung die Vorhänge einen Spalt
geöffnet --, turtelten drei Auben. Die hatten mit Beziehungsaufbau
keine Probleme, schien es. Die grauen Drachen gurrten und sprangen gegenseitig
auf ihre Rücken. Das war eine Kennenlernart, die Lilið womöglich
eher gelegen hätte als reden, aber wahrscheinlich hätte sie Allil überhaupt
nicht gepasst. Lilið unterdrückte ein Grinsen bei der Vorstellung.

"Entschuldige, wenn das unangenehm rüberkam.", sagte Allil. "Magie zu beherrschen,
hat einfach in dieser Welt eine Menge Vorteile."

Lilið nickte. "Wohl wahr."

"Du kannst Privilegien und Freiheit haben, weil du hineingeboren worden bist, oder
weil du gut in Magie bist. Ersteres ist bei mir eben nicht gegeben.", fuhr
Allil fort. "Ich habe keinen Oberschulabschluss, mit dem ich Magie studieren
könnte. Und zwar nicht, weil ich nicht gut genug gewesen wäre, sondern weil
das Schulsystem nicht so durchlässig ist, wie es immer heißt, und ich während
meiner Schulzeit auch noch arbeiten musste." Sie zögerte einen Moment und fügte dann
hinzu: "Oder stehlen."

Lilið wusste nicht genau, wie sie darauf reagieren sollte. Wollte Allil
Mitgefühl? Sie wirkte nicht unglücklich, eher emotionslos. "Das System
ist ziemlich ungerecht.", stimmte sie also zu. "Ich bin in einer recht
privilegierten Situation, bekomme aber trotzdem genug mit, um das zu
wissen."

"Deshalb bin ich froh, dass ich durch dich die Möglichkeit haben werde, eine Chance
auf einen nachträglichen Oberschulabschluss zu erlangen. Dann kann ich
Magie studieren. Und dann vermutlich eine Anstellung bekommen, durch die
ich mehr Freiheiten haben werde als jetzt. Vielleicht ein oder zwei Zimmer für mich, in
denen ich offiziell wohnen kann. Also, ohne Angst zu haben, aufzufliegen,
weil der Grund dafür, sie zu bewohnen, dann nicht mehr so eine Nummer wie
jetzt wäre.", beendete Allil die Ausführung ihrer Pläne.

Lilið lächelte. Es gab ihr immerhin ein besseres Gefühl dabei, so viel zu riskieren, dass,
ginge der Plan auf, nicht nur sie davon profitierte, sondern Allil vielleicht
noch viel mehr. Das war schön. Aber ob sie Allil nun durch dieses Gespräch wirklich näher
gekommen war, wusste sie nicht. Vielleicht ein wenig.

---

Der Abreisetag war bewölkt und windig, was ihnen gut in den Kram passte. Das
sich verändernde Licht lenkte Leute davon ab, genau hinzusehen.

Ihr Vater brachte sie zum kleinen Hafen, der zum Hof gehörte, und wartete mit
ihnen am Steg. Sie beobachteten gemeinsam, wie die Reisefragette einlief und
die Segel einholte. Sie hatte zu viel Tiefgang für den Hafen, weshalb ein
kleineres Beiboot zu Wasser gelassen wurde, dass Allil und Lilið einsammeln
würde. Als es anlegte, war der Moment für ihr geplantes Chaos gekommen. Allil
tat so, als würde ihr Gedärm sich sehr plötzlich entleeren wollen, also
verabschiedeten sie sich so hektisch von Lord Lurch, dass es nicht mehr dazu
kam, dass er sie vorstellte. Tatsächlich verabschiedete sich Lilið
von ihm höflich und Allil fiel ihm rasch zum Abschied um den Hals. Es war
natürlich vollkommen unangemessen, aber Lord Lurch war keine Person, die
sich mit Etikette auseinandersetzte, wenn eine etwas unbeholfene Mentorin
Darmprobleme hatte.

Als das Beiboot mit ihnen in See stach, fühlte Lilið sich bereits viel befreiter. Das hatte
also geklappt. Nun musste sie nur noch über zwei Reisetage hinweg erfolgreich
so tun, als wäre sie eine Mentorin, die für eine Erledigung auf den
Reiseinseln eine Unterbrechung der Reise plante, von wo aus sie erst
die nächste Reisefragette zum Internat zu nehmen gedachte. Das war sicher auch nicht
einfach, aber es handelte sich eher um eine kontinuierliche Herausforderung, nicht
mehr um so einen Moment, von dem alles abhinge, wie eben.

---

Die beiden Junior-Nautikae, die das Beiboot segelten, beeilten sich,
sie beide an Bord zu bringen. Allils Spiel war sehr überzeugend, als
sie die Strickleiter emporstieg und so tat, als müsse sie die Arschbacken
zusammenkneifen und als verkrampfte ihr Körper. Lilið spielte besorgt und blieb
dicht hinter ihr. Ihnen wurde ohne Umschweife die Bordtoilette gezeigt. Es gab
mehrere Kabinen, die von einem der seitlichen Gänge auf dem Mitteldeck
an der Reling entlang erreichbar waren. Auf diese Weise waren sie gut
durchlüftet. In den Holztüren befanden sich kleine Löcher nach außen. Lilið
wurde auch darüber informiert, dass sie, wenn sich Allils Zustand
nicht bessern würde, die Schiffsmedikae auf der gegenüberliegenden
Seite der Reisefragette finden würde, vom anderen der seitlichen Gänge auf dem
Mitteldeck aus erreichbar.

Lilið stellte sich vor die Tür, hinter der Allil ihren vorgetäuschten
Klogang überzeugend in die Länge zog. Lilið lehnte sich auf die Reling,
fühlte den Wind in den Haaren, der ihr Gefühl von Freiheit und
endlich Luft zu bekommen noch verstärkte, und sah den
schwarzen Ormoranen zu, die ihre glänzenden Flügel in
der Sonne trockneten. Diese Seedrachenart belebte fast jeden Hafen. Sie
fischten in den Gewässern nach Fressbarem, und wenn sie
genug hatten, dann hockten sie sich auf Steinmolen oder Brüstungen, breiteten
ihre majestätischen, schwarz glänzenden Flügel aus und ließen sie
in der Sonne wärmen und trocken. Das war ein Leben. Lilið hatte sich nicht
selten gewünscht, ein Seedrache zu sein.

Die Besatzung der Reisefragette lichtete den Anker, hisste die Segel wieder und das
Schiff setzte sich in Bewegung, noch bevor Allil die Toilette wieder
verließ. Allmählich, dachte Lilið, war ausreichend Zeit vergangen, um
den vorgetäuschten Klogang zu beenden. An Allils Stelle hätte sie
eher in einer halben Stunde einen weiteren Krampf vorgetäuscht, als
jetzt noch weiter auf der Toilette auszuharren. Aber an sich traute
sie Allils Einschätzungsvermögen in Sachen Schauspiel mehr als ihrem
eigenen.

Ein Mensch näherte sich Lilið und lehnte sich neben sie an die Reling. Der
geringe Abstand war ihr unangenehm, also rückte sie ein Stück zur Seite, um
ihn auf eine für sie angenehme Distanz zu vergrößern.

"Wohin fahrt ihr?", fragte der Mensch.

Das war also die erste Härteprobe, ob sie gut genug spielen könnte. "Wir
sind auf dem Weg zum Internat für skorsche Damen auf Frankeroge. Also,
Lilið ist auf dem Weg dorthin. Ich werde auf den Reiseinseln noch einen
Zwischenhalt einlegen. Und du?"

"Oh, welch eine Ehre!", sagte der fremde Mensch. Er rückte das kleine
Stück Abstand wieder auf, das Lilið sich gerade erst zurückerobert
hatte. "Ich bin nur so mäßig skorsch, muss ich zugeben. Ich bewundere
es immer vor allem bei Frauen, wenn sie es sind."

So ein Mensch war das also. Wenn Lilið noch weiter weggerückt wäre, hätte
er direkt von Allils Kabine gestanden. Lilið war auch generell eher
nicht so angetan von der Idee, den Rückzieher zu machen.

Sie hatte gerade überlegt, ob sie den gleichen Trick wie bei Herrn
Hut anwenden wollte: schauen, ob dieser Mensch es irgendwann wagte, sie
anzufassen, sich dann annähern und ihn zu bestehlen, während sie mit
einer Drohung ablenkte. Da fiel ihr ein, dass es an der Idee nun einige
neue Haken gab: Sie würden zwei Tage Zeit auf dem gleichen Schiff
verbringen. Sie könnte also nicht einfach ohne Weiteres fliehen. Wenn
sie erwischt würde, wären die Konsequenzen nun auch viel schlimmer, weil
ihr Vater nicht mehr einschreiten könnte. Vor allem nach einer
hoffentlich geglückten Flucht nicht mehr. Sie musste sich anders
zur Wehr setzen oder abgrenzen. Aber wie machte man so etwas normalerweise?

Zu ihrer Erleichterung trat in diesem Moment Allil aus der Kabine. Sie
sah fahl im Gesicht aus. Lilið fragte sich, ob sie sich geschminkt hatte,
es ihr nun nicht mehr nur vorgetäuscht nicht gut ginge oder ob das
ihre Chameleonmagie wäre.

"Weißt du schon, wo unsere Kabine ist, Allil?", fragte Allil.

Lilið schüttelte den Kopf.

"Das hättest du ja mal herausfinden können.", monierte Allil. "Na ja, dass
du gewartet hast, ist auch lieb."

---

Noch während der ersten Stunden Fahrt passierten sie Danmoge, eine Insel
vor Nederoge. Von allen Reisezielen, zu denen Lilið je selbst gesegelt
war, lag diese am weiteresten weg, daher fühlte es sich für Lilið
nostalgisch an, ihr nachzusehen. Es dauerte nicht lang, da
lag auch diese Insel hinter ihnen und sie waren umgeben
von nichts anderem als stahlgrauem Meer mit ein paar anderen,
kleineren Segelbooten, die darüber schipperten. Eine bedienstete Person
auf diesem Schiff zeigte ihnen Messe, Teeküche, ihre Kabine und andere
wichtige Orte.

In der Kabine fühlte sich Lilið immerhin halbwegs sicher vor Tomden. Der Mensch
von vor den Toiletten hatte sich inzwischen vorgestellt. Lilið bemerkte durchaus, dass er sie
immer wieder abpasste. Sie beide. Besonders auf schmalen Gängen. Er fasste
sie nie an, aber er kam ihr immer zu nah.

Lilið beschloss, sich dieses Mal nicht mit starken Mitteln zur Wehr
zu setzen und fühlte sich ausgelieferter als je zuvor. Es waren immerhin
nur zwei Tage, die sie es aushalten müsste. Wenn er nichts Schlimmeres im
Schilde führte. Lilið wich ihm aus, so gut es ging, aber sie konnte
nicht die ganze Zeit in ihrer Kabine bleiben. Zum einen drängte Allil
sie, sich unauffällig, also nicht allzu zurückgezogen zu verhalten, und
zum anderen mussten sie auch essen. Er sah auch bei den Mahlzeiten
immer zu ihnen herüber. Es wurden zwei anstrengende und unangenehme Tage.

---

Als ihr letzter Mitreisetag anbrach, fing Lilið optimistisch an, Erleichterung
zu fühlen, aber wenige Stunden bevor die Reisefragette die Reiseinseln erreichen sollte,
spitzte sich die Lage zu. Lilið und Allil saßen in der Teeküche (schon wieder eine
Teeküche) und führten ein Abschiedsgespräch, das sie zuvor einstudiert
hatten, während sie noch einmal Übungsaufgaben für die
Schule durchgingen. Allil stand auf, als das Wasser kochte,
um den Tee aufzugießen. Sie wartete noch an der Küchenzeile, bis er zu Ende gezogen
hätte, und natürlich gesellte sich Tomden zu ihr, viel zu dicht. Lilið wollte
Allil dort nicht allein lassen, also stand auch sie auf, um die Tassen
abzuholen.

"Darf ich euer Gespräch unterbrechen?", fragte sie. "Ich glaube, ich habe
den Fehler in deinem Lösungsansatz für Aufgabe 16 gefunden, und wenn wir
das noch besprechen wollen, bevor ich dich verlassen muss, dann sollten
wir das nicht allzu lange aufschieben."

Allil blickte sie dankbar an. "Ich komme gleich mit der Kanne nach!", sagte
sie, und sich an Tomden richtend fügte sie hinzu: "Es tut mir leid, vielleicht
habe ich ein andernmal mehr Zeit."

Tomden ließ sich nicht abwimmeln und kam zu ihnen an den Tisch, um sich
gefühlvoll bei Lilið zu verabschieden,
aber zu ihrer Überraschung blieb es dabei. Er wünschte eine gute Weiterreise und
verließ anschließend den Raum. Vielleicht hätte das ein Alarmsignal für
Lilið sein sollen, dachte sie im nächsten Moment, als sie die Teetasse
an ihre Lippen setzte, pustete, einen kleinen Schluck nippte -- und
Gift herausschmeckte. Ein Gift, über das
sie Bescheid wusste, auch wenn sie es selbst aus gutem Grund nie
benutzte. Sie schaffte es, kaum Tee zu schlucken und spuckte
den Rest zurück in ihre Tasse. Aber sie wusste, dass
das bisschen Gift schon ausreichen konnte, um sie übel in Mitleidenschaft
zu ziehen. Sie wusste nicht wie sehr, weil sie nicht vom Geschmack auf die
Konzentration schließen konnte.

Mit zitternden Händen setzte sie die Tasse ab und blickte Allil an. "Trink
das nicht.", sagte sie leise. "Lärchenwurz."

"Lärchenwurz?", fragte Allil kurz irritiert. Dann wirkte sie mit einem Mal
hellwach. "Oh scheiße! Hat er das in den Tee getan? Ich habe nicht gut
aufgepasst, aber er war den Tassen schon sehr nahe. Ich war nicht wach
genug, es tut mir leid!"

Lilið merkte, wie ihr schwindelig wurde. Gleichzeitig fühlte sie
Panik in sich aufsteigen und war sich nicht ganz sicher, wieviel
Schwindel vom Gift und wieviel von der Panik kam. "Ich brauche Hilfe.", murmelte
sie. Sie versuchte, klar zu denken, aber gerade gelang ihr das nicht.

Allil nickte. "Ich habe in unserer Kabine ein Gegengift.", erklärte
sie. "Das rote Medizintäschchen sollte ganz zu oberst im Rucksack
liegen. Darin findest du ein kleines braunes Fläschchen mit dem
Gegengift. Ich werde mich um die Entsorgung des Giftes hier kümmern und gleich
nachkommen. Schaffst du das ohne mich?"

Lilið stand schwankend auf, tat einen tiefen Atemzug und fühlte sich
ausreichend sicher auf den Beinen. "Ich glaube schon.", sagte sie.
"Vielleicht war es doch nicht so viel. Aber besser ist besser."

Sie wusste, wie das Gift wirkte. Wenn sie jetzt noch stehen konnte, dann
war sie einigermaßen auf der sicheren Seite.

Sie verließ die Teeküche zügigen Schrittes und als sie merkte, dass sich
der Gangschwindel in Grenzen hielt, rannte sie die Außentreppe
hinab zu ihrer Kabine. Sie schätzte, dass sie der Inhalt der ganzen
Tasse wahrscheinlich binnen weniger Minuten getötet hätte. Lärchenwurz
war ein Gift, das einfach angenehm kräuterig
schmeckte und in einem Tee nicht weiter auffiele. Sie hatte natürlich
nie wirklich Lärchenwurz probiert, aber ihre Mutter hatte ihr mal das
Kopfkraut der Mörbenwurzeln zu einem Tee zubereitet, das ungefähr den gleichen
Geschmack hatte. Von ihrer Mutter hatte sie einiges über Gifte gelernt.

Sie fand das Medizintäschchen tatsächlich ohne Probleme. Darin gab
es einige Fläschchen, Tinkturen und Verbandsmaterial. Immerhin war
nur eines der Fläschchen braun. Lilið entkorkte es und ein kräuteriger Geruch
stieg ihr in die Nase. Wieviel sollte sie nehmen? Sie dachte darüber
nach, was ihre Mutter ihr erzählt hatte. Liliðs Hand zitterte einen
Moment und verkrampfte sich zugleich um das geöffnete Fläschchen,
um nichts zu verschütten. Es gab kein Gegengift zu Lärchenwurz.

Liliðs Herz stolperte. Das konnte wieder die Panik sein, oder das
Gift. Wollte Allil sie vergiften? Oder wusste sie mehr von Giften
als Lilið?

Was hatte Marusch geschrieben? Allil schoss manchmal über das Ziel
hinaus oder so etwas? War da eine versteckte Botschaft gewesen, die
sie hätte entdecken sollen?

Lilið rechnete nicht damit, viel Zeit zu haben, aber wie automatisiert
zog sie das Buch aus der Jacke, die sie sich um die Hüften gebunden
hatte, und den Brief aus dem Buch. Sie war hellwach und zugleich
verschwamm der Brief immer wieder vor ihren Augen. Sie las die Stelle
über Allil noch einmal:

> Allil überschätzt sich manchmal ein wenig, aber sag ihr nicht, dass ich dir
> das geschrieben habe. Lieber solltest du ihr Mut machen, das braucht sie
> manchmal. Langsam wird der Briefbogen voll... Ich wünsche euch alles Gute!

Und ihr Atem stockte abermals. Das konnte kein Zufall sein. Zusammen mit
Maruschs Unterschrift war in der Nachricht ein Name versteckt: "Allil". Ihr
flatternder Blick konnte kaum auf den Brief fokussieren, aber die
Verschlüsselung war nur geschickt untergebracht, nicht schwer zu entziffern,
hatte sie sie erst einmal verstanden. Sie überflog den Brief und
entschlüsselte die kurze Botschaft darin. Marusch hatte
ihr also verraten, dass Allil sie möglicherweise vergiften wollte. Da
gab es für sie keinen Zweifel mehr.

Sie schloss einen Moment die Augen, um zu planen, was sie nun tun sollte. Sie
musste so tun, als wäre sie vergiftet, oder als wäre es ausversehen gescheitert,
damit Allil keinen Verdacht schöpfte. Sie musste schnell unter Leute, damit
sie weniger in Gefahr wäre. Es wäre wahrscheinlich in jedem Fall gut, die
Schiffsmedikae aufzusuchen.

Lilið suchte sich aus Allils Medizintasche eine kleine, hellblaue Flasche heraus, die
fast leer war, kippte deren letzten Inhalt auf dem Boden aus und füllte die
braune Flasche darein um. Sie steckte das umgefüllte Gift samt Brief
und Buch zurück in ihre Jacke und ließ die braune Flasche leer auf dem Boden
liegen, als wäre sie ihr aus der Hand geglitten. Sie griff sich ihre
Trinkflasche vom Bett, weil viel Flüssigkeit die Wirkung des Gifts
eindämmen würde, und trank direkt einige große Schlucke. Auf dem
Nachttisch lagen noch zwei Kekse, die sie sich in den Mund schob, nachdem
sie sie skeptisch beschnuppert hatte, ob sie nicht auch vergiftet
sein könnten. Aber auch Brotartiges im Magen würde die Wirkung der
kleinen Dosis, die sie zu sich genommen hatte, abdämpfen. Dann zog sie die Jacke
an, weil die Wirkung des Gifts sie bald frieren lassen würde, und huschte aus
der Kabine. Ab hier musste sie so tun, als wäre sie kaum in der Lage, zu
gehen, und sich in Richung der Kabine der Schiffsmedikae schleppen. Allil
war gut darin, sich anzuschleichen, das wusste Lilið. Sie wäre deshalb nicht
sinnvoll, erst dann zu spielen, dass sie kaum fähig zu gehen wäre, wenn
sie Allil entdecken würde.

Lilið bewegte sich taumelnd durch den Kreuzgang zur anderen Bordsseite, um von
dort die Treppe hinaufzunehmen. Es war ihr unheimlich, weil es eine einsame
Treppe war, aber wenn sie oben wäre, hätte sie es geschafft. Sie klammerte
sich ans Geländer und schnaufte, wie sie es unter Einfluss des Gifts
getan hätte, als sie die ersten Stufen emporklomm.

Vielleicht hätte sie doch nicht so laut atmen sollen. Vielleicht hätte
sie dann den Schatten bemerkt, der sich ihr von hinten näherte. "Es
tut mir leid, Lilið, aber dass du mich auffliegen lassen könntest, ist
für mich ein zu großes Risiko.", hörte sie Allil ihr zuraunen. Ihr
Körper gefror einen Moment und sie konnte sich nicht wehren, als
Allil sie in einer einzigen, fließenden, vielleicht
Magie-unterstützten Bewegung über Bord hievte. Dann war die Starre
auch schon vorbei. Lilið griff nach der
Reling, erreichte sie aber nicht. Sie schrabbte an der Bordwand
entlang. In einem letzten Moment großer Klarheit stieß sie sich mit
aller Kraft mit den Beinen von der Bordwand ab und tauchte, vom
Schiff weggleitend, ins eiskalte Wasser ein. In die Kielströmung
zu geraten, hätte ihr vielleicht das Leben gekostet. Rascher, als
bei dem Vorhaben, mit einer geringen Dosis Gift im Blut durch einen
Ozean zu schwimmen zumindest.

Es ging alles sehr schnell. Sie machte einige kräftige Schwimmzüge von
der Reisefragette weg, um nicht in ihren Sog gezogen zu werden, und nur
wenige Momente später, war sie schon so weit entfernt, dass Lilið sich wenig
Hoffnung machte, von irgendwem an Bord gesehen zu werden.
