Die Würfel fallen
=================

*CN: Misgendern, Erpressung, Entführung, Damsel in Distress,
Schmerz, Insekten und andere Krabbeltiere - unkonkret, Body-Horror,
Gaslighting?, Cliffhänger.*

Sie hatten sich Gedanken darüber gemacht, wann es von Vorteil
oder von Nachteil sein könnte, dass Lilið ihr Gesicht faltete, um
sich als jemand anderes auszugeben. Die Antwort, die sie
gefunden hatten, war interessanterweise, dass sie immer das geringere
Risiko eingehen würde, wenn sie es nicht täte.

Heelems Brief war auf den Namen Lilið ausgestellt und wahrscheinlich
auf sie geeicht. Natürlich könnte sie so tun, als wäre sie eine
andere Person mit Namen Lilið. Das würde funktionieren. Aber ob
eine Nautika-Zentrale in einer Umgeung, in der es eine bekannte
Person mit dem seltenen Namen Lilið gab, dann nicht ohnehin Verdacht
schöpfte, war fraglich. Es wäre besser, gleich mit offenen Karten
zu spielen, damit sie nicht in Verlegenheit gebracht werden würde,
nachzuweisen, nicht das Kind von Lord Lurch zu sein.

Es war nicht völlig unwahrscheinlich, dass sie als dieses Kind erkannt
würde, wenn sie die Zentrale betrat. Es wäre nicht einmal ausgeschlossen,
dass eine Person, die sich auf Eichungen verstand, sich an eine
Begegnung mit ihr erinnernd, sie durch Berührung mit ihrer
Haut hätte zuordnen können, auch wenn sie nicht wie sie selbst
ausgesehen hätte.

Die Wahrscheinlichkeit, dass
Nautikae ihr gut gesinnt wären, wäre höher, wenn sie sie
einfach direkt als Lilið erkannten, als würden sie erst
einmal einen Betrug vermuten, schätzten Marusch und Lilið.

Lilið faltete den Mantel, der das einzige war, was sie an Gepäck
bei sich trug, in eine Jacke um, die ihrer recht ähnlich sah. Wenn
sie ihr erstes Nautika-Zertifikat abholte, wäre es vielleicht auffällig,
wenn sie bereits einen Mantel besaß.

Sie freute sich über ihr Geschick beim Falten, weil sie es schaffte,
die Taschen zugänglich in die neue Struktur zu integrieren. Dann
spazierte sie aus dem weniger belebten Teil des Hafens, wo sie
angelegt hatten, zum Turm in der Mitte. Die blauen Schriftzüge
außen, die ihn als Aussichtsturm der Hafenwacht und als Zentrale
der Nautikae kennzeichneten, erfüllten Lilið jedesmal mit einem
Gefühl von Euphorie. Oder von Ankommen, von Glück, weil sie dazugehören
wollte, und nun endlich dazugehören würde.

Es gab eine Außentreppe, die in eine mittlere Etage und dann
weiter in den Ausguck führte. Sie war schon oft als Kind
in einem solchen Ausguck in ihrem Heimathafen gewesen. In diesem
vielleicht einmal, als sie hier herübergesegelt waren, als sie
vielleicht zwölf gewesen war. Aber auch wenn sie den Ausguck bei
solchen Türmen immer am meisten geliebt hatte, drückte sie dieses
Mal die schwere Tür der mittleren Etage zum Gemeinschafts- und
Geschäftsraum der Nautikae auf. Gedämpftes Tageslicht empfing
sie, das nicht so sehr blendete, sondern den Raum sanft
durchflutete. Es war angenehm kühl im Raum und gut
durchlüftet. Ein Nautika stand an der Küchenzeile und kochte
Tee und andere Heißgetränke. Ein anderes sortierte Papiere. Auf
dem Tisch in der Mitte waren verschiedene Karten ausgebreitet, die
dieselbe Region zeigten, vielleicht um die Karten miteinander
zu vergleichen. Beide Nautikae blickten sich nach ihr um. Ihre
Mäntel hingen an einer Garderobe und wirkten dort beeindruckend
vor sich hin.

"Lilið.", sagte das Nautika, das sich um Papiere kümmerte, leise,
vielleicht mehr zu sich selbst.

Das Wiedererkennen war also schnell passiert. Lilið nickte.

"Ich erinnere mich daran, wie dich hier als Kind, oder vielleicht
warst du aus dem Kindsein gerade raus, niemand davon abhalten
konnte, in den Turm zu steigen und so zu tun, als gehörtest
du zur Wacht.", sagte es.

Lilið wusste nicht genau, wie sie darauf reagieren sollte. "Ja,
das klingt nach mir."

"Wir können dich hier nicht verstecken. So gern wir wollten, aber
das Risiko ist zu hoch.", informierte das Nautika.

"Aber du hast unser Wort, dass wir nicht wissen, dass du hier gewesen
bist, wenn du wieder verschwindest.", versicherte das andere.

Das war interessant. "Würdet ihr mich kurz über die Lage
informieren?", fragte sie, sich spontan mit der Situation
arrangierend. "Ich wusste nicht, dass bekannt wäre, dass ich auf
einer Flucht bin."

"Es sind zwei Gerüchte im Umlauf.", antwortete das erste Nautika
wieder. "Das eine besagt, du seist auf dem Internat für skorsche
Damen auf Frankeroge. Du sollst vor nur wenigen Tagen dort gewesen
sein, versicherte ein Herr Hut, der wohl mit dafür verantwortlich
ist, dass du dort hingelangt bist?"

Das klang eigentlich gut, fand Lilið. Sie nickte. War also Herr Hut dort gewesen
und Allil hatte sich mittels Chameleonmagie erfolgreich als Lilið
ausgegeben? Sie verstand allerdings gut, dass sie dann nicht hier
gesehen werden sollte. Warum wussten die Nautikae eigentlich von
Herrn Hut?

"Das zweite Gerücht ist recht haarsträubend.", fuhr das Nautika
fort. "Irgendjemand hat deinem Vater unterstellt, den Raub eines
Schatzes organisiert und deine Reise zum Internat
für skorsche Damen nur vorgeschoben zu haben, damit du den wertvollen
Gegenstand unbemerkt wegbringen und verstecken kannst. Die Theorie ist abstruß
und an den Haaren herbeigezogen, wie das nun einmal so ist, wenn
ein Schatz gestohlen worden ist, niemand weiß warum, und Leute
sich Seegarn dazu ausdenken. Aber wenn du hier gesehen wirst und
wir nichts unternehmen, ist das deshalb trotzdem kritisch, verstehst
du?"

Lilið nickte. "Ich sehe ein, dass ich hier nicht gesehen werden
sollte, wenn ich eigentlich auf dem Internat für skorsche Damen
vermutet werde, und mir wäre tatsächlich lieb, wenn davon möglichst
niemand weiteres erfährt.", sagte sie. Sie holte tief Luft, lächelte
und holte Heelems Brief aus der Jacke. "Der Grund, warum ich nicht
dort bin, ist, dass ich eigentlich viel lieber Nautika werden
möchte. Und ich bin heute nicht hier, um mich zu verstecken, sondern
um dieses Schreiben gegen ein Zertifikat zum Leicht-Nautika einzulösen."

Ihr war durchaus unbehaglich bei dem Gedanken, dass nun Allil
in größerer Gefahr schwebte. Aber sie hoffte, dass Nautikae in
diesem Punkte zusammenhielten. Sie wäre nicht die erste Person
mit einer heimlichtuerischen Geschichte, um Nautika zu werden. Gerade
wenn es um die Wahl dazwischen ging, Magiefähigkeiten auszubilden oder Nautika
zu werden, war Nautikae bewusst, dass der gesellschaftliche Druck
für ersteres hoch war, und sie wussten die Entscheidung dagegen
wahrscheinlich zu schätzen, auch wenn haarsträubende
Umwege involviert wären.

Das Nautika, dass mit dem Tee beschäftigt war, lächelte sie an. Es
war gerade fertig und stellte die Getränke auf Untersetzer auf den
Tisch. "Nimm dir gern eine Tasse."

Das andere Nautika runzelte die Stirn, nicht abwertend, mehr
nachdenklich, glaubte Lilið zu erkennen, und nahm den Brief
entgegen. Es entfaltete und überflog ihn zunächst. "Wie bist
du an Heelem geraten?", fragte es. "Kamt ihr miteinander
zurecht?"

Lilið nickte verwirrt. "Ja, wir haben uns gut verstanden. Gibt
es ein Problem mit Heelem?"

Das Nautika schüttelte den Kopf. "Er hat einen Ruf, der
bis hier herreicht.", antwortete es. "Einen guten eigentlich,
bei Nautikae zumindest. Er hat über Jahre als Ober-Nautika
die Flotte der hiesigen Monarchie navigiert, ist dann
aber sozusagen durchgebrannt. Er ist wie ein Geist,
mal hier, mal dort, bildet aus, übernimmt viele kleine Aufträge,
aber nie welche, die der Monarchie von Nutzen sind. Deshalb
ist er gesucht und schwer zu finden."

Lilið wunderte das irgendwie gar nicht. Es passte voll ins
Bild von Leuten, mit denen sich Marusch umgab. Lilið fragte
sich, inwiefern ihr irgendwann auch so ein Ruf anhaften
würde. Sie grinste.

"Seine Ausbildungsmethoden sind allerdings sehr streng und
nicht jedermanns", das Nautika zögerte, "nicht jedermenschs
Sache. Daher frage ich. Ich habe dich als sehr ehrgeizig
in Erinnerung, aber dich hat Kritik immer am Boden zerstört."

Durch Liliðs Erinnerungen schwappte das Gefühl von Anspannung,
dass sie in Heelems Gegenwart anfangs gehabt hatte. Die Angst
vor Wertung. Aber Heelem war für sie viel angenehmer gewesen,
als es eine Person hätte sein können, die vorsichtiger und
freundlicher, dafür aber weniger an der Sache und mehr an
ihr Kritik geübt hätte. "Ich bin gut mit ihm
zurechtgekommen.", wiederholte sie.

Das Nautika durchsuchte eine Aktenschublade nach einer Urkunde
und berührte zugleich jene und ihren Brief.

"Überprüfst du, ob sie wirklich von Heelem ist?", fragte sie. Dann
hätten sie eine auf Heelem geeichte Urkunde hier. Irgendwie
fühlte sich der Gedanke schön an, als hätte sie ihn fast hier
wiedergetroffen.

"Ja, das muss ich.", antwortete das Nautika. "Magst du herkommen,
damit ich prüfen kann, ob sie auch wirklich für dich ist?"

Lilið nickte und trat zu ihm an die Schränke, die gleichzeitig
als Schreibtisch dienen konnten.

"Ich fasse dich nun kurz am Handgelenk an.", kündigte das
Nautika an.

Lilið war das sehr sympathisch. Sie streckte bereitwillig
den Arm aus. Die beiden Finger, die sich an ihre Haut legten,
fühlten sich kühl an. Lilið versuchte, die Magie zu fühlen, aber
die Berührung war zu schnell vorbei.

"Im Normalfall müsste ich dich noch prüfen.", informierte
das Nautika. "Ich müsste dich an einer fremden Karte erklären
lassen, wie eure Route funktioniert hat und wo es zu welchen
Schwierigkeiten gekommen ist, damit möglichst ausgeschlossen ist,
dass du nicht stattdessen mit einer Reisefragette oder
-kagutte gereist bist. Aber weil die Lage so heikel ist
und du nicht allzu lange hier sein solltest, würde ich gern lediglich
dein Navigations-Buch sehen wollen."

Lilið wurde sehr heiß, aber der Schweiß, der ihr ausbrach,
ließ sie kurz darauf beinahe frieren. "Das habe ich bei meiner
Mutter gelassen.", sagte sie kleinlaut. Sie war sich nicht
sicher, ob es eine gute Strategie war, ihre Mutter ins
Spiel zu bringen, aber ihr war klar, dass sie nicht viel
Zeit hatte verschwenden dürfen, zu antworten.

"Kannst du es holen?", bohrte das Nautika nach.

Natürlich musste diese Frage kommen. Sie würde dafür maximal
drei Stunden unterwegs sein, wenn sie spazieren und dabei
noch trödeln würde. Es wäre auch nicht ausgeschlossen, das zu tun. Es
existierte ja. Marusch trug es bei sich. Vielleicht würden
sie etwa zeitgleich dort eintreffen. Aber dann würde sie
das Treffen zum Anheuern verpassen. Sie schluckte. "Ich
verstehe, wenn das für euch zu viele Umstände macht, aber ich
würde lieber geprüft werden.", sagte sie. Und dann fiel
ihr ein, wie sie das gut begründen konnte: "Es wäre nicht
gut, wenn ich am Hof meines Vaters gesehen werden würde."

"Aber du hast das Buch doch dort abgegeben!", argumentierte
das Nautika.

Das stimmte. "Nachts.", erwiderte Lilið ohne Umschweife, während
ihr Kopf versuchte, an einer guten Begründung zu arbeiten, warum
sie es überhaupt getan hatte.

"Ah, ich verstehe.", sagte das Nautika und wandte sich an
das andere. "Würdest du eine eilige Prüfung machen? Es geht um
eine Überfahrt von Angelsoge mit einer Jolle hierher,
die vierzehn Tage hätte dauern sollen, aber Spielraum bis
heute hatte, das sind drei Tage mehr. Ich erstelle derweil
das Zertifikat."

Erleichterung durchströmte Lilið. Sie hätte an dieser Stelle
am wenigsten damit gerechnet, dass das Nautika die Begründung
einfach schlucken würde.

"Geht klar!" Das andere Nautika suchte aus einem hohen Schrank
auf der anderen Seite des Raums eine weitere Karte hervor, schob
anschließend die beiden Karten auf dem Tisch zusammen, sodass
Platz für die neue war.

Auf Lilið machte das zweite Nautika einen fröhlicheren,
lockereren Eindruck als das erste, was interessanterweise
weder dazu führte, dass es ihr sympathischer gewesen wäre, noch dazu,
dass die Prüfung besonders unaufmerksam von seiner Seite von
statten gegangen wäre.

Es war keine schwierige Prüfung. Lilið
sollte einfach ihre Route erklären. Zunächst also stellte
sie die Karte auf den Tag ein, an dem sie losgesegelt wäre, und
bewegte dann das Kartensteinchen darüber.

Lilið hatte Angst gehabt, dass sie die ersten Tage nicht mehr
in klarer Erinnerung haben könnte, aber die Angst stellte sich als
unbegründet heraus. Euphorie durchströmte sie, immer dann, wenn
sie genau wusste, wo sich diese Karte von ihrer unterschied, wo
diese etwas feiner war als die, die sie benutzt hatte, und
etwas gröber als die, die sie bei Heelem benutzt hatte. Sie
mochte den Unterschied der Karten erfühlen. Sie ließ sich
in ihrem Fluss nicht davon ablenken, als ein weiteres Nautika durch
die Tür trat und seinen Mantel an die Garderobe hängte.

"Seid ihr zur Sitzung mit der Prüfung durch?", fragte
jenes.

Irgendwer bestätigte. Das neu hinzugekommene Nautika holte
sich eine Tasse, setzte sich mit an den Tisch und beobachtete,
über den frisch eingegossenen Tee pustend. Es schien Lilið
nicht zu erkennen und niemand klärte es auf.

Das prüfende Nautika beobachtete sie bei allem haargenau, stellte bohrende
Fragen, aber sehr bald rechnete Lilið schon im vorhinein damit,
wann eine kommen würde, und beantwortete sie mit weniger Zögern. Das
Nautika freute das sichtlich.

"Es besteht kein Zweifel! Du bist die Route wirklich selbst
gesegelt.", schloss es, als Lilið das Kartensteinchen
in den Nederoger Handelshafen bewegte. "Aber ich mag anmerken,
dass du heute Nacht", das Nautika unterbrach sich, und führte
den Satz vielleicht anders zu Ende als geplant, "auf Danmoge
warst."

Lilið verstand sofort. Wieder wurde ihr heiß. Das war der
dezente Hinweis darauf, dass sie beim Lügen erwischt worden
war, auf eine Art formuliert, die diesen Umstand
dem hinzugekommenen Nautika nicht in der Deutlichkeit verriet.

Das Nautika, das damit beschäftigt gewesen war, das Zertifikat
zu erstellen, schritt zu ihnen an den Tisch und legte ein kleines,
stabiles Schriftstück vor ihr ab. Es bestand aus so etwas wie geöltem
Seegras-Wollpapier, eine Papierart, die besonders wasserbeständig
war. Es war ein Zertifikat auf ihren Namen als Nautika. Lilið
blickte überrascht auf. "Nautika?", fragte sie. Das Zertifikat,
das Heelem ihr versprochen hatte, hätte sie nur als Leicht-Nautika
ausgewiesen.

"Die Freiheit habe ich mir erlaubt.", antwortete das Nautika. "Heelem
ist durchaus bekannt dafür, zwar Leuten in vertrackten Situationen
Karrieremöglichkeiten zu verschaffen, was ich sehr begrüße, aber
eben auch dafür, knauserig mit Auszeichnungen zu sein, es sehr genau zu nehmen. Ich
sehe, was du in kurzer Zeit geleistet hast. Ich sehe, dass das nicht
ganz den Prüfungsbedingungen zum Nautika entspricht, aber du bist
ehrgeizig und ich vertraue darauf, dass du dem Zertifikat schon bald gerecht
werden und vorher die leichte Übertreibung meinerseits
nicht ausnutzen wirst."

Eine Gänsehaut kribbelte Liliðs Rücken hinunter und ihr Atem
blockierte einen Moment. "Danke!", hauchte sie. "Ich werde alles
daran setzen."

Gerade in dieser Situation wäre es auch hilfreich, dachte Lilið. Die
Crew, bei der sie nun anheuern wollte, erwartete ein Nautika. Sie
hätte irgendwie erklären müssen, warum sie nur Leicht-Nautika gewesen
wäre, und hatte sich Begründungen zurechtgelegt, die sie nun nicht
mehr brauchen würde.

"Frauen werden leicht unterschätzt, das weiß ich.", fügte das
Nautika hinzu und zerbrach damit für sie diesen schönen Moment. "Lass
dir nicht einreden, du wärest nicht gut genug. Das, was du
gemacht hast, war eine nicht zu unterschätzende Leistung. Ich
würde dir empfehlen, als nächstes eine Kagutte zu navigieren,
aber ich habe keine Ahnung, wie ich dir dabei helfen kann, eine
zu finden, die dich zu Ausbildungszwecken nehmen würde."

Lilið nickte wieder. Sie wusste nicht, was sie antworten sollte. Wären
nur die beiden Nautikae vom Anfang dagewesen, hätte sie vielleicht
darüber geredet, dass sie erst einmal Land gewinnen würde. "Danke.", sagte
sie einfach wieder.

"Hier ist in einer Viertelstunde Versammlung.", verkündete das
Nautika. "Wenn du nicht auf zu viele Menschen treffen möchtest,
würde ich dir empfehlen, zu gehen."

Ein verschleierter Rauswurf, aber auch ein guter Wunsch, interpretierte
Lilið. Sie nickte.

"Will sie nicht hinterher mit uns feiern?", fragte das später
hinzugekommene Nautika.

"Soweit ich sie kenne, war Feiern nie ihr Ding.", antwortete
das Nautika, das das Zerifikat vor ihr abgelegt hatte und es erst
jetzt loslies.

Lilið nickte. "Ich bedanke mich für alles. Und ich hoffe, ich komme
mal wieder.", sagte sie.

Bevor sie den Raum verlassen konnte, gratulierten ihr die anderen
beiden Nautika voll herzlicher Freude. Auf der Treppe kam ihr
noch eine Person entgegen, die sie stirnrunzelnd anblickte, kurz
grüßte, aber kein Gespräch mit ihr anfing. Vielleicht war sie also
noch einmal erkannt worden. Sie hoffte, dass sie das drinnen geklärt
bekämen, oder dass ohnehin einfach ein Interesse bestand, sie nicht
zu verraten.

Unten im Hafen verzog sie sich erst einmal in eine ruhige
Ecke, um sich einen Moment von der Aufregung zu erholen. Die
Sonne stand bereits überraschend tief. Sie konnte sich also nicht viel
Zeit nehmen, war vielleicht schon etwas zu spät. Durch die Prüfung
war doch recht viel Zeit vergangen.

---

Lilið entfaltete ihre Jacke mit einem Schütteln in den
Mantel. Er machte dabei ein flatterndes Geräusch, fast wie
ein Segel, das der Wind in seine bauchige Form brachte. Sie
mochte das fliegende Gewicht in den Armen und die Eleganz
der Flugbahn, als sie ihn um ihren Körper schwang und
die Arme einfädelte. Fast hätte sie vergessen, ihre Brüste
wegzufalten, aber das holte sie rasch nach.

Auch zu diesem Treffen würde sie ihr Gesicht nicht verändern. Der
Grund war, dass sie mit dieser Crew mehrere Tage Zeit am Stück
verbringen würde. Das Risiko war über diese Spanne hinweg einfach
zu hoch, fast schon als Tatsache gegeben, dass ihr irgendwann
die Gesichtszüge entgleiten und die Falten im Gesicht vor
den Augen anderer aufgehen würden. Sie grinste, als ihr die
Ähnlichkeit mit einer Redewendung bewusst wurde.

Die Crew hatte sich laut der Einladung des Nautikas auch in
Angelsoge zusammen eingefunden, um die Kronprinzessin bei
ihrer Heimreise vom Angelsoger Adelsball, auf dem diese
selbst nur kurz gewesen war, zurück zum Hof der Monarchie-Familie
zu entführen, aber das war viel später in die Tat umgesetzt worden, als
Lilið und Marusch losgefahren waren. Die Crew hatte mit ihrer
Kagutte nur sechs Tage hierher gebraucht. Für eine Kagutte
war das kein so komplex zu navigierender Reiseabschnitt, weshalb
sie dabei nicht so dringend auf die Hilfe eines Nautikas angewiesen gewesen
waren. Sie hatten dadurch, keines dabei zu haben, höchstens einen Tag verloren. Im Brief
war die Rede davon, dass sie ein Crewmitglied schon lange vor
der Entführung zurückgelassen und mit der Suche nach einem
passenden, verschwiegenen Nautika betraut hatten, das heute hier
dazustoßen sollte.

Die Crew würde also wahrscheinlich zum größten Teil aus Leuten
aus dem Königreich Sper bestehen, aber auch aus ein paar Leuten, die
überwiegend in Angelsoge zu Hause oder nirgendwo
waren. Entsprechend hatten Marusch und Lilið die
Wahrscheinlichkeit als hoch eingeschätzt, dass niemand
Lilið kennen würde, sie also mit ungefaltetem Gesicht keine
Probleme bekommen sollte.

Lilið atmete tief durch, kontrollierte noch einmal ob die Briefe
und das Zertifikat in sinnvoll gewählten Taschen waren, und fühlte
sich in die Rolle eines Nautikas aus Angelsoge ein, das gewillt
war, eine entführte Kronprinzessin gen Königreich Sper zu
navigieren. Es war ihre bisher größte Rolle. Sie fühlte sich in
die Ängstlichkeit ein, die das Nautika ausgestrahlt hatte, dem
sie in Danmoge begegnet waren. Trotzdem versuchte sie, auch
ein gutes Stück sie selbst zu bleiben, um die Rolle überzeugend
spielen zu können.

Dann schritt sie in den Werftteil des Hafens. Es gab
hier einige Bootshallen, aber Lilið entdeckte bald die
einzige, an der eine Treppe in einem von der Straße versteckten
Winkel angebracht war und in einen Raum darüber
führte. Zumindest war es die erste Treppe, die sie
fand und es schadete nicht, sie auszuprobieren. Der Abend dämmerte
bereits. Es war abgesehen von ungefähr fünfzehn Öwenen, die
hier herumwatschelten oder sich ausruhten, eine verlassene
Gegend. Sie stieg die wetterzerschlissene Treppe hinauf, spürte
das Gewicht des Mantels bewusst auf den Schultern. Oben angekommen
hörte sie auf die Geräusche. Geräusche einer Feier, dachte
sie, einer Gruppe, die bestimmt
zwanzig Personen umfasste. Eine große Crew für eine Kagutte, aber
sie hatte unten im Hafen auch eine gut bewachte, große Kagutte
gesehen.

Sie klopfte, aber niemand hörte sie. Also schob sie die Tür einfach
auf und trat ins dämmrige Licht eines zu einem Barraum umfunktionierten
Dachbodens. Es waren in der Tat viele Leute darin, standen in Grüppchen
im Raum verteilt, unterhielten sich. Manche lachten. Sie schritt etwas
weiter in den Raum hinein, vielleicht, weil sie wie natürlich und
nicht wie wartend aussehen wollte, um die Aufmerksamkeit nicht auf
sich zu ziehen. Einige der Grüppchen blickten tatsächlich wieder
weg, aber sie behielt die Aufmerksamkeit der Mehrheit. Was sie
nicht wunderte. Mit einem Mantel eines Nautikas in einen Raum
zu treten, machte immer Eindruck auf Leute.

In der Mitte, nun fast vor ihr, befand sich ein Tisch, um den
einige Personen in zerschlissenen Sesseln herumsaßen und ein Würfelspiel spielten.
Schräg dahinter stand eine einzelne Person und starrte sie förmlich
an. Im Raum wurde es leiser, auch andere Menschen beobachteten
sie weiterhin, aber dieser Blick bohrte sich
gefühlt in sie hinein. Ihr wurde wieder heiß und dann für einen Moment
schwindelig. Sie erkannte ihn vielmehr an Kleidung und der Art, wie
er sich nun auf sie zubewegte, am Räuspern und an seiner Brille, als
an seinem Gesicht.

Lilið sprang. Irgendetwas sagte ihr, dass sie schneller verschwinden
musste, als sie zur Tür hinaus wäre. Sie hatte sich mit Marusch
zusammen immer wieder eingeprägt, dass einfach wegzulaufen bei
einer Crew sinnlos wäre, die in der Lage gewesen war, die Kronprinzessin
zu entführen und mit jener zu entkommen. Es lag auf der Hand, dass
eine solche Crew einen Fluchtversuch nicht geduldet hätte, wenn
sie belauscht worden wäre, und dann kurzen Prozess gemacht hätte. Ihre
einzige Möglichkeit, wenn sie aufflog, war, sich zu falten und in
gefaltetem Zustand irgendwann zu entkommen. Und aufgeflogen war sie
gerade, glaubte sie. Auch wenn sie nicht sicher war, ob sie
ein Erkennen im Gesicht von Herrn Hut entdeckt hatte, gestarrt
hatte er genug. Sie sprang in eine Faltung, dachte nicht darüber
nach, was für eine, ließ ihre Reflexe übernehmen, weil sie selbst nicht klar
denken konnte.

Schmerz überkam sie, weil es eine sehr enge Faltung war, und
Ärger, weil sie damit hätte rechnen können, dass Herr Hut hier
irgendwo war. Er hatte schließlich Gelegenheit gehabt mit den
Nautikae zu reden. Und warum sollte Herr Hut das tun? Nicht
nur, weil er im Handelshafen war, sondern auch, weil er Ausschau
nach einem Nautika hielt. Das hielt sie zumindest für eine
naheliegende Folgerung.

"Wie unhöflich, einfach zu verschwinden.", hörte sie sehr
dumpf eine Stimme, die nicht zu Herrn Hut gehörte.

Dem Geräusch nach zu urteilen, öffnete und schloss jemand die Tür.

Lilið versuchte, zu erfühlen, was sie eigentlich war. Ihr Körper
war irgendwo hart aufgeschlagen, was sie aber unter dem Schmerz, den
die Faltung an sich schon mit sich brachte, kaum gespürt hatte, eher
gehört. Sie hatte Augen, stellte sie fest, kleine, punktförmige
Augen. Also war sie vielleicht ein Käfer oder so etwas.

"Er kann nicht weit sein.", meinte eine andere Stimme. "Hut, war
das unser Nautika?"

"Sieht fast so aus.", hörte Lilið die Stimme von Herrn Hut, die
sie mühelos wiedererkannte. "Der Mantel kam mir bekannt
vor, aber er sah aus wie die Tochter von Lord
Lurch, dieser Witzbold. Ich frage mich nur, warum?"

"Er sah aus, wie die Tochter von Lord Lurch?", fragte die Stimme
von vorher. "Warum denkst du, dass sie es nicht war?"

"Ich wüsste nicht, was die hier verloren hat.", antwortete Herr
Hut. "Sie kann es außerdem nicht sein. Einfach zu verschwinden
ist schon ein Akt fortgeschrittener Magie, und ich weiß, dass
diese Frau dahingehend nichts drauf hat. Ich war selbst
damit betraut, ihren Skorem nachträglich erneut einzuschätzen. Ich
habe ihn auch damals schon eingeschätzt, als die Göre ein Kind
war. Auf 80, und dazu stehe ich eigentlich auch heute noch." Er
lachte bitter auf. "Diese Familie ist echt der letzte Dreck. Die
Skoremetrika, die das Gör neu bewerten sollte, weil sie es besser
kann, weil sie ja eine Frau ist,", Herr Hut zog das Wort 'Frau'
ironisch in die Länge, "ist voll auf den Trick reingefallen, den
sie ihr dargelegt haben. Beim ersten Test konnte Lilið noch nicht falten,
aber draußen, als sie wussten, was für Magie gefragt
ist, oh Wunder, pustet der Wind das Testpapier weg, und
als ein Diener es wiederbrachte, war es gefaltet. Natürlich hat
niemand gesehen, wer es gefaltet hat, außer ich. Der Diener
natürlich."

"Komm zum Punkt!", orderte eine ziemlich laute Stimme an. "Wenn
wir belauscht wurden, müssen wir unseren Ort wechseln. Heikon,
Janne, sucht draußen, ob ihr wen findet. Hut, Drude, könnt ihr
hier Magie fühlen?"

Lilið versuchte zu schlucken, aber ihre Speiseröhre war kreuz
und quer durch ihren Körper verlegt. Das Wasser und die Beklemmung
flossen zurück in irgendeinen Teil ihres Mundes, der immerhin innen
lag, sodass sie keine Spuckeflecken hinterlassen würde.

Personen, die die Fähigkeit beherrschten, Magie zu erfühlen, die
andere ausübten, waren selten. Aber in dieser Gruppe waren
offensichtlich zwei, die es konnten.

"Fehlanzeige!", antwortete Herr Hut. "Er muss schon auf
und davon sein. Ich war mir, wie ich sagte, bereits beim
Kennenlernen nicht sicher, ob er wirklich vertrauenswürdig
ist, aber er war die beste Wahl, die ich bieten konnte. Wenigstens
politisch stabil und das ist die Hauptsache. Nicht?"

Lilið hörte wieder die Tür. Sie versuchte, ein Auge zu öffnen,
was ihr schließlich auch gelang. Es war nicht leicht, sie musste
vorsichtig an den Falten bei den Augenvertiefungen
zuppeln, damit sich das Augenlid
nicht verklemmte. Es war ein kleines Auge und sie sah daraus
farbverkehrt. Der Raum lag riesig vor ihr. Grobe
Maserung von Holz erstreckte sich vor ihrem Auge. Sie hatte
mehr als zwei, stellte sie verwirrt fest. So ein Tier
wollte sie eigentlich nicht sein, war sie auch noch nie
gewesen. Trotzdem versuchte sie, nach und nach Kontrolle über
ihren gefalteten Körper zu gewinnen, um vielleicht aus
dem Raum herauszukrabbeln. Vielleicht wäre auch der Weg zur
Tür ziemlich weit und sie würde einfach abwarten, bis die Crew
verschwunden wäre. Dann würde sie sich entfalten und auf Marusch
warten, in der Hoffnung, dass diese anheuern könnte. Oder sie würde
sich doch das Gesicht falten und es ein zweites Mal versuchen.

Sie öffnete ein zweites Auge, dessen Perspektive irgendwie
nicht so richtig zur ersten passte. Hatte sie ein Auge
im Hinterkopf? Durch dieses Auge hatte sie einen ganz guten
Blick in den Raum. Die Leute waren ruhiger geworden, tuschelten
eher. Herr Hut stand neben einer beeindruckenden, großen
Person, die im Gegensatz zu Herrn Hut tatsächlich einen
Hut trug, und schien ein Redebedürfnis zu haben. Aber
die Person mit Hut gab stattdessen noch einen weiteren
Befehl. "Stell dich neben die Tür, Drude, damit du uns
direkt sagen kannst, ob eine Person, die eintritt,
Chameleonmagie anwendet." Lilið vermutete, dass
die Person mit den Befehlen Kapitän war.

Auch der Bildausschnitt des dritten Auges, das sie
nun öffnete, zeigte ihr eine
andere Seite des Raums. Eine relativ große, kräftige Person in
einem dunklen Mantel und darunter eng anliegender Kleidung
schritt an ihr vorbei Richtung Tür. Das musste wohl Drude sein. Die
Person geriet dabei aus dem Blickfeld des dritten Auges in das
des ersten, und als sie sich neben der Tür umdrehte, trafen
sich ihre Blicke in einer Art, dass es Lilið nicht wie
Zufall vorkam. Drude hatte sie vielleicht bemerkt, aber
sagte nichts. Warum? Weil Drude keine Lust hatte, gegen
den sexistischen Herrn Hut die Stimme zu erheben?

"Wir warten noch eine halbe Stunde ab, dann wechseln
wir auf die Kagutte.", beschloss die Person, die Lilið
für den Kapitän hielt. "Wir versuchen dann eines der
Nautikae aus dem Hafen anzuwerben und zu verschweigen, was
wir für eine Ladung führen. Die Lage ist zu brenzlich,
hier zu lange zu warten."

Sie musste Marusch informieren, dachte Lilið. Sie musste
hier irgendwie rauskommen. Wenn sie es schaffte, rechtzeitig
hier herauszukommen, könnte sie sich vielleicht ja noch
als eines der Nautikae im Hafen anwerben
lassen. Aber wenn sie sich einfach entfaltete, wäre sie
wohl geliefert. Sie versuchte, an ihrem neuen Körper
Gliedmaßen zu finden, aber stattdessen wurden ihr nur
noch mehr Augen bewusst. Sie zählte sie, weil sie an nichts
anderes denken konnte. Es waren einundzwanzig. Welches
Tier hatte einundzwanzig Augen?

Die fünf Augen, mit denen sie seit eben an die Decke
starrte, schloss sie rasch wieder, was vielleicht eine natürliche
Reaktion war, wenn eine riesige Hand sich auf jene zubewegte. Sie
wurde gegriffen und angehoben und dachte, nun war es aus, nun
hatten sie sie entdeckt. Aber die Person behielt sie nicht in
der Hand. Stattdessen wurde sie in etwas geworfen, vielleicht
ein Becher, aber weicher, in dem sie leicht gefedert aufkam. Es
klapperte, als sie durchgeschüttelt wurde und gegen
würfelförmige Gegenstände stieß, die ungefähr so groß waren wie
sie. In einer Ecke ihres Bewusstseins (und es belustigte sie
unpassender Weise, dass ihr Bewusstsein vielleicht tatsächlich
in einer Ecke sitzen mochte, weil sie davon acht hatte), wurde
ihr klar, dass sie selbst ein Würfel war. Eine äußerst
unpraktische Form, wenn es darum ging, hier wegzukommen. Sie
war getarnt: Ein Würfel auf einem Tisch, wo gewürfelt wurde,
fiel nicht auf. Sie war fest genug verfaltet, dass ihr Körper
beim Schütteln nicht aufsprang. Aber sie konnte sich nicht
davonbewegen. Und viel schlimmer war: Sie würde auch nicht
zurückgelassen werden.
