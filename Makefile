SHORT_TITLE = WASUS
CHAPTERS := \
	00-Vorwort.md\
	00-Prolog.md\
	ArcI.pa\
	01-SkoremetrikaeImWohnzimmer.md\
	02-Weg.md\
	03-EinLangfingerZumNaechtlichenTee.md\
	04-Tauschgeschaeft.md\
	05-Welten.md\
	06-FruehstueckBeiEinerMedika.md\
	07-Anziehsachen.md\
	08-DasTanzdesasterVonLilithAusDerUnterwelt.md\
	09-EinbruchBeiDunkelheit.md\
	10-Diebesnest.md\
	11-Diebeserklaerung.md\
	12-Davonstehlen.md\
	13-UebersMeerRauschen.md\
	14-DieBeschaffenheitDerDinge.md\
	15-WindUndWetter.md\
	16-ImUnsicherenHafen.md\
	17-EinbruchUndAufbruch.md\
	18-Durchbruch.md\
	19-Seiten.md\
	20-TauschenUndTaeuschen.md\
	21-Anhaengen.md\
	22-UnterDemDeckmantel.md\
	23-DieWuerfelFallen.md\
	ArcII.pa\
	24-Ausmasse.md\
	25-ZumLetztenMahl.md\
	26-DasNautikaDerKoeniglichenGarde.md\
	27-Einleben.md\
	28-Einschleichen.md\
	29-DieKoeniginDerNacht.md\
	30-Vorhaben.md\
	31-AmWeltgefuegeRuetteln.md\
	32-BeimWartenTraeumenNachgehen.md\
	33-PlanA.md\
	34-BeimEntwischenErwischen.md\
	35-Umorientieren.md\
	36-Untertauchen.md\
	37-DieQualDerKarrierewahl.md\
	38-GehoertWerden.md\
	39-ReineMachen.md\
	40-Schweigen.md\
	41-Staub.md\
	ArcII-2.pa\
	42-StilleNacht.md\
	43-EinZurueckMehr.md\
	44-EinenFangMachen.md\
	45-EinenGutenDrahtHaben.md\
	46-AusStaubMachen.md\
	47-Treffsicherheit.md\
	48-NachUnsDieSinnflut.md\
	49-Epilog.md
INPUTS := $(CHAPTERS:%=chapters/%) metadata.yaml
OUTPUTS := WorteAusSchallUndStaub.pdf WorteAusSchallUndStaub.epub WorteAusSchallUndStaub.tex WorteAusSchallUndStaub.html WorteAusSchallUndStaub.odt ContentNotes.tex $(SHORT_TITLE)/ContentNotes.md $(SHORT_TITLE)
SPBUCHSATZV := SPBuchsatz_Projekte_1_4_6
#SPBUCHSATZV := SPBuchsatz_Projekte_1_4__Pre_Beta_11
#SPBUCHSATZV := SPBuchsatz_Projekte_1_3

all: $(OUTPUTS)

WorteAusSchallUndStaub.epub: $(INPUTS)
	pandoc \
		-t epub2\
		--css epub.css\
		$(INPUTS) \
		-o $@

WorteAusSchallUndStaub.html: $(INPUTS)
	pandoc \
		$(INPUTS) \
		-o $@

WorteAusSchallUndStaub.odt: $(INPUTS)
	pandoc \
		$(INPUTS) \
		-o $@

WorteAusSchallUndStaub.tex: $(INPUTS)
	~/.local/bin/pandoc \
		--top-level-division=chapter \
		-t SPBuchsatz.lua \
		$(INPUTS) \
		-o $@

ContentNotes.tex: chapters/ContentNotes.md
	~/.local/bin/pandoc \
		--top-level-division=chapter \
		-t SPBuchsatz.lua \
		$^ \
		-o $@

# changed Ifstr to ifstr in SPBuchsatz_System/Helferlein/Hilfsfunktionen.tex 
WorteAusSchallUndStaub.pdf: WorteAusSchallUndStaub.tex ContentNotes.tex
	cp WorteAusSchallUndStaub.tex \
		deps/$(SPBUCHSATZV)/Buch/tex/Haupttext.tex
	cp ContentNotes.tex \
		deps/$(SPBUCHSATZV)/Buch/tex/ContentNotes.tex
	cp TitleAuthor.tex \
		deps/$(SPBUCHSATZV)/Buch/tex/TitleAuthor.tex
	cd deps/$(SPBUCHSATZV)/Buch/build/ ; \
		lualatex ../tex/Buch.tex
	cp deps/$(SPBUCHSATZV)/Buch/build/Buch.pdf WorteAusSchallUndStaub.pdf

$(SHORT_TITLE)/titlelist.txt: Makefile
	mkdir -p $(shell dirname $@)
	@echo '$(filter-out %.pa, $(CHAPTERS))' | xargs -n 1 | cat -n >$@

get_chapter_number = $(shell \
	grep -E '^[[:digit:][:space:]]+$(1:chapters/%=%)$$' $(SHORT_TITLE)/titlelist.txt | \
		awk '{ print $$1 - 1 }' \
)

$(SHORT_TITLE)/%: chapters/% $(SHORT_TITLE)/titlelist.txt
	@echo '$@'
	@echo >$@ '---'
	@echo >>$@ 'layout: $(SHORT_TITLE)-chapters'
	@echo >>$@ 'page-category: story-chapters-$(SHORT_TITLE)'
	@echo >>$@ 'title: "$(call get_chapter_number,$<) - $(shell \
			head -n 1 '$<' \
		)"'
	@echo >>$@ 'number: $(call get_chapter_number,$<)'
	@echo >>$@ 'lang: de'
	@echo >>$@ '---'
	@cat $< >>$@

$(SHORT_TITLE)/ContentNotes.md: chapters/ContentNotes.md $(SHORT_TITLE)/titlelist.txt
	@echo '$@'
	@echo >$@ '---'
	@echo >>$@ 'layout: $(SHORT_TITLE)-chapters'
	@echo >>$@ 'page-category: story-chapters-$(SHORT_TITLE)'
	@echo >>$@ 'title: "Content-Notes"'
	@echo >>$@ 'number: 0'
	@echo >>$@ 'lang: de'
	@echo >>$@ '---'
	@echo >>$@ ''
	@cat $< >>$@

$(SHORT_TITLE): $(filter-out %.pa, $(CHAPTERS:%=$(SHORT_TITLE)/%)) ;

clean:
	rm -rf $(OUTPUTS)

.PHONY: clean $(SHORT_TITLE)

