Ein Langfinger zum nächtlichen Tee
==================================

*CN: Einbruch, Grusel, Vergiften*

Die Zeitanzeige kroch nur langsam dem verabredeten Zeitpunkt
entgegen. Alles wirkte still hier, aber das konnte täuschen, das
wusste Lilið. Sie tat sich schwer damit, nicht einzuschlafen, und
sah immer wieder auf die große Standuhr. In der Wartezeit hatte sie sich
das Buch angesehen. Erst hatte sie überlegt, es könnte auch
einfach Dreck auf den Seiten sein. Die Zeichen wirkten so
unsortiert. Aber es war schwarze Farbe, die nicht so wirkte, als
ließe sie sich leicht entfernen oder als wären irgendwelche
sandartigen Krümel darin, die auf Dreck hätten schließen lassen. Wenn Dreck, dann
am ehesten Schmieröl, das über die Zeit schwarz geworden war, aber
wie sollte so etwas halbwegs gleichverteilt auf ungefähr 50
Seiten gelangen?

Lilið gewann das kleine, unsinnige Buch rasch so lieb, dass sie
es in ihrer Fluchtjacke tragen wollte. Dünn genug war es dafür. Damit
es ihm dabei gut erginge, schlug sie es in Leder ein und nähte eine
etwas zu große Tasche in jener wasserdichten Jacke kleiner, damit es genau
passte. Die Jacke, in der all ihre wertvollsten Habseligkeiten lebten
und die sie immer griffbereit hatte.

Als es endlich Aufbruchszeit war und sie durch ein
Fenster beobachtete, wie tatsächlich eine gelangweilte, müde Wache vom Besuchshaus
den Weg Richtung Haupthaus einschlug, zog sie die Jacke über, schnürte ihr Reisegepäck
an den Körper und schlich vorsichtig die Treppe hinunter. Ein Windhauch
bewegte sich durchs Treppenhaus, sonst war alles still. Lilið war
trotzdem sofort hellwach, viel wacher, als es zwei halb durchgemachte
Nächte und zwei anstrengende Tage erlauben sollten. Da sollte
kein kalter Windhauch sein. Die Tür hatte sie nicht offen stehen
gelassen. Sie lauschte sehr intensiv und hörte gerade noch, wie sie
sehr leise verschlossen wurde. Jemand ließ bewusst die Türfalle nicht
einfach einschnappen, sondern hob die Klinke langsam wieder an. Lilið
hatte ein Bild einer Gestalt in ihrer Vorstellung vor Augen, die die
Tür mit gezielter Kraft zuvor in eine Position fixierte, in der das Einrasten der
Schlossfalle am geräuschärmsten wäre. Es war nur ein blasses Klicken.

Lilið dachte darüber nach, Alarm zu schlagen. So ein Verhalten war
nicht typisch für die Wachen. Aber Alarm zu schlagen, hätte ihre
Flucht vereitelt. Lilið beschloss, sich die Sache zunächst
genauer anzusehen, und wenn sie brenzlich würde, das
Alarmschlagen nachzuholen.

Sehr vorsichtig schlich sie die Treppen weiter hinab, bis sie in den
Eingangsbereich sehen konnte. Sie sah niemanden. Aber die Tür zur
Teeküche bewegte sich lautlos, bis sie wieder so angelehnt war, wie
Lilið sich erinnerte, sie zurückgelassen zu haben. Die Bewegung der Tür wurde
abrupt abgebremst, wohl damit die Tür sich nicht weiterbewegte und
irgendwann doch ein Geräusch verursachen würde, wenn sie den
Rahmen berührte. Da war definitiv kein Windhauch am Werk.

Der Spalt zwischen Tür und Rahmen war zu schmal für Menschen, um sich
hindurchzuschieben. Aber das stellte für Lilið nur ein mäßiges
Hindernis dar. Sie atmete einige Male tief ein und aus, dann
hielt sie die Luft an und machte sich flach.

Sie verstand Papier und manche andere Bastelgegenstände auf
Anhieb so gut, dass sie sie durch Magie und Hautkontakt
falten konnte, ohne wirklich nachzudenken. Gegenstände, die nicht
speziell zum Basteln gedacht waren und die sie noch nicht so gut
kannte, konnte sie nicht mal eben falten. Aber ihren Körper
beherrschte sie so sehr, dass sie ihn falten konnte, obwohl
er so komplex war. Fast beliebig. Allerdings musste sie bei starker Faltung
die Luft anhalten.

Nachdem sie mit dem Kopf voran, flach wie eine Tischplatte, durch
die Tür gelangt war, nahm sie wieder ein wenig an
Dreidimensionalität zu und schob sich an der Wand entlang
noch etwas mehr in den Raum hinein, bis sie einen guten
Überblick in alle Nischen hatte. Nun atmete sie wieder, sehr
flach, aber völlig ausreichend. Die Gestalt, die gerade
damit beschäftigt war, kontrolliert leise Schubladen aus Schränken
zu ziehen, diese vorsichtig zu durchsuchen und hinterher wieder
zuzuschieben, bemerkte trotzdem etwas und wandte sich um. Das
hatte Lilið einberechnet. Sie hielt die Luft abermals an und
machte sich flach wie Tapete, sich so faltend, dass sie ungefähr wie die
Wand hinter sich aussah. Sie hatte es oft geübt, aber noch nie
in einem Ernstfall gebraucht. Sie verharrte sehr ruhig, solange der Blick
der Person auf der anderen Seite des Raums auf sie gerichtet war. Die
Gestalt wirkte wenig hektisch, sie stand einfach still
da. Sie trug eine langes Hemd, das mit einem Gürtel mit allerlei
Taschen darin an den Körper gebunden war. Lilið vermutete Werkzeug
zum Schlösserknacken darin und musste fast schmunzeln. Ein Langfinger
wohl.

Ihr wurde schon allmählich schwummrig, als sich die Gestalt endlich
wieder ihrem Tun mit den Schubladen zuwandte. Lilið ließ
sehr vorsichtig wieder Luft in ihre Lungen strömen
und faltete sich für den Moment ganz aus, blieb einige Atemzüge einfach
stehen und beobachtete, wie die Person weiter leise Schubladen
durchsuchte.

Was sollte sie tun? Wenn es ein Langfinger gewesen wäre, der bloß Essen
stehlen gewollt hätte, hätte sie ihn vielleicht einfach machen lassen. Aber
wie er die Schubladen durchsuchte, war er wohl nach etwas anderem
her. Nach wertvollen Dingen, wie Silberbesteck? Oder nach etwas
sehr Bestimmtem? In letzterem Fall sollte sie wahrscheinlich durchaus
die Wachen informieren. Eigentlich war ihr das gerade recht egal, wenn
jemand ihrem Vater wertvolle Dinge stahl, aber zum einen wusste sie sicher
nicht über alle Schätze und ihre Bedeutung Bescheid, und zum anderen
würde sie ihre Mutter gefährden, wenn sie den Langfinger bei
so etwas gewähren ließe. Ihrer Mutter wegen waren ja gerade
keine Wachen hier.

Sie beschloss, die Teeküche erst einmal wieder zu verlassen, und sich
draußen Gedanken zu machen, wie sie weiter vorgehen sollte. Sie
wusste auch um die hohen Strafen, die auf Diebstahl standen. Weil
sie selbst ein Diebeswesen war, hatte sie sich sehr genau darüber
informiert. Wenn dieser Langfinger nicht gerade das Kind von irgendeinem
Lord war, würde das bittere Folgen für ihn haben. Vielleicht tödliche. Das
passte ihr eigentlich auch nicht.

Vielleicht konnte sie ihn irgendwie so erschrecken, dass er fliehen
würde. Das wäre es! Sie würde nach draußen schleichen und Krach
machen, sodass er glaubte, entdeckt worden zu sein. Aber eben erst, wenn
sie draußen und selbst sicher wäre.

Sie wandte sich zum Gehen und blickte dabei kurz zur Tür, um
nicht über etwas zu stolpern, aber eine Bewegung im Schatten ließ sie lautlos
herumfahren. Die fremde Person stand unvermittelt direkt vor ihr. Sie
hatte die Strecke zwischen ihnen so leise und rasch
überwunden, dass Lilið Magie dahinter vermutete. Die
Hände, die sie an ihren Oberarmen gegen die Wand drückten, fühlten sich fast
sanft an. Sie klammerten nicht, aber gegen die Kraft der Person hätte sie
sich trotzdem nicht ohne Weiteres stemmen können.

Sie konnte ihren Oberkörper kaum mehr bewegen, wohl aber ihre Unterarme. Mit
der Hand erwischte sie den Lichthahn, wie in einer routinierten und
doch etwas eingeschränkten Bewegung, und drehte das Licht ein wenig
auf. Nur ein wenig. Vielleicht fiel es dann vom Haupthaus nicht auf. Immerhin
war es ein gutes Stück entfernt und die dunklen Vorhänge der Teeküche zugezogen. Im
schwachen Licht erkannte sie ein zartes, ovales Gesicht, von dem
sie mutmaßte, dass es regelmäßig glattrasiert wurde, mit dunkelbraunen
Augenbrauen und Augen darin, die sie überraschend gelassen musterten.

"Bist du auch eingebrochen und wolltest etwas stehlen?", fragte die
Gestalt, ein schwaches Schmunzeln im Gesicht. Und in der Stimme.

Lilið hatte nicht den Plan, sich anzustrengen, möglichst nah
an der Wahrheit zu bleiben. Es galt hier, so
zu antworten, dass sie am besten entfliehen können würde. Dafür wäre
vielleicht gar nicht verkehrt, sich als Langfinger auszugeben. Vielleicht
könnten sie dann einen Pakt schließen, sich gegenseitig nicht zu
verraten, und würden beide rasch fliehen.

"Gehörst du zu den Wachen?", fragte sie, versuchte dabei, Unsicherheit
in der Stimme darzustellen. Das war doch sicher, was ein Langfinger
fragen würde, oder nicht? Die Sorge, von einer Wache erwischt zu
werden, musste doch vordergründig sein.

Innerlich musste sie grinsen, weil Unsicherheit in ihrer Situation auch
so durchaus angebracht gewesen wäre, sie sich aber erstaunlich
wenig unsicher fühlte, diese also spielen musste.

"Klar, da gehöre ich zu!", bestätigte die Person vor ihr. "Deshalb habe
ich mir so gedacht, als ich dich erwischt habe, nachdem ich irgendwie
verdächtig in Schubladen hegerumgewühlt habe, dass ich erst einmal offenlege,
selbst Langfinger zu sein, weil mir das viel naheliegender erschien, als
andere Wachen zu informieren."

"Nun, ich mag ja manchmal schwer von Begriff sein. Du hast recht, dass
mir hätte klar sein müssen, dass du keine Wache bist. Das war meine
große Sorge, deshalb war ich wohl fixiert darauf.", behauptete sie,
den spielerischen Tonfall übernehmend, aber mit etwas Kiebigkeit
ergänzend. "Aber ich habe durchaus begriffen, dass du mich für schwer
von Begriff hältst und mich dafür veralberst."

Zu ihrer Überraschung ließ der Langfinger sie los und nahm einen
Schritt Abstand. Ein unsinniger Teil von ihr wünschte sich die Nähe zurück.
Er hatte gut gerochen und sie hatte die interessanten Gesichtszüge gern aus der
Nähe studiert. Es war ein schönes Gesicht, fand sie. Der Griff war
außerdem zwar kräftig, aber eben auch sanft gewesen.

"Es tut mir leid." Die Stimme klang überzeugend geknickt, so
dass Lilið es der Person durchaus abkaufen mochte. "Mir gefiel es, mir
über meine Motivation als eventuelle Wache, so zu handeln, Gedanken
zu machen und ich mag Ironie. Mir lag es fern, dich abzuwerten. Und
doch verstehe ich, dass das dabei passiert ist. Das tut mir
leid."

Die Person bewegte sich durch den Raum wie es gute Langfinger taten:
Leise, zügig, selbstverständlich, den Blick möglichst überall, und
setzte sich an den Tisch. Sie machte eine einladende Geste.

"Ich soll mich setzen?", fragte Lilið.

"Wieviel Zeit haben wir?" Das Schmunzeln war in die warme Stimme
zurückgekehrt. Eine Stimme, die etwas direkt unter Liliðs Haut
zu elektrisieren vermochte.

Sie nickte. Der Plan stand für sie nach wievor, so eine Art Pakt
mit der Person zu schließen, sodass sie beide abhauen könnten, aber
die Person keine Gelegenheit mehr hätte, noch nach etwas Bestimmten zu
suchen. "Ich weiß nicht, wieviel Zeit wir haben." Das immerhin
entsprach sogar der Wahrheit. Auch wenn sie glaubte, dass ihre
Mutter nicht zu knapp kalkulieren würde, wenn es denn möglich
wäre, bei so etwas sinnvoll zu kalkulieren.

"Willst du einen Tee mit mir riskieren?", fragte der Langfinger
freundlich.

Lilið hob die Brauen. "So sehr vertraust du mir?"

"Du hast bisher nicht Alarm geschlagen, also schätze ich, dass
du mir nichts allzu Böses willst. Oder selbst in Schwierigkeiten
gerätst, wenn ich Alarm schlüge, bevor ich verschwände.", erklärte
der Langfinger. "Du nutzt dein Potenzial, mir zu schaden, jedenfalls
nicht aus, oder du hast keines."

Lilið senkte die Brauen wieder, schmunzelte und bewegte sich in
Richtung Teeherd. "Oder ich bin einfach deinem Charme verfallen.", verkniff
sie sich nicht, verschmitzt zu sagen.

"Ich finde deinen auch ganz reizend." Die Stimme war durchsetzt
von Albernheit, die den Ernst der Lage trotzdem nicht verfehlte. "Vielleicht
kommen wir zu einer Übereinkunft, die uns beide möglichst wenig gefährdet."

Lilið setzte den Kessel auf und nutzte Magie nur zur Entzündung des
Gases, dass sie mit dem Gashahn aufdrehte. Sie hätte es vielleicht
hinbekommen, das Wasser komplett mit Magie zu erhitzen. Aber an
der Zeit, die es gebraucht hätte, hätte der Langfinger vielleicht
einschätzen können, dass sie ihm, was Magie betraf, wahrscheinlich nicht
gewachsen wäre.

"Was für eine Sorte hättest du gern?", fragte sie.

"Die Gleiche wie du.", erwiderte der Langfinger. "Mach einfach
eine kleine ganze Kanne."

"Bedienen lässt dieser Langfinger sich also.", erwiderte sie im
gleichen alberigen Tonfall, den der Langfinger vorhin
angewandt hatte. Sie hatte gute Erfahrungen gemacht, Sympathiegefühle
zu erwecken, wenn sie Verhalten spiegelte.

Sie hatte gerade eine Teedose aus einer Schublade im Schrank entnommen, als sie das
Geräusch des wackelnden Stuhls vernahm und daher nicht ganz
so überrascht war, die Gestalt plötzlich direkt neben sich
zu sehen. Jene legte ihre Hände sanft auf die Kesselwände.

Lilið hatte wieder so einen Moment, in dem sie sich an die Stelle des
Kessels wünschte. Diese sanfte, bestimmte, zweckmäßige Berührung
auf der Haut. Warum fand sie diesen Langfinger so anziehend?

Aber eigentlich war ganz gut, dass sich ihr Blut nur metaphorisch
erhitzte, nicht, wie das Wasser im Kessel das tat, das noch einen Moment
uninteressiert ob der Hände leise vor sich hin flüsterte und dann,
vielleicht als der Langfinger den Kessel begriffen hatte, mit einem Mal sprudelte,
dass die Pfeife pfiff. Er entfernte sie mit einer fließenden Bewegung.

Lilið stellte die Teekanne, in die sie das Sieb mit dem Tee
gehängt hatte, direkt neben dem Langfinger ab. Sie wagte dabei, ihm ein
bisschen näher zu kommen, als eine Etikette es vorgesehen hätte, und
stellte zufrieden fest, dass er nicht zurückwich. Wieder roch
sie seinen Geruch, als er langsam den Tee aufgoss, sich dazu ihr
noch ein Stück näher bewegend, sodass sie sich fast berührten. Er
goss mit einer Eleganz ein, wie sie sie vielleicht Frau Wolle zugeschrieben
hätte. Der Blick, den er auf sie richtete, als die Kanne aufgefüllt
und der Kessel wieder abgestellt war, war fast zu viel. Der Langfinger lächelte
kaum, nur ein klein wenig. Sie fragte sich, ob sie den Gesichtsausdruck vollkommen
überinterpretierte, aber sie war sich doch recht sicher, dass ihm
sehr bewusst war, wie eng sie zusammen standen, und dadurch, dass
sie sich so ansehen, klar abgemacht war, dass sie sich die
Nähe beide ausgesucht hatten.

"Bringst du die Tassen?", fragte er leise. Eine angemessene Lautstärke
für ihre geringe Distanz.

Sie nickte. Sie tat noch etwas verdatterter, als sie es eigentlich
war.

Der Langfinger trug die Teekanne zum Tisch und platzierte sie in der
Mitte der Tischplatte auf einen bereitgestellten Untersetzer. Sie nahm
sich etwas Zeit dabei, die Tassen aus dem Schrank auszusuchen und
zu präparieren, bevor sie sie ebenfalls zum Tisch trug.

"Er muss nicht lange ziehen.", sagte sie mit einem Blick in die Kanne,
sich dem Langfinger gegenüber niederlassend. Wenn er füßeln wollte, würde
sie vielleicht nicht ausweichen, überlegte sie. Und nicht nur, um
ihm etwas vorzumachen. "Was für eine Übereinkunft hattest du dir
vorgestellt?"

"Ich bin auf der Suche nach einer bestimmten Kostbarkeit. Oder
wahlweise nach einer meisterhaften Diebesperson, die diesen
Gegenstand gestohlen hat oder mit mir stehlen könnte. Er ist sehr
gut bewacht. Oder bereits geklaut. Und dann vermutlich auch
gut bewacht.", erklärte der Langfinger sein Anliegen. Nebulös und
zugleich durch die ganzen Eventualitäten darin
ein bisschen witzig, fand Lilið. "Von deiner Kleidung
zu schließen hast du Diebstahl eigentlich nicht nötig zum
Überleben. Von deinen Bewegungen zu urteilen wärest du wiederum
recht gut darin."

"Nun ja, aber du hast mich entdeckt.", wandt Lilið ein. "So meisterhaft
kann ich nicht sein. Also, wenn dein Ansatz war, mich zu verdächtigen,
besagte, -- möglicherweise gar nicht existente --, meisterhafte Diebesperson
zu sein."

Die Diebesperson vor ihr verschränkte die Hände, um das Kinn
hineinzulegen und lächelte. "Nein, für so meisterhaft halte
ich dich eigentlich nicht. Ohne es böse zu meinen oder
dich abwerten zu wollen.", räumte sie ein. "Noch nicht zumindest."

Unter Liliðs Haut kribbelte es überraschend warm. Sie überspielte das
Gefühl, indem sie den Tee in die Tassen goss, bemüht wenigstens
ein wenig elegant dabei zu wirken. Aber ob ihre dünn-ledrigen,
fingerlosen Segelhandschuhe das Bild von Eleganz eher untermalen oder
schädigen würden, blieb ein Streitthema. Sie selbst fand sie durchaus
sehr schön. "Du möchtest mich anwerben und gegebenenfalls
trainieren?", riet sie als nächstes.

"Wenn dir das in den Kram passt, vielleicht.", antwortete
der Langfinger.

Der Dampf stieg aus den Tassen zwischen ihnen in die nur fahl
erleuchtete Teeküche hinauf. Lilið mochte das Bild, mochte
die Spannung und das Kribbeln dieser Begegnung. Sollte sie
sich anheuern lassen? Es fühlte sich ein wenig unreal
an.

Eine unscheinbare Bewegung und ein Maunzen von
der Tür her ließen sie kurz in die Wirklichkeit zurückkehren. Sie
erschreckte sich und erinnerte sich daran, dass die Situation
für sie gerade nur so sicher war, weil sie hier daheim war. Das
Maunzen kam von einer der nachtaktiveren Katzen, die
sich mit erhobenen Schwanz in die Küche schlich und leise und
körperbeherrschend auf den Tisch hopste. Lilið frustrierte
es ein bisschen, dass sie sich zum anderen Langfinger legte. Er
senkte den Blick nicht, als er ihr über das Fell strich,
entspannt und beiläufig. Wieder wünschte sich ein Teil von
Lilið an die Stelle der Katze, oder alternativ die
Katze zu sich. Eine Katze zu streicheln, hätte sie vieleicht
geerdet. Sie atmete langsam den herben Geruch des Tees ein,
in den sich auch der Geruch jener Person ihr gegenüber
mischte, und versuchte, sich zu sammeln.

"Um was für eine Kostbarkeit handelt es sich?", fragte sie.

"Um einen Bestandteil des Schatzes der Monarchie." Der Langfinger
tauschte mit der freien Hand ihre frisch
eingegossenen Tassen. "Du erlaubst, dass ich mir herausnehme, auf
Nummer sicher zu gehen?"

Lilið schnaubte. Der Langfinger war schnell gewesen, als er
beim Tassentausch ein bisschen Pulver oder eine andere Substanz
in nun ihre Tasse hatte schummeln können, aber nicht schnell
genug, dass es Lilith entgangen
wäre. Nun war der Inhalt der Tasse also doppelt vergiftet. Sie
hatte in der Tasse ja vor dem Eingießen bereits ein magenverwirrendes
Mittel hineingerieben. Nichts Schlimmes, es hätte nur etwas
geschwächt und eine größere Dringlichkeit bewirkt, das Grundstück
eilig zu verlassen. "Was das wohl jetzt für ein Cocktail ist? Ich
hole uns Mal neue Tassen."

"Ich würde die schon behalten, die ich jetzt habe.", erwiderte
die Diebesperson. Die Finger der einen Hand strichen weiter
kontinuierlich durch das Fellpaket, das anfing, leise zu
schnurren.

Lilið runzelte kurz die Stirn und befand, dass das Sinn ergab. Sie
war immerhin geistesgegenwärtig genug, die Kanne mitzunehmen, als sie
sich eine neue Tasse aussuchte. Dies war der Moment, in dem sie
tatsächlich anfing, sich zu gruseln. Dieser Langfinger hatte
wahrscheinlich das Potenzial, sie zu überlisten.

"Wie heißt du eigentlich?", fragte sie, um abzulenken.

"Marusch." Wie der Langfinger das aussprach, klang es fast schüchtern.

"Ein Frauenname.", stellte Lilið fest, als sie sich gegenüber der Person wieder
niederließ. Sie goss sich ihre Tasse ein und platzierte die Kanne etwas abseits
der Mitte, sodass sie Marusch zwischen Katze und Kanne hindurch
im Auge behalten konnte. "Eigentlich zumindest."

"Warum eigentlich?", fragte Marusch. "Ich fand ihn schön und
habe ihn mir ausgesucht. Wohl wissend, dass es ein Frauenname
ist."

"Ich heiße Lilið.", sagte Lilið und biss sich kurz darauf
fast metaphorisch auf die Zunge. Das hatte sie eigentlich nicht
preisgeben wollen. Nun gut. Hoffentlich war nicht allzu weitreichend
bekannt, dass das Kind des Lord Lurchs so hieß. "Und ich bin
keine Frau."

In Maruschs Gesicht breitete sich ein Lächeln aus, das mehr für
Marusch selbst zu sein schien, als für Lilið. "Ich auch nicht. Aber
ein Mann halt auch nicht. Ich mag gern nah dran an einer Frau
sein.", sagte Marusch. Immer noch war diese Ängstlichkeit
in der Stimme, die Lilið bei dem Thema nachfühlen konnte. "Es ist schön,
verstanden zu werden. Zumindest ein wenig."

Lilið trank sehr vorsichtig einen kleinen Schluck von ihrem Tee. Viele
Gedanken rannen durch ihren Kopf wie Regenrinnsale, die sich ihren
Weg auf einer Scheibe suchten. Sollte sie mit Marusch gemeinsam
fliehen? Weil sie diese Geschlechtssache verband? Oder weil ihr
Körper auf Marusch elektrisiert reagierte und sie das Abenteuer
mochte? "Wenn ich über dich rede, möchtest du dann, dass ich
es so tue, als würde ich von einer Frau reden? Mit 'sie' und
allem?", sprach sie die nächste Frage, die ihr kam, direkt aus.

Marusch gab ein amüsiertes Geräusch von sich. "Am liebsten
möchte ich, dass du gar nicht über mich sprichst. Aber wenn,
dann, hm. In meinem Kopf fühlt sich 'er'
manchmal noch sicherer an. Ein
zartes 'er' vielleicht. Aber wenn du mich je im Kleid sehen
solltest, dann lieber 'sie'." Marusch wirkte einen Moment
sehr nachdenklich. "Mache ich es für dich kompliziert? Ich würde
wirklich gern mit dir zusammenarbeiten."

"Die Pronomensache und so ist nicht das Problem.", hielt
Lilið fest.

Marusch wirkte mit einem Mal weicher, fast so weich wie
die tiefenentspannte Katze. "Wenn es wirklich kein Problem für
dich ist, dann vielleicht doch 'sie'.", sagte sie fast
schüchtern. "Ich habe es schon in der Familie
und im Freundesumfeld probiert und es fiel ihnen verschieden
schwer. Ich fühle mich noch nicht immer sicher genug, es
trotzdem zu wollen, wenn es Leute belastet, vor allem, wenn
ich sie mag."

Lilið nickte und lächelte einladend. "Es belastet mich
gar nicht und ich verstehe nicht, warum es das bei anderen
tut. Ich weiß, dass das passiert, aber, gnarfz!"

Marusch nickte mit dem Ansatz eines Lächelns, das erleichtert
und traurig zugleich auf Lilið wirkte. Vielleicht interpretierte
sie aber auch zu viel hinein.

Es war seltsam leicht, ein Gespräch über solche
Details zu führen, obwohl sie bisher in solchen Gesprächen immer
schon bei der Schwierigkeit stecken geblieben war, worum es
überhaupt ging. "Ich bin mir über meine auch noch nicht im
Klaren.", sagte sie. "Ich glaube, ich mag 'sie' schon sehr gern, aber
manchmal mag ich auch 'es'." Sie seufzte fast, als sie hinzufügte: "Ich
sehe eher ein Problem darin, dass ich nicht für eine gute Grundlage
für eine gemeinsame Diebeserklärung halte, dass wir uns gegenseitig
anlügen und versuchen zu vergiften."

"Ich hingegen halte, dass du nun zugegeben hast, dass du
mich angelogen hast und versucht hast, mich zu vergiften, für
einen guten Anfang, das Problem zu lösen." Maruschs nun
wieder breites Grinsen
war schön und steckte an, und das, obwohl ihre Worte Lilið
sauer machten.

"Und du möchtest lieber nichts zugeben?", fragte sie.

Im nächsten Augenblick fragte sie sich, ob sie damit
wieder metaphorisch auf die Nase fallen würde. Als sie gemeint hatte,
Marusch würde sie wegen Begriffsstutzigkeit abwerten, hatte Marusch
sofort eingelenkt und sich entschuldigt. Als Lilið sich beschwert
hatte, Marusch würde nicht helfen, war sie sofort da gewesen und
hatte das Wasser erhitzt.

"Ich habe dich, wenn wir von der Ironie am Anfang absehen, mit der
ich behauptet habe, eine Wache zu sein, weder angelogen, noch
versucht, dich zu vergiften.", sagte Marusch gelassen. Ernst dieses
Mal. "Ich mag trotzdem noch ehrlicher sein, wenn du möchtest. Ich
habe testen wollen, ob du davon mitbekommen würdest, wenn ich versuchen
würde, dir etwas in den Tee zu tun. Daher habe ich so getan als
ob."

"Würdest du", Lilið brach direkt ab. Sie hätte fragen gewollt, ob
Marusch einen Schluck aus der Tasse trinken würde, die nun am Rande
stand und abkühlte. Aber sie hatte schon selbst verraten, dass sie
Gift hineingegeben hatte. "Es fällt mir schwer, dir zu glauben.", gestand
sie also.

"Das kann ich verstehen.", erwiderte Marusch. "Vertrauen im Diebesgewerbe
ist wohl eine schwierige Sache. Und deshalb würde ich gern zuvorkommend
in noch zwei anderen Punkten offen sein: Ich werde dir wenig über meine
Identität vor Beginn meiner Diebeskarriere erzählen, selbst wenn wir
uns besser kennen lernen. Und, -- es würde mich überraschen, wenn
es dir entgangen wäre --, ich finde dich anziehend. Beides Gründe, aus
denen ich verstehen könnte, dass du nicht willst."

Das Gefühl, dass Lilið durchschoss, als Marusch die Sache mit der Anziehung direkt
ansprach, war heiß und flüssig, ließ sie fast weich in den Knien
werden. Glücklicherweise saß sie. "Es ist mir nicht entgangen.", sagte
sie. Sie verzichtete darauf, zuzugeben, dass sie ebenso empfand.

Die Katze streckte sich zu einer erstaunlichen Länge, kroch unter
Maruschs Hand hervor und sprang vom Tisch. Ihr Verschwinden
erinnerte Lilið daran, dass sie vielleicht nicht mehr viel Zeit haben
würden und sie fragte sich, was sie mit dieser
Zeit machen sollten. Sie beobachtete, wie Marusch
ganz entgegen ihres Eilegefühls genießend einen Schluck Tee trank, mit
geschlossenen Augen und mit einer Ruhe, die sich auf sie zu übertragen
versuchte.

"Ich soll auf ein Internat für skorsche Damen nach Frankeroge geschickt werden." Warum
sagte sie das? Irgendetwas hatte sich in ihr gelöst. Vielleicht, weil
Marusch so ruhig war und sie über Frausein und Nicht-Frausein gesprochen
hatten. Lilið schluckte.

"Es spricht nun einiges dafür, dass du hier eigentlich zu Hause bist
und fliehen möchtest.", schloss Marusch.

"Was?", fragte Lilið. "Wie kommst du darauf?" War es so offensichtlich?

"Ich sagte am Anfang schon, dass du durch deine Kleidung nicht wirkst, als
hättest du stehlen nötig.", errinnerte Marusch. "Und du bist wahrscheinlich
gut, aber machst auf mich eher den Eindruck eines Gelegenheitslangfingers als
den einer meisterhaften Person, die es auf sehr spezifische Wertgegenstände
abgesehen hätte. Es ist natürlich nicht ausgeschlossen, dass du mir durch sehr
gutes Schauspiel einen anderen Eindruck vermittelst, aber ich halte erst einmal
anderes für wahrscheinlicher." Marusch trank noch einen Schluck Tee. Dieses Mal
etwas eiliger und doch ersichtlich genießend. "Wenn ich dich hier also zufällig treffe, und
nicht weil du eine ähnliche Fährte hast wie ich, dann ist nicht
unwahrscheinlich, dass du hier in der Gegend wohnst. Als Person mit eher gut
gepflegter Kleidung gehörst du dann wahrscheinlich zu Lord Lurchs Hofstaat, ob
als bedienstete Person oder Teil der Familie. Wenn du ein Internat für skorsche
Damen besuchen sollst, dann eher zur Familie. Und darüber, dass du keine Dame
bist, sprachen wir schon. Das legt eine Motivation für eine Flucht nahe. Dafür
würden auch die Segelhandschuhe und dein Gepäck sprechen."

Lilið sah hinab auf ihre Segelhandschuhe, in denen sie ihre eigene Tasse
hielt. Sie trank ebenfalls noch einen Schluck, schloss unwillkührlich
die Augen, als das mild herbe Gebräu ihren Mund erwärmte. Sie
befand, dass Maruschs Schlussfolgerung tatsächlich naheliegend klang und
auch nicht allzu sehr schaden würde, wenn sie es bestätigte, also nickte sie.

Marusch trank die eigene Tasse leer und stellte sie ab. "Wenn du nicht mitkommen
würdest, würde ich dir eher raten, auf dem Weg zum Internat zu verschwinden als
jetzt schon. Frankeroge ist nicht gerade um die Ecke. Wenn du auf
dem Weg so verschwindest, dass dein Fehlen nicht sofort auffällt, wird
das Suchen nach dir dadurch schwieriger. Vielleicht
ergibt sich ja sogar, dass du eine Dame findest, die gern so tun mag, als wäre
sie du.", schlug Marusch vor. "Letzeres würde sicher nicht einfach werden, aber
wenn es klappt, würdest du dann erst einmal gar nicht gesucht."

"Warum gibst du mir Tipps?", fragte Lilið.

Marusch zuckte mit den Schultern. Ein etwas nachdenklicher Ausdruck trat in
ihr Gesicht. "Gute Frage. Ich", doch weiter kam Marusch nicht. Sie lauschten
beide auf die Schritte, die sich vorm Haus im Kies näherten.

Marusch räumte rasch die Teekanne und die gebrauchten Tassen in den Schrank
zurück, die noch gefüllte eingeschlossen. Lilið huschte zur Tür, öffnete
sie für Marusch etwas weiter und drehte das Licht aus. Binnen Bruchteilen von Sekunden
entschied sie sich, Maruschs Vorschlag zu befolgen. Wenn sie jetzt flöhe, wäre
das viel riskanter, als auf dem Weg zum Dameninternat. Sie sollte sich
dann aber bis zur Abfahrt besonders brav verhalten.

Sie folgte Marusch durch die Tür der Teeküche in den Flur. Sie
tauschten einen Blick in der Dunkelheit. Lilið schüttelte den Kopf,
nicht sehr überzeugt, vielleicht etwas traurig, um Bescheid zu
geben, dass sie nicht mit ihr mitkommen würde. Dann ging alles
sehr schnell. Eine Hand berührte sie sehr sanft für einen kurzen Moment
an der Wange. "Ich hoffe wir sehen uns mal wieder, Lilið.", hörte sie
Maruschs leise, warme Stimme in ihrem Ohr. Sie konnte nicht einmal so etwas
Albernes wie 'ich auch' sagen, da fühlte sie wieder den kalten Windhauch
von der Tür aus. Dieses Mal drohte sie, einfach zuzufallen. Lilið eilte
die paar Schritte zu ihr und schloss sie, vermutlich auf etwa die gleiche
leise Art, wie Marusch es vorhin getan hatte. Dann schlich sie die Treppen hinauf, und
noch bevor sie die obere Stufe erreicht hatte, spürte sie wieder den
kalten Wind von der Tür und hörte dieses Mal auch deutlich, wie sie
geöffnet wurde. "Niemand verlässt das Haus, es ist umzingelt!", hörte sie
die Stimme einer Wache, sich vorsichtshalber an mögliche Eindringlinge richtend.

Sie wollte auch nicht hinaus. Sie wollte ihre Reisesachen im Bettkasten verstecken,
sich rasch nachtfein umziehen, sich die Haare verstrubbeln und dann so tun, als
hätte sie keine Ahnung, was los wäre, und als wäre sie gar nicht so
glücklich, gerade geweckt worden zu sein.

Hoffentlich hatte Marusch es geschafft.
