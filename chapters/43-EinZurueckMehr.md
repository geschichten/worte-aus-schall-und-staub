Ein Zurück mehr
===============

*CN: Geschlechtszuweisungen und Unfug dazu. Ableismus. Zärtlichkeiten,
Erotik, Erregung und Masturbieren - erwähnt.*

"Wo fange ich an?" Mit diesen Worten weckte Marusch Lilið aus
dem Schlaf.

"Mist, ich sollte nicht einschlafen!", fluchte sie. Sie glaubte,
dass sie nicht lang geschlafen hatte. Ihr erster Blick
galt dem Kompass, aber die Müdigkeit drohte sie direkt wieder
einzuholen.

"Erwischt!", meinte Marusch grinsend. "Ich werde dich jetzt
wachhalten, wenn du nichts dagegen hast."

"Ich bitte darum!" Lilið fühlte Maruschs warmen Körper an ihrem
Rücken. Es war sehr gemütlich. Vielleicht sollte sie etwas daran
ändern. Aber erst einmal wollte sie ausprobieren, ob Marusch
mit dem Wachhalten erfolgreich sein würde.

"Ich denke, ich mache es spannend.", beschloss Marusch. "Die
Schrift war trickreich. Im Prinzip ist es eine Lautschrift. Eine
Schrift, die versucht, das Akustische in einzeln unterscheidbare
Einheiten einzuteilen, und diesen Buchstaben zuordnet. Das Ergebnis
ist im weitesten Sinne ähnlich zum baeðischen Alphabet. Ein
Unterschied ist, dass Buchstabengruppen, die bei uns einen
einzelnen Laut erzeugen, ein eigenes Zeichen in der
anderen Schrift haben."

"Hm.", machte Lilið. "Die Idee hattest du schon einmal, aber hatten wir nicht
herausgefunden, dass es weniger Grundzeichen gab als im Alphabet? Im
Baeðischen, meine ich. Müssten es mit deiner Behauptung nicht eher mehr sein?"
Lilið erinnerte sich, dass das der Stand gewesen war, bevor sie das Buch kopiert hatte,
aber dass die Variationen der knapp dreißig Grundzeichen nur fein waren, sie
also eher mit Sonderregeln als mit neuen Buchstaben gerechnet hatten. "Oder
machen die feinen Unterschiede, die erst mit meiner Kopie dazukamen, so viel
aus, dass ein ganzer Satz neuer Buchstaben entsteht?"

"Die machen so viel aus.", bestätigte Marusch Liliðs
letzten Gedanken. "Tatsächlich ist in jedem Zeichen nicht nur
jeweils ein Einzellaut abgebildet, der ungefähr einem baeðischen Buchstaben
entspräche, sondern auch die Höhe und Lautstärke des gesprochenen
Buchstaben. Also, wenn ich ein A leise spreche, dann ist das
Zeichen dafür ein sehr anderes, als wenn ich ein A laut spreche." Marusch
sprach die besagten 'A's entsprechend laut und leise
aus. "Und wenn ich wegen einer Betonung mit der Stimme nach oben
gehe, dann bekommt das A in der betonten Silbe auch ein anderes
Zeichen, als in einer unbetonten."

"Das wiederum müsste doch ein Alphabet, das etwa dem baeðischen
entspräche, mal vier nehmen.", überlegte Lilið. "Mindestens. Oder
mehr, wenn mehr als zwei Lautstärken und Tonhöhen abgebildet werden."

"Es werden drei Lautstärken und drei mal drei Tonhöhen abgebildet.", konkretisierte
Marusch. "Das macht siebenundzwanzig verschiedene Grundzeichen. Erinnerst du
dich an diese geschwungene Linie?" Marusch malte eine Welle auf
Liliðs Rücken.

Lilið genoss die Berührung, was sich ungehörig anfühlte, weil sie
nicht dazu gedacht war, genossen zu werden. "Ja."

"Das ist das Grundzeichen für einen Ton in mittlerer Lautstärke
etwa in Heelems Tonlage, nicht extra betont, aber auch nicht am
Satzende.", erklärte Marusch. "Es werden drei Tonlagen
unterschieden und innerhalb jener relativ dazu drei Tonhöhen
für Betonungen."

"Äh.", sagte Lilið so sachlich, wie dieser Laut eben ausgesprochen
werden konnte. "Also, die Grundzeichen beinhalten ausschließlich
Information über Tonhöhe und Lautstärke? Also, für alle Informationen, die wir
zum Lesen bräuchten, sind die Grundzeichen redundant?"

"Genau!" Marusch kicherte. "Ich halte für realistisch, dass ich
nicht einmal von selbst auf die Idee gekommen wäre, wofür die
Grundzeichen stehen könnten, wenn es nicht im Buch erklärt worden
wäre. Es gibt darüber ein eigenes Kapitel. Darin wird
auch analysiert, dass Menschen
Stimmen Geschlechter zuordnen. Die tiefe Tonlage tritt meistens
bei Männern auf, die mittlere bei Frauen und die hohe bei
Kindern. Die, die das Buch geschrieben haben, haben allerdings
kein Konzept von Geschlechtern. Sie finden seltsam, dass im Fall
dass Stimmhöhe Geschlecht bestimme, also Geschlecht
keine weitere Bedeutung hätte, als Höhen von Stimmen zu
benennen, warum es dann Abweichungen gäbe. So etwas wie
eine sehr hohe Männerstimme ergibt dann keinen Sinn. Wenn
aber Geschlecht andersworüber bestimmt würde, wundern sie sich,
warum Stimmen Geschlechter zugewiesen würden. Außerdem verstehen sie das Konzept
von Kindern besser als das Konzept von Geschlecht,
und finden seltsam, Stimmen Kindsein zuzuordnen. Besonders
verwirrt waren sie davon in dem Zusammenhang,
warum manche Menschen dann sehr hoch singen würden. Sie gehen
nicht in die Tiefe, aber ich fand den Abriss recht
amüsant. Denkst du ans Steuern?"

Lilið hatte nicht nur das Steuern fast vergessen, sondern auch
das Atmen und holte beides nach. Immerhin war der Kurs einfach
zu halten. Sie luvte minimal an, atmete tief durch und fragte:
"Wer hat das Buch verfasst?"

"Darf ich vorher noch darauf eingehen, wie die Schrift nun
funktioniert?", fragte Marusch. "Es dauert nicht lange." Lilið konnte
hören, dass sie breit grinste.

"In Ordnung." Lilið spürte eine alberne Ungeduld ins sich und fragte sich,
ob sie sie genoss oder es sie quälte. Oder beides.

Marusch malte ihr abermals die Welle auf den Rücken, aber wurde
zum Ende der Kurve hin langsamer. Sie malte ein fast unscheinbares
Zittern in das Ende der Linie, das Lilið vor allem merkte, weil
Marusch es durch mehr und weniger Druck betonte. "In dieser
Modulation der Linie am Ende kann stecken, ob es sich um
ein A, oder um eine Variante von A, nämlich ein O oder
ein langes E handelt. U ist das gleiche wie O und I ist das
gleiche wie ein kurzes E, das, was wir selbst nichtmal
vom Hören her von I unterscheiden können."

Lilið hob skeptisch die Augenbrauen und überstreckte den
Nacken so sehr, dass Marusch ihre Stirn sehen könnte.

Marusch kicherte und formte einen sanften Kuss auf den
Runzlungen aus, der Liliðs Körper weicher werden ließ. "Mist."

"Hm?", erwiderte Lilið irritiert. "Ich hatte zum Ausdruck bringen
wollen, dass da ganz schön viel Detail in der Schrift auf die Tonhöhen
und so eingeht, aber Vokale dann wieder nicht so sehr unterschieden werden,
wie sie könnten. Das ist so", Lilið runzelte die Stirn
abermals, "nicht hilfreich zum Lesen? Und nun verwirrst du mich
mit 'Mist'. Was ist los?"

"Ich werde gerade abgelenkt durch den Wunsch, dich zu küssen." Maruschs
Stimme war warm und amüsiert, und vielleicht fordernd. "Wenn du
gesteuert hast und für eine solche Unterbrechung zu haben
wärest."

"Woah ey!" Lilið bemühte sich, nicht laut zu rufen, um die anderen
nicht zu wecken. Sie setzte sich gerader hin, kontrollierte Segel
und Kompass. Alles stimmte noch. "Du machst es wirklich extrem
spannend. Na mach schon! Küss mich!"

Marusch legte die Arme sanft um sie herum und wanderte dann mit den
Fingerspitzen über ihren Körper zu ihrem Kinn. Sie waren angenehm
kalt. "Ich bin auch froh,
dass ich den Drang hierzu in der Zwischenzeit nicht irgendwo
verloren habe.", sagte sie. Sie küsste eine zarte Bahn an Liliðs
Schläfe hinab, über ihre Wangen, an ihrem Mund vorbei auf ihren Hals,
bis Lilið schnappatmete. Dann küsste sie Liliðs Lippen.

Lilið genoss es. Genoss jede heiße Berührung der Haut und
des Atems. Irgendwann hielt sie es nicht mehr aus und kammerte sich wieder
an Marusch. Bis diese "Au" sagte.

Lilið ließ lockerer. "Es tut mir leid.", flüsterte sie.

"Ich verstehe dich ja." Marusch lächelte sie an. "Der Kurs?"

Lilið kontrollierte abermals den Kurs. Sie hatte bei der Aktion
tatsächlich die Pinne bewegt. Sie waren immerhin nur für einen
kurzen Moment ein paar Grad in die falsche Richtung gesegelt. Das
machte nichts.

"Viele harte Konsonanten sind durch kleine Zusätze an
den Zeichen gekennzeichnet, die wie ein ausrutschen der
Linie am Ende wirken können.", führte Marusch zu Ende aus.

"Hm.", machte Lilið abermals. Wie war ihr Gehirn in der Lage, nun
wieder in dieses Thema einzutauchen? "Es klingt mir danach, als
wäre das Buch von Leuten geschrieben worden, die kein Baeðisch
sprechen, die keinen Plan haben, was an Sprache für uns wichtig
ist, und versucht haben, alles darin abzubilden, was sie
in verschiedene Kategorien differenzieren konnten."

"Du bist da was auf der Spur!" Marusch strich ihr über den
Kopf.

"Ich möchte die Nacht mit dir verbringen.", hauchte Lilið. Die
sanfte Berührung fühlte sich so schön an. Sie hatte ein
Verlangen nach mehr davon. Nach abwechselnder Leidenschaft
und Sanftheit und nach Marusch.

"Dann habe ich gute Nachrichten für dich.", raunte Marusch. "Wir
verbringen gerade die Nacht miteinander."

Lilið kicherte. "Weißt du, was ich eigentlich meine?"

"Ja." Wie zur Bestätigung streichelte Marusch an ihrer
Wange und ihrem Hals entlang, weiter über ihren Körper,
bis ihre Hand auf Liliðs Bauch zum Liegen kam. "Möchtest
du lieber nicht jetzt über das Buch reden?"

Lilið blickte auf den Kompass. "Doch. Und selbst wenn ich das
fürs Kuscheln aufschieben wollte, ergäbe es keinen Sinn, weil
ich steurern muss."

"Soll ich weniger provozieren?", fragte Marusch.

Dass war eine harte Entscheidung. Das Wort 'provozieren'
löste eine Welle von Erregung in Lilið aus, die sie heute
noch nicht gefühlt hatte. Wollte sie das überhaupt? Oder
wollte sie eigentlich lieber leidenschaftlich kuscheln?

"Ich weiß es nicht.", gab sie zu. "Aber ich bin schon wieder bei diesen
Leuten, die die Sprache gelernt haben. Denn meine These ergibt
vor allem Sinn, wenn sie die Sprache gar nicht verstanden haben,
sondern nur per Diktat irgendetwas aufgeschrieben hätten."

"Warum?", fragte Marusch.

"Weil sie sonst, wenn sie die Sprache verstanden hätten, gewusst
hätten, was für Information zum Verständnis nicht so relevant ist,
und diese zumindest fast redundante Information dann nicht, oder
wenigstens nicht so dominant abgebildet hätten. Die Hauptzeichen wären
dann nicht an Tonhöhe und so etwas geknüpft. Oder?", argumentierte Lilið.

"Es sei denn, Betonung ist gar nicht so irrelevant. Wir ergänzen
sie beim Lesen nur. Weil wir das können. Aber unserer Kultur
fremde Leute können das vielleicht nicht.", widersprach
Marusch. "Hinzu kommt, wenn diese Leute erst per Diktat mitgeschrieben
haben, um dann vom Aufschrieb, der ja nicht so schnell vergeht wie
Klang, angefangen haben, die Sprache zu lernen,
dann hätten sie die Schrift vielleicht trotzdem beibehalten."

"Was sind das für Leute?", fragte Lilið noch einmal. "Das ist
so interessant! Es fühlt sich an, wie von einem Weltraumvolk von
einem anderen Planeten ausgeforscht zu werden. Ich mag die
Vorstellung, sie ist großartig. Aber das ist es nicht, oder?"

Marusch schnaubte auf einmal und kicherte. "Also, doch, schon.", sagte
sie. "Nur das mit dem Planeten nicht."

"Du willst mich doch auf den Arm nehmen!" Aber ein Teil von Lilið
glaubte bereits an ein Weltraumvolk. Ihr fiel nur schwer, sich
diese Idee zu erlauben, weil sie wusste, dass die meisten
Menschen sie als Person weniger ernst nehmen würden, wenn sie
erführen, dass sie es für möglich hielte.

"Ich möchte dich nicht auf, aber wenn du magst, gern
in den Arm nehmen.", korrigierte Marusch und legte
auch den anderen Arm wieder um sie. "Nein, ich möchte dich nicht
vereimern. Ich finde gerade sehr validierend, dass das auch deine
erste naheliegende Schlussfolgerung war. Ich habe mich dabei
auch gefühlt, als würde ich etwas sehr Unrealistisches vermuten."

"Wenn das Weltraumvolk nicht von einem anderen Planeten kommt, woher
kommt es denn dann?" Eigentlich fand Lilið die Frage gar nicht so
wesentlich. Sie hatte gerade erfahren, dass es ein Volk da draußen
im Weltraum gab, das auf ihrem Planeten ein Buch geschrieben hatte. Aber
was sollte sie sonst fragen?

"Es ist ein nomadisches Volk.", erklärte Marusch. "Es reist in
unserem Sonnensystem und etwas darüber hinaus durchs All, ohne
irgendeinen festen Wohnsitz oder so etwas."

"Das steht in dem Buch?", fragte Lilið.

"Ja! Und noch so einiges.", bestätigte Marusch.

"Brauchen sie also keine Atmosphäre zum Leben?", wollte Lilið
wissen, noch ehe Marusch dazu käme, über das weitere zu reden.

"Sie brauchen keine. Im Gegenteil. Atmosphäre kann ihnen sogar
Schwierigkeiten bereiten. Unsere Luft ist ganz okay
für sie." Marusch grinste. "Das Meer macht ihnen eher zu schaffen
und vor allem unsere Risse zwischen den Seenplatten."

"Ohje! Meer haben wir eine Menge!" Lilið wusste nicht, ob sie
Mitleid haben sollte, aber sie konnte auch nicht vermeiden,
begeistert zu grinsen.

Marusch machte ein zustimmendes Geräusch. "Manche von ihnen sind,
als sie hier waren, im Meer gelandet. Wenn sie nicht zwischen
den Seenplatten landen, brauchen sie einfach eine ganze Weile, um
dem Meer zu entkommen. Sie rollen sich langsam über den Meeresgrund,
bis sie das Ufer erreichen. Zwischen den Seenplatten bräuchten sie
mehr Glück. Gegen die Strömungen kommen sie schwer an."

"Rollen?", fragte Lilið. Sie hatte unweigerlich sofort eine
klare Vorstellung.

"Genau!" Marusch fügte nichts weiter hinzu.

"Sind sie zufällig so etwas wie kopflose Igel?" Zwischen Liliðs
Aufregung und Begeisterung schob sich der Gedanke, wieder einmal
den Kurs zu kontrollieren. Er passte noch.

"Genau!", sagte Marusch abermals. "Es sind Igeldingse. Ich dachte
mir, dass du mit einem Bekanntschaft gemacht haben könntest. Die meisten
Igeldingse sind vor ungefähr zehn Jahren abgereist. Sie wollen
in etwa zwanzig, sofern ich deren Zeitrechnung richtig verstanden
und umgerechnet habe, zurückkehren, um die Zurückgebliebenen abzuholen
und zu schauen, ob sich dann doch eine Gelegenheit bieten sollte, mit
uns zu kommunizieren. Oder mit den Drachen. Sie haben sich beim
letzten Besuch auf uns fokussiert, aber wir sind hier ja nicht
das einzige Volk auf dem Planeten. Kommunikation mit Völkern hat
für sie beim letzten Mal jedenfalls nicht so gut geklappt, schreiben
sie. Sie hoffen, dass uns das Buch hilft, bis zu ihrem nächsten Besuch
einen Kommunikationsweg mit ihnen zu finden, wenn wir wollen."

"Was wenn nicht?", fragte Lilið.

Sie spürte, wie Maruschs Schultern in ihrem Rücken zuckten. "Nichts.", meinte
sie. "Sie schauen gelegentlich vorbei, versuchen, es uns leichter
zu machen, aber wenn es nicht klappt oder wir nicht wollen, ziehen sie halt
wieder ab."

"Und was, wenn es klappt?", fragte Lilið.

"Naja, dann reden sie mit uns.", erwiderte Marusch. "Ich glaube,
sie interessieren sich vorwiegend für die Art, wie wir kommunizieren. Und
ein wenig dafür, warum wir Fortschritt wollen und was das genau
sein soll. Aber an sich machen sie einen unvoreingenommenen Eindruck. Sie sind
vor allem kontaktfreudig. Sie mögen Austausch. Darum geht es
ihnen."

"Das kommt mir sehr nachvollziehbar vor.", überlegte Lilið. "Und
harmlos. Jetzt tut es mir spätestens leid, dass sie mit
Seenplattenritzen kämpfen mussten. Sind welche gestorben?"

"Ich weiß nicht, inwiefern sie ein Konzept von Sterben haben, oder
ob jenes unserem Konzept von Sterben ähnelt. Aber
es ist schon möglich.", antwortete Marusch. "Es ist aber auch
nicht unwahrscheinlich, denke ich, dass Lajanas Igeldings aus
so einer Spalte kommt. Das ist aber eine waghalsige Kalkulation. Das
lässt sich nicht klar aus dem Buch schließen."

"Ich finde es immer noch so umwerfend!", freute sich Lilið. "Dass
du mir hier erzählst, dass wir in etwa zwanzig Jahren eine freundliche
Invasion durch ein Weltraumvolk erleben. Und vielleicht können
wir bis dahin mit ihnen reden!"

"Hast du konkrete Ideen zu diesem Miteinanderreden?", fragte
Marusch. "Ich meine, du hast das Igeldings gesehen. Es hat
keine Augen und keinen Mund."

"Also, sie können schon einmal Bücher schreiben. Und sie können
uns verstehen." Lilið erinnerte sich daran, dass Lajana dem Igeldings
Fragen gestellt hatte, und es nur mäßig gut geklappt hatte. "Vielleicht
reden wir zu schnell. Und vielleicht ist es besser, wenn wir
lernen, über diese Wellen zu reden, die dieses Igeldings fabriziert."

"Fühlst du es auch?", fragte Marusch.

"Ja. Bis hier!", bestätigte Lilið. "Es hat was von dem Zupfen einer
Kompassnadel. Aber anders als eine Kompassnadel, die mir eine
Richtung zeigen würde, habe ich keine Ahnung, wo es sein könnte."

Marusch gab ein zustimmendes Geräusch von sich. "Sie haben diese
Stacheln mit recht viel Metall darin. Damit erzeugen sie tatsächlich
wechselnde Magnetfelder.", informierte sie.

Metall war ein Schlüsselwort. Lilið erinnerte sich daran, dass sie
im Kontakt mit dem Igeldings mehrfach das Gefühl gehabt hatte, es
wäre ihr bereits sehr vertraut gewesen. Vertraut, als hätte sie
sich über einen längeren Zeitraum intensiv mit seiner Beschaffenheit
auseinandergesetzt.

"Deshalb das Metall in den Buchseiten?", fragte Lilið. "Weil sie
Magnetfelder erzeugen können und damit Metal bewegen können oder
so?"

"Ja so etwa!", stimmte Marusch zu. "Ich weiß die Details nicht. Jedenfalls
kommunizieren sie untereinander über magnetische Wellen. Sie haben
sich schwer getan, das im Buch zu erklären, weil es für das Konzept,
das grundlegend für ihre ganze Existenz ist, noch keine Worte
in unserer Sprache gibt. Sie nutzen dafür Worte wie Strom und Fluss,
weil die Konzepte von Fließen sich wohl einigermaßen übertragen
lassen, aber es bewegt sich dabei kein Wasser, sondern etwas
anderes. Sie nennen es mal Blitzen, weil Blitze wohl auch was
mit ihrer Energie zu tun haben, und mal Bitzeln, weil
Menschen diesen Wortlaut benutzen, wenn
sie mit so etwas in Klein in Berührung kommen. Und sie schreiben dann von
Bitzelverschiebung und Bitzelungleichgewicht. Den Teil habe
ich noch nicht verstanden und würde ihn gern bei Gelegenheit mit
dir durchgehen."

"Mit mir?", fragte Lilið grinsend. "Weil ich gerade da bin, weil ich dich
nicht für verrückt erkläre, oder weil du glaubst, ich wäre hilfreich,
wenn es darum geht, Physik zu begreifen, für die wir noch nicht
einmal Worte haben?"

"Alles zusammen?" Marusch umarmte sie fester. "Und weil du meine
Begeisterung teilst."

"Das stimmt!" Lilið warf einen erneuten Blick auf den Kompass und
anschließend auf den Horizont, dort, wo das erste Morgenlicht
die Nacht verdrängte. "Bis zur Reiseinsel sollte es nicht mehr
weit sein. Ich merke, dass ich wirklich müde werde. Vielleicht kann ich
das Gespräch nicht mehr lange weiterführen. Aber ich bin doch
so neugierig. Wie ernähren sie sich?"

"Sie nehmen eigentlich recht direkt Sonnenenergie und kosmische
Schwingungen auf.", antwortete Marusch. "Das macht die Sache
mit dem Meer noch problematischer. Da unten kommt wenig Licht
an."

Im Bauch der Kagutte auch nicht, ergänzte Lilið in Gedanken. Sie
erinnerte sich daran, dass ihr das Igeldings am Ende energieloser
vorgekommen war. Passend zum Thema gähnte sie. "Wie
reisen sie? Sie können ja schlecht durch
die Luft rollen. Also, wie kommen sie wieder weg?"

"Sie haben eine Möglichkeit, sich über so etwas wie Organe, die
sich in ihrem Körper sehr schnell oder weniger schnell drehen, schwerer
oder leichter zu machen.", erklärte Marusch, wirkte dabei aber
unsicher. "Wenn sie sehr leicht sind, dann können sie von der
Atmosphäre weggetragen werden und ihre Schwebrichtung tatsächlich
durch ihre Härchen beeinflussen. Wenn sie sich dann im
richtigen Moment schwerer und leichter machen, können sie irgendwie
Fliehkräfte und Gravitationsfelder anderer Planeten oder der Sonne
nutzen, um ihre Bahnen zu lenken. So ganz habe ich auch das
nicht verstanden. Jedenfalls bewegen sie sich um Planeten
und zwischen ihnen ähnlich wie Monde und Kometen entlang."

Lilið stellte fest, dass es sie genauer interessierte, aber sie
zu müde war, darüber nachzudenken. Sie würde davon träumen, oder
morgen nachfragen. "Wie sind sie bloß auf die Idee gekommen, ein
Buch zu schreiben? Während Bücher visuell sind, und sie darein
etwas Akustisches übertragen haben, und sie selbst beide Sinne
nicht haben. Obwohl, akustisch ging irgendwie. Oder?"

"Sie wissen, dass Menschen haptisch Dinge erfassen können. Das
können sie im Prinzip auch, aber sie können haptische und magnetische
Reize nicht gut auseinanderhalten.", erklärte Marusch. "Sie haben
gehofft, dass das Buch für Menschen fühlbar ist, oder dass das,
was sie in die Seiten einbringen, visuell erkennbar wäre. Aber
sie haben eben kein Konzept von Visuellem."

"Das ergibt Sinn.", überlegte Lilið. "Dann ist wirklich stark,
dass sie auf die Idee gekommen sind, ein Buch zu schreiben. Immerhin
sind Bücher tatsächlich etwas, was Menschen häufig genauer
untersuchen."

"Ja, das ahnten sie." Nun gähnte auch Marusch. "Ein paar von ihnen
haben den Weg in eine Bibliothek gefunden. Sie konnten mit den Büchern
nicht viel anfangen, weil die Tinte für sie nicht erkennbar war. Aber
sie haben dort viele Menschen reden gehört. Und als sie wussten, wie
Bücher eigentlich funktionieren sollen, haben sie sich in einer
Buchbinderei ein Buch mitgenommen, das leer war und Notizbuch hieß."

"Woher wussten sie, dass es leer war?", fragte Lilið skeptisch. Sie
atmete erleichtert auf, als sie an der helleren Linie, die der
Horizont inzwischen rundum bildete, die Insel entdeckte. Nicht
mehr lange wach bleiben müssen. "Weißt du, wie man dieses Schiff
ankert?"

Marusch kicherte. "Ja weiß ich.", sagte sie und wechselte
sprunghaft das Thema wieder zu dem Weltallvolk zurück: "Sie wussten
es tatsächlich nicht sicher. Sie haben es aus Gesprächen geschlossen. Aber
sie hören menschliche Stimmen, indem sie die Luftschwingungen von Schall
in ihren Härchen wahrnehmen. Die sind eigentlich nicht so sehr fürs
Hören geeignet. Sie schwingen zum Beispiel auch viel mehr, wenn
etwas lauter ist. Und Tonhöhen machen den größten Unterschied. Kommt
dir das bekannt vor?"

Lilið nickte. "Das klingt nach nicht sehr kompatiblen Sinnen
für eine Kommunikation."

"Stimmt. Aber du fühlst die Schwingungen ja auch. Wir müssen sie also
nur noch erzeugen können. Dann müsste das schon gehen.", mutmaßte
Marusch.

Lilið erinnerte sich daran, dass sie es einmal probiert hatte. Sie
versuchte, sich zu entspannen und die Schwingung mit ihrem Körper
zu erzeugen, aber es klappte überhaupt nicht. Sie wollte es erst
auf die Müdigkeit schieben, aber dann wurde ihr bewusst, dass sie
die Schwingung nicht aus ihrem Körper heraus erzeugt hatte, sondern
in dem Igeldings selbst. Sie hatte, erinnerte sie sich plötzlich,
auch irgendwann einmal eine Kompassnadel verwirrt, indem sie sie
berührt hatte. "Ich bräuchte einen guten Draht."

Marusch kicherte. "So einer sollte zu beschaffen sein." Sie
atmete tief ein. Dabei drückte sich ihr Bauch gegen Liliðs
Rücken. "Lässt du mich aufstehen? Damit ich den Anker klarmachen
kann?"

Lilið war überrascht darüber, dass Marusch das jetzt schon wollte, aber
ließ sie aufstehen. Ihr wurde ziemlich schnell klar, dass sie die
Geschwindigkeit der Rennyacht doch unterschätzt hatte. Obwohl nur
mäßiger Wind war, näherten sie sich der Insel rasch. Sie ankerten
nicht allzu dicht vor Ufer, aber so, dass der Anker sie garantiert
auf der mit der Insel verbundenen Platte halten würde. Marusch
und sie holten noch die Segel ein, -- auch darin hatte Marusch mehr
Routine --, und zogen sich zu den anderen ins Unterdeck zurück. Eine
der Heckkojen war noch frei. Es war eine gepolsterte Fläche auf
erhöhtem Stauraum. Die Koje war kaum einen Meter hoch, sodass sie
mit den Füßen zuerst hineinkriechen mussten. Immerhin gab es
auch hier eine Tür, die ihnen tiefste Dunkelheit und etwas Privatsphäre
gab. Aber so sehr Lilið vorhin noch gewollt hatte, sie hatte keine
Energie mehr für viele Zärtlichkeiten. Sie war müde genug, dass
sie an Marusch geschmiegt, fast direkt einschlief.

---

Als sie halbwegs ausgeschlafen wieder an Deck stiegen, waren
alle anderen schon versammelt und frühstückten.

"Gut, dass ihr da seid!", meinte Heelem. "Ich dachte, ich warte auf
euch, bis ich frage, ob wir eigentlich bei dem Plan bleiben wollen,
zurück nach Angelsoge zu fahren."

Es folgte Schweigen. Marusch und Lilið setzten sich erst einmal
dazu. Lilið trank zügig ein Glas Wasser. Ihr Hals fühlte sich
sehr trocken an.

"Gute Frage.", meinte Marusch. "Lajana, ich denke, es hängt vor
allem an dir. Ich folge dir erst einmal überall hin, das weißt du."

"Hm.", machte Lajana bloß.

Lilið trank ein zweites Glas Wasser. "Haben wir einen beschränkten
Wasservorrat?", fiel ihr ein, zu fragen.

"Mit Marusch an Bord?", fragte Heelem skeptisch.

"Das hatte ich verheimlicht.", warf Marusch ein. "Ich kann Salz
aus Wasser entziehen. Deshalb hatte auf den Inseln immer ich nach
Wasser gesucht."

Nicht zum Masturbieren also, dachte Lilið. "Warum hast du das
verheimlicht?" Lilið betonte den Artikel und
runzelte skeptisch die Stirn. "Wolltest du, dass ich nicht weiß,
was du kannst?"

Marusch nickte. "Salz aus Wasser zu ziehen, ist schon ziemlich
fortgeschrittene Magie.", sagte sie. "Ich werde, wenn Leute
herausfinden, wie gut meine Magiekenntnisse sind, oft nicht
mehr wie ein Mensch behandelt. Als dürfe ich dann kein Interesse
an Normalsterblichen haben oder so ein Unfug. Daher habe ich
mir angewöhnt, Fähigkeiten zu verschweigen."

Lilið nickte. "Ich hätte nicht versprechen können, dass ich
keine Grundwut auf dich deswegen entwickelt hätte.", überlegte
sie. "Allzu skorsche Menschen haben so viel unverdienten
Respekt in der Gesellschaft."

Marusch nickte. "Den habe ich. Absolut.", bestätigte sie. "Ich
habe noch keinen guten Umgang damit gefunden."

"Ich wollte eigentlich zurück nach Angelsoge.", unterbrach
Lajana ihren Exkurs.

"Dann segeln wir zurück nach Angelsoge.", versicherte Heelem.

"Du wolltest eigentlich ein 'aber' anfügen, oder?", fragte Marusch.

Lilið vermutete, dass sie ihre Schwester einfach gut genug
kannte, um das richtig zu lesen.

Lajana nickte. "Ich habe mich nicht getraut, danach zu fragen. Weil
es uns alle noch einmal in Gefahr bringen würde. Und eigentlich
will ich das nicht riskieren.", sagte sie. "Aber wenn ihr dazu
bereit wäret, würde ich gern noch einmal zurück. Die haben mein
Igeldings noch. Vielleicht ist es irgendwo in den Überresten
der Sakrale. Vielleicht ist es noch auf der Kagutte."

"Es ist nicht in der Zentral-Sakrale. Oder es ist in einem der
aufgebrochenen Räume und da auch erst nach der Zerstörung hingebracht
worden. Die Sakrale schirmt die Schwinungen ab,
die das Igeldings aussendet.", hielt Lilið fest. "Ich halte die
Kagutte für wahrscheinlicher."

"Nimmst du es bis hier hin wahr?", fragte Drude.

"Ja! Du auch?" Lilið grinste.

Drude schüttelte den Kopf.

Heelem zuckte mit den Schultern. "Ich jedenfalls wäre dabei."

"Auf jeden Fall!", stimmte Lilið zu. Schneller, als sie sich das
genau überlegen konnte. Es war egal. Es ging um eine außerirdische
Person und es war der Auftrag der Königin. Was sollte sie
aufhalten?

"Ich glaube, wir sind ein ziemlich mächtiges Team.", sagte
Drude. "Und die Wachen der Kagutte gibt es zu einem großen Teil
nicht mehr. Wir sollten trotzdem nicht einfach drauf los, sondern
einen guten Plan entwickeln. Wie immer."

"Um einen Plan wäre ich auch dankbar.", stimmte Marusch zu. "Ich
fände es schön, wenn es nicht so endet wie unsere letzte
Mission. Aber ich wäre auch sehr dafür, das Igeldings wieder
bei uns zu haben."

War nun ein guter Zeitpunkt, den Rest der Crew über den bevorstehenden
Besuch von Außerirdischen in Kenntniss zu setzen? "Alligel.", flüsterte
Lilið grinsend.

Marusch zwinkerte ihr zu. Und dann taten sie es einfach.
