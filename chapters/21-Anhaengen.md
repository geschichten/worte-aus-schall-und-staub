Anhängen
========

*CN: Ungerechtigkeit, Freiheitsentzug, Entführung, Damsel in
Distress, Gedanken über Gift,
Fesseln und Knebeln erwähnt, Erotik, Body Horror.*

"Ich bin nicht so angetan von Krankheitsnummern.", raunte Lilið Marusch zu,
als sie außer Hörweite waren. "Ich glaube, das schadet real kranken
Menschen. Ihrem Ruf sozusagen."

Marusch drängte sie zwischen zwei unbewohnte Gebäude, Lagerräume
wahrscheinlich, und lehnte sich an die Wand. "Ich
mag so etwas auch nicht. Ich musste rasch improvisieren.", verteidigte sie
sich. "Der Überfall bekommt noch einen zweiten und vielleicht
einen dritten Akt. Vielleicht gefällt dir, dass der zweite Akt dazu
führen wird, dass unsere Rollen eben wahrscheinlich eher in guter
Erinnerung bleiben."

Lilið runzelte die Stirn. "Du überforderst mich.", sagte sie. Trotzdem
war ihr Gehirn schon dabei, mögliche Auslegungen des Gesagten zu
sortieren. "Möchtest du noch einmal in anderer Rolle in den Imbiss gehen,
dieses Mal als gesunde Person, der im Nachhinein wahrscheinlich
eher der Manteldiebstahl angehängt wird?"

"Ja.", antwortete Marusch. "Du bist gut! Ich hätte nicht damit gerechnet,
dass du aus meinem Gerede schon so viel ableiten kannst."

"Was?" Lilið ließ es zwischen Frage und ungläubigem Ausruf klingen. "Du
willst was? Warum?"

"Ich möchte das Nautika gern daran hindern, morgen auf Nederoge zur Crew zu
stoßen.", erklärte Marusch. "Das erfordert einen zweiten Akt. Möchtest
du mitmachen? Der zweite Akt geht schneller aber ist risikoreicher."

"Risikoreicher als einen Mantel unter sechs paar Augen und Ohren
wegzustehlen?", fragte Lilið.

Marusch wiegte den Kopf hin und her. "Schon, denke ich, ja.", sagte
sie. "Ich sprach von Ausstiegsmöglichkeit. Dies ist die erste."

"Wieviele bekomme ich?", fragte Lilið. "Und vor allem, haben wir die
Zeit, dass du mich etwas detaillierter ins Wie genau und ins Warum einweihen
kannst?"

"Du bekommst vor dem dritten Akt eine weitere. Sogar mit mehr
Bedenkzeit dann. Für den zweiten Akt haben wir leider nicht viel
Zeit, aber ich weihe dich rasch über ein paar Details ein, wenn
du möchtest.", versprach Marusch. Sie griff nach ihrem Gepäck, das
Lilið bei sich trug und nun ihr überreichte.

"Natürlich möchte ich!", betonte Lilið.

"Du würdest auch durch Mitwissenschaft schon mehr gefährdet sein.", erklärte
Marusch ihr Hadern.

"Aber ohne Details kann ich nicht entscheiden, ob ich mitmache.", argumentierte
Lilið. "Das Risiko werde ich also schon eingehen müssen, wenn ich nicht
jetzt unwissend bereits abspringen wollte. Was ich nicht will. Also leg
los."

"Ich werde dem Nautika ein Verbrechen anhängen.", sagte Marusch. "Einen
Diebstahl. Das werde ich als Lord Bärlauch tun. Ich habe eine Marke, die
mich als Lord Bärlauch ausweisen kann."

Liliðs Augenbrauen fühlten sich an, als würden sie versuchen, sich in
ihrem Haaransatz zu verstecken. "Du hast einen Mantel gestohlen, also,
eher durch mich stehlen lassen, und möchtest nun der bestohlenen Person
einen Diebstahl anhängen.", fasste sie zusammen. "Und die Marke? Hast du sie
Lord Bärlauch auch auf dem Angelsoger Adelsball abgezogen?"

Marusch nickte. "Auch eine von Lady Bärlauch. Deshalb gerade diese zwei."

"Und ich soll Lady Bärlauch spielen?", fragte Lilið.

"Sie ist eher zurückhaltend und spricht meistens leise. Das ist
keine so schwierige Rolle.", ermutigte Marusch.

"Ich habe keine Ahnung, wie sie aussieht.", gab Lilið zu bedenken.

"Die da drinnen mit hoher Wahrscheinlichkeit auch nicht.", argumentierte
Marusch.

"Hmm.", machte Lilið. Ganz überzeugt war sie von dem Plan noch
nicht. Vor allem kam er ihr sehr drastisch vor. Äußerst
ungerecht. Das Nautika könnte dabei sterben. "Das finale Ziel
ist, die Kronprinzessin zu retten?"

Marusch nickte abermals. "Magst du mir deinen Anzug leihen?"

Lilið schloss aus der Unterbrechung des Gesprächsfadens, dass ihre
Zeit knapp wurde. Das verstand sie: Maruschs gefaltete Jacke gab zwar einen
guten Ersatzmantel her, aber der Unterschied war auf jeden Fall
erkennbar. Spätestens, wenn keine Karten darin waren, sondern der
Stoff an den entsprechenden Stellen nur dicker war. Wenn sie den Verdacht von
einer vermeintlich kranken Person ablenken und auf Lord und Lady
Bärlauch verschieben wollten, mussten sie wieder reingehen, bevor es
aufflöge. Allein dafür wollte Lilið eigentlich zustimmen. Aber
sie fragte sich, was schlimmer war: Einem Nautika etwas anzuhängen,
für das es hingerichtet werden könnte, oder den Ruf behinderter
und kranker Menschen zu schädigen. Normalerweise immer
ersteres, aber auf der anderen Seite ging es um eine entführte
Kronprinzessin. Wobei Lilið scheiß egal war, dass sie eine Prinzessin
war. Sie hatte Gefühle für diese Frau, weil sie eben im Imbiss
wieder einmal eine Situation erlebt hatte, in der diese als dumm bezeichnet
worden war. In einem vermeintlich positiven Zusammenhang. Was es
noch schlimmer machte, fand Lilið. Und sie hasste so etwas.

Wo waren eigentlich Maruschs Flammen?

"Ja, du kannst den Anzug haben.", sagte sie. "Aber passt der dir überhaupt?"

"Noch nicht.", antwortete Marusch trocken.

"Ich soll dich kleiner und dicker falten?", fragte Lilið.

Sie hatte es schon einmal probiert: Marusch so zu falten, dass
sie wie Lilið aussah. Das war ihr nicht so gut gelungen. Dazu
hatte sie sich ihr eigenes Gesicht zu wenig vorstellen oder auf
eine andere Person projizieren können, aber auch
die Körperform war so verändert zu Maruschs
originaler, dass die Faltung schlecht gehalten
hatte. Allerdings, so erinnerte sie sich, war das auch am Anfang
ihrer Übungen gewesen, kurz nachdem sie sich selbst in Marusch
gefaltet hatte. Vielleicht hatte sie inzwischen ausreichend dazu gelernt.

Marusch nickte. "Das wäre ganz reizend.", sagte sie. Der
Charme klang nicht so sehr durch, wie er es in einer weniger
unangenehmen Lage getan hätte.

"Ich versuche es.", versprach Lilið. "Aber bevor ich entscheide,
ob ich Lady Bärlauch spiele, noch zwei Fragen. Ich hoffe, die
gehen schnell. Ich sehe, dass es eilig ist."

<!--War das Ballkleid nicht zurückgegeben? Ist es vielleicht nicht
auch zu schnieke? Vielleicht hat Marsuch einfach ein edleres Kleid dabei.-->
Marusch forderte sie mit einer Geste auf, zu fragen, während sie
Anzug und Ballkleid aus dem Gepäck hervorhedderte.

Also sollte sie sich in das Ballkleid stecken, schloss Lilið. Die Vorstellung,
wie sie sich ausversehen im Kleid entfalten würde und es auf diese
Art sprengte, belustigte sie völlig unpassend.

"Was hilft es, dem Nautika ein Verbrechen anzuhängen? Und wie hilft
der ganze Plan der Prinzessin?", fragte Lilið. "Ich nehme an, mit
ersterem willst du irgendwie verhindern, dass das Nautika morgen
zur Kagutte gelangt?"

"Genau!", bestätigte Marusch. "Ich habe mich lange gefragt, wie
ich vermeiden kann, dass das Nautika dort morgen ankommt. Ich habe
zunächst an Gift gedacht, aber ich kenne mich mit Giften nicht aus. Gifte
sind auch gefährlich und nicht so sehr mein Gebiet. Aber wenn du
dich damit gut auskennst und eine Idee hast, versuche ich gern, Pläne
zu ändern. Ich dachte nur, es ist auffällig, wenn das Nautika nun
für zwei Tage in tiefen Schlaf verfällt?"

Lilið schüttelte den Kopf. "Ich habe tödliches Gift dabei!", fiel
ihr ein. "Aber eigentlich will ich von deinem aktuellen Plan
eher weg, damit er weniger tödlich ist."

"Wenn wir das Nautika eines Verbrechens bezichtigen, würde
Smutje Andert in der Verpflichtung stehen, das Nautika von
den Wachen Danmoges festhalten zu lassen, wenn ich als Lord
Bärlauch das einverlange.", erklärte Marusch. "Das Nautika würde
dann erst einmal festsitzen, bis sich die Lage klärt."

Lilið nickte. "Ich kenne mich nicht so genau aus.", gab sie
zu. "Ich weiß, dass je nach gestohlenem Gegenstand Hinrichtung
droht."

Marusch nickte. "Ich versuche, zuzusehen, dass das nicht
passiert." Sie hatte nun auch die Marken gefunden, die sie
brauchte, und fing an, sich zu entkleiden. "Hast du eine
bessere Idee, ein Nautika für ungefähr zwei Tage aus dem
Verkehr zu ziehen?"

Lilið versuchte, nachzudenken, aber kam auf die Schnelle nicht
auf eine gute Idee. "Einschließen, wenn es schlafen geht?", fragte
sie wenig überzeugt.

"Dann klopft und schreit das Nautika und wird wieder ausgesperrt.", sagte
Marusch. "Fesseln und Knebeln hatte ich noch überlegt, aber
das können wir nicht ohne Weiteres leisten. Und zwei Tage ohne Flüssigkeit
sind auch eher ungesund."

Lilið nickte. Sie hatte gar nicht gemerkt, wie sie angefangen
hatte, ihren Körper zu falten, aber als sie das Ballkleid
überstreifte, mit den Gedanken noch beim Problem, passte es wie
angegossen.

"Verstehe ich deinen festlichen Aufzug richtig, dass du mitmachst?", fragte
Marusch.

"Die zweite Frage noch.", wiedersprach Lilið. "Wenn das Nautika aus
dem Verkehr ist, was hilft das der Kronprinzessin?"

"Ich nehme an, dass im Mantel des Nautika Infomationen zu finden
sein werden. Eine Einladung oder so etwas. Ein
Treffpunkt.", erklärte Marusch. "Gut, dass du das ansprichst. Es
ist gut, vorher nachzuschauen. Sonst muss ich noch
herausfinden, wie ich es aus dem Nautika herausbekomme."

Lilið entfaltete sofort Maruschs Jacke in den Mantel des Nautikas
und griff gezielt in eine der Taschen, in der ein Brief
steckte, den sie beim Falten erfühlt hatte. Sie reichte ihn
Marusch, weil sie sich in der Aufregung gerade nicht zutraute,
sinnerfassend zu lesen.

Marusch nahm ihn entgegen, warf einen Blick darauf und nickte. "Sehr
simple Kodierung. Treffpunkt ist in einem Flachdachgebäude, das
oberhalb einer Bootshalle am nederoger Handelshafen liegt. Dem
Partnerhafen des Königreichs Sper. Eine Treppe führt dort hinauf. Es
ist kein offizielles Gebäude und eine geschlossene Gesellschaft. Nur
die Crew wird anwesend sein und dich dann begutachten."

"Mich?", fragte Lilið überrascht. Und ärgerte sich dann, denn das
hätte ihr klar sein sollen. "Mich."

"Wenn du beim dritten Akt mitmachen magst. Sonst mich.", korrigierte
Marusch.

"Aber du bist kein Nautika!", protestierte Lilið. Sie selbst konnte
rechtzeitig eines sein. Ein Leicht-Nautika allerdings nur. Hoffentlich
reichte das, um das Nautika zu ersetzen, das sie gerade aus dem
Verkehr zu ziehen gedachten.

"Viel schlimmer ist, dass ich kein Zertifikat haben werde, das auf
mich geeicht ist, mit dem ich mich als eines ausgeben
könnte.", antwortete Marusch. "Ich müsste das des Nautikas
nehmen und irgendwie verhindern, dass sie es mit meiner Haut abgleichen."

"Dass du keine Fähigkeiten hast, eine Kagutte ans Ziel zu navigieren,
hältst du für weniger problematisch?", fragte Lilið.

"Da sie nicht ans Ziel soll, ja.", antwortete Marusch und hielt sich
nicht davon ab, leise zu kichern.

Lilið musste mitlachen und versuchen, das nicht allzu laut zu tun. "In
Ordnung. Ich falte dich. Und ich versuche mich an einem Spiel als Lady Bärlauch. Du
würdest den Plan ohnehin ausführen, oder? Es macht ihn nur sicherer, wenn
ich mitmache."

Marusch nickte. "Zu zweit mit Schauspiel zu betrügen, bringt häufig mehr Sicherheit
mit sich.", sagte sie. "Und du machst deine Sache eigentlich immer sehr
gut und souverän."

Das Kompliment hatte eine unerwartet starke Wirkung auf Lilið. Sie hätte
in diesem Zusammenhang nicht mit einem gerechnet, aber sie wollte gut
darin sein, Rollen zu spielen. Das positive Gefühl blockierte in der Aufregung
einen Moment ihre Atemwege.

Sie hatten keine Zeit für dieses Gefühl. Obwohl es schön war, versuchte
Lilið, es zu verdrängen, und trat auf Marusch zu. Sie legte ihre Hände
an Maruschs nackte Oberarme (Marusch war auch sonst fast
nackt, trug nur Unterwäsche und die rasierten Haare bildeten eine
Gänsehaut), um sie zu fühlen und schloss die Augen. Sie begab
sich in den Zustand, in dem sie falten konnte, und freute sich, dass
sie beim Wechsel in dieses Gedankenuniversum allmählich Routine
bekam. Auch diese Freude lenkte sie kurz ab.

Sie fühlte, wie sie Maruschs Knochen zieharmonikaartig
ineinander faltete. Irgendwo musste Masse immer hin, was zur
Folge hatte, dass die Knochen nach einer Faltung in etwas
Kleineres einen dickeren, stabileren Eindruck
machten. Das Gewebe darum herum war nun zu weit für sie, aber
Lilið hatte bereits mit dem Falten desselben anfangen müssen, dort,
wo es mit dem Knochen verbunden war.

Marusch gab einen leicht zischenden Laut von sich. Lilið fand rasch
die Stelle in den Waden, die weh getan haben musste und korrigierte
den Vorgang. Dann ging alles ziemlich schnell, und als sie von Marusch
wegtrat, stand dort eine etwas kleinere Person mit einem Körperbau, der
in den Anzug passen sollte.

Noch hatte Lilið eine mentale Bindung zu Marusch, die sie nun
auflöste. Auch dazu schloss sie die Augen, um es behutsam zu
tun, aber sie merkte, dass dabei etwas nicht richtig lief. Als
sie die Augen öffnete, musste sie kichern. Um Maruschs Gesicht
zu verändern, hatte sie die Nase mit einer Falttechnik
verzwirbelt und in einer neuen, kleineren Form
fixiert. Die Drehung hatte sich halb aufgelöst, sodass die Nase nun
zur Seite ausgerichtet war. Warum waren Nasen eigentlich immer
am schwierigsten?

Marusch fasste sich mit der Hand an die Nase. "Das ist nicht richtig
so, oder?", fragte sie. Sie versuchte vorsichtig, sie nach unten zu
drehen, aber dabei sprang ihr Arm in seine alte Länge zurück. Abgesehen
von Ring- und Kleine Finger.

Lilið schüttelte den Kopf. "Das hält nicht.", sagte sie.

Marusch schüttelte sich, sodass die Faltung nach und nach wieder
aufsprang. Lilið berüherte sie, um die letzten Verhedderungen
aufzulösen, dort, wo Falten unter andere geschoben waren, um
eigentlich das Gesamtkonstrukt zu stabilisieren, was aber ja
nicht geklappt hatte.

"Lohnt es sich, wenn du es noch einmal probierst?", fragte Marusch
sanft.

Lilið hatte eigentlich wenig Hoffnung. Aber sie atmete tief ein
und aus und lehnte noch nicht ab. Sie dachte stattdessen nach,
oder versuchte es zumindest.

"Brauchst du etwas?", fragte Marusch, noch weicher als eben, vielleicht
besorgt. "Setze ich dich unter Druck?"

Lilið schüttelte den Kopf. "Ich setze mich selber unter Druck.", fiel
ihr auf. Und dann wusste sie, was helfen könnte. "Dreh dich um."

Marusch gehorchte ohne Zögern und ohne Fragen.

Lilið trat von hinten an sie heran, so nah, dass sich ihre Körper
großflächig berührten. Sie fasste Marusch sanft an den Schultern
an und legte ihre Nase in Maruschs linke Halsbeuge. Sie roch den
Geruch, der ihr so vertraut war. Sie wanderte mit den Händen an
Maruschs Oberarmen hinab und um sie herum, langsam und zartfühlend.

"Soll mich das erregen?", fragte Marusch leise. "Ist das das Ziel?"

"Nein, aber es ist nicht schlimm für mich, wenn es passiert.", antwortete
Lilið. "Du steckst halb in einer Rolle, in einem
Schauspiel. Du verdrängst Gefühle, die dich
sonst sehr aufwühlen würden. Das ist in Ordnung, die gehen mich nur
was an, wenn du sie mir zeigen willst. Aber du verdrängst auf diese
Weise auch fast alles, was du bist. Ich kann dich nicht greifen."

Sie spürte die Wirkung ihrer Worte. Marusch musste nichts
aussprechen. Sie fühlte, wie Maruschs Körper an Anspannung verlor, die
dazu da war, sich zu beherrschen, und stattdessen einem neuen
Gefühl Platz machte. Es berührte Lilið sehr, dass dieses Gefühl
Zuneigung für sie war. Und eine Art tiefe Trauer. Wie ein dunkler,
ruhiger See. Mehr musste sie nicht wissen. Mehr musste Marusch
ihr nicht anvertrauen dafür, dass sie eine Verbindung aufbauen
konnte, die mehr Stabilität versprach.

Maruschs Körper lag vertrauensvoll an ihren gelehnt, als sie
die Arme sanft um sie schloss und sie faltete. Und als sie
Marusch dieses Mal losließ, hielt die Faltung. Marusch gab
nun einen überzeugenden Lord ab, der zudem entspannter und
mehr in sich ruhend wirkte als das Bild, das Lilið zuvor zu
falten versucht hatte.

"Danke.", hauchte Marusch. "Das war schön."

Lilið lächelte. "Für mich auch."

Marusch genoss den Moment nicht lang. Sie zog den Anzug
an und zuppelte ihn zurecht. Sie ließ Lilið noch
ein paar Anpassungen an der Kleidung vornehmen, damit
sie weiterhin edel, aber nicht ganz so festlich wirkte. Das
war weniger schwierig. Dann versteckte Marusch ihrer beider
Gepäck. Lilið konnte nicht lassen, ihre Jacke mit dem Buch
darin in eine Handtasche umzufalten, die zu Maruschs Kleid
passte. Marusch warf einen letzten prüfenden Blick
auf sie beide und führte sie schließlich eiligen Schrittes
zurück zum Imbiss an.

Vor der Tür unter den Orcheenschwänzen, die inzwischen nicht
einmal mehr im Traum zuckten, verschnauften sie nur solange, bis
sie atmeten, als wären sie normal schnell gegangen. Marusch trat
ein, erhobenen Hauptes wie so ein Lord und hielt ihr galant die
Tür auf. Auch Lilið drückte den Rücken durch und folgte
gemessenen Schrittes. Eigentlich kannte sie die Rolle einer Edeldame als
Spross des Lord Lurchs, aber gerade fühlte sie sich sehr
fremd an. Sie blieb, weil Marusch Lady Bärlauch als zurückhaltend
beschrieben hatte, dicht hinter Marusch, als diese ein paar
Schritte in den Raum hinein trat und sich umblickte.

Nicht wenig überraschend für sie warf Smutje Andert ihnen keine
freundlichen Blicke zu. Sie vermutete, dass sein Raunen in
Richtung Schiffskoch, dessen Teller er gerade einsammelte, einen
abwertenden Spruch ihnen gegenüber beinhaltete. Sie verstand
nichts, außer das Wort 'Anzug'.

"Guten Tag, verehrte Leute. Wir sind Lord und Lady
Bärlauch." Marusch sprach in einem Ton, der
eine gute Mischung aus bemüht einladend, aber eigentlich eher
geschäftlich war. "Wir wissen, dass wir hier nicht willkommen
sind. Deshalb werden wir auch nicht lange bleiben, aber leider
ist gerade das der Grund, warum wir hier sind. Meine Frau möchte
in der Hafenstadt gegen Mittag den Dieb gesehen haben, der uns
einen Kompass gestohlen hat. Einen wertvollen. Ich vergesse
immer, wie genau das Gerät heißt?" Marusch wandte sich
fragenden Blickes an Lilið.

Lilið versuchte, angestrengt nachzudenken. Das war nicht abgesprochen.
Sollte sie sich etwas ausdenken? Aber gleichzeitig wurde ihr bewusst,
dass Marusch vorhin Gelegenheit gehabt hatte, eben erwähnten
Kompass in das Gepäck des Nautikas zu schmuggeln, damit der
Diebstahl belegt werden könnte. Das war nicht unwahrscheinlich.

Trotzdem musste sie rasch antworten. Es sollte ein wertvoller
Gegenstand sein, vermutete sie. Und Marusch fragte sie nach einer
genaueren Bezeichnung, weil Marusch sich wiederum mit nautischen
Begriffen nicht auskannte. Wäre es schlimm, wenn der ins
Gepäck des Nautikas geschmuggelte Kompass den Kriterien dann
nicht entspräche? Das Problem müsste Marusch dann lösen,
beschloss sie. Sie suchte also einen Kompromiss zwischen wahrscheinlich
und wertvoll und sagte leise, fast scheu: "Einen hegemonischen
Kleinkompass." Hegemonisch bedeutete, dass der Kompass eine
Eichung hatte, die zwar nie ganz präzise war, aber an allen
Stellen der Welt ungefähr richtig. Diesen hatte ein Musika
vor etwa zwei Jahrhunderten entwickelt. Es war spannend, wie
oft Magie, Musik und Nautik, obwohl die drei Disziplinen so verschieden
waren, in der Wissenschaft von Berühmtheiten zusammen betrachtet
wurden. Das Gerät war jedenfalls auch heute so komplex herzustellen,
dass es kaum Kompantikae gab, bei denen sie zu bekommen waren. Besonders,
wenn die Mechanik so klein gebaut wurde, dass sie in eine Tasche
passen konnten.

"Ein hegemonischer Kleinkompass wurde uns gestohlen.", widerholte
Marusch Liliðs unsichere Worte und formten daraus phonetisch
etwas, was auch nach Anschuldigung klang.

Lilið fiel ein weiterer Grund ein, warum gut war, dass sie eine
zurückhaltende Rolle spielte. Sie war im Gegensatz zu Marusch
noch nicht so geübt darin, ihre Stimme zu verstellen.

"Die Person hat sich als Nautika ausgegeben. Daher sind wir hier. Das
wäre die passende Absteige für so eine Person.", erklärte Marusch
selbstbewusst. "Wir sind also gleich wieder weg. Es sei denn,
meine Frau entdeckt die Person. Magst du dich kurz umsehen, mein
Lapizstern?"

Lilið nickte und trippelte zwei Schritte vor, als wäre sie ängstlich. Tatsächlich
hätte sie vielleicht ängstlich sein sollen, hatte für das Gefühl
aber keinen Platz. Sie sah für ihr unsicheres Schauspiel
erst auf den Boden, um nicht zu stolpern, und dann in die fünf
Gesichter der verstummten Gruppe. Sie hoffte, dass sie ihren Schock
gut spielte, als sie sich an ihrem Anzugärmel an Maruschs Arm
festhielt -- nicht zu verkrampft, damit die Faltung keinesfalls aufsprang. "Diese
Person!" Sie deutete auf das Nautika und sprach nur minimal lauter als
vorhin. Ihre Stimme war dabei trotzdem überzeugend dünn und um einiges
höher, als sie sie vorhin benutzt hatte.

"Ich habe nichts gestohlen!", rief das Nautika aus.

Lilið erkannte die Stimme, und war erleichtert, dass sie sich nicht
geirrt hatte. Gesichter hatte sie nicht wiedererkannt. Sitzposition,
Haarlänge und Hautfarbe, sowie der gefälschte Mantel hatten ihr
die Indizien für ihren Fingerzeig gegeben, aber vorsichtshalber hatte
sie bei der Geste auch etwas gezittert.

"Ich würde nie stehlen!", betonte das Nautika ein weiteres Mal. Seine
Stimme bebte vor Angst. "Ihr könnt deshalb keine Nachweise haben."

Lilið fand interessant, dass das Nautika sofort an Nachweise dachte. War
das nicht eher eine Sache, zu der vor allem Menschen neigten, die
Verbrechen abstreiten wollten, die sie auch tatsächlich begangen hatten?

"Nicht hier.", bestätigte Marusch und legte den Arm schützend um
Liliðs Schultern. "Der Diebstahl hat auf unserem Hof in Angelsoge
stattgefunden. Meine Frau hat dich dabei gesehen, ohne dass du es
gemerkt hast. Unser Imagika hat hinterher mit ihr ein Abbild
von dir erstellt."

Von so etwas hatte Lilið gehört. Wenn Menschen, die im Gegensatz zu
ihr sich an Gesichter erinnern konnten, unter Stress eine Person dabei
beobachteten, ein Verbrechen zu begehen, so wurden hinterher manchmal
speziell ausgebildete Imagikae darauf angesetzt, ein Phantombild
mit ihnen zu erstellen. Der Stress sorgte oft dafür, dass sich
viele wichtige Details für Imagikae erreichbar einbrannten. Imagikae,
die tief in der Wissenschaft der menschlichen Psyche
steckten, konnten dann oft überraschend genau ein Abbild erstellen.

"Natürlich wollen wir nicht, dass eine unschuldige Person zu schaden
kommt.", versicherte Marusch. "Ich verlange daher, dass diese Person
von danmoger Wachen zu unserem Hof nach Angelsoge verbracht wird, damit
geprüft werden kann, dass meine Frau sich nicht irrt. Leider sind wir
nur auf der Durchreise und selbst ohne Wachen hier."

Lilið schüttelte den Kopf, wie um klar zu machen, dass sie sich
definitiv nicht irrte. "Ich bin sicher, aber ich stimme dieser
Gründlichkeit nur allzu gern zu.", sagte sie schwach.

"Das weiß ich doch, mein Lapizstern.", murmelte Marusch ihr ins Haar.

Smutje Andert trat auf sie zu. "Ich werde selbstverständlich die
danmoger Wachen holen.", sagte er. "Ich muss leider aus formalen
Gründen darauf bestehen, wenigstens eine Marke zu sehen. Und aus Gründen
der Kapazität darauf, dass ihr hier bleibt und die Lage im Griff
behaltet, bis ich wieder da bin."

"Selbstverständlich.", erwiderte Marusch. "Für beides habe ich
volles Verständnis. Danmoge ist ja nicht so groß, dass jeder
Imbiss seine eigene Wache hat." Sie löste ihren Arm von Lilið
und holte die beiden Marken hervor, die sie auswiesen.

Lilið überraschte, dass Smutje Andert sie nur kurz betrachtete. Sie
überraschte auch, dass sie damit betraut wurden, den
Imbiss zu bewachen, während Smutje Andert sich einen Mantel nahm
und hinauseilte. Aber letzteres ergab für sie plötzlich einen Sinn, der
sie fast schockierte: Sie waren hier in der Stellung von Lord und
Lady Bärlauch, Personen, die auch auf dem Angelsoger Adelsball willkommen
gewesen waren. Wo sie nicht willkommen gewesen wären, wenn sie nicht
auch derbst skorsch gewesen wären. Mit anderen Worten, sie wurden
hier wie selbstverständlich für zwei Personen mit vielfältiger und sorfälgtig
geschulter Verteidigungs- und Angriffsmagie gehalten. Es musste
für alle hier nun eine bedrohliche Stimmung sein, in der sie lieber
nichts falsch machten, weil sie zu wissen glaubten, dass sie keine
Wahl hätten.

Marusch setzte sich mit Lilið zusammen an den Tisch, an dem
vorhin die Person im Anzug gesessen hatte. Sie drehte den Stuhl
etwas Richtung Raum und behielt ihn im Blick, ohne es zu
verschleiern. Unheimlich sozusagen, in der gegenteiligen Bedeutung
von heimlich.

Es ergab sich kaum ein Tischgespräch in der Runde am mittigen
Tisch. Sie unterhielten sich kurz über die Möglichkeit, dass
die beiden Seeleute von eben etwas damit zu tun haben könnten, aber
kamen rasch zum Schluss, dass das unwahrscheinlich wäre. Diese
hätten direkt die Wachen geschickt, das wäre einfacher
gewesen. Sie besprachen das sehr indirekt, sodass Lilið das
Gespräch nicht verstanden hätte, hätte sie nicht den Kontext
aus ihrem letzten Besuch gekannt. Das Nautika teilte
anschließend noch einmal verzweifelt mit, dass es doch
morgen einen wichtigen Termin hatte, und was es denn nun tun sollte. Der
Schiffskoch gab resigniert zu verstehen, dass das Nautika froh sein
solle, wenn es die neue Situation jetzt überlebte. Eines der
werdenden Nautikae sprach ihm Mut zu, dass, wenn es sich wirklich
nichts zu Schulden kommen lassen hätte, es auf Angelsoge wieder
frei gelassen würde. Die Person aus dem Königreich Sper berichtete,
dass sie auf ihrer Handelsreise im Land auch noch in Angelsoge
vorbeikommen würde, und besonders leise flüsternd fügte sie hinzu,
dass sie gehört hatte, dass Lord und Lady Bärlauch gerechte
Leute sein sollten, die sehr genau prüften, bevor sie Urteile
fällten.

Lilið beobachtete, wie dem Nautika die Tränen kamen. Ihr war
sympathisch, dass es dies nicht zu verbergen versuchte. Der Schiffskoch
nahm es in den Arm und streichelte über seine Schulter. Lilið
taten alle Beteiligten in der Runde leid.

Es kam ihr vor, als wäre eine Stunde vergangen, als endlich Smutje
Andert mit der Wache zurückkam. Auch diese besah sich nur kurz
die Marken, die Marusch abermals vorlegte. Lilið schloss, dass
Lord und Lady Bärlauch wirklich den Ruf haben mussten, sehr
mächtig zu sein, und niemand sich leichtfertig
mit ihnen anlegen würde, wenn selbst die
Wache nicht genauer kontrollierte, ob sie wirklich Lord und
Lady Bärlauch waren. Sie hätten Eichungen prüfen
können, aber das taten sie nicht. Sie führten das Nautika ab,
das sich dabei nicht wehrte, und versicherten, dass sie mit ihm in
spätestens vier Tagen an Liliðs und Maruschs vermeintlichen
Hof eingetroffen sein sollten.

<!--Wenn der Mantel sich zur Jacke faltet, müsste er vielleicht
als die von Maruschs erster Rolle wiedererkannt werden können?-->
Lilið ging auf, wie geschickt das alles war. Auch, dass Marusch
keineswegs einen Kompass in die Taschen des Nautikas geschmuggelt
hatte, also ein Nachweis erst vier Tage später erbracht hätte werden
können. In vier Tagen würde das gesamte Lügenkonstrukt
auseinanderfallen. Das Nautika würde wissen, dass sie nicht Lord
und Lady Bärlauch gewesen wären, hätte aber keine Möglichkeit, sie
je wiederzufinden. Es würde frei gelassen werden und alle wären
verdutzt und verärgert. Aber niemand hätte einen langfristigen
Schaden davon, außer, dass das Nautika nicht zur Crew hatte gelangen
können.

Und ja, Marusch hatte wohl recht mit der Annahme, dass das Nautika
eher vermuten würde, dass sie ihm in der Rolle von Lord und Lady Bärlauch
den Mantel gestohlen hätten. Es war alles sehr elegant eingefädelt.

Marusch bedankte sich, aufrichtig wirkend, bei Smutje Andert. "Die
Störung tut uns wirklich außerordentlich leid.", betonte sie.

"Da könnt ihr ja nichts für.", nuschelte Smutje Andert.

"Trotzdem. Ich hinterfrage oft, wo wir erwünscht sind, und wo
zurecht nicht.", antwortete Marusch. "Es ist schade, wenn wir
diesen Raum nicht immer geben können. Gehabt euch wohl. Ich
wünsche, dass es nicht allzu bald wieder zu Vorfällen wie diesem
kommen wird."

Marusch deutete eine Verbeugung an und legte Lilið die Hand zwischen
die Schulterblätter, um sie gen Tür zu leiten. Wieder hielt sie
ihr die Tür auf. Die Luft fühlte sich besonders frisch in der Lunge
an, als Lilið realisierte, dass bei diesem zweiten Akt wirklich
nichts schief gegangen war. Und für Akt drei hätte sie sicher
einige Stunden Bedenkzeit. Aber vielleicht zu wenig Schlaf, um
diese voll zu nutzen.
