Vorwort {.unnumbered}
=======

Das Buch entstand im Juli und August 2022 in etwa zweieinhalb Monat Schreibzeit. Es
wird, wie all meine Werke, im Laufe der Zeit bearbeitet.

Ich behandele in wörtlicher Rede Punkte gleichberechtigt mit anderen Satzzeichen. Ich setze
mich damit über gängige Grammatik-Regeln hinweg, einfach, weil es mir so besser
gefällt und konsistenter vorkommt.
