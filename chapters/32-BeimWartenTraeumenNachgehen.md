Beim Warten Träumen nachgehen
=============================

*CN: Tierleid, Genitalien, Masturbieren - erwähnt, Körperlfüssigkeiten,
Sex, BDSM, Dominition Submission, Wehrlosigkeit, Extase, Blut,
Beißen, Erinnerung an Massaker, Ratten, Breath Play.*

Lilið fühlte sich albern, als sie sich leise in der
Koje verkroch, nur um kurz darauf von Matrose Ott geweckt
zu werden. Sie war überrascht, wie ausgeschlafen sie sich
fühlte, obwohl sie doch kaum geschlafen hatte. Aber irgendetwas
hatte sich bei ihr im Gespräch mit Lajana gesetzt und ihr mehr
Selbstsicherheit gegeben. Und das, kombiniert mit der
Stimmung, in der sie sich hatte ausruhen und tatsächlich wegdösen
können, hatte zu Erholung geführt.

Die Nacht war sternenklar. Ein weit entfernter Leuchtturm hinterm Horizont
blinkte das Signal, das Lilið zuvor in der Karte gelesen hatte. Das erleichterte die
Navigation. Lilið passte die Karte an den neuen Stand der Dinge an und
fand heraus, dass ihre Annäherung aus den Vortagen viel genauer
war, als sie vermutet hätte. Sie nutzte die Gelegenheit, um die Karte
noch einmal auf einem anderen Rechenweg zu justieren, um die Ergebnisse
zu vergleichen. Die Resultate der Varianten stimmten
fast haargenau überein. Lilið fragte sich,
ob es ihr unheimlich sein sollte. Aber mit einem sichtbaren Leuchtturm mit
passender Kennung und Sternen war es eben nicht so schwierig wie
sonst.

Drude war wieder nicht da. Lilið stellte sich vor, dass dey einfach
im Bett liegen blieb, von dort aus Anwesenheiten und
Magie über die Decks der Kagutte hinweg spürte und wusste, wo Lilið sich ungefähr
befand. Was für eine beeindruckende Person.

Weil besagte Person aber nicht da war, um sich mit Lilið zu unterhalten, hatte
Lilið Zeit, die sie nutzen konnte, um sich einige Möglichkeiten auszudenken,
wann während der Fahrt sie aus welchem angeblichen Grund am besten einen halben
Tag Verzug einbauen könnte. Mit vier verschiedenen Varianten, die sie je nach
Wind und Wetter früher oder später begründen könnte, ging sie wieder schlafen.
Einer der Pläne würde wetterunabhängig klappen, aber den könnte sie erst kurz
vor ihrem Passieren der Insel umsetzen.

---

In ihren zweiten drei Stunden schlief sie tief und fest, aber hatte
einen seltsamen Traum, der sie mehr verstörte als alles, was sie hier
bisher erlebt hatte. Der Traum fühlte sich sehr real an, und das war
eigentlich er auch, weil es eine Erinnerung von ihr war: Sie musste
so um die elf Jahre alt gewesen sein. Sie war mit einem Kescher im
Wasser gewesen, den sie immer wieder durch den Sandboden gezogen hatte. Der
Sand war hinten aus dem Netz wieder herausgeflossen wie hellbraune
Unterwasserwolken. Die Garnelen waren im Netz geblieben. Immer, wenn sie
vier oder fünf im Netz hatte, hatte sie sie am Strand in einen Eimer
getan. Und als er genügend voll war, dass sie für eine Beilage bei
einer Mahlzeit ausreichten, war sie mit dem Eimer nach Hause gegangen
und hatte ihn ihrer Mutter gegeben.

"Willst du sie essen?", hatte sie gefragt.

Lilið hatte genickt.

"Dann musst du sie kochen. Kannst du das?"

Lilið wachte auf. Sie wusste, dass sie sie damals gekocht hatte. Nicht
ohne mit der Wimper zu zucken, aber sie hatte auch nicht lange
gezögert. Sie hatte dabei irgendeine Art seltsamen Stolz empfunden,
dass sie es konnte. Tiere zu töten. Aber gleichzeitig hatte es sich
schlecht angefühlt. Sie hatte das Gefühl verdrängt, weil es so vieles
gab, was sich schlecht anfühlte, was sie trotzdem tat. Zur Schule
gehen zum Beispiel, und so mancher Besuch bei Medikae. Oder
eben Dinge, die sich erst schlecht anfühlten,
mit der Gewöhnung aber allmählich gut. Dinge lernen zum Beispiel, die
sie nicht auf Anhieb irgendwie brauchbar konnte. Aber Lilið hatte
nie gelernt, Gefallen daran zu finden, Lebewesen zu töten. Und
nun überlegte sie, dass das vielleicht einen Grund hatte. Dass
es gar nicht zu ihr passte. Wieso hatte sie sich damals dazu
entschieden, die Garnelen zu töten? Und warum bereitete ihr das in
ihrem Kopf mehr Schwierigkeiten, als bei einem Massaker dabei
gewesen zu sein, bei dem Menschen gestorben waren?

Die letzte Frage konnte sie immerhin beantworten: Zum einen waren
die Menschen nicht ganz so wehrlos gewesen. Wehrlos schon, aber nicht
so völlig ausgeliefert gegenüber einer einzelnen Person, die sie
in einem Eimer mit sich herumschleppen konnte. Zum anderen
trug sie bei den Menschen nicht die Verantwortung. Es war Lilið
widererwartend nicht passiert, sich irgendwann ausversehen verantwortlich
zu fühlen. Vielleicht sollte sie Drude dafür dankbar sein, vielleicht
hatte dey mit dafür gesorgt, dass das bis jetzt nicht passiert war.

Lilið fragte sich erneut, warum Marusch kein Fleisch aß. Ob es
ähnliche Gründe waren, aus denen Lilið es nun nicht mehr tun
würde? Lilið seufzte, diesen Entschluss gefasst habend, erleichtert
und stand auf. Sie hatte das Gefühl, damit einen Weg einzuschlagen,
den sie eigentlich schon als kleines Kind für richtig gehalten
und nur nicht eingeschlagen hatte, weil die ganzen Leute um sie
herum anders lebten, und sie sich deshalb nicht hatte vorstellen
können, dass ihr Wunschweg richtig oder auch nur erlaubt sein
könnte. Sie erlaubte sich ihren Weg jetzt. Egal was die anderen
täten.

Matrose Ott ließ sich dieses Mal Zeit. Er hatte sie nicht, wie
gewohnt, geweckt, aber Lilið wusste, dass Aufstehen nun
an der Reihe war. Sie fand ihn auch nicht an
Deck, als sie ihre ersten Daten des Morgens aufnahm, aber schließlich
im Kartenraum auf dem Boden liegend und schlafend. Sie hätte ihn am
liebsten einfach schlafen gelassen, weil ohnehin für ihn Schlafenszeit
wäre, aber sie wollte den Raum für sich und Drude haben, falls dey
käme.

"In der Koje ist es bestimmt bequemer!", raunte sie ihm zu.

Er erschreckte sich, fuhr aus dem Schlaf, entschuldigte sich mehrfach,
dass nun wichtige Daten fehlen würden, und verzog sich.

Ob das die Gelegenheit wäre, ihm die Schuld für einen Verzug
zuzuschieben? Eigentlich war es dafür viel zu früh. Lilið versuchte
trotzem irgendwie eine Möglichkeit zu erarbeiten, und hatte gerade
herausgefunden, dass es aussichtslos war, als Drude den Raum betrat.

"Ich habe mich ausgeschlafen.", sagte dey. "Gehen wir den Plan noch
einmal wach durch?"

Lilið nickte. "Gern."

Sie taten es zweimal. Einfach um gründlich zu sein. Drude hörte aufmerksam
zu und stellte viele Was-Wenn-Fragen. Lilið gab das ein Gefühl von
Sicherheit. Vor allem, weil Drude es gut machte und dabei dere eigenen
Möglichkeiten eingeschränkt hätte, Lilið irgendwann mit etwas in die
Quere zu kommen. Es fühlte sich nicht nach einer Falle an. Aber Lilið wollte
trotzdem noch einen möglichen Plan B suchen, der unabhängiger von Drude
wäre. Auf der anderen Seite, wenn sie Heelems und vielleicht Maruschs
Hilfe haben wollte, dann müsste sie schon zur passenden Zeit auf diese
Insel gelangen. Wie unabhängig konnte der Plan dann noch sein?

Ab frühem Vormittag überwachte Matrose Ott wieder jeden ihrer Schritte, abgesehen
von den zwei Stunden Schlaf, die sie mit Drude nun zusätzlich in der Koje verbrachte,
bis er am frühen Abend schlafen ging. Das wäre die Zeit gewesen, in der
Lilið sich vielleicht mit einem Plan B hätte auseinandersetzen können. Aber
sie war nicht erfolgreich damit. Es war ein seltsamer Tagesrhythmus. Immerhin
hatte sie nun insgesamt genügend Schlaf gehabt, dass sie brauchbar klar
denken konnte.

Nach dem Abendessen saß Drude wieder mit ihr im Kartenraum. Dey wirkte
rastlos auf Lilið. "Wir könnten theoretisch jetzt unsere Anspannung
herunterfahren, Pause
machen und Kräfte sammeln für wenn es losgeht.", sagte dey. "Es ist alles
gründlich geplant, oder? Aber es wird erst in frühestens eineinhalb
Tagen etwas davon umgesetzt."

Lilið nickte. "Möchtest du Ablenkung?", fragte sie.

"Wenn du welche hast, gern.", bat Drude. "Ich möchte dir immer noch über
deine Macht erzählen, aber lieber morgen früh."

"Wieder lieber nicht vorm Schlafen.", schloss Lilið.

Drude nickte.

Lilið holte tief Luft. Wie sollte sie so etwas anfangen? "Hättest
du eventuell Lust auf Sex mit Lajana und mir?" Lilið hätte die
Gelegenheit auch nutzen können, von einem flotten Dreier zu
reden. Wann bot sich dieses geflügelte Wort schon einmal an?

Über Drudes Stirn huschte ein Runzeln. "Und ich dachte, ich wäre
direkt.", kommentierte dey. "Magst du mir ein paar Details erzählen?"

"Ich habe sie noch nicht gefragt. Das tue ich, solltest du einverstanden
sein.", leitete Lilið ein. "Lajana würde gern Sex mit mir haben, aber
so, dass ich mich nicht wehren kann."

Sie war doch noch nicht wach, stellte Lilið fest. Das waren eigentlich
viel zu persönliche Dinge, um sie einfach so weiterzuerzählen.

Drude blickte sie auf eine Weise an, die Lilið nicht so ganz deuten
konnte, aber irgendwie zufrieden wirkte dey schon. "Und du hast
dir vorgestellt, dass ich dich Lajana ausliefere?"

Lilið bemerkte, dass ihre Knie bei diesem einen Wort bereits weich
wurden. Ausliefern. Sie beobachtete Drude genau, als sie
nickte. Etwas Angst hatte sie schon. Wo führte das hin?

"Mein Interesse ist geweckt.", antwortete Drude und klang dabei
überraschend sachlich. "Ich habe kein
Interesse an Sex. Wenn Leute es in meiner Gegenwart tun, kratzt
mich das nicht, und ich glaube, auch nicht, wenn Lajana Sex mit dir
hat, während ich dich festhalte. Das kann ich mir vorstellen. Selbst
involvierter in Sexuelles zu sein, nicht. Ich weiß nicht, ob die
Grenze irgendwie klar für dich ist."

"Ich glaube schon.", sagte Lilið. "Ich bin nicht sicher. Aber
da sollten wir vermutlich mit Lajana im Detail drüber reden, was
für dich in Ordnung wäre und was nicht. Und ob sie dich überhaupt
dabei haben mag."

"Bevor wir irgendetwas zu dritt besprechen, würde ich gern mit
dir etwas zu zweit ausprobieren. Weil es dir das letzte Mal, als ich
etwas in der Richtung probiert habe, eine Weile nicht gut
ging.", stellte Drude klar. "Magst du?"

Lilið nickte zögerlich. "Das ist wahrscheinlich sinnvoll. Was
genau hast du vor?"

"Im Wesentlichen, dich festhalten.", teilte Drude mit. "Magst
du jetzt?"

"Was muss ich tun?", fragte Lilið. Sie bereitete sich mental darauf
vor, sich hinzuknien, wenn Drude es verlangen würde. Aber eigentlich
war das nicht, was sie wollte. Oder doch? "Im Moment fühle ich mich
unsicher."

"Ich werde jedes Mal, wenn ich etwas ändere, vorher fragen.", versprach
Drude. "Und wenn du nicht mehr willst, sagst du 'Stopp' oder so etwas."

Lilið nickte. Auch das waren schon einigermaßen klare Anweisungen. Woher
nahm Drude die Sicherheit? Lilið spürte überhaupt keine. Aber ein Teil
davon, keine zu haben, fühlte sich gut an. Noch nicht so gut wie
neulich.

"Dreh mir den Rücken zu.", wies Drude sie an.

Da war es. Lilið spürte, wie ihre Vulvalippen irgendwie reagierten.
Anschwollen? Kribbelten? Sie drehte sich um, spürte, wie weich sie
wirklich in den Knien war.

"Darf ich meinen Arm von hinten durch deinen Ellbogen fädeln und
dich festhalten?", fragte Drude.

Das würde ihr beim Stehen helfen, dachte Lilið. "Ja.", stimmte sie
leise zu.

Sie hörte Drude auf leisen Sohlen näher kommen, und hielt die Arme
bereitwillig nach hinten, damit Drude sie festhalten könnte. Drudes
Griff war angenehm fest. Lilið atmete schneller und merkte abermals,
wie ein Gefühl von Anspannung durch ihren Körper rann. Sie spürte
Drudes Körper von hinten gegen ihren gedrückt. Sie mochte dere
breiten Schultern, die sie durch Drudes eng aliegende Kleidung
fühlte. Sie spürte deren Atem und roch den herben Geruch.

"Darf ich meinen anderen Unterarm an deinen Hals legen?", fragte
Drude.

Lilið wurde schwindelig bei der Vorstellung, aber sie wollte es
auch. Sie nickte. Sprechen war schwierig in diesem Zustand.

Drude war relativ groß. Trotzdem musste dey Lilið ein bisschen in die
Hocke ziehen, um mit dem anderen Arm über ihre Schulter zu reichen. Dey
legte ihn sanft, aber überzeugt an Liliðs Kehle. Lilið hatte das Gefühl zu
schmelzen. Ihr Atem flatterte. Wo sollte das hinführen? Warum tat
es das mit ihr?

"Darf ich meine Arme in Fischform bringen?", fragte Drude sehr leise
in Liliðs Ohr.

"Ja." Liliðs Stimme war nichts als ein Flüstern.

Drudes Stimme hatte etwas Gieriges an sich gehabt. Das hatte
Lilið noch mehr erregt. Drudes Arme verwandelten sich fließend, bis
die spitzen Fortsätze und Schuppen in Liliðs Arme hinter ihrem
Rücken drückten, und gegen ihren Hals. Wenn sie sich jetzt
versuchen würde zu befreien, wäre es gefährlich. Sie würde
mindestens Kratzer abbekommen, aber wenn Drude sie dann nicht
frei ließe, könnte ihr noch viel mehr passieren.

Lilið realisierte, dass sie sich gerade Drude ausgeliefert
hatte. Sie merkte, dass sich Körperschleim in ihrer Hose
sammelte. Es war ein Gefühl wie geliebt werden, nur wesentlich
stärker und irgendwie anders. "Ich bin sehr erregt.", flüsterte
sie. "Stört dich das?"

"Ich mag das." Drudes Stimme klang gelassen und hatte etwas
Dominantes an sich.

Lilið stellte sich vor, wie bewusst demm war, dass dey mit
Lilið nun alles hätte machen können. "Was hast du davon?", flüsterte
sie. "Warum magst du es?"

"Ich mag den Moment sehr, in dem sich mir eine Person vollkommen
übergibt. Die Kontrolle über sich ganz abgibt und sie mir
schenkt.", erklärte Drude.

Liliðs Atem zitterte, als die Wucht der Worte zu ihr
durchdrang. Sie verstand erst, was es
für Drude bedeutete, nachdem ihr Körper sich damit auseinandergesetzt
hatte, dass sie besagte Kontrolle abgegeben hatte. "Warum ist
das so schön?", fragte sie. "Warum will ich das?"

"Ich weiß es nicht." Drudes geraunte Worte vibrierten unter Liliðs
Haut. "Wahrscheinlich muss es ein Gegenstück zu meinem Fetisch
geben, das du eben zufällig hast."

Lilið hatte das Wort Fetisch noch nie in einem positiven Kontext
gehört, stellte sie fest. "Meiner ist allerdings sexuell und deiner
nicht.", flüsterte sie und wiederholte die Frage von vorhin: "Ist
das schlimm?"

"Solange ich nichts Sexuelles machen muss, mag ich es.", widersprach
Drude. "Und da ich die Kontrolle habe, suche ich mir aus, was ich mit
dir mache. Ich würde, wenn ihr das wollt, zum Beispiel Lajana deine
Erregung zur Verfügung stellen. Keine Ahnung, was sie damit machen
will." Unvermittelt ließ dey Lilið los.

Lilið sank zitternd auf den Boden. Sie würde irgendwann masturbieren
müssen, überlegte sie. Oder sich das für Lajana aufsparen.

"Brauchst du Hilfe?", fragte Drude.

"Ich muss mich nur kurz erholen.", widersprach Lilið. "Ich bin immer
noch sehr überwältigt davon, was es mit mir macht."

"Das kann ich verstehen.", antwortete Drude. Dey setzte sich auf den
Stuhl, auf dem Lilið sonst immer saß. "Ich mache so etwas
auch nicht oft und das letzte Mal ist lange her. Es
war sehr schön. Danke dir."

Lilið rappelte sich auf und setzte sich auf den Stuhl gegenüber, nur
gerade so auf die Stuhlkante, um nicht die Feuchte in ihrer Unterhose
durch sie hindurch in ihre Hose zu drücken. "Kommst du gleich mit
zu Lajana?", fragte sie.

"Du müsstest mich falten. Kannst du das?", fragte Drude. "Denn
ich komme sonst nicht durch die Tür, ohne die Tür aufzumachen."

"Oh, an das Problem habe ich nicht gedacht.", murmelte Lilið. "Ich
kann dich im Vorfeld falten, glaube ich. Dazu hatten wir genug
Nähe und Körperkontakt." Natürlich hatte sie Drude erfühlt,
während sie aneinandergeschmiegt geschlafen hatten. "Aber ich falte
mich auch immer um, wenn ich mich unter der Tür durchschiebe, und
in gefaltetem Zustand habe ich noch nie auch noch eine andere
Person gefaltet."

"Üben wir das dann jetzt?", fragte Drude. "Es ist ohnehin nicht
verkehrt, das zu können."

Lilið nickte.

---

Nach einigen Faltversuchen stellte sich heraus, dass Lilið
Drude zwar durchaus falten konnte, während sie selbst gefaltet war, dass
es aber viel einfacher war, sie beide von vornherein in eine
Ratte zu falten. Drude war in der Faltung um sie herumgewickelt
und musste nicht einmal üben, sich halbwegs wie eine Ratte zu
bewegen. Es war eine interessante neue Erkenntnis für Lilið, dass
sie sich als Paar falten konnte, aber es ergab sofort Sinn: Sie
konnte ja auch zwei Stück Papier zu einem Modell falten. Sie hatte
viel mehr Kontrolle über einen fremden Körper, wenn er direkt
an sie geschmiegt war. Es war vorteilhaft, wenn das während der
Faltung so blieb.

---

Lajana starrte sie sehr irritiert an, als Lilið dieses
Mal sie beide entfaltete. "Warum wart ihr nicht zwei
Ratten?", fragte sie. "Oder
liegt das daran, dass du ein Fisch warst, und Lilið dich
verschluckt hat?"

Lilið kicherte bei der Vorstellung und Drude schüttelte den
Kopf.

"Wir waren zu zweit eine Ratte.", erklärte Drude.

"Es ist, wie wenn du eine Ranich faltest.", fügte
Lilið hinzu. Die meisten Menschen lernten während ihrer
Kindheit, Raniche zu falten. Vielleicht hatte Lajana es
auch getan oder zumindest mal dabei zugesehen. "Nur aus
zwei aneinander gelegten Papieren."

"Sodass die Ranich zweifarbig ist?", fragte Lajana.

Lilið nickte. "Bei zwei verschieden farbigen Papieren im Prinzip
schon.", sagte sie. "Aber bei der Ranich wäre das eine der Papiere
komplett innerhalb des anderen. Und so war das auch ungefähr
bei uns."

Lajana schloss die Augen und dachte nach. Sie wirkte frustriert,
als sie sie wieder öffnete. "Das müsstest du mir irgendwann zeigen,
Lilið, das verstehe ich nicht.", sagte sie. "Aber ich will zuerst
wissen, warum ihr beide hier seid. Willst du jetzt doch Sex mit mir
haben, Drude?"

Lilið konnte sich schon wieder ein Lachen nicht ganz verkneifen. "Hast
du demm auch gefragt?"

"Hat sie. Sonst wäre ich nicht direkt mitgekommen, sondern hätte
euch erst einmal ohne mich darüber reden lassen.", antwortete
Drude und wandte sich an Lajana. "Ich möchte
keinen Sex mit euch haben. Immer noch nicht. Aber ich würde dir Lilið
ausliefern, wenn du das möchtest. Also, festhalten, sodass sie sich
nicht wehren kann, während du mit ihr was auch immer machst."

Lilið hatte irgendwie damit gerechnet, dass es mehr Vorbereitung
hätte geben müssen, bis sie das eröffneten. Aber vielleicht ergab
das auch einfach Sinn.

"Ich hatte an Fesseln gedacht.", murmelte Lajana.

Lilið kicherte abermals. "Wieso bin ich darauf nicht gekommen?" Wieso
fühlte sie sich so sehr nach albern und kicherte dauernd?

"Wollt ihr ohne mich?", fragte Drude.

Lajana blickte zwischen ihnen hin und her. "Hast du einen Vorzug, Lilið?"

Lilið zögerte, aber nickte dann. "Schon.", sagte sie. "Aber ich komme mir
dabei sehr egoistisch vor. Und als würde ich die Situation ausnutzen."

"Sag einfach, was du willst.", sagte Lajana. "Ich kann dann immer noch
sagen, ob mir das gefällt oder nicht. Du kannst ja in jedem Fall nicht
viel machen."

Lilið grinste verlegen. "Ich würde gern von Drude ausgeliefert werden.", sagte
sie. "Ich glaube, das hat sich zu einem Traum von mir entwickelt. Aber
ich glaube, wenn dir das nicht recht ist, finden Drude und ich auch
unabhängig davon noch einmal eine Möglichkeit." Allerdings fand Lilið
die Vorstellung eigentlich angenehm aufregend, für Lajana ausgeliefert zu
sein.

Lajana blickte noch einmal zwischen ihnen hin und her und wirkte
nachdenklich. "Also Drude würde einfach nur deine Arme festhalten,
als wärest du gefesselt. Aber eben mit menschlichen oder fischlichen
Fesseln.", fasste sie zusammen. "Und ich würde dann Sex mit dir haben,
während du wehrlos bist und das genießt."

Lilið schluckte. Sie würde dann gleich zwei Leuten auf verschiedene
Art ausgeliefert sein. Sie nickte. "Genau.", nuschelte sie.

"Darf ich dir die Hose dann ausziehen?", fragte Lajana.

Lilið nickte noch einmal. "Ich würde gern wieder so etwas wie
'Stopp' sagen können, falls es zu viel wird.", sagte sie. "Hast
du vor, etwas mit meiner Vulva zu machen?"

Lajana schüttelte den Kopf. "Ich glaube, dafür fühle ich mich zu
unsicher.", sagte sie. "Ich würde mich gern, wie gestern Nacht
an deinem Bein reiben. Aber dieses Mal, dass es dabei nackt
ist. Ich mag deine Weiche. Mochtest du das mit dem Bein gestern?"

Lilið nickte. Sie merkte, wie sie vor Anspannung jetzt schon
zitterte. Ein Teil von ihr wollte, dass diese Warterei vorbei
wäre. Dass Lajana jetzt doch 'nein' sagen würde, oder dass sie
schon mitten dabei wären.

Lajana und Drude begannen eine Absprache über die Haltung, in der
Drude Lilið fixieren sollte. Lilið merkte, wie sie mochte, dass sie
nicht mehr ins Gespräch involviert war. Ab nun wurde über sie
bestimmt. Ihr Körper reagierte.

Sie war schon halb in einer Art Trance, als Drude sie auf die
Seite legte und sich selbst hinter sie. "Ich fädele nun den
Arm wie vorhin ein und fixiere dich.", sagte dey.

Lilið nickte. Sie spürte diese unendliche Nähe zu Drudes
Körper, als Drudes Arm ihre Oberarme an deren Körper presste. Im
Liegen musste Lilið keine seltsam halb hockende Körperhaltung
einnehmen, damit Drudes anderer Arm um ihren Hals gelegt werden
konnte. Sie konnte sogar dem Druck von Drudes Unterarmmuskeln
nachgeben und ihren Kopf nach hinten kippen. Nun lang
er unter Drudes Kopf eingeklemmt auf dem
Boden.

Drude drehte sich samt Lilið halb auf den Rücken. Lilið sah es
nicht, aber fühlte, wie sich Lajana an ihrer Hose zu schaffen
machte. Obwohl Liliðs Beine nicht fixiert waren, hätte sie mit
ihnen nicht viel machen können. Sie waren labberig und gehorchten
ihr nicht.

Sie kam halb zu sich, als sich die Zeit, die Lajana sich mit den
Knöpfen abmühte, in die Länge zog.

"Brauchst du Hilfe?", fragte Drude leise.

Lilið bekam Lajanas Antwort kaum mit, aber Drude löste den Arm
von ihrem Hals um ihr an die Hose zu fassen. Drude schaffte
es mit einer Hand, die Knöpfe zu öffnen, kurz und schmerzlos,
und Lilið merkte, wie sie dabei sehr feucht wurde. Ihr
Atem zitterte.

Während Lajana nun ihre Hose herunterzog, legte sich Drudes
Arm wieder um Liliðs Hals, vielleicht eine Spur überzeugter
als vorher. Eine Spur, die bei Lilið Schnappatmung auslöste.

"Magst du es?", fragte Lajana.

"Sehr.", flüsterte Lilið.

Und auch von Drude kam ein zustimmendes Summen. Dunkel, dachte
Lilið. Sie fühlte sich wie eine Beute, die schon aufgehört
hatte, zu zappeln. Und dieses Gefühl durchdrang ihren ganzen
Körper, elektrisierte sie.

"So seht ihr auch aus.", sagte Lajana.

Sie klang glücklich, fand Lilið. Aber dann konnte sie nicht mehr
denken. Sie fühlte Lajanas Hände, die um ihre Hüfte griffen, wo sie sich überzeugt
festhielt, während sich ihre Beine um Liliðs unteren Oberschenkel
schlangen und sie sich positionierte. Lilið spürte die
Feuchtigkeit auf ihrem Bein. Es erregte sie sehr, die Intensität
überraschte sie. Mehr als sie eine Berührung im Genitalbereich erregt
hätte. Sie fühlte, wie Drude sie festhielt, gegen deren Körper
gepresst, wie dieser ruhig, aber auf seine Art erregt atmete. Ihr
wurde bewusst, dass Lajana ihren Körper in einer Weise nutzte,
der vor allem Lajana stimulierte. Ihr Körper stand einfach zur
Verfügung, wurde benutzt, und das ließ sie mehr in diesen Zustand
abdriften. Tiefer noch, als sie für möglich gehalten hätte. Sie
merkte erst nach einer Weile, dass die fiepsenden Geräusche beim
Atmen von ihr kamen. Aber auch Lajana keuchte. Sie bewegte sich
nicht schnell, aber voller Anspannung und mit wachsendem Wollen,
das spürte Lilið. Und dann spürte sie Drudes Mund an ihrer
Halsbeuge.

Lilið nutzte den geringen Bewegungsspielraum, den sie hatte, um
sich dagegen zu lehnen. Drude öffnete die Lippen und drückte die
Zähne sachte gegen ihren Hals. Dann löste dey sich zu Liliðs
Enttäuschung wieder. "Wenn du dich nicht sträubst, werde ich
dich in den Hals beißen.", flüsterte dey.

Lilið atmete mit einem Mal so schnell, dass ihr schwindelig wurde. Wieder
spürte sie die Zähne gegen ihren Hals drücken. Dieses Mal waren sie
spitzer. Durch Lajanas Bewegungen drückten sie rhythmisch mehr und
weniger gegen die Haut. Aber sie stachen nicht durch sie hindurch, ruhten da einfach.

Lilið zitterte. Sie wollte so sehr, dass Drude biss. Sie fühlte
sich wundervoll, so eingeengt zwischen zwei Leuten, die sie genossen,
die sie einfach ausbeuteten. Warum biss Drude nicht zu? Unter ihrer
Haut an ihrem Hals loderte ein Verlangen danach, noch mehr von
Drude vereinnahmt zu werden.

Drude löste die Zähne wieder von ihrem Hals. "Ich traue mich doch nicht.", raunte
sie leise. "Nicht, ohne dass du mich darum bittest."

Lilið fühlte sich, als würde ihr ganzer Körper auseinanderrinnen. Ihre
Unterhose war durchnässt mit Körperschleim. "Bitte.", flehte sie. Ohne
Luft und doch mit verzweifelter Energie.

Wieder spürte sie Zähne in ihrer Halsbeuge. Lajanas Griff an ihrer Hüfte
klammerte fester. Lilið merkte, wie sie ihr das Bein doch reflexartig
und Einverständnis erklärend etwas entgegendrückte, und mochte, dass
darauffolgende, zarte Aufseufzen Lajanas.

Dann verlor sie alles an Kontrolle, gab vollkommen auf und ließ los, als
die Zähne durch ihre Haut in sie eindrangen. Nichts gehörte mehr ihr. Sie
zerfloss zu einem dunklen Rauschen in einen kühlen Abgrund aus Aufgabe. Sie
spürte Drudes Zunge auf ihrer Haut, ihr Blut leckend, die festen Griffe
beider Personen, die ihren Körper fixierten, und ließ sich einfach fallen
in tiefe Schwärze.

---

Als sie wieder zu sich kam, war sie so entspannt wie vielleicht noch nie
in ihrem Leben. War sie bewusstlos gewesen? "Geht es euch gut?", fragte
sie.

Drude fixierte sie nicht mehr, lag aber noch an sie gekuschelt. Lajana
saß im Schneidersitz vor ihrem Gesicht. Sie nickte. "Drude meinte, ich
solle mir keine Sorgen um dich machen."

"Aber du hast dir trotzdem Sorgen gemacht?", fragte Lilið alarmiert.

"Ein wenig.", antwortete Lajana zaghaft. "Aber du wirktest auch so
glücklich!" Sie runzelte die Stirn. "Oder nicht glücklich, aber etwas
anderes sehr Gutes."

Lilið nickte. "Aber ich habe euch dabei fast vergessen.", sagte sie. "Nein,
nicht vergessen, aber mir
ist, glaube ich, das passiert, wovor ich Angst hatte. Dass ich in einen
Rausch gerate und mir einfach nehme. Wobei ich eigentlich nicht so richtig
in der Lage war, mir zu nehmen."

"Für mich war das sehr schön.", sagte Drude. "Für mich war schön, dich
auszuliefern, vorhin beim Üben ja auch schon. Aber ich weiß dann eben nicht
so recht weiter. Ich fand es sehr schön, dich also einer Person zur
Verfügung zu stellen, die dich dann einfach benutzen kann."

Lilið bemerkte, dass ihr Körper auf die Worte schon wieder reagierte, aber
nicht so stark, wie es zuvor gewesen war. Sie hätte damit gerechnet, nun
erst recht ein Bedürfnis zu haben, zu masturbieren, aber das war nicht der
Fall. Durch diesen einen Moment, in dem sie sich so vollständig losgelöst
gefühlt hatte, war sie interessanter Weise befriedigt.

"Hattest du einen Orgasmus?", fragte Lilið Lajana.

Diese schüttelte den Kopf. "Also, es hat irgendwann aufgehört, dass
ich mehr wollte.", sagte Lajana. "Das habe ich auch manchmal, wenn ich
es mir selber mache. Dass es nicht so richtig ein Höhepunkt ist, sondern
einfach so aufhört."

"Hättest du gern einen gehabt?", fragte Drude.

Lajana schüttelte noch einmal den Kopf, dieses Mal energisch. "Ich
hätte nichts dagegen gehabt, aber darum ging es mir nicht.", sagte
sie. "Es war schön. Ich wollte Sex mit einer Person haben, und jetzt
hatte ich Sex mit Lilið und du warst auch dabei. Und es hat sich schön
und fast ungefährlich angefühlt."

"Vielleicht müssen wir an diesem fast arbeiten.", murmelte Drude. Dey
streichelte Lilið fest über den Arm. Immer noch mit Flossenhänden, aber
irgendwie waren diese weniger dornig. Vielleicht waren sie nur halb
transformiert.

"Ich fand den Moment, wo es sich ein bisschen gefährlich angefühlt
hat, eigentlich sehr schön.", widersprach Lajana. "Du hast Lilið
gebissen und sie wollte das. Ihr saht dabei beide sehr genießend
aus und das war ein wunderschöner Moment. Wirklich! Aber dann
war Lilið eben plötzlich weg. Das hat mir ein bisschen Angst gemacht."

"Ich weiß nicht einmal, wo ich war." Lilið versuchte, sich zu
erinnern. "Ich war ganz weg, denke ich. Es war entspannend. Ich
fühle mich gerade sehr wohl. Es ist nichts Schlimmes passiert."

Lajana lächelte. "Dann mochte ich das auch alles. Ich fand
schön, wie du immer mehr aufgegeben hast. Weil du das magst.", sagte
sie. "Mich hat dein Gefiepse erregt. Ich glaube, ich kann nur, wenn
die andere Person genießt. Danke euch, dass ihr das mit mir
gemacht habt."
