Skoremetrikae im Wohnzimmer
===========================

*CN: Misgendern - ziemlich penetrant, Mysogynie und Sexismus, sexuelle Übergriffigkeit*

Fremde Personen im Wohnzimmer waren an sich nichts Ungewöhnliches im Hause
Lurch. Diese zwei bereiteten Lilið allerdings von Anfang an Unbehagen. Die eine
Person war zu, hm, gediegen? Lilið erkannte sie eigentlich als ihre ehemalige
Bastellehrerin aus der Erstschule, nur dass sie nun so viel unnahbarer wirkte,
dass Lilið für nicht ausgeschlossen hielt, dass es sich womöglich um einen Zwilling
handelte. Ihr Rücken war besonders gerade, die Bewegungen stellten einen
Kontrast zu allem dar, was auch nur ansatzweise an Hektik erinnern könnte. Sie
trug Hellblautöne und Seide. Hinweise darauf, dass sie gesellschaftlich etwas
aufgestiegen wäre, wenn sie einst besagte Erstschullehrerin gewesen war. Und Hinweise
darauf, dass sie eine Frau wäre, aber darauf gab Lilið aus Gründen nicht
viel. Sie war kein Fan von Dingen, die Geschlechter trennten oder
kodierten, und glaubte, dass so einige Menschen da eher unfreiwillig
mitmachten. Allerdings wirkte diese Person eins mit sich, ihrer Haltung
und ihrer Kleidung. Sie stand nah am Tisch, eine ihrer langgliedrigen
Hände auf eine Stuhllehne gelegt. Der Braunton ihrer Haut war so
hell, dass er kaum einen Kontrast zum lackierten Irkenholz des
Stuhlrahmens bildete. Lilið fand diese Beinaheeinfarbigkeit durchaus
schön. (Unabhängig von der Helligkeit.)

Die zweite Person hatte sie auch noch in verblichener Erinnerung, aber sie
kam nicht sofort darauf, woher. Sie trug ein blassrotes, matteres
Gewand, das trotzdem versuchte, elegant zu wirken. Lilið schmunzelte mal
wieder bei dem Gedanken an das Paradox, dass die männlich kodierte
Kleidung weniger elegant war, weniger Glanz und weniger entspannende
Farben hatte, aber besonders höherrangige Personen, elegant aussehen
wollten, was bei Männern, (oder eben Menschen, die sich entsprechend
kodieren wollten oder mussten,) zu gewissen Konflikten führte. Manchmal
glänzte die Spitze doch, manchmal war doch ein Hauch Blau irgendwo
untergemischt, aber eben nicht zu viel. Diese fast fremde Person, die
sich gerade mit überschlagenen Beinen auf einem Lehnstuhl am Tisch
niederließ, hatte zum Beispiel ein glänzend fliederfarben eingefasstes
Revers.

Ihr Vater, Lord Lurch, trotzte mal wieder allen Modenormen. Er trug ein blassgrünes
Gewand, auf das mit silbrigem Faden unsauber Drachen gestickt waren, auf denen Lurche
ritten. (Worauf sollten sie auch sonst reiten?) Die Robe war mit Stoffeinsätzen versehen,
die darauf hindeuteten, dass das Kleidungsstück einst nicht auf seinen Körper zugeschneidert
worden war, sondern angepasst hatte werden müssen.

Lilið hatte sie damals für ihn gebastelt, als sie so 12
gewesen war, also vor etwa sieben Jahren. Als sie herausgefunden
hatte, dass die Farbe Blau nicht zufällig eine war, die viele Kinder mochten, sondern
dass diese Kinder alle Mädchen waren. Sie erinnerte sich, damals weiterbeobachtet zu
haben, dass Jungen fast alle Farben trugen, aber schon recht häufig blassrot. Dass
Erwachsene weniger verspielte Kleidung trugen als Kinder und die Stoffe vom Adel und
anderen höherrangigen Leuten andere Beschaffenheiten hatten als die von weniger
veradeltem Volk.

Sie hatte also Grün gewählt, eine Farbe, die möglichst wenig Bedeutung hatte, und
darauf ein verspieltes Muster gestickt, das ihr damals passend erschienen war. (Sie
fand es heute immer noch sehr passend.) Obwohl sie eigentlich nicht
gut im Sticken war. Sie hatte diese Robe ihrem Vater zum Namenstag geschenkt. Er
wusste nicht um ihre Motive, ausgerechnet Grün zu wählen und verspielte
Motive zu wählen, vermutete Lilið. Solche Gedanken traute niemand
einem kleinen Mädchen zu, selbst wenn es in grünen Feldhosen herumlief und sich
von einem Tag auf den anderen weigerte, Blau zu tragen.

Lord Lurch liebte dieses Gewand und trug es oft und voll Stolz. Es machte ihn
froh. Ihn kratzte das Gemunkel der Leute nicht. Es wurde zudem auch höchstens
vorsichtig gemunkelt. Mit Lord Lurch war zwar gut spaßen, aber es sich mit ihm
zu verspaßen war wiederum kein Spaß. Er herrschte nicht ohne Grund über die
ganze Insel, und es war keine der kleineren.

Der Besuch unterhielt sich mit Lord Lurch zunächst über Politik, besonders
Bildungspolitik, dann über Wetter und Nautik, und schließlich über Skorem, -- und
das durchaus mal anders, als sonst darüber gesprochem wurde, weshalb Lilið
hier genauer zuhörte. Skorem war letztendlich ein Maß für das Potenzial
von Menschen, Magie zu erlernen. Eine Person mit Skorem von 100 hatte ein
recht durchschnittliches Potenzial. Ab einem Skorem von etwa 130 wurde eine Person
als skorsch beschrieben.

Liliðs Skorem war dafür, dass sie das Kind von Lord Lurch war und es hieß, Skorem
wäre vererbbar, recht niedrig. Im Vergleich zur Bevölkerung war er einfach
durchschnittlich. Er war irgendwann in der Erstschule
einst auf 102 geschätzt worden. Geschätzt, auf 102. Lilið hatte es schon damals
absurd empfunden. Skorem war nicht einfach zu schätzen, weil viele Faktoren
eine Rolle spielten. Dass erfahrene Lehrkräfte es brauchbar können mochten,
konnte Lilið sich noch vorstellen, aber nicht auf Einer genau.

Lilið war in der Schule nie gut in Magie gewesen. Nicht, weil sie das Fach nicht
interessiert hätte. Sondern weil die Vermittlung unangenehm gewesen war. Wer
nicht gut in Magie war, war sozusagen weniger wert, oder wurde dazu gebracht,
sich so zu fühlen. Eine schlechte Zensur in Basteln führte einfach nicht zu den
gleichen belastenden Auswirkungen wie eine schlechte Zensur in Magie. Lilið hatte
sich in Magie angestrengt, auf einem schlechteren Mittelmäßig zu bleiben, damit
ihr Vater ihr keine Nachhilfe verpassen würde. Ihr Lernen war also angstmotiviert
gewesen, selbst wenn es keine allzu starke Angst gewesen war, was den ganzen Spaß,
den Magie vielleicht hätte machen können, jedenfalls verdrängt hatte. Zudem
gab es zu Magie nicht nur den einen Zugang, in der Schule wurde aber nur einer
vermittelt. Dieser schlaue Gedanke war nicht auf Liliðs Mist gewachsen. Ihre
Mutter sprach oft davon, dass sie vielleicht besser in Magie gewesen wäre, wäre
es ihr anders, spielerischer, vermittelt worden. Und ungefähr um so etwas
versprach das Gespräch zu gehen.

"Neuere Studien zeigen, dass wir unsere Skorem-Tests überarbeiten
sollten.", sagte Frau Wolle -- sie hatten sich inzwischen wieder
vorgestellt. In ihrer Stimme schwang kaum eine Emotion mit. "Sie
bevorteilen Kinder aus Familien mit guter magischer Ausbildung, weil
sie von ihren Eltern schon eine Menge mitbekommen."

Sie hatten sich inzwischen alle an den Tisch gesetzt. Frau Wolle
saß direkt neben ihr, Herr Hut und ihr Vater auf der anderen Seite
des Tisches. Lilið mochte den schmalen Tisch nicht. Wenn sie die
Füße ausstreckte, waren da immer andere im Weg.

"Mein Spross hatte da beides.", erwiderte Liliðs Vater. "Ihre Mutter
hat wenig Ausbildung genossen. Leider scheint von mir aber nicht viel
abgefärbt zu haben."

"Der Bias der Tests kommt nicht nur durch das Elternhaus
zustande.", fuhr Frau Wolle fort. "Auch
Kinder mit anderen Denkstrukturen schneiden schlechter ab, als sie
eigentlich sind."

Lilið horchte genau auf, ließ sich allerdings nichts anmerken, sondern
steckte sich gespielt desinteressiert einen schon fast aufgegessenen Keks in
ihren Mund.

"Mädchen und Frauen zum Beispiel.", ergänzte Herr Hut.

Lilið ließ sich die Enttäuschung genau so wenig anmerken, wie das
Interesse zuvor. Eine Enttäuschung, die sie nicht vollständig
einordnen konnte. Sie fühlte sich immer sehr unbehaglich, wenn
sie einer Gruppe zusortiert wurde, die nur aus Frauen bestand. Oder
wenn ihr eine Eigenschaft zugeschrieben wurde, weil sie eine
Frau wäre, selbst wenn diese zutreffen könnte, wie zum Beispiel, dass
sie deshalb von Magie ein anderes Verständnis haben müsste.

"Deshalb wurden nun mehr Frauen als Skoremetrikae eingestellt, die, bis
die Tests nachgebessert sind, auch ohne Tests ihre Urteile über
Mädchen und Frauen abgeben dürfen. Ich bin noch nicht ganz sicher, was
ich davon halte, aber es ist ja auch nur ein Provisorium.", fuhr
Herr Hut fort. Er sprach auf eine herzlich, einladende Weise, die
Liliðs Vater anzustecken schien.

Lilið war sich ziemlich sicher, dass sie auf eine sehr andere Art
nichts davon hielt als Herr Hut, und das war sehr frustrierend. Die
Atmosphäre fühlte sich für sie beklemmend an. Ein Teil von ihr
hätte nun liebend gern bessere Magiefähigkeiten gehabt und die
Tischdecke in Brand gesetzt.

Stattdessen zog Frau Wolle ein Etui aus einer in ihrer Kleidung
eingenähten, unauffälligen Tasche, entnahm ihm ein Papier
und legte es auf besagte, nicht brennende Tischdecke vor Lilið ab. Es war
nicht einfach nur ein Papier. Es gab Falten darin. Berg- und Talfalten, und
zwar viele. Liliðs Gehirn arbeitete, ohne es überhaupt zu berühren,
sofort los. Es formte sich in ihrem Kopf, den Falten entsprechend,
sortierte Papierlappen unter andere, weil sie sonst nirgens
hinkonnten und innerhalb von wenigen flachen Atemzügen bildete
sich in Liliðs Kopf das Konzept einer Batrosse, eines eher
großeren Seedrachens.

Lilið versuchte, sich unauffällig zu verhalten. Sie wusste, dass
sie geistig kurz weggewesen war, in einem ganz eigenen
Raum in ihrem Kopf. Sie hörte nun das Gezwitscher der Sangseln
aus dem Garten plötzlich sehr intensiv. Die schwarzen Singdrachen
waren optisch nicht sehr beeindruckend, und das obwohl schwarz
eine beeindruckende Farbe war. Aber sie waren weder majestätisch noch
winzig und versteckten sich in Bäumen. Aber der Gesang der
Sangseln war beeindruckender und vielfältiger, als der fast
jeder anderen Drachenart.

Lilið atmete langsam ein und aus, um sich zu erden. Ebenfalls unauffällig. Sie
schloss durchaus, dass gerade ihr Skorem neu gemessen werden sollte. Von ihrer ehemaligen
Bastellehrerin, die nun also Skoremetrika war. Skoremetrikae vermaßen
beruflich Skorem. Aber was sollte das bringen? Das wollte Lilið
gern vorher wissen, bevor andere an irgendwelche neuen Werte gelangen
würden. Müsste sie dann noch einmal zur Schule gehen? Sie hatte gerade
ihren Mittelabschluss gemacht und überlegte, Nautika zu werden. Ein
Beruf, bei dem sie wenig Besitz gebraucht hätte und Magie entsprechend
nicht so nötig wäre, um ihn zu beschützen. Auch ein Beruf mit einer
kniffligen Ausbildung, die ihr niemand so recht zutraute, weil Leute
glaubten, wer navigieren könnte, müsste eigentlich auch in Magie gut
sein und umgekehrt.

"Sie scheint mir wirklich nicht besonders skorsch.", murmelte
Herr Hut.

Lilið fühlte sich leicht provoziert, aber gab dem Impuls nicht
nach. Vorsichtig berührte sie das Papier, um es zur Seite zu
schieben und aufzublicken. "Was wäre denn der Plan, wenn ich
nun diesen Alefinten falten würde?"

Sie nahm das sehr sachte Schmunzeln wahr, das sich für einen kurzen
Moment auf Frau Wolles Gesicht verirrt hatte. Lilið wusste nicht, wie
sie es deuten sollte. Sie hätte eher mit Enttäuschung gerechnet. Aber
vielleicht wäre es besser gewesen, nicht einen Alefinten
vorzuschlagen, weil es bewusst nach falscher Fährte wirken mochte?

"Oder will niemand über den Alefinten im Raum reden?", fügte
Lilið also hinzu, um harmlos zu begründen, warum sie auf einen
Alefinten gekommen war.

Dieses Mal regte sich nichts in Frau Wolles Gesicht. "Es ist jüngst
auf der Insel Frankeroge ein Internat für skorsche Damen eröffnet worden."

"Und ihr denkt, dass ich dahingehen sollte?" Lilið betonte es mehr als
gemutmaßten Fakt als als Frage.

"Sofern wir dich als ausreichend skorsch einschätzen, ist das das
Ansinnen.", erklärte Herr Hut. Er streckte und dehnte sich etwas, und
wie zufällig berührte sein Fuß Liliðs ohnehin schon möglichst
platzsparend zurücksortierte Beine.

"Ich glaube, das Problem ist eher, dass ich keine Dame bin." Lilið
versuchte trotz allem, den Anschein selbstsicherer Entspanntheit zu vermitteln. Innerlich
machte sie sich auf ein stressiges Gespräch gefasst und arbeitete
an einen Fluchtplan, warum sie ausgerechnet jetzt das Tischgespräch
verlassen müsste.

"Was ist so schlimm daran, eine Dame zu sein?", fragte Frau Wolle.

Sie klang interessiert, fand Lilið, nicht wertend. Sie mochte die
Frage trotzdem nicht. "Dass kann ich dir nicht sagen. Im Gegensatz
zu dir bin ich keine. Mir fehlt es da an Erfahrung."

"Woran machst du fest, dass ich eine bin?", fragte Frau Wolle.

"Ich wagte, es daraus zu schließen, dass du nicht weißt, was so
schlimm daran ist, keine zu sein. Solche Fragen gestellt zu
bekommen, zum Beispiel.", erwiderte Lilið. Sie hatte ähnliche Gespräche
oft mit imaginären Personen trainiert, sonst wäre sie nie
so schlagfertig gewesen. Aber irgendwann würde der Vorrat an
zurechtgelegten und einstudierten Reaktionen ausgehen, das wusste
sie. "Sicher sind aber die Erfahrungen aller Nicht-Damen nicht
gleich. Entschuldige bitte, wenn ich da zu voreilig Schlüsse
gezogen haben sollte."

"Ich glaube eigentlich, dass undamenhaftes Verhalten das
geringere Problem wäre.", mischte
sich Herr Hut ein. "Es würde niemand merken, dass du keine Dame
wärest. Das Internat ist bereits geöffnet worden für Frauen weniger
hoher Herkunft, auch wenn jene natürlich für den Prototypen
bevorteilt werden. Bewusst, damit das Projekt Chancen hat."

Er hatte also nicht verstanden, worum es ging, stellte Lilið für sich
fest. Ein Teil von ihr war so wütend, dass sie ihn wegsperren musste und
nun nur noch mäßig konzentriert da saß. Das hatte keinen Sinn. Es
wäre besser, wenn sie das Tischgespräch nun einfach verließe. "So
spannend das Gespräch auch ist, ich muss leider gehen.", sagte
sie also.

"Aber du kommst wieder.", bestimmte ihr Vater.

Sie blickte ihn einen Moment an. Mehr war nicht notwendig für die nonverbale
Kommunikation zwischen ihnen, die ihr klar machte, wie wichtig es ihm war,
dass sie sich diesen Skoremetrikae noch einmal aussetzte. Und er es im
Zweifel durchzusetzen versuchen würde.

---

Sie ließ sich Zeit. Sie besuchte zunächst das Badezimmer, um sich
zurückzuziehen, und benutzte die Toilette vor allem, weil wenn sie sie
direkt benutzen müsste, wenn sie wieder da wäre, klar wäre, dass sie
sich aus anderen Gründen zurückgezogen hätte. Anschließend ging sie
durch einen der Ausgänge weiter hinten im Besuchshaus, sodass sie nicht am Wohnzimmer
vorbeikäme, in den Garten. Das Sangselgezwitscher begleitete
sie, als sie ihre Runden drehte. Kleine Spantunken hopsten
aufgeregt über den Boden, wo eine
bedienstete Person des Hofstaats wohl beim Transport von Backware
gekrümelt hatte. Sie pflückten mit ihren kleinen Händen die weichen
Krümel und die Körner auseinander, weil die Krümel beim Feuerspeien
verbrennen würden, die Körner ihnen aber geröstet besser mundeten. So
erklärte Lilið sich zumindest das Verhalten der kleinen, braungrauen
Drachen. Sie sah ihnen eine Weile zu. 

Leider kannte ihr Vater sie wohl zu gut. Oder das Wetter war zu
schön. Jedenfalls zog ihr Vater mit dem Besuch schon bald in den Garten um. Lilið
vermied aus bestimmten Gründen nicht, gesehen zu werden. Dazu hätte
sie im Normalfall doch Magie angewandt, -- eine sehr spezielle --, und
das wollte sie gerade nicht.

Magie war eben auch nicht Magie. Es gab schon ein paar wenige Zweige von
Magie, in denen sie brauchbare Fähigkeiten trainiert hatte.

Während sich Frau Wolle und ihr Vater an einem kleinen Gartentisch
niederließen, den einige Bedienstete eilig zurechtrückten und
mit Decke, Kerze und Keksen versahen, gedachte Herr Hut, sich zu
Lilið zu gesellen. Sie machte es ihm schwer, indem sie einen Weg von ihm
weg einschlug.

"Sie kommt schon noch.", hörte sie ihren Papa zu Frau Wolle
sagen. "Sie ist manchmal etwas kontaktscheu. Dann hilft es ihr, sich
das Meer anzusehen, etwas durchzuatmen, und dann geht es wieder."

Er hatte nicht ganz unrecht. Aber Lilið mochte nicht, wie er so über
sie sprach. Es ging dann oft wieder, aber sie wollte eigentlich gar
nicht.

Trotzdem bewegte sie sich die Stufen hinauf auf die Terasse, von der
sie gerade so über den Deich hinweg auf das Meer blicken konnte. Ein paar weiße
Öwenen flogen gegen den Wind und auf diese Weise fast auf der Stelle. Ihre
Schwingen nach den Böen immer gerade so ausrichtend, dass sie
sie nicht wegpusteten. Natürlich konnten alle Drachen fliegen. Wobei, das
stimmte auch nicht. Aber viele konnten es. Bei Öwenen hatte Lilið aber
den Eindruck, sie beherrschten das Handwerk besonders gut. (Oder eher
das Schwingenwerk als das Handwerk). Die Seedrachen
waren mehr in der Luft als die meisten anderen Drachen. Vielleicht
lag es daran, dass sie mit nur kaum merklichen Veränderungen ihrer
Schwingenstellung so perfekt auf der Stelle schwebten und den
gischtdurchsetzten Böen trotzten.

Lilið atmete die vertraute Seeluft, die doch jeden Tag anders
roch. Und stockte mitten im konzentrierten Atmen,
als sich Herr Hut direkt neben sie stellte, während
seine Hand ihren Hintern streifte. Es war so beiläufig, dass es
wohl wie ein Versehen wirken sollte. Als ob je eine Person, die
solche Art Belästigung gewohnt gewesen wäre, je abgekauft hätte,
dass es ausversehen passierte.

Lilið kannte das und verhielt sich, wie antrainiert, unintuitiv: Sie
lehnte sich in die Berührung, sodass sie definitiv nicht mehr wie
ein Ausversehen gelesen werden könnte, ihre eigene Hand aus verschiedenen
Gründen zwischen den Körpern einfädelnd. Unter anderem um sich wegschieben
zu können. "Genieß diesen Moment, es wird der letzte der Sorte
sein.", raunte sie ihm zu. "Wenn sich deine Hand noch einmal, und sei
es unbeabsichtigt, auf meinen Arsch verirrt, oder sollte ich beobachten, dass
du irgendeine andere Person ungewollt sexualisiert berührst, -- und ich
lege aus, wann das der Fall ist --, dann verirrt sich mein Ellenbogen in
Organe, die dir wichtig sind."

Sie löste ihre Körper von einander und beobachtete mit Belustigung, die
sie nur spürte, weil das Adrenalin ihr Grauen derzeit noch wegspülte, wie
er seine Hand unwillkürlich in Richtung seines Schritts bewegte.

"Ich dachte eher an deine Lunge oder dein Herz.", korrigierte sie seine
unausgesprochenen Gedanken. "Für dein Genital würde ich eher mein Knie
einsetzen."

Herr Hut lächelte. "Ich habe den Hinweis verstanden.", sagte er. "Ich
mag dich trotzdem daran erinnern, dass es nicht so besonnen ist, einen
hochkarätigen Magier zu bedrohen. Hier auf dem Hof ihres Vaters bist
du geschützt. Aber wenn du Magie nicht bis zu einem gewissen
Level erlernst, werden Menschen, die
im Gegensatz zu mir tatsächlich ein unlauteres Interesse an dir haben,
irgendwann ausnutzen, dass du eher wehrlos bist, wenn dein
Vater nicht in der Nähe ist. Du solltest Magie erlernen. Besonders
als Tochter von Lord Lurch. Sag später nicht, ich hätte dich
nicht gewarnt."

Er schlug schlendernd den Weg zum Tisch zurück ein, drehte sich aber noch einmal
zu ihr um. "Kommst du?"

"Ich komme gleich nach.", sagte sie. "Du musst verzeihen, dass ich so
einen ekligen Übergriff erst einmal verdauen muss."

"Wie gesagt, es tut mir leid. Mir war deine Grenzen nicht bewusst, die
meisten Mädchen sind da lockerer. Ich will niemandem etwas Böses.", sagte
Herr Hut. "Nimm dir die Zeit, die du brauchst. Natürlich."

Entgegen seines Namens trug er keinen Hut, den er hätte lüpfen können, um diese
Unverschämtheit noch zu krönen. (Auch keine Krone übrigens.)

Lilið stand still da und atmete. Achtete nur auf den Atem und blickte
aufs Meer hinaus. Von den Öwenen war nur noch eine da. Es tanzten hübsche
Schaumkrönchen auf dem Meer, aber von der Brandung drang nur ein kontinuierliches
Rauschen an ihr Ohr. Eines, das sie trotzdem liebte.

Sie würde ihrem Vater von diesem Übergriff erzählen. Aber ob es
etwas bringen würde? Sie erinnerte sich nun wieder an Herrn Hut. Sie hatte ihn
für ein paar Tage im Magie-Unterricht in der Erstschule gehabt. Da war er
auch schon eklig gewesen. Lilið erinnerte sich nicht an Details, nur dass
ihr Vater mit irgendwelchen Leuten in der Schule gesprochen und
sie bald darauf eine neue Magie-Lehrkraft gehabt hatte.

Sie wollte eigentlich wirklich nicht zurück zu den anderen. Sie fühlte
sich inzwischen sehr miserabel. Das Gespräch und die Erinnerungen hinterließen
ihre Spuren in ihr.

Dennoch sortierte sie sich. Das Kalkühl in ihr gewann mal wieder. Sie
wollte wissen, ob Herr Hut an jenem Internat für skorsche Damen unterrichtete. Damit
sie wissen würde, mit welcher Priorität sie sich gegen den Besuch des
Internats wehren sollte. Ob sie ihm vielleicht eine Woche lang eine Chance geben würde, eh sie
es abbräche, oder ob sie, wenn Herr Hut dort unterrichtete, gar nicht erst
hinfahren würde. Um keinen Preis.

Während sie ihren Kopf sortierte, sortierte sie außerdem ihre Beute. Ihre
Hand, die sie zwischen sich und Herrn Hut eingefädelt
hatte, hatte sich nämlich wie ausversehen in eine seiner Taschen verirrt
und ihren Inhalt herausbefördert. Es war ein Ring und ein kleines Etui
mit ein paar Marken darin. Sie sortierte alles unauffällig in die
weniger offensichtlich eingenähten Taschen ihrer eigenen Kleidung.

---

Als sie sich zu den anderen setzte, legte Frau Wolle wieder dieses Stück
Papier vor ihr ab. Es drohte, im Wind wegzuwehen. Und so sehr Lilið das Papier
gern nicht beachtet hätte, ihr Gehirn brachte es nicht über sich, zu akzeptieren, dass
es wegfliegen würde, weil sonst niemand eingriff, also griff sie ein. Um es
zu erhaschen, musste sie genauer hinsehen, und erkannte, noch
während sie wieder in den fürs Basteln eigenen Denkraum abdriftete, dass
der Trick mit dem Wind durchaus Absicht gewesen sein könnte.

Sie begriff sofort, als sie das Papier erblickte, dass es doch nicht dasselbe
wie vorhin war. Das vorgefaltete Muster war ein anderes. Dankbar schlang
sich ihr Gehirn um die Ablenkung von all dem Mist, der gerade ablief. Der
Teil ihres Denkens, der gern zufrieden mit sich selber und ihren Leistungen
war, ließ sich dieses Mal nicht so leicht bändigen. Binnen kürzesten Momenten
hatte sie raus, dass dieses Papier tatsächlich einen Alefinten
ergeben würde. Mit filigran gefalteten Stoßzähnen. Und als ihre Hand
das Papier berührte, um es am Wegfliegen zu hindern, stob
ihre Magie daraus in das Papier. Die Faltungen formten sich so schnell, dass
keine der anwesenden Personen folgen konnten. Der entstandene Alefint wollte, wie
das Papier, aus dem er gemacht war, davonfliegen, aber Lilið erwischte ihn
am Stoßzahn. Und ließ dann doch los, damit der Beweis dieses Unfalls möglichst
davon flöge.

Eine der bediensteten Personen, die eigentlich gerade damit beschäftigt
gewesen war, Lilið Tee einzugießen, stellte rasch die Kanne ab,
sprintete hinterher und brachte ihn zurück. Lilið machte keine Anstalten,
ihn entgegen zu nehmen, also gab sie ihn ihrem Vater.

"Ungewöhnliche Magie.", kommentierte Herr Hut.

Lilið konnte nicht lesen, ob er beeindruckt war. Abfällig wirkte
er jedenfalls nicht auf sie.

"Ich würde ihren Skorem auf 160 schätzen.", sagte Frau Wolle. Sie
hingegen klang ziemlich zufrieden.

Nein, stöhnte Lilið innerlich in ihrem Kopf.

"160!", betonte ihr Vater. "Das ist hoch."

"Würdest schätzen, oder schätzt du?", fragte Herr Hut. "Davon abhängig ist schließlich
die Aufnahme."

"Ich schätze ihren Skorem auf 160 ein.", sagte Frau Wolle ohne Umschweife.

"Dann wirst du hiermit von der Monarchie aufgefordert, besagtes
Internat zu besuchen.", verkündete Herr Hut. Er wirkte nicht sehr
überzeugt aber irgendwie bemüht.

"Von der Monarchie?", fragte Lord Lurch.

"Ab 150 ist das Anordnung der Königin.", antwortete Herr Hut. "Ich
fände das anders auch besser. Sie will schließlich nicht. Oder
willst du?"

Lilið schüttelte den Kopf. Ihr war nicht so klar, warum Herr Hut nun
plötzlich diesen Kurs einschlug. Ob es Mysogynie war, also er
nicht wollte, dass Leute gebildet würden, die er für Frauen hielt,
oder ob es war, weil er hoffte, sie würde seine Übergriffigkeit eher
nicht verraten, wenn sie nicht bald mehr Zeit miteinander verbringen
müssten. Oder wenn er sich für sie einsetzte. Vielleicht waren
auch all ihre Ansätze zu Mutmaßungen Unfug.

"Ich hätte mir gewünscht, dass das freiwilliger abgeht.", sagte
ihr Vater sanft. "Aber ich finde, das ist eine gute Chance für dich. Ich
habe nichts davon gewusst." Nachdenklich wog er den vom Fangen leicht
angeknitterten Alefinten in der Hand.

Lilið hatte auch nicht gewusst, dass sie ein Papier nur durch eine
sanfte Berührung in so aufgewühltem Zustand falten konnte. Obwohl
sie oft aufgewühlt gewesen war, kurz bevor sie ein neues Stück
Magie durchdrang.

Falten war ihr Spezialgebiet. Falten erschloss sich ihr einfach. Wenn
sich ihr die Beschaffenheit von etwas wirklich tief erschloss, dann
konnte sie es falten.

Noch ein Grund, dieses Gespräch rasch hinter sich zu bringen. Was, wenn
die beiden Lehrpersonen herausfanden, dass sich ihre Faltfähigkeiten
nicht nur auf Papier beschränkten?

"Unterrichtest du dort, Herr Hut?", fragte Lilið frei heraus.

Herr Hut schüttelte den Kopf. "Im Unterricht wirst du mich da nicht
genießen dürfen. Ich komme gelegentlich für Gutachten vorbei, aber es unterrichten
nur Lehrerinnen."

Einen Moment wurden Liliðs Horrorgedanken an eine sexuell übergriffige
Lehrkraft weggeschoben durch die Horrorvorstellung, nur mit Frauen
zusammen zu sein. Oder selbst als eine vereinnahmt zu werden. Interessanterweise
wusste sie nicht, was schlimmer war. Intuitiv dachte sie, Übergriffigkeit
müsste schlimmer sein, aber es gab Netze, die sich bildeten, sodass
sich Betroffene von so etwas gegenseitig schützten. Und sie würde
Leute finden, die es verstehen würden. Dass sie sich in einem Raum
nicht wohl fühlte, in den sie gesteckt wurde, weil sie für eine Frau
gehalten würde, oder dass ihr unangenehm war, wenn etwas in der Richtung
ständig irgendwie mitschwang, das verstand bisher niemand. Am ehesten
vielleicht ihre Mutter.

Sie wollte dringend zu ihrer Mutter. Und sie brauchte eine Schwinge. Im
zweiten Wohnsitz von Lord Lurch einmal quer über die Landnase mit
dem Leuchtturm an ein anderes Ufer der Insel, wo sich auch Liliðs Mutter
derzeit aufhielt, stand eines von diesen eleganten, hölzernen Tasteninstrumenten.
Eine Langschwinge, deren Saiten durch die Berührung mit den weichen
Hämmern einen sanften Klang aggressiv ertönen lassen konnten. Es
hatte Lilið immer geholfen, ihre Emotionen in Musik umzusetzen.

"Ich gehe zu Mutter."
