Die Königin der Nacht
=====================

*CN: Schlafmangel, Ratte*

Der Tag zog sich wie zäher Teig. Lilið hatte sich vielleicht
noch nie so ungeduldig gefühlt. Frühstücken, Navigieren, dem
Matrosen seine Fragen beantworten, mit denen er kontrollierte,
dass sie ihre Aufgaben richtig tat. In einem umbemerkten Moment, in
dem sie eigentlich an einem Fluchtplan hatte schmieden wollen,
festellen, dass das ohne Schlaf keinen Sinn ergab und stattdessen
eine knappe halbe Stunde unter dem Kartentisch schlafen,
dann Mittagessen. Alles war zu grell und zu laut. Sogar
Wind und Nieselregen, was sie sonst liebte, war heute ein
unangenehmer Reiz. Atmen fühlte sich schwer an. Eine
zweite halbe Stunde Schlaf bekam sie
kurz vorm Abendessen und hätte Drude sie nicht daraus geweckt, hätte
sie vielleicht das Essen verpasst. Vielleicht wäre das nicht
so schlimm gewesen, aber auf der anderen Seite gab es heute
sogar Nachtisch und die Süße darin trug dazu bei, dass
sich anschließend Navigieren wieder normal anfühlte und
nicht, als ob es eine anstrengende körperliche Belastung
wäre, dies zu tun. Leider war sie dann zur Schlafenszeit, so
aufgedreht, dass sie nicht einschlafen konnte. Sie wälzte
sich, keine sinnvolle Position findend, und gab es nach
kurzer Zeit einfach auf. Dann wohl die beiden Zeitfenster
für ihre je drei Stunden Schlaf tauschen und in den ersten
dreien die Prinzessin aufsuchen. Das war ohnehin das, was sie
den ganzen Tag trotz Angst vor Komplikationen kaum hatte
erwarten können. Wachwechsel zu jener Wache, die die Prinzessin am
Vortag aus dem anderen Raum gelassen hatte, war gerade gewesen und
dauerte jetzt sieben Stunden.

Lilið schlich sich vorsichtig zu der Treppe und achtete dabei
darauf, von niemandem gesehen zu werden. Aber sie
hätte sicher nicht bemerkt, wenn Drude sie beobachtet hätte.
Hoffentlich hatte Drude mehr Erfolg mit dem Schlafen als sie.

Am Fuße der Treppe wählte sie dieses Mal als Form, in die sie
sich faltete, doch eine Ratte aus. Eine Ratte, selbst wenn Lilið
sich für Ratten untypisch bewegte, wäre unter den Dingen,
die sich in so einem Flur bewegen würden, doch am
unauffälligsten. Sie linste um die Ecke. Die Wache döste
wieder. Jetzt schon, am Anfang ihrer Schicht. Oder sie tat
nur so, aber Lilið rechnete damit, dass sie zumindest am Vortag
wirklich gedöst hatte, sonst wäre Lilið bemerkt worden.

Sie krabbelte unbeholfen zu der Tür, die die Wache nicht bewacht
hatte und auch heute nicht beachtete. Sie
blickte sich dabei immer wieder nach ihr um, erwartete halb,
die Wache irgendwann doch nicht mehr dort sitzen oder alternativ deren
Blick direkt auf sie gerichtet zu sehen. Aber nichts passierte.

Unter der Tür gab es eine schmale Ritze. Also war die Tür auch
im geschlossenen Zustand kein Hindernis für Lilið. Sie hatte sich
als Ratte bereits vorausschauend auf eine Art ziehharmonikaartig gefaltet, dass sie
nun leicht erst den forderen Teil ihres Körpers flachfalten
konnte, um ihn mit den Hinterbeinchen unter der Ritze hindurch zu schieben,
und anschließend zu tauschen, was an ihrem Körper flach war, um den
hinteren Teil nachzuziehen. Mit ihren Augen hatte sie, als der
vordere Teil im Raum ankam, an die Decke sehen müssen, und als
sie sie wieder umfaltete, bemerkte sie, dass die Prinzessin
sie, anders als die Wache, durchaus direkt ansah. Und dass der Raum hell erleuchtet
war, während durch die Ritze unter der Tür kein Licht gefallen
war.

Die Prinzessin war damit beschäftigt gewesen, ein Kleid zu besticken. Sie saß
dazu auf dem Boden, die Beine angezogen und zur Seite gekippt, fast direkt
unter der entzündeten Lampe. Das Kleid, das sie trug, war das gleiche von gestern, aber
den Wollpullover nutzte sie nun als Sitzpolsterung. Das Kleid, das sie bestickte,
war dunkelblau und sie stickte mit schwarzem, nicht sehr gut dafür geeignetem
Garn Sterne darauf. Es erinnerte Lilið an ihr Kunstwerk für ihren Vater, das
sie einmal gestickt hatte. Die Arbeit war ähnlich wenig ordentlich, was auf
mangelnde Erfahrung hinwies, aber auch andere Gründe haben konnte.

Die Prinzessin hatte, seit Lilið im Raum war, noch keinen weiteren Stich
gemacht, sondern hielt die Nadel in die Luft, sodass der Faden gespannt
blieb, und blickte Lilið unverwandt an. Als Lilið fertig war, ihre Faltung
wieder zu sortieren und ruhig vor der Tür lungerte, grüßte die Prinzessin
mit einem freundlichen "Hallo!".

Lilið reagierte nicht sofort. Was sollte sie tun? Sie hätte sich für diesen
Teil des Plans im Vorfeld Gedanken machen sollen. Erst einmal war sie froh,
dass die Prinzessin nicht schrie, aber vielleicht würde sie es nachholen, sollte
Lilið sich entfalten.

"Du hast dich ganz schön platt gemacht!", kommentierte die Prinzessin. Sie
ließ endlich die Nadel sinken. "Willst du herkommen?"

Lilið beschloss, erst einmal näher zu krabbeln, so scheu, dass an ihrem
Krabbelstiel vielleicht nicht auffiele, dass sie gar keine Ratte war.

"Bist du eine Ratte?", fragte die Prinzessin.

Konnte die Prinzessin Gedanken lesen oder sah sie ihr das doch an? Oder
war es eine neutrale Frage? Eigentlich war es egal: Es war die
Gelegenheit, den Kopf zu schütteln, damit sie sich weniger erschrecken
würde, wenn Lilið sich entfaltete. Also tat sie es.

"Ich habe auch ein Igeldings, das kein Igel ist!", teilte die Prinzessin
mit und deutete auf etwas neben sich.

Dem Fingerzeig folgend sah Lilið etwas am Boden liegen, was sie nicht
einordnen konnte. Es hatte viele feine Stacheln, dünner und
glänzender als die von Igeln. Sie kamen Lilið auch länger und
spitzer vor, aber sie konnte sich vorstellen, dass das vom
Igel abhing. Zwischen den nadelartigen Stacheln waren Stacheln dünn wie
Haare, oder es waren tatsächlich Haare, die aber aus irgendwelchen
Gründen schnurgerade abstanden. Unten waren die Stacheln weggebogen oder
eingedellt, sodass das ansonsten quasi kugelrunde Etwas auf dem Boden lag
und nicht wegrollte. Es mochte so eingerollt sein, dass ein Kopf versteckt
hätte sein können, aber Lilið vermutete, dass es gar keinen Kopf hatte. Dann lag
es nahe, dass es nicht nur kein Igel sondern auch sonst kein Tier war.

Lilið hatte bei ihrer Betrachtung nicht mitbekommen, wie sie sich genähert
hatte, aber als sie nun wieder aufblickte, war sie der Prinzessin
schon beinahe zu nah für einen üblichen Abstand zwischen Menschen.

Die Prinzessin blickte einfach neugierig auf sie herab. "Vielleicht habe ich
das Igeldings nicht.", korrigierte sie sich. "Es gehört sich
selbst. Aber es ist bei mir, verstehst du?"

"Ich möchte mich in einen Menschen entfalten, ist das in Ordnung?", fragte
Lilið, statt darauf einzugehen. Es war vielleicht das
erste Mal, dass sie ihre Stimme in einer gefalteten
Form benutzte. Sie klang weniger voll als sonst, sondern
eher eingequetscht. Zumal ihr Mund
irgendwo im Bauch der Ratte saß, weil ihre Nase sich perfekt zu Schnauze
und Mund der Ratte hatte falten lassen. Aber ihr war der Gedanke gekommen,
dass es trotzdem möglich sein sollte, also war es sinnvoll, die Prinzessin
vorzuwarnen.

Die Prinzessin hob die Augenbrauen und schob sie etwas zusammen, nickte
aber.

Lilið versuchte, es besonders sachte zu tun. Sie fühlte sich merkwürdig
dabei. Vielleicht wie als würde sie sich ausziehen, dabei hatte sie sogar
den Mantel des Nautikas mitgefaltet. Sie schaffte es, sich so zu entfalten,
dass sie der Prinzessin am Ende auf dem Boden gegenübersaß, das Haupt
gesenkt in einer angedeuteten Verbeugung. "Du bist die Kronprinzessin,
nicht wahr?", fragte sie.

"Die Königin.", korrigierte jene Person. "Aber Lajana reicht. Ich heiße
Lajana. Und du?"

"Lilið.", antwortete Lilið. Ob das so eine gute Idee war? Dass die
Prinzessin sie beim richtigen Namen kannte? Oder die Königin? Oder
konnte sie tatsächlich von ihr als Lajana denken? Keine Person, die
mit den Familien der Monarchie nicht sehr vertraut wäre, durfte
deren Vornamen benutzten. Das ging soweit, dass Lilið vom
Namen Lajana noch nie mitbekommen hatte. "Köningin?"

"Ich verstehe von Recht nichts, aber meine Freundin schon und die
hat gesagt, dass nach der gescheiterten Krönung ich dran gewesen
wäre.", erklärte Lajana. "Vor zwei Jahren sollten mich also
Leute krönen, und weil die es nicht gemacht haben, haben meine
Freundin und ich das im Kleinen gemacht."

Lilið nickte. Es war bekannt, dass die Kronprinzessin per Gesetz schon
längst hätte gekrönt werden müssen. "Aber das Volk akzeptiert diese
Krönung natürlich nicht.", sagte Lilið. "Es weiß da vielleicht nicht
einmal etwas von. Es tut mir leid, wenn ich dich deswegen falsch
angeredet habe."

Lajana kicherte. "Natürlich weiß es nichts davon.", sagte sie und
sprach dann ernst weiter: "Glaubst
du, dass mir das alles nicht bewusst wäre? Ich weiß, dass mich niemand
ernst nimmt. Zumindest nicht, bis irgendetwas passiert. Eine offizielle
Krönung zum Beispiel, oder dass sich die Welt verändert. Ich
verstehe bestimmt vieles nicht, aber ich verstehe, dass ich im
Moment nur verborgen Königin sein kann."

"Ich verstehe.", sagte Lilið und lächelte. "Ich würde dich sehr gern
als meine Königin anerkennen. Ich würde dir gern helfen."

"Würdest du mich trainieren?", fragte Lajana.

Lilið hob zum ersten Mal den Kopf und blickte ihr verwirrt ins Gesicht.
"In Magie?", fragte sie, weil es das erste war, was ihr in den Sinn
kam, aber fügte direkt hinzu: "Oder in was?"

Lajana schüttelte den Kopf. "Die Wache, die du draußen gesehen hast,
meinte, mir solle es gut gehen. Dass ich zwar Gefangene wäre, aber
trotzdem solle ich möglichst alles haben, was ich brauche. Deshalb lässt sie mich
in ihrer Schicht immer in diesen Raum.", erklärte sie. "Mein Zellenraum
ist sehr klein, der reicht vielleicht gerade so für Liegestütze, meint
die Wache. Aber hier könnte ich mich ein bisschen bewegen. Das wäre gesund
für meinen Körper. Aber im Kreis gehen ist so langweilig! Ich würde
mir da eine Anleitung wünschen, die das interessanter macht, aber ich
will dich auch nicht herumkommandieren, nur weil du mich als Königin
anerkennst."

Lilið blickte sich im Raum um. Er war auch nicht gerade groß, immerhin
befanden sie sich an Bord einer Kagutte, die durch ihre Ausmaße
beschränkte, wie groß hier Räume sein könnten. Aber er war groß
genug, dass Lilið vielleicht nicht allzu schwindelig werden
würde, würde sie hier im Kreis rennen. Zumindest, wenn sie gelegentlich die
Richtung wechselte. Die engen Kurven würden es trotzdem
anstrengend machen.

Den Dingen nach zu urteilen, die es hier gab, war der Raum wahrscheinlich
eigentlich als Fracht- oder Abstellraum gedacht. Es gab vor allem Regale
und Gurte, um Sachen zu befestigen, ein paar nicht gebrauchte Hängematten
und Bodenmatten vermutlich für den Fall, dass die Kagutte mit größerer
Besetzung führe, Eimer, Besen, ein paar Spiele, irgendwelche
Holzbretter, Haken und Ösen, leere Säcke,
Werkzeug und Handarbeitskram. Lilið hätte so einen Raum eher dreckig
erwartet, aber vermutete, dass sich Lajana in ihren ersten Nächten hier
daran gesetzt hatte, alles zu putzen. Oder war das eine Tätigkeit, die
eine Königin niemals ausführen würde? "Hast du hier sauber
gemacht?", fragte Lilið.

Lajana nickte. "Ich habe am zweiten Tag den Dreck wieder im Raum verteilt
und noch einmal geputzt, um etwas zu tun zu haben, aber ich fand
sticken dann weniger langweilig.", erklärte sie. "In der zweiten Nacht,
meine ich, nicht am zweiten Tag. Und nähen ist zwar nett, das
mache ich gern. Aber ich kann es nicht gut. Und es ist keine
Bewegung. Keine gesunde. Verstehst du?"

"Ja, ich verstehe.", antwortete Lilið. "Ich kann hier jede Nacht für zwei Stunden
sein. Mehr Zeit habe ich leider nicht. Ich könnte mit dir zusammen in
den zwei Stunden im Raum im Kreis gehen und wir unterhalten uns dabei. Und
vielleicht kann ich mir noch etwas Besseres, Aktiveres ausdenken. Klingt
das gut?"

"Ja!" Lajana legte ihr Nähzeug beiseite und sprang auf.

Lilið hätte beinahe geseufzt. Sie hatte gerade eigentlich weniger Lust
auf Bewegung, weil sie so müde war, aber das würde schon noch kommen. Sie
stand schwerfällig auf, möglichst trotzdem ohne sich die Erschöpfung
anmerken zu lassen, und bot dann, auch wenn es sich standesmäßig vielleicht
überhaupt nicht gehörte, der Prinzessin den Arm an. Diese hakte sich ohne
Zögern mit einem Lächeln ein und sie begannen ihre Runden.

"Ich sollte vielleicht nicht so vertrauensselig sein.", sagte Lajana. "Du
warst eine Ratte. Das fand ich sympathisch. Und bisher wirkst du nett. Aber
trotzdem."

Lilið nickte. "Stimmt schon." Sie wusste nicht, ob es die Müdigkeit war, oder
ein extremes Berührungsbedürfnis, das sie im Moment hatte, aber sie mochte
die kühlen Hände auf ihrem Handgelenk und hätte am liebsten den Ärmel weiter
hochgeschoben. Da ihr ohnehin zu warm war, sollte sie beim nächsten Besuch
vielleicht ihren Mantel ablegen, bevor sie spazierten. "Ich finde diese Situation
auch sehr kompliziert.", gab sie zu. "Ich bin eigentlich nur gerade
so von Adel und kenne daher zwar höfischen Umgang, aber auch nicht so sehr, dass
ich mich darin sicher fühlen würde. Erst recht nicht mit einer Königin. Ich
hätte in meinem Leben nicht damit gerechnet, mit einer Königin persönlich
zu reden oder gar den Vornamen angeboten zu bekommen. Und nun bin ich
hier auf einer Kagutte, wo du gefangen bist und ich dir helfen möchte. Was
also auch nicht gerade ein besonders höfischer Ort ist, um eine Königin
kennenzulernen. Und ich kenne mich so wenig aus. Das überfordert
mich gerade alles."

Lajana drückte ihre Hand kurz fester. Es war eine überraschend persönliche
Geste. "Das kann ich verstehen.", sagte sie. "Wir können gern alles Höfische
sein lassen. Es hat sich für mich sowieso immer wie ein Spiel angefühlt,
bei dem ich andere herumkommandieren kann. Ich will aber nicht
herumkommandieren. Ich will regieren. Findest du, dass herumkommandieren
und regieren das Gleiche ist?"

Lilið schüttelte den Kopf. "Nein.", sagte sie, und dachte dann erst
verspätet genauer über die Frage nach. "Gute Frage, eigentlich. Was genau
ist regieren? Ich kann mir vorstellen, dass manche Leute regieren, indem
sie herumkommandieren, aber ich glaube, es gibt auch Formen, zu
regieren, die ganz ohne Herumkommandieren auskommen." Sie dachte
einen kurzen Moment an diese Insel, auf der sie gewesen
war. Schleseroge. Hatte es dort eine Regierung gegeben?

"Ich denke, so etwas möchte ich.", sagte Lajana. "Ich möchte, dass
sich alle wohlfühlen können. Und sie selbst sein können. Aber ich
glaube, irgendwer muss das organisieren und alles. Und aufpassen,
dass niemand andere daran hindert, sich wohlzufühlen. Da hängt
bestimmt eine Menge Arbeit dran, die trotzdem regiert werden
muss. Das müsste ich als Königin dann tun. Und ich weiß nicht, ob
ich das kann, aber das möchte ich eigentlich."

"Das klingt wunderschön!" Lilið meinte das wirklich. An sich hatte
sie mit Drude zuvor schon ein Gespräch in der Richtung gehabt, was
sie sich aus der Regierung durch Lajana erhoffen würde. Aber dass
Lajana sie mit ein paar kurzen Sätzen davon überzeugen könnte, dass
sie doch die richtige Person dafür sein mochte, hätte Lilið
nicht gedacht. Nach all dem, was sie über die Prinzessin gehört
hatte. Oder nach der einleitenden Vorstellung durch sie, dass
sie sich schon als Königin sah, bevor sie es näher erklärt hatte.

"Ich weiß nicht, ob ich das kann.", widerholte Lajana. Sie klang
dabei traurig, fand Lilið.

Dieses Mal versuchte Lilið tröstend ihre Hand gegen Lajanas zu
drücken, was von unten weniger gut ging. "Es ist
eine sehr schwere Aufgabe.", sagte sie. "Unterstützt
dich jemand? Der Kronprinz vielleicht? Ich habe mitbekommen, dass
er eher nicht erpicht auf das Regieren ist, aber ich habe auch
keine Ahnung, wie euer Verhältnis ist, und ob er überhaupt
Fähigkeiten hätte, die er teilen könnte, die dir helfen." Lilið
hätte sich vielleicht noch mehr bei dem Versuch verheddert, weniger
zu spekulieren, indem sie mit zu vielen Worten entschärfte, was
sie bereits gesagt hatte, aber Lajana neben ihr fing immer
mehr zu zucken an, während sie weitersprach. Und als sie einen Blick
auf Lajana warf, wirkte diese eher verstört oder sogar unglücklich. Also
brach Lilið ab.

"Können wir es lassen, über den Kronprinzen zu reden? Er existiert
für mich nicht.", sagte sie.

Lilið schluckte. Das fühlte sich nach harten Worten und harten
Gefühlen an. "Natürlich.", sagte sie. "Im Zweifel bist du Königin
und kannst das bestimmen."

"Ich will nicht kommandieren. Und bestimmen ist nun wirklich
das Gleiche wie kommandieren. Können wir auch dabei bleiben, dass
ich Lajana bin?", bat sie.

"Es tut mir leid.", antwortete Lilið. "Ja, klar können wir das. Mich
stresst die Situation ein wenig. Du hast Recht und ich hätte dir
nicht sagen sollen, dass du bestimmen kannst."

"Du bist immer noch unterwürfig.", stellte Lajana fest.

Lilið wurde einen Moment schwummrig im Kopf. Vielleicht weil das
Wort 'unterwürfig' etwas in ihr auslöste, sie an Drude erinnerte. Obwohl
von Lajana ja gerade nicht so ein Wunsch ausging, sondern einzig
der Gedanke, mit einer Königin zusammen zu flanieren, diese
merkwürdige Stimmung in ihr auslöste. Sie konnte nicht leugnen,
dass Lajana recht hatte, aber sie konnte sich auch nicht
entschuldigen, weil sie sich nicht zutraute, eine Entschuldigung
ohne jene Unterwürfigkeit hervorzubringen. Sie würde wohl gedanklich
üben müssen, sich mit ihr als gleichwertig zu sehen. "Wollen wir die Richtung
wechseln und etwas schneller gehen?", fragte Lilið.

Lajana gab ein zartes, zustimmendes Geräusch von sich und wechselte die
Richtung. Sie fügte sich rasch in das neue Tempo und es wirkte, als
würde es sie glücklich machen. Lilið steckte ihr Lächeln an, auch wenn
sie sich immer noch unsicher fühlte. Sie lief nun auf der Innenseite
und blickte in die Mitte des Raums, wo das Igeldings der Prinzessin
lag, -- und sie erschreckte sich. Hatte es nicht eben noch einen halben
Schritt näher an der Tür gelegen? Hatte es sich bewegt, als sie nicht
hingesehen hatte? Nun lag es sehr ruhig da.

"Es waren noch Fragen offen.", griff Lajana das Gespräch wieder
auf und lenkte Lilið von ihrer Beobachtung ab. "Zum Beispiel, ob
mich jemand unterstützt. Und da gibt es schon ein paar. Soll
ich aufzählen?"

"Gern!", bat Lilið. Sie haderte innerlich mit ihrer Zustimmung,
weil sie sich Gedanken darüber machte, dass
Lajana einfach so sensible Inhalte weitererzählte, und ob Lilið
vielleicht besser selbst Grenzen ziehen sollte. Aber vielleicht
war es auch eine Ausnahmesituation. Vielleicht tat Lajana es eben
bei einer Person, die als Ratte bei ihr aufkreuzte und sich
freundlich verhielt. Oder bei einem Igeldings, das Lilið ab nun
im Auge behielt.

"Im Moment ist da die Wache. Die, die draußen sitzt.", sagte
Lajana. "Sie lässt mich in diesen Raum und macht mit Magie, dass
nach draußen kein Ton und kein Licht dringt, damit ich Privatsphäre
habe."

Oh, dachte Lilið. Darum also unterhielten sie sich in Normallautstärke
und nichts passierte. Sie hatte es unterbewusst wahrgenommen,
dass sie das taten und dass ihr Gespräch eigentlich draußen hätte
hörbar sein sollen, aber Lajana hatte es so selbstverständlich getan
und es war nicht direkt etwas passiert, dass Lilið den Gedanken
erst einmal verdrängt hatte. "Mir hätte das klar sein sollen.", murmelte
Lilið.

"Ich führe gern Selbstgespräche, daher macht die das vor allem,
glaube ich.", sagte Lajana. "Sie sagt zwar, das wäre, damit ich
Privatsphäre habe, aber sie hat das erst gemacht, als ich ausversehen
mit mir selber geredet habe oder mit dem Igeldings, und ich glaube,
das ist, damit anderen Wachen, wenn sie mal runterkommen, nicht
auffällt, dass sie mich aus dem falschen Zimmer hören."

Lilið nickte und lächelte. "Das kann ich mir vorstellen.", sagte
sie. Es erfüllte sie mit einem Gefühl von wenigstens etwas
mehr Sicherheit, dass Lajana in der Lage war, so etwas zu
durchschauen. Vielleicht basierte ihr Vertrauen Lilið gegenüber
also doch auf etwas Sinnvollem. Liliðs löste den Blick einen Moment
vom Igeldings, nicht nur, weil ihr dabei doch leicht schwindelig
wurde, sondern auch, um ihm eine Möglichkeit zu geben, sich
unbemerkt zu bewegen, falls es sich nur dann bewegen würde, wenn
sie nicht hinsähe.

"Im Moment unterstützt sie mich nicht, weil sie ja nicht hier
ist, aber über die meiste Zeit meines Lebens hinweg
unterstützt mich Marusch.", fuhr Lajana fort.

Lilið stolperte fast, weil sie abrupt langsamer wurde, Lajana aber
nicht. Lilið unterdrückte den Impuls, sich nach höfischer Manier
übergründlich zu entschuldigen. "Marusch?"

"Meine Freundin.", erklärte Lajana. "Wir sind zusammen groß geworden
und sie war immer für mich da."

"Ich kenne Marusch.", informierte Lilið. "Vielleicht hätte mir klar
sein sollen, dass ihr miteinander persönlich zu tun habt. Den Verdacht
hatte ich schon ein oder zwei Mal."

"Hat Marusch dich geschickt?", fragte Lajana, löste dann aber plötzlich
ihre Hand aus Liliðs und hielt sich mit beiden den Mund zu. Nur
für einen Moment, dann gab sie ihn wieder frei, aber behielt die
Hände in seiner Nähe. "Wenn ja, erzähl mir möglichst nicht viel davon,
ja? Ich kann zwar eigentlich schweigen, aber manchmal verrate ich doch
in den falschen Momenten das Falsche und könnte dadurch einen Plan
vereiteln. Sagt sie."

Nun waren sie doch stehen geblieben. Lilið betrachtete Lajanas Gesicht,
während sie abwägte. Außerdem durchwellte sie ein Gefühl von Zuneigung. Weil
sie Marusch so sehr mochte, und nun eine Person traf, durch die sie
eine Verbindung zu ihr fühlte. Ob Marusch noch lebte? "Ich informiere
dich also erst über Dinge, sobald du etwas wissen musst.", versicherte
sie. "Ist das in deinem Sinne?"

Lajana nickte zögerlich. "Ich bin neugierig, aber ich glaube, das wäre
das beste.", sagte sie. "Vielleicht können wir uns aber trotzdem über
Marusch unterhalten. Wenn das geht, ohne dass du zu viel erzählst. Woher
kennst du sie? Geht es ihr gut?"

Dieses Mal war es ein eiskaltes Gefühl, das durch Liliðs Brustkorb
schwappte wie eine kalte Welle. Sollte sie Lajana von ihren Sorgen
über deren Freundin erzählen? "Ich weiß nicht, wie es ihr geht.", sagte
Lilið wahrheitsgemäß. Sie fühlte das Resonieren ihrer eigenen
Stimme in sich, weil sie so sanft dabei wurde. "Und ich würde mich
gern mit dir über Marusch unterhalten. Und dich weiter kennen
lernen. Aber meine Zeit ist rum. Ich
kann erst morgen Nacht wiederkommen."

Lajana lächelte, legte die Hände aneinander, die immer noch in
der Nähe ihres Mundes verharrt hatten, und beugte Kopf
und Körper sachte. Das fühlte sich surreal an: Eine Königin, die sich vor Lilið
verbeugte! "Danke, dass du da warst.", sagte Lajana. "Ich freue mich
auf ein Widersehen."

Lilið wiederholte die Geste einfach. "Ich mich auch." Dann schritt sie
Richtung Tür, wo sie sich wieder zur Ratte faltete. Das tat sie, das Gesicht der
Prinzessin zugewandt, die sie dabei neugierig beobachtete. Kurz bevor
Lilið sich umdrehte, um unter der Tür hindurchzukriechen, sah
sie es: Das Igeldings richtete sich auf,
indem es die unteren Stacheln durchstreckte, und rollte das Stück zurück,
dorthin, wo es am Anfang gelegen hatte. Morgen müsste Lilið unbedingt mehr
darüber herausfinden. Hatte es was mit Magie zu tun? Musste es eigentlich,
aber es wäre sehr ungewöhnliche.
